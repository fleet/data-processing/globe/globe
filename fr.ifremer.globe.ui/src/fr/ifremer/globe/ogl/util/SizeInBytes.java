package fr.ifremer.globe.ogl.util;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

import com.jogamp.common.nio.Buffers;

import fr.ifremer.globe.ogl.core.vectors.Vector3F;

public class SizeInBytes {
	public static <T> int getSize(T[] values) {
		if (values == null) {
			throw new IllegalArgumentException("values");
		}

		return values.length * getSize(values[0]);
	}

	public static <T> int getSize(T value) {
		if (value instanceof Buffer) {
			if (value instanceof ByteBuffer) {
				return ((Buffer) value).capacity() * Buffers.SIZEOF_BYTE;
			} else if (value instanceof IntBuffer) {
				return ((Buffer) value).capacity() * Buffers.SIZEOF_INT;
			} else if (value instanceof FloatBuffer) {
				return ((Buffer) value).capacity() * Buffers.SIZEOF_FLOAT;
			} else if (value instanceof DoubleBuffer) {
				return ((Buffer) value).capacity() * Buffers.SIZEOF_DOUBLE;
			} else if (value instanceof ShortBuffer) {
				return ((Buffer) value).capacity() * Buffers.SIZEOF_SHORT;
			} else if (value instanceof LongBuffer) {
				return ((Buffer) value).capacity() * Buffers.SIZEOF_LONG;
			}
		} else {
			return getSize(value.getClass());
		}

		return 0;
	}

	public static <T> int getSize(Class<T> clazz) {
		if (clazz.isAssignableFrom(Byte.class)) {
			return Buffers.SIZEOF_BYTE;
		} else if (clazz.isAssignableFrom(Short.class)) {
			return Buffers.SIZEOF_SHORT;
		} else if (clazz.isAssignableFrom(Integer.class)) {
			return Buffers.SIZEOF_INT;
		} else if (clazz.isAssignableFrom(Long.class)) {
			return Buffers.SIZEOF_LONG;
		} else if (clazz.isAssignableFrom(Float.class)) {
			return Buffers.SIZEOF_FLOAT;
		} else if (clazz.isAssignableFrom(Double.class)) {
			return Buffers.SIZEOF_DOUBLE;
		} else if (clazz.isAssignableFrom(Character.class)) {
			return 2;
		} else if (clazz.isAssignableFrom(Boolean.class)) {
			// Not sure of that
			return 1;
		} else if (clazz.isAssignableFrom(Vector3F.class)) {
			return 3 * Buffers.SIZEOF_FLOAT;
		} else {
			throw new IllegalArgumentException("Can't compute the size of this value type");
		}
	}
}