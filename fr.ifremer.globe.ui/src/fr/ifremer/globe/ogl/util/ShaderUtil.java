package fr.ifremer.globe.ogl.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GL3;

import fr.ifremer.globe.utils.exception.GIOException;

/**
 * ShaderLoader allows to load a shader program from files.
 * 
 * @author jlandrein
 */
public class ShaderUtil {

	private static Logger logger = LoggerFactory.getLogger(ShaderUtil.class);
	/**
	 * This function creates a shader and return a program.
	 * @param gl1 context of the OpenGL program
	 * @param vertSrc source of the vertices shader
	 * @param fragSrc source of the fragment shader
	 * @return a OpenGL program created from the input sources
	 * @throws IOException
	 */
	public static int createShader(Object requester, GL gl1, String vertSrc, String fragSrc) throws GIOException {

		GL2 gl = gl1.getGL2();
		int shaderprogram = 0;
		String[] vertSource = loadSource(requester, vertSrc);
		String[] fragSource = loadSource(requester, fragSrc);

		int vertID = gl.glCreateShader(GL2ES2.GL_VERTEX_SHADER);
		int fragID = gl.glCreateShader(GL2ES2.GL_FRAGMENT_SHADER);

		// vertSource
		gl.glShaderSource(vertID, 1, vertSource, (IntBuffer) null);
		gl.glCompileShader(vertID);
		if (!checkShaderCompilation(vertID, gl)) {
			return 0;
		}

		// fragSource
		gl.glShaderSource(fragID, 1, fragSource, (IntBuffer) null);
		gl.glCompileShader(fragID);
		if (!checkShaderCompilation(fragID, gl)) {
			return 0;
		}

		// Create the program and attach shaders to the OpenGL program
		shaderprogram = gl.glCreateProgram();
		gl.glAttachShader(shaderprogram, vertID);
		gl.glAttachShader(shaderprogram, fragID);

		gl.glLinkProgram(shaderprogram);
		gl.glValidateProgram(shaderprogram);

		return shaderprogram;
	}

	/**
	 * This function creates a shader and return a program.
	 * @param gl1 context of the OpenGL program
	 * @param vertSrc source of the vertices shader
	 * @param geomSrc source of the geometry shader
	 * @param fragSrc source of the fragment shader
	 * @return a OpenGL program created from the input sources
	 * @throws IOException
	 */
	public static int createShader(Object requester, GL gl1, String vertSrc, String geomSrc, String fragSrc) throws GIOException {

		GL2 gl = gl1.getGL2();
		int shaderprogram = 0;
		String[] vertSource = loadSource(requester, vertSrc);
		String[] geomSource = loadSource(requester, geomSrc);
		String[] fragSource = loadSource(requester, fragSrc);

		int vertID = gl.glCreateShader(GL2ES2.GL_VERTEX_SHADER);
		int geomID = gl.glCreateShader(GL3.GL_GEOMETRY_SHADER);
		int fragID = gl.glCreateShader(GL2ES2.GL_FRAGMENT_SHADER);

		// vertSource
		gl.glShaderSource(vertID, 1, vertSource, (IntBuffer) null);
		gl.glCompileShader(vertID);
		if (!checkShaderCompilation(vertID, gl)) {
			throw new GIOException("Error compiling Vertex shader "+ Arrays.toString(vertSource));
		}

		// geomSource
		gl.glShaderSource(geomID, 1, geomSource, (IntBuffer) null);
		gl.glCompileShader(geomID);
		if (!checkShaderCompilation(geomID, gl)) {
			throw new GIOException("Error compiling Geometry shader "+ Arrays.toString(geomSource));
		}

		// fragSource
		gl.glShaderSource(fragID, 1, fragSource, (IntBuffer) null);
		gl.glCompileShader(fragID);
		if (!checkShaderCompilation(fragID, gl)) {
			throw new GIOException("Error compiling Fragment shader "+ Arrays.toString(fragSource));
		}

		// Create the program and attach shaders to the OpenGL program
		shaderprogram = gl.glCreateProgram();
		if(shaderprogram==0)
		{
			throw new GIOException("Error creating shader program");
		}
		gl.glAttachShader(shaderprogram, vertID);
		gl.glAttachShader(shaderprogram, geomID);
		gl.glAttachShader(shaderprogram, fragID);

		gl.glLinkProgram(shaderprogram);
		checkProgramStatus(shaderprogram, gl1);
		gl.glValidateProgram(shaderprogram);
		checkProgramStatus(shaderprogram, gl1);

		return shaderprogram;
	}

	/**
	 * This function reads a shader file in order to get sources.
	 * @param shaderFile
	 * 
	 * @return String[]
	 * @throws IOException
	 */
	private static String[] loadSource(Object requester, String shaderFile) throws GIOException {

		StringBuilder sb = new StringBuilder();

		try(InputStream is = requester.getClass().getResourceAsStream(shaderFile);
				BufferedReader br = new BufferedReader(new InputStreamReader(is))) {


			String line;
			while ((line = br.readLine()) != null) {

				sb.append(line);
				sb.append('\n');
			}

		} catch (Exception e) {
			throw new GIOException("Error parsing shader file " + shaderFile, e);
		}
		return new String[] { sb.toString() };
	}

	/**
	 * This function checks if the shader has been compiled successfully
	 * 
	 * @param shaderID
	 * @param gl
	 * @return boolean
	 */
	private static boolean checkShaderCompilation(int shaderID, GL gl1) {

		GL2 gl = gl1.getGL2();

		int[] error = new int[4];
		boolean status = true;

		gl.glGetShaderiv(shaderID, GL2ES2.GL_COMPILE_STATUS, error, 0);
		if (error[0] != GL.GL_TRUE ) {

			gl.glGetShaderiv(shaderID, GL2ES2.GL_INFO_LOG_LENGTH, error, 1);

			int size = Integer.valueOf(error[1]);
			byte[] msg = new byte[size];
			gl.glGetShaderInfoLog(shaderID, size, error, 2, msg, 0);
			gl.glDeleteShader(shaderID);

			status = false;
			logger.error("Erreur de compilation du shader : " + new String(msg));
		}

		return status;
	}
	private static void checkProgramStatus(int shaderprogram, GL gl1) throws GIOException
	{
		GL2 gl = gl1.getGL2();

		// debug compilation linker Shader
		int statusLinker[] = new int[1];
		gl.glGetProgramiv(shaderprogram, GL2ES2.GL_LINK_STATUS, IntBuffer.wrap(statusLinker));
		if (statusLinker[0] == GL.GL_FALSE) {
			int infoLogLenght[] = new int[1];
			gl.glGetProgramiv(shaderprogram, GL2ES2.GL_INFO_LOG_LENGTH, IntBuffer.wrap(infoLogLenght));
			ByteBuffer infoLog = Buffers.newDirectByteBuffer(infoLogLenght[0]);
			gl.glGetProgramInfoLog(shaderprogram, infoLogLenght[0], null, infoLog);
			byte[] infoBytes = new byte[infoLogLenght[0]];
			infoLog.get(infoBytes);
			String out = new String(infoBytes);
			throw new GIOException("Compilation Linker Shader error:\n" + out);
		}
	}
}