package fr.ifremer.globe.ogl.util;

public enum ViewProjectionType {
	PERSPECTIVE,
	ORTHOGRAPHIC
}
