package fr.ifremer.globe.ogl.util;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A KeyedCollection is a light weight wrapper around a Map (LinkedHashMap)
 * values set. When a value is added using the add method of this class a key is
 * obtained for the value using either the provided KeyExtractor, or if no
 * KeyExtractor was provided, the value is cast to Keyable and the getKey()
 * method is called.
 * 
 * The underlying Map can be obtainded with the toMap method. Any changes to
 * this map are directly reflected in this collection. Additions to the map do
 * not need to implement Keyable, nor do the values need to be keyed using the
 * key returned from the KeyExtractor.getKey(value) or the key returned from the
 * Keyable.getKey() Method.
 */
public class KeyedCollection<K, V> extends AbstractCollection<V> {
	private final KeyExtractor<? extends K, ? super V> keyExtractor;
	private final LinkedHashMap<K, V> map;

	public KeyedCollection() {
		// NOTE: V must implement Keyable or class cast exception will be thrown
		// on
		// add
		keyExtractor = null;
		map = new LinkedHashMap<K, V>();
	}

	public KeyedCollection(KeyExtractor<? extends K, ? super V> keyExtractor) {
		this.keyExtractor = keyExtractor;
		map = new LinkedHashMap<K, V>();
	}

	@SuppressWarnings({ "unchecked" })
	public KeyedCollection(Collection<? extends V> c) {
		if (c instanceof KeyedCollection) {
			@SuppressWarnings("rawtypes")
			KeyedCollection keyedCollection = (KeyedCollection) c;
			// NOTE: if types don't match bad things could happen
			keyExtractor = keyedCollection.keyExtractor;
		} else {
			// NOTE: V must implement Keyable or class cast exception will be
			// thrown
			// on add
			keyExtractor = null;
		}
		map = new LinkedHashMap<K, V>();
		addAll(c);
	}

	public KeyedCollection(int initialCapacity) {
		// NOTE: V must implement Keyable or class cast exception will be thrown
		// on
		// add
		keyExtractor = null;
		map = new LinkedHashMap<K, V>(initialCapacity);
	}

	/**
	 * Get the underlying map used by this collection.
	 * 
	 * Any changes to this map are directly reflected in this collection.
	 * Additions to the map do not need to implement Keyable, nor do the values
	 * need to be keyed using the key returned from the
	 * KeyExtractor.getKey(value) or the key returned from the Keyable.getKey()
	 * Method.
	 * 
	 * @return the indexed contents of this collection
	 */
	public Map<K, V> toMap() {
		return map;
	}

	public V get(K key) {
		return map.get(key);
	}

	@Override
	public boolean add(V value) {
		K key = getKey(value);
		if (key == null) {
			throw new NullPointerException("key is null");
		}
		V oldValue = map.put(key, value);
		return value != oldValue;
	}

	@Override
	public Iterator<V> iterator() {
		return map.values().iterator();
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public String toString() {
		return map.toString();
	}

	@SuppressWarnings({ "unchecked" })
	protected K getKey(V value) {
		if (keyExtractor == null) {
			return ((Keyable<? extends K>) value).getKey();
		} else {
			return keyExtractor.getKey(value);
		}
	}

	public boolean containsKey(Object o) {
		Iterator<K> e = map.keySet().iterator();
		if (o == null) {
			while (e.hasNext()) {
				if (e.next() == null) {
					return true;
				}
			}
		} else {
			while (e.hasNext()) {
				if (o.equals(e.next())) {
					return true;
				}
			}
		}
		return false;
	}

}