package fr.ifremer.globe.ogl.util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;

import javax.swing.SwingUtilities;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

import fr.ifremer.globe.ui.utils.color.ColorMap;

/**
 * compute palette texture
 */
public class PaletteTexture {

	// Argument about color palette of this renderer
	protected Texture paletteTexture = null;
	private BufferedImage paletteImage = null;
	protected int currentPalette;
	protected boolean currentPaletteInverted;

	/**
	 * Current GLContext
	 */
	private GL currentGL;

	public void dispose() {
		SwingUtilities.invokeLater(() -> {
			if (currentGL != null && currentGL.getContext().makeCurrent() != GLContext.CONTEXT_NOT_CURRENT) {
				if (paletteTexture != null) {
					paletteTexture.destroy(currentGL);
				}
			}
		});
	};

	/**
	 * Initialize the color palette which will be applied to this renderer.
	 * 
	 * @param gl OpenGL environment context
	 * @return
	 */
	public Texture getColorPalette(GL2 gl, int colorMapIndex, boolean isColorMapInverted) {
		// check if texture shall be generated
		if (this.paletteImage == null || this.paletteTexture == null //
				|| currentPalette != colorMapIndex || currentPaletteInverted != isColorMapInverted) {

			// Reset the variables which defines the condition just before
			this.currentPalette = colorMapIndex;
			this.currentPaletteInverted = isColorMapInverted;
			// Build the buffered image which will represent the chosen palette
			float[][] colors = ColorMap.getColormap(this.currentPalette);
			int width = 100;
			int height = 5;
			this.paletteImage = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()
					.getDefaultConfiguration().createCompatibleImage(width, height);
			// Define a shader program for color palette generation
			Graphics2D g2 = (Graphics2D) paletteImage.getGraphics();
			// Fill the buffered image with the color defined by the chosen palette
			for (int i = 0; i < width; i++) {
				int index = Math.round((i / (float) (width - 1)) * 255);
				if (isColorMapInverted) {
					index = 255 - index;
				}
				int r = (int) (colors[ColorMap.red][index] * 255);
				int g = (int) (colors[ColorMap.green][index] * 255);
				int b = (int) (colors[ColorMap.blue][index] * 255);
				// Use OpenGL instruction to draw a vertical line with the desired color
				g2.setColor(new Color(r, g, b));
				g2.drawLine(i, 0, i, height);
			}
			// Dispose the shader program used for color palette generation
			g2.dispose();
			// Generation of a texture from the buffered image.
			if (paletteTexture != null) {
				paletteTexture.destroy(gl);
			}
			paletteTexture = AWTTextureIO.newTexture(gl.getGLProfile(), paletteImage, false);
			currentGL = gl;
		}
		return paletteTexture;
	}
}
