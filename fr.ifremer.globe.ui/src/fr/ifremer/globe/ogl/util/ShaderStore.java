package fr.ifremer.globe.ogl.util;

import java.util.HashMap;
import java.util.Map;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLContext;
import javax.swing.SwingUtilities;

import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Utility class to maintain and store shader program ids
 */
public class ShaderStore {
	/**
	 * Map which link all the shader programs with their ID. One shader program instance needed for each OGL context.
	 */
	private Map<GL, Integer> shaderPrograms;
	private ShaderProvider provider;

	public static final int DEFAULT_SHADER_PROGRAM = 0;

	public interface ShaderProvider {
		int computeShaders(GL2 gl) throws GIOException;
	}

	public ShaderStore(ShaderProvider provider) {
		this.provider = provider;
		shaderPrograms = new HashMap<>();
	}

	/**
	 * retrieve the shader program associated with the current GL context
	 *
	 * @param gl OpenGL environment context
	 * @return the ID of the shader program used by this renderer
	 * @throws GIOException if an error occured during compilation *
	 */
	public int getShaderProgram(GL2 gl) throws GIOException {
		Integer shaderProgramID = shaderPrograms.get(gl);
		if (shaderProgramID == null) {
			try {
				shaderProgramID = provider.computeShaders(gl);
			} catch (Exception e) {
				throw new GIOException("Unknown error occured during shader compilation", e);
			}
			if (shaderProgramID != 0) {
				shaderPrograms.put(gl, shaderProgramID);
			} else { // this case should never happen, shader provider should ensure good compilation or error exception
						// should be thrown
				throw new GIOException("Unknown error occured during shader compilation");
			}
		}
		return shaderProgramID;
	}

	/** Dipose this store. Delete all computed shaders */
	public void dispose() {
		SwingUtilities.invokeLater(() -> {
			for (var iterator = shaderPrograms.entrySet().iterator(); iterator.hasNext();) {
				var entry = iterator.next();
				iterator.remove();
				if (entry.getKey().isGL2()) {
					GLContext context = entry.getKey().getGL2().getContext();
					if (context != null && context.makeCurrent() > 0)
						entry.getKey().getGL2().glDeleteProgram(entry.getValue());
				}
			}
		});
	}
}
