package fr.ifremer.globe.ogl.util;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;

public class CoordinatesProjection {

	/** Text rendering */
	private static GLU glu = new GLU();

	/**
	 * Constructor
	 */
	private CoordinatesProjection() {
		// utility class
	}

	/**
	 * Converts screen coordinates to scene coordinates
	 * 
	 * @param glad
	 * @param x
	 * @param y
	 * @param coords
	 */
	public static void fromScreenToScene(GLAutoDrawable glad, int x, int y, double[] coords) {
		double mvmatrix[] = new double[16];
		double projmatrix[] = new double[16];
		int viewport[] = new int[4];
		GL2 gl2 = glad.getGL().getGL2();

		// because of the projection input coordinate system which is different from window AWT
		int h = glad.getSurfaceHeight();
		y = h - y;

		gl2.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		gl2.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl2.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
		glu.gluUnProject(x, y, 0, mvmatrix, 0, projmatrix, 0, viewport, 0, coords, 0);
	}

	/**
	 * Converts scene coordinates to screen coordinates
	 * 
	 * @param glad
	 * @param x
	 * @param y
	 * @param z
	 * @param coords
	 */
	public static void fromSceneToScreen(GLAutoDrawable glad, double x, double y, double z, double[] coords) {
		double mvmatrix[] = new double[16];
		double projmatrix[] = new double[16];
		int viewport[] = new int[4];
		GL2 gl2 = glad.getGL().getGL2();
		gl2.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		gl2.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl2.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
		glu.gluProject(x, y, z, mvmatrix, 0, projmatrix, 0, viewport, 0, coords, 0);

		// because of the projection result coordinate system which is different from window AWT
		int h = glad.getSurfaceHeight();
		coords[1] = h - coords[1];
	}
}
