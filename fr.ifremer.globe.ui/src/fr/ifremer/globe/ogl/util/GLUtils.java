package fr.ifremer.globe.ogl.util;

import java.util.Optional;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GLUtils {
	private final static Logger logger = LoggerFactory.getLogger(GLUtils.class);

	/**
	 * Returns the OpenGL error trace
	 * 
	 * @param gl OpenGL environment context
	 */
	public static Optional<String> checkGLError(GL2 gl) {
		StringBuilder sb = new StringBuilder();
		for (int err; (err = gl.glGetError()) != GL2.GL_NO_ERROR;) {
			String errString = "Existing glError : 0x" + Integer.toHexString(err);
			switch (err) {
			case GL.GL_INVALID_ENUM:
				errString += " (= GL_INVALID_ENUM)";
				break;
			case GL.GL_INVALID_VALUE:
				errString += " (= GL_INVALID_VALUE)";
				break;
			case GL.GL_INVALID_OPERATION:
				errString += " (= GL_INVALID_OPERATION)";
				break;
			case GL.GL_INVALID_FRAMEBUFFER_OPERATION:
				errString += " (= GL_INVALID_FRAMEBUFFER_OPERATION)";
				break;
			}
			sb.append(errString);
			logger.warn(errString);
		}
		return sb.length() > 0 ? Optional.of(sb.toString()) : Optional.empty();
	}

}
