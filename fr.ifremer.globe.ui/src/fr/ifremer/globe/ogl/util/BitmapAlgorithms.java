package fr.ifremer.globe.ogl.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

/**
 * <p>
 * Description : BitmapAlgorithms.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 17 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class BitmapAlgorithms {

	/**
	 * Logger.
	 */
	private static final Logger LOGGER = Logger.getLogger(BitmapAlgorithms.class);

	public static BufferedImage createBitmapFromPoint(int radiusInPixels) {
		if (radiusInPixels < 1) {
			throw new IllegalArgumentException("radius");
		}

		int diameter = radiusInPixels * 2;

		BufferedImage bitmap = new BufferedImage(diameter, diameter, BufferedImage.TYPE_INT_RGB);
		Graphics graphics = bitmap.getGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillOval(0, 0, diameter, diameter);
		graphics.dispose();

		return bitmap;
	}

	public static BufferedImage createBitmapFromText(String text, Font font) {

		// From
		// http://www.coderanch.com/t/481563/java/java/Placing-text-BufferedImage
		BufferedImage img = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
		Graphics g = img.getGraphics();
		g.setFont(font);
		FontRenderContext frc = g.getFontMetrics().getFontRenderContext();

		Rectangle2D rect = font.getStringBounds(text, frc);
		// release resources
		g.dispose();

		img = new BufferedImage((int) Math.ceil(rect.getWidth()), (int) Math.ceil(rect.getHeight()), BufferedImage.TYPE_INT_RGB);

		g = img.getGraphics();
		g.setColor(Color.BLUE);
		g.setFont(font);
		g.drawString(text, 0, 0);
		// release resources
		g.dispose();

		return img;
	}

	public static ByteBuffer getData(BufferedImage img) {
		ByteBuffer result = null;

		DataBuffer dataBuffer = img.getData().getDataBuffer();
		if (dataBuffer instanceof DataBufferByte) {
			result = ByteBuffer.wrap(((DataBufferByte) dataBuffer).getData());
		}

		else {
			// Source : http://mindprod.com/jgloss/imageio.html#TOBYTES
			try {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write(img, "bmp", baos);
				baos.flush();
				byte[] imageBytes = baos.toByteArray();
				baos.close();

				result = ByteBuffer.wrap(imageBytes);
			} catch (IOException e) {
				LOGGER.error(e, e);
			}
		}

		return result;
	}

	public static BufferedImage getImage(ByteBuffer data) {
		BufferedImage result = null;

		byte[] imageInByte = data.array();
		InputStream in = new ByteArrayInputStream(imageInByte);
		try {
			result = ImageIO.read(in);
		} catch (IOException e) {
			LOGGER.error(e, e);
		}

		return result;
	}

	public static BufferedImage getImage(int width, int height, ByteBuffer data) {
		// Source :
		// http://stackoverflow.com/questions/6729672/jogl-procedural-textures-are-blobbed

		int[] pixelInts = new int[width * height];

		int p = width * height * 4;
		int q; // Index into ByteBuffer
		int i = 0; // Index into target int[]
		int w3 = width * 4; // Number of bytes in each row
		for (int row = 0; row < width; row++) {
			p -= w3;
			q = p;
			for (int col = 0; col < width; col++) {
				int iR = data.get(q++) * 2; // 127*2 ~ 256.
				int iG = data.get(q++) * 2;
				int iB = data.get(q++) * 2;
				int iA = data.get(q++) * 2;
				pixelInts[i++] = ((iA & 0x000000FF) << 24) | ((iR & 0x000000FF) << 16) | ((iG & 0x000000FF) << 8) | (iB & 0x000000FF);
			}
		}

		// Create a new BufferedImage from the pixeldata.
		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		bufferedImage.setRGB(0, 0, width, height, pixelInts, 0, width);
		return bufferedImage;
	}

	/**
	 * This method flips the image horizontally
	 * 
	 * @param img
	 *            --> BufferedImage Object to be flipped
	 */
	public static BufferedImage horizontalflip(BufferedImage img) {
		// Source : http://sanjaal.com/java/tag/flip-bufferedimage-in-java/
		int w = img.getWidth();
		int h = img.getHeight();
		BufferedImage dimg = new BufferedImage(w, h, img.getType());
		Graphics2D g = dimg.createGraphics();
		/*
		 * img - the specified image to be drawn. This method does nothing if
		 * img is null. dx1 - the x coordinate of the first corner of the
		 * destination rectangle. dy1 - the y coordinate of the first corner of
		 * the destination rectangle. dx2 - the x coordinate of the second
		 * corner of the destination rectangle. dy2 - the y coordinate of the
		 * second corner of the destination rectangle. sx1 - the x coordinate of
		 * the first corner of the source rectangle. sy1 - the y coordinate of
		 * the first corner of the source rectangle. sx2 - the x coordinate of
		 * the second corner of the source rectangle. sy2 - the y coordinate of
		 * the second corner of the source rectangle. observer - object to be
		 * notified as more of the image is scaled and converted.
		 */
		g.drawImage(img, 0, 0, w, h, w, 0, 0, h, null);
		g.dispose();
		return dimg;
	}

	public static int sizeOfPixelsInBytes(BufferedImage bitmap) {

		int result = getData(bitmap).capacity();

		return result;
	}

	/**
	 * This method flips the image vertically.
	 * 
	 * @param img
	 *            --> BufferedImage object to be flipped
	 */
	public static BufferedImage verticalflip(BufferedImage img) {
		// Source : http://sanjaal.com/java/tag/flip-bufferedimage-in-java/
		int w = img.getWidth();
		int h = img.getHeight();
		BufferedImage dimg = new BufferedImage(w, h, img.getColorModel().getTransparency());
		Graphics2D g = dimg.createGraphics();
		g.drawImage(img, 0, 0, w, h, 0, h, w, 0, null);
		g.dispose();
		return dimg;
	}

}
