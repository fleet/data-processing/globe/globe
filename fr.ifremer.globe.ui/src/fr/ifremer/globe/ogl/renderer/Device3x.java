package fr.ifremer.globe.ogl.renderer;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.glu.GLU;

import org.apache.log4j.Logger;
import org.eclipse.swt.widgets.Composite;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureData;
import com.jogamp.opengl.util.texture.TextureIO;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

import fr.ifremer.globe.ogl.core.geometry.Mesh;
import fr.ifremer.globe.ogl.core.geometry.indices.IndicesInt;
import fr.ifremer.globe.ogl.core.geometry.indices.IndicesType;
import fr.ifremer.globe.ogl.core.geometry.vertexattributes.VertexAttribute;
import fr.ifremer.globe.ogl.core.geometry.vertexattributes.VertexAttributeDoubleVector3;
import fr.ifremer.globe.ogl.core.geometry.vertexattributes.VertexAttributeRGB;
import fr.ifremer.globe.ogl.core.geometry.vertexattributes.VertexAttributeRGBA;
import fr.ifremer.globe.ogl.core.geometry.vertexattributes.VertexAttributeType;
import fr.ifremer.globe.ogl.core.vectors.EmulatedVector3D;
import fr.ifremer.globe.ogl.core.vectors.Vector2F;
import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.ogl.core.vectors.Vector3F;
import fr.ifremer.globe.ogl.core.vectors.Vector4F;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.buffers.IndexBuffer;
import fr.ifremer.globe.ogl.renderer.buffers.UniformBuffer;
import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;
import fr.ifremer.globe.ogl.renderer.jogl.ExtensionsGL2x;
import fr.ifremer.globe.ogl.renderer.jogl.GraphicsWindowGL3x;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.IndexBufferGL3x;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.UniformBufferGL2x3x;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.VertexBufferGL2x3x;
import fr.ifremer.globe.ogl.renderer.jogl.shaders.ShaderProgramGL3x;
import fr.ifremer.globe.ogl.renderer.jogl.textures.TextureSamplerGL2xGL3;
import fr.ifremer.globe.ogl.renderer.mesh.MeshBuffers;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderProgram;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderVertexAttribute;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderVertexAttributeCollection;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.CameraEyeHighUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.CameraEyeLowUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.CameraEyeUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.CameraLightPositionUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.DrawAutomaticUniformFactoryCollection;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.HighResolutionSnapScaleUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.InverseViewportDimensionsUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.LightPropertiesUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.ModelMatrixUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.ModelViewMatrixRelativeToEyeUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.ModelViewMatrixUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.ModelViewPerspectiveMatrixRelativeToEyeUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.ModelViewPerspectiveMatrixUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.ModelZToClipCoordinatesUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.PerspectiveMatrixUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.SunPositionUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.ViewMatrixUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.ViewportOrthographicMatrixUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.ViewportTransformationMatrixUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.ViewportUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.Wgs84HeightUniformFactory;
import fr.ifremer.globe.ogl.renderer.shaders.linkautomaticuniforms.LinkAutomaticUniformCollection;
import fr.ifremer.globe.ogl.renderer.shaders.linkautomaticuniforms.TextureUniform;
import fr.ifremer.globe.ogl.renderer.textures.TextureFloat;
import fr.ifremer.globe.ogl.renderer.textures.TextureFloatData;
import fr.ifremer.globe.ogl.renderer.textures.TextureMagnificationFilter;
import fr.ifremer.globe.ogl.renderer.textures.TextureMinificationFilter;
import fr.ifremer.globe.ogl.renderer.textures.TextureSampler;
import fr.ifremer.globe.ogl.renderer.textures.TextureSamplers;
import fr.ifremer.globe.ogl.renderer.textures.TextureWrap;
import fr.ifremer.globe.ogl.renderer.vertexarray.ComponentDatatype;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute;
import fr.ifremer.globe.ogl.renderer.window.GraphicsWindow;
import fr.ifremer.globe.ogl.renderer.window.WindowType;
import fr.ifremer.globe.ogl.util.SizeInBytes;

/**
 * <p>
 * Description : Device.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class Device3x implements Device {

	static final public Device3x instance=new Device3x();

	// La classe statique a été transformée en singleton afin
	// d'implémenter l'interface GLeventListener donnant accès à l'objet GL.

	/**
	 * Logger.
	 */
	private static final Logger LOGGER = Logger.getLogger(Device3x.class);

	public static void initialize() {

		GL3 gl = GLU.getCurrentGL().getGL3();

		LOGGER.debug("Device initialized");

		// GL.GetInteger(GetPName.MaxVertexAttribs, out
		// s_maximumNumberOfVertexAttributes);
		// GL.GetInteger(GetPName.MaxCombinedTextureImageUnits, out
		// s_numberOfTextureUnits);
		// GL.GetInteger(GetPName.MaxColorAttachments, out
		// s_maximumNumberOfColorAttachments);
		IntBuffer result = IntBuffer.allocate(1);
		gl.glGetIntegerv(GL2ES2.GL_MAX_VERTEX_ATTRIBS, result);
		s_maximumNumberOfVertexAttributes = result.get(0);
		gl.glGetIntegerv(GL2ES2.GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, result);
		s_numberOfTextureUnits = result.get(0);
		gl.glGetIntegerv(GL2ES2.GL_MAX_COLOR_ATTACHMENTS, result);
		s_maximumNumberOfColorAttachments = result.get(0);

		// /////////////////////////////////////////////////////////////

		s_extensions = new ExtensionsGL2x();

		// /////////////////////////////////////////////////////////////

		LinkAutomaticUniformCollection linkAutomaticUniforms = new LinkAutomaticUniformCollection();

		gl.glGetIntegerv(GL2ES2.GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, result);
		int numberOfTextureUnits = result.get(0);
		for (int i = 0; i < numberOfTextureUnits; ++i) {
			linkAutomaticUniforms.add(new TextureUniform(i));
		}

		s_linkAutomaticUniforms = linkAutomaticUniforms;

		// /////////////////////////////////////////////////////////////

		DrawAutomaticUniformFactoryCollection drawAutomaticUniformFactories = new DrawAutomaticUniformFactoryCollection();

		drawAutomaticUniformFactories.add(new SunPositionUniformFactory());
		drawAutomaticUniformFactories.add(new LightPropertiesUniformFactory());
		drawAutomaticUniformFactories.add(new CameraLightPositionUniformFactory());
		drawAutomaticUniformFactories.add(new CameraEyeUniformFactory());
		drawAutomaticUniformFactories.add(new CameraEyeHighUniformFactory());
		drawAutomaticUniformFactories.add(new CameraEyeLowUniformFactory());
		drawAutomaticUniformFactories.add(new ModelViewPerspectiveMatrixRelativeToEyeUniformFactory());
		drawAutomaticUniformFactories.add(new ModelViewMatrixRelativeToEyeUniformFactory());
		drawAutomaticUniformFactories.add(new ModelViewPerspectiveMatrixUniformFactory());
		// drawAutomaticUniformFactories.add(new
		// ModelViewOrthographicMatrixUniformFactory());
		drawAutomaticUniformFactories.add(new ModelViewMatrixUniformFactory());
		drawAutomaticUniformFactories.add(new ModelMatrixUniformFactory());
		drawAutomaticUniformFactories.add(new ViewMatrixUniformFactory());
		drawAutomaticUniformFactories.add(new PerspectiveMatrixUniformFactory());
		// drawAutomaticUniformFactories.add(new
		// OrthographicMatrixUniformFactory());
		drawAutomaticUniformFactories.add(new ViewportOrthographicMatrixUniformFactory());
		drawAutomaticUniformFactories.add(new ViewportUniformFactory());
		drawAutomaticUniformFactories.add(new InverseViewportDimensionsUniformFactory());
		drawAutomaticUniformFactories.add(new ViewportTransformationMatrixUniformFactory());
		drawAutomaticUniformFactories.add(new ModelZToClipCoordinatesUniformFactory());
		// drawAutomaticUniformFactories.add(new
		// WindowToWorldNearPlaneUniformFactory());
		drawAutomaticUniformFactories.add(new Wgs84HeightUniformFactory());
		// drawAutomaticUniformFactories.add(new
		// PerspectiveNearPlaneDistanceUniformFactory());
		// drawAutomaticUniformFactories.add(new
		// PerspectiveFarPlaneDistanceUniformFactory());
		drawAutomaticUniformFactories.add(new HighResolutionSnapScaleUniformFactory());
		// drawAutomaticUniformFactories.add(new
		// PixelSizePerDistanceUniformFactory());

		s_drawAutomaticUniformFactories = drawAutomaticUniformFactories;

		// /////////////////////////////////////////////////////////////

		s_textureSamplers = new TextureSamplers();

	}

	public static GraphicsWindow createWindow(int width, int height) {
		return createWindow(width, height, "");
	}

	public static GraphicsWindow createWindow(Composite parent)
	{
		return new GraphicsWindowGL3x(parent);

	}

	public static GraphicsWindow createWindow(int width, int height, String title) {
		return createWindow(width, height, title, WindowType.Default);
	}

	public static GraphicsWindow createWindow(int width, int height, String title, WindowType windowType) {
		return new GraphicsWindowGL3x(width, height, title, windowType);
	}

	public static ShaderProgram createShaderProgram(String vertexShaderSource, String fragmentShaderSource) {
		try {
			return new ShaderProgramGL3x(vertexShaderSource, fragmentShaderSource);
		} catch (CouldNotCreateVideoCardResourceException e) {
			LOGGER.error(e, e);
		}

		return null;
	}

	public static ShaderProgram createShaderProgram(String vertexShaderSource, String geometryShaderSource,
			String fragmentShaderSource) {
		try {
			return new ShaderProgramGL3x(vertexShaderSource, geometryShaderSource, fragmentShaderSource, true);
		} catch (CouldNotCreateVideoCardResourceException e) {
			LOGGER.error(e, e);
		}

		return null;
	}

	public static ShaderProgram createShaderProgram(String vertexShaderSource, String geometryShaderSource,
			String fragmentShaderSource, boolean example) {
		try {
			return new ShaderProgramGL3x(vertexShaderSource, geometryShaderSource, fragmentShaderSource, example);
		} catch (CouldNotCreateVideoCardResourceException e) {
			LOGGER.error(e, e);
		}

		return null;
	}

	public static VertexBuffer createVertexBuffer(BufferHint usageHint, int sizeInBytes) {
		return new VertexBufferGL2x3x(usageHint, sizeInBytes);
	}

	public static IndexBuffer createIndexBuffer(BufferHint usageHint, int size) {
		return new IndexBufferGL3x(usageHint, size);
	}

	@SuppressWarnings("unchecked")
	public static MeshBuffers createMeshBuffers(Mesh mesh, ShaderVertexAttributeCollection shaderAttributes,
			BufferHint usageHint) {
		if (mesh == null) {
			throw new IllegalArgumentException("mesh");
		}

		if (shaderAttributes == null) {
			throw new IllegalArgumentException("shaderAttributes");
		}

		MeshBuffers meshBuffers = new MeshBuffers(instance);

		if (mesh.getIndices() != null) {
			if (mesh.getIndices().getDatatype() == IndicesType.Int) {
				List<Integer> meshIndices = ((IndicesInt) mesh.getIndices()).getValues();

				IntBuffer indices = Buffers.newDirectIntBuffer(meshIndices.size());
				for (int j = 0; j < meshIndices.size(); ++j) {
					indices.put(meshIndices.get(j));
				}

				IndexBuffer indexBuffer = createIndexBuffer(usageHint, SizeInBytes.getSize(indices));
				indexBuffer.copyFromSystemMemory(indices);
				meshBuffers.setIndexBuffer(indexBuffer);
			} else {
				throw new IllegalStateException(
						"mesh.Indices.Datatype " + mesh.getIndices().getDatatype().toString() + " is not supported.");
			}
		}

		//
		// Emulated double precision vectors are a special case: one mesh vertex
		// attribute
		// yields two shader vertex attributes. As such, these are handled
		// separately before
		// normal attributes.
		//
		Set<String> ignoreAttributes = new HashSet<String>();

		for (VertexAttribute<?> attribute : mesh.getAttributes()) {
			if (attribute instanceof VertexAttributeDoubleVector3) {
				VertexAttributeDoubleVector3 emulated = (VertexAttributeDoubleVector3) attribute;

				int highLocation = -1;
				int lowLocation = -1;

				for (ShaderVertexAttribute shaderAttribute : shaderAttributes) {
					if (shaderAttribute.getName() == emulated.getName() + "High") {
						highLocation = shaderAttribute.getLocation();
					} else if (shaderAttribute.getName() == emulated.getName() + "Low") {
						lowLocation = shaderAttribute.getLocation();
					}

					if ((highLocation != -1) && (lowLocation != -1)) {
						break;
					}
				}

				if ((highLocation == -1) && (lowLocation == -1)) {
					//
					// The shader did not have either attribute. No problem.
					//
					continue;
				} else if ((highLocation == -1) || (lowLocation == -1)) {
					throw new IllegalArgumentException("An emulated double vec3 mesh attribute requires both "
							+ emulated.getName() + "High and " + emulated.getName()
							+ "Low vertex attributes, but the shader only contains one matching attribute.");
				}

				//
				// Copy both high and low parts into a single vertex buffer.
				//
				List<Vector3D> values = ((VertexAttribute<Vector3D>) attribute).getValues();

				FloatBuffer buffer = Buffers.newDirectFloatBuffer(2 * values.size() * 3);
				EmulatedVector3D v;
				Vector3F high;
				Vector3F low;
				for (int i = 0; i < values.size(); ++i) {
					v = new EmulatedVector3D(values.get(i));
					high = v.getHigh();
					buffer.put(high.getX());
					buffer.put(high.getY());
					buffer.put(high.getZ());
					low = v.getLow();
					buffer.put(low.getX());
					buffer.put(low.getY());
					buffer.put(low.getZ());
				}
				buffer.rewind();
				ByteBuffer byteBuffer = Buffers.copyFloatBufferAsByteBuffer(buffer);
				VertexBuffer vertexBuffer = createVertexBuffer(usageHint, byteBuffer.capacity());
				vertexBuffer.copyFromSystemMemory(byteBuffer);

				int stride = 2 * SizeInBytes.getSize(Vector3F.class);
				meshBuffers.getAttributes().set(highLocation,
						new VertexBufferAttribute(vertexBuffer, ComponentDatatype.Float, 3, false, 0, stride));
				meshBuffers.getAttributes().set(lowLocation, new VertexBufferAttribute(vertexBuffer,
						ComponentDatatype.Float, 3, false, SizeInBytes.getSize(Vector3F.class), stride));

				ignoreAttributes.add(emulated.getName() + "High");
				ignoreAttributes.add(emulated.getName() + "Low");
			}
		}

		for (ShaderVertexAttribute shaderAttribute : shaderAttributes) {
			if (ignoreAttributes.contains(shaderAttribute.getName())) {
				continue;
			}

			if (!mesh.getAttributes().contains(shaderAttribute.getName())) {
				throw new IllegalArgumentException("Shader requires vertex attribute \"" + shaderAttribute.getName()
				+ "\", which is not present in mesh.");
			}

			VertexAttribute<?> attribute = mesh.getAttributes().get(shaderAttribute.getName());

			if (attribute.getDatatype() == VertexAttributeType.EmulatedDoubleVector3) {
				List<Vector3D> values = ((VertexAttribute<Vector3D>) attribute).getValues();

				FloatBuffer buffer = Buffers.newDirectFloatBuffer(values.size() * 3);
				Vector3F v;
				for (int i = 0; i < values.size(); ++i) {
					v = values.get(i).toVector3F();
					buffer.put(v.getX());
					buffer.put(v.getY());
					buffer.put(v.getZ());
				}
				ByteBuffer byteBuffer = Buffers.copyFloatBufferAsByteBuffer(buffer);
				VertexBuffer vertexBuffer = createVertexBuffer(usageHint, byteBuffer.capacity());
				vertexBuffer.copyFromSystemMemory(byteBuffer);

				meshBuffers.getAttributes().set(shaderAttribute.getLocation(),
						new VertexBufferAttribute(vertexBuffer, ComponentDatatype.Float, 3));
			} else if (attribute.getDatatype() == VertexAttributeType.Float) {
				VertexBuffer vertexBuffer = createVertexBuffer(((VertexAttribute<Float>) attribute).getValuesBuffer(),
						usageHint);

				meshBuffers.getAttributes().set(shaderAttribute.getLocation(),
						new VertexBufferAttribute(vertexBuffer, ComponentDatatype.Float, 1));
			} else if (attribute.getDatatype() == VertexAttributeType.FloatVector2) {
				VertexBuffer vertexBuffer = createVertexBuffer(
						((VertexAttribute<Vector2F>) attribute).getValuesBuffer(), usageHint);

				meshBuffers.getAttributes().set(shaderAttribute.getLocation(),
						new VertexBufferAttribute(vertexBuffer, ComponentDatatype.Float, 2));
			} else if (attribute.getDatatype() == VertexAttributeType.FloatVector3) {
				VertexBuffer vertexBuffer = createVertexBuffer(
						((VertexAttribute<Vector3F>) attribute).getValuesBuffer(), usageHint);

				meshBuffers.getAttributes().set(shaderAttribute.getLocation(),
						new VertexBufferAttribute(vertexBuffer, ComponentDatatype.Float, 3));
			} else if (attribute.getDatatype() == VertexAttributeType.FloatVector4) {
				VertexBuffer vertexBuffer = createVertexBuffer(
						((VertexAttribute<Vector4F>) attribute).getValuesBuffer(), usageHint);

				meshBuffers.getAttributes().set(shaderAttribute.getLocation(),
						new VertexBufferAttribute(vertexBuffer, ComponentDatatype.Float, 4));
			} else if (attribute.getDatatype() == VertexAttributeType.UnsignedByte) {
				if (attribute instanceof VertexAttributeRGBA) {
					VertexBuffer vertexBuffer = createVertexBuffer(
							((VertexAttribute<Byte>) attribute).getValuesBuffer(), usageHint);

					meshBuffers.getAttributes().set(shaderAttribute.getLocation(),
							new VertexBufferAttribute(vertexBuffer, ComponentDatatype.Byte, 4, true, 0, 0));
				}

				else if (attribute instanceof VertexAttributeRGB) {
					VertexBuffer vertexBuffer = createVertexBuffer(
							((VertexAttribute<Byte>) attribute).getValuesBuffer(), usageHint);

					meshBuffers.getAttributes().set(shaderAttribute.getLocation(),
							new VertexBufferAttribute(vertexBuffer, ComponentDatatype.Byte, 3, true, 0, 0));
				} else {
					VertexBuffer vertexBuffer = createVertexBuffer(
							((VertexAttribute<Byte>) attribute).getValuesBuffer(), usageHint);

					meshBuffers.getAttributes().set(shaderAttribute.getLocation(),
							new VertexBufferAttribute(vertexBuffer, ComponentDatatype.Byte, 1));
				}
			} else {
				LOGGER.error("attribute.Datatype");
			}
		}

		return meshBuffers;
	}

	private static VertexBuffer createVertexBuffer(ByteBuffer values, BufferHint usageHint) {
		VertexBuffer vertexBuffer = createVertexBuffer(usageHint, values.capacity());
		vertexBuffer.copyFromSystemMemory(values);
		return vertexBuffer;
	}

	public static UniformBuffer createUniformBuffer(BufferHint usageHint, int sizeInBytes) {
		return new UniformBufferGL2x3x(usageHint, sizeInBytes);
	}

	public static Texture createTexture(BufferedImage bitmap, boolean generateMipmaps) {
		GLProfile glprofile = GLProfile.getGL2GL3();
		Texture result = AWTTextureIO.newTexture(glprofile, bitmap, generateMipmaps);
		return result;
	}

	public static Texture createTexture(int width, int height, int type, boolean generateMipmaps) {
		GLProfile glprofile = GLProfile.getGL2GL3();
		Texture result = AWTTextureIO.newTexture(glprofile, new BufferedImage(width, height, type), generateMipmaps);
		return result;
	}

	public static Texture createTextureRectangle(BufferedImage bitmap) {
		Texture result = TextureIO.newTexture(GL2.GL_TEXTURE_RECTANGLE_ARB);
		GLProfile glprofile = GLProfile.getGL2GL3();
		TextureData data = AWTTextureIO.newTextureData(glprofile, bitmap, false);
		GL3 gl = GLU.getCurrentGL().getGL3();
		result.updateImage(gl, data, GL2.GL_TEXTURE_RECTANGLE_ARB);
		return result;
	}

	public static TextureFloat createTextureFloatRectangle(FloatBuffer buffer, int width, int height) {
		TextureFloatData data = new TextureFloatData(buffer, width, height);

		TextureFloat result = new TextureFloat(GL.GL_TEXTURE_2D);
		GL3 gl = GLU.getCurrentGL().getGL3();
		result.updateImage(gl, data);
		return result;
	}

	public static TextureSampler createTexture2DSampler(TextureMinificationFilter minificationFilter,
			TextureMagnificationFilter magnificationFilter, TextureWrap wrapS, TextureWrap wrapT) {
		return new TextureSamplerGL2xGL3(minificationFilter, magnificationFilter, wrapS, wrapT, 1);
	}

	public static TextureSampler createTexture2DSampler(TextureMinificationFilter minificationFilter,
			TextureMagnificationFilter magnificationFilter, TextureWrap wrapS, TextureWrap wrapT,
			float maximumAnistropy) {
		return new TextureSamplerGL2xGL3(minificationFilter, magnificationFilter, wrapS, wrapT, maximumAnistropy);
	}

	public static TextureSamplers getTextureSamplers() {
		return s_textureSamplers;
	}

	public static void finish() {
		final GL3 gl = GLU.getCurrentGL().getGL3();

		gl.glFinish();
	}

	public static void flush() {
		final GL3 gl = GLU.getCurrentGL().getGL3();

		gl.glFlush();
	}

	public static Extensions getExtensions() {
		return s_extensions;
	}

	/**
	 * The collection is not thread safe.
	 */
	public static LinkAutomaticUniformCollection getLinkAutomaticUniforms() {
		return s_linkAutomaticUniforms;
	}

	/**
	 * The collection is not thread safe.
	 */
	public static DrawAutomaticUniformFactoryCollection getDrawAutomaticUniformFactories() {
		return s_drawAutomaticUniformFactories;
	}

	@Override
	public int getMaximumNumberOfVertexAttributes() {
		return s_maximumNumberOfVertexAttributes;
	}

	public static int getNumberOfTextureUnits() {
		return s_numberOfTextureUnits;
	}

	public static int getMaximumNumberOfColorAttachments() {
		return s_maximumNumberOfColorAttachments;
	}

	private static int s_maximumNumberOfVertexAttributes;
	private static int s_numberOfTextureUnits;
	private static int s_maximumNumberOfColorAttachments;

	private static Extensions s_extensions;
	private static LinkAutomaticUniformCollection s_linkAutomaticUniforms;
	private static DrawAutomaticUniformFactoryCollection s_drawAutomaticUniformFactories;

	private static TextureSamplers s_textureSamplers;

}
