package fr.ifremer.globe.ogl.renderer.scene;

import fr.ifremer.globe.ogl.util.ViewProjectionType;

public class DefaultCameraCreator {
	/**
	 * create camera depending on projection type
	 * */
	public synchronized static Camera create(ViewProjectionType projection)
	{
		if(projection==ViewProjectionType.PERSPECTIVE)
		{
			return  new PerspectiveCamera();
		} else
		{
			return new OrthographicCamera();
		}
	}

}
