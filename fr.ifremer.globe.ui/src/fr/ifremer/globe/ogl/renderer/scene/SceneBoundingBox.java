package fr.ifremer.globe.ogl.renderer.scene;
/**
 * this class retain the scene bounds.
 * This is used to compute the orthographic matrix
 * x and y are the dtm axis
 * z is the altitude axis
 * */
public class SceneBoundingBox {

	double xmin;
	double xmax;
	double ymin;
	double ymax; 
	double zmin;
	double zmax;
	
}
