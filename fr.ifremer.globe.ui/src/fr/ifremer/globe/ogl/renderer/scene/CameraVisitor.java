/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ogl.renderer.scene;

/**
 * Perform a specific process depending on Camera type.
 */
public interface CameraVisitor {

	/** Perform a process on a {@link OrthographicCamera}. */
	void accept(OrthographicCamera camera);
	/** Perform a process on a {@link PerspectiveCamera}. */
	void accept(PerspectiveCamera camera);
}
