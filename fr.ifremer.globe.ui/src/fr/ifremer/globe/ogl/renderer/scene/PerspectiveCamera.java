package fr.ifremer.globe.ogl.renderer.scene;

import fr.ifremer.globe.ogl.core.matrices.Matrix4D;

public class PerspectiveCamera  extends Camera {
	private double m_FieldOfViewY = 0;
	private double m_AspectRatio = 0;

	private double m_PerspectiveNearPlaneDistance = 0;
	private double m_PerspectiveFarPlaneDistance = 0;
	PerspectiveCamera()
	{
		m_FieldOfViewY = Math.PI / 6.0;
		m_AspectRatio = 1;
		m_PerspectiveNearPlaneDistance = 0.01;
		m_PerspectiveFarPlaneDistance = 64;

	}
	public double getFieldOfViewX() {
		return (2.0 * Math.atan(m_AspectRatio * Math.tan(m_FieldOfViewY * 0.5)));
	}

	public double getFieldOfViewY() {
		return m_FieldOfViewY;
	}

	public void setFieldOfViewY(double fieldOfViewY) {
		m_FieldOfViewY = fieldOfViewY;
	}

	public double getAspectRatio() {
		return m_AspectRatio;
	}

	public void setAspectRatio(double aspectRatio) {
		m_AspectRatio = aspectRatio;
	}

	public double getPerspectiveNearPlaneDistance() {
		return m_PerspectiveNearPlaneDistance;
	}

	public void setPerspectiveNearPlaneDistance(double perspectiveNearPlaneDistance) {
		m_PerspectiveNearPlaneDistance = perspectiveNearPlaneDistance;
	}

	public double getPerspectiveFarPlaneDistance() {
		return m_PerspectiveFarPlaneDistance;
	}

	public void setPerspectiveFarPlaneDistance(double perspectiveFarPlaneDistance) {
		m_PerspectiveFarPlaneDistance = perspectiveFarPlaneDistance;
	}
	@Override
	public	void reshape(int width, int height) {
		setAspectRatio(width/(double) height);		
	}
	@Override
	public Matrix4D getProjectionMatrix() {
		return Matrix4D.createPerspectiveFieldOfView(getFieldOfViewY(), getAspectRatio(), getPerspectiveNearPlaneDistance(), getPerspectiveFarPlaneDistance());
	}

	/** Follow the link.
	 * @see fr.ifremer.globe.ogl.renderer.scene.Camera#visit(fr.ifremer.globe.ogl.renderer.scene.CameraVisitor)
	 */
	@Override
	public void visit(CameraVisitor visitor) {
		visitor.accept(this);		
	}

}
