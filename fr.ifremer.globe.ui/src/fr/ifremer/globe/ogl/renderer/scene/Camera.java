package fr.ifremer.globe.ogl.renderer.scene;

import fr.ifremer.globe.ogl.core.geometry.Ellipsoid;
import fr.ifremer.globe.ogl.core.matrices.Matrix4D;
import fr.ifremer.globe.ogl.core.vectors.Vector3D;
import fr.ifremer.globe.ogl.core.vectors.Vector3F;

public abstract class Camera {

	public Camera() {
		m_Eye = Vector3D.getUnitY().negate();
		m_Target = Vector3D.getZero();
		m_Up = Vector3D.getUnitZ();
	}

	/**
	 * Return the distance between the camera position (eye) and the camera target point projected on z=0 plane.
	 */
	public Vector3D getTargetOnXYPlane() {
		Vector3D toTarget = (m_Target.subtract(m_Eye)).normalize();
		double distance = -m_Eye.getZ() / toTarget.getZ();
		Vector3D targetOnXYPlane = m_Eye.add(toTarget.multiply(distance));
		return targetOnXYPlane;
	}

	/**
	 * Return the distance between the camera position (eye) and the camera target point projected on z=0 plane.
	 */
	public double getDistanceFromTargetOnXYPlane() {
		Vector3D toTarget = (m_Target.subtract(m_Eye)).normalize();
		double distance = -m_Eye.getZ() / toTarget.getZ();
		return distance;
	}

	public Vector3D getEye() {
		return m_Eye;
	}

	public void setEye(Vector3D eye) {
		m_Eye = eye;
	}

	public Vector3D getTarget() {
		return m_Target;
	}

	public void setTarget(Vector3D target) {
		m_Target = target;
	}

	public Vector3D getUp() {
		return m_Up;
	}

	public void setUp(Vector3D up) {
		m_Up = up;
	}

	private Vector3D m_Eye = null;
	private Vector3D m_Target = null;
	private Vector3D m_Up = null;

	public Vector3F getEyeHigh() {
		return m_Eye.toVector3F();
	}

	public Vector3F getEyeLow() {
		return (m_Eye.subtract(getEyeHigh().toVector3D())).toVector3F();
	}

	public Vector3D getForward() {
		return (m_Target.subtract(m_Eye)).normalize();
	}

	public Vector3D getRight() {
		return getForward().cross(m_Up).normalize();
	}

	/**
	 * compute and return the projection matrix associated with the given camera
	 */
	public abstract Matrix4D getProjectionMatrix();

	public double getHeight(Ellipsoid shape) {
		return shape.toGeodetic3D(m_Eye).getHeight();
	}

	/**
	 * recompute a camera on a reshaping event
	 */
	public abstract void reshape(int width, int height);

	public abstract void visit(CameraVisitor visitor);

}