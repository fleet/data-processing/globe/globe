package fr.ifremer.globe.ogl.renderer.scene;

import java.awt.Rectangle;

import fr.ifremer.globe.ogl.core.matrices.Matrix42;
import fr.ifremer.globe.ogl.core.matrices.Matrix4D;
import fr.ifremer.globe.ogl.core.vectors.Vector3D;

public class SceneState {

	public SceneState( Camera camera) {
		m_DiffuseIntensity = 0.65f;
		m_SpecularIntensity = 0.25f;
		m_AmbientIntensity = 0.10f;
		m_Shininess = 12;
		m_Camera=camera;

		m_SunPosition = new Vector3D(200000, 0, 0);
		m_ModelMatrix = Matrix4D.Identity;
		m_HighResolutionSnapScale = 1;
	}

	public float getDiffuseIntensity() {
		return m_DiffuseIntensity;
	}

	public void setDiffuseIntensity(float diffuseIntensity) {
		m_DiffuseIntensity = diffuseIntensity;
	}

	public float getSpecularIntensity() {
		return m_SpecularIntensity;
	}

	public void setSpecularIntensity(float specularIntensity) {
		m_SpecularIntensity = specularIntensity;
	}

	public float getAmbientIntensity() {
		return m_AmbientIntensity;
	}

	public void setAmbientIntensity(float ambientIntensity) {
		m_AmbientIntensity = ambientIntensity;
	}

	public float getShininess() {
		return m_Shininess;
	}

	public void setShininess(float shininess) {
		m_Shininess = shininess;
	}

	public Camera getCamera() {
		return m_Camera;
	}

	public void setCamera(Camera camera) {
		m_Camera = camera;
	}

	private float m_DiffuseIntensity = 0.0f;
	private float m_SpecularIntensity = 0.0f;
	private float m_AmbientIntensity = 0.0f;
	private float m_Shininess = 0.0f;
	private Camera m_Camera = null;

	public Vector3D getSunPosition() {
		return m_SunPosition;
	}

	public void setSunPosition(Vector3D sunPosition) {
		m_SunPosition = sunPosition;
	}

	private Vector3D m_SunPosition = null;

	public Vector3D getCameraLightPosition() {
		return m_Camera.getEye();
	}

	public Matrix4D computeViewportTransformationMatrix(Rectangle viewport, double nearDepthRange, double farDepthRange) {
		double halfWidth = viewport.getWidth() * 0.5;
		double halfHeight = viewport.getHeight() * 0.5;
		double halfDepth = (farDepthRange - nearDepthRange) * 0.5;

		//
		// Bottom and top swapped: MS -> OpenGL
		//
		return new Matrix4D(halfWidth, 0.0, 0.0, viewport.getMinX() + halfWidth, 0.0, halfHeight, 0.0, viewport.getMinY() + halfHeight, 0.0, 0.0, halfDepth, nearDepthRange + halfDepth, 0.0, 0.0, 0.0,
				1.0);
	}

	public static Matrix4D computeViewportOrthographicMatrix(Rectangle viewport) {
		//
		// Bottom and top swapped: MS -> OpenGL
		//
		return Matrix4D.createOrthographicOffCenter(viewport.getMinX(), viewport.getMaxX(), viewport.getMinY(), viewport.getMaxY(), 0.0, 1.0);
	}

	//	public Matrix4D getOrthographicMatrix() {
	//		//
	//		// Bottom and top swapped: MS -> OpenGL
	//		//
	//		return Matrix4D.createOrthographicOffCenter(m_Camera.getOrthographicLeft(), m_Camera.getOrthographicRight(), m_Camera.getOrthographicTop(), m_Camera.getOrthographicBottom(),
	//				m_Camera.getOrthographicNearPlaneDistance(), m_Camera.getOrthographicFarPlaneDistance());
	//	}

	public Matrix4D getProjectionMatrix() {
		return m_Camera.getProjectionMatrix();
	}

	public Matrix4D getViewMatrix() {
		return Matrix4D.lookAt(m_Camera.getEye(), m_Camera.getTarget(), m_Camera.getUp());
	}

	public Matrix4D getModelMatrix() {
		return m_ModelMatrix;
	}

	public void setModelMatrix(Matrix4D modelMatrix) {
		m_ModelMatrix = modelMatrix;
	}

	private Matrix4D m_ModelMatrix = null;

	public Matrix4D getModelViewMatrix() {
		return getViewMatrix().multiply(m_ModelMatrix);
	}

	public Matrix4D getModelViewMatrixRelativeToEye() {
		Matrix4D m = getModelViewMatrix();
		return new Matrix4D(m.getColumn0Row0(), m.getColumn1Row0(), m.getColumn2Row0(), 0.0, m.getColumn0Row1(), m.getColumn1Row1(), m.getColumn2Row1(), 0.0, m.getColumn0Row2(), m.getColumn1Row2(),
				m.getColumn2Row2(), 0.0, m.getColumn0Row3(), m.getColumn1Row3(), m.getColumn2Row3(), m.getColumn3Row3());
	}

	public Matrix4D getModelViewPerspectiveMatrixRelativeToEye() {
		return getProjectionMatrix().multiply(getModelViewMatrixRelativeToEye());
	}

	public Matrix4D getModelViewPerspectiveMatrix() {
		return getProjectionMatrix().multiply(getModelViewMatrix());
	}

	//	public Matrix4D getModelViewOrthographicMatrix() {
	//		return getOrthographicMatrix().multiply(getModelViewMatrix());
	////		return getModelViewMatrix().multiply(getOrthographicMatrix());
	//	}

	public Matrix42<Double> getModelZToClipCoordinates() {
		//
		// Bottom two rows of model-view-projection matrix
		Matrix4D m;

		m = getModelViewPerspectiveMatrix();
		return new Matrix42<Double>(Double.class, m.getColumn0Row2(), m.getColumn1Row2(), m.getColumn2Row2(), m.getColumn3Row2(), m.getColumn0Row3(), m.getColumn1Row3(), m.getColumn2Row3(),
				m.getColumn3Row3());
	}

	public double getHighResolutionSnapScale() {
		return m_HighResolutionSnapScale;
	}

	public void setHighResolutionSnapScale(double highResolutionSnapScale) {
		m_HighResolutionSnapScale = highResolutionSnapScale;
	}

	private double m_HighResolutionSnapScale = 0.0;
}