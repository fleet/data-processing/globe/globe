package fr.ifremer.globe.ogl.renderer.scene;

import fr.ifremer.globe.ogl.core.matrices.Matrix4D;

/**
 * contains the settings of the orthographic camera
 * */
public class OrthographicCamera extends Camera {
	private double m_OrthographicNearPlaneDistance = 0;
	private double m_OrthographicFarPlaneDistance = 0;
	private double m_OrthographicLeft = 0;
	private double m_OrthographicRight = 0;
	private double m_OrthographicBottom = 0;
	private double m_OrthographicTop = 0;
	private double m_AspectRatio=1;

	/**
	 * indicate if this camera should keep aspect ratio on resize
	 * */
	private boolean keepAspectRatio=false;

	public OrthographicCamera() {
		super();

		m_OrthographicNearPlaneDistance = 0;
		m_OrthographicFarPlaneDistance = 1;

		m_OrthographicLeft = 0;
		m_OrthographicRight = 1;

		m_OrthographicBottom = 0;
		m_OrthographicTop = 1;
	}


	@Override
	public
	void reshape(int width, int height) {
		if(keepAspectRatio)
			m_AspectRatio=(double)height/width;
	}

	public double getOrthographicNearPlaneDistance() {
		return m_OrthographicNearPlaneDistance;
	}

	public void setOrthographicNearPlaneDistance(double orthographicNearPlaneDistance) {
		m_OrthographicNearPlaneDistance = orthographicNearPlaneDistance;
	}

	public double getOrthographicFarPlaneDistance() {
		return m_OrthographicFarPlaneDistance;
	}

	public void setOrthographicFarPlaneDistance(double orthographicFarPlaneDistance) {
		m_OrthographicFarPlaneDistance = orthographicFarPlaneDistance;
	}

	public double getOrthographicLeft() {
		return m_OrthographicLeft;
	}

	public void setOrthographics(double orthographicLeft,double orthographicRight,double orthographicBottom,double orthographicTop)
	{
		m_OrthographicLeft = orthographicLeft;
		m_OrthographicRight = orthographicRight;
		m_OrthographicBottom = orthographicBottom;
		m_OrthographicTop = orthographicTop;
	}

	public double getOrthographicRight() {
		return m_OrthographicRight;
	}

	public double getOrthographicBottom() {
		return m_OrthographicBottom;
	}

	public double getOrthographicTop() {
		return m_OrthographicTop;
	}

	public double getAspectRatio() {
		return m_AspectRatio;
	}

	public void setAspectRatio(double aspectRatio) {
		m_AspectRatio = aspectRatio;
	}

	@Override
	public Matrix4D getProjectionMatrix() {
		return Matrix4D.createOrthographicOffCenter(m_OrthographicLeft , m_OrthographicRight, 
				m_OrthographicBottom*m_AspectRatio, m_OrthographicTop*m_AspectRatio,
				m_OrthographicNearPlaneDistance, m_OrthographicFarPlaneDistance);
	}

	/** Follow the link.
	 * @see fr.ifremer.globe.ogl.renderer.scene.Camera#visit(fr.ifremer.globe.ogl.renderer.scene.CameraVisitor)
	 */
	@Override
	public void visit(CameraVisitor visitor) {
		visitor.accept(this);		
	}


	/**
	 * indicate if this camera should keep aspect ratio on resize
	 * @return the keepAspectRatio
	 */
	public boolean isKeepAspectRatio() {
		return keepAspectRatio;
	}


	/**
	 *	indicate if this camera should keep aspect ratio on resize
	 * @param keepAspectRatio the keepAspectRatio to set
	 */
	public void setKeepAspectRatio(boolean keepAspectRatio) {
		this.keepAspectRatio = keepAspectRatio;
	}

}
