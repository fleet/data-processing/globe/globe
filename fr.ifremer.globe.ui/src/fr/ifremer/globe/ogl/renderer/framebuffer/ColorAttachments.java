package fr.ifremer.globe.ogl.renderer.framebuffer;

import com.jogamp.opengl.util.texture.Texture;

/**
 * <p>
 * Description : ColorAttachments.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 17 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class ColorAttachments implements Iterable<Texture> {

	public abstract Texture get(int index);

	public abstract void setTexture2D(int index, Texture texture);

	public abstract int size();
}