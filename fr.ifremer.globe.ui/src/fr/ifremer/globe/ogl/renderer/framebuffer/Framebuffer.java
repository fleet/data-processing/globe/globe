package fr.ifremer.globe.ogl.renderer.framebuffer;

import com.jogamp.opengl.util.texture.Texture;

/**
 * <p>
 * Description : Framebuffer.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 17 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class Framebuffer {

	public abstract ColorAttachments getColorAttachments();

	public abstract Texture getDepthAttachment();

	public abstract void setDepthAttachment(Texture value);

	protected abstract void dispose(boolean disposing);

	public void dispose() {
		dispose(true);

	}

	// NOT SUPPORTED
	// public abstract Texture2D getDepthStencilAttachment();

	// NOT SUPPORTED
	// public abstract void setDepthStencilAttachment(Texture2D value);
}
