package fr.ifremer.globe.ogl.renderer.infrastructure;

import java.net.URL;

import fr.ifremer.globe.ogl.util.StringIO;
import fr.ifremer.globe.utils.exception.runtime.RessourceNotFoundException;

public class EmbeddedResources {

	public static String getText(ClassLoader loader,String resourceName) {
		URL url = loader.getResource(resourceName);
		return get(url);
	}

	public static String getText(Class<?> clazz, String resourceName) {
		URL url = clazz.getClassLoader().getResource(resourceName);
		return get(url);
	}

	private static String get(URL url)
	{
		try {
			return StringIO.readFully(url);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RessourceNotFoundException("Impossible de lire le fichier contenant le vertex shader", e);
		}
	}
	public static String getText(String resourceName) {

		String result = "";

		try {
			result = StringIO.readFully(resourceName);
		}catch (Exception e) {
			e.printStackTrace();
			throw new RessourceNotFoundException("Impossible de lire le fichier contenant le vertex shader", e);
		}

		return result;
	}
}