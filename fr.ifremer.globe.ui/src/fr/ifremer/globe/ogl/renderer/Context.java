package fr.ifremer.globe.ogl.renderer;

import java.awt.Rectangle;

import fr.ifremer.globe.ogl.core.geometry.Mesh;
import fr.ifremer.globe.ogl.core.geometry.PrimitiveType;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.clearstate.ClearState;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.framebuffer.Framebuffer;
import fr.ifremer.globe.ogl.renderer.mesh.MeshBuffers;
import fr.ifremer.globe.ogl.renderer.renderstate.RenderState;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderVertexAttributeCollection;
import fr.ifremer.globe.ogl.renderer.textures.TextureUnits;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexArray;

/**
 * <p>
 * Description : Context.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class Context {

	public abstract void clear(ClearState clearState);

	public abstract Framebuffer createFramebuffer();

	public abstract VertexArray createVertexArray();

	public abstract VertexArray  createVertexArray(Mesh mesh, ShaderVertexAttributeCollection shaderAttributes, BufferHint usageHint);

	public VertexArray createVertexArray(MeshBuffers meshBuffers) {
		VertexArray va = createVertexArray();

		va.setDisposeBuffers(true);
		va.setIndexBuffer(meshBuffers.getIndexBuffer());
		for (int i = 0; i < meshBuffers.getAttributes().getMaximumCount(); ++i) {
			va.getAttributes().set(i, meshBuffers.getAttributes().get(i));
		}

		return va;
	}

	public abstract void draw(PrimitiveType primitiveType, DrawState drawState, SceneState sceneState);

	public abstract void draw(PrimitiveType primitiveType, int offset, int count, DrawState drawState, SceneState sceneState);

	public abstract Framebuffer getFramebuffer();

	public abstract TextureUnits getTextureUnits();

	public abstract Rectangle getViewport();

	public abstract boolean makeCurrent();

	public abstract void release();

	public abstract void setFramebuffer(Framebuffer framebuffer);

	public abstract void setViewport(Rectangle rectangle);

	public abstract void forceApplyRenderState(RenderState renderState);

}
