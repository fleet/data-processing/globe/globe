package fr.ifremer.globe.ogl.renderer.textures;

import fr.ifremer.globe.ogl.renderer.Device2x;

/**
 * <p>
 * Description : TextureSamplers.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class TextureSamplers {

	/**
	 * Logger.
	 */
	// private static final Logger LOGGER =
	// Logger.getLogger(TextureSamplers.class);

	public TextureSamplers() {
		_nearestClamp = Device2x.createTexture2DSampler(TextureMinificationFilter.Nearest, TextureMagnificationFilter.Nearest, TextureWrap.Clamp, TextureWrap.Clamp);

		_linearClamp = Device2x.createTexture2DSampler(TextureMinificationFilter.Linear, TextureMagnificationFilter.Linear, TextureWrap.Clamp, TextureWrap.Clamp);

		_nearestRepeat = Device2x.createTexture2DSampler(TextureMinificationFilter.Nearest, TextureMagnificationFilter.Nearest, TextureWrap.Repeat, TextureWrap.Repeat);

		_linearRepeat = Device2x.createTexture2DSampler(TextureMinificationFilter.Linear, TextureMagnificationFilter.Linear, TextureWrap.Repeat, TextureWrap.Repeat);
	}

	public TextureSampler getNearestClamp() {
		return _nearestClamp;
	}

	public TextureSampler getLinearClamp() {
		return _linearClamp;
	}

	public TextureSampler getNearestRepeat() {
		return _nearestRepeat;
	}

	public TextureSampler getLinearRepeat() {
		return _linearRepeat;
	}

	private TextureSampler _nearestClamp;
	private TextureSampler _linearClamp;
	private TextureSampler _nearestRepeat;
	private TextureSampler _linearRepeat;
}
