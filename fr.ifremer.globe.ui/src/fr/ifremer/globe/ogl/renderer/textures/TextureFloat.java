package fr.ifremer.globe.ogl.renderer.textures;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLES2;
import com.jogamp.opengl.GLException;

import com.jogamp.opengl.util.texture.Texture;

/***
 * Represents an opengl texture float object
 * inspired by {@link Texture object }. Contains convenience routines
 * for enabling/disabling OpenGL texture state, binding this texture,
 * and computing texture coordinates for both the entire image as well
 * as a sub-image.
 * */
public class TextureFloat {
	/** The GL target type. */
	private int target;
	/** The GL texture ID. */
	private int texID;

	// Constructor for use when creating e.g. cube maps, where there is
	// no initial texture data
	public TextureFloat(int target ) {
		texID = 0;
		this.target = target;
	}
	/**
	 * Enables this texture's target (e.g., GL_TEXTURE_2D) in the
	 * given GL context's state. This method is a shorthand equivalent
	 * of the following OpenGL code:
	 * <pre>
	 *   gl.glEnable(texture.getTarget());
	 * </pre>
	 * <p>
	 * Call is ignored if the {@link GL} object's context
	 * is using a core profile, see {@link GL#isGLcore()},
	 * or if {@link #getTarget()} is {@link GLES2#GL_TEXTURE_EXTERNAL_OES}.
	 * </p>
	 * <p>
	 * See the <a href="#perftips">performance tips</a> above for hints
	 * on how to maximize performance when using many Texture objects.
	 * </p>
	 * @param gl the current GL object
	 *
	 * @throws GLException if no OpenGL context was current or if any
	 * OpenGL-related errors occurred
	 */
	public void enable(GL gl) throws GLException {
		if( !gl.isGLcore() && GLES2.GL_TEXTURE_EXTERNAL_OES != target) {
			gl.glEnable(target);
		}
	}
	/**
	 * Disables this texture's target (e.g., GL_TEXTURE_2D) in the
	 * given GL state. This method is a shorthand equivalent
	 * of the following OpenGL code:
	 * <pre>
	 *   gl.glDisable(texture.getTarget());
	 * </pre>
	 * <p>
	 * Call is ignored if the {@link GL} object's context
	 * is using a core profile, see {@link GL#isGLcore()},
	 * or if {@link #getTarget()} is {@link GLES2#GL_TEXTURE_EXTERNAL_OES}.
	 * </p>
	 * <p>
	 * See the <a href="#perftips">performance tips</a> above for hints
	 * on how to maximize performance when using many Texture objects.
	 * </p>
	 * @param gl the current GL object
	 *
	 * @throws GLException if no OpenGL context was current or if any
	 * OpenGL-related errors occurred
	 */
	public void disable(GL gl) throws GLException {
		if( !gl.isGLcore() && GLES2.GL_TEXTURE_EXTERNAL_OES != target ) {
			gl.glDisable(target);
		}
	}
	/**
	 * Binds this texture to the given GL context. This method is a
	 * shorthand equivalent of the following OpenGL code:
	     <pre>
	     gl.glBindTexture(texture.getTarget(), texture.getTextureObject());
	     </pre>
	 *
	 * See the <a href="#perftips">performance tips</a> above for hints
	 * on how to maximize performance when using many Texture objects.
	 *
	 * @param gl the current GL context
	 * @throws GLException if no OpenGL context was current or if any
	 * OpenGL-related errors occurred
	 */
	public void bind(GL gl) throws GLException {
		validateTexID(gl, true);
		gl.glBindTexture(target, texID);
	}
	/**,
	 * Destroys the native resources used by this texture object.
	 *
	 * @throws GLException if any OpenGL-related errors occurred
	 */
	public void destroy(GL gl) throws GLException {
		if(0!=texID) {
			gl.glDeleteTextures(1, new int[] {texID}, 0);
			texID = 0;
		}
	}
	/**
	 * Returns the OpenGL "target" of this texture.
	 *
	 * @return the OpenGL target of this texture
	 * @see com.jogamp.opengl.GL#GL_TEXTURE_2D
	 * @see com.jogamp.opengl.GL2#GL_TEXTURE_RECTANGLE_ARB
	 */
	public int getTarget() {
		return target;
	}
	/**
	 * Returns the underlying OpenGL texture object for this texture
	 * and generates it if not done yet.
	 * <p>
	 * Most applications will not need to access this, since it is
	 * handled automatically by the bind(GL) and destroy(GL) APIs.
	 * </p>
	 * @param gl required to be valid and current in case the texture object has not been generated yet,
	 *           otherwise it may be <code>null</code>.
	 * @see #getTextureObject()
	 */
	public int getTextureObject(GL gl) {
		validateTexID(gl, false);
		return texID;
	}
	/**
	 * Returns the underlying OpenGL texture object for this texture,
	 * maybe <code>0</code> if not yet generated.
	 * <p>
	 * Most applications will not need to access this, since it is
	 * handled automatically by the bind(GL) and destroy(GL) APIs.
	 * </p>
	 * @see #getTextureObject(GL)
	 */
	public int getTextureObject() {
		return texID;
	}
	private boolean validateTexID(GL gl, boolean throwException) {
		if( 0 == texID ) {
			if( null != gl ) {
				int[] tmp = new int[1];
				gl.glGenTextures(1, tmp, 0);
				texID = tmp[0];
				if ( 0 == texID && throwException ) {
					throw new GLException("Create texture ID invalid: texID "+texID+", glerr 0x"+Integer.toHexString(gl.glGetError()));
				}
			} else if ( throwException ) {
				throw new GLException("No GL context given, can't create texture ID");
			}
		}
		return 0 != texID;
	}
	public void updateImage(GL2GL3 gl, TextureFloatData data) throws GLException {
		updateImage(gl,  data, GL.GL_TEXTURE_2D, GL.GL_NEAREST, GL.GL_NEAREST, GL.GL_RGB,  GL2ES2.GL_RED, GL.GL_FLOAT);

	}

	public void updateImage(GL2GL3 gl, TextureFloatData data, int target, int minFilter, int magFilter, int internalFormat, int format, int type) throws GLException {
		validateTexID(gl, true);
		gl.glBindTexture(target, texID);
		gl.glTexParameteri(target, GL.GL_TEXTURE_MIN_FILTER, minFilter);
		gl.glTexParameteri(target, GL.GL_TEXTURE_MAG_FILTER, magFilter);
		gl.glTexImage2D(target, 0, internalFormat, data.getWidth(), data.getHeight(), 0, format, type, data.getBuffer());
		if (minFilter == GL.GL_LINEAR_MIPMAP_LINEAR) {
			gl.glGenerateMipmap(target);
		}
	}

}
