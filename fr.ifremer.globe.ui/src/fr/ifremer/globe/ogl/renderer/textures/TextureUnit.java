package fr.ifremer.globe.ogl.renderer.textures;

import com.jogamp.opengl.util.texture.Texture;

/**
 * <p>
 * Description : TextureUnit.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 17 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class TextureUnit {

	public abstract Texture getTexture();

	public abstract void setTexture(Texture value);

	public abstract TextureSampler getTextureSampler();

	public abstract void setTextureSampler(TextureSampler value);
}