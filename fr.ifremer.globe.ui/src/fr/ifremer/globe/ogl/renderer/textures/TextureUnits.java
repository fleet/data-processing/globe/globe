package fr.ifremer.globe.ogl.renderer.textures;

/**
 * <p>
 * Description : TextureUnits.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 17 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class TextureUnits implements Iterable<TextureUnit> {

	public abstract TextureUnit get(int index);

	public abstract int size();

	public abstract void clean();
}