package fr.ifremer.globe.ogl.renderer.textures;

import java.nio.FloatBuffer;

public class TextureFloatData {
	protected   FloatBuffer buffer;
    protected int width;
    protected int height;
   
    
    public TextureFloatData(FloatBuffer buffer, int width, int height) {
		super();
		this.buffer = buffer;
		this.width = width;
		this.height = height;
	}

	/** Returns the width in pixels of the texture data. */
    public int getWidth() { return width; }
    /** Returns the height in pixels of the texture data. */
    public int getHeight() { return height; }
    
    /** Returns the texture data, or null if it is specified as a set of mipmaps. */
    public FloatBuffer getBuffer() {
        return buffer;
    }  
}
