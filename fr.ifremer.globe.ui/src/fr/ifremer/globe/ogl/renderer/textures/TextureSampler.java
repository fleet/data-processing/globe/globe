package fr.ifremer.globe.ogl.renderer.textures;

/**
 * <p>
 * Description : TextureSampler.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class TextureSampler {

	/**
	 * Logger.
	 */
	// private static final Logger LOGGER =
	// Logger.getLogger(TextureSampler.class);

	protected TextureSampler(TextureMinificationFilter minificationFilter, TextureMagnificationFilter magnificationFilter, TextureWrap wrapS, TextureWrap wrapT, float maximumAnistropy) {
		_minificationFilter = minificationFilter;
		_magnificationFilter = magnificationFilter;
		_wrapS = wrapS;
		_wrapT = wrapT;
		_maximumAnistropy = maximumAnistropy;
	}

	public TextureMinificationFilter getMinificationFilter() {
		return _minificationFilter;
	}

	public TextureMagnificationFilter getMagnificationFilter() {
		return _magnificationFilter;
	}

	public TextureWrap getWrapS() {
		return _wrapS;
	}

	public TextureWrap getWrapT() {
		return _wrapT;
	}

	public float getMaximumAnisotropic() {
		return _maximumAnistropy;
	}

	private TextureMinificationFilter _minificationFilter;
	private TextureMagnificationFilter _magnificationFilter;
	private TextureWrap _wrapS;
	private TextureWrap _wrapT;
	private float _maximumAnistropy;
}
