package fr.ifremer.globe.ogl.renderer.jogl.textures;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.Device2x;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;
import fr.ifremer.globe.ogl.renderer.textures.TextureSampler;

public class TextureUnitGL2x extends TextureUnitGLBase {


	public TextureUnitGL2x(int index, ICleanableObserver observer) {
		super(index, observer);
	}

	@Override
	protected GL2GL3 getGL() {
		return  GLU.getCurrentGL().getGL2();
	}

	@Override
	protected TextureSampler getDefaultSampler() {
		return Device2x.getTextureSamplers().getLinearClamp();
	}

}