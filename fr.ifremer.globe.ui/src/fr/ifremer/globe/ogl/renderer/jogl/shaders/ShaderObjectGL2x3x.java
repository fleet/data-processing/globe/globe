package fr.ifremer.globe.ogl.renderer.jogl.shaders;

import java.nio.IntBuffer;

import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.CouldNotCreateVideoCardResourceException;

import com.jogamp.common.nio.Buffers;

public abstract class ShaderObjectGL2x3x {
	abstract String getBuiltInConstants();
	abstract String getBuiltInFunction();

	public ShaderObjectGL2x3x(int shaderType, String source) throws CouldNotCreateVideoCardResourceException {
		String builtinConstants = getBuiltInConstants();

		String builtinFunctions = getBuiltInFunction();

		String modifiedSource;

		//
		// This requires that #version be the first line in the shader. This
		// doesn't follow the spec exactly, which allows whitespace and
		// comments to come beforehand.
		//
		if (source.startsWith("#version")) {
			modifiedSource = "//" + source;
		} else {
			modifiedSource = source;
		}

		String[] sources = new String[] { builtinConstants, modifiedSource, builtinFunctions };

		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
		_shaderObject = gl.glCreateShader(shaderType);

		gl.glShaderSource(_shaderObject, sources.length, sources, null, 0);

		gl.glCompileShader(_shaderObject);

		IntBuffer compileStatus = Buffers.newDirectIntBuffer(1);
		gl.glGetShaderiv(_shaderObject, GL2ES2.GL_COMPILE_STATUS, compileStatus);

		if (compileStatus.get(0) == 0) {
			System.out.println(sources[0]);
			System.out.println(sources[1]);
			System.out.println(sources[2]);

			String compileLog = getCompileLog();
			System.out.println(compileLog);

			throw new CouldNotCreateVideoCardResourceException("Could not compile shader object.  Compile Log:  \n\n" + compileLog);
		}
	}

	public int getHandle() {
		return _shaderObject;
	}

	public String getCompileLog() {
		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
		int[] infoLogLength = new int[1];
		gl.glGetShaderiv(_shaderObject, GL2ES2.GL_INFO_LOG_LENGTH, infoLogLength, 0);
		byte[] infoLog = new byte[infoLogLength[0]];
		gl.glGetShaderInfoLog(_shaderObject, infoLogLength[0], infoLogLength, 0, infoLog, 0);
		String result = new String(infoLog);
		return result;
	}

	public void dispose() {
		dispose(true);

	}

	protected void dispose(boolean disposing) {
		// Always delete the shader, even in the finalizer.
		if (_shaderObject != 0) {

			GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
			gl.glDeleteShader(_shaderObject);

			_shaderObject = 0;
		}

	}

	private int _shaderObject;
}