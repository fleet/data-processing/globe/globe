package fr.ifremer.globe.ogl.renderer.jogl.vertexarray;

import java.util.HashSet;
import java.util.Set;

import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.buffers.IndexBuffer;
import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.IndexBufferGLBase;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexArray;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

public abstract class VertexArrayGLBase extends VertexArray {
	public VertexArrayGLBase() {
		// NOT SUPPORTED
		// _name = new VertexArrayNameGL3x();
		_attributes = createVertexBufferAttributes();
	}

	public abstract VertexBufferAttributesGLBase createVertexBufferAttributes() ;
	public abstract int getMaximumNumberOfVertexAttributes();
	public void bind() {
		// NOT SUPPORTED
		// GL.BindVertexArray(_name.Value);
		_attributes.dirty();
		_dirtyIndexBuffer = true;
	}

	@Override
	public void unBind() {
		// VertexBufferGL3x.unBind();

		int count = getMaximumNumberOfVertexAttributes();
		for (int i = 0; i < count; i++) {
			_attributes.detach(i);
		}

		IndexBufferGLBase.unBind(GLU.getCurrentGL().getGL2GL3());
	}

	public void clean() {
		_attributes.clean();

		if (_dirtyIndexBuffer) {
			if (_indexBuffer != null) {
				IndexBufferGLBase bufferObjectGL = (IndexBufferGLBase) _indexBuffer;
				bufferObjectGL.bind();
			} else {
				IndexBufferGLBase.unBind(GLU.getCurrentGL().getGL2GL3());
			}

			_dirtyIndexBuffer = false;
		}
	}

	public int getMaximumArrayIndex() {
		return _attributes.getMaximumArrayIndex();
	}

	@Override
	public VertexBufferAttributes getAttributes() {
		return _attributes;
	}

	@Override
	public IndexBuffer getIndexBuffer() {
		return _indexBuffer;
	}

	@Override
	public void setIndexBuffer(IndexBuffer value) {
		_indexBuffer = value;
		_dirtyIndexBuffer = true;
	}

	@Override
	protected void dispose(boolean disposing) {
		if (disposing) {
			if (getDisposeBuffers()) {
				//
				// Multiple components may share the same vertex buffer, so
				// find the unique set of vertex buffers used by this vertex
				// array.
				//
				Set<VertexBuffer> vertexBuffers = new HashSet<VertexBuffer>();

				for (VertexBufferAttribute attribute : _attributes) {
					vertexBuffers.add(attribute.getVertexBuffer());
				}

				for (VertexBuffer vb : vertexBuffers) {
					vb.dispose();
				}

				if (_indexBuffer != null) {
					_indexBuffer.dispose();
				}
			}

			// NOT SUPPORTED
			// _name.Dispose();
		}

	}

	// NOT SUPPORTED
	// private VertexArrayNameGL3x _name;

	private VertexBufferAttributesGLBase _attributes;
	private IndexBuffer _indexBuffer;
	private boolean _dirtyIndexBuffer;
}