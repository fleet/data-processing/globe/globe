package fr.ifremer.globe.ogl.renderer.jogl.shaders;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.core.vectors.Vector3I;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanable;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.shaders.UniformType;

public class UniformIntVector3GL2x3x extends Uniform<Vector3I> implements ICleanable {
	public UniformIntVector3GL2x3x(String name, int location, ICleanableObserver observer) {
		super(name, UniformType.IntVector3);
		_location = location;
		_dirty = true;
		_observer = observer;
		_observer.notifyDirty(this);
	}

	@Override
	public Vector3I getValue() {
		return _value;
	}

	@Override
	public void setValue(Vector3I value) {
		if (!_dirty && (_value != value)) {
			_dirty = true;
			_observer.notifyDirty(this);
		}

		_value = value;
	};

	@Override
	public void clean() {
		if( _value != null) {
			GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
			gl.glUniform3i(_location, _value.getX(), _value.getY(), _value.getZ());
		}
		_dirty = false;
	}

	private int _location;
	private Vector3I _value;
	private boolean _dirty;
	private ICleanableObserver _observer;
}