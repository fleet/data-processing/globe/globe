package fr.ifremer.globe.ogl.renderer.jogl.shaders;

import fr.ifremer.globe.ogl.renderer.CouldNotCreateVideoCardResourceException;
import fr.ifremer.globe.ogl.renderer.Device3x;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.shaders.UniformCollection;

public class ShaderProgramGL3x extends ShaderProgramGL2x3x {

	public ShaderProgramGL3x(String vertexShaderSource, String fragmentShaderSource)
			throws CouldNotCreateVideoCardResourceException {
		super(vertexShaderSource, fragmentShaderSource);
	}
	public ShaderProgramGL3x(String vertexShaderSource, String geometryShaderSource, String fragmentShaderSource, boolean initializeAutomaticUniform)
			throws CouldNotCreateVideoCardResourceException {
		super(vertexShaderSource, geometryShaderSource,fragmentShaderSource,initializeAutomaticUniform);
	}
	public ShaderProgramGL3x(String vertexShaderSource, String geometryShaderSource, String fragmentShaderSource,
			Boolean example) throws CouldNotCreateVideoCardResourceException {
		super(vertexShaderSource, geometryShaderSource, fragmentShaderSource, example);
		// TODO Auto-generated constructor stub
	}
	@Override
	ShaderObjectGL2x3x createShaderObject(int shaderType, String source) throws CouldNotCreateVideoCardResourceException {
		return new ShaderObjectGL3x(shaderType,source);
	}
	@Override
	protected void initializeAutomaticUniforms(UniformCollection uniforms) {

		for (Uniform<?> uniform : uniforms) {
			if (Device3x.getLinkAutomaticUniforms().containsKey(uniform.getName())) {
				Device3x.getLinkAutomaticUniforms().get(uniform.getName()).set(uniform);
			} else if (Device3x.getDrawAutomaticUniformFactories().containsKey(uniform.getName())) {
				_drawAutomaticUniforms.add(Device3x.getDrawAutomaticUniformFactories().get(uniform.getName()).create(uniform));
			}
		}
	}


}
