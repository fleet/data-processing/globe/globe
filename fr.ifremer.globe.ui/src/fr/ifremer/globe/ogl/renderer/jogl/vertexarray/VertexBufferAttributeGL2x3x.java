package fr.ifremer.globe.ogl.renderer.jogl.vertexarray;

import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute;

/**
 * <p>
 * Description : VertexBufferAttributeGL3x.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 21 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class VertexBufferAttributeGL2x3x {

	public VertexBufferAttribute getVertexBufferAttribute() {
		return m_VertexBufferAttribute;
	}

	public void setVertexBufferAttribute(VertexBufferAttribute vertexBufferAttribute) {
		m_VertexBufferAttribute = vertexBufferAttribute;
	}

	public boolean isDirty() {
		return m_Dirty;
	}

	public void setDirty(boolean dirty) {
		m_Dirty = dirty;
	}

	private VertexBufferAttribute m_VertexBufferAttribute = null;
	private boolean m_Dirty = false;
}
