package fr.ifremer.globe.ogl.renderer.jogl.vertexarray;

import fr.ifremer.globe.ogl.renderer.Device2x;

public class VertexArrayGL2x extends VertexArrayGLBase {

	@Override
	public VertexBufferAttributesGL2x createVertexBufferAttributes() {
		return new VertexBufferAttributesGL2x();
	}

	@Override
	public int getMaximumNumberOfVertexAttributes() {
		return Device2x.instance.getMaximumNumberOfVertexAttributes();
	}

}