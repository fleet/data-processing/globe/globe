package fr.ifremer.globe.ogl.renderer.jogl.textures;

import com.jogamp.opengl.GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;

public class TextureUnitsGL3x extends TextureUnitsGLBase<TextureUnitGL3x> implements ICleanableObserver {

	@Override
	protected GL3 getGL() {
		return GLU.getCurrentGL().getGL3();
	}

	@Override
	TextureUnitGL3x create(int i) {

		return new TextureUnitGL3x(i,this);
	}


}