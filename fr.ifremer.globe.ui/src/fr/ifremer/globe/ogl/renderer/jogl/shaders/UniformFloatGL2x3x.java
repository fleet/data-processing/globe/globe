package fr.ifremer.globe.ogl.renderer.jogl.shaders;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.jogl.ICleanable;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.shaders.UniformType;

public class UniformFloatGL2x3x extends Uniform<Float> implements ICleanable {
	public UniformFloatGL2x3x(String name, int location, ICleanableObserver observer) {
		super(name, UniformType.Float);
		_location = location;
		_dirty = true;
		_observer = observer;
		_observer.notifyDirty(this);
	}

	@Override
	public Float getValue() {
		return _value;
	}

	@Override
	public void setValue(Float value) {
		if (!_dirty && (_value != value)) {
			_dirty = true;
			_observer.notifyDirty(this);
		}

		_value = value;
	}

	@Override
	public void clean() {

		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
		gl.glUniform1f(_location, _value);

		_dirty = false;
	}

	private int _location;
	private float _value;
	private boolean _dirty;
	private ICleanableObserver _observer;
}