package fr.ifremer.globe.ogl.renderer.jogl.shaders;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.core.matrices.Matrix4F;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanable;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.shaders.UniformType;

public class UniformFloatMatrix44GL2x3x extends Uniform<Matrix4F> implements ICleanable {
	public UniformFloatMatrix44GL2x3x(String name, int location, ICleanableObserver observer) {
		super(name, UniformType.FloatMatrix44);
		_location = location;
		_value = new Matrix4F();
		_dirty = true;
		_observer = observer;
		_observer.notifyDirty(this);
	}

	@Override
	public Matrix4F getValue() {
		return _value;
	}

	@Override
	public void setValue(Matrix4F value) {
		if (!_dirty && (_value != value)) {
			_dirty = true;
			_observer.notifyDirty(this);
		}

		_value = value;
	}

	@Override
	public void clean() {
		if( _value != null) {
			GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
			gl.glUniformMatrix4fv(_location, 1, false, _value.getReadOnlyColumnMajorValues(), 0);
		}
		_dirty = false;
	}

	private int _location;
	private Matrix4F _value;
	private boolean _dirty;
	private ICleanableObserver _observer;
}