package fr.ifremer.globe.ogl.renderer.jogl;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;

import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;

import com.jogamp.opengl.util.FPSAnimator;

import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.window.GraphicsWindow;

public abstract class GraphicsWindowBase extends GraphicsWindow implements GLEventListener {

	@Override
	public abstract GLCanvas getComponent() ;

	@Override
	public void addKeyListener(KeyListener listener) {
		getComponent().addKeyListener(listener);
	}

	@Override
	public void addMouseListener(MouseListener listener) {
		getComponent().addMouseListener(listener);
	}

	@Override
	public void addMouseMotionListener(MouseMotionListener listener) {
		getComponent().addMouseMotionListener(listener);
	}

	@Override
	public void addMouseWheelListener(MouseWheelListener listener) {
		getComponent().addMouseWheelListener(listener);
	}

	@Override
	public synchronized void display(GLAutoDrawable drawable) {
		fireUpdateFrame(drawable);
		firePreRender(drawable);
		fireRender(drawable);
		firePostRender(drawable);

		// Should not be called with GLAutoDrawable with automatic buffer
		// swapping
		// enabled (as default)
		// getComponent().swapBuffers();
	}

	// @Override
	// public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
	// boolean deviceChanged) {
	// fireDisplayChanged(drawable, modeChanged, deviceChanged);
	// }

	@Override
	public abstract Context getContext() ;

	@Override
	public int getHeight() {
		return getComponent().getHeight();
	}

	@Override
	public int getWidth() {
		return getComponent().getWidth();
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		fireInit(drawable);
	}

	@Override
	public void removeKeyListener(KeyListener listener) {
		getComponent().removeKeyListener(listener);
	}

	@Override
	public void removeMouseListener(MouseListener listener) {
		getComponent().removeMouseListener(listener);
	}

	@Override
	public void removeMouseMotionListener(MouseMotionListener listener) {
		getComponent().removeMouseMotionListener(listener);
	}

	@Override
	public void removeMouseWheelListener(MouseWheelListener listener) {
		getComponent().removeMouseWheelListener(listener);
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		fireReshape(drawable, x, y, width, height);
	}

	@Override
	public void display() {
		if (getContext().makeCurrent()) {
			// bvalliere : repaint seems to be faster and smoother than display
			getComponent().repaint();
			// getComponent().display();
			getContext().release();
		}
	}

	@Override
	public void run(double updateRate) {
		m_Animator = new FPSAnimator(getComponent(), (int) updateRate, true);
		m_Animator.setIgnoreExceptions(true);
		m_Animator.start();
	}

	@Override
	protected void dispose(boolean disposing) {
		getComponent().removeGLEventListener(this);
		if (m_Animator != null) {
			m_Animator.remove(getComponent());
		}
	}

	@Override
	public void pause() {
		if (m_Animator != null && m_Animator.isAnimating()) {
			m_Animator.stop();
		}
	}

	@Override
	public void resume() {
		if (m_Animator != null && !m_Animator.isAnimating()) {
			m_Animator.start();
		}
	}

	private static FPSAnimator m_Animator = null;

	@Override
	public void dispose(GLAutoDrawable arg0) {
		// TODO Auto-generated method stub

	}
}