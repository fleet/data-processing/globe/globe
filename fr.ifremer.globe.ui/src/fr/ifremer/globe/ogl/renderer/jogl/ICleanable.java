package fr.ifremer.globe.ogl.renderer.jogl;

public interface ICleanable {
	void clean();
}