package fr.ifremer.globe.ogl.renderer.jogl.shaders;

import fr.ifremer.globe.ogl.renderer.CouldNotCreateVideoCardResourceException;
import fr.ifremer.globe.ogl.renderer.Device2x;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.shaders.UniformCollection;

public class ShaderProgramGL2x extends ShaderProgramGL2x3x {

	public ShaderProgramGL2x(String vertexShaderSource, String fragmentShaderSource)
			throws CouldNotCreateVideoCardResourceException {
		super(vertexShaderSource, fragmentShaderSource);
	}
	public ShaderProgramGL2x(String vertexShaderSource, String geometryShaderSource, String fragmentShaderSource,
			Boolean example) throws CouldNotCreateVideoCardResourceException {
		super(vertexShaderSource, geometryShaderSource, fragmentShaderSource, example);
		// TODO Auto-generated constructor stub
	}
	public ShaderProgramGL2x(String vertexShaderSource, String geometryShaderSource, String fragmentShaderSource, boolean initializeAutomaticUniform)
			throws CouldNotCreateVideoCardResourceException {
		super(vertexShaderSource, geometryShaderSource,fragmentShaderSource,initializeAutomaticUniform);
	}
	@Override
	ShaderObjectGL2x3x createShaderObject(int shaderType, String source) throws CouldNotCreateVideoCardResourceException {
		return new ShaderObjectGL2x(shaderType,source);
	}
	@Override
	protected void initializeAutomaticUniforms(UniformCollection uniforms) {

		for (Uniform<?> uniform : uniforms) {
			if (Device2x.getLinkAutomaticUniforms().containsKey(uniform.getName())) {
				Device2x.getLinkAutomaticUniforms().get(uniform.getName()).set(uniform);
			} else if (Device2x.getDrawAutomaticUniformFactories().containsKey(uniform.getName())) {
				_drawAutomaticUniforms.add(Device2x.getDrawAutomaticUniformFactories().get(uniform.getName()).create(uniform));
			}
		}
	}

}
