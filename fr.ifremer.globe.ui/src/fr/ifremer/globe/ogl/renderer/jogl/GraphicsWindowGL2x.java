package fr.ifremer.globe.ogl.renderer.jogl;

import java.awt.Frame;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import fr.ifremer.globe.ogl.renderer.window.WindowType;

import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;

public class GraphicsWindowGL2x extends GraphicsWindowBase {

	public GraphicsWindowGL2x(int width, int height, String title, WindowType windowType) {
		super();
		if (width < 0) {
			throw new IllegalArgumentException("Width must be greater than or equal to zero.");
		}

		if (height < 0) {
			throw new IllegalArgumentException("Height must be greater than or equal to zero.");
		}

		GLProfile glprofile = GLProfile.getDefault();
		GLCapabilities caps = new GLCapabilities(glprofile);

		_gameWindow = new GLCanvas(caps, null, null);

		_gameWindow.addGLEventListener(this);

		_context = new ContextGL2x(_gameWindow);
	}

	public GraphicsWindowGL2x(Composite parent) {
		Frame frame = SWT_AWT.new_Frame(parent);
		GLProfile glprofile = GLProfile.getDefault();
		GLCapabilities caps = new GLCapabilities(glprofile);
		_gameWindow = new GLCanvas(caps, null, null);

		_gameWindow.addGLEventListener(this);
		_context = new ContextGL2x(_gameWindow);

		frame.add(_gameWindow);
	}

	@Override
	public GLCanvas getComponent() {
		return _gameWindow;
	}

	private ContextGL2x _context;
	private GLCanvas _gameWindow;


	@Override
	public ContextGL2x getContext() {
		return _context;
	}
}