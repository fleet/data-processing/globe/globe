package fr.ifremer.globe.ogl.renderer.jogl;

import java.awt.Color;
import java.awt.Rectangle;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.core.geometry.PrimitiveType;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.clearstate.ClearState;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.IndexBufferGLBase;
import fr.ifremer.globe.ogl.renderer.jogl.vertexarray.VertexArrayGLBase;
import fr.ifremer.globe.ogl.renderer.renderstate.AlphaTest;
import fr.ifremer.globe.ogl.renderer.renderstate.Blending;
import fr.ifremer.globe.ogl.renderer.renderstate.ColorMask;
import fr.ifremer.globe.ogl.renderer.renderstate.DepthRange;
import fr.ifremer.globe.ogl.renderer.renderstate.DepthTest;
import fr.ifremer.globe.ogl.renderer.renderstate.FacetCulling;
import fr.ifremer.globe.ogl.renderer.renderstate.ProgramPointSize;
import fr.ifremer.globe.ogl.renderer.renderstate.RasterizationMode;
import fr.ifremer.globe.ogl.renderer.renderstate.RenderState;
import fr.ifremer.globe.ogl.renderer.renderstate.ScissorTest;
import fr.ifremer.globe.ogl.renderer.renderstate.StencilTest;
import fr.ifremer.globe.ogl.renderer.renderstate.StencilTestFace;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ContextGLBase extends Context {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContextGLBase.class);

	protected abstract GL2GL3 getGL() ;
	protected abstract void createTextureUnit();
	public ContextGLBase(GLCanvas gameWindow) {

		gameWindow.addGLEventListener(new GLEventListener() {

			@Override
			public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
			}

			@Override
			public void init(GLAutoDrawable arg0) {

				LOGGER.debug("Context initialized");

				GL2GL3 gl = getGL();
				float[] tempFloatArray = new float[1];
				int[] tempIntArray = new int[1];
				gl.glGetFloatv(GL.GL_DEPTH_CLEAR_VALUE, tempFloatArray, 0);
				_clearDepth = tempFloatArray[0];
				gl.glGetIntegerv(GL.GL_STENCIL_CLEAR_VALUE, tempIntArray, 0);
				_clearStencil = tempIntArray[0];
				float[] clearColor = new float[4];
				gl.glGetFloatv(GL.GL_COLOR_CLEAR_VALUE, clearColor, 0);

				_clearColor = new Color(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);

				_renderState = new RenderState();

				createTextureUnit();
				//
				// Sync GL state with default render state.
				//
				forceApplyRenderState(_renderState);

				setViewport(new Rectangle(0, 0, _gameWindow.getWidth(), _gameWindow.getHeight()));
			}

			@Override
			public void display(GLAutoDrawable arg0) {
			}

			@Override
			public void dispose(GLAutoDrawable arg0) {

			}
		});

		_gameWindow = gameWindow;
	}

	/**
	 * apply a render state
	 * */
	@Override
	public void forceApplyRenderState(RenderState renderState) {
		GL2GL3 gl = getGL();

		// NOT SUPPORTED

		// enable(EnableCap.PrimitiveRestart,
		// renderState.PrimitiveRestart.Enabled);
		// GL.PrimitiveRestartIndex(renderState.PrimitiveRestart.Index);

		// enable(EnableCap.CullFace, renderState.FacetCulling.Enabled);
		// GL.CullFace(TypeConverterGL3x.To(renderState.FacetCulling.Face));
		// GL.FrontFace(TypeConverterGL3x.To(renderState.FacetCulling.FrontFaceWindingOrder));
		enable(GL.GL_CULL_FACE, renderState.getFacetCulling().isEnabled(),gl);
		gl.glCullFace(TypeConverterGL2x3x.to(renderState.getFacetCulling().getFace()));
		gl.glFrontFace(TypeConverterGL2x3x.to(renderState.getFacetCulling().getFrontFaceWindingOrder()));

		// enable(EnableCap.ProgramPointSize, renderState.ProgramPointSize ==
		// ProgramPointSize.Enabled);
		// GL.PolygonMode(MaterialFace.FrontAndBack,
		// TypeConverterGL3x.To(renderState.RasterizationMode));
		enable(GL3.GL_PROGRAM_POINT_SIZE_ARB, renderState.getProgramPointSize() == ProgramPointSize.Enabled,gl);
		gl.glPolygonMode(GL.GL_FRONT_AND_BACK, TypeConverterGL2x3x.to(renderState.getRasterizationMode()));

		// enable(EnableCap.ScissorTest, renderState.ScissorTest.Enabled);
		// Rectangle rectangle = renderState.ScissorTest.Rectangle;
		// GL.Scissor(rectangle.Left, rectangle.Bottom, rectangle.Width,
		// rectangle.Height);
		enable(GL.GL_SCISSOR_TEST, renderState.getScissorTest().isEnabled(),gl);
		Rectangle rectangle = renderState.getScissorTest().getRectangle();
		gl.glScissor(rectangle.x, rectangle.y + rectangle.height, rectangle.width, rectangle.height);

		// enable(EnableCap.StencilTest, renderState.StencilTest.Enabled);
		// ForceApplyRenderStateStencil(StencilFace.Front,
		// renderState.StencilTest.FrontFace);
		// ForceApplyRenderStateStencil(StencilFace.Back,
		// renderState.StencilTest.BackFace);
		enable(GL.GL_STENCIL_TEST, renderState.getStencilTest().isEnabled(),gl);
		forceApplyRenderStateStencil(GL.GL_FRONT, renderState.getStencilTest().getFrontFace());
		forceApplyRenderStateStencil(GL.GL_BACK, renderState.getStencilTest().getBackFace());

		// enable(EnableCap.DepthTest, renderState.DepthTest.Enabled);
		// GL.DepthFunc(TypeConverterGL3x.To(renderState.DepthTest.Function));
		enable(GL.GL_DEPTH_TEST, renderState.getDepthTest().isEnabled(),gl);
		gl.glDepthFunc(TypeConverterGL2x3x.to(renderState.getDepthTest().getFunction()));


		// GL.DepthRange(renderState.DepthRange.Near,
		// renderState.DepthRange.Far);
		gl.glDepthRange(renderState.getDepthRange().getNear(), renderState.getDepthRange().getFar());

		// enable(EnableCap.Blend, renderState.Blending.Enabled);
		// GL.BlendFuncSeparate(
		// TypeConverterGL3x.To(renderState.Blending.SourceRGBFactor),
		// TypeConverterGL3x.To(renderState.Blending.DestinationRGBFactor),
		// TypeConverterGL3x.To(renderState.Blending.SourceAlphaFactor),
		// TypeConverterGL3x.To(renderState.Blending.DestinationAlphaFactor));
		// GL.BlendEquationSeparate(
		// TypeConverterGL3x.To(renderState.Blending.RGBEquation),
		// TypeConverterGL3x.To(renderState.Blending.AlphaEquation));
		// GL.BlendColor(renderState.Blending.Color);
		enable(GL.GL_BLEND, renderState.getBlending().isEnabled(),gl);
		gl.glBlendFuncSeparate(TypeConverterGL2x3x.to(renderState.getBlending().getSourceRGBFactor()), TypeConverterGL2x3x.to(renderState.getBlending().getDestinationRGBFactor()),
				TypeConverterGL2x3x.to(renderState.getBlending().getSourceAlphaFactor()), TypeConverterGL2x3x.to(renderState.getBlending().getDestinationAlphaFactor()));
		gl.glBlendEquationSeparate(TypeConverterGL2x3x.to(renderState.getBlending().getRGBEquation()), TypeConverterGL2x3x.to(renderState.getBlending().getAlphaEquation()));
		float[] color = renderState.getBlending().getColor().getColorComponents(null);
		gl.glBlendColor(color[0], color[1], color[2], color.length > 3 ? color[3] : 1.0f);

		// GL.DepthMask(renderState.DepthMask);
		// GL.ColorMask(renderState.ColorMask.Red, renderState.ColorMask.Green,
		// renderState.ColorMask.Blue, renderState.ColorMask.Alpha);
		gl.glDepthMask(renderState.getDepthMask());
		gl.glColorMask(renderState.getColorMask().getRed(), renderState.getColorMask().getGreen(), renderState.getColorMask().getBlue(), renderState.getColorMask().getAlpha());
	}

	private void forceApplyRenderStateStencil(int face, StencilTestFace test) {

		// GL.StencilOpSeparate(face,
		// TypeConverterGL3x.to(test.getStencilFailOperation()),
		// TypeConverterGL3x.to(test.getDepthFailStencilPassOperation()),
		// TypeConverterGL3x.to(test.getDepthPassStencilPassOperation()));
		//
		// GL.StencilFuncSeparate(face,
		// TypeConverterGL3x.to(test.getFunction()),
		// test.getReferenceValue(),
		// test.getMask());
		GL2GL3 gl=getGL();
		gl.glStencilOpSeparate(face, TypeConverterGL2x3x.to(test.getStencilFailOperation()), TypeConverterGL2x3x.to(test.getDepthFailStencilPassOperation()),
				TypeConverterGL2x3x.to(test.getDepthPassStencilPassOperation()));

		gl.glStencilFuncSeparate(face, TypeConverterGL2x3x.to(test.getFunction()), test.getReferenceValue(), test.getMask());
	}

	@Override
	public boolean makeCurrent() {
		GLContext context = _gameWindow.getContext();
		return (context != null) && (context.makeCurrent() != GLContext.CONTEXT_NOT_CURRENT);
	}

	@Override
	public void release() {
		GLContext context = _gameWindow.getContext();
		if ( context != null){
			context.release();
		}
	}




	@Override
	public Rectangle getViewport() {
		return _viewport;
	}

	@Override
	public void setViewport(Rectangle rectangle) {
		if (rectangle.getWidth() < 0 || rectangle.getHeight() < 0) {
			throw new IllegalArgumentException("The viewport width and height must be greater than or equal to zero.");
		}

		if (_viewport != rectangle) {
			_viewport = rectangle;

			// GL.Viewport(value);

			final GL2GL3 gl = getGL();
			gl.glViewport(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
		}
	}


	@Override
	public void clear(ClearState clearState) {

		applyFramebuffer();

		GL2GL3 gl = getGL();
		applyScissorTest(clearState.getScissorTest(),gl);
		applyColorMask(clearState.getColorMask(),gl);
		applyDepthMask(clearState.isDepthMask(),gl);
		// TODO: StencilMaskSeparate


		if (_clearColor != clearState.getColor()) {

			// GL.ClearColor(clearState.getColor());
			float[] color = clearState.getColor().getColorComponents(null);
			gl.glClearColor(color[0], color[1], color[2], color.length > 3 ? color[3] : 1.0f);

			_clearColor = clearState.getColor();
		}

		if (_clearDepth != clearState.getDepth()) {

			// GL.ClearDepth((double)clearState.getDepth());
			gl.glClearDepth(clearState.getDepth());

			_clearDepth = clearState.getDepth();
		}

		if (_clearStencil != clearState.getStencil()) {
			gl.glClearStencil(clearState.getStencil());

			_clearStencil = clearState.getStencil();
		}

		gl.glClear(TypeConverterGL2x3x.to(clearState.getBuffers()));
	}

	@Override
	public void draw(PrimitiveType primitiveType, int offset, int count, DrawState drawState, SceneState sceneState) {
		verifyDraw(drawState, sceneState);
		applyBeforeDraw(drawState, sceneState);

		reallyDraw(primitiveType,drawState,sceneState);

		applyAfterDraw(drawState);
	}
	protected abstract void reallyDraw(PrimitiveType primitiveType, DrawState drawState, SceneState sceneState);

	@Override
	public void draw(PrimitiveType primitiveType, DrawState drawState, SceneState sceneState) {
		verifyDraw(drawState, sceneState);
		applyBeforeDraw(drawState, sceneState);

		VertexArrayGLBase vertexArray = (VertexArrayGLBase) drawState.getVertexArray();
		IndexBufferGLBase indexBuffer = (IndexBufferGLBase) vertexArray.getIndexBuffer();

		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();

		if (indexBuffer != null) {
			gl.glDrawRangeElements(TypeConverterGL2x3x.to(primitiveType), 0, vertexArray.getMaximumArrayIndex(), indexBuffer.getCount(), TypeConverterGL2x3x.to(indexBuffer.getDatatype()), 0);

		} else {
			gl.glDrawArrays(TypeConverterGL2x3x.to(primitiveType), 0, vertexArray.getMaximumArrayIndex() + 1);
		}

		applyAfterDraw(drawState);
	}

	// NOT SUPPORTED
	//
	// private void applyPrimitiveRestart(PrimitiveRestart primitiveRestart) {
	// if (_renderState.getPrimitiveRestart().isEnabled() != primitiveRestart
	// .isEnabled()) {
	// Enable(EnableCap.PrimitiveRestart, primitiveRestart.isEnabled());
	// _renderState.getPrimitiveRestart().setEnabled(
	// primitiveRestart.isEnabled());
	// }
	//
	// if (primitiveRestart.isEnabled()) {
	// if (_renderState.getPrimitiveRestart().getIndex() != primitiveRestart
	// .getIndex()) {
	// GL.PrimitiveRestartIndex(primitiveRestart.getIndex());
	// _renderState.getPrimitiveRestart()
	// .setIndex(primitiveRestart.getIndex());
	// }
	// }
	// }

	private void applyFacetCulling(FacetCulling facetCulling, GL2GL3 gl) {
		if (_renderState.getFacetCulling().isEnabled() != facetCulling.isEnabled()) {
			enable(GL.GL_CULL_FACE, facetCulling.isEnabled(),gl);
			_renderState.getFacetCulling().setEnabled(facetCulling.isEnabled());
		}

		if (facetCulling.isEnabled()) {
			if (_renderState.getFacetCulling().getFace() != facetCulling.getFace()) {

				// GL.CullFace(TypeConverterGL3x.to(facetCulling.getFace()));
				gl.glCullFace(TypeConverterGL2x3x.to(facetCulling.getFace()));
				_renderState.getFacetCulling().setFace(facetCulling.getFace());
			}

			if (_renderState.getFacetCulling().getFrontFaceWindingOrder() != facetCulling.getFrontFaceWindingOrder()) {
				// GL.FrontFace(TypeConverterGL3x.to(facetCulling.getFrontFaceWindingOrder()));
				gl.glFrontFace(TypeConverterGL2x3x.to(facetCulling.getFrontFaceWindingOrder()));
				_renderState.getFacetCulling().setFrontFaceWindingOrder(facetCulling.getFrontFaceWindingOrder());
			}
		}
	}

	private void applyProgramPointSize(ProgramPointSize programPointSize,GL2GL3 gl) {
		if (_renderState.getProgramPointSize() != programPointSize) {
			enable(GL3.GL_PROGRAM_POINT_SIZE_ARB, programPointSize == ProgramPointSize.Enabled,gl);
			_renderState.setProgramPointSize(programPointSize);
		}
	}

	private void applyRasterizationMode(RasterizationMode rasterizationMode, GL2GL3 gl) {
		if (_renderState.getRasterizationMode() != rasterizationMode) {

			// GL.PolygonMode(MaterialFace.FrontAndBack,TypeConverterGL3x.To(rasterizationMode));
			gl.glPolygonMode(GL.GL_FRONT_AND_BACK, TypeConverterGL2x3x.to(rasterizationMode));
			_renderState.setRasterizationMode(rasterizationMode);
		}
	}

	private void applyScissorTest(ScissorTest scissorTest, GL2GL3 gl) {
		Rectangle rectangle = scissorTest.getRectangle();

		if (rectangle.getWidth() < 0) {
			throw new IllegalArgumentException("renderState.ScissorTest.Rectangle.Width must be greater than or equal to zero.");
		}

		if (rectangle.getHeight() < 0) {
			throw new IllegalArgumentException("renderState.ScissorTest.Rectangle.Height must be greater than or equal to zero.");
		}

		if (_renderState.getScissorTest().isEnabled() != scissorTest.isEnabled()) {
			enable(GL.GL_SCISSOR_TEST, scissorTest.isEnabled(),gl);
			_renderState.getScissorTest().setEnabled(scissorTest.isEnabled());
		}

		if (scissorTest.isEnabled()) {
			if (_renderState.getScissorTest().getRectangle() != scissorTest.getRectangle()) {

				// GL.Scissor(rectangle.Left, rectangle.Bottom,
				// rectangle.Width,rectangle.Height);
				gl.glScissor(rectangle.x, rectangle.y + rectangle.height, rectangle.width, rectangle.height);
				_renderState.getScissorTest().setRectangle(scissorTest.getRectangle());
			}
		}
	}

	private void applyStencilTest(StencilTest stencilTest,GL2GL3 gl) {
		if (_renderState.getStencilTest().isEnabled() != stencilTest.isEnabled()) {
			enable(GL.GL_STENCIL_TEST, stencilTest.isEnabled(),gl);
			_renderState.getStencilTest().setEnabled(stencilTest.isEnabled());
		}

		if (stencilTest.isEnabled()) {
			applyStencil(GL.GL_FRONT, _renderState.getStencilTest().getFrontFace(), stencilTest.getFrontFace(),gl);
			applyStencil(GL.GL_BACK, _renderState.getStencilTest().getBackFace(), stencilTest.getBackFace(),gl);
		}
	}

	private static void applyStencil(int face, StencilTestFace currentTest, StencilTestFace test, GL2GL3 gl) {


		if ((currentTest.getStencilFailOperation() != test.getStencilFailOperation()) || (currentTest.getDepthFailStencilPassOperation() != test.getDepthFailStencilPassOperation())
				|| (currentTest.getDepthPassStencilPassOperation() != test.getDepthPassStencilPassOperation())) {

			// GL.StencilOpSeparate(face,
			// TypeConverterGL3x.to(test.getStencilFailOperation()),
			// TypeConverterGL3x.to(test.getDepthFailStencilPassOperation()),
			// TypeConverterGL3x.to(test.getDepthPassStencilPassOperation()));
			gl.glStencilOpSeparate(face, TypeConverterGL2x3x.to(test.getStencilFailOperation()), TypeConverterGL2x3x.to(test.getDepthFailStencilPassOperation()),
					TypeConverterGL2x3x.to(test.getDepthPassStencilPassOperation()));

			currentTest.setStencilFailOperation(test.getStencilFailOperation());
			currentTest.setDepthFailStencilPassOperation(test.getDepthFailStencilPassOperation());
			currentTest.setDepthPassStencilPassOperation(test.getDepthPassStencilPassOperation());
		}

		if ((currentTest.getFunction() != test.getFunction()) || (currentTest.getReferenceValue() != test.getReferenceValue()) || (currentTest.getMask() != test.getMask())) {

			// GL.StencilFuncSeparate(face,
			// TypeConverterGL3x.To(test.getFunction()),
			// test.ReferenceValue, test.Mask);
			gl.glStencilFuncSeparate(face, TypeConverterGL2x3x.to(test.getFunction()), test.getReferenceValue(), test.getMask());

			currentTest.setFunction(test.getFunction());
			currentTest.setReferenceValue(test.getReferenceValue());
			currentTest.setMask(test.getMask());
		}
	}

	private void applyDepthTest(DepthTest depthTest, GL2GL3 gl) {
		if (_renderState.getDepthTest().isEnabled() != depthTest.isEnabled()) {
			enable(GL.GL_DEPTH_TEST, depthTest.isEnabled(),gl);
			_renderState.getDepthTest().setEnabled(depthTest.isEnabled());
		}

		if (depthTest.isEnabled()) {
			if (_renderState.getDepthTest().getFunction() != depthTest.getFunction()) {

				// GL.DepthFunc(TypeConverterGL3x.To(depthTest.Function));
				gl.glDepthFunc(TypeConverterGL2x3x.to(depthTest.getFunction()));

				_renderState.getDepthTest().setFunction(depthTest.getFunction());
			}
		}
	}



	private void applyDepthRange(DepthRange depthRange, GL2GL3 gl) {
		if (depthRange.getNear() < 0.0 || depthRange.getNear() > 1.0) {
			throw new IllegalArgumentException("renderState.DepthRange.Near must be between zero and one.");
		}

		if (depthRange.getFar() < 0.0 || depthRange.getFar() > 1.0) {
			throw new IllegalArgumentException("renderState.DepthRange.Far must be between zero and one.");
		}

		// if ((_renderState.getDepthRange().getNear() != depthRange.getNear())
		// || (_renderState.getDepthRange().getFar() != depthRange.getFar())) {

		// GL.DepthRange(depthRange.getNear(), depthRange.getFar());
		gl.glDepthRange(depthRange.getNear(), depthRange.getFar());

		_renderState.getDepthRange().setNear(depthRange.getNear());
		_renderState.getDepthRange().setFar(depthRange.getFar());
		// }
	}

	private void applyBlending(Blending blending, GL2GL3 gl) {
		// if (_renderState.getBlending().isEnabled() != blending.isEnabled()) {
		enable(GL.GL_BLEND, blending.isEnabled(),gl);
		_renderState.getBlending().setEnabled(blending.isEnabled());
		// }

		if (blending.isEnabled()) {
			if ((_renderState.getBlending().getSourceRGBFactor() != blending.getSourceRGBFactor()) || (_renderState.getBlending().getDestinationRGBFactor() != blending.getDestinationRGBFactor())
					|| (_renderState.getBlending().getSourceAlphaFactor() != blending.getSourceAlphaFactor())
					|| (_renderState.getBlending().getDestinationAlphaFactor() != blending.getDestinationAlphaFactor())) {

				// GL.BlendFuncSeparate(TypeConverterGL3x.To(blending.SourceRGBFactor),
				// TypeConverterGL3x.To(blending.DestinationRGBFactor),
				// TypeConverterGL3x.To(blending.SourceAlphaFactor),
				// TypeConverterGL3x.To(blending.DestinationAlphaFactor));
				gl.glBlendFuncSeparate(TypeConverterGL2x3x.to(blending.getSourceRGBFactor()), TypeConverterGL2x3x.to(blending.getDestinationRGBFactor()),
						TypeConverterGL2x3x.to(blending.getSourceAlphaFactor()), TypeConverterGL2x3x.to(blending.getDestinationAlphaFactor()));

				_renderState.getBlending().setSourceRGBFactor(blending.getSourceRGBFactor());
				_renderState.getBlending().setDestinationRGBFactor(blending.getDestinationRGBFactor());
				_renderState.getBlending().setSourceAlphaFactor(blending.getSourceAlphaFactor());
				_renderState.getBlending().setDestinationAlphaFactor(blending.getDestinationAlphaFactor());
			}

			if ((_renderState.getBlending().getRGBEquation() != blending.getRGBEquation()) || (_renderState.getBlending().getAlphaEquation() != blending.getAlphaEquation())) {

				// GL.BlendEquationSeparate(
				// TypeConverterGL3x.to(blending.getRGBEquation()),
				// TypeConverterGL3x.to(blending.getAlphaEquation()));
				gl.glBlendEquationSeparate(TypeConverterGL2x3x.to(blending.getRGBEquation()), TypeConverterGL2x3x.to(blending.getAlphaEquation()));

				_renderState.getBlending().setRGBEquation(blending.getRGBEquation());
				_renderState.getBlending().setAlphaEquation(blending.getAlphaEquation());
			}

			if (_renderState.getBlending().getColor() != blending.getColor()) {

				// GL.BlendColor(blending.getColor());
				float[] color = blending.getColor().getColorComponents(null);
				gl.glBlendColor(color[0], color[1], color[2], color.length > 3 ? color[3] : 1.0f);
				_renderState.getBlending().setColor(blending.getColor());
			}
		}
	}

	private void applyColorMask(ColorMask colorMask, GL2GL3 gl) {
		if (_renderState.getColorMask() != colorMask) {

			// GL.ColorMask(colorMask.getRed(), colorMask.getGreen(),
			// colorMask.getBlue(), colorMask.getAlpha());
			gl.glColorMask(colorMask.getRed(), colorMask.getGreen(), colorMask.getBlue(), colorMask.getAlpha());
			_renderState.setColorMask(colorMask);
		}
	}

	private void applyDepthMask(boolean depthMask, GL2GL3 gl) {
		if (_renderState.getDepthMask() != depthMask) {

			// GL.DepthMask(depthMask);
			gl.glDepthMask(depthMask);
			_renderState.setDepthMask(depthMask);
		}
	}

	public static void enable(int enableCap, boolean enable, GL2GL3 gl) {

		if (enable) {
			// GL.Enable(enableCap);
			gl.glEnable(enableCap);
		} else {
			// GL.Disable(enableCap);
			gl.glDisable(enableCap);
		}
	}

	private void verifyDraw(DrawState drawState, SceneState sceneState) {
		if (drawState == null) {
			throw new IllegalArgumentException("drawState");
		}

		if (drawState.getRenderState() == null) {
			throw new IllegalArgumentException("drawState.RenderState");
		}

		if (drawState.getShaderProgram() == null) {
			throw new IllegalArgumentException("drawState.ShaderProgram");
		}

		if (drawState.getVertexArray() == null) {
			throw new IllegalArgumentException("drawState.VertexArray");
		}

		if (sceneState == null) {
			throw new IllegalArgumentException("sceneState");
		}

	}

	protected void applyBeforeDraw(DrawState drawState, SceneState sceneState) {
		applyRenderState(drawState.getRenderState());
		applyVertexArray(drawState.getVertexArray());
		applyShaderProgram(drawState, sceneState);
		getTextureUnits().clean();
		applyFramebuffer();
	}

	protected abstract void applyAfterDraw(DrawState drawState) ;

	private void applyRenderState(RenderState renderState) {
		// NOT SUPPORTED
		GL2GL3 gl=getGL();
		// applyPrimitiveRestart(renderState.getPrimitiveRestart());

		applyFacetCulling(renderState.getFacetCulling(),gl);
		applyProgramPointSize(renderState.getProgramPointSize(),gl);
		applyRasterizationMode(renderState.getRasterizationMode(),gl);
		applyScissorTest(renderState.getScissorTest(),gl);
		applyStencilTest(renderState.getStencilTest(),gl);
		applyAlphaTest(renderState.getAlphaTest());
		applyDepthTest(renderState.getDepthTest(),gl);
		applyDepthRange(renderState.getDepthRange(),gl);
		applyBlending(renderState.getBlending(),gl);
		applyColorMask(renderState.getColorMask(),gl);
		applyDepthMask(renderState.getDepthMask(),gl);
	}

	protected void applyAlphaTest(AlphaTest alphaTest) {

	}
	protected abstract void applyVertexArray(VertexArray vertexArray);

	protected abstract void applyShaderProgram(DrawState drawState, SceneState sceneState) ;
	protected  abstract void applyFramebuffer() ;

	private Color _clearColor;
	private float _clearDepth;
	private int _clearStencil;
	private Rectangle _viewport;

	protected RenderState _renderState;

	private GLCanvas _gameWindow;
}