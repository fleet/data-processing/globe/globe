package fr.ifremer.globe.ogl.renderer.jogl.shaders;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.jogl.ICleanable;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.shaders.UniformType;

public class UniformIntGL2x3x extends Uniform<Integer> implements ICleanable {
	public UniformIntGL2x3x(String name, int location, UniformType type, ICleanableObserver observer) {
		super(name, type);
		_location = location;
		_dirty = true;
		_observer = observer;
		_observer.notifyDirty(this);
	}

	@Override
	public Integer getValue() {
		return _value;
	}

	@Override
	public void setValue(Integer value) {
		if (!_dirty && (_value != value)) {
			_dirty = true;
			_observer.notifyDirty(this);
		}

		_value = value;
	}

	@Override
	public void clean() {

		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
		gl.glUniform1i(_location, _value);

		_dirty = false;
	}

	private int _location;
	private int _value;
	private boolean _dirty;
	private ICleanableObserver _observer;
}