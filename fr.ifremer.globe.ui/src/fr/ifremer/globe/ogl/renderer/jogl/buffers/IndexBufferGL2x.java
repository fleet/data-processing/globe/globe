package fr.ifremer.globe.ogl.renderer.jogl.buffers;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;

public class IndexBufferGL2x extends IndexBufferGLBase {

	public IndexBufferGL2x(BufferHint usageHint, int size) {
		super(usageHint, size);
	}

	@Override
	protected GL2GL3 getGL() {
		return GLU.getCurrentGL().getGL2();
	}

}