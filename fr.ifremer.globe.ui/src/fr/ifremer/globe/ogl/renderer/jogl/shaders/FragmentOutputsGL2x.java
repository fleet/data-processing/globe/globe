package fr.ifremer.globe.ogl.renderer.jogl.shaders;

import fr.ifremer.globe.ogl.renderer.jogl.names.ShaderProgramNameGL2x3x;
import fr.ifremer.globe.ogl.renderer.shaders.FragmentOutputs;

public class FragmentOutputsGL2x extends FragmentOutputs {
	public FragmentOutputsGL2x(ShaderProgramNameGL2x3x program) {
		_program = program;
	}

	@Override
	public int get(String index) {

		// NOT SUPPORTED
		// // int i = GL.GetFragDataLocation(_program.getValue(), index);
		// GL2 gl = GLU.getCurrentGL().getGL2();
		// int i = gl.glGetFragDataLocationEXT(_program.getValue(),
		// index.getBytes(),
		// 0);
		//
		// if (i == -1) {
		// throw new IllegalArgumentException(index);
		// }
		//
		// return i;

		// Returning 0 : index of gl_FragColor
		return 0;
	}

	@SuppressWarnings("unused")
	private ShaderProgramNameGL2x3x _program;
}