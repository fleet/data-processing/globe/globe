package fr.ifremer.globe.ogl.renderer.jogl;

import java.util.EnumSet;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GL2ES3;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GL3;

import fr.ifremer.globe.ogl.core.geometry.PrimitiveType;
import fr.ifremer.globe.ogl.core.geometry.WindingOrder;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.buffers.IndexBufferDatatype;
import fr.ifremer.globe.ogl.renderer.clearstate.ClearBuffers;
import fr.ifremer.globe.ogl.renderer.renderstate.AlphaTestFunction;
import fr.ifremer.globe.ogl.renderer.renderstate.BlendEquation;
import fr.ifremer.globe.ogl.renderer.renderstate.CullFace;
import fr.ifremer.globe.ogl.renderer.renderstate.DepthTestFunction;
import fr.ifremer.globe.ogl.renderer.renderstate.DestinationBlendingFactor;
import fr.ifremer.globe.ogl.renderer.renderstate.RasterizationMode;
import fr.ifremer.globe.ogl.renderer.renderstate.SourceBlendingFactor;
import fr.ifremer.globe.ogl.renderer.renderstate.StencilOperation;
import fr.ifremer.globe.ogl.renderer.renderstate.StencilTestFunction;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderVertexAttributeType;
import fr.ifremer.globe.ogl.renderer.shaders.UniformType;
import fr.ifremer.globe.ogl.renderer.textures.TextureMagnificationFilter;
import fr.ifremer.globe.ogl.renderer.textures.TextureMinificationFilter;
import fr.ifremer.globe.ogl.renderer.textures.TextureWrap;
import fr.ifremer.globe.ogl.renderer.vertexarray.ComponentDatatype;

public class TypeConverterGL2x3x {
	public static int to(EnumSet<ClearBuffers> mask) {
		int clearMask = 0;

		if (mask.contains(ClearBuffers.ColorBuffer)) {
			clearMask |= GL.GL_COLOR_BUFFER_BIT;
		}

		if (mask.contains(ClearBuffers.DepthBuffer)) {
			clearMask |= GL.GL_DEPTH_BUFFER_BIT;
		}

		if (mask.contains(ClearBuffers.StencilBuffer)) {
			clearMask |= GL.GL_STENCIL_BUFFER_BIT;
		}

		return clearMask;
	}

	public static ShaderVertexAttributeType toShaderVertexAttributeType(int type) {
		switch (type) {
		case GL.GL_FLOAT:
			return ShaderVertexAttributeType.Float;
		case GL2ES2.GL_FLOAT_VEC2:
			return ShaderVertexAttributeType.FloatVector2;
		case GL2ES2.GL_FLOAT_VEC3:
			return ShaderVertexAttributeType.FloatVector3;
		case GL2ES2.GL_FLOAT_VEC4:
			return ShaderVertexAttributeType.FloatVector4;
		case GL2ES2.GL_FLOAT_MAT2:
			return ShaderVertexAttributeType.FloatMatrix22;
		case GL2ES2.GL_FLOAT_MAT3:
			return ShaderVertexAttributeType.FloatMatrix33;
		case GL2ES2.GL_FLOAT_MAT4:
			return ShaderVertexAttributeType.FloatMatrix44;
		case GL2ES2.GL_INT:
			return ShaderVertexAttributeType.Int;
		case GL2ES2.GL_INT_VEC2:
			return ShaderVertexAttributeType.IntVector2;
		case GL2ES2.GL_INT_VEC3:
			return ShaderVertexAttributeType.IntVector3;
		case GL2ES2.GL_INT_VEC4:
			return ShaderVertexAttributeType.IntVector4;
		}

		throw new IllegalArgumentException("type");
	}

	public static UniformType toUniformType(int type) {
		switch (type) {
		case GL2ES2.GL_INT:
			return UniformType.Int;
		case GL.GL_FLOAT:
			return UniformType.Float;
		case GL2ES2.GL_FLOAT_VEC2:
			return UniformType.FloatVector2;
		case GL2ES2.GL_FLOAT_VEC3:
			return UniformType.FloatVector3;
		case GL2ES2.GL_FLOAT_VEC4:
			return UniformType.FloatVector4;
		case GL2ES2.GL_INT_VEC2:
			return UniformType.IntVector2;
		case GL2ES2.GL_INT_VEC3:
			return UniformType.IntVector3;
		case GL2ES2.GL_INT_VEC4:
			return UniformType.IntVector4;
		case GL2ES2.GL_BOOL:
			return UniformType.Bool;
		case GL2ES2.GL_BOOL_VEC2:
			return UniformType.BoolVector2;
		case GL2ES2.GL_BOOL_VEC3:
			return UniformType.BoolVector3;
		case GL2ES2.GL_BOOL_VEC4:
			return UniformType.BoolVector4;
		case GL2ES2.GL_FLOAT_MAT2:
			return UniformType.FloatMatrix22;
		case GL2ES2.GL_FLOAT_MAT3:
			return UniformType.FloatMatrix33;
		case GL2ES2.GL_FLOAT_MAT4:
			return UniformType.FloatMatrix44;
		case GL2GL3.GL_SAMPLER_1D:
			return UniformType.Sampler1D;
		case GL2ES2.GL_SAMPLER_2D:
			return UniformType.Sampler2D;
		case GL2.GL_SAMPLER_2D_RECT_ARB:
			return UniformType.Sampler2DRectangle;
		case GL2.GL_SAMPLER_2D_RECT_SHADOW_ARB:
			return UniformType.Sampler2DRectangleShadow;
		case GL2ES2.GL_SAMPLER_3D:
			return UniformType.Sampler3D;
		case GL2ES2.GL_SAMPLER_CUBE:
			return UniformType.SamplerCube;
		case GL2GL3.GL_SAMPLER_1D_SHADOW:
			return UniformType.Sampler1DShadow;
		case GL2ES2.GL_SAMPLER_2D_SHADOW:
			return UniformType.Sampler2DShadow;
		case GL2ES3.GL_FLOAT_MAT2x3:
			return UniformType.FloatMatrix23;
		case GL2ES3.GL_FLOAT_MAT2x4:
			return UniformType.FloatMatrix24;
		case GL2ES3.GL_FLOAT_MAT3x2:
			return UniformType.FloatMatrix32;
		case GL2ES3.GL_FLOAT_MAT3x4:
			return UniformType.FloatMatrix34;
		case GL2ES3.GL_FLOAT_MAT4x2:
			return UniformType.FloatMatrix42;
		case GL2ES3.GL_FLOAT_MAT4x3:
			return UniformType.FloatMatrix43;
		case GL2GL3.GL_SAMPLER_1D_ARRAY:
			return UniformType.Sampler1DArray;
		case GL3.GL_SAMPLER_2D_ARRAY:
			return UniformType.Sampler2DArray;
		case GL2GL3.GL_SAMPLER_1D_ARRAY_SHADOW:
			return UniformType.Sampler1DArrayShadow;
		case GL2ES3.GL_SAMPLER_2D_ARRAY_SHADOW:
			return UniformType.Sampler2DArrayShadow;
		case GL2ES3.GL_SAMPLER_CUBE_SHADOW:
			return UniformType.SamplerCubeShadow;
		case GL2GL3.GL_INT_SAMPLER_1D:
			return UniformType.IntSampler1D;
		case GL2ES3.GL_INT_SAMPLER_2D:
			return UniformType.IntSampler2D;
		case GL2GL3.GL_INT_SAMPLER_2D_RECT:
			return UniformType.IntSampler2DRectangle;
		case GL2ES3.GL_INT_SAMPLER_3D:
			return UniformType.IntSampler3D;
		case GL2ES3.GL_INT_SAMPLER_CUBE:
			return UniformType.IntSamplerCube;
		case GL2GL3.GL_INT_SAMPLER_1D_ARRAY:
			return UniformType.IntSampler1DArray;
		case GL2ES3.GL_INT_SAMPLER_2D_ARRAY:
			return UniformType.IntSampler2DArray;
		case GL2GL3.GL_UNSIGNED_INT_SAMPLER_1D:
			return UniformType.UnsignedIntSampler1D;
		case GL2ES3.GL_UNSIGNED_INT_SAMPLER_2D:
			return UniformType.UnsignedIntSampler2D;
		case GL2GL3.GL_UNSIGNED_INT_SAMPLER_2D_RECT:
			return UniformType.UnsignedIntSampler2DRectangle;
		case GL2ES3.GL_UNSIGNED_INT_SAMPLER_3D:
			return UniformType.UnsignedIntSampler3D;
		case GL2ES3.GL_UNSIGNED_INT_SAMPLER_CUBE:
			return UniformType.UnsignedIntSamplerCube;
		case GL2GL3.GL_UNSIGNED_INT_SAMPLER_1D_ARRAY:
			return UniformType.UnsignedIntSampler1DArray;
		}

		throw new IllegalArgumentException("type");
	}

	public static BufferHint toBufferHint(int hint) {
		switch (hint) {
		case GL2ES2.GL_STREAM_DRAW:
			return BufferHint.StreamDraw;
		case GL2ES3.GL_STREAM_READ:
			return BufferHint.StreamRead;
		case GL2ES3.GL_STREAM_COPY:
			return BufferHint.StreamCopy;
		case GL.GL_STATIC_DRAW:
			return BufferHint.StaticDraw;
		case GL2ES3.GL_STATIC_READ:
			return BufferHint.StaticRead;
		case GL2ES3.GL_STATIC_COPY:
			return BufferHint.StaticCopy;
		case GL.GL_DYNAMIC_DRAW:
			return BufferHint.DynamicDraw;
		case GL2ES3.GL_DYNAMIC_READ:
			return BufferHint.DynamicRead;
		case GL2ES3.GL_DYNAMIC_COPY:
			return BufferHint.DynamicCopy;
		}

		throw new IllegalArgumentException("type");
	}

	public static int to(BufferHint hint) {
		switch (hint) {
		case StreamDraw:
			return GL2ES2.GL_STREAM_DRAW;
		case StreamRead:
			return GL2ES3.GL_STREAM_READ;
		case StreamCopy:
			return GL2ES3.GL_STREAM_COPY;
		case StaticDraw:
			return GL.GL_STATIC_DRAW;
		case StaticRead:
			return GL2ES3.GL_STATIC_READ;
		case StaticCopy:
			return GL2ES3.GL_STATIC_COPY;
		case DynamicDraw:
			return GL.GL_DYNAMIC_DRAW;
		case DynamicRead:
			return GL2ES3.GL_DYNAMIC_READ;
		case DynamicCopy:
			return GL2ES3.GL_DYNAMIC_COPY;
		}

		throw new IllegalArgumentException("type");
	}

	public static int to(ComponentDatatype type) {
		switch (type) {
		case Byte:
			return GL.GL_BYTE;
		case Short:
			return GL.GL_SHORT;
		case Int:
			return GL2ES2.GL_INT;
		case Float:
			return GL.GL_FLOAT;
		}

		throw new IllegalArgumentException("type");
	}

	public static int to(PrimitiveType type) {
		switch (type) {
		case Points:
			return GL.GL_POINTS;
		case Lines:
			return GL.GL_LINES;
		case LineLoop:
			return GL.GL_LINE_LOOP;
		case LineStrip:
			return GL.GL_LINE_STRIP;
		case Triangles:
			return GL.GL_TRIANGLES;
		case TriangleStrip:
			return GL.GL_TRIANGLE_STRIP;
		case LinesAdjacency:
			return GL3.GL_LINES_ADJACENCY_ARB;
		case LineStripAdjacency:
			return GL3.GL_LINE_STRIP_ADJACENCY_ARB;
		case TrianglesAdjacency:
			return GL3.GL_TRIANGLES_ADJACENCY_ARB;
		case TriangleStripAdjacency:
			return GL3.GL_TRIANGLE_STRIP_ADJACENCY_ARB;
		case TriangleFan:
			return GL.GL_TRIANGLE_FAN;
		case Quads:
			return GL2GL3.GL_QUADS;
		}

		throw new IllegalArgumentException("type");
	}

	public static int to(IndexBufferDatatype type) {
		switch (type) {
		case Int:
			return GL.GL_UNSIGNED_INT;
		}

		throw new IllegalArgumentException("type");
	}

	public static int to(DepthTestFunction function) {
		switch (function) {
		case Never:
			return GL.GL_NEVER;
		case Less:
			return GL.GL_LESS;
		case Equal:
			return GL.GL_EQUAL;
		case LessThanOrEqual:
			return GL.GL_LEQUAL;
		case Greater:
			return GL.GL_GREATER;
		case NotEqual:
			return GL.GL_NOTEQUAL;
		case GreaterThanOrEqual:
			return GL.GL_GEQUAL;
		case Always:
			return GL.GL_ALWAYS;
		}

		throw new IllegalArgumentException("function");
	}

	public static int to(AlphaTestFunction function) {
		switch (function) {
		case Never:
			return GL.GL_NEVER;
		case Less:
			return GL.GL_LESS;
		case Equal:
			return GL.GL_EQUAL;
		case LessThanOrEqual:
			return GL.GL_LEQUAL;
		case Greater:
			return GL.GL_GREATER;
		case NotEqual:
			return GL.GL_NOTEQUAL;
		case GreaterThanOrEqual:
			return GL.GL_GEQUAL;
		case Always:
			return GL.GL_ALWAYS;
		}

		throw new IllegalArgumentException("function");
	}

	public static int to(CullFace face) {
		switch (face) {
		case Front:
			return GL.GL_FRONT;
		case Back:
			return GL.GL_BACK;
		case FrontAndBack:
			return GL.GL_FRONT_AND_BACK;
		}

		throw new IllegalArgumentException("face");
	}

	public static int to(WindingOrder windingOrder) {
		switch (windingOrder) {
		case Clockwise:
			return GL.GL_CW;
		case Counterclockwise:
			return GL.GL_CCW;
		}

		throw new IllegalArgumentException("windingOrder");
	}

	public static int to(RasterizationMode mode) {
		switch (mode) {
		case Point:
			return GL2GL3.GL_POINT;
		case Line:
			return GL2GL3.GL_LINE;
		case Fill:
			return GL2GL3.GL_FILL;
		}

		throw new IllegalArgumentException("mode");
	}

	public static int to(StencilOperation operation) {
		switch (operation) {
		case Zero:
			return GL.GL_ZERO;
		case Invert:
			return GL.GL_INVERT;
		case Keep:
			return GL.GL_KEEP;
		case Replace:
			return GL.GL_REPLACE;
		case Increment:
			return GL.GL_INCR;
		case Decrement:
			return GL.GL_DECR;
		case IncrementWrap:
			return GL.GL_INCR_WRAP;
		case DecrementWrap:
			return GL.GL_DECR_WRAP;
		}

		throw new IllegalArgumentException("operation");
	}

	public static int to(StencilTestFunction function) {
		switch (function) {
		case Never:
			return GL.GL_NEVER;
		case Less:
			return GL.GL_LESS;
		case Equal:
			return GL.GL_EQUAL;
		case LessThanOrEqual:
			return GL.GL_LEQUAL;
		case Greater:
			return GL.GL_GREATER;
		case NotEqual:
			return GL.GL_NOTEQUAL;
		case GreaterThanOrEqual:
			return GL.GL_GEQUAL;
		case Always:
			return GL.GL_ALWAYS;
		}

		throw new IllegalArgumentException("function");
	}

	public static int to(BlendEquation equation) {
		switch (equation) {
		case Add:
			return GL.GL_FUNC_ADD;
		case Minimum:
			return GL2ES3.GL_MIN;
		case Maximum:
			return GL2ES3.GL_MAX;
		case Subtract:
			return GL.GL_FUNC_SUBTRACT;
		case ReverseSubtract:
			return GL.GL_FUNC_REVERSE_SUBTRACT;
		}

		throw new IllegalArgumentException("equation");
	}

	public static int to(SourceBlendingFactor factor) {
		switch (factor) {
		case Zero:
			return GL.GL_ZERO;
		case One:
			return GL.GL_ONE;
		case SourceAlpha:
			return GL.GL_SRC_ALPHA;
		case OneMinusSourceAlpha:
			return GL.GL_ONE_MINUS_SRC_ALPHA;
		case DestinationAlpha:
			return GL.GL_DST_ALPHA;
		case OneMinusDestinationAlpha:
			return GL.GL_ONE_MINUS_DST_ALPHA;
		case DestinationColor:
			return GL.GL_DST_COLOR;
		case OneMinusDestinationColor:
			return GL.GL_ONE_MINUS_DST_COLOR;
		case SourceAlphaSaturate:
			return GL.GL_SRC_ALPHA_SATURATE;
		case ConstantColor:
			return GL2ES2.GL_CONSTANT_COLOR;
		case OneMinusConstantColor:
			return GL2ES2.GL_ONE_MINUS_CONSTANT_COLOR;
		case ConstantAlpha:
			return GL2ES2.GL_CONSTANT_ALPHA;
		case OneMinusConstantAlpha:
			return GL2ES2.GL_ONE_MINUS_CONSTANT_ALPHA;
		}

		throw new IllegalArgumentException("factor");
	}

	public static int to(DestinationBlendingFactor factor) {
		switch (factor) {
		case Zero:
			return GL.GL_ZERO;
		case One:
			return GL.GL_ONE;
		case SourceColor:
			return GL.GL_SRC_COLOR;
		case OneMinusSourceColor:
			return GL.GL_ONE_MINUS_SRC_COLOR;
		case SourceAlpha:
			return GL.GL_SRC_ALPHA;
		case OneMinusSourceAlpha:
			return GL.GL_ONE_MINUS_SRC_ALPHA;
		case DestinationAlpha:
			return GL.GL_DST_ALPHA;
		case OneMinusDestinationAlpha:
			return GL.GL_ONE_MINUS_DST_ALPHA;
		case DestinationColor:
			return GL.GL_DST_COLOR;
		case OneMinusDestinationColor:
			return GL.GL_ONE_MINUS_DST_COLOR;
		case ConstantColor:
			return GL2ES2.GL_CONSTANT_COLOR;
		case OneMinusConstantColor:
			return GL2ES2.GL_ONE_MINUS_CONSTANT_COLOR;
		case ConstantAlpha:
			return GL2ES2.GL_CONSTANT_ALPHA;
		case OneMinusConstantAlpha:
			return GL2ES2.GL_ONE_MINUS_CONSTANT_ALPHA;
		}

		throw new IllegalArgumentException("factor");
	}

	public static int to(TextureMinificationFilter filter) {
		switch (filter) {
		case Nearest:
			return GL.GL_NEAREST;
		case Linear:
			return GL.GL_LINEAR;
		case NearestMipmapNearest:
			return GL.GL_NEAREST_MIPMAP_NEAREST;
		case LinearMipmapNearest:
			return GL.GL_LINEAR_MIPMAP_NEAREST;
		case NearestMipmapLinear:
			return GL.GL_NEAREST_MIPMAP_LINEAR;
		case LinearMipmapLinear:
			return GL.GL_LINEAR_MIPMAP_LINEAR;
		}

		throw new IllegalArgumentException("filter");
	}

	public static int to(TextureMagnificationFilter filter) {
		switch (filter) {
		case Nearest:
			return GL.GL_NEAREST;
		case Linear:
			return GL.GL_LINEAR;
		}

		throw new IllegalArgumentException("filter");
	}

	public static int to(TextureWrap wrap) {
		switch (wrap) {
		case Clamp:
			return GL.GL_CLAMP_TO_EDGE;
		case Repeat:
			return GL.GL_REPEAT;
		case MirroredRepeat:
			return GL.GL_MIRRORED_REPEAT;
		}

		throw new IllegalArgumentException("wrap");
	}
}