package fr.ifremer.globe.ogl.renderer.jogl.vertexarray;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.Device2x;

public class VertexBufferAttributesGL2x extends VertexBufferAttributesGLBase {
	public VertexBufferAttributesGL2x() {
		super();
	}

	@Override
	protected void createAttributes() {
		_attributes = new VertexBufferAttributeGL2x3x[Device2x.instance.getMaximumNumberOfVertexAttributes()];
		for (int i = 0; i < _attributes.length; i++) {
			_attributes[i] = new VertexBufferAttributeGL2x3x();
		}
	}




	@Override
	protected GL2GL3 getGL() {
		return GLU.getCurrentGL().getGL2();
	}

}