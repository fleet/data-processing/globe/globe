package fr.ifremer.globe.ogl.renderer.jogl.textures;

import fr.ifremer.globe.ogl.renderer.textures.TextureMagnificationFilter;
import fr.ifremer.globe.ogl.renderer.textures.TextureMinificationFilter;
import fr.ifremer.globe.ogl.renderer.textures.TextureSampler;
import fr.ifremer.globe.ogl.renderer.textures.TextureWrap;

public class TextureSamplerGL2xGL3 extends TextureSampler {
	public TextureSamplerGL2xGL3(TextureMinificationFilter minificationFilter, TextureMagnificationFilter magnificationFilter, TextureWrap wrapS, TextureWrap wrapT, float maximumAnistropy) {
		super(minificationFilter, magnificationFilter, wrapS, wrapT, maximumAnistropy);

		// NOT SUPPORTED
		// if (Device.getInstance().getExtensions().getAnisotropicFiltering())
		// {
		// GL.SamplerParameter(_name.getValue(),
		// (ArbSamplerObjects)All.TextureMaxAnisotropyExt, maximumAnistropy);
		// }
		// else
		// {
		// if (maximumAnistropy != 1)
		// {
		// throw new
		// InsufficientVideoCardException("Anisotropic filtering is not supported.  The extension GL_EXT_texture_filter_anisotropic was not found.");
		// }
		// }
	}

	protected void dispose(boolean disposing) {
		if (disposing) {

		}

	}

}