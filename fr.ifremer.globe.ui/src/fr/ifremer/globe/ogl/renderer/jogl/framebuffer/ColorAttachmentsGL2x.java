package fr.ifremer.globe.ogl.renderer.jogl.framebuffer;

import fr.ifremer.globe.ogl.renderer.Device2x;

public class ColorAttachmentsGL2x extends ColorAttachmentsGLBase {

	@Override
	public int getMaximumNumberOfColorAttachments()
	{
		return  Device2x.getMaximumNumberOfColorAttachments();
	}
}