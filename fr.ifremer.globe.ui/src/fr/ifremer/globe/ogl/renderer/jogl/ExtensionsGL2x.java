package fr.ifremer.globe.ogl.renderer.jogl;

import fr.ifremer.globe.ogl.renderer.Extensions;

public class ExtensionsGL2x extends Extensions {

	public ExtensionsGL2x() {
//		final GL2 gl = GLU.getCurrentGL().getGL2();

		_anisotropicFiltering = false;
	//	int numberOfExtensions;
		// GL.GetInteger(GetPName.NumExtensions, out numberOfExtensions);
		// Cette propriété se trouve dans la classe JOGL GL2GL3 qui ne
		// semble pas présente dans cette version de JOGL
	//	numberOfExtensions = 0;

		// Convertir
		// for (int i = 0; i < numberOfExtensions; ++i) {
		// if (GL.GetString(StringName.Extensions, i) ==
		// "GL_EXT_texture_filter_anisotropic") {
		// _anisotropicFiltering = true;
		// break;
		// }
		// }
	}

	@Override
	public boolean getAnisotropicFiltering() {
		return _anisotropicFiltering;
	}

	private boolean _anisotropicFiltering;
}