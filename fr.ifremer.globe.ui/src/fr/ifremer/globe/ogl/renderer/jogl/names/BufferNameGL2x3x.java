package fr.ifremer.globe.ogl.renderer.jogl.names;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

/**
 * Buffer object names.<br>
 * Names are 
 */
public class BufferNameGL2x3x {
	public BufferNameGL2x3x() {
		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
		// Specifies the number of buffer object names to be generated : only 1
		gl.glGenBuffers(1, _value, 0);

	}

	public void dispose() {
		dispose(true);

	}

	public int[] getValue() {
		return _value;
	}

	private void dispose(boolean disposing) {
		if (_value[0] != 0) {
			GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
			gl.glDeleteBuffers(1, _value, 0);

			_value[0] = 0;
		}
	}

	/** Array in which the generated buffer object names are stored. */
	private int[] _value = new int[1];
}