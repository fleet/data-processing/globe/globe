package fr.ifremer.globe.ogl.renderer.jogl.textures;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GL2GL3;

import fr.ifremer.globe.ogl.renderer.jogl.ICleanable;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;
import fr.ifremer.globe.ogl.renderer.textures.TextureUnit;
import fr.ifremer.globe.ogl.renderer.textures.TextureUnits;

public abstract class TextureUnitsGLBase<TextureUnitType extends TextureUnitGLBase> extends TextureUnits implements ICleanableObserver {
	protected abstract GL2GL3 getGL();
	abstract TextureUnitType create(int i);

	@SuppressWarnings("unchecked")
	public TextureUnitsGLBase() {
		//
		// Device.NumberOfTextureUnits is not initialized yet.
		//

		GL2GL3 gl = getGL();
		IntBuffer result = IntBuffer.allocate(1);
		gl.glGetIntegerv(GL2ES2.GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, result);
		int numberOfTextureUnits = result.get(0);

		_textureUnits = new ArrayList<TextureUnit>();
		for (int i = 0; i < numberOfTextureUnits; ++i) {
			TextureUnitType textureUnit = create(i);
			_textureUnits.add(textureUnit);
		}
		_dirtyTextureUnits = new ArrayList<ICleanable>();
		_lastTextureUnit = (TextureUnitType) _textureUnits.get(numberOfTextureUnits - 1);
	}

	@Override
	public TextureUnit get(int index) {
		return _textureUnits.get(index);
	}

	@Override
	public int size() {
		return _textureUnits.size();
	}

	@Override
	public Iterator<TextureUnit> iterator() {
		return _textureUnits.iterator();
	}

	@Override
	public void clean() {
		for (int i = 0; i < _dirtyTextureUnits.size(); ++i) {
			_dirtyTextureUnits.get(i).clean();
		}
		_dirtyTextureUnits.clear();
		_lastTextureUnit.cleanLastTextureUnit();
	}

	@Override
	public void notifyDirty(ICleanable value) {
		_dirtyTextureUnits.add(value);
	}

	private List<TextureUnit> _textureUnits;
	private List<ICleanable> _dirtyTextureUnits;
	private TextureUnitType _lastTextureUnit;
}