package fr.ifremer.globe.ogl.renderer.jogl.buffers;

import java.nio.FloatBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

import com.jogamp.common.nio.Buffers;

/**
 * Class representing a CPU-RAM buffer intended to be transferred into GPU-RAM.
 * Elements can be vertex coordinates, texture coordinates...etc.
 * 
 * @author apside
 */
public class FloatVertexBuffer {

	/** Name of the Open GL buffer */
	private int[] bufferName;

	/** Buffer */
	private FloatBuffer buffer;

	/** Size of elements */
	private int verticeSize;

	/**
	 * Builds a new pre-allocated buffer. Elements may be vertices coordinates, texture coordinates...etc. For instance,
	 * when storing vertices, $verticeSize should be 2 (x,y) or 3 (x,y,z) or 4 (x,y,z,color) ...
	 * 
	 * @param verticeSize
	 *            Size of elements.
	 * @param size
	 *            Number of elements (size of the buffer = $size * elementSize).
	 */
	public FloatVertexBuffer(int verticeSize, int size) {
		this.verticeSize = verticeSize;
		buffer = Buffers.newDirectFloatBuffer(size * verticeSize);
	}

	/**
	 * Builds a new empty buffer. Elements may be vertices coordinates, texture coordinates...etc. For instance, when
	 * storing vertices, $elementSize should be 2 (x,y) or 3 (x,y,z) or 4 (x,y,z,color) ...
	 * 
	 * @param verticeSize
	 *            Size of elements.
	 */
	public FloatVertexBuffer(int verticeSize) {
		this.verticeSize = verticeSize;
	}

	/**
	 * Allocates a new float buffer in CPU RAM.
	 * 
	 * @param size
	 *            Size of the buffer.
	 */
	public void allocate(int size) {
		if (buffer != null) {
			buffer.clear();
		}
		buffer = Buffers.newDirectFloatBuffer(size * verticeSize);
	}

	/**
	 * Add a value to buffer.
	 * 
	 * @param x
	 */
	public void put(float v) {
		buffer.put(v);
	}

	public FloatBuffer getBuffer()
	{
		return buffer;
	}

	/**
	 * Add a values to buffer
	 * 
	 * @param values
	 */
	public void put(float[] values) {
		if (values.length != this.verticeSize) {
			throw new IllegalArgumentException("Invalid array length. Expected: " + this.verticeSize);
		}
		buffer.put(values);
	}



	public int getVerticeSize() {
		return verticeSize;
	}

	public int getBufferName() {
		return bufferName[0];
	}

	public void clear() {
		if(buffer!=null)
			buffer.clear();
	}

	public float peek() {
		return buffer.get();
	}

	public void rewind() {
		buffer.rewind();
	}

	/**
	 * Bind the buffer into GPU-RAM and loads the float buffer data into the new allocated GPU-RAM.
	 * 
	 * @param gl
	 *            The GL context.
	 */
	public void sendToGpu(GL gl1) {
		GL2 gl = gl1.getGL2();

		buffer.rewind();

		delete(gl);
		bufferName = new int[1];
		// Get a valid name
		gl.glGenBuffers(1, bufferName, 0);
		// Bind the buffer
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, bufferName[0]);
		// Load the data
		gl.glBufferData(GL.GL_ARRAY_BUFFER, buffer.capacity() * Buffers.SIZEOF_FLOAT, buffer, GL.GL_STATIC_DRAW);
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
	}

	public void delete(GL gl1) {
		GL2 gl = gl1.getGL2();
		if (bufferName != null) {
			gl.glDeleteBuffers(1, bufferName, 0);
			bufferName = null;
		}
	}

}