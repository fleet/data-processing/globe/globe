package fr.ifremer.globe.ogl.renderer.jogl.vertexarray;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.Device3x;

public class VertexBufferAttributesGL3x extends VertexBufferAttributesGLBase {
	public VertexBufferAttributesGL3x() {
		super();
	}




	@Override
	protected void createAttributes() {
		_attributes = new VertexBufferAttributeGL2x3x[Device3x.instance.getMaximumNumberOfVertexAttributes()];
		for (int i = 0; i < _attributes.length; i++) {
			_attributes[i] = new VertexBufferAttributeGL2x3x();
		}
	}




	@Override
	protected GL2GL3 getGL() {
		return GLU.getCurrentGL().getGL3();
	}
}