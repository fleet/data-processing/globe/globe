package fr.ifremer.globe.ogl.renderer.jogl.textures;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.Device3x;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;
import fr.ifremer.globe.ogl.renderer.textures.TextureSampler;

public class TextureUnitGL3x extends TextureUnitGLBase {


	public TextureUnitGL3x(int index, ICleanableObserver observer) {
		super(index, observer);
	}

	@Override
	protected GL2GL3 getGL() {
		return  GLU.getCurrentGL().getGL3();
	}

	@Override
	protected TextureSampler getDefaultSampler() {
		return Device3x.getTextureSamplers().getLinearClamp();
	}

}