package fr.ifremer.globe.ogl.renderer.jogl.names;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import org.eclipse.ui.services.IDisposable;

public class FramebufferNameGL2x3x implements IDisposable {
	public FramebufferNameGL2x3x() {

		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
		gl.glGenFramebuffers(1, _value, 0);
	}

	@Override
	public void dispose() {
		dispose(true);

	}

	public int getValue() {
		return _value[0];
	}

	private void dispose(boolean disposing) {
		if (_value[0] != 0) {

			GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
			gl.glDeleteFramebuffers(1, _value, 0);

			_value[0] = 0;
		}
	}

	private int[] _value = new int[1];
}