package fr.ifremer.globe.ogl.renderer.jogl.textures;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;

public class TextureUnitsGL2x extends TextureUnitsGLBase<TextureUnitGL2x> implements ICleanableObserver {

	@Override
	protected GL2 getGL() {
		return GLU.getCurrentGL().getGL2();
	}

	@Override
	TextureUnitGL2x create(int i) {

		return new TextureUnitGL2x(i,this);
	}

}