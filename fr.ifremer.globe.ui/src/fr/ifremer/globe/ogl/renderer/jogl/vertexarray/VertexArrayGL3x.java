package fr.ifremer.globe.ogl.renderer.jogl.vertexarray;

import fr.ifremer.globe.ogl.renderer.Device3x;

public class VertexArrayGL3x extends VertexArrayGLBase {

	@Override
	public VertexBufferAttributesGL3x createVertexBufferAttributes() {
		return new VertexBufferAttributesGL3x();
	}

	@Override
	public int getMaximumNumberOfVertexAttributes() {
		return Device3x.instance.getMaximumNumberOfVertexAttributes();
	}

}