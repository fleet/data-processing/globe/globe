package fr.ifremer.globe.ogl.renderer.jogl.shaders;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.jogl.ICleanable;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.shaders.UniformType;

public class UniformBoolGL2x3x extends Uniform<Boolean> implements ICleanable {
	public UniformBoolGL2x3x(String name, int location, ICleanableObserver observer) {
		super(name, UniformType.Bool);
		_location = location;
		_dirty = true;
		_observer = observer;
		_observer.notifyDirty(this);
	}

	@Override
	public Boolean getValue() {
		return _value;
	}

	@Override
	public void setValue(Boolean value) {
		if (!_dirty && (_value != value)) {
			_dirty = true;
			_observer.notifyDirty(this);
		}

		_value = value;
	}

	@Override
	public void clean() {

		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
		gl.glUniform1i(_location, _value ? GL.GL_TRUE : GL.GL_FALSE);

		_dirty = false;
	}

	private int _location;
	private boolean _value;
	private boolean _dirty;
	private ICleanableObserver _observer;
}