package fr.ifremer.globe.ogl.renderer.jogl.framebuffer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import com.jogamp.opengl.util.texture.Texture;

import fr.ifremer.globe.ogl.renderer.framebuffer.ColorAttachments;

public abstract class ColorAttachmentsGLBase extends ColorAttachments {

	public abstract int getMaximumNumberOfColorAttachments();

	public ColorAttachmentsGLBase() {
		int size = getMaximumNumberOfColorAttachments();
		_colorAttachments = new ArrayList<ColorAttachment>(size);
		for (int i = 0; i < size; i++) {
			_colorAttachments.add(new ColorAttachment());
		}
	}

	@Override
	public Texture get(int index) {
		return _colorAttachments.get(index).getTexture();
	}

	@Override
	public void setTexture2D(int index, Texture value) {

		if (_colorAttachments.get(index).getTexture() != value) {
			if ((_colorAttachments.get(index).getTexture() != null) && (value == null)) {
				--_count;
			} else if ((_colorAttachments.get(index).getTexture() == null) && (value != null)) {
				++_count;
			}

			_colorAttachments.get(index).setTexture(value);
			_colorAttachments.get(index).setDirty(true);
			setDirty(true);
		}
	}

	@Override
	public int size() {
		return _count;
	}

	@Override
	public Iterator<Texture> iterator() {
		return new Iterator<Texture>() {

			private int index = 0;

			@Override
			public boolean hasNext() {
				boolean hasNext = index < _colorAttachments.size();

				if (hasNext) {
					hasNext = false;

					for (int i = index; i < _colorAttachments.size(); i++) {
						hasNext |= (_colorAttachments.get(i) == null);
					}
				}

				return hasNext;
			}

			@Override
			public Texture next() {
				if (index == _colorAttachments.size()) {
					throw new NoSuchElementException();
				}

				Texture result = null;
				do {
					result = _colorAttachments.get(index++).getTexture();
				} while (index < _colorAttachments.size() && result == null);

				return result;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

		};
	}

	public boolean isDirty() {
		return m_Dirty;
	}

	public void setDirty(boolean dirty) {
		m_Dirty = dirty;
	}

	private boolean m_Dirty = false;

	public List<ColorAttachment> getAttachments() {
		return _colorAttachments;
	}

	private List<ColorAttachment> _colorAttachments;
	private int _count;
}