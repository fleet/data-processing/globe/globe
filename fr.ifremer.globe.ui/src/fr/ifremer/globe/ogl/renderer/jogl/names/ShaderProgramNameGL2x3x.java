package fr.ifremer.globe.ogl.renderer.jogl.names;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import org.eclipse.ui.services.IDisposable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShaderProgramNameGL2x3x implements IDisposable {

	private static final Logger LOGGER = LoggerFactory.getLogger(ShaderProgramNameGL2x3x.class);

	public ShaderProgramNameGL2x3x() {

		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
		_value = gl.glCreateProgram();

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Shader Program " + _value + " was generated");
		}
	}

	@Override
	public void dispose() {
		dispose(true);
	}

	public int getValue() {
		return _value;
	}

	private void dispose(boolean disposing) {
		if (_value != 0) {

			GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
			gl.glDeleteProgram(_value);
			_value = 0;
		}
	}

	private int _value;
}