package fr.ifremer.globe.ogl.renderer.jogl.framebuffer;

import fr.ifremer.globe.ogl.renderer.Device3x;

public class ColorAttachmentsGL3x extends ColorAttachmentsGLBase {

	@Override
	public int getMaximumNumberOfColorAttachments()
	{
		return  Device3x.getMaximumNumberOfColorAttachments();
	}
}