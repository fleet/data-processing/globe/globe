package fr.ifremer.globe.ogl.renderer.jogl.vertexarray;

import java.util.Iterator;
import java.util.NoSuchElementException;

import com.jogamp.opengl.GL2GL3;

import fr.ifremer.globe.ogl.renderer.jogl.TypeConverterGL2x3x;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.VertexBufferGL2x3x;
import fr.ifremer.globe.ogl.renderer.vertexarray.ComponentDatatype;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

public abstract class VertexBufferAttributesGLBase extends VertexBufferAttributes {
	public VertexBufferAttributesGLBase() {
		createAttributes();
	}

	protected abstract void createAttributes();
	protected abstract GL2GL3 getGL();
	@Override
	public Iterator<VertexBufferAttribute> iterator() {
		return new Iterator<VertexBufferAttribute>() {

			private int index = 0;

			@Override
			public boolean hasNext() {
				boolean hasNext = index < _attributes.length;

				if (hasNext) {
					hasNext = false;

					for (int i = index; i < _attributes.length; i++) {
						hasNext |= (_attributes[i] == null);
					}
				}

				return hasNext;
			}

			@Override
			public VertexBufferAttribute next() {
				if (index == _attributes.length) {
					throw new NoSuchElementException();
				}
				VertexBufferAttribute result = null;
				do {
					result = _attributes[index++].getVertexBufferAttribute();
				} while (index < _attributes.length && result == null);

				return result;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

		};
	}

	@Override
	public VertexBufferAttribute get(int index) {
		return _attributes[index].getVertexBufferAttribute();
	}

	@Override
	public void set(int index, VertexBufferAttribute value) {
		if (_attributes[index].getVertexBufferAttribute() != value) {
			if (value != null) {
				if (value.getNumberOfComponents() < 1 || value.getNumberOfComponents() > 4) {
					throw new IllegalArgumentException("NumberOfComponents must be between one and four.");
				}

				if (value.getNormalize()) {
					if ((value.getComponentDatatype() != ComponentDatatype.Byte) && (value.getComponentDatatype() != ComponentDatatype.Short)
							&& (value.getComponentDatatype() != ComponentDatatype.Int)
							// NOT SUPPORTED
							// && (value.getComponentDatatype() !=
							// ComponentDatatype.UnsignedInt)
							// && (value.getComponentDatatype() !=
							// ComponentDatatype.UnsignedShort)
							// && (value.getComponentDatatype() !=
							// ComponentDatatype.UnsignedByte)
							) {
						throw new IllegalArgumentException("When Normalize is true, ComponentDatatype must be Byte, UnsignedByte, Short, UnsignedShort, Int, or UnsignedInt.");
					}
				}
			}

			if ((_attributes[index].getVertexBufferAttribute() != null) && (value == null)) {
				--_count;
			} else if ((_attributes[index].getVertexBufferAttribute() == null) && (value != null)) {
				++_count;
			}

			_attributes[index].setVertexBufferAttribute(value);
			_attributes[index].setDirty(true);
			_dirty = true;
		}
	}

	@Override
	public int getCount() {
		return _count;
	}

	@Override
	public int getMaximumCount() {
		return _attributes.length;
	}

	public void clean() {
		if (_dirty) {
			int maximumArrayIndex = 0;

			for (int i = 0; i < _attributes.length; ++i) {
				VertexBufferAttribute attribute = _attributes[i].getVertexBufferAttribute();

				if (_attributes[i].isDirty()) {
					if (attribute != null) {
						attach(i);
					} else {
						detach(i);
					}

					_attributes[i].setDirty(false);
				}

				if (attribute != null) {
					maximumArrayIndex = Math.max(getNumberOfVertices(attribute) - 1, maximumArrayIndex);
				}
			}

			_dirty = false;
			_maximumArrayIndex = maximumArrayIndex;
		}
	}

	private void attach(int index) {
		GL2GL3 gl = getGL();

		gl.glEnableVertexAttribArray(index);

		VertexBufferAttribute attribute = _attributes[index].getVertexBufferAttribute();
		VertexBufferGL2x3x bufferObjectGL = (VertexBufferGL2x3x) attribute.getVertexBuffer();

		bufferObjectGL.bind();

		gl.glVertexAttribPointer(index, attribute.getNumberOfComponents(), TypeConverterGL2x3x.to(attribute.getComponentDatatype()), attribute.getNormalize(), attribute.getStrideInBytes(),
				attribute.getOffsetInBytes());

	}

	public void detach(int index) {
		GL2GL3 gl = getGL();

		gl.glDisableVertexAttribArray(index);

	}

	public int getMaximumArrayIndex() {

		if (_dirty) {
			throw new IllegalStateException("MaximumArrayIndex is not valid until Clean() is called.");
		}

		return _maximumArrayIndex;
	}

	private int getNumberOfVertices(VertexBufferAttribute attribute) {
		return attribute.getVertexBuffer().getSizeInBytes() / attribute.getStrideInBytes();
	}

	public void dirty() {
		_dirty = true;
		for (VertexBufferAttributeGL2x3x vbattr : _attributes) {
			vbattr.setDirty(true);
		}
	}

	protected VertexBufferAttributeGL2x3x[] _attributes;
	private int _count;
	private int _maximumArrayIndex;
	private boolean _dirty;
}