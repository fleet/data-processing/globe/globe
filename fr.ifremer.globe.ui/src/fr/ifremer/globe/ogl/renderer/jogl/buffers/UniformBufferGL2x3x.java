package fr.ifremer.globe.ogl.renderer.jogl.buffers;

import java.nio.ByteBuffer;

import com.jogamp.opengl.GL2;

import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.buffers.UniformBuffer;
import fr.ifremer.globe.ogl.renderer.jogl.names.BufferNameGL2x3x;

public class UniformBufferGL2x3x extends UniformBuffer {
	public UniformBufferGL2x3x(BufferHint usageHint, int sizeInBytes) {
		_bufferObject = new BufferGL2x3x(GL2.GL_UNIFORM_BUFFER_EXT, usageHint, sizeInBytes);
	}

	public BufferNameGL2x3x getHandle() {
		return _bufferObject.getHandle();
	}

	@Override
	public void copyFromSystemMemory(ByteBuffer bufferInSystemMemory, int destinationOffsetInBytes, int lengthInBytes) {
		_bufferObject.copyFromSystemMemory(bufferInSystemMemory, destinationOffsetInBytes, lengthInBytes);
	}

	@Override
	public ByteBuffer copyToSystemMemory(int offsetInBytes, int sizeInBytes) {
		return _bufferObject.copyToSystemMemory(offsetInBytes, sizeInBytes);
	}

	@Override
	public int getSizeInBytes() {
		return _bufferObject.getSizeInBytes();
	}

	@Override
	public BufferHint getUsageHint() {
		return _bufferObject.getUsageHint();
	}

	protected void dispose(boolean disposing) {
		if (disposing) {
			_bufferObject.dispose();
		}

	}

	BufferGL2x3x _bufferObject;
}