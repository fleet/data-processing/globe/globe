package fr.ifremer.globe.ogl.renderer.jogl.shaders;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GL2ES3;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.CouldNotCreateVideoCardResourceException;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanable;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;
import fr.ifremer.globe.ogl.renderer.jogl.TypeConverterGL2x3x;
import fr.ifremer.globe.ogl.renderer.jogl.names.ShaderProgramNameGL2x3x;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.FragmentOutputs;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderProgram;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderVertexAttribute;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderVertexAttributeCollection;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.shaders.UniformBlockCollection;
import fr.ifremer.globe.ogl.renderer.shaders.UniformCollection;
import fr.ifremer.globe.ogl.renderer.shaders.UniformType;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexLocations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.common.nio.Buffers;

public abstract class ShaderProgramGL2x3x extends ShaderProgram implements ICleanableObserver {

	private static final Logger LOGGER = LoggerFactory.getLogger(ShaderProgramGL2x3x.class);

	abstract ShaderObjectGL2x3x createShaderObject(int shaderType, String source) throws CouldNotCreateVideoCardResourceException ;

	public ShaderProgramGL2x3x(String vertexShaderSource, String fragmentShaderSource) throws CouldNotCreateVideoCardResourceException {
		this(vertexShaderSource, "", fragmentShaderSource,true);
	}

	public ShaderProgramGL2x3x(String vertexShaderSource, String geometryShaderSource, String fragmentShaderSource, boolean initializeAutomaticUniform) throws CouldNotCreateVideoCardResourceException {


		_vertexShader = createShaderObject(GL2ES2.GL_VERTEX_SHADER, vertexShaderSource);
		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("Create Vertex Shader program" + vertexShaderSource);
		}

		if (geometryShaderSource.length() > 0) {
			_geometryShader =createShaderObject(GL3.GL_GEOMETRY_SHADER_ARB, geometryShaderSource);
		}
		_fragmentShader =createShaderObject(GL2ES2.GL_FRAGMENT_SHADER, fragmentShaderSource);
		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("Create Fragment Shader program" + fragmentShaderSource);
		}
		_program = new ShaderProgramNameGL2x3x();
		int programHandle = _program.getValue();

		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();

		gl.glAttachShader(programHandle, _vertexShader.getHandle());
		if (geometryShaderSource.length() > 0) {

			LOGGER.debug("Attach Geometry Shader program");
			gl.glAttachShader(programHandle, _geometryShader.getHandle());
		}

		gl.glAttachShader(programHandle, _fragmentShader.getHandle());

		// Replace layout function in GLSL 330+
		gl.glBindAttribLocation(programHandle, VertexLocations.Position, "position");

		gl.glLinkProgram(programHandle);

		IntBuffer result = Buffers.newDirectIntBuffer(1);
		int linkStatus;
		gl.glGetProgramiv(programHandle, GL2ES2.GL_LINK_STATUS, result);
		linkStatus = result.get(0);

		if (linkStatus == 0) {
			throw new CouldNotCreateVideoCardResourceException("Could not link shader program.  Link Log:  \n\n" + getProgramInfoLog());
		}

		_fragmentOutputs = new FragmentOutputsGL2x(_program);
		_vertexAttributes = findVertexAttributes(_program);
		_dirtyUniforms = new ArrayList<ICleanable>();
		_uniforms = findUniforms(_program);
		_uniformBlocks = findUniformBlocks(_program);

		//This function crash the application when displaying a SimpleSceneExample
		if(initializeAutomaticUniform)
			initializeAutomaticUniforms(_uniforms);
	}

	public ShaderProgramGL2x3x(String vertexShaderSource, String geometryShaderSource, String fragmentShaderSource, Boolean example) throws CouldNotCreateVideoCardResourceException {

		_vertexShader =createShaderObject(GL2ES2.GL_VERTEX_SHADER, vertexShaderSource);
		if (geometryShaderSource.length() > 0) {
			_geometryShader=createShaderObject(GL3.GL_GEOMETRY_SHADER_ARB, geometryShaderSource);
		}
		_fragmentShader =createShaderObject(GL2ES2.GL_FRAGMENT_SHADER, fragmentShaderSource);

		_program = new ShaderProgramNameGL2x3x();
		int programHandle = _program.getValue();

		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();

		LOGGER.debug("Attach Vertex Shader program");
		gl.glAttachShader(programHandle, _vertexShader.getHandle());
		if (geometryShaderSource.length() > 0) {

			LOGGER.debug("Attach Geometry Shader program");
			gl.glAttachShader(programHandle, _geometryShader.getHandle());
		}

		LOGGER.debug("Attach Fragment Shader program");
		gl.glAttachShader(programHandle, _fragmentShader.getHandle());

		// Replace layout function in GLSL 330+
		gl.glBindAttribLocation(programHandle, VertexLocations.Position, "position");

		LOGGER.debug("Link Shader program " + programHandle);
		gl.glLinkProgram(programHandle);

		IntBuffer result = Buffers.newDirectIntBuffer(1);
		int linkStatus;
		gl.glGetProgramiv(programHandle, GL2ES2.GL_LINK_STATUS, result);
		linkStatus = result.get(0);

		if (linkStatus == 0) {
			throw new CouldNotCreateVideoCardResourceException("Could not link shader program.  Link Log:  \n\n" + getProgramInfoLog());
		}

		_fragmentOutputs = new FragmentOutputsGL2x(_program);
		_vertexAttributes = findVertexAttributes(_program);
		_dirtyUniforms = new ArrayList<ICleanable>();
		_uniforms = findUniforms(_program);
		_uniformBlocks = findUniformBlocks(_program);

		//This function crash the application when displaying a SimpleSceneExample
		//initializeAutomaticUniforms(_uniforms);
	}

	private static ShaderVertexAttributeCollection findVertexAttributes(ShaderProgramNameGL2x3x program) {
		int programHandle = program.getValue();

		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
		IntBuffer result = Buffers.newDirectIntBuffer(1);

		int numberOfAttributes;
		gl.glGetProgramiv(programHandle, GL2ES2.GL_ACTIVE_ATTRIBUTES, result);
		numberOfAttributes = result.get(0);

		int attributeNameMaxLength;
		gl.glGetProgramiv(programHandle, GL2ES2.GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, result);
		attributeNameMaxLength = result.get(0);

		ShaderVertexAttributeCollection vertexAttributes = new ShaderVertexAttributeCollection();
		for (int i = 0; i < numberOfAttributes; ++i) {
			int[] attributeNameLength = new int[1];
			int[] attributeLength = new int[1];
			int[] attributeType = new int[1];
			byte[] attributeNameBuilder = new byte[attributeNameMaxLength];

			gl.glGetActiveAttrib(programHandle, i, attributeNameMaxLength, attributeNameLength, 0, attributeLength, 0, attributeType, 0, attributeNameBuilder, 0);

			String attributeName = new String(attributeNameBuilder).trim();

			if (attributeName.startsWith("gl_")) {
				//
				// Names starting with the reserved prefix of "gl_" have a
				// location of
				// -1.
				//
				continue;
			}

			int attributeLocation = gl.glGetAttribLocation(programHandle, attributeName);

			vertexAttributes.add(new ShaderVertexAttribute(attributeName, attributeLocation, TypeConverterGL2x3x.toShaderVertexAttributeType(attributeType[0]), attributeLength[0]));
		}

		return vertexAttributes;
	}

	private UniformCollection findUniforms(ShaderProgramNameGL2x3x program) {
		int programHandle = program.getValue();

		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
		IntBuffer result = Buffers.newDirectIntBuffer(1);

		int numberOfUniforms;
		gl.glGetProgramiv(programHandle, GL2ES2.GL_ACTIVE_UNIFORMS, result);
		numberOfUniforms = result.get(0);

		int uniformNameMaxLength;
		gl.glGetProgramiv(programHandle, GL2ES2.GL_ACTIVE_UNIFORM_MAX_LENGTH, result);
		uniformNameMaxLength = result.get(0);

		UniformCollection uniforms = new UniformCollection();
		for (int i = 0; i < numberOfUniforms; ++i) {
			int[] uniformNameLength = new int[1];
			int[] uniformSize = new int[1];
			int[] uniformType = new int[1];
			byte[] uniformNameBuilder = new byte[uniformNameMaxLength];

			gl.glGetActiveUniform(programHandle, i, uniformNameMaxLength, uniformNameLength, 0, uniformSize, 0, uniformType, 0, uniformNameBuilder, 0);

			String uniformName = correctUniformName(new String(uniformNameBuilder).trim());

			if (uniformName.startsWith("gl_")) {
				continue;
			}

			int uniformLocation = gl.glGetUniformLocation(programHandle, uniformName);

			uniforms.add(createUniform(uniformName, uniformLocation, uniformType[0], uniformSize[0]));
		}

		return uniforms;
	}

	private Uniform<?> createUniform(String name, int location, int type, int size) {
		// special case to manage int arrays
		if (size != 1) {
			switch (type) {
			case GL2ES2.GL_INT:
				return new UniformIntArrayGL2x3x(name, location, UniformType.Int, this);
			case GL2ES2.GL_FLOAT:
				return new UniformFloatArrayGL2x3x(name, location, UniformType.Float, this);
			default:
				throw new IllegalArgumentException("Arrays for type " + type + " are not supported.");
			}
		}

		switch (type) {
		case GL.GL_FLOAT:
			return new UniformFloatGL2x3x(name, location, this);
		case GL2ES2.GL_FLOAT_VEC2:
			return new UniformFloatVector2GL2x3x(name, location, this);
		case GL2ES2.GL_FLOAT_VEC3:
			return new UniformFloatVector3GL2x3x(name, location, this);
		case GL2ES2.GL_FLOAT_VEC4:
			return new UniformFloatVector4GL2x3x(name, location, this);
		case GL2ES2.GL_INT:
			return new UniformIntGL2x3x(name, location, UniformType.Int, this);
		case GL2ES2.GL_INT_VEC2:
			return new UniformIntVector2GL2x3x(name, location, this);
		case GL2ES2.GL_INT_VEC3:
			return new UniformIntVector3GL2x3x(name, location, this);
		case GL2ES2.GL_BOOL:
			return new UniformBoolGL2x3x(name, location, this);
		case GL2ES2.GL_FLOAT_MAT4:
			return new UniformFloatMatrix44GL2x3x(name, location, this);
		case GL2ES3.GL_FLOAT_MAT4x2:
			return new UniformFloatMatrix42GL2x3x(name, location, this);
		case GL2GL3.GL_SAMPLER_1D:
		case GL2ES2.GL_SAMPLER_2D:
		case GL2.GL_SAMPLER_2D_RECT_ARB:
		case GL2.GL_SAMPLER_2D_RECT_SHADOW_ARB:
		case GL2ES2.GL_SAMPLER_3D:
		case GL2ES2.GL_SAMPLER_CUBE:
		case GL2GL3.GL_SAMPLER_1D_SHADOW:
		case GL2ES2.GL_SAMPLER_2D_SHADOW:
		case GL2GL3.GL_SAMPLER_1D_ARRAY:
		case GL3.GL_SAMPLER_2D_ARRAY:
		case GL2GL3.GL_SAMPLER_1D_ARRAY_SHADOW:
		case GL2ES3.GL_SAMPLER_2D_ARRAY_SHADOW:
		case GL2ES3.GL_SAMPLER_CUBE_SHADOW:
		case GL2GL3.GL_INT_SAMPLER_1D:
		case GL2ES3.GL_INT_SAMPLER_2D:
		case GL2GL3.GL_INT_SAMPLER_2D_RECT:
		case GL2ES3.GL_INT_SAMPLER_3D:
		case GL2ES3.GL_INT_SAMPLER_CUBE:
		case GL2GL3.GL_INT_SAMPLER_1D_ARRAY:
		case GL2ES3.GL_INT_SAMPLER_2D_ARRAY:
		case GL2GL3.GL_UNSIGNED_INT_SAMPLER_1D:
		case GL2ES3.GL_UNSIGNED_INT_SAMPLER_2D:
		case GL2GL3.GL_UNSIGNED_INT_SAMPLER_2D_RECT:
		case GL2ES3.GL_UNSIGNED_INT_SAMPLER_3D:
		case GL2ES3.GL_UNSIGNED_INT_SAMPLER_CUBE:
		case GL2GL3.GL_UNSIGNED_INT_SAMPLER_1D_ARRAY:
		case GL2ES3.GL_UNSIGNED_INT_SAMPLER_2D_ARRAY:
			return new UniformIntGL2x3x(name, location, TypeConverterGL2x3x.toUniformType(type), this);
			// NOT SUPPORTED
			// case GL2.GL_INT_VEC4:
			// return new UniformIntVector4GL3x(name, location, this);
			// case GL2.GL_BOOL_VEC2:
			// return new UniformBoolVector2GL3x(name, location, this);
			// case GL2.GL_BOOL_VEC3:
			// return new UniformBoolVector3GL3x(name, location, this);
			// case GL2.GL_BOOL_VEC4:
			// return new UniformBoolVector4GL3x(name, location, this);
			// case GL2.GL_FLOAT_MAT2:
			// return new UniformFloatMatrix22GL3x(name, location, this);
			// case GL2.GL_FLOAT_MAT3:
			// return new UniformFloatMatrix33GL3x(name, location, this);
			// case GL2.GL_FLOAT_MAT2x3:
			// return new UniformFloatMatrix23GL3x(name, location, this);
			// case GL2.GL_FLOAT_MAT2x4:
			// return new UniformFloatMatrix24GL3x(name, location, this);
			// case GL2.GL_FLOAT_MAT3x2:
			// return new UniformFloatMatrix32GL3x(name, location, this);
			// case GL2.GL_FLOAT_MAT3x4:
			// return new UniformFloatMatrix34GL3x(name, location, this);
			// case GL2.GL_FLOAT_MAT4x3:
			// return new UniformFloatMatrix43GL3x(name, location, this);
		}

		//
		// A new Uniform derived class needs to be added to support this uniform
		// type.
		//
		throw new IllegalArgumentException("An implementation for uniform type " + type + " does not exist.");
	}

	private static UniformBlockCollection findUniformBlocks(ShaderProgramNameGL2x3x program) {
		UniformBlockCollection uniformBlocks = new UniformBlockCollection();

		// NOT SUPPORTED
		// int programHandle = program.getValue();
		//
		// int numberOfUniformBlocks;
		// GL.GetProgram(programHandle, ProgramParameter.ActiveUniformBlocks,
		// out
		// numberOfUniformBlocks);
		//
		// for (int i = 0; i < numberOfUniformBlocks; ++i)
		// {
		// String uniformBlockName = GL.GetActiveUniformBlockName(programHandle,
		// i);
		//
		// int uniformBlockSizeInBytes;
		// GL.GetActiveUniformBlock(programHandle, i,
		// ActiveUniformBlockParameter.UniformBlockDataSize, out
		// uniformBlockSizeInBytes);
		//
		// int numberOfUniformsInBlock;
		// GL.GetActiveUniformBlock(programHandle, i,
		// ActiveUniformBlockParameter.UniformBlockActiveUniforms, out
		// numberOfUniformsInBlock);
		//
		// int[] uniformIndicesInBlock = new int[numberOfUniformsInBlock];
		// GL.GetActiveUniformBlock(programHandle, i,
		// ActiveUniformBlockParameter.UniformBlockActiveUniformIndices,
		// uniformIndicesInBlock);
		//
		// //
		// // Query uniforms in this named uniform block
		// //
		// int[] uniformTypes = new int[numberOfUniformsInBlock];
		// int[] uniformOffsetsInBytes = new int[numberOfUniformsInBlock];
		// int[] uniformLengths = new int[numberOfUniformsInBlock];
		// int[] uniformArrayStridesInBytes = new int[numberOfUniformsInBlock];
		// int[] uniformmatrixStrideInBytess = new int[numberOfUniformsInBlock];
		// int[] uniformRowMajors = new int[numberOfUniformsInBlock];
		// GL.GetActiveUniforms(programHandle, numberOfUniformsInBlock,
		// uniformIndicesInBlock, ActiveUniformParameter.UniformType,
		// uniformTypes);
		// GL.GetActiveUniforms(programHandle, numberOfUniformsInBlock,
		// uniformIndicesInBlock, ActiveUniformParameter.UniformOffset,
		// uniformOffsetsInBytes);
		// GL.GetActiveUniforms(programHandle, numberOfUniformsInBlock,
		// uniformIndicesInBlock, ActiveUniformParameter.UniformSize,
		// uniformLengths);
		// GL.GetActiveUniforms(programHandle, numberOfUniformsInBlock,
		// uniformIndicesInBlock, ActiveUniformParameter.UniformArrayStride,
		// uniformArrayStridesInBytes);
		// GL.GetActiveUniforms(programHandle, numberOfUniformsInBlock,
		// uniformIndicesInBlock, ActiveUniformParameter.UniformMatrixStride,
		// uniformmatrixStrideInBytess);
		// GL.GetActiveUniforms(programHandle, numberOfUniformsInBlock,
		// uniformIndicesInBlock, ActiveUniformParameter.UniformIsRowMajor,
		// uniformRowMajors);
		//
		// UniformBlock uniformBlock = new UniformBlockGL3x(uniformBlockName,
		// uniformBlockSizeInBytes, i);
		//
		// for (int j = 0; j < numberOfUniformsInBlock; ++j)
		// {
		// String uniformName = GL.GetActiveUniformName(programHandle,
		// uniformIndicesInBlock[j]);
		// uniformName = CorrectUniformName(uniformName);
		//
		// UniformType uniformType =
		// TypeConverterGL3x.To((ActiveUniformType)uniformTypes[j]);
		//
		// uniformBlock.Members.Add(CreateUniformBlockMember(uniformName,
		// uniformType, uniformOffsetsInBytes[j], uniformLengths[j],
		// uniformArrayStridesInBytes[j],
		// uniformmatrixStrideInBytess[j], uniformRowMajors[j]));
		// }
		//
		// uniformBlocks.Add(uniformBlock);
		//
		// //
		// // Create a one to one mapping between uniform blocks and uniform
		// buffer
		// objects.
		// //
		// GL.UniformBlockBinding(programHandle, i, i);
		// }

		return uniformBlocks;
	}

	// NOT SUPPORTED
	// public static UniformBlockMember createUniformBlockMember(String name,
	// UniformType type, int offsetInBytes, int length, int arrayStrideInBytes,
	// int matrixStrideInBytes, int rowMajor) {
	// if (length == 1) {
	// if (!isMatrix(type)) {
	// //
	// // Non array, non matrix member
	// //
	// return new UniformBlockMember(name, type, offsetInBytes);
	// }
	// else {
	// //
	// // Non array, matrix member
	// //
	// return new UniformBlockMatrixMember(name, type, offsetInBytes,
	// matrixStrideInBytes, Convert.ToBoolean(rowMajor));
	// }
	// }
	// else {
	// if (!isMatrix(type)) {
	// //
	// // Array, non matrix member
	// //
	// return new UniformBlockArrayMember(name, type, offsetInBytes, length,
	// arrayStrideInBytes);
	// }
	// else {
	// //
	// // Array, matrix member
	// //
	// return new UniformBlockMatrixArrayMember(name, type, offsetInBytes,
	// length, arrayStrideInBytes, matrixStrideInBytes,
	// Convert.ToBoolean(rowMajor));
	// }
	// }
	// }

	// private static boolean isMatrix(UniformType type) {
	// return (type == UniformType.FloatMatrix22) || (type ==
	// UniformType.FloatMatrix33) || (type == UniformType.FloatMatrix44) ||
	// (type == UniformType.FloatMatrix23)
	// || (type == UniformType.FloatMatrix24) || (type ==
	// UniformType.FloatMatrix32) || (type == UniformType.FloatMatrix34) ||
	// (type == UniformType.FloatMatrix42)
	// || (type == UniformType.FloatMatrix43);
	// }

	private static String correctUniformName(String name) {
		//
		// On ATI, array uniforms have a [0] suffix
		//
		if (name.endsWith("[0]")) {
			return name.substring(0, name.length() - 3);
		}

		return name;
	}

	public ShaderProgramNameGL2x3x getHandle() {
		return _program;
	}

	public void bind() {

		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();

		gl.glUseProgram(_program.getValue());
	}

	public static void unBind() {
		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();

		gl.glUseProgram(0);
	}

	public void clean(Context context, DrawState drawState, SceneState sceneState) {
		setDrawAutomaticUniforms(context, drawState, sceneState);

		for (int i = 0; i < _dirtyUniforms.size(); ++i) {
			_dirtyUniforms.get(i).clean();
		}
		_dirtyUniforms.clear();
	}

	private String getProgramInfoLog() {

		// return GL.GetProgramInfoLog(_program.getValue());
		GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();

		IntBuffer infoLogLength = Buffers.newDirectIntBuffer(1);
		gl.glGetShaderiv(_program.getValue(), GL2ES2.GL_INFO_LOG_LENGTH, infoLogLength);
		ByteBuffer infoLog = Buffers.newDirectByteBuffer(infoLogLength.get(0));
		gl.glGetProgramInfoLog(_program.getValue(), infoLogLength.get(0), infoLogLength, infoLog);
		return infoLog.toString();
	}

	@Override
	public String getLog() {
		return getProgramInfoLog();
	}

	@Override
	public FragmentOutputs getFragmentOutputs() {
		return _fragmentOutputs;
	}

	@Override
	public ShaderVertexAttributeCollection getVertexAttributes() {
		return _vertexAttributes;
	}

	@Override
	public UniformCollection getUniforms() {
		return _uniforms;
	}

	@Override
	public UniformBlockCollection getUniformBlocks() {
		return _uniformBlocks;
	}

	@Override
	public void notifyDirty(ICleanable value) {
		_dirtyUniforms.add(value);
	}

	@Override
	protected void dispose(boolean disposing) {
		if (disposing) {
			_program.dispose();
			_vertexShader.dispose();
			if (_geometryShader != null) {
				_geometryShader.dispose();
			}
			_fragmentShader.dispose();
		}

	}

	private ShaderObjectGL2x3x _vertexShader;
	private ShaderObjectGL2x3x _geometryShader;
	private ShaderObjectGL2x3x _fragmentShader;
	private ShaderProgramNameGL2x3x _program;
	private FragmentOutputsGL2x _fragmentOutputs;
	private ShaderVertexAttributeCollection _vertexAttributes;
	private List<ICleanable> _dirtyUniforms;
	private UniformCollection _uniforms;
	private UniformBlockCollection _uniformBlocks;
}