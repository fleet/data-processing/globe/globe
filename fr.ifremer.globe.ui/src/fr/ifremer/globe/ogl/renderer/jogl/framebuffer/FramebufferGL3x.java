package fr.ifremer.globe.ogl.renderer.jogl.framebuffer;

import java.util.EnumSet;
import java.util.List;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.glu.GLU;

import com.jogamp.opengl.util.texture.Texture;

import fr.ifremer.globe.ogl.renderer.framebuffer.ColorAttachments;
import fr.ifremer.globe.ogl.renderer.framebuffer.Framebuffer;
import fr.ifremer.globe.ogl.renderer.jogl.names.FramebufferNameGL2x3x;

public class FramebufferGL3x extends Framebuffer {
	public FramebufferGL3x() {
		_name = new FramebufferNameGL2x3x();
		_colorAttachments = new ColorAttachmentsGL3x();
	}

	public void bind() {

		GL3 gl = GLU.getCurrentGL().getGL3();
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, _name.getValue());
	}

	public static void unBind() {

		GL3 gl = GLU.getCurrentGL().getGL3();
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);
	}

	public void clean() {
		if (_colorAttachments.isDirty()) {
			List<ColorAttachment> colorAttachments = _colorAttachments.getAttachments();

			int[] drawBuffers = new int[colorAttachments.size()];

			for (int i = 0; i < colorAttachments.size(); ++i) {
				if (colorAttachments.get(i).isDirty()) {
					attach(GL.GL_COLOR_ATTACHMENT0 + i, colorAttachments.get(i).getTexture());
					colorAttachments.get(i).setDirty(false);
				}

				if (colorAttachments.get(i).getTexture() != null) {
					drawBuffers[i] = GL.GL_COLOR_ATTACHMENT0 + i;
				}
			}

			GL3 gl = GLU.getCurrentGL().getGL3();
			gl.glDrawBuffers(drawBuffers.length, drawBuffers, 0);

			_colorAttachments.setDirty(false);
		}

		if (!_dirtyFlags.isEmpty()) {
			if (_dirtyFlags.contains(DirtyFlags.DepthAttachment)) {
				attach(GL.GL_DEPTH_ATTACHMENT, _depthAttachment);
			}

			// //
			/// The depth-stencil attachment overrides the depth attachment:
			//
			// "Attaching a level of a texture to GL_DEPTH_STENCIL_ATTACHMENT
			// is equivalent to attaching that level to both the
			// GL_DEPTH_ATTACHMENT and the GL_STENCIL_ATTACHMENT attachment
			// points simultaneously."
			//
			// We do not expose just a stencil attachment because
			// TextureFormat
			// // does not contain a stencil only format.
			// //
			if (_dirtyFlags.contains(DirtyFlags.DepthStencilAttachment)) {
				attach(GL.GL_STENCIL_ATTACHMENT,
						_depthStencilAttachment);
			}

			_dirtyFlags.clear();
		}
	}

	@Override
	public ColorAttachments getColorAttachments() {
		return _colorAttachments;
	}

	@Override
	public Texture getDepthAttachment() {
		return _depthAttachment;
	}

	@Override
	public void setDepthAttachment(Texture value) {
		if (_depthAttachment != value) {

			_depthAttachment = value;
			_dirtyFlags.add(DirtyFlags.DepthAttachment);
		}
	};

	public Texture getDepthStencilAttachment() {
		return _depthStencilAttachment;
	}

	public void setDepthStencilAttachment(Texture value) {
		if (_depthStencilAttachment != value) {

			_depthStencilAttachment = value;
			_dirtyFlags.add(DirtyFlags.DepthStencilAttachment);
		}
	};

	public static void attach(int attachPoint, Texture texture) {
		if (texture != null) {

			GL3 gl = GLU.getCurrentGL().getGL3();
			gl.glFramebufferTexture(GL.GL_FRAMEBUFFER, attachPoint, texture.getTextureObject(), 0);
		} else {
			GL3 gl = GLU.getCurrentGL().getGL3();
			gl.glFramebufferTexture(GL.GL_FRAMEBUFFER, attachPoint, 0, 0);
		}
	}

	@Override
	protected void dispose(boolean disposing) {
		if (disposing) {
			_name.dispose();
		}

	}

	private enum DirtyFlags {
		DepthAttachment
		, DepthStencilAttachment
	}

	private FramebufferNameGL2x3x _name;
	private ColorAttachmentsGL3x _colorAttachments;
	private Texture _depthAttachment;
	// NOT SUPPORTED
	private Texture _depthStencilAttachment;
	private EnumSet<DirtyFlags> _dirtyFlags = EnumSet.noneOf(DirtyFlags.class);
}