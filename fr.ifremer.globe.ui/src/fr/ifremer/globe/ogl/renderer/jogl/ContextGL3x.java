package fr.ifremer.globe.ogl.renderer.jogl;

import java.nio.IntBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.GL3;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.core.geometry.Mesh;
import fr.ifremer.globe.ogl.core.geometry.PrimitiveType;
import fr.ifremer.globe.ogl.renderer.Device3x;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.framebuffer.Framebuffer;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.IndexBufferGL3x;
import fr.ifremer.globe.ogl.renderer.jogl.framebuffer.FramebufferGL3x;
import fr.ifremer.globe.ogl.renderer.jogl.shaders.ShaderProgramGL2x3x;
import fr.ifremer.globe.ogl.renderer.jogl.textures.TextureUnitsGL3x;
import fr.ifremer.globe.ogl.renderer.jogl.vertexarray.VertexArrayGL3x;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderVertexAttributeCollection;
import fr.ifremer.globe.ogl.renderer.textures.TextureUnits;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexArray;

import com.jogamp.common.nio.Buffers;

public class ContextGL3x extends ContextGLBase {


	public ContextGL3x(GLCanvas gameWindow) {
		super(gameWindow);

	}

	@Override
	protected  GL3 getGL()
	{
		return GLU.getCurrentGL().getGL3();

	}


	@Override
	public VertexArray createVertexArray() {
		return new VertexArrayGL3x();
	}

	@Override
	public Framebuffer createFramebuffer() {
		return new FramebufferGL3x();
	}


	@Override
	public Framebuffer getFramebuffer() {
		return _setFramebuffer;
	}

	@Override
	public void setFramebuffer(Framebuffer framebuffer) {
		_setFramebuffer = (FramebufferGL3x) framebuffer;
	}




	@Override
	protected void reallyDraw(PrimitiveType primitiveType, DrawState drawState, SceneState sceneState)
	{
		VertexArrayGL3x vertexArray = (VertexArrayGL3x) drawState.getVertexArray();
		IndexBufferGL3x indexBuffer = (IndexBufferGL3x) vertexArray.getIndexBuffer();

		GL3 gl = getGL();

		if (indexBuffer != null) {
			gl.glDrawRangeElements(TypeConverterGL2x3x.to(primitiveType), 0, vertexArray.getMaximumArrayIndex(), indexBuffer.getCount(), TypeConverterGL2x3x.to(indexBuffer.getDatatype()), 0);

		} else {
			gl.glDrawArrays(TypeConverterGL2x3x.to(primitiveType), 0, vertexArray.getMaximumArrayIndex() + 1);
		}
	}
	// NOT SUPPORTED
	//
	// private void applyPrimitiveRestart(PrimitiveRestart primitiveRestart) {
	// if (_renderState.getPrimitiveRestart().isEnabled() != primitiveRestart
	// .isEnabled()) {
	// Enable(EnableCap.PrimitiveRestart, primitiveRestart.isEnabled());
	// _renderState.getPrimitiveRestart().setEnabled(
	// primitiveRestart.isEnabled());
	// }
	//
	// if (primitiveRestart.isEnabled()) {
	// if (_renderState.getPrimitiveRestart().getIndex() != primitiveRestart
	// .getIndex()) {
	// GL.PrimitiveRestartIndex(primitiveRestart.getIndex());
	// _renderState.getPrimitiveRestart()
	// .setIndex(primitiveRestart.getIndex());
	// }
	// }
	// }






	@Override
	protected void applyAfterDraw(DrawState drawState) {
		drawState.getVertexArray().unBind();
		ShaderProgramGL2x3x.unBind();
		FramebufferGL3x.unBind();
	}


	@Override
	protected void applyVertexArray(VertexArray vertexArray) {
		VertexArrayGL3x vertexArrayGL3x = (VertexArrayGL3x) vertexArray;
		vertexArrayGL3x.bind();
		vertexArrayGL3x.clean();
	}

	@Override
	protected void applyShaderProgram(DrawState drawState, SceneState sceneState) {
		ShaderProgramGL2x3x shaderProgramGL3x = (ShaderProgramGL2x3x) drawState.getShaderProgram();

		// if (_boundShaderProgram != shaderProgramGL3x) {
		shaderProgramGL3x.bind();
		_boundShaderProgram = shaderProgramGL3x;
		// }

		_boundShaderProgram.clean(this, drawState, sceneState);

		GL3 gl = GLU.getCurrentGL().getGL3();

		// GL.ValidateProgram(_boundShaderProgram.getHandle().getValue());
		gl.glValidateProgram(_boundShaderProgram.getHandle().getValue());

		int validateStatus;

		// GL.GetProgram(_boundShaderProgram.getHandle().getValue(),
		// ProgramParameter.ValidateStatus, out validateStatus);
		IntBuffer result = Buffers.newDirectIntBuffer(1);
		gl.glGetProgramiv(_boundShaderProgram.getHandle().getValue(), GL2ES2.GL_VALIDATE_STATUS, result);
		validateStatus = result.get(0);

		if (validateStatus == 0) {
			throw new IllegalArgumentException("Shader program validation failed: " + _boundShaderProgram.getLog());
		}
	}

	@Override
	protected void applyFramebuffer() {
		// if (_boundFramebuffer != _setFramebuffer) {
		if (_setFramebuffer != null) {
			_setFramebuffer.bind();
		} else {
			FramebufferGL3x.unBind();
		}

		_boundFramebuffer = _setFramebuffer;
		// }

		if (_setFramebuffer != null) {
			_setFramebuffer.clean();

			// FramebufferErrorCode errorCode = GL
			// .CheckFramebufferStatus(FramebufferTarget.Framebuffer);
			GL3 gl = GLU.getCurrentGL().getGL3();
			int errorCode = gl.glCheckFramebufferStatus(GL.GL_FRAMEBUFFER);

			if (errorCode != GL.GL_FRAMEBUFFER_COMPLETE) {
				throw new IllegalStateException("Frame buffer is incomplete.  Error code: " + errorCode);
			}
		}
	}


	private ShaderProgramGL2x3x _boundShaderProgram;
	private FramebufferGL3x _boundFramebuffer;
	private FramebufferGL3x _setFramebuffer;

	private TextureUnitsGL3x _textureUnits;

	@Override
	protected void createTextureUnit() {
		_textureUnits = new TextureUnitsGL3x();

	}

	@Override
	public TextureUnits getTextureUnits() {
		return _textureUnits;
	}
	@Override
	public VertexArray createVertexArray(Mesh mesh, ShaderVertexAttributeCollection shaderAttributes, BufferHint usageHint) {
		return createVertexArray(Device3x.createMeshBuffers(mesh, shaderAttributes, usageHint));
	}

	/**
	 * @return the _boundFramebuffer
	 */
	public FramebufferGL3x get_boundFramebuffer() {
		return _boundFramebuffer;
	}

}