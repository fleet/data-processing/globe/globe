package fr.ifremer.globe.ogl.renderer.jogl.shaders;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.core.matrices.Matrix42;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanable;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;
import fr.ifremer.globe.ogl.renderer.shaders.UniformType;

public class UniformFloatMatrix42GL2x3x extends Uniform<Matrix42<Float>> implements ICleanable {
	public UniformFloatMatrix42GL2x3x(String name, int location, ICleanableObserver observer) {
		super(name, UniformType.FloatMatrix42);
		_location = location;
		_value = new Matrix42<Float>(Float.class);
		_dirty = true;
		_observer = observer;
		_observer.notifyDirty(this);
	}

	@Override
	public Matrix42<Float> getValue() {
		return _value;
	}

	@Override
	public void setValue(Matrix42<Float> value) {
		if (!_dirty && (_value != value)) {
			_dirty = true;
			_observer.notifyDirty(this);
		}

		_value = value;
	}

	@Override
	public void clean() {

		if( _value != null) {
			GL2GL3 gl = GLU.getCurrentGL().getGL2GL3();
			Float[] data = _value.getReadOnlyColumnMajorValues();
			float[] matrix = new float[data.length];
			for (int i = 0; i < matrix.length; i++) {
				matrix[i] = data[i];
			}
			gl.glUniformMatrix4x2fv(_location, 1, false, matrix, 0);
		}
		_dirty = false;
	}

	private int _location;
	private Matrix42<Float> _value;
	private boolean _dirty;
	private ICleanableObserver _observer;
}