package fr.ifremer.globe.ogl.renderer.jogl;

import java.nio.IntBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GL2ES2;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.core.geometry.Mesh;
import fr.ifremer.globe.ogl.core.geometry.PrimitiveType;
import fr.ifremer.globe.ogl.renderer.Device2x;
import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.framebuffer.Framebuffer;
import fr.ifremer.globe.ogl.renderer.jogl.buffers.IndexBufferGL2x;
import fr.ifremer.globe.ogl.renderer.jogl.framebuffer.FramebufferGL2x;
import fr.ifremer.globe.ogl.renderer.jogl.shaders.ShaderProgramGL2x3x;
import fr.ifremer.globe.ogl.renderer.jogl.textures.TextureUnitsGL2x;
import fr.ifremer.globe.ogl.renderer.jogl.vertexarray.VertexArrayGL2x;
import fr.ifremer.globe.ogl.renderer.renderstate.AlphaTest;
import fr.ifremer.globe.ogl.renderer.renderstate.RenderState;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderVertexAttributeCollection;
import fr.ifremer.globe.ogl.renderer.textures.TextureUnits;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexArray;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

import com.jogamp.common.nio.Buffers;

public class ContextGL2x extends ContextGLBase {


	public ContextGL2x(GLCanvas gameWindow) {
		super(gameWindow);

	}

	@Override
	protected GL2 getGL()
	{
		return GLU.getCurrentGL().getGL2();

	}
	@Override
	public void forceApplyRenderState(RenderState renderState) {
		super.forceApplyRenderState(renderState);
		GL2 gl = getGL();
		enable(GL2ES1.GL_ALPHA_TEST, renderState.getAlphaTest().isEnabled(),gl);
		gl.glAlphaFunc(TypeConverterGL2x3x.to(renderState.getAlphaTest().getFunction()), renderState.getAlphaTest().getValue());
	}



	@Override
	public VertexArray createVertexArray() {
		return new VertexArrayGL2x();
	}

	@Override
	public Framebuffer createFramebuffer() {
		return new FramebufferGL2x();
	}



	@Override
	public Framebuffer getFramebuffer() {
		return _setFramebuffer;
	}

	@Override
	public void setFramebuffer(Framebuffer framebuffer) {
		_setFramebuffer = (FramebufferGL2x) framebuffer;
	}

	@Override
	protected void reallyDraw(PrimitiveType primitiveType, DrawState drawState, SceneState sceneState)
	{
		VertexArrayGL2x vertexArray = (VertexArrayGL2x) drawState.getVertexArray();
		IndexBufferGL2x indexBuffer = (IndexBufferGL2x) vertexArray.getIndexBuffer();

		GL2 gl = getGL();

		if (indexBuffer != null) {
			gl.glDrawRangeElements(TypeConverterGL2x3x.to(primitiveType), 0, vertexArray.getMaximumArrayIndex(), indexBuffer.getCount(), TypeConverterGL2x3x.to(indexBuffer.getDatatype()), 0);

		} else {
			gl.glDrawArrays(TypeConverterGL2x3x.to(primitiveType), 0, vertexArray.getMaximumArrayIndex() + 1);
		}

	}


	@Override
	protected void applyAlphaTest(AlphaTest alphaTest) {
		// if (_renderState.getAlphaTest().isEnabled() != alphaTest.isEnabled())
		// {
		GL2 gl=getGL();
		enable(GL2ES1.GL_ALPHA_TEST, alphaTest.isEnabled(),gl);
		_renderState.getAlphaTest().setEnabled(alphaTest.isEnabled());
		// }

		// if (alphaTest.isEnabled()) {
		if (_renderState.getAlphaTest().getFunction() != alphaTest.getFunction() || _renderState.getAlphaTest().getValue() != alphaTest.getValue()) {
			gl.glAlphaFunc(TypeConverterGL2x3x.to(alphaTest.getFunction()), alphaTest.getValue());

			_renderState.getAlphaTest().setFunction(alphaTest.getFunction());
		}
		// }
	}




	@Override
	protected void applyAfterDraw(DrawState drawState) {
		drawState.getVertexArray().unBind();
		ShaderProgramGL2x3x.unBind();
		FramebufferGL2x.unBind();
	}



	@Override
	protected void applyVertexArray(VertexArray vertexArray) {
		VertexArrayGL2x vertexArrayGL3x = (VertexArrayGL2x) vertexArray;
		vertexArrayGL3x.bind();
		vertexArrayGL3x.clean();
	}

	@Override
	protected void applyShaderProgram(DrawState drawState, SceneState sceneState) {
		ShaderProgramGL2x3x shaderProgramGL3x = (ShaderProgramGL2x3x) drawState.getShaderProgram();

		// if (_boundShaderProgram != shaderProgramGL3x) {
		shaderProgramGL3x.bind();
		_boundShaderProgram = shaderProgramGL3x;
		// }

		_boundShaderProgram.clean(this, drawState, sceneState);

		GL2 gl = GLU.getCurrentGL().getGL2();

		// GL.ValidateProgram(_boundShaderProgram.getHandle().getValue());
		gl.glValidateProgram(_boundShaderProgram.getHandle().getValue());

		int validateStatus;

		// GL.GetProgram(_boundShaderProgram.getHandle().getValue(),
		// ProgramParameter.ValidateStatus, out validateStatus);
		IntBuffer result = Buffers.newDirectIntBuffer(1);
		gl.glGetProgramiv(_boundShaderProgram.getHandle().getValue(), GL2ES2.GL_VALIDATE_STATUS, result);
		validateStatus = result.get(0);

		if (validateStatus == 0) {
			throw new IllegalArgumentException("Shader program validation failed: " + _boundShaderProgram.getLog());
		}
	}

	@Override
	protected void applyFramebuffer() {
		// if (_boundFramebuffer != _setFramebuffer) {
		if (_setFramebuffer != null) {
			_setFramebuffer.bind();
		} else {
			FramebufferGL2x.unBind();
		}

		_boundFramebuffer = _setFramebuffer;
		// }

		if (_setFramebuffer != null) {
			_setFramebuffer.clean();

			// FramebufferErrorCode errorCode = GL
			// .CheckFramebufferStatus(FramebufferTarget.Framebuffer);
			GL2 gl = GLU.getCurrentGL().getGL2();
			int errorCode = gl.glCheckFramebufferStatus(GL.GL_FRAMEBUFFER);

			if (errorCode != GL.GL_FRAMEBUFFER_COMPLETE) {
				throw new IllegalStateException("Frame buffer is incomplete.  Error code: " + errorCode);
			}
		}
	}



	private ShaderProgramGL2x3x _boundShaderProgram;
	@SuppressWarnings("unused")
	private FramebufferGL2x _boundFramebuffer;
	private FramebufferGL2x _setFramebuffer;

	private TextureUnitsGL2x _textureUnits;

	@Override
	protected void createTextureUnit() {
		_textureUnits = new TextureUnitsGL2x();

	}



	@Override
	public TextureUnits getTextureUnits() {
		return _textureUnits;
	}

	@Override
	public VertexArray createVertexArray(Mesh mesh, ShaderVertexAttributeCollection shaderAttributes, BufferHint usageHint) {
		return createVertexArray(Device2x.createMeshBuffers(mesh, shaderAttributes, usageHint));
	}

	public VertexArray createVertexArray(VertexBufferAttributes vertexBufferAttributes, BufferHint usageHint) {
		VertexArray va = createVertexArray();
		va.setDisposeBuffers(true);
		for (int i = 0; i < vertexBufferAttributes.getMaximumCount(); ++i) {
			va.getAttributes().set(i, vertexBufferAttributes.get(i));
		}
		return va;
	}


}