package fr.ifremer.globe.ogl.renderer.jogl.buffers;

import java.awt.image.BufferedImage;
import java.nio.Buffer;
import java.nio.ByteBuffer;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.jogl.TypeConverterGL2x3x;
import fr.ifremer.globe.ogl.renderer.jogl.names.BufferNameGL2x3x;
import fr.ifremer.globe.ogl.util.BitmapAlgorithms;
import fr.ifremer.globe.ogl.util.SizeInBytes;

import com.jogamp.common.nio.Buffers;

public class PixelBufferGL2x {
	public PixelBufferGL2x(int type, BufferHint usageHint, int sizeInBytes) {
		if (sizeInBytes <= 0) {
			throw new IllegalArgumentException("sizeInBytes must be greater than zero.");
		}

		_name = new BufferNameGL2x3x();

		_sizeInBytes = sizeInBytes;
		_type = type;
		_usageHint = TypeConverterGL2x3x.to(usageHint);

		//
		// Allocating here with GL.BufferData, then writing with
		// GL.BufferSubData
		// in CopyFromSystemMemory() should not have any serious overhead:
		//
		// http://www.opengl.org/discussion_boards/ubbthreads.php?ubb=showflat&Number=267373#Post267373
		//
		// Alternately, we can delay GL.BufferData until the first
		// CopyFromSystemMemory() call.
		//
		bind();

		GL2 gl = GLU.getCurrentGL().getGL2();
		gl.glBufferData(_type, sizeInBytes, null, _usageHint);

	}

	public void copyFromSystemMemory(Buffer bufferInSystemMemory, int destinationOffsetInBytes, int lengthInBytes) {
		if (destinationOffsetInBytes < 0) {
			throw new IllegalArgumentException("destinationOffsetInBytes must be greater than or equal to zero.");
		}

		if (destinationOffsetInBytes + lengthInBytes > _sizeInBytes) {
			throw new IllegalArgumentException("destinationOffsetInBytes + lengthInBytes must be less than or equal to SizeInBytes.");
		}

		if (lengthInBytes < 0) {
			throw new IllegalArgumentException("lengthInBytes must be greater than or equal to zero.");
		}

		if (lengthInBytes > SizeInBytes.getSize(bufferInSystemMemory)) {
			throw new IllegalArgumentException("lengthInBytes must be less than or equal to the size of bufferInSystemMemory in bytes.");
		}

		bind();

		GL2 gl = GLU.getCurrentGL().getGL2();
		gl.glBufferSubData(_type, destinationOffsetInBytes, lengthInBytes, bufferInSystemMemory);
	}

	public void copyFromBitmap(BufferedImage bitmap) {

		ByteBuffer bufferInSystemMemory = BitmapAlgorithms.getData(bitmap);
		int sizeInBytes = bufferInSystemMemory.capacity();

		if (sizeInBytes > _sizeInBytes) {
			throw new IllegalArgumentException("bitmap's size in bytes must be less than or equal to SizeInBytes.");
		}

		bind();

		GL2 gl = GLU.getCurrentGL().getGL2();
		gl.glBufferSubData(_type, 0, sizeInBytes, bufferInSystemMemory);

	}

	public ByteBuffer copyToSystemMemory(int offsetInBytes, int lengthInBytes) {
		if (offsetInBytes < 0) {
			throw new IllegalArgumentException("offsetInBytes must be greater than or equal to zero.");
		}

		if (lengthInBytes <= 0) {
			throw new IllegalArgumentException("lengthInBytes must be greater than zero.");
		}

		if (offsetInBytes + lengthInBytes > _sizeInBytes) {
			throw new IllegalArgumentException("offsetInBytes + lengthInBytes must be less than or equal to SizeInBytes.");
		}

		ByteBuffer bufferInSystemMemory = Buffers.newDirectByteBuffer(lengthInBytes);
		bind();
		GL2 gl = GLU.getCurrentGL().getGL2();
		gl.glGetBufferSubData(_type, offsetInBytes, lengthInBytes, bufferInSystemMemory);

		return bufferInSystemMemory;
	}

	public BufferedImage copyToBitmap(int width, int height, int pixelFormat) {
		if (width <= 0) {
			throw new IllegalArgumentException("width must be greater than zero.");
		}

		if (height <= 0) {
			throw new IllegalArgumentException("height must be greater than zero.");
		}

		BufferedImage bitmap = new BufferedImage(width, height, pixelFormat);

		ByteBuffer bufferInSystemMemory = BitmapAlgorithms.getData(bitmap);
		int sizeInBytes = bufferInSystemMemory.capacity();

		bind();

		GL2 gl = GLU.getCurrentGL().getGL2();
		gl.glGetBufferSubData(_type, 0, sizeInBytes, bufferInSystemMemory);
		bitmap = BitmapAlgorithms.getImage(bufferInSystemMemory);

		// OpenGL had rows bottom to top. Bitmap wants them top to bottom.
		//

		bitmap = BitmapAlgorithms.verticalflip(bitmap);

		return bitmap;
	}

	public int getSizeInBytes() {
		return _sizeInBytes;
	}

	public BufferHint getUsageHint() {
		return TypeConverterGL2x3x.toBufferHint(_usageHint);
	}

	public BufferNameGL2x3x getHandle() {
		return _name;
	}

	public void bind() {

		GL2 gl = GLU.getCurrentGL().getGL2();
		gl.glBindBuffer(_type, _name.getValue()[0]);
	}

	public void dispose() {
		_name.dispose();

	}

	private BufferNameGL2x3x _name;
	private int _sizeInBytes;
	private int _type;
	private int _usageHint;
}