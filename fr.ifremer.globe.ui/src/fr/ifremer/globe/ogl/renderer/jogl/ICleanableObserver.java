package fr.ifremer.globe.ogl.renderer.jogl;

public interface ICleanableObserver {
	void notifyDirty(ICleanable value);
}