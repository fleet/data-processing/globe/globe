package fr.ifremer.globe.ogl.renderer.jogl.textures;

import java.util.EnumSet;

public enum TextureDirtyFlags {
	Texture, TextureSampler;

	public static final EnumSet<TextureDirtyFlags> All = EnumSet.of(Texture, TextureSampler);
}