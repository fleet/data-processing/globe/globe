package fr.ifremer.globe.ogl.renderer.jogl.buffers;

import java.nio.Buffer;
import java.nio.ByteBuffer;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.glu.GLU;

import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.jogl.TypeConverterGL2x3x;
import fr.ifremer.globe.ogl.renderer.jogl.names.BufferNameGL2x3x;
import fr.ifremer.globe.ogl.util.SizeInBytes;

import com.jogamp.common.nio.Buffers;

public class BufferGL2x3x {

	protected GL2GL3 getGL()
	{
		return GLU.getCurrentGL().getGL2GL3();

	}
	public BufferGL2x3x(int type, BufferHint usageHint, int sizeInBytes) {
		this(null,type,usageHint,sizeInBytes);
	}
	public BufferGL2x3x(ByteBuffer buffer, int type, BufferHint usageHint, int sizeInBytes) {
		if (sizeInBytes <= 0) {
			throw new IllegalArgumentException("sizeInBytes must be greater than zero.");
		}

		_name = new BufferNameGL2x3x();

		_sizeInBytes = sizeInBytes;
		_type = type;
		_usageHint = TypeConverterGL2x3x.to(usageHint);

		//
		// Allocating here with GL.BufferData, then writing with
		// GL.BufferSubData
		// in CopyFromSystemMemory() should not have any serious overhead:
		//
		// http://www.opengl.org/discussion_boards/ubbthreads.php?ubb=showflat&Number=267373#Post267373
		//
		// Alternately, we can delay GL.BufferData until the first
		// CopyFromSystemMemory() call.
		//

		GL2GL3 gl = getGL();

		bind();
		gl.glBufferData(_type, sizeInBytes, buffer, _usageHint);

	}

	public void copyFromSystemMemory(Buffer bufferInSystemMemory, int destinationOffsetInBytes, int lengthInBytes) {
		if (destinationOffsetInBytes < 0) {
			throw new IllegalArgumentException("destinationOffsetInBytes must be greater than or equal to zero.");
		}

		if (destinationOffsetInBytes + lengthInBytes > _sizeInBytes) {
			throw new IllegalArgumentException("destinationOffsetInBytes + lengthInBytes must be less than or equal to SizeInBytes.");
		}

		if (lengthInBytes < 0) {
			throw new IllegalArgumentException("lengthInBytes must be greater than or equal to zero.");
		}

		if (lengthInBytes > SizeInBytes.getSize(bufferInSystemMemory)) {
			throw new IllegalArgumentException("lengthInBytes must be less than or equal to the size of bufferInSystemMemory in bytes.");
		}

		GL2GL3 gl = getGL();

		bind();
		gl.glBufferSubData(_type, destinationOffsetInBytes, lengthInBytes, bufferInSystemMemory);
	}

	public ByteBuffer copyToSystemMemory(int offsetInBytes, int lengthInBytes) {
		if (offsetInBytes < 0) {
			throw new IllegalArgumentException("offsetInBytes must be greater than or equal to zero.");
		}

		if (lengthInBytes <= 0) {
			throw new IllegalArgumentException("lengthInBytes must be greater than zero.");
		}

		if (offsetInBytes + lengthInBytes > _sizeInBytes) {
			throw new IllegalArgumentException("offsetInBytes + lengthInBytes must be less than or equal to SizeInBytes.");
		}

		ByteBuffer bufferInSystemMemory = Buffers.newDirectByteBuffer(lengthInBytes);
		GL2GL3 gl = getGL();

		gl.glGetBufferSubData(_type, offsetInBytes, lengthInBytes, bufferInSystemMemory);

		return bufferInSystemMemory;
	}

	public int getSizeInBytes() {
		return _sizeInBytes;
	}

	public BufferHint getUsageHint() {
		return TypeConverterGL2x3x.toBufferHint(_usageHint);
	}

	public BufferNameGL2x3x getHandle() {
		return _name;
	}

	public void bind() {

		GL2GL3 gl = getGL();
		gl.glBindBuffer(_type, _name.getValue()[0]);
	}

	public void dispose() {
		_name.dispose();

	}

	private BufferNameGL2x3x _name;
	private int _sizeInBytes;
	private int _type;
	private int _usageHint;
}