package fr.ifremer.globe.ogl.renderer.jogl.framebuffer;

import com.jogamp.opengl.util.texture.Texture;

/**
 * <p>
 * Description : ColorAttachment.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 21 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class ColorAttachment {

	public Texture getTexture() {
		return m_Texture;
	}

	public void setTexture(Texture texture) {
		m_Texture = texture;
	}

	public boolean isDirty() {
		return m_Dirty;
	}

	public void setDirty(boolean dirty) {
		m_Dirty = dirty;
	}

	private Texture m_Texture = null;
	private boolean m_Dirty = false;
}
