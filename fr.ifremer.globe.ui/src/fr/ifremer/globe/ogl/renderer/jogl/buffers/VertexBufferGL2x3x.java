package fr.ifremer.globe.ogl.renderer.jogl.buffers;

import java.nio.Buffer;
import java.nio.ByteBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2GL3;

import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;

public class VertexBufferGL2x3x extends VertexBuffer {
	public VertexBufferGL2x3x(BufferHint usageHint, int sizeInBytes) {
		_bufferObject = new BufferGL2x3x(GL.GL_ARRAY_BUFFER, usageHint, sizeInBytes);
	}
	public VertexBufferGL2x3x(Buffer buffer,BufferHint usageHint, int sizeInBytes) {
		_bufferObject = new BufferGL2x3x(GL.GL_ARRAY_BUFFER, usageHint, sizeInBytes);
		copyFromSystemMemory(buffer,0,sizeInBytes);
	}
	public void bind() {

		_bufferObject.bind();
	}

	public static void unBind(GL2GL3 gl) {
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
	}

	protected int destinationOffsetInBytes;
	protected int lengthInBytes;
	@Override
	public void copyFromSystemMemory(Buffer bufferInSystemMemory, int destinationOffsetInBytes, int lengthInBytes) {
		this.destinationOffsetInBytes = destinationOffsetInBytes;
		this.lengthInBytes = lengthInBytes;
		_bufferObject.copyFromSystemMemory(bufferInSystemMemory, destinationOffsetInBytes, lengthInBytes);
	}

	@Override
	public ByteBuffer copyToSystemMemory(int offsetInBytes, int sizeInBytes) {
		return _bufferObject.copyToSystemMemory(offsetInBytes, sizeInBytes);
	}

	@Override
	public int getSizeInBytes() {
		return _bufferObject.getSizeInBytes();
	}

	@Override
	public BufferHint getUsageHint() {
		return _bufferObject.getUsageHint();
	}

	@Override
	protected void dispose(boolean disposing) {
		if (disposing) {
			_bufferObject.dispose();
		}

	}

	private BufferGL2x3x _bufferObject;

	/** Follow the link.
	 * @see fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer#refresh(java.nio.Buffer)
	 */
	@Override
	public void refresh(Buffer byteBuffer) {
		byteBuffer.reset();
		_bufferObject.copyFromSystemMemory(byteBuffer, destinationOffsetInBytes, lengthInBytes);
	}
}