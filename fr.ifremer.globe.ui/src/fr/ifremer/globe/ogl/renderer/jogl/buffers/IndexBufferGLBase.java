package fr.ifremer.globe.ogl.renderer.jogl.buffers;

import java.nio.IntBuffer;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2GL3;

import fr.ifremer.globe.ogl.renderer.buffers.BufferHint;
import fr.ifremer.globe.ogl.renderer.buffers.IndexBuffer;
import fr.ifremer.globe.ogl.renderer.buffers.IndexBufferDatatype;

import com.jogamp.common.nio.Buffers;

public abstract class IndexBufferGLBase extends IndexBuffer {
	protected abstract GL2GL3 getGL();

	public IndexBufferGLBase(BufferHint usageHint, int size) {
		_bufferObject = new BufferGL2x3x(GL.GL_ELEMENT_ARRAY_BUFFER, usageHint, size * Buffers.SIZEOF_INT);

	}


	public void bind() {

		_bufferObject.bind();
	}

	public static void unBind(GL2GL3 gl) {

		gl.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	public int getCount() {
		return _count;
	}

	@Override
	public void copyFromSystemMemory(IntBuffer bufferInSystemMemory, int destinationOffsetInBytes, int lengthInBytes) {

		_dataType = IndexBufferDatatype.Int;

		_count = bufferInSystemMemory.capacity();

		bufferInSystemMemory.rewind();
		_bufferObject.copyFromSystemMemory(Buffers.copyIntBufferAsByteBuffer(bufferInSystemMemory), destinationOffsetInBytes, lengthInBytes);
	}

	@Override
	public IntBuffer copyToSystemMemory(int offsetInBytes, int sizeInBytes) {
		return _bufferObject.copyToSystemMemory(offsetInBytes, sizeInBytes).asIntBuffer();
	}

	@Override
	public int getSizeInBytes() {
		return _bufferObject.getSizeInBytes();
	}

	@Override
	public BufferHint getUsageHint() {
		return _bufferObject.getUsageHint();
	}

	@Override
	public IndexBufferDatatype getDatatype() {
		return _dataType;
	}

	@Override
	protected void dispose(boolean disposing) {
		if (disposing) {
			_bufferObject.dispose();
		}

	}

	private BufferGL2x3x _bufferObject;
	private IndexBufferDatatype _dataType;
	private int _count;
}