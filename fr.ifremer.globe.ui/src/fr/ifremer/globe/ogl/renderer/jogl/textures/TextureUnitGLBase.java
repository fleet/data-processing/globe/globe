package fr.ifremer.globe.ogl.renderer.jogl.textures;

import java.util.EnumSet;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2GL3;
import javax.naming.OperationNotSupportedException;

import org.apache.log4j.Logger;

import com.jogamp.opengl.util.texture.Texture;

import fr.ifremer.globe.ogl.renderer.jogl.ICleanable;
import fr.ifremer.globe.ogl.renderer.jogl.ICleanableObserver;
import fr.ifremer.globe.ogl.renderer.jogl.TypeConverterGL2x3x;
import fr.ifremer.globe.ogl.renderer.textures.TextureMinificationFilter;
import fr.ifremer.globe.ogl.renderer.textures.TextureSampler;
import fr.ifremer.globe.ogl.renderer.textures.TextureUnit;
import fr.ifremer.globe.ogl.renderer.textures.TextureWrap;

public abstract class  TextureUnitGLBase extends TextureUnit implements ICleanable {

	/**
	 * Logger.
	 */
	private static final Logger LOGGER = Logger.getLogger(TextureUnitGLBase.class);

	public TextureUnitGLBase(int index, ICleanableObserver observer) {
		_textureUnitIndex = index;

		_textureUnit = GL.GL_TEXTURE0 + index;
		_observer = observer;
	}

	@Override
	public Texture getTexture() {
		return _texture;
	}

	@Override
	public void setTexture(Texture texture) {

		if (_texture != texture) {
			if (_dirtyFlags.isEmpty()) {
				_observer.notifyDirty(this);
			}

			_dirtyFlags.add(TextureDirtyFlags.Texture);
			_texture = texture;
		}
	}
	protected abstract GL2GL3 getGL();
	protected abstract TextureSampler getDefaultSampler();


	@Override
	public TextureSampler getTextureSampler() {
		return _textureSampler;
	}

	@Override
	public void setTextureSampler(TextureSampler value) {
		TextureSamplerGL2xGL3 sampler = (TextureSamplerGL2xGL3) value;

		if (_textureSampler != sampler) {
			if (_dirtyFlags.isEmpty()) {
				_observer.notifyDirty(this);
			}

			_dirtyFlags.add(TextureDirtyFlags.TextureSampler);
			_textureSampler = sampler;
		}
	}

	public void cleanLastTextureUnit() {
		//
		// If the last texture unit has a texture attached, it
		// is cleaned even if it isn't dirty because the last
		// texture unit is used for texture uploads and downloads in
		// Texture2DGL3x, the texture unit could be dirty without
		// knowing it.
		//
		if (_texture != null) {
			_dirtyFlags.add(TextureDirtyFlags.Texture);
		}

		clean();
	}

	@Override
	public void clean() {
		if (!_dirtyFlags.isEmpty()) {

			try {
				validate();
			} catch (OperationNotSupportedException e) {
				LOGGER.error(e, e);
			}

			GL2GL3 gl = getGL();
			gl.glActiveTexture(_textureUnit);

			if (_texture != null) {
				_texture.bind(gl);

				// On fait toujours ca pour être sur que le sampler est bien
				// activé
				applySampler(_textureSampler, _texture);
			} else {

				// Texture2DGL3x.unBind(GL.GL_TEXTURE_2D);
				// Texture2DGL3x.unBind(GL2.GL_TEXTURE_RECTANGLE_ARB);
				gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
				gl.glBindTexture(GL2.GL_TEXTURE_RECTANGLE_ARB, 0);
			}

			_dirtyFlags.clear();
		}
	}

	private void applySampler(TextureSampler sampler, Texture texture) {
		// Default case
		if (sampler == null) {
			// Default sampler, compatiable when attaching a non-mimapped
			// texture to a frame buffer object.
			applySampler(getDefaultSampler(), texture);
		} else {
			int minFilter = TypeConverterGL2x3x.to(sampler.getMinificationFilter());
			int magFilter = TypeConverterGL2x3x.to(sampler.getMagnificationFilter());
			int wrapS = TypeConverterGL2x3x.to(sampler.getWrapS());
			int wrapT = TypeConverterGL2x3x.to(sampler.getWrapT());

			GL2GL3 gl = getGL();
			texture.setTexParameteri(gl, GL.GL_TEXTURE_MIN_FILTER, minFilter);
			texture.setTexParameteri(gl, GL.GL_TEXTURE_MAG_FILTER, magFilter);
			texture.setTexParameteri(gl, GL.GL_TEXTURE_WRAP_S, wrapS);
			texture.setTexParameteri(gl, GL.GL_TEXTURE_WRAP_T, wrapT);

		}
	}

	private void validate() throws OperationNotSupportedException {
		if (_texture != null) {
			if (_textureSampler == null) {
				throw new OperationNotSupportedException("A texture sampler must be assigned to a texture unit with one or more bound textures.");
			}

			if (_texture.getTarget() == GL2.GL_TEXTURE_RECTANGLE_ARB) {
				if (_textureSampler.getMinificationFilter() != TextureMinificationFilter.Linear && _textureSampler.getMinificationFilter() != TextureMinificationFilter.Nearest) {
					throw new OperationNotSupportedException(
							"The texture sampler is incompatible with the rectangle texture bound to the same texture unit.  Rectangle textures only support linear and nearest minification filters.");
				}

				if (_textureSampler.getWrapS() == TextureWrap.Repeat || _textureSampler.getWrapS() == TextureWrap.MirroredRepeat || _textureSampler.getWrapT() == TextureWrap.Repeat
						|| _textureSampler.getWrapT() == TextureWrap.MirroredRepeat) {
					throw new OperationNotSupportedException(
							"The texture sampler is incompatible with the rectangle texture bound to the same texture unit.  Rectangle textures do not support repeat or mirrored repeat wrap modes.");
				}
			}
		}
	}

	@SuppressWarnings("unused")
	private int _textureUnitIndex;

	private int _textureUnit;
	private ICleanableObserver _observer;
	private Texture _texture;
	private TextureSamplerGL2xGL3 _textureSampler;
	private EnumSet<TextureDirtyFlags> _dirtyFlags = EnumSet.noneOf(TextureDirtyFlags.class);
}