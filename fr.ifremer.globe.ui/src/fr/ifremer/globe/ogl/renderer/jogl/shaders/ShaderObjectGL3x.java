package fr.ifremer.globe.ogl.renderer.jogl.shaders;

import fr.ifremer.globe.ogl.renderer.CouldNotCreateVideoCardResourceException;
import fr.ifremer.globe.ogl.renderer.infrastructure.EmbeddedResources;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexLocations;
import fr.ifremer.globe.ogl.util.Trig;

public class ShaderObjectGL3x extends ShaderObjectGL2x3x {

	public ShaderObjectGL3x(int shaderType, String source) throws CouldNotCreateVideoCardResourceException {
		super(shaderType, source);
	}

	@Override
	String getBuiltInConstants() {
		String builtinConstants = "#version 330 \n" 
				+ "#define og_positionVertexLocation          " + VertexLocations.Position + " \n"
				+ "#define og_normalVertexLocation            " + VertexLocations.Normal + " \n"
				+ "#define og_textureCoordinateVertexLocation " + VertexLocations.TextureCoordinate + " \n"
				+ "#define og_colorVertexLocation             " + VertexLocations.Color + " \n" 
				+ "#define og_positionHighVertexLocation      " + VertexLocations.PositionHigh + " \n"
				+ "#define og_positionLowVertexLocation       " + VertexLocations.PositionLow + " \n"
				+ "const float og_E =                " + Math.E + "; \n" 
				+ "const float og_pi =               " + Math.PI + "; \n" 
				+ "const float og_oneOverPi =        " + Trig.OneOverPi + "; \n"
				+ "const float og_piOverTwo =        " + Trig.PiOverTwo + "; \n"
				+ "const float og_piOverThree =      " + Trig.PiOverThree + "; \n"
				+ "const float og_piOverFour =       " + Trig.PiOverFour + "; \n" 
				+ "const float og_piOverSix =        " + Trig.PiOverSix + "; \n" 
				+ "const float og_threePiOver2 =     " + Trig.ThreePiOver2 + "; \n"
				+ "const float og_twoPi =            " + Trig.TwoPi + "; \n" 
				+ "const float og_oneOverTwoPi =     " + Trig.OneOverTwoPi + "; \n" 
				+ "const float og_radiansPerDegree = " + Trig.RadiansPerDegree + "; \n" 
				+ "const float og_maximumFloat =     " + Float.MAX_VALUE + "; \n" 
				+ "const float og_minimumFloat =     " + Float.MIN_VALUE + "; \n";
		return builtinConstants;
	}

	@Override
	String getBuiltInFunction() {
		String builtinFunctions = EmbeddedResources.getText(this.getClass(),"fr/ifremer/globe/ogl/renderer/jogl/glsl/BuiltinFunctions.glsl");
		return builtinFunctions;
	}

}
