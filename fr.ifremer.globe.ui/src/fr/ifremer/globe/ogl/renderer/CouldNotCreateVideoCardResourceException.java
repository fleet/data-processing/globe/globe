package fr.ifremer.globe.ogl.renderer;

public class CouldNotCreateVideoCardResourceException extends Exception {

	/**
	 * UID.
	 */
	private static final long serialVersionUID = -8091436653462259695L;

	public CouldNotCreateVideoCardResourceException() {
	}

	public CouldNotCreateVideoCardResourceException(String message) {
		super(message);
	}

	public CouldNotCreateVideoCardResourceException(String message, Exception inner) {
		super(message, inner);
	}
}