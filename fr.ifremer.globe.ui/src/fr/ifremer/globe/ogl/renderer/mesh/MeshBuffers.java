package fr.ifremer.globe.ogl.renderer.mesh;

import fr.ifremer.globe.ogl.renderer.Device;
import fr.ifremer.globe.ogl.renderer.buffers.IndexBuffer;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

/**
 * <p>
 * Description : MeshBuffers. Does not own vertex and index buffers. They must
 * be disposed.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class MeshBuffers {

	public MeshBuffers(Device device) {
		_attributes = new MeshVertexBufferAttributes(device.getMaximumNumberOfVertexAttributes());
	}
	/**
	 * Logger.
	 */
	// private static final Logger LOGGER = Logger.getLogger(MeshBuffers.class);

	public VertexBufferAttributes getAttributes() {
		return _attributes;
	}

	public IndexBuffer getIndexBuffer() {
		return m_IndexBuffer;
	}

	public void setIndexBuffer(IndexBuffer indexBuffer) {
		m_IndexBuffer = indexBuffer;
	}

	private MeshVertexBufferAttributes _attributes;
	private IndexBuffer m_IndexBuffer;

}
