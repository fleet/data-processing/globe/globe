package fr.ifremer.globe.ogl.renderer.mesh;

import java.util.Arrays;
import java.util.Iterator;

import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes;

/**
 * <p>
 * Description : MeshVertexBufferAttributes.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class MeshVertexBufferAttributes extends VertexBufferAttributes {

	/**
	 * Logger.
	 */
	// private static final Logger LOGGER =
	// Logger.getLogger(MeshVertexBufferAttributes.class);

	public MeshVertexBufferAttributes(int maximumNumberOfVertexAttributes) {
		_attributes = new VertexBufferAttribute[maximumNumberOfVertexAttributes];
	}

	/**
	 * @see fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes#get(int)
	 */
	@Override
	public VertexBufferAttribute get(int index) {
		return _attributes[index];
	}

	/**
	 * @see fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes#getCount()
	 */
	@Override
	public int getCount() {
		return _count;
	}

	/**
	 * @see fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes#getMaximumCount()
	 */
	@Override
	public int getMaximumCount() {
		return _attributes.length;
	}

	/**
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<VertexBufferAttribute> iterator() {
		return Arrays.asList(_attributes).iterator();
	}

	/**
	 * @see fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttributes#set(int,
	 *      fr.ifremer.globe.ogl.renderer.vertexarray.VertexBufferAttribute)
	 */
	@Override
	public void set(int index, VertexBufferAttribute attribute) {
		if ((_attributes[index] != null) && (attribute == null)) {
			--_count;
		} else if ((_attributes[index] == null) && (attribute != null)) {
			++_count;
		}

		_attributes[index] = attribute;
	}

	private VertexBufferAttribute[] _attributes;
	private int _count;
}
