package fr.ifremer.globe.ogl.renderer.clearstate;

import java.util.EnumSet;

public enum ClearBuffers {
	ColorBuffer, DepthBuffer, StencilBuffer;

	public static final EnumSet<ClearBuffers> ColorAndDepthBuffer = EnumSet.of(ClearBuffers.ColorBuffer, ClearBuffers.DepthBuffer);
	public static final EnumSet<ClearBuffers> All = EnumSet.of(ClearBuffers.ColorBuffer, ClearBuffers.DepthBuffer, ClearBuffers.StencilBuffer);
}
