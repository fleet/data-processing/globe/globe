package fr.ifremer.globe.ogl.renderer.clearstate;

import java.awt.Color;
import java.util.EnumSet;

import fr.ifremer.globe.ogl.renderer.renderstate.ColorMask;
import fr.ifremer.globe.ogl.renderer.renderstate.ScissorTest;

public class ClearState {

	public ClearState() {
		m_ScissorTest = new ScissorTest();
		m_ColorMask = new ColorMask(true, true, true, true);
		m_DepthMask = true;
		m_FrontStencilMask = ~0;
		m_BackStencilMask = ~0;

		m_Buffers = ClearBuffers.All;
		m_Color = Color.WHITE;
		m_Depth = 1;
		m_Stencil = 0;
	}

	public ScissorTest getScissorTest() {
		return m_ScissorTest;
	}

	public void setScissorTest(ScissorTest scissorTest) {
		m_ScissorTest = scissorTest;
	}

	public ColorMask getColorMask() {
		return m_ColorMask;
	}

	public void setColorMask(ColorMask colorMask) {
		m_ColorMask = colorMask;
	}

	public boolean isDepthMask() {
		return m_DepthMask;
	}

	public void setDepthMask(boolean depthMask) {
		m_DepthMask = depthMask;
	}

	public int getFrontStencilMask() {
		return m_FrontStencilMask;
	}

	public void setFrontStencilMask(int frontStencilMask) {
		m_FrontStencilMask = frontStencilMask;
	}

	public int getBackStencilMask() {
		return m_BackStencilMask;
	}

	public void setBackStencilMask(int backStencilMask) {
		m_BackStencilMask = backStencilMask;
	}

	public EnumSet<ClearBuffers> getBuffers() {
		return m_Buffers;
	}

	public void setBuffers(EnumSet<ClearBuffers> buffers) {
		m_Buffers = buffers;
	}

	public Color getColor() {
		return m_Color;
	}

	public void setColor(Color color) {
		m_Color = color;
	}

	public float getDepth() {
		return m_Depth;
	}

	public void setDepth(float depth) {
		m_Depth = depth;
	}

	public int getStencil() {
		return m_Stencil;
	}

	public void setStencil(int stencil) {
		m_Stencil = stencil;
	}

	private ScissorTest m_ScissorTest = null;
	private ColorMask m_ColorMask = null;
	private boolean m_DepthMask = false;
	private int m_FrontStencilMask = 0;
	private int m_BackStencilMask = 0;

	private EnumSet<ClearBuffers> m_Buffers = null;
	private Color m_Color = null;
	private float m_Depth = 0.0f;
	private int m_Stencil = 0;
}