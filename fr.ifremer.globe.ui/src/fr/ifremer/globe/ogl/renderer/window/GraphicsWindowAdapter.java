package fr.ifremer.globe.ogl.renderer.window;

import com.jogamp.opengl.GLAutoDrawable;

/**
 * <p>
 * Description : GraphicsWindowAdapter.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 23 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class GraphicsWindowAdapter implements GraphicsWindowListener {

	/**
	 * @see fr.ifremer.globe.ogl.renderer.window.GraphicsWindowListener#displayChanged(com.jogamp.opengl.GLAutoDrawable,
	 *      boolean, boolean,
	 *      fr.ifremer.globe.ogl.renderer.window.GraphicsWindow)
	 */
	@Override
	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged, GraphicsWindow window) {
	}

	/**
	 * @see fr.ifremer.globe.ogl.renderer.window.GraphicsWindowListener#init(com.jogamp.opengl.GLAutoDrawable,
	 *      fr.ifremer.globe.ogl.renderer.window.GraphicsWindow)
	 */
	@Override
	public void init(GLAutoDrawable drawable, GraphicsWindow window) {
	}

	/**
	 * @see fr.ifremer.globe.ogl.renderer.window.GraphicsWindowListener#postRender(com.jogamp.opengl.GLAutoDrawable,
	 *      fr.ifremer.globe.ogl.renderer.window.GraphicsWindow)
	 */
	@Override
	public void postRender(GLAutoDrawable drawable, GraphicsWindow windows) {
	}

	/**
	 * @see fr.ifremer.globe.ogl.renderer.window.GraphicsWindowListener#preRender(com.jogamp.opengl.GLAutoDrawable,
	 *      fr.ifremer.globe.ogl.renderer.window.GraphicsWindow)
	 */
	@Override
	public void preRender(GLAutoDrawable drawable, GraphicsWindow windows) {
	}

	/**
	 * @see fr.ifremer.globe.ogl.renderer.window.GraphicsWindowListener#render(com.jogamp.opengl.GLAutoDrawable,
	 *      fr.ifremer.globe.ogl.renderer.window.GraphicsWindow)
	 */
	@Override
	public void render(GLAutoDrawable drawable, GraphicsWindow windows) {
	}

	/**
	 * @see fr.ifremer.globe.ogl.renderer.window.GraphicsWindowListener#reshape(com.jogamp.opengl.GLAutoDrawable,
	 *      int, int, int, int,
	 *      fr.ifremer.globe.ogl.renderer.window.GraphicsWindow)
	 */
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height, GraphicsWindow window) {
	}

	/**
	 * @see fr.ifremer.globe.ogl.renderer.window.GraphicsWindowListener#update(com.jogamp.opengl.GLAutoDrawable,
	 *      fr.ifremer.globe.ogl.renderer.window.GraphicsWindow)
	 */
	@Override
	public void update(GLAutoDrawable drawable, GraphicsWindow windows) {
	}
}
