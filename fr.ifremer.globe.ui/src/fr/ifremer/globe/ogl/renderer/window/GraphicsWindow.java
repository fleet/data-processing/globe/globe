package fr.ifremer.globe.ogl.renderer.window;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.util.HashSet;
import java.util.Set;

import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.awt.GLCanvas;

import fr.ifremer.globe.ogl.renderer.Context;

/**
 * <p>
 * Description : GraphicsWindow.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class GraphicsWindow {

	public void addGraphicsWindowListener(GraphicsWindowListener listener) {
		m_GraphicsWindowListeners.add(listener);
	}

	public abstract GLCanvas getComponent();

	public abstract void addKeyListener(KeyListener listener);

	public abstract void addMouseListener(MouseListener listener);

	public abstract void addMouseMotionListener(MouseMotionListener listener);

	public abstract void addMouseWheelListener(MouseWheelListener listener);

	public abstract Context getContext();

	public abstract int getHeight();

	public abstract int getWidth();

	public void removeGraphicsWindowListener(GraphicsWindowListener listener) {
		m_GraphicsWindowListeners.remove(listener);
	}

	public abstract void removeKeyListener(KeyListener listener);

	public abstract void removeMouseListener(MouseListener listener);

	public abstract void removeMouseMotionListener(MouseMotionListener listener);

	public abstract void removeMouseWheelListener(MouseWheelListener listener);

	public abstract void run(double updateRate);

	public abstract void display();

	protected abstract void dispose(boolean disposing);

	public void dispose() {
		dispose(true);

	}

	protected void fireDisplayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
		for (GraphicsWindowListener listener : m_GraphicsWindowListeners) {
			listener.displayChanged(drawable, modeChanged, deviceChanged, this);
		}
	}

	protected void fireInit(GLAutoDrawable drawable) {
		for (GraphicsWindowListener listener : m_GraphicsWindowListeners) {
			listener.init(drawable, this);
		}
	}

	protected void firePostRender(GLAutoDrawable drawable) {
		for (GraphicsWindowListener listener : m_GraphicsWindowListeners) {
			listener.postRender(drawable, this);
		}
	}

	protected void firePreRender(GLAutoDrawable drawable) {
		for (GraphicsWindowListener listener : m_GraphicsWindowListeners) {
			listener.preRender(drawable, this);
		}
	}

	protected void fireRender(GLAutoDrawable drawable) {
		for (GraphicsWindowListener listener : m_GraphicsWindowListeners) {
			listener.render(drawable, this);
		}
	}

	protected void fireReshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		for (GraphicsWindowListener listener : m_GraphicsWindowListeners) {
			listener.reshape(drawable, x, y, width, height, this);
		}
	}

	protected void fireUpdateFrame(GLAutoDrawable drawable) {
		for (GraphicsWindowListener listener : m_GraphicsWindowListeners) {
			listener.update(drawable, this);
		}
	}

	private Set<GraphicsWindowListener> m_GraphicsWindowListeners = new HashSet<GraphicsWindowListener>();

	public abstract void pause();

	public abstract void resume();
}
