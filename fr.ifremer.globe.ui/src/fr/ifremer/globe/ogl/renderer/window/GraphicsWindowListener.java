package fr.ifremer.globe.ogl.renderer.window;

import com.jogamp.opengl.GLAutoDrawable;

/**
 * <p>
 * Description : Listener on a GraphicsWindow.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 17 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public interface GraphicsWindowListener {

	/**
	 * Called by the drawable when the display mode or the display device
	 * associated with the GLDrawable has changed
	 */
	default void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged, GraphicsWindow window) {
	};

	/**
	 * Initialization.
	 */
	default void init(GLAutoDrawable drawable, GraphicsWindow window) {
	};

	/**
	 * Post render.
	 */
	default void postRender(GLAutoDrawable drawable, GraphicsWindow windows) {
	};

	/**
	 * Pre render.
	 */
	default void preRender(GLAutoDrawable drawable, GraphicsWindow windows) {
	};

	/**
	 * Render.
	 */
	void render(GLAutoDrawable drawable, GraphicsWindow windows);

	/**
	 * Reshape.
	 */
	default void reshape(GLAutoDrawable drawable, int x, int y, int width, int height, GraphicsWindow window) {
	};

	/**
	 * Update.
	 */
	default void update(GLAutoDrawable drawable, GraphicsWindow windows){};

}
