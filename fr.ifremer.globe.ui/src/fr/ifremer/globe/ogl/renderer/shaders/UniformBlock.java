package fr.ifremer.globe.ogl.renderer.shaders;

import fr.ifremer.globe.ogl.renderer.buffers.UniformBuffer;

/**
 * <p>
 * Description : UniformBlock.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class UniformBlock {

	/**
	 * Logger.
	 */
	// private static final Logger LOGGER =
	// Logger.getLogger(UniformBlock.class);

	private UniformBlock(String name, int sizeInBytes) {
		_name = name;
		_sizeInBytes = sizeInBytes;
		_members = new UniformBlockMemberCollection();
	}

	public String getName() {
		return _name;
	}

	public int getSizeInBytes() {
		return _sizeInBytes;
	}

	public UniformBlockMemberCollection getMembers() {
		return _members;
	}

	public void bind(UniformBuffer uniformBuffer) {
	}

	private String _name;
	private int _sizeInBytes;
	private UniformBlockMemberCollection _members;
}
