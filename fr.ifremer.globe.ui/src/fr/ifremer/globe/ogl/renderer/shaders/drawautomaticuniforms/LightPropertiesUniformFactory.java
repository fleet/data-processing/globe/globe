package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class LightPropertiesUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_diffuseSpecularAmbientShininess";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new LightPropertiesUniform(uniform);
	}

}