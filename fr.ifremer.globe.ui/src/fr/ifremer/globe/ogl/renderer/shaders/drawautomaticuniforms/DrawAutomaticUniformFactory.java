package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

/**
 * <p>
 * Description : DrawAutomaticUniformFactory.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class DrawAutomaticUniformFactory {

	public abstract String getName();

	public abstract DrawAutomaticUniform create(Uniform<?> uniform);

}
