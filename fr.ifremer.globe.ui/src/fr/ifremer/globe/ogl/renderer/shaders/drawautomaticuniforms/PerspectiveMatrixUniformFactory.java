package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class PerspectiveMatrixUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_perspectiveMatrix";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new PerspectiveMatrixUniform(uniform);
	}

}