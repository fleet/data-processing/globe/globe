package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class CameraLightPositionUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_cameraLightPosition";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new CameraLightPositionUniform(uniform);
	}

}