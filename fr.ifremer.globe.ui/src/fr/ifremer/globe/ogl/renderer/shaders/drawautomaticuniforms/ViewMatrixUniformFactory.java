package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class ViewMatrixUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_viewMatrix";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new ViewMatrixUniform(uniform);
	}

}