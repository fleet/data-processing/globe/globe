package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class ViewportUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_viewport";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new ViewportUniform(uniform);
	}

}