package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;
//package fr.ifremer.globe.dtmviewer.clipmap.renderer.shaders.drawautomaticuniforms;
//
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.Context;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.drawstate.DrawState;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.scene.SceneState;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.shaders.Uniform;
//
//public class PixelSizePerDistanceUniform extends DrawAutomaticUniform {
//	@SuppressWarnings("unchecked")
//	public PixelSizePerDistanceUniform(Uniform<?> uniform) {
//		_uniform = (Uniform<Float>) uniform;
//	}
//
//	@Override
//	public void set(Context context, DrawState drawState, SceneState sceneState) {
//		_uniform.setValue((float) (Math.tan(0.5 * sceneState.getCamera().getFieldOfViewY()) * 2.0 / context.getViewport().getHeight()));
//	}
//
//	private Uniform<Float> _uniform;
//}