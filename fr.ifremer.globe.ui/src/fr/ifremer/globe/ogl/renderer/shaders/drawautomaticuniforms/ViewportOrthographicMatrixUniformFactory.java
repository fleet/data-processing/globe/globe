package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class ViewportOrthographicMatrixUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_viewportOrthographicMatrix";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new ViewportOrthographicMatrixUniform(uniform);
	}

}