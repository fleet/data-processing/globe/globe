package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;
//package fr.ifremer.globe.dtmviewer.clipmap.renderer.shaders.drawautomaticuniforms;
//
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.Context;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.drawstate.DrawState;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.scene.SceneState;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.shaders.Uniform;
//
//public class PerspectiveNearPlaneDistanceUniform extends DrawAutomaticUniform {
//	@SuppressWarnings("unchecked")
//	public PerspectiveNearPlaneDistanceUniform(Uniform<?> uniform) {
//		_uniform = (Uniform<Float>) uniform;
//	}
//
//	@Override
//	public void set(Context context, DrawState drawState, SceneState sceneState) {
//		_uniform.setValue((float) sceneState.getCamera().getPerspectiveNearPlaneDistance());
//	}
//
//	private Uniform<Float> _uniform;
//}