package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.core.matrices.Matrix4F;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class PerspectiveMatrixUniform extends DrawAutomaticUniform {
	@SuppressWarnings("unchecked")
	public PerspectiveMatrixUniform(Uniform<?> uniform) {
		_uniform = (Uniform<Matrix4F>) uniform;
	}

	@Override
	public void set(Context context, DrawState drawState, SceneState sceneState) {
		_uniform.setValue(sceneState.getProjectionMatrix().toMatrix4F());
	}

	private Uniform<Matrix4F> _uniform;
}