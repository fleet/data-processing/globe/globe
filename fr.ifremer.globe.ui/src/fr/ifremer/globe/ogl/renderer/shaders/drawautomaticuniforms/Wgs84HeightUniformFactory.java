package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class Wgs84HeightUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_wgs84Height";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new Wgs84HeightUniform(uniform);
	}

}