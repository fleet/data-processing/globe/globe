package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class CameraEyeHighUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_cameraEyeHigh";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new CameraEyeHighUniform(uniform);
	}

}