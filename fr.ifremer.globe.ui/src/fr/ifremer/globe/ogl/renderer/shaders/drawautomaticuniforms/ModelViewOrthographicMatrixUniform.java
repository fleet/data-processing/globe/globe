package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;
//package fr.ifremer.globe.dtmviewer.clipmap.renderer.shaders.drawautomaticuniforms;
//
//import fr.ifremer.globe.dtmviewer.clipmap.core.matrices.Matrix4F;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.Context;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.drawstate.DrawState;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.scene.SceneState;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.shaders.Uniform;
//
//public class ModelViewOrthographicMatrixUniform extends DrawAutomaticUniform {
//	@SuppressWarnings("unchecked")
//	public ModelViewOrthographicMatrixUniform(Uniform<?> uniform) {
//		_uniform = (Uniform<Matrix4F>) uniform;
//	}
//
//	@Override
//	public void set(Context context, DrawState drawState, SceneState sceneState) {
//		_uniform.setValue(sceneState.getModelViewOrthographicMatrix().toMatrix4F());
//	}
//
//	private Uniform<Matrix4F> _uniform;
//}