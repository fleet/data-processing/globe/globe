package fr.ifremer.globe.ogl.renderer.shaders.linkautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

/**
 * <p>
 * Description : TextureUniform.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class TextureUniform extends LinkAutomaticUniform {

	/**
	 * Logger.
	 */
	// private static final Logger LOGGER =
	// Logger.getLogger(TextureUniform.class);

	public TextureUniform(int textureUnit) {
		_textureUnit = textureUnit;
	}

	@Override
	public String getName() {
		return "og_texture" + _textureUnit;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void set(Uniform<?> uniform) {
		((Uniform<Integer>) uniform).setValue(_textureUnit);
	}

	int _textureUnit;
}
