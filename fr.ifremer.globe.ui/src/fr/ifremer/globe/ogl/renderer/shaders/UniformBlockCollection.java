package fr.ifremer.globe.ogl.renderer.shaders;

import fr.ifremer.globe.ogl.util.KeyExtractor;
import fr.ifremer.globe.ogl.util.KeyedCollection;

/**
 * <p>
 * Description : UniformBlockCollection.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class UniformBlockCollection extends KeyedCollection<String, UniformBlock> implements KeyExtractor<String, UniformBlock> {

	/**
	 * Logger.
	 */
	// private static final Logger LOGGER =
	// Logger.getLogger(UniformBlockCollection.class);

	@Override
	public String getKey(UniformBlock value) {
		return value.getName();
	};

}
