package fr.ifremer.globe.ogl.renderer.shaders;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.DrawAutomaticUniform;

/**
 * <p>
 * Description : ShaderProgram.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class ShaderProgram {

	public abstract String getLog();

	public abstract FragmentOutputs getFragmentOutputs();

	public abstract ShaderVertexAttributeCollection getVertexAttributes();

	public abstract UniformCollection getUniforms();

	public abstract UniformBlockCollection getUniformBlocks();

	protected abstract void initializeAutomaticUniforms(UniformCollection uniforms) ;

	protected void setDrawAutomaticUniforms(Context context, DrawState drawState, SceneState sceneState) {
		for (int i = 0; i < _drawAutomaticUniforms.size(); ++i) {
			_drawAutomaticUniforms.get(i).set(context, drawState, sceneState);
		}
	}

	protected abstract void dispose(boolean disposing);

	public void dispose() {
		dispose(true);

	}

	protected List<DrawAutomaticUniform> _drawAutomaticUniforms = new ArrayList<DrawAutomaticUniform>();
}
