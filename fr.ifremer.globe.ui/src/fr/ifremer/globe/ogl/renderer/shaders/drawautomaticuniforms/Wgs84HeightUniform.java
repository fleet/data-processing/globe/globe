package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.core.geometry.Ellipsoid;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class Wgs84HeightUniform extends DrawAutomaticUniform {
	@SuppressWarnings("unchecked")
	public Wgs84HeightUniform(Uniform<?> uniform) {
		_uniform = (Uniform<Float>) uniform;
	}

	@Override
	public void set(Context context, DrawState drawState, SceneState sceneState) {
		_uniform.setValue((float) sceneState.getCamera().getHeight(Ellipsoid.Wgs84));
	}

	private Uniform<Float> _uniform;
}