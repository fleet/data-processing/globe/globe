package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.util.KeyExtractor;
import fr.ifremer.globe.ogl.util.KeyedCollection;

/**
 * <p>
 * Description : DrawAutomaticUniformFactoryCollection.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class DrawAutomaticUniformFactoryCollection extends KeyedCollection<String, DrawAutomaticUniformFactory> implements KeyExtractor<String, DrawAutomaticUniformFactory> {

	@Override
	public String getKey(DrawAutomaticUniformFactory value) {
		return value.getName();
	};

}
