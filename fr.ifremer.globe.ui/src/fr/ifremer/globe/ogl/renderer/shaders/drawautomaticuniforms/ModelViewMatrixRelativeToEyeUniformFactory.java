package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class ModelViewMatrixRelativeToEyeUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_modelViewMatrixRelativeToEye";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new ModelViewMatrixRelativeToEyeUniform(uniform);
	}

}