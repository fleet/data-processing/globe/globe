package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class ModelZToClipCoordinatesUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_modelZToClipCoordinates";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new ModelZToClipCoordinatesUniform(uniform);
	}

}