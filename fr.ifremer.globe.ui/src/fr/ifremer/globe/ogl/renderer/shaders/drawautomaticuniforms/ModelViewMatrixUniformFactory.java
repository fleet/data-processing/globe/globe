package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class ModelViewMatrixUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_modelViewMatrix";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new ModelViewMatrixUniform(uniform);
	}

}