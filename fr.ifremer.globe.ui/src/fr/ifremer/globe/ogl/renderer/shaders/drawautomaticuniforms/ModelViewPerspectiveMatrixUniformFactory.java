package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class ModelViewPerspectiveMatrixUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_modelViewPerspectiveMatrix";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new ModelViewPerspectiveMatrixUniform(uniform);
	}

}