package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.core.vectors.Vector3F;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class SunPositionUniform extends DrawAutomaticUniform {
	@SuppressWarnings("unchecked")
	public SunPositionUniform(Uniform<?> uniform) {
		_uniform = (Uniform<Vector3F>) uniform;
	}

	@Override
	public void set(fr.ifremer.globe.ogl.renderer.Context context, DrawState drawState, SceneState sceneState) {
		_uniform.setValue(sceneState.getSunPosition().toVector3F());
	}

	private Uniform<Vector3F> _uniform;
}