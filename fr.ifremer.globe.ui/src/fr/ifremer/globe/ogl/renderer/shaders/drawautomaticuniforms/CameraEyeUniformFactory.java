package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class CameraEyeUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_cameraEye";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new CameraEyeUniform(uniform);
	}

}