package fr.ifremer.globe.ogl.renderer.shaders;

/**
 * <p>
 * Description : UniformBlockMember.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class UniformBlockMember {

	public UniformBlockMember(String name, UniformType type, int offsetInBytes) {
		_name = name;
		_type = type;
		_offsetInBytes = offsetInBytes;
	}

	public String getName() {
		return _name;
	}

	public UniformType getDatatype() {
		return _type;
	}

	public int getOffsetInBytes() {
		return _offsetInBytes;
	}

	private String _name;
	private UniformType _type;
	private int _offsetInBytes;
}
