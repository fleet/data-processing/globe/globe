package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.core.vectors.Vector3F;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class CameraEyeHighUniform extends DrawAutomaticUniform {
	@SuppressWarnings("unchecked")
	public CameraEyeHighUniform(Uniform<?> uniform) {
		_uniform = (Uniform<Vector3F>) uniform;
	}

	@Override
	public void set(Context context, DrawState drawState, SceneState sceneState) {
		_uniform.setValue(sceneState.getCamera().getEyeHigh());
	}

	private Uniform<Vector3F> _uniform;
}