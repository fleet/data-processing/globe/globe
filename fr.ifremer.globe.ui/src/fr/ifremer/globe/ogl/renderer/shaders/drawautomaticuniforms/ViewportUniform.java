package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import java.awt.Rectangle;

import fr.ifremer.globe.ogl.core.vectors.Vector4F;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class ViewportUniform extends DrawAutomaticUniform {
	@SuppressWarnings("unchecked")
	public ViewportUniform(Uniform<?> uniform) {
		_uniform = (Uniform<Vector4F>) uniform;
	}

	@Override
	public void set(Context context, DrawState drawState, SceneState sceneState) {
		//
		// viewport.Bottom should really be used but Rectangle goes top to
		// botom,
		// not bottom to top.
		//
		Rectangle viewport = context.getViewport();
		_uniform.setValue(new Vector4F((float) viewport.getMinX(), (float) viewport.getMinY(), (float) viewport.getWidth(), (float) viewport.getHeight()));
	}

	private Uniform<Vector4F> _uniform;
}