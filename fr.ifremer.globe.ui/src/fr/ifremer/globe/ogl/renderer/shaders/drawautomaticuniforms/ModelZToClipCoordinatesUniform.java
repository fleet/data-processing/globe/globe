package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.core.matrices.Matrix42;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class ModelZToClipCoordinatesUniform extends DrawAutomaticUniform {
	@SuppressWarnings("unchecked")
	public ModelZToClipCoordinatesUniform(Uniform<?> uniform) {
		_uniform = (Uniform<Matrix42<Float>>) uniform;
	}

	@Override
	public void set(Context context, DrawState drawState, SceneState sceneState) {
		_uniform.setValue(Matrix42.DoubleToFloat(sceneState.getModelZToClipCoordinates()));
	}

	private Uniform<Matrix42<Float>> _uniform;
}