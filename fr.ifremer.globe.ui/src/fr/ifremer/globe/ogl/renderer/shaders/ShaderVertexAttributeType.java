package fr.ifremer.globe.ogl.renderer.shaders;

/**
 * <p>
 * Description : ShaderVertexAttributeType.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public enum ShaderVertexAttributeType {
	Float, FloatVector2, FloatVector3, FloatVector4, FloatMatrix22, FloatMatrix33, FloatMatrix44, Int, IntVector2, IntVector3, IntVector4
}
