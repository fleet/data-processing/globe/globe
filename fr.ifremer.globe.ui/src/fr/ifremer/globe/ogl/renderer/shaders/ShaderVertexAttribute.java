package fr.ifremer.globe.ogl.renderer.shaders;

/**
 * <p>
 * Description : ShaderVertexAttribute.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class ShaderVertexAttribute {

	public ShaderVertexAttribute(String name, int location, ShaderVertexAttributeType type, int length) {
		_name = name;
		_location = location;
		_type = type;
		_length = length;
	}

	public ShaderVertexAttributeType getDatatype() {
		return _type;
	}

	public int getLength() {
		return _length;
	}

	public int getLocation() {
		return _location;
	}

	public String getName() {
		return _name;
	}

	private int _length; // TODO: Array type
	private int _location;
	private String _name;
	private ShaderVertexAttributeType _type;
}
