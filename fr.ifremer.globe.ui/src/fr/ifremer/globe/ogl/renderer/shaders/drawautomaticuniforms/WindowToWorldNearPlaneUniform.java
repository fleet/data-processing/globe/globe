package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;
//package fr.ifremer.globe.dtmviewer.clipmap.renderer.shaders.drawautomaticuniforms;
//
//import fr.ifremer.globe.dtmviewer.clipmap.ClipmapPreferences;
//import fr.ifremer.globe.dtmviewer.clipmap.core.matrices.Matrix4D;
//import fr.ifremer.globe.dtmviewer.clipmap.core.matrices.Matrix4F;
//import fr.ifremer.globe.dtmviewer.clipmap.core.vectors.Vector3D;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.Context;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.drawstate.DrawState;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.scene.Camera;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.scene.SceneState;
//import fr.ifremer.globe.dtmviewer.clipmap.renderer.shaders.Uniform;
//
//public class WindowToWorldNearPlaneUniform extends DrawAutomaticUniform {
//	@SuppressWarnings("unchecked")
//	public WindowToWorldNearPlaneUniform(Uniform<?> uniform) {
//		_uniform = (Uniform<Matrix4F>) uniform;
//	}
//
//	@SuppressWarnings("deprecation")
//	@Override
//	public void set(Context context, DrawState drawState, SceneState sceneState) {
//		Camera camera = sceneState.getCamera();
//		double theta = camera.getFieldOfViewX() * 0.5;
//		double phi = camera.getFieldOfViewY() * 0.5;
//		double nearDistance;
//		if (ClipmapPreferences.USE_PERSPECTIVE) {
//			nearDistance = camera.getPerspectiveNearPlaneDistance();
//		} else {
//			nearDistance = camera.getOrthographicNearPlaneDistance();
//		}
//
//		//
//		// Coordinate system for the near plane: origin is at center, x and y
//		// span [-1, 1] just like noramlized device coordinates.
//		//
//		// Project eye onto near plane
//		Vector3D origin = camera.getEye().add(camera.getForward().multiply(nearDistance));
//		// Rescale right to near plane
//		Vector3D xAxis = camera.getRight().multiply((nearDistance * Math.tan(theta)));
//		// Rescale up to near plane
//		Vector3D yAxis = camera.getUp().multiply(nearDistance * Math.tan(phi));
//
//		_uniform.setValue(new Matrix4D(xAxis.getX(), yAxis.getX(), 0.0, origin.getX(), xAxis.getY(), yAxis.getY(), 0.0, origin.getY(), xAxis.getZ(), yAxis.getZ(), 0.0, origin.getZ(), 0.0, 0.0, 0.0,
//				1.0).toMatrix4F());
//	}
//
//	private Uniform<Matrix4F> _uniform;
//}