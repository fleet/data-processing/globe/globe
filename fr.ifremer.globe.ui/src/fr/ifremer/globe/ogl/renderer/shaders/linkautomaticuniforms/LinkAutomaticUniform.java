package fr.ifremer.globe.ogl.renderer.shaders.linkautomaticuniforms;

import org.apache.log4j.Logger;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

/**
 * <p>
 * Description : LinkAutomaticUniform.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class LinkAutomaticUniform {

	/**
	 * Logger.
	 */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(LinkAutomaticUniform.class);

	public abstract String getName();

	public abstract void set(Uniform<?> uniform);
}
