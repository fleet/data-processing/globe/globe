package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class InverseViewportDimensionsUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_inverseViewportDimensions";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new InverseViewportDimensionsUniform(uniform);
	}

}