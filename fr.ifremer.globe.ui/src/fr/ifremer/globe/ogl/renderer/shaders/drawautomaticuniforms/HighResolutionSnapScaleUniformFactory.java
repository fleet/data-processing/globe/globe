package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class HighResolutionSnapScaleUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_highResolutionSnapScale";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new HighResolutionSnapScaleUniform(uniform);
	}

}