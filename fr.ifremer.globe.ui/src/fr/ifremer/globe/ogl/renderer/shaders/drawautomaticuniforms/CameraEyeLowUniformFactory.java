package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class CameraEyeLowUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_cameraEyeLow";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new CameraEyeLowUniform(uniform);
	}

}