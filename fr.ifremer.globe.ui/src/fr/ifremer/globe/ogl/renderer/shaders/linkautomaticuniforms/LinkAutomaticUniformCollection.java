package fr.ifremer.globe.ogl.renderer.shaders.linkautomaticuniforms;

import org.apache.log4j.Logger;

import fr.ifremer.globe.ogl.util.KeyExtractor;
import fr.ifremer.globe.ogl.util.KeyedCollection;

/**
 * <p>
 * Description : LinkAutomaticUniformCollection.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class LinkAutomaticUniformCollection extends KeyedCollection<String, LinkAutomaticUniform> implements KeyExtractor<String, LinkAutomaticUniform> {

	/**
	 * Logger.
	 */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(LinkAutomaticUniformCollection.class);

	@Override
	public String getKey(LinkAutomaticUniform value) {
		return value.getName();
	};

}
