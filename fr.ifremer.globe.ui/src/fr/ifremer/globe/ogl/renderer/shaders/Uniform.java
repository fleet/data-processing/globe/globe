package fr.ifremer.globe.ogl.renderer.shaders;

/**
 * <p>
 * Description : Shader uniform.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 14 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class Uniform<T> {

	/**
	 * Logger.
	 */
	// private static final Logger LOGGER = Logger.getLogger(Uniform.class);

	protected Uniform(String name, UniformType type) {
		_name = name;
		_type = type;
	}

	public abstract T getValue();

	public abstract void setValue(T value);

	public String getName() {
		return _name;
	}

	public UniformType getDatatype() {
		return _type;
	}

	private String _name;
	private UniformType _type;

}