package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import java.awt.Rectangle;

import fr.ifremer.globe.ogl.core.vectors.Vector2F;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class InverseViewportDimensionsUniform extends DrawAutomaticUniform {
	@SuppressWarnings("unchecked")
	public InverseViewportDimensionsUniform(Uniform<?> uniform) {
		_uniform = (Uniform<Vector2F>) uniform;
	}

	@Override
	public void set(Context context, DrawState drawState, SceneState sceneState) {
		//
		// viewport.Bottom should really be used but Rectangle goes top to
		// botom,
		// not bottom to top.
		//
		Rectangle viewport = context.getViewport();
		_uniform.setValue(new Vector2F(1.0f / (float) viewport.getWidth(), 1.0f / (float) viewport.getHeight()));
	}

	private Uniform<Vector2F> _uniform;
}