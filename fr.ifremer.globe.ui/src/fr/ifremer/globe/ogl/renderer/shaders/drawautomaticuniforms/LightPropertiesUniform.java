package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.core.vectors.Vector4F;
import fr.ifremer.globe.ogl.renderer.Context;
import fr.ifremer.globe.ogl.renderer.drawstate.DrawState;
import fr.ifremer.globe.ogl.renderer.scene.SceneState;
import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class LightPropertiesUniform extends DrawAutomaticUniform {
	@SuppressWarnings("unchecked")
	public LightPropertiesUniform(Uniform<?> uniform) {
		_uniform = (Uniform<Vector4F>) uniform;
	}

	@Override
	public void set(Context context, DrawState drawState, SceneState sceneState) {
		_uniform.setValue(new Vector4F(sceneState.getDiffuseIntensity(), sceneState.getSpecularIntensity(), sceneState.getAmbientIntensity(), sceneState.getShininess()));
	}

	private Uniform<Vector4F> _uniform;
}