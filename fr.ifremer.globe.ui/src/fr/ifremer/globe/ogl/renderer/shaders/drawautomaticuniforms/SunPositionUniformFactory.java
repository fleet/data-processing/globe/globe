package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class SunPositionUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_sunPosition";
	}

	@Override
	public fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms.DrawAutomaticUniform create(Uniform<?> uniform) {
		return new SunPositionUniform(uniform);
	}

}