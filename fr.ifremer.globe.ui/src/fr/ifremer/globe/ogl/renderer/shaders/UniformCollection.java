package fr.ifremer.globe.ogl.renderer.shaders;

import fr.ifremer.globe.ogl.util.KeyExtractor;
import fr.ifremer.globe.ogl.util.KeyedCollection;

/**
 * <p>
 * Description : UniformCollection.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class UniformCollection extends KeyedCollection<String, Uniform<?>> implements KeyExtractor<String, Uniform<?>> {

	@Override
	public String getKey(Uniform<?> value) {
		return value.getName();
	}

}
