package fr.ifremer.globe.ogl.renderer.shaders;

import fr.ifremer.globe.ogl.util.KeyExtractor;
import fr.ifremer.globe.ogl.util.KeyedCollection;

/**
 * <p>
 * Description : ShaderVertexAttributeCollection.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class ShaderVertexAttributeCollection extends KeyedCollection<String, ShaderVertexAttribute> implements KeyExtractor<String, ShaderVertexAttribute> {

	@Override
	public String getKey(ShaderVertexAttribute value) {
		return value.getName();
	}
}
