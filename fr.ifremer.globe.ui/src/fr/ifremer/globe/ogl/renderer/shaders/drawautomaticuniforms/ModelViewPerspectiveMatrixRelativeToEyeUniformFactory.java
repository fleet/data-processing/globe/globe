package fr.ifremer.globe.ogl.renderer.shaders.drawautomaticuniforms;

import fr.ifremer.globe.ogl.renderer.shaders.Uniform;

public class ModelViewPerspectiveMatrixRelativeToEyeUniformFactory extends DrawAutomaticUniformFactory {

	@Override
	public String getName() {
		return "og_modelViewPerspectiveMatrixRelativeToEye";
	}

	@Override
	public DrawAutomaticUniform create(Uniform<?> uniform) {
		return new ModelViewPerspectiveMatrixRelativeToEyeUniform(uniform);
	}

}