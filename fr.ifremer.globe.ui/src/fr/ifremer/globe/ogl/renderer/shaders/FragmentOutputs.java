package fr.ifremer.globe.ogl.renderer.shaders;

/**
 * <p>
 * Description : FragmentOutputs.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class FragmentOutputs {

	public abstract int get(String index);
}
