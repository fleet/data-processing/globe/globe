package fr.ifremer.globe.ogl.renderer;

public interface Device {
	public int getMaximumNumberOfVertexAttributes() ;
}