package fr.ifremer.globe.ogl.renderer.drawstate;

import fr.ifremer.globe.ogl.renderer.renderstate.RenderState;
import fr.ifremer.globe.ogl.renderer.shaders.ShaderProgram;
import fr.ifremer.globe.ogl.renderer.vertexarray.VertexArray;

public class DrawState {
	public DrawState() {
		m_RenderState = new RenderState();
	}

	public DrawState(RenderState renderState, ShaderProgram shaderProgram, VertexArray vertexArray) {
		m_RenderState = renderState;
		m_ShaderProgram = shaderProgram;
		m_VertexArray = vertexArray;
	}

	public RenderState getRenderState() {
		return m_RenderState;
	}

	public void setRenderState(RenderState renderState) {
		m_RenderState = renderState;
	}

	public ShaderProgram getShaderProgram() {
		return m_ShaderProgram;
	}

	public void setShaderProgram(ShaderProgram shaderProgram) {
		m_ShaderProgram = shaderProgram;
	}

	public VertexArray getVertexArray() {
		return m_VertexArray;
	}

	public void setVertexArray(VertexArray vertexArray) {
		m_VertexArray = vertexArray;
	}

	private RenderState m_RenderState = null;
	private ShaderProgram m_ShaderProgram = null;
	private VertexArray m_VertexArray = null;

	/**
	 * Release all ressources.
	 */
	public void dispose() {
		if (getVertexArray() != null) {
			getVertexArray().dispose();
		}
		if (getShaderProgram() != null) {
			getShaderProgram().dispose();
		}
	}
}