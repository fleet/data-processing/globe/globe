package fr.ifremer.globe.ogl.renderer.renderstate;

public class DepthTest {
	public DepthTest() {
		m_Enabled = true;
		m_Function = DepthTestFunction.Less;
	}

	public boolean isEnabled() {
		return m_Enabled;
	}

	public void setEnabled(boolean enabled) {
		m_Enabled = enabled;
	}

	public DepthTestFunction getFunction() {
		return m_Function;
	}

	public void setFunction(DepthTestFunction function) {
		m_Function = function;
	}

	private boolean m_Enabled = false;
	private DepthTestFunction m_Function = null;
}