package fr.ifremer.globe.ogl.renderer.renderstate;

public class StencilTest {
	public StencilTest() {
		m_Enabled = false;
		m_FrontFace = new StencilTestFace();
		m_BackFace = new StencilTestFace();
	}

	public boolean isEnabled() {
		return m_Enabled;
	}

	public void setEnabled(boolean enabled) {
		m_Enabled = enabled;
	}

	public StencilTestFace getFrontFace() {
		return m_FrontFace;
	}

	public void setFrontFace(StencilTestFace frontFace) {
		m_FrontFace = frontFace;
	}

	public StencilTestFace getBackFace() {
		return m_BackFace;
	}

	public void setBackFace(StencilTestFace backFace) {
		m_BackFace = backFace;
	}

	private boolean m_Enabled = false;
	private StencilTestFace m_FrontFace = null;
	private StencilTestFace m_BackFace = null;
}