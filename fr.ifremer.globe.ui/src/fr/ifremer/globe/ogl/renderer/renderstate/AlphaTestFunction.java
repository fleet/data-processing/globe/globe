package fr.ifremer.globe.ogl.renderer.renderstate;

public enum AlphaTestFunction {
	Never, Less, Equal, LessThanOrEqual, Greater, NotEqual, GreaterThanOrEqual, Always
}