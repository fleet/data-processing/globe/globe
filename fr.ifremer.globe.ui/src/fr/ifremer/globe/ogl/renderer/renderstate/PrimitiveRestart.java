package fr.ifremer.globe.ogl.renderer.renderstate;

public class PrimitiveRestart {
	public PrimitiveRestart() {
		m_Enabled = false;
		m_Index = 0;
	}

	public boolean isEnabled() {
		return m_Enabled;
	}

	public void setEnabled(boolean enabled) {
		m_Enabled = enabled;
	}

	public int getIndex() {
		return m_Index;
	}

	public void setIndex(int index) {
		m_Index = index;
	}

	private boolean m_Enabled = false;
	private int m_Index = 0;
}