package fr.ifremer.globe.ogl.renderer.renderstate;

public class RenderState {
	public RenderState() {
		m_PrimitiveRestart = new PrimitiveRestart();
		m_FacetCulling = new FacetCulling();
		m_ProgramPointSize = ProgramPointSize.Disabled;
		m_RasterizationMode = RasterizationMode.Fill;
		m_ScissorTest = new ScissorTest();
		m_StencilTest = new StencilTest();
		m_AlphaTest = new AlphaTest();
		m_DepthTest = new DepthTest();
		m_DepthRange = new DepthRange();
		m_Blending = new Blending();
		m_ColorMask = new ColorMask(true, true, true, true);
		m_DepthMask = true;
	}

	public PrimitiveRestart getPrimitiveRestart() {
		return m_PrimitiveRestart;
	}

	public void setPrimitiveRestart(PrimitiveRestart primitiveRestart) {
		m_PrimitiveRestart = primitiveRestart;
	}

	public FacetCulling getFacetCulling() {
		return m_FacetCulling;
	}

	public void setFacetCulling(FacetCulling facetCulling) {
		m_FacetCulling = facetCulling;
	}

	public ProgramPointSize getProgramPointSize() {
		return m_ProgramPointSize;
	}

	public void setProgramPointSize(ProgramPointSize programPointSize) {
		m_ProgramPointSize = programPointSize;
	}

	public RasterizationMode getRasterizationMode() {
		return m_RasterizationMode;
	}

	public void setRasterizationMode(RasterizationMode rasterizationMode) {
		m_RasterizationMode = rasterizationMode;
	}

	public ScissorTest getScissorTest() {
		return m_ScissorTest;
	}

	public void setScissorTest(ScissorTest scissorTest) {
		m_ScissorTest = scissorTest;
	}

	public StencilTest getStencilTest() {
		return m_StencilTest;
	}

	public void setStencilTest(StencilTest stencilTest) {
		m_StencilTest = stencilTest;
	}

	public DepthTest getDepthTest() {
		return m_DepthTest;
	}

	public void setDepthTest(DepthTest depthTest) {
		m_DepthTest = depthTest;
	}

	public AlphaTest getAlphaTest() {
		return m_AlphaTest;
	}

	public void setAlphaTest(AlphaTest alphaTest) {
		m_AlphaTest = alphaTest;
	}

	public DepthRange getDepthRange() {
		return m_DepthRange;
	}

	public void setDepthRange(DepthRange depthRange) {
		m_DepthRange = depthRange;
	}

	public Blending getBlending() {
		return m_Blending;
	}

	public void setBlending(Blending blending) {
		m_Blending = blending;
	}

	public ColorMask getColorMask() {
		return m_ColorMask;
	}

	public void setColorMask(ColorMask colorMask) {
		m_ColorMask = colorMask;
	}

	public boolean getDepthMask() {
		return m_DepthMask;
	}

	public void setDepthMask(boolean depthMask) {
		m_DepthMask = depthMask;
	}

	private PrimitiveRestart m_PrimitiveRestart = null;
	private FacetCulling m_FacetCulling;
	private ProgramPointSize m_ProgramPointSize;
	private RasterizationMode m_RasterizationMode;
	private ScissorTest m_ScissorTest;
	private StencilTest m_StencilTest;
	private AlphaTest m_AlphaTest;
	private DepthTest m_DepthTest;
	private DepthRange m_DepthRange;
	private Blending m_Blending;
	private ColorMask m_ColorMask;
	private boolean m_DepthMask;
}