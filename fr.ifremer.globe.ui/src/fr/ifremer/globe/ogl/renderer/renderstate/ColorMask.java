package fr.ifremer.globe.ogl.renderer.renderstate;

import java.util.Locale;

/**
 * <p>
 * Description : ColorMask.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 17 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class ColorMask {

	public ColorMask(boolean red, boolean green, boolean blue, boolean alpha) {
		_red = red;
		_green = green;
		_blue = blue;
		_alpha = alpha;
	}

	public boolean getRed() {
		return _red;
	}

	public boolean getGreen() {
		return _green;
	}

	public boolean getBlue() {
		return _blue;
	}

	public boolean getAlpha() {
		return _alpha;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ColorMask) {
			return equals((ColorMask) obj);
		}

		return false;
	}

	public boolean equals(ColorMask other) {
		return _red == other._red && _green == other._green && _blue == other._blue && _alpha == other._alpha;
	}

	@Override
	public String toString() {
		return String.format(Locale.getDefault(), "(Red: {0}, Green: {1}, Blue: {2}, Alpha: {3})", _red, _green, _blue, _alpha);
	}

	@Override
	public int hashCode() {
		return Boolean.valueOf(_red).hashCode() ^ Boolean.valueOf(_green).hashCode() ^ Boolean.valueOf(_blue).hashCode() ^ Boolean.valueOf(_alpha).hashCode();
	}

	private boolean _red;
	private boolean _green;
	private boolean _blue;
	private boolean _alpha;
}
