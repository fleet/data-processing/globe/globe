package fr.ifremer.globe.ogl.renderer.renderstate;

public class StencilTestFace {
	public StencilTestFace() {
		m_StencilFailOperation = StencilOperation.Keep;
		m_DepthFailStencilPassOperation = StencilOperation.Keep;
		m_DepthPassStencilPassOperation = StencilOperation.Keep;
		m_Function = StencilTestFunction.Always;
		m_ReferenceValue = 0;
		m_Mask = ~0;
	}

	public StencilOperation getStencilFailOperation() {
		return m_StencilFailOperation;
	}

	public void setStencilFailOperation(StencilOperation stencilFailOperation) {
		m_StencilFailOperation = stencilFailOperation;
	}

	public StencilOperation getDepthFailStencilPassOperation() {
		return m_DepthFailStencilPassOperation;
	}

	public void setDepthFailStencilPassOperation(StencilOperation depthFailStencilPassOperation) {
		m_DepthFailStencilPassOperation = depthFailStencilPassOperation;
	}

	public StencilOperation getDepthPassStencilPassOperation() {
		return m_DepthPassStencilPassOperation;
	}

	public void setDepthPassStencilPassOperation(StencilOperation depthPassStencilPassOperation) {
		m_DepthPassStencilPassOperation = depthPassStencilPassOperation;
	}

	public StencilTestFunction getFunction() {
		return m_Function;
	}

	public void setFunction(StencilTestFunction function) {
		m_Function = function;
	}

	public int getReferenceValue() {
		return m_ReferenceValue;
	}

	public void setReferenceValue(int referenceValue) {
		m_ReferenceValue = referenceValue;
	}

	public int getMask() {
		return m_Mask;
	}

	public void setMask(int mask) {
		m_Mask = mask;
	}

	private StencilOperation m_StencilFailOperation = null;
	private StencilOperation m_DepthFailStencilPassOperation = null;
	private StencilOperation m_DepthPassStencilPassOperation = null;

	private StencilTestFunction m_Function = null;
	private int m_ReferenceValue = 0;
	private int m_Mask = 0;
}