package fr.ifremer.globe.ogl.renderer.renderstate;

public enum SourceBlendingFactor {
	Zero, One, SourceAlpha, OneMinusSourceAlpha, DestinationAlpha, OneMinusDestinationAlpha, DestinationColor, OneMinusDestinationColor, SourceAlphaSaturate, ConstantColor, OneMinusConstantColor, ConstantAlpha, OneMinusConstantAlpha
}