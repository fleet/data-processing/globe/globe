package fr.ifremer.globe.ogl.renderer.renderstate;

import fr.ifremer.globe.ogl.core.geometry.WindingOrder;

public class FacetCulling {
	public FacetCulling() {
		m_Enabled = false;
		m_Face = CullFace.Back;
		m_FrontFaceWindingOrder = WindingOrder.Counterclockwise;
	}

	public boolean isEnabled() {
		return m_Enabled;
	}

	public void setEnabled(boolean enabled) {
		m_Enabled = enabled;
	}

	public CullFace getFace() {
		return m_Face;
	}

	public void setFace(CullFace face) {
		m_Face = face;
	}

	public WindingOrder getFrontFaceWindingOrder() {
		return m_FrontFaceWindingOrder;
	}

	public void setFrontFaceWindingOrder(WindingOrder frontFaceWindingOrder) {
		m_FrontFaceWindingOrder = frontFaceWindingOrder;
	}

	private boolean m_Enabled = false;
	private CullFace m_Face = null;
	private WindingOrder m_FrontFaceWindingOrder = null;
}