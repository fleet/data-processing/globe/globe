package fr.ifremer.globe.ogl.renderer.renderstate;

public enum ProgramPointSize {
	Enabled, Disabled
}