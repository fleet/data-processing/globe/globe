package fr.ifremer.globe.ogl.renderer.renderstate;

import java.awt.Rectangle;

public class ScissorTest {
	public ScissorTest() {
		m_Enabled = false;
		m_Rectangle = new Rectangle(0, 0, 0, 0);
	}

	public boolean isEnabled() {
		return m_Enabled;
	}

	public void setEnabled(boolean enabled) {
		m_Enabled = enabled;
	}

	public Rectangle getRectangle() {
		return m_Rectangle;
	}

	public void setRectangle(Rectangle rectangle) {
		m_Rectangle = rectangle;
	}

	private boolean m_Enabled = false;
	private Rectangle m_Rectangle = null;
}