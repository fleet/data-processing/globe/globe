package fr.ifremer.globe.ogl.renderer.renderstate;

public enum DestinationBlendingFactor {
	Zero, One, SourceColor, OneMinusSourceColor, SourceAlpha, OneMinusSourceAlpha, DestinationAlpha, OneMinusDestinationAlpha, DestinationColor, OneMinusDestinationColor, ConstantColor, OneMinusConstantColor, ConstantAlpha, OneMinusConstantAlpha
}