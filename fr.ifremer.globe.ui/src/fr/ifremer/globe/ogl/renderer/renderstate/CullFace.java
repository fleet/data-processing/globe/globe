package fr.ifremer.globe.ogl.renderer.renderstate;

public enum CullFace {
	Front, Back, FrontAndBack
}