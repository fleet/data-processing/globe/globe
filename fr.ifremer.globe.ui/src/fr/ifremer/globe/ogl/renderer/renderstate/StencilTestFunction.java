package fr.ifremer.globe.ogl.renderer.renderstate;

public enum StencilTestFunction {
	Never, Less, Equal, LessThanOrEqual, Greater, NotEqual, GreaterThanOrEqual, Always,
}