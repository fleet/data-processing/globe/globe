package fr.ifremer.globe.ogl.renderer.renderstate;

import java.awt.Color;

public class Blending {
	public Blending() {
		m_Enabled = false;
		m_SourceRGBFactor = SourceBlendingFactor.One;
		m_SourceAlphaFactor = SourceBlendingFactor.One;
		m_DestinationRGBFactor = DestinationBlendingFactor.Zero;
		m_DestinationAlphaFactor = DestinationBlendingFactor.Zero;
		m_RGBEquation = BlendEquation.Add;
		m_AlphaEquation = BlendEquation.Add;
		m_Color = new Color(0, 0, 0, 0);
	}

	public boolean isEnabled() {
		return m_Enabled;
	}

	public void setEnabled(boolean enabled) {
		m_Enabled = enabled;
	}

	public SourceBlendingFactor getSourceRGBFactor() {
		return m_SourceRGBFactor;
	}

	public void setSourceRGBFactor(SourceBlendingFactor sourceRGBFactor) {
		m_SourceRGBFactor = sourceRGBFactor;
	}

	public SourceBlendingFactor getSourceAlphaFactor() {
		return m_SourceAlphaFactor;
	}

	public void setSourceAlphaFactor(SourceBlendingFactor sourceAlphaFactor) {
		m_SourceAlphaFactor = sourceAlphaFactor;
	}

	public DestinationBlendingFactor getDestinationRGBFactor() {
		return m_DestinationRGBFactor;
	}

	public void setDestinationRGBFactor(DestinationBlendingFactor destinationRGBFactor) {
		m_DestinationRGBFactor = destinationRGBFactor;
	}

	public DestinationBlendingFactor getDestinationAlphaFactor() {
		return m_DestinationAlphaFactor;
	}

	public void setDestinationAlphaFactor(DestinationBlendingFactor destinationAlphaFactor) {
		m_DestinationAlphaFactor = destinationAlphaFactor;
	}

	public BlendEquation getRGBEquation() {
		return m_RGBEquation;
	}

	public void setRGBEquation(BlendEquation rGBEquation) {
		m_RGBEquation = rGBEquation;
	}

	public BlendEquation getAlphaEquation() {
		return m_AlphaEquation;
	}

	public void setAlphaEquation(BlendEquation alphaEquation) {
		m_AlphaEquation = alphaEquation;
	}

	public Color getColor() {
		return m_Color;
	}

	public void setColor(Color color) {
		m_Color = color;
	}

	private boolean m_Enabled = false;
	private SourceBlendingFactor m_SourceRGBFactor = null;
	private SourceBlendingFactor m_SourceAlphaFactor = null;
	private DestinationBlendingFactor m_DestinationRGBFactor = null;
	private DestinationBlendingFactor m_DestinationAlphaFactor = null;
	private BlendEquation m_RGBEquation = null;
	private BlendEquation m_AlphaEquation = null;
	private Color m_Color = null;
}