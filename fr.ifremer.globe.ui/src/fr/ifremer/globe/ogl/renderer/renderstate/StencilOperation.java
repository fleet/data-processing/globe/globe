package fr.ifremer.globe.ogl.renderer.renderstate;

public enum StencilOperation {
	Zero, Invert, Keep, Replace, Increment, Decrement, IncrementWrap, DecrementWrap
}