package fr.ifremer.globe.ogl.renderer.renderstate;

public class DepthRange {
	public DepthRange() {
		m_Near = 0.0;
		m_Far = 1.0;
	}

	public double getNear() {
		return m_Near;
	}

	public void setNear(double near) {
		m_Near = near;
	}

	public double getFar() {
		return m_Far;
	}

	public void setFar(double far) {
		m_Far = far;
	}

	private double m_Near = 0;
	private double m_Far = 0;
}