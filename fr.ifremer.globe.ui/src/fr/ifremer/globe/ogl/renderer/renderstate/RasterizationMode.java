package fr.ifremer.globe.ogl.renderer.renderstate;

public enum RasterizationMode {
	Point, Line, Fill
}