package fr.ifremer.globe.ogl.renderer.renderstate;

public class AlphaTest {
	public AlphaTest() {
		m_Enabled = false;
		m_Function = AlphaTestFunction.Less;
		m_Value = 0.0f;
	}

	public AlphaTestFunction getFunction() {
		return m_Function;
	}

	public float getValue() {
		return m_Value;
	}

	public boolean isEnabled() {
		return m_Enabled;
	}

	public void setEnabled(boolean enabled) {
		m_Enabled = enabled;
	}

	public void setFunction(AlphaTestFunction function) {
		m_Function = function;
	}

	public void setValue(float value) {
		m_Value = value;
	}

	private boolean m_Enabled = false;
	private AlphaTestFunction m_Function = null;
	private float m_Value = 0.0f;
}