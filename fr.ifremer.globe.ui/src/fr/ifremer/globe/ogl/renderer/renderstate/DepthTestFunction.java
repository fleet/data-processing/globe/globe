package fr.ifremer.globe.ogl.renderer.renderstate;

public enum DepthTestFunction {
	Never, Less, Equal, LessThanOrEqual, Greater, NotEqual, GreaterThanOrEqual, Always
}