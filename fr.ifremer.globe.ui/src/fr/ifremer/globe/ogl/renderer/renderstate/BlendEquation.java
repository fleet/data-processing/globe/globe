package fr.ifremer.globe.ogl.renderer.renderstate;

public enum BlendEquation {
	Add, Minimum, Maximum, Subtract, ReverseSubtract
}