package fr.ifremer.globe.ogl.renderer.vertexarray;

/**
 * <p>
 * Description : VertexBufferAttributes.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class VertexBufferAttributes implements Iterable<VertexBufferAttribute> {

	public abstract VertexBufferAttribute get(int index);

	public abstract void set(int index, VertexBufferAttribute attribute);

	public abstract int getCount();

	public abstract int getMaximumCount();
}
