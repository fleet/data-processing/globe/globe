package fr.ifremer.globe.ogl.renderer.vertexarray;

import fr.ifremer.globe.ogl.renderer.buffers.VertexBuffer;

/**
 * <p>
 * Description : VertexBufferAttribute.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public class VertexBufferAttribute {

	public VertexBufferAttribute(VertexBuffer vertexBuffer, ComponentDatatype componentDatatype, int numberOfComponents) {
		this(vertexBuffer, componentDatatype, numberOfComponents, false, 0, 0);
	}

	public VertexBufferAttribute(VertexBuffer vertexBuffer, ComponentDatatype componentDatatype, int numberOfComponents, boolean normalize, int offsetInBytes, int strideInBytes) {
		if (numberOfComponents <= 0) {
			throw new IllegalArgumentException("numberOfComponents must be greater than zero.");
		}

		if (offsetInBytes < 0) {
			throw new IllegalArgumentException("offsetInBytes must be greater than or equal to zero.");
		}

		if (strideInBytes < 0) {
			throw new IllegalArgumentException("stride must be greater than or equal to zero.");
		}

		_vertexBuffer = vertexBuffer;
		_componentDatatype = componentDatatype;
		_numberOfComponents = numberOfComponents;
		_normalize = normalize;
		_offsetInBytes = offsetInBytes;

		if (strideInBytes == 0) {
			//
			// Tightly packed
			//
			_strideInBytes = numberOfComponents * VertexArraySizes.SizeOf(componentDatatype);
		} else {
			_strideInBytes = strideInBytes;
		}
	}

	public VertexBuffer getVertexBuffer() {
		return _vertexBuffer;
	}

	public ComponentDatatype getComponentDatatype() {
		return _componentDatatype;
	}

	public int getNumberOfComponents() {
		return _numberOfComponents;
	}

	public boolean getNormalize() {
		return _normalize;
	}

	public int getOffsetInBytes() {
		return _offsetInBytes;
	}

	public int getStrideInBytes() {
		return _strideInBytes;
	}

	private VertexBuffer _vertexBuffer;
	private ComponentDatatype _componentDatatype;
	private int _numberOfComponents;
	private boolean _normalize;
	private int _offsetInBytes;
	private int _strideInBytes;
}
