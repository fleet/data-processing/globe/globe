package fr.ifremer.globe.ogl.renderer.vertexarray;

import fr.ifremer.globe.ogl.renderer.buffers.IndexBuffer;

/**
 * <p>
 * Description : VertexArray.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class VertexArray {

	public VertexBufferAttributes getAttributes() {
		return null;
	}

	public IndexBuffer getIndexBuffer() {
		return null;
	}

	public abstract void setIndexBuffer(IndexBuffer indexBuffer);

	public boolean getDisposeBuffers() {
		return m_DisposeBuffers;
	}

	public void setDisposeBuffers(boolean disposeBuffers) {
		m_DisposeBuffers = disposeBuffers;
	}

	protected abstract void dispose(boolean disposing);

	public void dispose() {
		dispose(true);

	}
	public abstract void unBind();
	private boolean m_DisposeBuffers = false;
}
