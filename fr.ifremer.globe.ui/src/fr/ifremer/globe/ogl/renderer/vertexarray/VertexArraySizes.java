package fr.ifremer.globe.ogl.renderer.vertexarray;

import fr.ifremer.globe.ogl.renderer.buffers.IndexBufferDatatype;
import fr.ifremer.globe.ogl.util.SizeInBytes;

public class VertexArraySizes {
	public static int SizeOf(IndexBufferDatatype type) {
		switch (type) {
		// case UnsignedShort:
		// return sizeof(ushort);
		// case UnsignedInt:
		// return sizeof(uint);
		case Int:
			return SizeInBytes.getSize(Integer.class);
		}

		throw new IllegalArgumentException("type");
	}

	public static int SizeOf(ComponentDatatype type) {
		switch (type) {
		case Byte:
			// case ComponentDatatype.UnsignedByte:
			return SizeInBytes.getSize(Byte.class);
		case Short:
			return SizeInBytes.getSize(Short.class);
			// case ComponentDatatype.UnsignedShort:
			// return sizeof(ushort);
		case Int:
			return SizeInBytes.getSize(Integer.class);
			// case ComponentDatatype.UnsignedInt:
			// return sizeof(uint);
		case Float:
			return SizeInBytes.getSize(Float.class);
			// case ComponentDatatype.HalfFloat:
			// return SizeInBytes<Half>.Value;
		}

		throw new IllegalArgumentException("type");
	}
}