package fr.ifremer.globe.ogl.renderer.vertexarray;

public class VertexLocations {
	public static final int Position = 0;
	public static final int Normal = 2;
	public static final int TextureCoordinate = 3;
	public static final int Color = 4;

	//
	// Having Position and PositionHigh share the same location
	// allows different shaders to share the same vertex array,
	// even if one is using DSFUN90 and one is not.
	//
	// FYI There is/was an ATI bug where location was required:
	//
	// http://www.opengl.org/discussion_boards/ubbthreads.php?ubb=showflat&Number=286280
	//
	public static final int PositionHigh = Position;
	public static final int PositionLow = 1;
}