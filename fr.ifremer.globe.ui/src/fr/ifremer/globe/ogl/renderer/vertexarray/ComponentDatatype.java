package fr.ifremer.globe.ogl.renderer.vertexarray;

/**
 * <p>
 * Description : ComponentDatatype.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public enum ComponentDatatype {

	Byte, Short, Int, Float
}
