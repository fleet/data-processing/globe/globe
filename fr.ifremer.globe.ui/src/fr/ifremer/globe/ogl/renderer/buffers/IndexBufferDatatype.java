package fr.ifremer.globe.ogl.renderer.buffers;

/**
 * <p>
 * Description : IndexBufferDatatype.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public enum IndexBufferDatatype {

	// JOGL ne prend en compte que les int
	// //
	// // OpenGL supports byte indices but D3D does not. We do not use them
	// // because they are unlikely to have a speed or memory benefit:
	// //
	// //
	// http://www.opengl.org/discussion_boards/ubbthreads.php?ubb=showflat&Number=285547
	// //
	//
	// UnsignedShort, UnsignedInt
	Int

}
