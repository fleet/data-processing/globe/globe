package fr.ifremer.globe.ogl.renderer.buffers;

import java.nio.ByteBuffer;

import fr.ifremer.globe.ogl.util.SizeInBytes;

/**
 * <p>
 * Description : UniformBuffer.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class UniformBuffer {

	public void copyFromSystemMemory(ByteBuffer bufferInSystemMemory) {
		copyFromSystemMemory(bufferInSystemMemory, 0);
	}

	public void copyFromSystemMemory(ByteBuffer bufferInSystemMemory, int destinationOffsetInBytes) {
		copyFromSystemMemory(bufferInSystemMemory, destinationOffsetInBytes, SizeInBytes.getSize(bufferInSystemMemory));
	}

	public abstract void copyFromSystemMemory(ByteBuffer bufferInSystemMemory, int destinationOffsetInBytes, int lengthInBytes);

	public ByteBuffer copyToSystemMemory() {
		return copyToSystemMemory(0, getSizeInBytes());
	}

	public abstract ByteBuffer copyToSystemMemory(int offsetInBytes, int sizeInBytes);

	public abstract int getSizeInBytes();

	public abstract BufferHint getUsageHint();
}
