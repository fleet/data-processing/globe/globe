package fr.ifremer.globe.ogl.renderer.buffers;

import java.nio.IntBuffer;

import fr.ifremer.globe.ogl.util.SizeInBytes;

/**
 * <p>
 * Description : IndexBuffer.
 * </p>
 * 
 * <p>
 * Copyright (c) 2013
 * </p>
 * 
 * <p>
 * Société : IPSIS
 * </p>
 * 
 * <p>
 * Date : 16 janv. 2013
 * </p>
 * 
 * @author Antoine CAILLY
 * @version 1.0
 */
public abstract class IndexBuffer {

	/**
	 * Logger.
	 */
	// private static final Logger LOGGER = Logger.getLogger(IndexBuffer.class);

	public void copyFromSystemMemory(IntBuffer bufferInSystemMemory) {
		copyFromSystemMemory(bufferInSystemMemory, 0);
	}

	public void copyFromSystemMemory(IntBuffer bufferInSystemMemory, int destinationOffsetInBytes) {
		copyFromSystemMemory(bufferInSystemMemory, destinationOffsetInBytes, SizeInBytes.getSize(bufferInSystemMemory));
	}

	public abstract void copyFromSystemMemory(IntBuffer bufferInSystemMemory, int destinationOffsetInBytes, int lengthInBytes);

	public IntBuffer copyToSystemMemory() {
		return copyToSystemMemory(0, getSizeInBytes());
	}

	public abstract IntBuffer copyToSystemMemory(int offsetInBytes, int sizeInBytes);

	public abstract int getSizeInBytes();

	public abstract IndexBufferDatatype getDatatype();

	public abstract BufferHint getUsageHint();

	protected abstract void dispose(boolean disposing);

	public void dispose() {
		dispose(true);

	}
}
