package fr.ifremer.globe.ogl.renderer.buffers;

/**
 * <p>
 * Description : BufferHint.
 * 
 * <pre>
 * A hint to the GL implementation as to how a buffer object's data store will be accessed. 
 * This enables the GL implementation to make more intelligent decisions that may significantly impact buffer object performance. 
 * It does not, however, constrain the actual usage of the data store. usage can be broken down into two parts: 
 * first, the frequency of access (modification and usage), and second, the nature of that access. 
 * 
 * The frequency of access may be one of these:
 * STREAM
 *The data store contents will be modified once and used at most a few times.
 *
 *STATIC
 *The data store contents will be modified once and used many times.
 *
 *DYNAMIC
 *The data store contents will be modified repeatedly and used many times.
 *
 *The nature of access may be one of these:
 *
 *DRAW
 *The data store contents are modified by the application, and used as the source for GL drawing and image specification commands.
 *
 *READ
 *The data store contents are modified by reading data from the GL, and used to return that data when queried by the application.
 *
 *COPY
 *The data store contents are modified by reading data from the GL, and used as the source for GL drawing and image specification commands.
 *
 * </pre>
 * </p>
 * 
 */
public enum BufferHint {

	StreamDraw, StreamRead, StreamCopy, StaticDraw, StaticRead, StaticCopy, DynamicDraw, DynamicRead, DynamicCopy

}
