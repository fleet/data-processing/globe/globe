package fr.ifremer.globe.ogl;

import java.awt.Font;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;

import com.jogamp.opengl.util.awt.TextRenderer;


/**
 * Defines an abstract class for GLComponent
 * 
 * @author bvalliere@ipsis.com
 */
public abstract class AbstractGLComponent implements GLComponent {

	//private static final Logger logger = LoggerFactory.getLogger(AbstractGLComponent.class);

	/** Text rendering */
	protected GLU glu = new GLU();

	protected double txtCoord[] = new double[4];
	/** Project / unproject */
	protected int viewport[] = new int[4];

	protected double mvmatrix[] = new double[16];
	protected double projmatrix[] = new double[16];
	protected double coordNear[] = new double[4];

	protected double coordFar[] = new double[4];

	/** Vertices VBO */
	protected VBO verticesVBO = null;

	/** Colors VBO */
	protected VBO colorsVBO = null;

	protected boolean needUpdate = true;

	private boolean visible = true;

	/** Small size renderer (must be created while drawing) */
	private TextRenderer smallRenderer = null;
	
	/** Medium size renderer (must be created while drawing) */
	private TextRenderer renderer = null;

	@Override
	public abstract void draw(GLAutoDrawable glad);

	public void invalidate() {
		needUpdate = true;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	protected TextRenderer getSmallRenderer() {
		if (smallRenderer == null) {
			smallRenderer = new TextRenderer(new Font("SansSerif", Font.BOLD, 10));
		}
		return smallRenderer;
	}

	protected TextRenderer getRenderer() {
		if (renderer == null) {
			renderer = new TextRenderer(new Font("SansSerif", Font.BOLD, 12));
		}
		return renderer;
	}

	/**
	 * Converts screen coordinates to scene coordinates
	 * 
	 * @param glad
	 * @param x
	 * @param y
	 * @param coords
	 */
	protected void fromScreenToScene(GLAutoDrawable glad, int x, int y, double[] coords) {
		GL2 gl2 = glad.getGL().getGL2();
		int h = glad.getSurfaceHeight();
		gl2.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		gl2.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl2.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
		glu.gluUnProject(x, h - y, 0.5, mvmatrix, 0, projmatrix, 0, viewport, 0, coords, 0);
	}

	/**
	 * Converts scene coordinates to screen coordinates
	 * 
	 * @param glad
	 * @param x
	 * @param y
	 * @param z
	 * @param coords
	 */
	protected void fromSceneToScreen(GLAutoDrawable glad, double x, double y, double z, double[] coords) {
		GL2 gl2 = glad.getGL().getGL2();
		gl2.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		gl2.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl2.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
		glu.gluProject(x, y, z, mvmatrix, 0, projmatrix, 0, viewport, 0, coords, 0);
		int h = glad.getSurfaceHeight();
		coords[1] = h - coords[1];
	}
}
