package fr.ifremer.globe.gws.server.process.custom;

import java.io.File;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.program.Program;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.fleet.gws.rsocket.flatbuffers.StringListPayload;
import fr.ifremer.globe.core.runtime.gws.GwsServiceAgent;
import fr.ifremer.globe.core.runtime.gws.param.EstimateTideParams;
import fr.ifremer.globe.core.runtime.gws.param.ExportToMbgParams;
import fr.ifremer.globe.core.runtime.gws.param.ExportToNviParams;
import fr.ifremer.globe.core.runtime.gws.param.ExportToXsfParams;
import fr.ifremer.globe.core.runtime.gws.param.MergeXsfParams;
import fr.ifremer.globe.core.runtime.gws.param.MigrateXsfParams;
import fr.ifremer.globe.core.runtime.gws.param.PredictFesHarmonicTideParams;
import fr.ifremer.globe.core.runtime.gws.param.PredictShomHarmonicTideParams;
import fr.ifremer.globe.core.runtime.gws.param.SmootherParams;
import fr.ifremer.globe.core.runtime.gws.param.turnfilter.TurnFilterParams;
import fr.ifremer.globe.gws.ContextInitializer;
import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.process.driver.xsf.XsfDriverProcess;
import fr.ifremer.globe.gws.server.process.function.AbstractFunction;
import fr.ifremer.globe.gws.server.process.function.PayloadTranslator;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Allow access to specific services defined in GWS
 */
@Component(name = "globe_gws_service_agent", service = GwsServiceAgent.class)
public class ServiceAgentImpl implements GwsServiceAgent {

	private static final String COMPUTE_CUT_FILE_FROM_MBG_MASK = "app/function/conf/mask_mbg_to_cut.json";

	private static final Logger logger = LoggerFactory.getLogger(GwsServerProxy.class);

	/** Instance of XsfDriverDelegateProcess in GWS server. */
	private XsfDriverProcess xsfDriverProcess;
	
	@Override
	public void openHelp(String serviceName) {
		try {
			GwsService service = findService(serviceName);
			String help = GwsServerProxy.grab().detailService(service.id()).map(GwsService::help).orElse(null);
			if (help != null && !help.isEmpty()) {
				File helpFile = TemporaryCache.createTemporaryFile("help_", ".html");
				helpFile.deleteOnExit();
				Files.writeString(helpFile.toPath(), help);
				if (Program.launch(helpFile.getPath()))
					return;
			}
		} catch (Exception e) {
			logger.warn("Unable to open help page for '" + serviceName + "'", e);
		}
		Messages.openErrorMessage("Help", "Unable to open help page for '" + serviceName + "'");
	}

	/** {@inheritDoc} */
	@Override
	public IStatus exportToNvi(ExportToNviParams params, IProgressMonitor monitor, Logger logger) throws GIOException {
		GwsService service = findService(ExportToNviProcess.SERVICE_NAME);
		ExportToNviProcess exportToNviProcess = new ExportToNviProcess(service, params);
		ContextInitializer.inject(exportToNviProcess);
		return exportToNviProcess.launchNoUI(monitor, logger);
	}

	@Override
	public IStatus exportToXsf(ExportToXsfParams params, IProgressMonitor monitor, Logger logger) throws GException {
		GwsService service = findService(ExportToXsfProcess.SERVICE_NAME);
		ExportToXsfProcess exportToXsfProcess = new ExportToXsfProcess(service, params);
		ContextInitializer.inject(exportToXsfProcess);
		return exportToXsfProcess.launchNoUI(monitor, logger);
	}

	@Override
	public IStatus exportToMbg(ExportToMbgParams params, IProgressMonitor monitor, Logger logger) throws GException {
		GwsService service = findService(ExportToMbgProcess.SERVICE_NAME);
		ExportToMbgProcess exportToMbgProcess = new ExportToMbgProcess(service, params);
		ContextInitializer.inject(exportToMbgProcess);
		return exportToMbgProcess.launchNoUI(monitor, logger);
	}

	/** {@inheritDoc} */
	@Override
	public List<String> computeCutFileFromMbgMask(List<File> mbgFiles) throws GIOException {
		var function = new AbstractFunction<Consumer<String>>(Optional.empty(), COMPUTE_CUT_FILE_FROM_MBG_MASK) {

			/** Custom PayloadTranslator to consume the cut lines */
			@Override
			protected PayloadTranslator<Consumer<String>> getPayloadTranslator(Consumer<String> valueConsumer) {
				return new PayloadTranslator<>(valueConsumer) {
					@Override
					public void acceptStringListPayload(StringListPayload payload) {
						for (int i = 0; i < payload.stringsLength(); i++)
							valueConsumer.accept(payload.strings(i));
					}
				};
			}

			/** Overload to set the input MBG files */
			@Override
			protected boolean editArguments(GwsServiceConf functionConf) {
				functionConf.getInputFilesArgument()
						.ifPresent(arg -> new ArgumentValueSetter().setFiles(mbgFiles, arg));
				return super.editArguments(functionConf);
			}
		};

		List<String> result = new LinkedList<>();
		function.accept(result::add);

		return result;
	}

	@Override
	public IStatus mergeXsf(MergeXsfParams params, IProgressMonitor monitor, Logger logger) throws GException {
		GwsService service = findService(CutMergeXsfProcess.SERVICE_NAME);
		CutMergeXsfProcess process = new CutMergeXsfProcess(service, params);
		ContextInitializer.inject(process);
		return process.launchNoUI(monitor, logger);
	}

	@Override
	public IStatus migrateXsf(MigrateXsfParams params, IProgressMonitor monitor, Logger logger) throws GException {
		GwsService service = findService(MigrateXsfProcess.SERVICE_NAME);
		MigrateXsfProcess process = new MigrateXsfProcess(service, params);
		ContextInitializer.inject(process);
		return process.launchNoUI(monitor, logger);
	}

	private GwsService findService(String serviceName) throws GIOException {
		GwsServerProxy gwsServerProxy = GwsServerProxy.grab();
		return gwsServerProxy.findService(serviceName)//
				.orElseThrow(() -> new GIOException("Service '" + serviceName + "' not defined in GWS configuration."));
	}

	@Override
	public IStatus estimateNavigationTide(EstimateTideParams params, IProgressMonitor monitor, Logger logger)
			throws GException {
		GwsService service = findService(EstimateTideProcess.ESTIMATE_NAV_PYAT_SERVICE_NAME);
		EstimateTideProcess process = new EstimateTideProcess(service, params);
		ContextInitializer.inject(process);
		return process.launchNoUI(monitor, logger);
	}

	@Override
	public IStatus estimateXsfNmeaTide(EstimateTideParams params, IProgressMonitor monitor, Logger logger)
			throws GException {
		GwsService service = findService(EstimateTideProcess.ESTIMATE_XSFNMEA_PYAT_SERVICE_NAME);
		EstimateTideProcess process = new EstimateTideProcess(service, params);
		ContextInitializer.inject(process);
		return process.launchNoUI(monitor, logger);
	}

	@Override
	public IStatus predictShomHarmonicTide(PredictShomHarmonicTideParams params, IProgressMonitor monitor,
			Logger logger) throws GException {
		GwsService service = findService(PredictShomHarmonicTideProcess.SERVICE_NAME);
		PredictShomHarmonicTideProcess process = new PredictShomHarmonicTideProcess(service, params);
		ContextInitializer.inject(process);
		return process.launchNoUI(monitor, logger);
	}

	@Override
	public IStatus predictFesHarmonicTide(PredictFesHarmonicTideParams params, IProgressMonitor monitor, Logger logger)
			throws GException {
		GwsService service = findService(PredictFesHarmonicTideProcess.SERVICE_NAME);
		PredictFesHarmonicTideProcess process = new PredictFesHarmonicTideProcess(service, params);
		ContextInitializer.inject(process);
		return process.launchNoUI(monitor, logger);
	}

	@Override
	public IStatus smoother(SmootherParams params, IProgressMonitor monitor, Logger logger) throws GException {
		GwsService service = findService(SmootherProcess.SERVICE_NAME);
		SmootherProcess process = new SmootherProcess(service, params);
		ContextInitializer.inject(process);
		return process.launchNoUI(monitor, logger);
	}

	@Override
	public IStatus filterTurn(TurnFilterParams params, IProgressMonitor monitor, Logger logger) throws GException {
		GwsService service = findService(TurnFilterProcess.SERVICE_NAME);
		TurnFilterProcess process = new TurnFilterProcess(service, params);
		ContextInitializer.inject(process);
		return process.launchNoUI(monitor, logger);
	}

	/**
	 * @param xsfDriverDelegateProcess the {@link #xsfDriverDelegateProcess} to set
	 */
	public void setXsfDriverProcess(XsfDriverProcess xsfDriverProcess) {
		this.xsfDriverProcess = xsfDriverProcess;
	}

	/**
	 * @return the {@link #xsfDriverProcess}
	 */
	public XsfDriverProcess getXsfDriverProcess() {
		return xsfDriverProcess;
	}

}
