package fr.ifremer.globe.gws.server.process.driver.xsf;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.fleet.gws.rsocket.flatbuffers.XsfAcquisitionMode;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderNavigationData;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * 
 */
public class XsfNavigationDataExtended extends SounderNavigationData {
	
	/** List of classic {@link NavigationMetadataType} **/
	public final static NavigationMetadataType<Integer> MODE = new NavigationMetadataType<>("Acquisition Mode", "", XsfNavigationDataExtended::getAcquisitionModeName);
	
	private static final Logger logger = LoggerFactory.getLogger(XsfNavigationDataExtended.class);

	/** Instance of XsfDriver in GWS server. */
	private WritableObject<XsfDriverProcess> xsfDriverProcess;

	protected boolean acquisitionModeLoaded = false;
	protected Optional<XsfAcquisitionMode> acquisitionModeLayer = Optional.empty();
	static protected List<String> globalModeNames = new ArrayList<String>();
	protected List<Integer> fileModeIndexMapping = new ArrayList<>();

	/** Constructor **/
	public XsfNavigationDataExtended(ISounderNcInfo info, boolean readonly, WritableObject<XsfDriverProcess> xsfDriverProcess) throws GIOException {
		super(info,readonly);
		this.xsfDriverProcess = xsfDriverProcess;
		extendMetadata();
	}
	
	/**
	 * Defines additional {@link NavigationMetadata} for XSF navigation.
	 */
	private void extendMetadata() throws GIOException {
		// try to find known metadata
		metadata.add(MODE.withValueSupplier(this::getAcquisitionMode));
	}
	
	public int getAcquisitionMode(int index) {
		if(!acquisitionModeLoaded) {
			loadAcquisitionMode();
			logger.debug(globalModeNames.toString());
		}
		if(acquisitionModeLayer.isPresent()) {
			return fileModeIndexMapping.get(acquisitionModeLayer.get().modeIndex(indexes.get(index)));
		}
		return -1;
	}

	public static String getAcquisitionModeName(int globalModeIndex) {
		if(globalModeIndex >= 0 & globalModeIndex < globalModeNames.size()) {
			return globalModeNames.get(globalModeIndex);
		}
		return "Unknown";
	}
	
	
	/**
	 * Load acquisition mode of the file and map local index with a global dictionary of modes.
	 * In case of multiping acquisition, modes from different swath index / frequency planes are merged together
	 */
	private void loadAcquisitionMode() {
		try {
			if (xsfDriverProcess.isNotNull()) {
				acquisitionModeLoaded = true;
				acquisitionModeLayer = xsfDriverProcess.get().fetchAcquisitionMode(getFileName())
						.map(acquisitionMode -> {
							for(int i = 0; i < acquisitionMode.modeNameLength(); i++) {
								String nameWithoutSwathIndex = acquisitionMode.modeName(i).replaceAll("\\(.*\\)", "");
								int nameIndex = globalModeNames.indexOf(nameWithoutSwathIndex);
								if(nameIndex < 0) {
									nameIndex = globalModeNames.size();
									globalModeNames.add(nameWithoutSwathIndex);
								}
								fileModeIndexMapping.add(nameIndex);
							}
							return acquisitionMode;})
						.onErrorComplete()//
						.blockOptional(Duration.ofSeconds(60));
			}
		} catch (IllegalStateException e) {
			logger.warn(e.getMessage());
		}
	}

}
