package fr.ifremer.globe.gws.server.process;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.gws.ContextInitializer;
import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueGetter;
import fr.ifremer.globe.gws.server.service.json.JsonArgumentDeserializer;
import fr.ifremer.globe.gws.server.service.json.JsonArgumentSerializer;
import fr.ifremer.globe.gws.ui.wizard.WizardAdapter;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.cache.WorkspaceCache;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import jakarta.inject.Inject;

/**
 * Launcher of a GWS service.
 */
public class ServiceLauncher {

	private static final Logger logger = LoggerFactory.getLogger(ServiceLauncher.class);

	/** Cache folder for all argument files */
	public static final File CACHE_DIR = WorkspaceCache.getCacheDirectory("GwsToolboxExplorer");

	@Inject
	private IFileLoadingService fileLoadingService;
	@Inject
	private ITreeNodeFactory treeNodeFactory;
	@Inject
	private GwsServerProxy gwsServerProxy;

	/** GWS service to launch */
	private GwsService gwsServiceToLaunch;

	/**
	 * Constructor
	 */
	public ServiceLauncher(GwsService gwsService) {
		gwsServiceToLaunch = gwsService;
	}

	/**
	 * Launch the service. <br>
	 * <ul>
	 * <li>Retrieve the service configuration</li>
	 * <li>Parse the configuration (json)</li>
	 * <li>Restore previous argument values from the cache</li>
	 * <li>Edit the configuration with a Wizard</li>
	 * <li>Save the edited argument values in the cache</li>
	 * <li>Ask the GWS server to execute the service with the new configuration</li>
	 * </ul>
	 */
	public void launch(Shell shell) {
		gwsServerProxy.detailService(gwsServiceToLaunch.id()).ifPresent(detailedService -> {
			gwsServiceToLaunch = detailedService;
			doLaunch(shell);
		});

	}

	/**
	 * Launch the service. {@link #gwsServiceToLaunch} is now a fully loaded GwsService
	 * <ul>
	 * <li>Restore previous argument values from the cache</li>
	 * <li>Edit the configuration with a Wizard</li>
	 * <li>Save the edited argument values in the cache</li>
	 * <li>Ask the GWS server to execute the service with the new configuration</li>
	 * </ul>
	 */
	private void doLaunch(Shell shell) {
		try {
			GwsServiceConf configuration = gwsServiceToLaunch.description();

			File argumentFile = gainArgumentCacheFile();
			restoreArguments(configuration, argumentFile);

			aboutToEditArgument(configuration);

			// Edit configuration
			WizardAdapter wizardAdapter = new WizardAdapter(configuration,
					Optional.ofNullable(gwsServiceToLaunch.helpPath()), shell);
			if (openWizard(wizardAdapter)) { // OK pressed...
				// Launch the service execution
				ServiceProcess process = aboutToCreateServiceProcess(gwsServiceToLaunch, configuration);

				aboutToSetPayloadIntercepter(Optional.of(wizardAdapter), process);

				// Allow to apply actions just before launching service
				aboutToRunServiceProcess(process, configuration);

				// save after all configuration modifications
				if (saveArguments(configuration, argumentFile)) {
					process.whenIsDone(e -> onServiceFinished(configuration));
					process.runInBackground(true);
				}
			}
		} catch (GException | IllegalArgumentException e) {
			logger.error("Error in service execution (" + gwsServiceToLaunch.name() + ")", e);
		}
	}

	/**
	 * Launch the service in a no UI mode (there is no UI interaction, no wizard, no console view). <br>
	 * <ul>
	 * <li>Retrieve the service configuration</li>
	 * <li>Parse the configuration (json)</li>
	 * <li>Restore previous argument values from the cache</li>
	 * <li>Save the edited argument values in the cache</li>
	 * <li>Ask the GWS server to execute the service with the new configuration</li>
	 * </ul>
	 */
	public IStatus launchNoUI(IProgressMonitor monitor, Logger log) {
		Optional<GwsService> details = gwsServerProxy.detailService(gwsServiceToLaunch.id());
		if (details.isPresent()) {
			GwsService gwsService = details.get();
			// Decode the description of the service to get all arguments.
			try {
				GwsServiceConf configuration = gwsService.description();

				// Allow sub-launcher to complete the configuration
				aboutToEditArgument(configuration);

				// Creates the service
				ServiceProcess process = aboutToCreateServiceProcess(gwsService, configuration);

				// By default, process is not recorded in logbook when there is no UI
				process.setRecordInLogBook(false);

				// Allow sub-launcher to choose the PayloadIntercepter
				aboutToSetPayloadIntercepter(process);

				// Allow to apply actions just before launching service
				aboutToRunServiceProcess(process, configuration);

				process.whenIsDone(e -> onServiceFinished(configuration));
				// Launching
				return runTheProcess(process, monitor, log);
			} catch (IllegalArgumentException | GException e) {
				return Status.error("Error in service execution (" + gwsService.name() + ")", e);
			}
		}
		return Status.error("GWS not available");
	}

	/**
	 * Launches the service with a progress dialog (but no wizard to set parameters).
	 */
	public void launchInForeground() {
		IProcessService.grab().runInForeground(gwsServiceToLaunch.name(), this::launchNoUI);
	}

	/** Restore previous arguments from cache */
	protected void restoreArguments(GwsServiceConf configuration, File argumentFile) throws GException {
		try {
			if (argumentFile.exists())
				new JsonArgumentDeserializer().deserializeValues(configuration, argumentFile, false);
		} catch (IOException e) {
			throw GIOException.wrap(e.getMessage(), e);
		}
	}

	/** Possible reaction of the sub-class, just before the wizard is opened */
	protected void aboutToEditArgument(GwsServiceConf configuration) throws GException {
		// Nothing to do by default
	}

	/** Creates the instance of the ServiceProcess */
	protected ServiceProcess aboutToCreateServiceProcess(GwsService gwsService, GwsServiceConf configuration) {
		ServiceProcess result = new ServiceProcess(gwsService, configuration);
		ContextInitializer.inject(result);
		return result;
	}

	/**
	 * Possible reaction of the sub-class, just before the process is launched
	 * 
	 * @throws GException
	 */
	protected void aboutToRunServiceProcess(ServiceProcess gwsService, GwsServiceConf configuration) throws GException {
		// Unload output files from project explorer to avoid write access issue in GWS process
		unloadOutputFiles(configuration);
	}

	/**
	 * After the wizard closing and configuration modifications done, save the arguments in cache
	 * 
	 * @return false if service must abort
	 */
	protected boolean saveArguments(GwsServiceConf configuration, File argumentFile) throws GException {
		try {
			new JsonArgumentSerializer().serialize(configuration.arguments(), argumentFile);

		} catch (IOException e) {
			throw GIOException.wrap(e.getMessage(), e);
		}

		// Check if non ascii character is present
		try {
			Files.readAllLines(argumentFile.toPath(), StandardCharsets.US_ASCII);
		} catch (IOException e) {
			String message = """
					Input parameters contain non-ASCII characters.
					Files whose path contains special characters or accents may not be openable (especially netCDF files).
					Run the process anyway ?
					""";
			return Messages.openSyncQuestionMessage("Warning : non ASCII character in parameters", message);
		}
		return true;
	}

	protected void aboutToSetPayloadIntercepter(ServiceProcess process) {
		aboutToSetPayloadIntercepter(Optional.empty(), process);
	}

	/** By default, intercepts file payloads to load them in the project explorer */
	protected void aboutToSetPayloadIntercepter(Optional<WizardAdapter> wizardAdapter, ServiceProcess process) {
		wizardAdapter.ifPresent(wizard -> {
			if (wizard.haveToLoadFilesAfter()) {
				process.setPayloadIntercepter(PayloadIntercepter
						.forFileList(files -> loadResultingFile(files, wizard.getWhereToloadFiles())));
			}
		});
	}

	/**
	 * By default, the service is run in a synchronous way (Returned at the end of the process).<br>
	 * It is possible to change this behavior by overloading onServiceFinished and changing the value of
	 */
	protected IStatus runTheProcess(ServiceProcess process, IProgressMonitor monitor, Logger log) {
		return process.apply(monitor, log);
	}

	/** Cleaning entry point after service execution */
	protected void onServiceFinished(GwsServiceConf configuration) {
		logger.info("Service '{}' finished", configuration.name());
	}

	/**
	 * Open the wizard to edit the argument.<br>
	 * Subclass can redefine this method to interfere in the argument value before or after the edition
	 */
	protected boolean openWizard(WizardAdapter wizardAdapter) {
		return wizardAdapter.openWizard();
	}

	/** React to the reception of files. */
	protected void loadResultingFile(List<File> files, String targetNodePath) {
		treeNodeFactory.createFileInfoNode(files.stream().map(File::getPath).toList(), targetNodePath);
		fileLoadingService.reload(files.toArray(File[]::new));
	}

	/** Unload output files if exist in project explorer */
	protected void unloadOutputFiles(GwsServiceConf configuration) {
		configuration.getOutputFilesArgument().ifPresent(outputfileArg -> {
			ArgumentValueGetter getter = new ArgumentValueGetter();
			List<File> outFiles = getter.getFiles(outputfileArg.getValue());
			fileLoadingService.unload(outFiles.toArray(File[]::new));
		});
	}

	/** Deduce the name of the argument file in the cache folder */
	private File gainArgumentCacheFile() {
		return new File(CACHE_DIR, gwsServiceToLaunch.name().replaceAll("\\W+", "_") + ".json");

	}
}