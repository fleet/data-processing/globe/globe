package fr.ifremer.globe.gws.server.process.custom;

import fr.ifremer.globe.core.runtime.gws.param.ExportToMbgParams;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.process.ServiceProcess;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.utils.exception.GException;
import jakarta.inject.Inject;

/**
 * GWS service to converting files in MBG format.
 */
public class ExportToMbgProcess extends ServiceLauncher {

	public static final String SERVICE_NAME = "Convert sounding file to MBG";

	/** Parameters to set */
	private final ExportToMbgParams params;

	@Inject
	private IFileLoadingService fileLoadingService;

	/**
	 * Constructor
	 */
	public ExportToMbgProcess(GwsService specificService, ExportToMbgParams params) {
		super(specificService);
		this.params = params;
	}

	/** Overloaded for setting some arguments before launching the service */
	@Override
	protected void aboutToEditArgument(GwsServiceConf configuration) throws GException {
		ArgumentValueSetter setter = new ArgumentValueSetter();
		setter.setString("mbg", configuration.getArgumentOfNameOrThrow("fmt"));
		setter.setFile(params.in(), configuration.getArgumentOfNameOrThrow("in"));
		setter.setFile(params.out(), configuration.getArgumentOfNameOrThrow("out"));
		setter.setBoolean(params.overwrite(), configuration.getArgumentOfNameOrThrow("overwrite"));
		setter.setString(params.mbgCdi(), configuration.getArgumentOfNameOrThrow("mbgCdi"));
		setter.setString(params.mbgReference(), configuration.getArgumentOfNameOrThrow("mbgReference"));
		setter.setString(params.mbgShipName(), configuration.getArgumentOfNameOrThrow("mbgShipName"));
		setter.setString(params.mbgSurveyName(), configuration.getArgumentOfNameOrThrow("mbgSurveyName"));

		super.aboutToEditArgument(configuration);
	}

	@Override
	protected void aboutToRunServiceProcess(ServiceProcess process, GwsServiceConf configuration) throws GException {
		// MBG conversion always recorded in the logbook
		process.setRecordInLogBook(true);
		// Unload output file
		fileLoadingService.unload(params.out());
	}

}
