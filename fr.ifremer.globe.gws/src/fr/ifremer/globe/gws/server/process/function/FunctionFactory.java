package fr.ifremer.globe.gws.server.process.function;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import fr.ifremer.fleet.gws.rsocket.flatbuffers.MapOfDoubleEntry;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.MapOfDoublePayload;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.StringListPayload;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.utils.function.MapOfDoubleConsumer;

/**
 * Factory of AbstractFunction.
 */
public class FunctionFactory {

	/**
	 * Creates a function that computes and returns a map of double. <br>
	 * When consumed, this object will execute the service designated by its name, intercept the doubles and then
	 * translate it to a map of double
	 */
	public static AbstractFunction<MapOfDoubleConsumer> forMapOfDouble(Optional<GwsServiceConf> callingService,
			String functionName) {
		return new AbstractFunction<MapOfDoubleConsumer>(callingService, functionName) {

			@Override
			protected PayloadTranslator<MapOfDoubleConsumer> getPayloadTranslator(MapOfDoubleConsumer valueConsumer) {
				return new PayloadTranslator<>(valueConsumer) {
					@Override
					public void acceptMapOfDoublePayload(MapOfDoublePayload payload) {
						Map<String, Double> result = new HashMap<>();
						for (int i = 0; i < payload.entriesLength(); i++) {
							MapOfDoubleEntry entry = payload.entries(i);
							result.put(entry.key(), entry.value());
						}
						valueConsumer.accept(result);
					}
				};
			}
		};
	}

	/**
	 * Creates a function that computes and returns a list of String.
	 */
	public static AbstractFunction<Consumer<List<String>>> forListOfString(Optional<GwsServiceConf> callingService,
			String functionName) {
		return new AbstractFunction<Consumer<List<String>>>(callingService, functionName) {

			@Override
			protected PayloadTranslator<Consumer<List<String>>> getPayloadTranslator(
					Consumer<List<String>> valueConsumer) {
				return new PayloadTranslator<>(valueConsumer) {
					@Override
					public void acceptStringListPayload(StringListPayload payload) {
						List<String> result = new LinkedList<>();
						for (int i = 0; i < payload.stringsLength(); i++)
							result.add(payload.strings(i));
						valueConsumer.accept(result);
					}
				};
			}
		};
	}

	/**
	 * Constructor
	 */
	private FunctionFactory() {
		super();
	}

}
