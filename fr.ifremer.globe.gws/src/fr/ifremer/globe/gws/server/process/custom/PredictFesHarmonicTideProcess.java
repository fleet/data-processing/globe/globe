package fr.ifremer.globe.gws.server.process.custom;

import fr.ifremer.globe.core.runtime.gws.param.PredictFesHarmonicTideParams;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.utils.exception.GException;

/**
 * GWS service to predict FES2014 harmonic tide.
 */
public class PredictFesHarmonicTideProcess extends ServiceLauncher {

	public static final String SERVICE_NAME = "FES2014 harmonic tide predictor";

	/** Parameters to set */
	private final PredictFesHarmonicTideParams params;

	/**
	 * Constructor
	 */
	public PredictFesHarmonicTideProcess(GwsService specificService, PredictFesHarmonicTideParams params) {
		super(specificService);
		this.params = params;
	}

	/** Overloaded for setting some arguments before launching the service */
	@Override
	protected void aboutToEditArgument(GwsServiceConf configuration) throws GException {
		ArgumentValueSetter setter = new ArgumentValueSetter();
		setter.setFile(params.dataFile(), configuration.getArgumentOfNameOrThrow("data_file"));
		setter.setFile(params.outputFile(), configuration.getArgumentOfNameOrThrow("output_file"));
		setter.setFile(params.modelDir(), configuration.getArgumentOfNameOrThrow("model_dir"));
		super.aboutToEditArgument(configuration);
	}

}
