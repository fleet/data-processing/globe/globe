package fr.ifremer.globe.gws.server.process.custom;

import fr.ifremer.globe.core.runtime.gws.param.ExportToXsfParams;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.process.ServiceProcess;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.utils.exception.GException;
import jakarta.inject.Inject;

/**
 * GWS service to migrate XSF files.
 */
public class ExportToXsfProcess extends ServiceLauncher {

	public static final String SERVICE_NAME = "Convert sounding file to XSF";

	/** Parameters to set */
	private final ExportToXsfParams params;

	@Inject
	private IFileLoadingService fileLoadingService;

	/**
	 * Constructor
	 */
	public ExportToXsfProcess(GwsService specificService, ExportToXsfParams params) {
		super(specificService);
		this.params = params;
	}

	/** Overloaded for setting some arguments before launching the service */
	@Override
	protected void aboutToEditArgument(GwsServiceConf configuration) throws GException {
		ArgumentValueSetter setter = new ArgumentValueSetter();
		setter.setString("xsf", configuration.getArgumentOfNameOrThrow("fmt"));
		setter.setFile(params.in(), configuration.getArgumentOfNameOrThrow("in"));
		setter.setFile(params.out(), configuration.getArgumentOfNameOrThrow("out"));
		setter.setBoolean(params.overwrite(), configuration.getArgumentOfNameOrThrow("overwrite"));
		setter.setString(params.xsfKeywords(), configuration.getArgumentOfNameOrThrow("xsfKeywords"));
		setter.setString(params.xsfLicense(), configuration.getArgumentOfNameOrThrow("xsfLicense"));
		setter.setString(params.xsfRights(), configuration.getArgumentOfNameOrThrow("xsfRights"));
		setter.setString(params.xsfSummary(), configuration.getArgumentOfNameOrThrow("xsfSummary"));
		setter.setString(params.xsfTitle(), configuration.getArgumentOfNameOrThrow("xsfTitle"));
		setter.setBoolean(params.ignoreWC(), configuration.getArgumentOfNameOrThrow("ignoreWC"));

		super.aboutToEditArgument(configuration);
	}
	
	@Override
	protected void aboutToRunServiceProcess(ServiceProcess process, GwsServiceConf configuration) throws GException {
		// XSF conversion always recorded in the logbook
		process.setRecordInLogBook(true);
		// Unload output file
		fileLoadingService.unload(params.out());
	}
}
