package fr.ifremer.globe.gws.server.process.driver.xsf;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.NavigationSupplierPriority;
import fr.ifremer.globe.core.io.xsf.info.XsfInfo;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationSupplier;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;

/**
 * Service providing informations on XSF files.
 */
@Component(name = "globe_gws_xsf_navigation_supplier", //
		service = { INavigationSupplier.class, XsfNavigationSupplierExtended.class }, //
		property = { NavigationSupplierPriority.PROPERTY_PRIORITY + ":Integer="
				+ NavigationSupplierPriority.EXTENDED_GWS_PRIORITY })
public class XsfNavigationSupplierExtended implements INavigationSupplier {

	/** Logger */
	public static final Logger LOGGER = LoggerFactory.getLogger(XsfNavigationSupplierExtended.class);

	@Reference
	private ISessionService sessionService;

	/** Instance of XsfDriver in GWS server. */
	private WritableObject<XsfDriverProcess>  xsfDriverProcess = new WritableObject<XsfDriverProcess>(null);

	/**
	 * @param xsfDriverProcess the {@link #xsfDriverProcess} to set
	 */
	public void setXsfDriverProcess(XsfDriverProcess xsfDriverProcess) {
		this.xsfDriverProcess.set(xsfDriverProcess);
	}

	/**
	 * @return the {@link #xsfDriverProcess}
	 */
	public XsfDriverProcess getXsfDriverProcess() {
		return xsfDriverProcess.get();
	}

	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(IFileInfo fileInfo) {
		if (fileInfo instanceof XsfInfo xsfInfo) {
			return Optional.of(accessMode -> { //
				INavigationData navigationData = new XsfNavigationDataExtended(xsfInfo, accessMode, xsfDriverProcess);
				return navigationData;
			});
		}
		return Optional.empty();
	}

	/**
	 * Supply a INavigationDataSupplier on the specified of navigation file.
	 * 
	 * @return the INavigationDataSupplier. Optional.empty if none
	 */
	@Override
	public Optional<INavigationDataSupplier> getNavigationDataSupplier(String filePath) {
		return IFileService.grab().getFileInfo(filePath).flatMap(this::getNavigationDataSupplier);
	}
}