package fr.ifremer.globe.gws.server.process.custom;

import fr.ifremer.globe.core.runtime.gws.param.SmootherParams;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.utils.exception.GException;

/**
 * Smoother GWS service.
 */
public class SmootherProcess extends ServiceLauncher {

	public static final String SERVICE_NAME = "Smoother";

	/** Parameters to set */
	private final SmootherParams params;

	/**
	 * Constructor
	 */
	public SmootherProcess(GwsService specificService, SmootherParams params) {
		super(specificService);
		this.params = params;
	}

	/** Overloaded for setting some arguments before launching the service */
	@Override
	protected void aboutToEditArgument(GwsServiceConf configuration) throws GException {
		ArgumentValueSetter setter = new ArgumentValueSetter();
		setter.setFile(params.inputFile(), configuration.getArgumentOfNameOrThrow("i_path"));
		setter.setFile(params.outputFile(), configuration.getArgumentOfNameOrThrow("o_path"));
		setter.setString(params.type(), configuration.getArgumentOfNameOrThrow("i_type"));
		setter.setString(params.algorithm(), configuration.getArgumentOfNameOrThrow("algorithm"));
		setter.setDouble(params.criticalFreq(), configuration.getArgumentOfNameOrThrow("critical_freq"));
		setter.setDouble(params.samplingFreq(), configuration.getArgumentOfNameOrThrow("sampling_freq"));
		setter.setInt(params.order(), configuration.getArgumentOfNameOrThrow("order"));
		setter.setInt(params.windowSize(), configuration.getArgumentOfNameOrThrow("window_size"));
		super.aboutToEditArgument(configuration);
	}

}
