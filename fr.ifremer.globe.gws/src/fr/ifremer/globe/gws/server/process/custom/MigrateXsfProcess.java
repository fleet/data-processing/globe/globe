package fr.ifremer.globe.gws.server.process.custom;

import fr.ifremer.globe.core.runtime.gws.param.MigrateXsfParams;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.utils.exception.GException;

/**
 * GWS service to migrate XSF files.
 */
public class MigrateXsfProcess extends ServiceLauncher {

	public static final String SERVICE_NAME = "Migrate XSF files";

	/** Parameters to set */
	private final MigrateXsfParams params;

	/**
	 * Constructor
	 */
	public MigrateXsfProcess(GwsService specificService, MigrateXsfParams params) {
		super(specificService);
		this.params = params;
	}

	/** Overloaded for setting some arguments before launching the service */
	@Override
	protected void aboutToEditArgument(GwsServiceConf configuration) throws GException {
		ArgumentValueSetter setter = new ArgumentValueSetter();
		setter.setFiles(params.inXsfFiles(), configuration.getArgumentOfNameOrThrow("i_paths"));
		setter.setFiles(params.outXsfFiles(), configuration.getArgumentOfNameOrThrow("o_paths"));
		setter.setBoolean(params.overwrite(), configuration.getArgumentOfNameOrThrow("overwrite"));

		if (params.referenceFolder() != null)
			setter.setFile(params.referenceFolder(), configuration.getArgumentOfNameOrThrow("i_ref"));
		else {
			// Configure the script to use list of reference files instead of a folder
			GwsServiceArgument refFolderArg = configuration.getArgumentOfNameOrThrow("i_ref");
			refFolderArg.setNargs("+");
			setter.setFiles(params.refXsfFiles(), refFolderArg);
		}
		super.aboutToEditArgument(configuration);
	}

}
