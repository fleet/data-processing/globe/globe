package fr.ifremer.globe.gws.server.process.custom;

import fr.ifremer.globe.core.runtime.gws.param.EstimateTideParams;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.utils.exception.GException;

/**
 * GWS service to estimate navigation and XSF tide.
 */
public class EstimateTideProcess extends ServiceLauncher {

	public static final String ESTIMATE_NAV_PYAT_SERVICE_NAME = "Navigation tide estimator";
	public static final String ESTIMATE_XSFNMEA_PYAT_SERVICE_NAME = "XSF NMEA tide estimator";

	/** Parameters to set */
	private final EstimateTideParams params;

	/**
	 * Constructor
	 */
	public EstimateTideProcess(GwsService specificService, EstimateTideParams params) {
		super(specificService);
		this.params = params;
	}

	/** Overloaded for setting some arguments before launching the service */
	@Override
	protected void aboutToEditArgument(GwsServiceConf configuration) throws GException {
		ArgumentValueSetter setter = new ArgumentValueSetter();
		setter.setFiles(params.inputFiles(), configuration.getArgumentOfNameOrThrow("input_files"));
		setter.setFile(params.outputFile(), configuration.getArgumentOfNameOrThrow("output_file"));
		setter.setFile(params.modelDir(), configuration.getArgumentOfNameOrThrow("model_dir"));
		setter.setBoolean(params.enableFiltering(), configuration.getArgumentOfNameOrThrow("positioning_type_filter"));
		setter.setBoolean(params.displayMatplot(), configuration.getArgumentOfNameOrThrow("display"));
		setter.setBoolean(params.predictiveMode(), configuration.getArgumentOfNameOrThrow("predictive_mode"));
		setter.setString(params.referenceSurface(), configuration.getArgumentOfNameOrThrow("reference_surface"));
		setter.setDouble(params.referenceAltitude(), configuration.getArgumentOfNameOrThrow("reference_altitude"));

		super.aboutToEditArgument(configuration);
	}

}
