package fr.ifremer.globe.gws.server.process.driver.adcp;

import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

import org.apache.commons.lang.math.FloatRange;
import org.apache.commons.lang.math.NumberUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.fleet.gws.rsocket.flatbuffers.AdcpFileInfo;
import fr.ifremer.globe.core.io.adcp.AdcpInfo;
import fr.ifremer.globe.core.model.current.CurrentFiltering;
import fr.ifremer.globe.core.model.current.ICurrentData;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfoSupplier;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.utils.Pair;

/**
 * Service providing informations on ADCP (acoustic doppler current profiler) files.
 */
@Component(name = "globe_gws_adcp_file_info_supplier", service = { IFileInfoSupplier.class, AdcpInfoSupplier.class })
public class AdcpInfoSupplier extends BasicFileInfoSupplier<AdcpInfo> {

	/** Logger */
	public static final Logger LOGGER = LoggerFactory.getLogger(AdcpInfoSupplier.class);

	@Reference
	private ISessionService sessionService;

	/** Instance of AdcpDriver in GWS server. Empty until the driver is running */
	private Optional<AdcpDriverProcess> adcpDriverProcess = Optional.empty();

	/** Extensions */
	public static final String EXTENSION_STA = "STA";

	/**
	 * Constructor
	 */
	public AdcpInfoSupplier() {
		super(EXTENSION_STA, "ADCP", ContentType.ADCP_STA);
	}

	/**
	 * Make an instance of AdcpInfo for the specified file
	 */
	@Override
	public Optional<AdcpInfo> getFileInfo(String filePath) {
		Optional<AdcpInfo> result = Optional.empty();
		if (hasRightExtension(filePath)) {
			// Save setting in session
			FileInfoSettings settings = AdcpInfo.computeFileInfoSettings(new CurrentFiltering(0, Optional.empty()));
			result = getFileInfo(filePath, settings);
			if (result.isPresent())
				sessionService.getPropertiesContainer().add(filePath, FileInfoSettings.REALM_PROPERTIES_FILE_SETTINGS,
						settings);
		}

		return result;
	}

	/**
	 * @return the IFileInfo corresponding to the specified file using the specified settings
	 */
	@Override
	public Optional<AdcpInfo> getFileInfo(String filePath, FileInfoSettings settings) {
		if (settings.getContentType() == ContentType.ADCP_STA) {
			AdcpInfo result = new AdcpInfo(filePath, this::filterCurrentData);
			int sampling = settings.getInt(AdcpInfo.SAMPLING_PROPERTY, 0);
			float minRange = NumberUtils.toFloat(settings.get(AdcpInfo.MIN_RANGE_PROPERTY), Float.NaN);
			float maxRange = NumberUtils.toFloat(settings.get(AdcpInfo.MAX_RANGE_PROPERTY), Float.NaN);
			Optional<FloatRange> range = Float.isFinite(minRange) && Float.isFinite(maxRange)
					? Optional.of(new FloatRange(minRange, maxRange))
					: Optional.empty();
			CurrentFiltering filtering = new CurrentFiltering(sampling, range);

			try {
				if (adcpDriverProcess.isPresent()) {
					AdcpDriverProcess driver = adcpDriverProcess.get();
					return driver.openFile(filePath)//
							.map(adcpFileInfo -> initAdcpInfo(result, adcpFileInfo))//
							.then(driver.fetchCurrent(filePath, filtering)//
									.map(adcpCurrent -> {
										AdcpCurrentData adcpCurrentData = new AdcpCurrentData(result, filtering,
												adcpCurrent);
										result.setCurrentData(adcpCurrentData);
										return result;
									}))
							.onErrorComplete()//
							.blockOptional(Duration.ofSeconds(60));
				} else
					return Optional.of(result);
			} catch (IllegalStateException e) {
				LOGGER.warn(e.getMessage());
			}
		}
		return Optional.empty();
	}

	/** Request the filtering of the current */
	private Optional<ICurrentData> filterCurrentData(AdcpInfo adcpInfo, CurrentFiltering filtering) {
		if (adcpDriverProcess.isPresent()) {
			AdcpDriverProcess driver = adcpDriverProcess.get();
			return driver.fetchCurrent(adcpInfo.getPath(), filtering)//
					.map(adcpCurrent -> new AdcpCurrentData(adcpInfo, filtering, adcpCurrent))//
					.map(ICurrentData.class::cast)//
					.onErrorComplete()//
					.blockOptional();
		}
		return Optional.empty();
	}

	/** Fill the AdcpInfo fields with the values received from service (AdcpFileInfo) */
	private AdcpInfo initAdcpInfo(AdcpInfo adcpInfo, AdcpFileInfo adcpFileInfo) {
		adcpInfo.setVectorCount(adcpFileInfo.vectorCount());
		adcpInfo.setRangeLimits(new FloatRange(adcpFileInfo.rangeMin(), adcpFileInfo.rangeMax()));
		adcpInfo.setGeoBox(new GeoBox(adcpFileInfo.latitudeMax(), adcpFileInfo.latitudeMin(),
				adcpFileInfo.longitudeMin(), adcpFileInfo.longitudeMax()));
		adcpInfo.setStartEndDate(Pair.of(Instant.ofEpochMilli(adcpFileInfo.datetimeMin()),
				Instant.ofEpochMilli(adcpFileInfo.datetimeMax())));
		return adcpInfo;
	}

	/**
	 * @param adcpDriverProcess the {@link #adcpDriverProcess} to set
	 */
	public void setAdcpDriverProcess(AdcpDriverProcess adcpDriverProcess) {
		this.adcpDriverProcess = Optional.ofNullable(adcpDriverProcess);
	}

}