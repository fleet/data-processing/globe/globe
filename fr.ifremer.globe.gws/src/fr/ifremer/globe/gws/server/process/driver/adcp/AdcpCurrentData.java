package fr.ifremer.globe.gws.server.process.driver.adcp;

import java.io.IOException;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.math.FloatRange;

import fr.ifremer.globe.core.io.adcp.AdcpInfo;
import fr.ifremer.globe.core.model.current.CurrentFiltering;
import fr.ifremer.globe.core.model.current.CurrentMetadataType;
import fr.ifremer.globe.core.model.current.ICurrentData;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.AdcpCurrent;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * 
 */
public class AdcpCurrentData implements ICurrentData, INavigationData {

	private final AdcpInfo adcpInfo;
	private final CurrentFiltering currentFiltering;
	private final AdcpCurrent adcpCurrent;

	private final List<NavigationMetadata<?>> navigationMetadata = new LinkedList<>();
	/** List of metadata for vectors */
	private final List<NavigationMetadata<Double>> vectorMetada = new LinkedList<>();

	/**
	 * Constructor
	 */
	public AdcpCurrentData(AdcpInfo adcpInfo, CurrentFiltering currentFiltering, AdcpCurrent adcpCurrent) {
		this.adcpInfo = adcpInfo;
		this.currentFiltering = currentFiltering;
		this.adcpCurrent = adcpCurrent;
	}

	/** {@inheritDoc} */
	@Override
	public void close() throws IOException {
	}

	/** {@inheritDoc} */
	@Override
	public IFileInfo getFileInfo() {
		return adcpInfo;
	}

	/** {@inheritDoc} */
	@Override
	public CurrentFiltering getFiltering() {
		return currentFiltering;
	}

	/** {@inheritDoc} */
	@Override
	public int getTotalVectorCount() {
		return adcpInfo.getVectorCount();
	}

	/** {@inheritDoc} */
	@Override
	public int getVectorCount() {
		return adcpCurrent.vectorCount();
	}

	/** {@inheritDoc} */
	@Override
	public FloatRange getRangeLimits() {
		return adcpInfo.getRangeLimits();
	}

	/** {@inheritDoc} */
	@Override
	public long getVectorTime(int index) throws GIOException {
		return index < adcpCurrent.timeIndexLength() ? adcpCurrent.time(adcpCurrent.timeIndex(index)) : 0;
	}

	/** {@inheritDoc} */
	@Override
	public Instant getVectorInstant(int index) throws GIOException {
		return Instant.ofEpochMilli(getVectorTime(index));
	}

	/** {@inheritDoc} */
	@Override
	public double getVectorLongitude(int index) throws GIOException {
		return index < adcpCurrent.timeIndexLength() ? adcpCurrent.longitude(adcpCurrent.timeIndex(index)) : 0;
	}

	/** {@inheritDoc} */
	@Override
	public double getVectorLatitude(int index) throws GIOException {
		return index < adcpCurrent.timeIndexLength() ? adcpCurrent.latitude(adcpCurrent.timeIndex(index)) : 0;
	}

	/** {@inheritDoc} */
	@Override
	public float getVectorElevation(int index) throws GIOException {
		return index < adcpCurrent.rangeLength() ? adcpCurrent.range(index) : 0f;
	}

	/** {@inheritDoc} */
	@Override
	public float getVectorNorthwardVelocity(int index) throws GIOException {
		return index < adcpCurrent.northwardVelocityLength() ? adcpCurrent.northwardVelocity(index) : 0f;
	}

	/** {@inheritDoc} */
	@Override
	public float getVectorEastwardVelocity(int index) throws GIOException {
		return index < adcpCurrent.eastwardVelocityLength() ? adcpCurrent.eastwardVelocity(index) : 0f;
	}

	/** {@inheritDoc} */
	@Override
	public List<NavigationMetadata<Double>> getVectorMetadata() {
		if (vectorMetada.isEmpty()) {
			vectorMetada.add(CurrentMetadataType.HEADING.withValueSupplier(this::getVectorHeading));
			vectorMetada.add(CurrentMetadataType.VALUE.withValueSupplier(this::getVectorValue));
		}
		return vectorMetada;
	}

	///////
	// INavigationData
	///////

	@Override
	public String getFileName() {
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public int size() {
		return adcpCurrent.timeLength();
	}

	/** {@inheritDoc} */
	@Override
	public long getTime(int index) throws GIOException {
		return index < adcpCurrent.timeLength() ? adcpCurrent.time(index) : 0;
	}

	/** {@inheritDoc} */
	@Override
	public double getLongitude(int index) throws GIOException {
		return index < adcpCurrent.longitudeLength() ? adcpCurrent.longitude(index) : 0;
	}

	/** {@inheritDoc} */
	@Override
	public double getLatitude(int index) throws GIOException {
		return index < adcpCurrent.latitudeLength() ? adcpCurrent.latitude(index) : 0;
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		if (navigationMetadata.isEmpty()) {
			navigationMetadata.add(CurrentMetadataType.HEADING.withValueSupplier(this::getVectorHeading));
			navigationMetadata.add(CurrentMetadataType.VALUE.withValueSupplier(this::getVectorValue));
		}
		return navigationMetadata;
	}

}
