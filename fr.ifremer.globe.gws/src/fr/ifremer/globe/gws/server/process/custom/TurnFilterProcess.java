package fr.ifremer.globe.gws.server.process.custom;

import fr.ifremer.globe.core.runtime.gws.param.turnfilter.TurnFilterParams;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.utils.exception.GException;

/**
 * GWS service to filter navigation turns.
 */
public class TurnFilterProcess extends ServiceLauncher {

	public static final String SERVICE_NAME = "Turn filter";

	/** Parameters to set */
	private final TurnFilterParams params;

	/**
	 * Constructor
	 */
	public TurnFilterProcess(GwsService specificService, TurnFilterParams params) {
		super(specificService);
		this.params = params;
	}

	/** Overloaded for setting some arguments before launching the service */
	@Override
	protected void aboutToEditArgument(GwsServiceConf configuration) throws GException {
		ArgumentValueSetter setter = new ArgumentValueSetter();
		setter.setFile(params.inputFile(), configuration.getArgumentOfNameOrThrow("i_paths"));
		setter.setFile(params.outputFile(), configuration.getArgumentOfNameOrThrow("o_cut_file"));
		setter.setString(params.method().value, configuration.getArgumentOfNameOrThrow("filter_method"));

		params.headingParameters().ifPresentOrElse(headingParams -> {
			setter.setDuration(headingParams.interval(), configuration.getArgumentOfNameOrThrow("heading_period"));
			setter.setDouble(headingParams.threshold(), configuration.getArgumentOfNameOrThrow("heading_threshold"));
		}, () -> {
			setter.setDuration(null, configuration.getArgumentOfNameOrThrow("heading_period"));
			setter.setDouble(null, configuration.getArgumentOfNameOrThrow("heading_threshold"));
		});

		params.speedParameters().ifPresentOrElse(speedParams -> {
			setter.setDuration(speedParams.interval(), configuration.getArgumentOfNameOrThrow("speed_period"));
			setter.setDouble(speedParams.threshold(), configuration.getArgumentOfNameOrThrow("speed_threshold"));
		}, () -> {
			setter.setDuration(null, configuration.getArgumentOfNameOrThrow("speed_period"));
			setter.setDouble(null, configuration.getArgumentOfNameOrThrow("speed_threshold"));
		});

		params.minimumDuration().ifPresentOrElse(
				minDuration -> setter.setDuration(minDuration,
						configuration.getArgumentOfNameOrThrow("minimum_duration")),
				() -> setter.setDuration(null, configuration.getArgumentOfNameOrThrow("minimum_duration")));

		setter.setBoolean(false, configuration.getArgumentOfNameOrThrow("save_in_input_file"));

		super.aboutToEditArgument(configuration);
	}

}
