package fr.ifremer.globe.gws.server.process;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.fleet.gws.rsocket.flatbuffers.FileListPayload;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.MapOfDoublePayload;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.StringListPayload;

/**
 * Object used to intercept payloads received by a ServiceProcess
 */
public interface PayloadIntercepter {

	static final Logger logger = LoggerFactory.getLogger(PayloadIntercepter.class);

	/**
	 * Intercepts a file list contained in a payload.
	 */
	public static PayloadIntercepter forFileList(Consumer<List<File>> consumer) {
		return new PayloadIntercepter() {
			@Override
			public void acceptFileListPayload(FileListPayload payload) {
				List<File> result = new LinkedList<>();
				for (int i = 0; i < payload.filePathsLength(); i++)
					result.add(new File(payload.filePaths(i)));
				consumer.accept(result);
			}
		};
	}

	/**
	 * Intercepts a string contained in a payload.
	 */
	public static PayloadIntercepter forString(Consumer<String> consumer) {
		return new PayloadIntercepter() {
			@Override
			public void acceptStringPayload(String payload) {
				consumer.accept(payload);
			}
		};
	}

	/** What to do with a ByteBuffer of the route "MapOfDoublePayload" */
	default void acceptMapOfDoublePayload(MapOfDoublePayload payload) {
		logger.info("MapOfDoublePayload received but ignored");
	}

	/** What to do with a ByteBuffer of the route "FileListPayload" */
	default void acceptFileListPayload(FileListPayload rootAsFileListPayload) {
		logger.info("FileListPayload received but ignored");
	}

	/** What to do with a ByteBuffer of the route "StringListPayload" */
	default void acceptStringListPayload(StringListPayload rootAsStringListPayload) {
		logger.info("StringListPayload received but ignored");
	}

	/** What to do with a ByteBuffer of the route "StringPayload" */
	default void acceptStringPayload(String stringPayload) {
		logger.info("StringPayload received but ignored");
	}

	/** What to do when a terminate signal is received */
	default void acceptTerminate() {
		logger.debug("Terminate signal received");
	}

}
