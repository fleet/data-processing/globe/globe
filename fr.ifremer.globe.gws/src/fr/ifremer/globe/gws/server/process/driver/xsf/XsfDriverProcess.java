package fr.ifremer.globe.gws.server.process.driver.xsf;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.flatbuffers.FlatBufferBuilder;

import fr.ifremer.fleet.gws.rsocket.GwsPayload;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.XsfAcquisitionMode;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.XsfAcquisitionModeRequest;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.process.ServiceProcess;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.core.publisher.Sinks.One;

/**
 * GWS driver service for XSF files.
 */
public class XsfDriverProcess extends ServiceLauncher {

	private static final Logger logger = LoggerFactory.getLogger(XsfDriverProcess.class);

	public static final String XSF_DRIVER_SERVICE = "Driver of XSF files";

	/**
	 * Context used to inject this object. <br>
	 * This is the context created by GwsServerBootstrap.<br>
	 * XsfDriverProcess use this context to inject the ServiceProcess it creates (ContextInitializer may not be set up
	 * at this point)
	 */
	@Inject
	private IEclipseContext eclipseContext;
	
	/** Service process running for XSF driver */
	private One<ServiceProcess> xsfProcess = Sinks.one();

	/**
	 * Constructor
	 */
	public XsfDriverProcess(GwsService gwsService) {
		super(gwsService);
	}

	/** Launch the service just after injection */
	@PostConstruct
	private void postConstruct() {
		launchNoUI();
	}

	/**
	 * Overloads to catch the process
	 */
	@Override
	protected IStatus runTheProcess(ServiceProcess process, IProgressMonitor monitor, Logger log) {
		xsfProcess.tryEmitValue(process);
		return super.runTheProcess(process, monitor, log);
	}

	/** Request some filtered current data for the specified file */
	public Mono<XsfAcquisitionMode> fetchAcquisitionMode(String path) {
		FlatBufferBuilder builder = new FlatBufferBuilder();
		int filePath = builder.createString(path);
		XsfAcquisitionModeRequest.startXsfAcquisitionModeRequest(builder);
		XsfAcquisitionModeRequest.addFilePath(builder, filePath);
		builder.finish(XsfAcquisitionModeRequest.endXsfAcquisitionModeRequest(builder));

		GwsPayload request = new GwsPayload("get_xsf_acquisition_mode", builder.dataBuffer());

		return xsfProcess.asMono() //
				.flatMap(p -> p.requestResponseForPayload(request))//
				.map(payload -> XsfAcquisitionMode.getRootAsXsfAcquisitionMode(payload.getData()));

	}

	/** {@inheritDoc} */
	@Override
	protected ServiceProcess aboutToCreateServiceProcess(GwsService gwsService, GwsServiceConf configuration) {
		ServiceProcess result = new ServiceProcess(gwsService, configuration) {

			/** A driver service never stops. The end of the process never happens */
			@Override
			protected void waitForTheServiceToFinish() {
				// Don't wait. A driver never dies
			}
		};
		ContextInjectionFactory.inject(result, eclipseContext);
		return result;
	}

	/** Launching the service... */
	private void launchNoUI() {
		launchNoUI(//
				new NullProgressMonitor(), // This service has no progression and is not stoppable
				logger);
	}
}
