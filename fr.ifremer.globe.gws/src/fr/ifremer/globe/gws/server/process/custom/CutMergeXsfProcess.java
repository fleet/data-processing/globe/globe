package fr.ifremer.globe.gws.server.process.custom;

import fr.ifremer.globe.core.runtime.gws.param.MergeXsfParams;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.utils.exception.GException;

/**
 * GWS service to Cut / Merge XSF files.
 */
public class CutMergeXsfProcess extends ServiceLauncher {

	public static final String SERVICE_NAME = "Cut / Merge XSF files";

	/** Parameters to set */
	private final MergeXsfParams params;

	/**
	 * Constructor
	 */
	public CutMergeXsfProcess(GwsService specificService, MergeXsfParams params) {
		super(specificService);
		this.params = params;
	}

	/** Overloaded for setting some arguments before launching the service */
	@Override
	protected void aboutToEditArgument(GwsServiceConf configuration) throws GException {
		ArgumentValueSetter setter = new ArgumentValueSetter();
		setter.setFiles(params.inXsfFiles(), configuration.getArgumentOfNameOrThrow("i_paths"));
		setter.setFiles(params.outCutXsfFiles(), configuration.getArgumentOfNameOrThrow("o_paths"));
		setter.setFile(params.cutFile(), configuration.getArgumentOfNameOrThrow("cut_file"));
		setter.setBoolean(params.overwrite(), configuration.getArgumentOfNameOrThrow("overwrite"));
		super.aboutToEditArgument(configuration);
	}

}
