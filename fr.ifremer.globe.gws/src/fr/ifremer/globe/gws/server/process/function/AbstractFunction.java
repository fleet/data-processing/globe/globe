package fr.ifremer.globe.gws.server.process.function;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gws.ContextInitializer;
import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.process.ServiceProcess;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.ui.wizard.WizardAdapter;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import jakarta.inject.Inject;

/**
 * Template method to call a function.<br>
 * A function is a service that computes a result transmitted via the RSocket. <br>
 * This result is send to a typed consumer.
 */
public abstract class AbstractFunction<C> implements Consumer<C> {

	protected static Logger logger = LoggerFactory.getLogger(AbstractFunction.class);

	@Inject
	private GwsServerProxy gwsServerProxy;

	/** Service calling the function */
	protected final Optional<GwsServiceConf> callingService;

	/** Configuration file path describing the function to launch */
	protected final String functionConfPath;

	/**
	 * Constructor
	 */
	protected AbstractFunction(Optional<GwsServiceConf> callingService, String functionConfPath) {
		this.callingService = callingService;
		this.functionConfPath = functionConfPath;
		ContextInitializer.inject(this);
	}

	/** Intercepter of Payload, to translate payload in expected value object */
	protected abstract PayloadTranslator<C> getPayloadTranslator(C valueConsumer);

	/** {@inheritDoc} */
	@Override
	public void accept(C valueConsumer) {
		GwsService functionService = gwsServerProxy.detailFunction(functionConfPath).orElse(null);
		if (functionService != null && functionService.description() != null) {
			GwsServiceConf functionConf = functionService.description();
			// Prepare arguments values (get from calling service or edited in a wizard)
			if (editArguments(functionConf)) {
				ServiceProcess process = new ServiceProcess(functionService, functionConf);
				process.setRecordInLogBook(false);
				ContextInitializer.inject(process);
				process.setPayloadIntercepter(getPayloadTranslator(valueConsumer));
				process.runInForeground();
			}
		} else
			logger.warn("Function not found in the toolbox server '{}'", functionConfPath);
	}

	/**
	 * Transfers all arguments of the calling process to the function<br>
	 * Launch a wizard to edit the missing ones
	 *
	 * @return false when cancel is required
	 */
	protected boolean editArguments(GwsServiceConf functionConf) {

		// Transfers values from calling process to function
		if (callingService.isPresent()) {
			for (GwsServiceArgument outArgument : functionConf.arguments()) {
				Optional<GwsServiceArgument> inArgument = callingService.get().getArgumentOfName(outArgument.getName());
				if (inArgument.isPresent() && inArgument.get().getValue() != null) {
					outArgument.setValue(inArgument.get().getValue());
				}
			}
		}

		// set output json file path
		Optional<GwsServiceArgument> outfile = functionConf.getArgumentOfType(GwsServiceArgument.OUT_FILE);
		try {
			var prefix = StringUtils.abbreviateMiddle(functionConf.name(), "_", 10);
			if (outfile.isPresent()) {
				outfile.get().setValue(TemporaryCache.createTemporaryFile(prefix + "_", ".json").toString());
			}
		} catch (IOException e) {
			logger.warn("Unable to create a temporary file ", e);
			return false;
		}

		// Any missing argument ?
		var missingArguments = functionConf.arguments().stream().filter(arg -> arg.getValue() == null).toList();
		if (!missingArguments.isEmpty() && !editMissingArguments(functionConf.name(), missingArguments)) {
			// Edition cancelled...
			return false;
		}

		return true;
	}

	/**
	 * Some argument values are missing. Edit them with a wizard.
	 *
	 * @return true if edition was performed correctly
	 */
	protected boolean editMissingArguments(String functionName, List<GwsServiceArgument> missingArguments) {
		GwsServiceConf functionConf = new GwsServiceConf(functionName, true, missingArguments, List.of());
		var wizardAdapter = new WizardAdapter(functionConf, Optional.empty(), Display.getCurrent().getActiveShell());
		wizardAdapter.setWithSummary(false);
		return wizardAdapter.openWizard();
	}

}
