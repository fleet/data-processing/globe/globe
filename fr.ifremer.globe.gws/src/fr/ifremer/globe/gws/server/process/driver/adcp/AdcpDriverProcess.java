package fr.ifremer.globe.gws.server.process.driver.adcp;

import java.util.EnumSet;
import java.util.function.Consumer;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.flatbuffers.FlatBufferBuilder;

import fr.ifremer.fleet.gws.rsocket.GwsPayload;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.AdcpCurrent;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.AdcpCurrentRequest;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.AdcpFileInfo;
import fr.ifremer.globe.core.io.adcp.AdcpInfo;
import fr.ifremer.globe.core.model.current.CurrentFiltering;
import fr.ifremer.globe.core.model.file.FileInfoEvent;
import fr.ifremer.globe.core.model.file.FileInfoEvent.FileInfoState;
import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.process.ServiceProcess;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.core.publisher.Sinks.One;

/**
 * GWS driver service for ADCP files.
 */
public class AdcpDriverProcess extends ServiceLauncher {

	private static final Logger logger = LoggerFactory.getLogger(AdcpDriverProcess.class);

	public static final String ADCP_DRIVER_SERVICE = "Driver of ADCP files";

	/**
	 * Context used to inject this object. <br>
	 * This is the context created by GwsServerBootstrap.<br>
	 * AdcpDriverProcess use this context to inject the ServiceProcess it creates (ContextInitializer may not be set up
	 * at this point)
	 */
	@Inject
	private IEclipseContext eclipseContext;
	@Inject
	private FileInfoModel fileInfoModel;
	private final Consumer<FileInfoEvent> fileInfoModelListener = this::onFileInfoEvent;

	/** Service process running for ADCP driver */
	private One<ServiceProcess> adcpProcess = Sinks.one();

	/**
	 * Constructor
	 */
	public AdcpDriverProcess(GwsService gwsService) {
		super(gwsService);
	}

	/** Launch the service just after injection */
	@PostConstruct
	private void postConstruct() {
		launchNoUI();
		fileInfoModel.addListener(fileInfoModelListener);
	}

	/** Launch the service just after injection */
	@PreDestroy
	private void preDestroy() {
		fileInfoModel.removeListener(fileInfoModelListener);
	}

	/**
	 * Overloads to catch the process
	 */
	@Override
	protected IStatus runTheProcess(ServiceProcess process, IProgressMonitor monitor, Logger log) {
		adcpProcess.tryEmitValue(process);
		return super.runTheProcess(process, monitor, log);
	}

	/** Request the opening on the file */
	public Mono<AdcpFileInfo> openFile(String path) {
		GwsPayload request = new GwsPayload("open_adcp_file", path);
		return adcpProcess.asMono() //
				.flatMap(p -> p.requestResponseForPayload(request))//
				.map(payload -> AdcpFileInfo.getRootAsAdcpFileInfo(payload.getData()));
	}

	/** Request some filtered current data for the specified file */
	public Mono<AdcpCurrent> fetchCurrent(String path, CurrentFiltering filtering) {
		FlatBufferBuilder builder = new FlatBufferBuilder();
		int filePath = builder.createString(path);
		AdcpCurrentRequest.startAdcpCurrentRequest(builder);
		AdcpCurrentRequest.addFilePath(builder, filePath);
		AdcpCurrentRequest.addSampling(builder, filtering.sampling());
		filtering.range().ifPresent(range -> {
			AdcpCurrentRequest.addRangeMin(builder, range.getMinimumFloat());
			AdcpCurrentRequest.addRangeMax(builder, range.getMaximumFloat());
		});
		builder.finish(AdcpCurrentRequest.endAdcpCurrentRequest(builder));

		GwsPayload request = new GwsPayload("get_adcp_current", builder.dataBuffer());

		return adcpProcess.asMono() //
				.flatMap(p -> p.requestResponseForPayload(request))//
				.map(payload -> AdcpCurrent.getRootAsAdcpCurrent(payload.getData()));

	}

	/** {@inheritDoc} */
	@Override
	protected ServiceProcess aboutToCreateServiceProcess(GwsService gwsService, GwsServiceConf configuration) {
		ServiceProcess result = new ServiceProcess(gwsService, configuration) {

			/** A driver service never stops. The end of the process never happens */
			@Override
			protected void waitForTheServiceToFinish() {
				// Don't wait. A driver never dies
			}
		};
		ContextInjectionFactory.inject(result, eclipseContext);
		return result;
	}

	/** Launching the service... */
	private void launchNoUI() {
		launchNoUI(//
				new NullProgressMonitor(), // This service has no progression and is not stoppable
				logger);
	}

	/**
	 * Fire a request to close an ADCP file
	 */
	private void onFileInfoEvent(FileInfoEvent event) {
		if (EnumSet.of(FileInfoState.RELOADING, FileInfoState.REMOVED, FileInfoState.UNLOADED)
				.contains(event.getState())) {
			IFileInfo fileInfo = event.getfileInfo();
			if (fileInfo instanceof AdcpInfo adcpInfo) {
				GwsPayload request = new GwsPayload("close_adcp_file", adcpInfo.getPath());
				adcpProcess.asMono() //
						.flatMap(p -> p.fireAndForgetPayload(request))//
						.block();
			}
		}
	}
}
