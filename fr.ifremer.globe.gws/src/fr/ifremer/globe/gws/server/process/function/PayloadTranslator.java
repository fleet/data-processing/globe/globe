package fr.ifremer.globe.gws.server.process.function;

import fr.ifremer.globe.gws.server.process.PayloadIntercepter;

/**
 * Intercepter of Payload, to translate payload received from Pyat function. <br>
 * Inherited class have to specify the translation and send the result to the consumer
 */
public abstract class PayloadTranslator<C> implements PayloadIntercepter {

	private final C consumer;

	/**
	 * Constructor
	 */
	protected PayloadTranslator(C consumer) {
		this.consumer = consumer;
	}

	/**
	 * @return the {@link #consumer}
	 */
	public C getConsumer() {
		return consumer;
	}

}
