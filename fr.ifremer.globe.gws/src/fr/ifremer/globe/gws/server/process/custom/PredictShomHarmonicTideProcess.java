package fr.ifremer.globe.gws.server.process.custom;

import fr.ifremer.globe.core.runtime.gws.param.PredictShomHarmonicTideParams;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.utils.exception.GException;

/**
 * GWS service to predict Shom harmonic tide.
 */
public class PredictShomHarmonicTideProcess extends ServiceLauncher {

	public static final String SERVICE_NAME = "Shom harmonic tide predictor";

	/** Parameters to set */
	private final PredictShomHarmonicTideParams params;

	/**
	 * Constructor
	 */
	public PredictShomHarmonicTideProcess(GwsService specificService, PredictShomHarmonicTideParams params) {
		super(specificService);
		this.params = params;
	}

	/** Overloaded for setting some arguments before launching the service */
	@Override
	protected void aboutToEditArgument(GwsServiceConf configuration) throws GException {
		ArgumentValueSetter setter = new ArgumentValueSetter();
		setter.setFile(params.dataFile(), configuration.getArgumentOfNameOrThrow("data_file"));
		setter.setFile(params.outputFile(), configuration.getArgumentOfNameOrThrow("output_file"));
		setter.setFile(params.harmonicFile(), configuration.getArgumentOfNameOrThrow("harmonic_file"));
		super.aboutToEditArgument(configuration);
	}

}
