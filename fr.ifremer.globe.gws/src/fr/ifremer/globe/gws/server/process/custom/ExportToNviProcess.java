package fr.ifremer.globe.gws.server.process.custom;

import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.nvi.datacontainer.NavigationLayers;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.navigation.sensortypes.NmeaSensorType;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.runtime.gws.param.ExportToNviParams;
import fr.ifremer.globe.gws.server.process.PayloadIntercepter;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.process.ServiceProcess;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueGetter;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.gws.ui.wizard.WizardAdapter;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.impl.LargeArray;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import jakarta.inject.Inject;

/**
 * GWS specific service to export a INavigationData in NVI V2 format.
 */
public class ExportToNviProcess extends ServiceLauncher {

	private static final Logger logger = LoggerFactory.getLogger(ExportToNviProcess.class);

	public static final String SERVICE_NAME = "Export binary files to NVI";

	@Inject
	private INavigationService navigationService;
	@Inject
	private IArrayFactory arrayFactory;

	/** Parameters of the export */
	private final ExportToNviParams params;

	/**
	 * Constructor
	 */
	public ExportToNviProcess(GwsService specificService, ExportToNviParams params) {
		super(specificService);
		this.params = params;
	}

	/** Overloaded for setting some arguments before editing in Wizard */
	@Override
	protected void aboutToEditArgument(GwsServiceConf configuration) throws GException {
		super.aboutToEditArgument(configuration);
		ArgumentValueSetter setter = new ArgumentValueSetter();
		setter.setFiles(params.inputFiles(), configuration.getArgumentOfNameOrThrow("i_paths"));
		params.ouputFiles()
				.ifPresent(value -> setter.setFiles(value, configuration.getArgumentOfNameOrThrow("o_paths")));
		params.overwrite()
				.ifPresent(value -> setter.setBoolean(value, configuration.getArgumentOfNameOrThrow("overwrite")));
	}

	/** Overloaded for setting the PayloadIntercepter */
	@Override
	protected void aboutToSetPayloadIntercepter(Optional<WizardAdapter> wizardAdapter, ServiceProcess process) {
		if (wizardAdapter.isPresent())
			// Priority to wizard parameters, if any
			super.aboutToSetPayloadIntercepter(wizardAdapter, process);
		else
			params.whereToLoadOutputFiles().ifPresent(//
					whereToloadFiles -> process.setPayloadIntercepter(
							PayloadIntercepter.forFileList(files -> loadResultingFile(files, whereToloadFiles))));
	}

	@Override
	protected boolean saveArguments(GwsServiceConf configuration, File argumentFile) throws GException {
		prepareMappedFiles(configuration);
		return super.saveArguments(configuration, argumentFile);
	}

	@Override
	protected void aboutToRunServiceProcess(ServiceProcess process, GwsServiceConf configuration) throws GException {
		prepareMappedFiles(configuration);
		process.setRecordInLogBook(true);
		super.aboutToRunServiceProcess(process, configuration);
	}

	/** Overridden for deleting temporary files */
	@Override
	protected void onServiceFinished(GwsServiceConf configuration) {
		super.onServiceFinished(configuration);
		// Delete temporary files
		configuration.arguments().stream()//
				.filter(arg -> GwsServiceArgument.FILE.equals(arg.getType()))//
				.map(GwsServiceArgument::getValue)//
				.filter(Objects::nonNull) //
				.forEach(path -> FileUtils.deleteQuietly(new File(path)));
	}

	/**
	 * Request the INavigationData of each input files and prepare the mapped files expected by the service
	 */
	private void prepareMappedFiles(GwsServiceConf configuration) throws GException {
		List<INavigationData> allNavigationData = params.navigationData().orElse(new ArrayList<>());
		// No INavigationData in params. Open input files and load them
		boolean loadNavigationData = allNavigationData.isEmpty();
		if (loadNavigationData) {
			ArgumentValueGetter getter = new ArgumentValueGetter();
			List<File> navFiles = getter.getFiles(configuration.getArgumentOfNameOrThrow("i_paths").getValue());
			for (File file : navFiles) {
				var navigationDataSupplier = navigationService.getNavigationDataProvider(file.getPath());
				if (navigationDataSupplier.isPresent()) {
					INavigationData navigationData = navigationDataSupplier.get().getNavigationData(true);
					allNavigationData.add(navigationData);
				} else {
					logger.error("Unsupported input file {}", file.getName());
				}
			}
		}

		writeNavigationToMappedFiles(allNavigationData, configuration);

		if (loadNavigationData)
			allNavigationData.forEach(IOUtils::closeQuietly);
	}

	/**
	 * Process the export of the specified navigation data list. <br>
	 * 
	 * @param allNavigationData List of navigation data. All navigation points are merged to represent an unique one.
	 */
	private void writeNavigationToMappedFiles(List<INavigationData> allNavigationData, GwsServiceConf configuration)
			throws GException {

		int[] navPointsInFiles = params.navPointsInFiles().orElse(null);
		int[] valueCount = navPointsInFiles;

		// If no navPointsInFiles in params, prepares an int array of the number of output files
		ArgumentValueGetter getter = new ArgumentValueGetter();
		if (valueCount == null || valueCount.length == 0) {
			List<File> outFiles = getter.getFiles(configuration.getArgumentOfNameOrThrow("o_paths").getValue());
			valueCount = new int[outFiles.size()];
		}

		Map<String, LargeArray> mappedFiles = Map.of();
		try {
			if (navPointsInFiles == null || navPointsInFiles.length == 0) {
				// If not specified in params, filling valueCount with the nb of nav points for each output file
				int valueIndex = 0;
				for (INavigationData navigationData : allNavigationData) {
					// When merging all nav points in one file, accumulate all navigationData.size() in valueCount[0]
					valueCount[valueIndex] = valueCount[valueIndex] + navigationData.size();
					if (valueCount.length > 1) { // length == 1 in case of merge
						valueIndex++;
					}
				}
			}

			mappedFiles = prepareMappedFiles(valueCount);
			fillMappedFiles(mappedFiles, allNavigationData);
			setMappedFileInConfiguration(configuration, valueCount, mappedFiles);
		} catch (IOException | IllegalArgumentException e) {
			throw new GIOException("Error with '" + SERVICE_NAME + "' service : " + e.getMessage(), e);
		} finally {
			mappedFiles.values().stream().forEach(LargeArray::close);
		}
	}

	/**
	 * Export the navigation in the mapped files.
	 */
	private void fillMappedFiles(Map<String, LargeArray> files, List<INavigationData> allNavigationData)
			throws GIOException {
		int offset = 0;
		for (INavigationData navigationData : allNavigationData) {
			int size = navigationData.size();
			for (int i = 0; i < size; i++) {
				// 1_000_000l => From milli to nano
				files.get(NavigationLayers.TIME_VARIABLE_NAME).putLong(offset, navigationData.getTime(i) * 1_000_000l);

				files.get(NavigationLayers.LATITUDE_VARIABLE_NAME).putDouble(offset, navigationData.getLatitude(i));
				files.get(NavigationLayers.LONGITUDE_VARIABLE_NAME).putDouble(offset, navigationData.getLongitude(i));

				float altitude = navigationData.getMetadataValue(NavigationMetadataType.ALTITUDE, i).orElse(Float.NaN);
				files.get(NavigationLayers.HEIGHT_ABOVE_REFERENCE_ELLIPSOID_VARIABLE_NAME).putFloat(offset, altitude);

				float heading = navigationData.getMetadataValue(NavigationMetadataType.HEADING, i).orElse(Float.NaN);
				files.get(NavigationLayers.HEADING_VARIABLE_NAME).putFloat(offset, heading);

				float elevation = navigationData.getMetadataValue(NavigationMetadataType.ELEVATION, i)
						.orElse(Float.NaN);
				files.get(NavigationLayers.VERTICAL_OFFSET_VARIABLE_NAME).putFloat(offset, elevation);

				NmeaSensorType sensorType = navigationData.getMetadataValue(NavigationMetadataType.SENSOR_TYPE, i)
						.filter(NmeaSensorType.class::isInstance).map(NmeaSensorType.class::cast)
						.orElse(NmeaSensorType.NOT_DEFINED);
				files.get(NavigationLayers.POSITION_QUALITY_INDICATOR_VARIABLE_NAME).putByte(offset,
						(byte) sensorType.getValue());

				float speed = navigationData.getMetadataValue(NavigationMetadataType.SPEED, i).orElse(Float.NaN);
				files.get(NavigationLayers.SPEED_OVER_GROUND_VARIABLE_NAME).putFloat(offset, speed);

				files.get(NavigationLayers.QUALITY_FLAG_VARIABLE_NAME).putByte(offset,
						(byte) (navigationData.isValid(i) ? 2 : 0));
				offset++;
			}
		}
	}

	/** Initialize some mapped files to serialize the navigation. One file for one NVI V2 variable */
	private Map<String, LargeArray> prepareMappedFiles(int[] valueCount) throws IOException {
		int size = Arrays.stream(valueCount).sum();
		var result = new HashMap<String, LargeArray>();
		result.put(NavigationLayers.TIME_VARIABLE_NAME,
				arrayFactory.makeLargeArray(size, Long.BYTES, NavigationLayers.TIME_VARIABLE_NAME));
		result.put(NavigationLayers.LATITUDE_VARIABLE_NAME,
				arrayFactory.makeLargeArray(size, Double.BYTES, NavigationLayers.LATITUDE_VARIABLE_NAME));
		result.put(NavigationLayers.LONGITUDE_VARIABLE_NAME,
				arrayFactory.makeLargeArray(size, Double.BYTES, NavigationLayers.LONGITUDE_VARIABLE_NAME));
		result.put(NavigationLayers.HEIGHT_ABOVE_REFERENCE_ELLIPSOID_VARIABLE_NAME, arrayFactory.makeLargeArray(size,
				Float.BYTES, NavigationLayers.HEIGHT_ABOVE_REFERENCE_ELLIPSOID_VARIABLE_NAME));
		result.put(NavigationLayers.HEADING_VARIABLE_NAME,
				arrayFactory.makeLargeArray(size, Float.BYTES, NavigationLayers.HEADING_VARIABLE_NAME));
		result.put(NavigationLayers.VERTICAL_OFFSET_VARIABLE_NAME,
				arrayFactory.makeLargeArray(size, Float.BYTES, NavigationLayers.VERTICAL_OFFSET_VARIABLE_NAME));
		result.put(NavigationLayers.POSITION_QUALITY_INDICATOR_VARIABLE_NAME, arrayFactory.makeLargeArray(size,
				Byte.BYTES, NavigationLayers.POSITION_QUALITY_INDICATOR_VARIABLE_NAME));
		result.put(NavigationLayers.SPEED_OVER_GROUND_VARIABLE_NAME,
				arrayFactory.makeLargeArray(size, Float.BYTES, NavigationLayers.SPEED_OVER_GROUND_VARIABLE_NAME));
		result.put(NavigationLayers.QUALITY_FLAG_VARIABLE_NAME,
				arrayFactory.makeLargeArray(size, Byte.BYTES, NavigationLayers.QUALITY_FLAG_VARIABLE_NAME));

		for (LargeArray largeArray : result.values()) {
			largeArray.setDeleteOnClose(false);
			largeArray.setByteOrder(ByteOrder.nativeOrder());
		}

		return result;
	}

	/** Before launching the service, set all mapped file in arguments */
	private void setMappedFileInConfiguration(GwsServiceConf configuration, int[] valueCount,
			Map<String, LargeArray> mappedVariables) throws IllegalArgumentException {
		ArgumentValueSetter setter = new ArgumentValueSetter();
		setter.setInts(valueCount, configuration.getArgumentOfNameOrThrow("o_value_count"));
		// Set all file arguments
		for (var variableEntry : mappedVariables.entrySet())
			setter.setFile(variableEntry.getValue().getFile(),
					configuration.getArgumentOfNameOrThrow(variableEntry.getKey()));
	}

}
