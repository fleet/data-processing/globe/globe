package fr.ifremer.globe.gws.server.process;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.ProgressMonitorWrapper;
import org.eclipse.core.runtime.SubMonitor;

/**
 * IProgressMonitor for managing a progression from 0f to 1f
 */
public class PercentProgressMonitor extends ProgressMonitorWrapper {

	/** Amount of work currently being done, from 0f (nothing done) to 1f (progression complete) */
	private float worked = 0f;

	/**
	 * Constructor
	 */
	public PercentProgressMonitor(IProgressMonitor monitor) {
		super(SubMonitor.convert(monitor, 100));
	}

	/**
	 * Sets the progression to a new threshold
	 */
	public void worked(float work) {
		if (work > worked) {
			super.worked(Math.round((work - worked) * 100));
			this.worked = work;
		}
	}

}
