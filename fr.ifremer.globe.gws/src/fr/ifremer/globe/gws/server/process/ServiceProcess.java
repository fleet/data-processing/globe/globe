package fr.ifremer.globe.gws.server.process;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;

import fr.ifremer.fleet.gws.client.GwsSocketClient;
import fr.ifremer.fleet.gws.rsocket.GwsPayload;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.FileListPayload;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.LogLevel;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.LogPayload;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.MapOfDoublePayload;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.ProgressPayload;
import fr.ifremer.fleet.gws.rsocket.flatbuffers.StringListPayload;
import fr.ifremer.globe.core.runtime.job.GProcess;
import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.job.GwsJob;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.ui.GwsToolboxExplorer;
import fr.ifremer.globe.utils.cache.WorkspaceCache;
import io.rsocket.Payload;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.core.publisher.Sinks.Many;

/**
 * Job for monitoring the execution of a service.<br>
 * This job updates its IProgressMonitor and Logger with the received payload from the service.<br>
 * When the job is cancelled, the cancellation request is forward to the service
 * 
 */
public class ServiceProcess extends GProcess {

	/** Cache folder for all argument files */
	public static final File CACHE_DIR = WorkspaceCache.getCacheDirectory(GwsToolboxExplorer.class.getSimpleName());

	/** Server proxy */
	@Inject
	private GwsServerProxy gwsServerProxy;

	private final GwsService gwsService;
	private final GwsServiceConf configuration;

	/** Indicates if the execution of this service is recorded in the log book of GWS */
	private boolean recordInLogBook = true;

	/** Intercepter of payload. All received payload are forward to the object */
	protected PayloadIntercepter payloadIntercepter = new PayloadIntercepter() {
		// By default, this intercepter has no specific action
	};

	/** Queue where payloads received from websocket are pushed */
	private final Many<GwsPayload> queueOfReceivedPayload;
	/** Subscription to {@link #queueOfReceivedPayload} */
	private Disposable receivingQueueSubscription;

	/** Used to signal the end of the process */
	private Sinks.Empty<Void> onFinish = Sinks.empty();

	/** Client of GWS communicating with the RSocket */
	private GwsSocketClient socketClient;

	/**
	 * Result of the execution. <br>
	 * ServiceProcess use its own status because when the job is not scheduled, JobManager do not manage
	 * InternalJob#result (always null)
	 */
	private IStatus serviceResult = Status.OK_STATUS;

	/**
	 * Constructor
	 */
	public ServiceProcess(GwsService gwsService, GwsServiceConf configuration) {
		super(gwsService.name());
		this.gwsService = gwsService;
		this.configuration = configuration;

		// Defines Queue of to store receiving payloads
		queueOfReceivedPayload = Sinks.many().unicast().onBackpressureBuffer(new LinkedList<GwsPayload>());
	}

	/**
	 * Running the process.<br>
	 * Opens the websocket and waits for payloads
	 */
	@Override
	protected IStatus apply(IProgressMonitor monitor, Logger logger) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		subMonitor.worked(20);
		PercentProgressMonitor percentMonitor = new PercentProgressMonitor(subMonitor.slice(80));
		logger.info("Requesting the server...");
		Optional<GwsJob> jobOpt = gwsServerProxy.launchService(gwsService, configuration, recordInLogBook);
		if (jobOpt.isPresent()) {
			GwsJob job = jobOpt.get();

			logger.info("Connecting to service on port {}", job.websocketPort());
			socketClient = new GwsSocketClient(job.websocketHost(), job.websocketPort(), queueOfReceivedPayload);
			socketClient.connect();
			receivingQueueSubscription = queueOfReceivedPayload.asFlux()//
					.subscribe(payload -> processPayload(payload, percentMonitor, logger), //
							e -> finish(Status.error(getName(), e)), // onError
							// On complete
							() -> finish(Status.OK_STATUS));

			waitForTheServiceToFinish();

			return serviceResult;
		} else {
			return Status.error("Service launch failed ('" + gwsService.name() + "')");
		}
	}

	/** Delegating the sending of the payload to the GwsSocketClient */
	public Mono<Void> fireAndForgetPayload(GwsPayload payload) {
		return socketClient.fireAndForgetPayload(payload);
	}

	/** Delegating the request of the payload to the GwsSocketClient */
	public Mono<Payload> requestResponseForPayload(GwsPayload payload) {
		return socketClient.requestResponseForPayload(payload);
	}

	/** Maintain the thread alive until the end of the service */
	protected void waitForTheServiceToFinish() {
		onFinish.asMono().block(); // Blocking until finish() is called
	}

	/** Finish the process by setting the status */
	protected void finish(IStatus status) {
		if (serviceResult.isOK())
			serviceResult = status;
		done(serviceResult);

		if (receivingQueueSubscription != null)
			receivingQueueSubscription.dispose();
		onFinish.tryEmitEmpty();
	}

	/**
	 * Cancellation request is made. Informs the service
	 * 
	 * @return True on success
	 */
	@Override
	protected void canceling() {
		fireAndForgetPayload(new GwsPayload("cancel")).block();
		super.canceling();
	}

	/** A payload has been received. Process it according to the specified route */
	private void processPayload(GwsPayload payload, PercentProgressMonitor monitor, Logger logger) {
		String route = payload.route();
		switch (route) {
		case "LogPayload" -> processLogPayload(payload, logger);
		case "ProgressPayload" -> processProgressPayload(payload, monitor);
		case "FileListPayload" -> payloadIntercepter
				.acceptFileListPayload(FileListPayload.getRootAsFileListPayload(payload.payload()));
		case "StringPayload" -> payloadIntercepter
				.acceptStringPayload(StandardCharsets.UTF_8.decode(payload.payload()).toString());
		case "StringListPayload" -> payloadIntercepter
				.acceptStringListPayload(StringListPayload.getRootAsStringListPayload(payload.payload()));
		case "MapOfDoublePayload" -> payloadIntercepter
				.acceptMapOfDoublePayload(MapOfDoublePayload.getRootAsMapOfDoublePayload(payload.payload()));
		case "terminate" -> {
			payloadIntercepter.acceptTerminate();
			onFinish.tryEmitEmpty();
		}
		default -> logger.warn("Receiving a message with an unexpected route {}. Ignored", route);
		}
	}

	/** A logging payload has been received. Process it */
	private void processLogPayload(GwsPayload payload, Logger logger) {
		LogPayload logPayload = LogPayload.getRootAsLogPayload(payload.payload());
		String message = logPayload.message();
		switch (logPayload.level()) {
		case LogLevel.INFO -> logger.info(message);
		case LogLevel.WARN -> logger.warn(message);
		case LogLevel.ERROR -> {
			logger.error(message);
			serviceResult = Status.error(message);
			String stack = logPayload.stack();
			if (stack != null && !stack.isEmpty())
				logger.error(stack);

		}
		default -> logger.debug(message);
		}
	}

	/** A progress payload has been received. Process it */
	private void processProgressPayload(GwsPayload payload, PercentProgressMonitor monitor) {
		ProgressPayload logPayload = ProgressPayload.getRootAsProgressPayload(payload.payload());
		String taskname = logPayload.taskname();
		if (taskname != null && !taskname.isEmpty())
			monitor.subTask(taskname);
		monitor.worked(logPayload.progress());
	}

	/**
	 * Set the PayloadIntercepter
	 */
	public void setPayloadIntercepter(PayloadIntercepter payloadIntercepter) {
		this.payloadIntercepter = payloadIntercepter;
	}

	/**
	 * @param recordInLogBook the {@link #recordInLogBook} to set
	 */
	public void setRecordInLogBook(boolean recordInLogBook) {
		this.recordInLogBook = recordInLogBook;
	}

}
