package fr.ifremer.globe.gws.server.job;

/**
 * Status of a job launched by the server
 */
public enum JobStatus {
	PENDING, RUNNING, SUCCESS, ERROR, CANCELLED;
}
