package fr.ifremer.globe.gws.server.job;

/**
 * Represents an execution of a GwsService on the GWS server
 */
public record GwsJob(

		/** id of the job */
		int jobId,
		/** id of a service */
		int serviceId,
		/** Name */
		String serviceName,
		/** RSocket host */
		String websocketHost,
		/** RSocket port */
		int websocketPort,
		/** State */
		JobStatus status) {
}
