package fr.ifremer.globe.gws.server.logbook;

import java.time.LocalDateTime;
import java.util.List;

import com.google.gson.JsonElement;

import fr.ifremer.globe.gws.server.job.JobStatus;
import fr.ifremer.globe.gws.server.service.GwsService;

/**
 * Log book event get from GWS
 */
public record GwsLogBookEvent(long id, String description, JobStatus status, LocalDateTime start, LocalDateTime end,
		String error, GwsService service, List<GwsEnvVar> environments, JsonElement parameters, String logs) {

	/**
	 * @return a copy of this event with a new {@link GwsService}.
	 */
	public GwsLogBookEvent copyWith(GwsService service) {
		return new GwsLogBookEvent(id, description, status, start, end, error, service, environments, parameters, logs);
	}
}
