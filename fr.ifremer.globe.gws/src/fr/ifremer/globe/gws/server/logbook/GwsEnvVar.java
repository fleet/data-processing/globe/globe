package fr.ifremer.globe.gws.server.logbook;

/**
 * 
 */
public record GwsEnvVar(String variable, String value) {

}
