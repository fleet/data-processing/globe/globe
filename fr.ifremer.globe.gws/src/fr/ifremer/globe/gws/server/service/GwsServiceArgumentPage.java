package fr.ifremer.globe.gws.server.service;

import java.util.List;

/**
 * Set of GwsServiceArguments grouped in the same wizard page
 */
public record GwsServiceArgumentPage(String title, String description, List<String> parameters, Boolean visible) {
	public GwsServiceArgumentPage {
		if (visible == null)
			visible = Boolean.TRUE;
	}
}
