package fr.ifremer.globe.gws.server.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import com.google.gson.annotations.SerializedName;

import fr.ifremer.globe.gws.server.service.GwsServiceArgument.DecodedType;

/**
 * Represents all the acceptable arguments of a service, described in a Json text
 */
public record GwsServiceConf(//
		String name, //
		Boolean visible, // Null if absent from the configuration file. Null means true.
		@SerializedName("parameters") List<GwsServiceArgument> arguments, //
		List<GwsServiceArgumentPage> pages) {

	/** Compact constructor */
	public GwsServiceConf {
		if (visible == null)
			visible = Boolean.TRUE;
		if (arguments == null)
			arguments = new LinkedList<>();
		if (pages == null)
			pages = new LinkedList<>();
	}

	/**
	 * @return the argument holding the specified type
	 */
	public Optional<GwsServiceArgument> getArgumentOfType(String type) {
		if (type != null) {
			for (GwsServiceArgument serviceArgument : arguments) {
				DecodedType[] decodedTypes = serviceArgument.decodeTypes();
				if (decodedTypes != null) {
					for (DecodedType decodedType : decodedTypes) {
						if (type.equalsIgnoreCase(decodedType.getRefinedType())) {
							return Optional.of(serviceArgument);
						}
					}
				}
			}
		}
		return Optional.empty();
	}

	/**
	 * @return the argument holding the specified type
	 */
	public Optional<GwsServiceArgument> getArgumentOfName(String name) {
		return name != null ? arguments().stream().filter(arg -> name.equalsIgnoreCase(arg.getName())).findFirst()
				: Optional.empty();
	}

	/**
	 * @return the argument holding the specified type.
	 * @throws IllegalArgumentException when argument is not known
	 */
	public GwsServiceArgument getArgumentOfNameOrThrow(String name) throws IllegalArgumentException {
		return getArgumentOfName(name).orElseThrow(
				() -> new IllegalArgumentException(String.format("Service %s has no argument %s", name(), name)));
	}

	/**
	 * @return the "infile" argument
	 */
	public Optional<GwsServiceArgument> getInputFilesArgument() {
		return getArgumentOfType(GwsServiceArgument.IN_FILE);
	}

	/**
	 * @return the "outfile" argument
	 */
	public Optional<GwsServiceArgument> getOutputFilesArgument() {
		return getArgumentOfType(GwsServiceArgument.OUT_FILE);
	}

	/**
	 * @return true if the specified argument occurs in the condition of another argument
	 */
	public boolean isLinkedToOtherArgumentByContiditon(GwsServiceArgument serviceArgument) {
		for (GwsServiceArgument argument : arguments) {
			for (DecodedType decodedType : argument.decodeTypes()) {
				if (decodedType.getEqualsCondition() != null
						&& decodedType.getEqualsCondition()[0].equals(serviceArgument.getName())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Search the specified argument in pages defined for the process
	 * 
	 * @return the found page if any
	 */
	public Optional<GwsServiceArgumentPage> getPageFor(GwsServiceArgument serviceArgument) {
		for (GwsServiceArgumentPage page : pages()) {
			for (String parameterName : page.parameters()) {
				if (parameterName.equals(serviceArgument.getName())) {
					return Optional.of(page);
				}
			}
		}
		return Optional.empty();
	}

}
