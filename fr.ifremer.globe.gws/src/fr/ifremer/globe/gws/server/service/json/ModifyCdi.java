/**
 * 
 */
package fr.ifremer.globe.gws.server.service.json;

import java.util.List;

import com.google.gson.reflect.TypeToken;

/**
 * Plain record to serialize a CDI modification in String format
 *
 */
public record ModifyCdi(String oldValue, String newValue) {

	public static final TypeToken<List<ModifyCdi>> LIST_MODIFY_CDI_TYPE = new TypeToken<List<ModifyCdi>>() {
	};

}
