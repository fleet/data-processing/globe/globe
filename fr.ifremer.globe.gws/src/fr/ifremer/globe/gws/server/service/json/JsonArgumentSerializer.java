package fr.ifremer.globe.gws.server.service.json;

import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.BOOL;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CDI_FILTER;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CDI_MODIFY;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CONFIGURATION_ARGUMENT_NAME;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CSV_MANDATORY_HEADERS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CSV_OPTIONAL_HEADERS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.DEPTH_CORRECTION_NOMINAL_WATERLINE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.DOUBLE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.GEOBOX_COORDS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.INTEGER;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.LAYERS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.MASK_POLYGONS_REVERSE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.OVERWRITE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.TIME_INTERVAL_REVERSE_GEO_MASK;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonWriter;

import fr.ifremer.globe.core.model.dtm.filter.FilterDtm;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument.DecodedType;

/**
 * Utility class to serialize arguments in a json file
 */
public class JsonArgumentSerializer {

	private static final Logger LOGGER = LoggerFactory.getLogger(JsonArgumentSerializer.class);

	private static final Set<String> EXCLUDED_ARGS = Set.of(
			// Configuration attributes, not service arguments
			CSV_MANDATORY_HEADERS, CSV_OPTIONAL_HEADERS//
	);

	protected ArgumentValueGetter argumentValueGetter = new ArgumentValueGetter();

	/**
	 * Constructor
	 */
	public JsonArgumentSerializer() {
		super();
	}

	/**
	 * Write the process into a temporary file
	 */
	public void serialize(List<GwsServiceArgument> serviceArguments, File outputFile) throws IOException {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		outputFile.getParentFile().mkdirs();
		try (JsonWriter writer = gson.newJsonWriter(new FileWriter(outputFile))) {
			serialize(serviceArguments, writer);
		} catch (JsonSyntaxException e) {
			LOGGER.error("Error while writing file : ", e);
		}
	}

	/**
	 * Write the process into a temporary file
	 */
	public void serialize(List<GwsServiceArgument> serviceArguments, JsonWriter writer) throws IOException {
		writer.beginObject();

		for (GwsServiceArgument argument : serviceArguments) {
			if (!CONFIGURATION_ARGUMENT_NAME.equals(argument.getName())) // FIXME already possible ?
				serialize(writer, argument);
		}

		writer.endObject();
	}

	/**
	 * Serialize one argument
	 */
	protected void serialize(JsonWriter writer, GwsServiceArgument argument) throws IOException {
		String value = argument.getValue();

		// if no value : get default
		if (value == null || value.isEmpty()) {
			value = argument.getDefaultValue();
		}

		if (value != null && !value.isEmpty()) {
			DecodedType[] types = argument.decodeTypes();
			// Consider only the first type.
			String refinedType = types.length > 0 ? types[0].getRefinedType() : null;
			if (refinedType != null && !EXCLUDED_ARGS.contains(refinedType)) {
				if (LAYERS.equalsIgnoreCase(refinedType)) {
					serializeLayers(writer, argument.getName(), value);
				} else if (GEOBOX_COORDS.equalsIgnoreCase(refinedType)) {
					serializeGeobox(writer, argument.getName(), value);
				} else if (CDI_MODIFY.equalsIgnoreCase(refinedType)) {
					serializeModifiedCdis(writer, argument.getName(), value);
				} else if (CDI_FILTER.equalsIgnoreCase(refinedType)) {
					serializeCdisFilters(writer, argument.getName(), value);
				} else if (argument.requireListAsValue()) {
					serializeList(writer, argument.getName(), refinedType, value);
				} else if (BOOL.equalsIgnoreCase(refinedType)
						// Some specific parameters that should be consider as "boolean"
						|| OVERWRITE.equalsIgnoreCase(refinedType)
						|| MASK_POLYGONS_REVERSE.equalsIgnoreCase(refinedType)
						|| TIME_INTERVAL_REVERSE_GEO_MASK.equalsIgnoreCase(refinedType)) {
					serializeBoolean(writer, argument.getName(), value);
				} else if (DOUBLE.equalsIgnoreCase(refinedType)
						|| DEPTH_CORRECTION_NOMINAL_WATERLINE.equalsIgnoreCase(refinedType)) {
					serializeDouble(writer, argument.getName(), value);
				} else if (INTEGER.equalsIgnoreCase(refinedType)) {
					serializeInteger(writer, argument.getName(), value);
				} else {
					serializePlainArgument(writer, argument.getName(), value);
				}
			}
		}
	}

	/**
	 * Serialize an argument in a list format
	 */
	protected void serializePlainArgument(JsonWriter writer, String name, String value) throws IOException {
		writer.name(name).value(value);
	}

	/**
	 * Serialize a boolean argument
	 */
	protected void serializeBoolean(JsonWriter writer, String name, String value) throws IOException {
		Boolean bool = argumentValueGetter.getBoolean(value);
		if (bool != null) {
			writer.name(name).value(bool.booleanValue());
		}
	}

	/**
	 * Serialize an integer argument
	 */
	protected void serializeInteger(JsonWriter writer, String name, String value) throws IOException {
		Integer l = argumentValueGetter.getInt(value);
		if (l != null)
			writer.name(name).value(l);
	}

	/**
	 * Serialize a double argument
	 */
	protected void serializeDouble(JsonWriter writer, String name, String value) throws IOException {
		Double d = argumentValueGetter.getDouble(value);
		if (d != null)
			writer.name(name).value(d);
	}

	/**
	 * Serialize an argument in a list format
	 * 
	 * @param value2
	 */
	protected void serializeList(JsonWriter writer, String name, String refinedType, String value) throws IOException {
		// Dictionary ?
		var dictionary = argumentValueGetter.getDictionary(value);
		if (!dictionary.isEmpty()) {
			writer.name(name).beginObject();
			for (var pair : dictionary.entrySet()) {
				writer.name(pair.getKey()).value(pair.getValue());
			}
			writer.endObject();
			return;
		}

		// List
		writer.name(name).beginArray();
		for (String oneValue : argumentValueGetter.getStrings(value)) {
			if (INTEGER.equalsIgnoreCase(refinedType))
				writer.value(Integer.valueOf(oneValue));
			else
				writer.value(oneValue);
		}
		writer.endArray();
	}

	/**
	 * Serialize an argument in a Geobox format
	 */
	protected void serializeGeobox(JsonWriter writer, String name, String value) throws IOException {
		GeoBox geobox = argumentValueGetter.getGeobox(value);
		if (geobox != null) {
			writer.name(name).beginObject();

			writer.name("north").value(geobox.getTop());
			writer.name("south").value(geobox.getBottom());

			writer.name("west").value(geobox.getLeft());
			writer.name("east").value(geobox.getRight());

			writer.endObject(); // name
		}
	}

	/**
	 * Serialize an argument in a modified cdi format
	 */
	protected void serializeModifiedCdis(JsonWriter writer, String name, String value) throws IOException {
		Map<String, String> cdis = argumentValueGetter.getModifiedCdiValue(value);
		if (!cdis.isEmpty()) {
			writer.name(name).beginArray();
			for (var cdi : cdis.entrySet()) {
				writer.beginObject();
				writer.name("old").value(cdi.getKey());
				writer.name("new").value(cdi.getValue());
				writer.endObject(); // name
			}
			writer.endArray(); // name
		}
	}

	/**
	 * Serialize an argument in a cdi filters format
	 */
	protected void serializeCdisFilters(JsonWriter writer, String name, String value) throws IOException {
		List<String> filters = argumentValueGetter.getStrings(value);
		if (!filters.isEmpty()) {
			writer.name(name).beginArray();
			for (String filter : filters) {
				FilterDtm filterDtm = FilterDtm.parseString(filter);
				writer.beginObject();
				writer.name("reset_layer").value(filterDtm.resetLayerName());
				writer.name("filter_layer").value(filterDtm.filterLayerName());
				writer.name("oper").value(filterDtm.type().getToStringName());
				if (filterDtm.valueA().isPresent())
					writer.name("a").value(filterDtm.valueA().getAsDouble());
				if (filterDtm.valueB().isPresent())
					writer.name("b").value(filterDtm.valueB().getAsDouble());
				if (filterDtm.valueCDI().isPresent())
					writer.name("cdi").value(filterDtm.valueCDI().get());
				writer.endObject();
			}
			writer.endArray(); // name
		}
	}

	/**
	 * Serialize an argument in a Layer list format
	 */
	protected void serializeLayers(JsonWriter writer, String name, String value) throws IOException {
		String[] layers = value.split(",");
		if (layers.length > 0) {
			writer.name(name).beginObject();
			for (String layer : layers) {
				writer.name(layer).value(true);
			}
			writer.endObject(); // name
		}
	}

}
