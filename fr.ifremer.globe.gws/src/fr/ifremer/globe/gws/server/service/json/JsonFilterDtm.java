package fr.ifremer.globe.gws.server.service.json;

import java.util.Optional;
import java.util.OptionalDouble;

import com.google.gson.annotations.SerializedName;

import fr.ifremer.globe.core.model.dtm.filter.FilterDtm;
import fr.ifremer.globe.core.model.dtm.filter.FilteringType;

/**
 * A Json representation of FilterDtm
 */
public record JsonFilterDtm(//
		// Name of layer on which to apply cell reset
		@SerializedName("reset_layer") String resetLayerName,
		// Name of the layer on which to apply the filter condition
		@SerializedName("filter_layer") String filterLayerName,
		// Filtering condition type (==, >, <...)
		@SerializedName("oper") FilteringType type,
		// CDI value of the condition when filterLayerName is CDI_LAYER
		@SerializedName("cdi") String valueCDI,
		// First value of the condition
		@SerializedName("a") Double valueA,
		// Second value of the condition (when between condition)
		@SerializedName("b") Double valueB) {

	@Override
	public String toString() {
		return new FilterDtm(resetLayerName, filterLayerName, type, Optional.ofNullable(valueCDI),
				valueA != null ? OptionalDouble.of(valueA) : OptionalDouble.empty(),
				valueB != null ? OptionalDouble.of(valueB) : OptionalDouble.empty()).toString();
	}

}
