package fr.ifremer.globe.gws.server.service.json;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;

/**
 * Utility class to serialize argument's value into String as expected by service
 */
public class ArgumentValueSetter {

	/**
	 * Constructor
	 */
	public ArgumentValueSetter() {
		super();
	}

	/**
	 * Set the String to the specfied argument as value
	 */
	public void setString(String value, GwsServiceArgument serviceArgument) {
		serviceArgument.setValue(value != null ? value : serviceArgument.getDefaultValue());
	}

	/**
	 * Set the Strings to the specfied argument as value
	 */
	public void setStrings(Collection<String> value, GwsServiceArgument serviceArgument) {
		String stringValue = serviceArgument.getDefaultValue();
		if (value != null && !value.isEmpty()) {
			stringValue = StringUtils.join(value.iterator(), ',');
		}
		serviceArgument.setValue(stringValue);
	}

	/**
	 * Set the Dictionary to the specified argument as value.
	 */
	public void setDictionary(Map<String, String> value, GwsServiceArgument serviceArgument) {
		String stringValue = serviceArgument.getDefaultValue();
		if (value != null) {
			stringValue = new GsonBuilder().create().toJson(value);
		}
		serviceArgument.setValue(stringValue);
	}

	/**
	 * Set the object list as value of the specified argument.
	 */
	public void setObjects(List<?> value, GwsServiceArgument serviceArgument) {
		String stringValue = serviceArgument.getDefaultValue();
		if (value != null) {
			stringValue = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()
					.toJson(value);
		}
		serviceArgument.setValue(stringValue);
	}

	/**
	 * Transform a Boolean value to a String and set the result to the specfied argument
	 */
	public void setBoolean(Boolean value, GwsServiceArgument serviceArgument) {
		String stringValue = serviceArgument.getDefaultValue();
		if (value != null) {
			stringValue = value.booleanValue() ? "True" : "False";
		}
		serviceArgument.setValue(stringValue);
	}

	/**
	 * Transform a Double value to a String and set the result to the specfied argument
	 */
	public void setDouble(Double value, GwsServiceArgument serviceArgument) {
		String stringValue = serviceArgument.getDefaultValue();
		if (value != null && !value.isNaN()) {
			stringValue = value.toString();
		}
		serviceArgument.setValue(stringValue);
	}

	/**
	 * Transform a Character value to a String and set the result to the specfied argument
	 */
	public void setCharacter(Character value, GwsServiceArgument serviceArgument) {
		String stringValue = serviceArgument.getDefaultValue();
		if (value != null) {
			stringValue = value.toString();
		}
		serviceArgument.setValue(stringValue);
	}

	/**
	 * Transform a Integer value to a String and set the result to the specfied argument
	 */
	public void setInt(Integer value, GwsServiceArgument serviceArgument) {
		String stringValue = serviceArgument.getDefaultValue();
		if (value != null) {
			stringValue = value.toString();
		}
		serviceArgument.setValue(stringValue);
	}

	/**
	 * Transform a list of int value to a String and set the result to the specfied argument
	 */
	public void setInts(int[] values, GwsServiceArgument serviceArgument) {
		String stringValue = serviceArgument.getDefaultValue();
		if (values != null) {
			stringValue = Arrays.stream(values).mapToObj(String::valueOf).collect(Collectors.joining(","));
		}
		serviceArgument.setValue(stringValue);
	}

	/**
	 * Transform a Duration value to a String and set the result to the specified argument
	 */
	public void setDuration(Duration value, GwsServiceArgument serviceArgument) {
		serviceArgument
				.setValue((value != null) ? String.valueOf(value.toSeconds()) : serviceArgument.getDefaultValue());
	}

	/**
	 * Transform a Duration value to a String and set the result to the specified argument
	 */
	public void setInstant(Instant value, GwsServiceArgument serviceArgument) {
		serviceArgument.setValue((value != null) ? value.toString() : serviceArgument.getDefaultValue());
	}

	/**
	 * Transform a Geobox value to a String and set the result to the specfied argument
	 */
	public void setGeobox(GeoBox value, GwsServiceArgument serviceArgument) {
		String stringValue = serviceArgument.getDefaultValue();
		if (value != null) {
			stringValue = value.getTop() + " " + value.getBottom() + " " + value.getLeft() + " " + value.getRight();
		}
		serviceArgument.setValue(stringValue);
	}

	/**
	 * Transform a ProjectionSettings value to a String and set the result to the specfied argument
	 */
	public void setProjection(ProjectionSettings value, GwsServiceArgument serviceArgument) {
		String stringValue = serviceArgument.getDefaultValue();
		if (value != null) {
			stringValue = value.proj4String();
		}
		serviceArgument.setValue(stringValue);
	}

	/**
	 * Transform a File value to a String and set the result to the specfied argument
	 */
	public void setFile(File value, GwsServiceArgument serviceArgument) {
		String stringValue = serviceArgument.getDefaultValue();
		if (value != null && !value.getPath().isEmpty()) {
			stringValue = value.getAbsolutePath();
		}
		serviceArgument.setValue(stringValue);
	}

	/**
	 * Transform some File values to a String and set the result to the specfied argument
	 */
	public void setFiles(List<File> value, GwsServiceArgument serviceArgument) {
		String stringValue = null; // no default value for files
		if (value != null && !value.isEmpty()) {
			if ("1".equals(serviceArgument.getNargs()) || "?".equals(serviceArgument.getNargs())) {
				stringValue = value.get(0).getPath();
			} else {
				stringValue = StringUtils.join(value, ',');
			}
		}
		serviceArgument.setValue(stringValue);
	}

	/**
	 * Transform modified cdis to a String
	 */
	public void setModifiedCdiValue(List<String> originalCdis, List<String> newCdis,
			GwsServiceArgument serviceArgument) {

		List<ModifyCdi> allModifications = new LinkedList<>();
		for (int i = 0; i < Math.min(originalCdis.size(), newCdis.size()); i++)
			allModifications.add(new ModifyCdi(originalCdis.get(i), newCdis.get(i)));

		Gson gson = new Gson();
		serviceArgument.setValue(gson.toJson(allModifications, ModifyCdi.LIST_MODIFY_CDI_TYPE.getType()));
	}

	public static void main(String[] args) {
		String text = "{old : \"tete\", new:";
		Pattern pattern = Pattern.compile("=");
		String[] strings = pattern.split(text, 2);
		for (String s : strings) {
			System.out.println(s);
		}
		System.out.println("---------");
		String[] strings1 = pattern.split(text);
		for (String s : strings1) {
			System.out.println(s);
		}
	}
}
