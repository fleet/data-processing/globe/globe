package fr.ifremer.globe.gws.server.service.json;

import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.BOOL;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CDI_FILTER;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CDI_MODIFY;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.GEOBOX_COORDS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.IN_FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.LAYERS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.MASK_POLYGONS_REVERSE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.OUT_FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.OVERWRITE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.PROJECTION;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.TIME_INTERVAL_REVERSE_GEO_MASK;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument.DecodedType;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.utils.map.TypedKey;
import fr.ifremer.globe.utils.map.TypedMap;

/**
 * Utility class to deserialize arguments from a json file and set the values to a process
 */
public class JsonArgumentDeserializer {

	private static final Logger LOGGER = LoggerFactory.getLogger(JsonArgumentDeserializer.class);

	private static final Set<String> EXCLUDED_ARGS = Set.of(
			// input/output files not deserialize (avoid to load input files from cache)
			IN_FILE, OUT_FILE,
			// Geobox always computed
			GEOBOX_COORDS);

	private final ArgumentValueSetter argumentValueSetter = new ArgumentValueSetter();

	/**
	 * Constructor
	 */
	public JsonArgumentDeserializer() {
		super();
	}

	/**
	 * Read an argument file and fill the ServiceConfiguration with them
	 * 
	 * 
	 * @param restoreAll true to restore all values, ignoring {@link #EXCLUDED_ARGS} and
	 *            {@link GwsServiceArgument#RESTORE}
	 */
	public void deserializeValues(GwsServiceConf configuration, File inputFile, boolean restoreAll) throws IOException {
		// Map of parsed arguments
		TypedMap argumentValues = new TypedMap();

		Gson gson = new GsonBuilder().create();
		try (JsonReader reader = gson.newJsonReader(new FileReader(inputFile))) {
			if (reader.peek() == JsonToken.BEGIN_OBJECT) {
				reader.beginObject();
				while (reader.peek() != JsonToken.END_OBJECT) {
					if (reader.peek() == JsonToken.NAME) {
						Optional<GwsServiceArgument> argument = configuration.getArgumentOfName(reader.nextName());
						if (reader.peek() != JsonToken.NULL && argument.isPresent()) {
							deserialize(reader, argument.get(), argumentValues, restoreAll);
						} else {
							reader.skipValue();
						}
					} else {
						reader.skipValue();
					}
				}
				reader.endObject();
			}
		} catch (Exception e) {
			// JsonSyntaxException, IllegalStateException or any other JsonReader Exception
			LOGGER.error("Error while reading file : ", e);
		}
	}

	/**
	 * Deserialize one argument
	 * 
	 * @param restoreAll true to restore all values, ignoring {@link #EXCLUDED_ARGS} and
	 *            {@link GwsServiceArgument#RESTORE}
	 */
	protected void deserialize(JsonReader reader, GwsServiceArgument argument, TypedMap argumentValues,
			boolean restoreAll) throws IOException {
		DecodedType[] types = argument.decodeTypes();
		// Consider only the first type.
		String refinedType = types.length > 0 ? types[0].getRefinedType() : null;
		if (refinedType == null)
			return;

		// Test if restoration allowed for this argument
		if (!restoreAll && //
				(EXCLUDED_ARGS.contains(refinedType) && //
						"true".equalsIgnoreCase(
								types[0].getParameters().getOrDefault(GwsServiceArgument.RESTORE, "true"))))
			return;

		if (LAYERS.equalsIgnoreCase(refinedType)) {
			deserializeLayers(reader, argument);
		} else if (CDI_MODIFY.equalsIgnoreCase(refinedType)) {
			deserializeModifiedCdis(reader, argument);
		} else if (CDI_FILTER.equalsIgnoreCase(refinedType)) {
			deserializeCdisFilters(reader, argument);
		} else if (argument.requireListAsValue()) {
			deserializeList(reader, argument);
		} else if (OVERWRITE.equalsIgnoreCase(refinedType) || MASK_POLYGONS_REVERSE.equalsIgnoreCase(refinedType)
				|| TIME_INTERVAL_REVERSE_GEO_MASK.equalsIgnoreCase(refinedType)) {
			deserializeBoolean(reader, argument);
		} else if (PROJECTION.equalsIgnoreCase(refinedType)) {
			deserializeProjection(reader, argument, argumentValues);
		} else if (GEOBOX_COORDS.equalsIgnoreCase(refinedType)) {
			deserializeGeobox(reader, argument, argumentValues);
		} else if (FILE.equalsIgnoreCase(refinedType)) {
			/// do not deserialize file if already set by user selection
			if (argument.getValue() == null || argument.getValue().isEmpty())
				deserializePlainArgument(reader, argument, refinedType);
		} else {
			deserializePlainArgument(reader, argument, refinedType);
		}
	}

	/**
	 * Deserialize a plain String and set the value to the argument
	 */
	protected void deserializePlainArgument(JsonReader reader, GwsServiceArgument argument, String refinedType)
			throws IOException {
		if (BOOL.equals(refinedType)) {
			deserializeBoolean(reader, argument);
		} else {
			JsonToken token = reader.peek();
			if (token == JsonToken.STRING || token == JsonToken.NUMBER || token == JsonToken.BOOLEAN) {
				argumentValueSetter.setString(reader.nextString(), argument);
			} else {
				reader.skipValue();
			}
		}
	}

	/**
	 * Deserialize a Boolean and set the value to the argument
	 */
	protected void deserializeBoolean(JsonReader reader, GwsServiceArgument argument) throws IOException {
		if (reader.peek() == JsonToken.BOOLEAN) {
			argumentValueSetter.setBoolean(reader.nextBoolean(), argument);
		} else {
			reader.skipValue();
		}
	}

	/**
	 * Deserialize a list of String and set the value to the argument
	 */
	protected void deserializeList(JsonReader reader, GwsServiceArgument argument) throws IOException {
		// Dictionary ?
		Gson gson = new GsonBuilder().create();
		if (reader.peek() == JsonToken.BEGIN_OBJECT) {
			Map<String, String> values = gson.fromJson(reader, Map.class);
			argumentValueSetter.setDictionary(values, argument);
			return;
		}

		// List ?
		if (reader.peek() == JsonToken.BEGIN_ARRAY) {
			List<String> values = gson.fromJson(reader, List.class);
			argumentValueSetter.setStrings(values, argument);
		} else {
			reader.skipValue();
		}
	}

	/**
	 * Deserialize a GeoBox and set the value to the argument
	 */
	protected void deserializeGeobox(JsonReader reader, GwsServiceArgument argument, TypedMap argumentValues)
			throws IOException {
		if (reader.peek() == JsonToken.BEGIN_OBJECT) {
			ProjectionSettings projection = argumentValues.get(new TypedKey<ProjectionSettings>(PROJECTION))
					.orElse(StandardProjection.LONGLAT);
			GeoBox geobox = new GeoBox(projection);
			reader.beginObject();
			while (reader.peek() != JsonToken.END_OBJECT) {
				switch (reader.nextName()) {
				case "north":
					geobox.setTop(reader.nextDouble());
					break;
				case "south":
					geobox.setBottom(reader.nextDouble());
					break;
				case "west":
					geobox.setLeft(reader.nextDouble());
					break;
				case "east":
					geobox.setRight(reader.nextDouble());
					break;
				default:
					reader.skipValue();
					break;
				}
			}
			reader.endObject();
			argumentValueSetter.setGeobox(geobox, argument);
		} else {
			reader.skipValue();
		}
	}

	/**
	 * Deserialize an argument in a modified cdi format
	 */
	protected void deserializeModifiedCdis(JsonReader reader, GwsServiceArgument argument) throws IOException {
		if (reader.peek() == JsonToken.BEGIN_ARRAY) {
			List<String> originalCdis = new ArrayList<>();
			List<String> newCdis = new ArrayList<>();
			reader.beginArray();
			while (reader.peek() == JsonToken.BEGIN_OBJECT) {
				reader.beginObject();
				while (reader.peek() == JsonToken.NAME) {
					if ("old".equals(reader.nextName())) {
						originalCdis.add(reader.nextString());
					} else {
						newCdis.add(reader.nextString());
					}
				}
				reader.endObject();
			}
			reader.endArray();
			argumentValueSetter.setModifiedCdiValue(originalCdis, newCdis, argument);
		} else {
			reader.skipValue();
		}
	}

	/**
	 * Deserialize an argument in a cdi filters format
	 */
	protected void deserializeCdisFilters(JsonReader reader, GwsServiceArgument argument) throws IOException {
		if (reader.peek() == JsonToken.BEGIN_ARRAY) {
			TypeToken<LinkedList<JsonFilterDtm>> listFilterDtmType = new TypeToken<>() {
			};
			List<JsonFilterDtm> filterDtms = new Gson().fromJson(reader, listFilterDtmType);
			argumentValueSetter.setStrings(filterDtms.stream().map(JsonFilterDtm::toString).toList(), argument);
		}
	}

	/**
	 * Deserialize an argument in a Layer list format
	 */
	protected void deserializeLayers(JsonReader reader, GwsServiceArgument argument) throws IOException {
		if (reader.peek() == JsonToken.BEGIN_OBJECT) {
			List<String> values = new ArrayList<>();

			reader.beginObject();
			while (reader.peek() != JsonToken.END_OBJECT) {
				String layer = reader.nextName();
				if (reader.nextBoolean()) {
					values.add(layer);
				}
			}
			reader.endObject();
			argumentValueSetter.setStrings(values, argument);
		} else {
			reader.skipValue();
		}
	}

	/**
	 * Deserialize a Projection argument
	 * 
	 * @param argumentValues
	 */
	protected void deserializeProjection(JsonReader reader, GwsServiceArgument argument, TypedMap argumentValues)
			throws IOException {
		ProjectionSettings projection = new ProjectionSettings(reader.nextString());
		argumentValueSetter.setProjection(projection, argument);
		argumentValues.put(new TypedKey<ProjectionSettings>(PROJECTION).bindValue(projection));
	}
}
