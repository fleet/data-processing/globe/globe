package fr.ifremer.globe.gws.server.service.json;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;

/**
 * Utility class to deserialize argument's value as expected by service
 */
public class ArgumentValueGetter {

	private static final Logger LOGGER = LoggerFactory.getLogger(ArgumentValueGetter.class);

	/**
	 * Constructor
	 */
	public ArgumentValueGetter() {
		super();
	}

	/**
	 * Return the String value of the argument
	 *
	 * @return the default value when value is null.
	 */
	public String getValue(GwsServiceArgument serviceArgument) {
		String stringValue = serviceArgument.getValue();
		if (stringValue == null || stringValue.isEmpty()) {
			stringValue = serviceArgument.getDefaultValue();
		}
		if (serviceArgument.requireListAsValue()) {
			stringValue = removeBrackets(stringValue);
		}
		return stringValue != null ? stringValue : "";
	}

	/**
	 * Transform a String value to a Boolean
	 *
	 * @param value to transform. May be null
	 * @return null when value is null
	 */
	public Boolean getBoolean(String value) {
		return value != null && !value.isEmpty() ? "true".equalsIgnoreCase(value) : null;
	}

	/**
	 * Transform a String value to a Double
	 *
	 * @param value to transform. May be null
	 * @return null when value is null
	 */
	public Double getDouble(String value) {
		return value != null && !value.isEmpty() ? NumberUtils.toDouble(value) : null;
	}

	/**
	 * Return the double value of the argument
	 *
	 * @return the default value when value is null.
	 */
	public Double getDouble(GwsServiceArgument serviceArgument) {
		return getDouble(getValue(serviceArgument));
	}

	/**
	 * Return the int value of the argument
	 *
	 * @return the default value when value is null.
	 */
	public Integer getInt(GwsServiceArgument serviceArgument) {
		return getInt(getValue(serviceArgument));
	}

	/**
	 * Transform a String value to a Integer
	 *
	 * @param value to transform. May be null
	 * @return null when value is null
	 */
	public Integer getInt(String value) {
		return value != null && !value.isEmpty() ? (int) NumberUtils.toDouble(value) : null;
	}

	/**
	 * Return the char value of the argument
	 *
	 * @return the default value when value is null.
	 */
	public Character getCharacter(GwsServiceArgument serviceArgument) {
		return getCharacter(getValue(serviceArgument));
	}

	/**
	 * Transform a String value to a Character
	 *
	 * @param value to transform. May be null
	 * @return null when value is null
	 */
	public Character getCharacter(String value) {
		return value != null && !value.isEmpty() ? value.charAt(0) : null;
	}

	/**
	 * Return the duration value of the argument
	 *
	 * @return the default value or 0 when value is missing.
	 */
	public Duration getDuration(GwsServiceArgument serviceArgument) {
		return Duration.ofSeconds(NumberUtils.toInt(getValue(serviceArgument)));
	}

	/**
	 * Return the instant value of the argument
	 *
	 * @return the default value or empty when value is missing
	 */
	public Optional<Instant> getInstant(GwsServiceArgument serviceArgument) {
		String value = getValue(serviceArgument);
		if (!value.isEmpty()) {
			return Optional.of(Instant.parse(value));
		}
		return Optional.empty();
	}

	/**
	 * Transform a String value to a Geobox
	 *
	 * @param value to transform. May be null
	 * @return null when value is null
	 */
	public GeoBox getGeobox(String value) {
		GeoBox result = null;
		if (value != null && !value.isEmpty()) {
			String[] coords = value.split(" ");
			if (coords.length == 4) {
				double north = NumberUtils.toDouble(coords[0], 0d);
				double south = NumberUtils.toDouble(coords[1], 0d);
				double west = NumberUtils.toDouble(coords[2], 0d);
				double east = NumberUtils.toDouble(coords[3], 0d);
				result = new GeoBox(north, south, east, west);
			}
		}
		return result;
	}

	/**
	 * Transform a String value to a File
	 *
	 * @param value to transform. May be null
	 * @return null when value is null
	 */
	public File getFile(String value) {
		File result = null;
		if (value != null) {
			result = new File(value);
		}
		return result;
	}

	/**
	 * Transform a String value to a Strings list
	 *
	 * @param value to transform. May be null
	 * @return empty list when value is null
	 */
	public List<String> getStrings(GwsServiceArgument serviceArgument) {
		return getStrings(getValue(serviceArgument));
	}

	/**
	 * Transform a String value to a Strings list
	 *
	 * @param value to transform. May be null
	 * @return empty list when value is null
	 */
	public List<String> getStrings(String value) {
		List<String> result = null;
		if (value != null && !value.isEmpty() && !"[]".equals(value)) {
			value = removeBrackets(value);
			result = new ArrayList<>(Arrays.asList(value.split(",")));
		}
		return result != null ? result : new ArrayList<>();
	}

	/**
	 * Return the ProcessArgument's value to a dictionary
	 *
	 * @param value to transform. May be null
	 * @return empty map when value is null
	 */
	public Map<String, String> getDictionary(GwsServiceArgument serviceArgument) {
		return getDictionary(getValue(serviceArgument));
	}

	/**
	 * Transform a String value to a Strings map
	 *
	 * @param value to transform. May be null
	 * @return empty map when value is null
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> getDictionary(String value) {
		try {
			// assume the string if formated as JSON
			if (value != null && value.length() > 1)
				return new GsonBuilder().create().fromJson(value, LinkedHashMap.class);
		} catch (JsonParseException e) {
		}
		return Collections.emptyMap();
	}

	/**
	 * Transform a String value to a Files list
	 *
	 * @param value to transform. May be null
	 * @return empty list when value is null
	 */
	public List<File> getFiles(String value) {
		List<File> result = null;
		if (value != null && !value.isEmpty() && !"[]".equals(value)) {
			value = removeBrackets(value);
			String[] files = value.split(",");
			result = Arrays.stream(files)//
					// Must have a real path
					.filter(file -> !FilenameUtils.getPath(file).isEmpty()) //
					.map(File::new)//
					.toList();
		}
		return result != null ? result : new ArrayList<>();
	}

	/**
	 * Transform modified cdis value (String) to a list of Pair (old CDI, new CDI)
	 */
	public Map<String, String> getModifiedCdiValue(String value) {
		Map<String, String> result = new TreeMap<>();
		if (value != null && !value.isEmpty()) {
			Gson gson = new Gson();
			try {
				List<ModifyCdi> modifiedCdis = gson.fromJson(value, ModifyCdi.LIST_MODIFY_CDI_TYPE.getType());
				modifiedCdis.stream()
						.forEach(modifiedCdi -> result.put(modifiedCdi.oldValue(), modifiedCdi.newValue()));
			} catch (JsonSyntaxException e) {
				LOGGER.debug("'{}' is not parsable by json", value);
			}
		}
		return result;
	}

	/**
	 * @return the String value of the argument without the brackets
	 */
	protected String removeBrackets(String value) {
		String result = value;
		if (result != null && !result.isEmpty() && result.charAt(0) == '['
				&& result.charAt(result.length() - 1) == ']') {
			result = result.substring(1, result.length() - 1);
		}
		return result;
	}
}
