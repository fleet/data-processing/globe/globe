package fr.ifremer.globe.gws.server.service;

/**
 * Service get from GWS
 */
public record GwsService(int id, String name, String configurationPath, GwsServiceConf description, String help, String helpPath) {
}
