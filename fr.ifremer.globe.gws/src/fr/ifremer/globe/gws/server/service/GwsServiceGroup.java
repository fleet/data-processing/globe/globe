package fr.ifremer.globe.gws.server.service;

import java.util.List;

/**
 * Group of services names get from GWS
 */
public record GwsServiceGroup(String name, List<GwsService> services, List<GwsServiceGroup> subGroups) {

	/** Deprecated group **/
	public static final String DEPRECATED_GROUP_NAME = "_Deprecated";

	/** Convert groups **/
	public static final List<String> CONVERT_GROUP_NAMES = List.of("export", "export to", "convert", "convert to");

	/**
	 * @return true if this is the deprecated group
	 */
	public boolean isDeprecatedGroup() {
		return DEPRECATED_GROUP_NAME.equalsIgnoreCase(name);
	}

	/**
	 * @return true if this is one of the convert group
	 */
	public boolean isConvertGroup() {
		return CONVERT_GROUP_NAMES.contains(name.toLowerCase());
	}
}
