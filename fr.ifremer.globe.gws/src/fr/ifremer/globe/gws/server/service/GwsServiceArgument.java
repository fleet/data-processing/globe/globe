package fr.ifremer.globe.gws.server.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang.math.DoubleRange;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.math.Range;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import fr.ifremer.globe.core.model.file.ContentType;

/**
 * POJO representing one of the service arguments, belonging to the service configuration.
 *
 * <pre>
	    Exemple of type :
	      resolution(unit=meters)[10.0,30.0]?{merge_type=fill}
	    where :
	      "resolution" is the refined type of the argument used to determine the wizard page
	      "unit" is one the parameters expected by the target wizard page
	      "10.0,30.0" is the range of possible values for the argument
	      "merge_type=fill" is the condition to enable the wizard page. Here, the wizard
	                        page if only visible when the merge_type argument value is
	                        equals to "fill"
 * </pre>
 */
public class GwsServiceArgument {
	/** How to find the type without any parameter, range nor condition */
	protected static final Pattern TYPE_PATTERN = Pattern.compile("(^[\\w#]+)");
	/** How to find a parameter in the type */
	protected static final Pattern PARAMETER_PATTERN = Pattern.compile("\\((.+)\\)");
	/** How to find a range in the type */
	protected static final Pattern RANGE_PATTERN = Pattern.compile("\\[([-+]?\\d+\\.?\\d*),([-+]?\\d+\\.?\\d*)\\]");
	/** How to find a condition in the type ie "?{merge_type=fill}" */
	protected static final Pattern EQUALS_CONDITION_PATTERN = Pattern.compile("\\?\\{(.+)=(.+)\\}");

	/** Parameters in argument's name */
	// "restore=False" means that the argument is never restored from a json argument file (in cache)
	public static final String RESTORE = "restore";
	// Accepted extensions (ie, for input file)
	public static final String EXTENSIONS = "extensions";
	// Accepted extension (ie, for input file)
	public static final String EXTENSION = "extension";

	/** Names of arguments */
	// Files
	public static final String IN_FILE = "infile";
	public static final String OUT_FILE = "outfile";
	public static final String FILE = "file";
	public static final String IN_FOLDER = "infolder";
	public static final String SINGLE_FOLDER = "folder";
	public static final String OVERWRITE = "outfile#overwrite";

	// CDI
	public static final String CDI_SORT = "sort_cdi";
	public static final String CDI_OPERATOR = "cdi_filter#operator";
	public static final String CDI_FILTER = "cdi_filter#filter";
	public static final String CDI_MODIFY = "cdi#modify";
	public static final String CDI_SET = "cdi#set";

	// Mask
	public static final String MASK_ENABLE = "mask#enable";
	public static final String MASK_SIZE = "mask#size";
	public static final String MASK_FILES = "mask#files";

	// Geobox
	public static final String GEOBOX_COORDS = "geobox#coords";
	public static final String GEOBOX_RESOLUTION = "geobox#resolution";
	public static final String PROJECTION = "projection";
	public static final String SPATIAL_RESOLUTION = "spatial_resolution";

	// CSV
	public static final String CSV_DELIMITER = "csv#delimiter";
	public static final String CSV_DECIMALPOINT = "csv#decimalpoint";
	public static final String CSV_DEPTHSIGN = "csv#depthsign";
	public static final String CSV_SKIPROWS = "csv#skiprows";
	public static final String CSV_MANDATORY_HEADERS = "csv#mandatory_headers";
	public static final String CSV_OPTIONAL_HEADERS = "csv#optional_headers";
	public static final String CSV_HEADERS_TYPES = "csv#headers_types";
	public static final String CSV_INDEXES = "csv#indexes";

	// Layers
	public static final String LAYERS = "layers";

	// Sensors
	public static final String SENSORS = "sensors";

	// WC filter
	public static final String WC_FILTERS = "wc_filters";

	// Time interval / cut file
	public static final String TIME_INTERVAL_CUT_FILE = "time_interval#cut_file";
	public static final String TIME_INTERVAL_START_DATE = "time_interval#start_date";
	public static final String TIME_INTERVAL_END_DATE = "time_interval#end_date";
	public static final String TIME_INTERVAL_GEO_MASK_FILE = "time_interval#geo_mask_file";
	public static final String TIME_INTERVAL_REVERSE_GEO_MASK = "time_interval#reverse_geo_mask";

	// Depth correction
	public static final String DEPTH_CORRECTION_TIDE_TYPE = "depth_correction#tide_type";
	public static final String DEPTH_CORRECTION_TIDE_SOURCE = "depth_correction#tide_source";
	public static final String DEPTH_CORRECTION_TIDE_TTB = "depth_correction#tide_ttb";
	public static final String DEPTH_CORRECTION_PLATFORM_ELEVATION_TTB = "depth_correction#platform_elevation_ttb";
	public static final String DEPTH_CORRECTION_NOMINAL_WATERLINE = "depth_correction#nominal_waterline";
	public static final String DEPTH_CORRECTION_DRAUGHT_SOURCE = "depth_correction#draught_source";
	public static final String DEPTH_CORRECTION_DRAUGHT_TTB = "depth_correction#draught_ttb";

	// Geobox
	public static final String MASK_POLYGONS = "polygons";
	public static final String MASK_POLYGONS_REVERSE = "polygons#reverse";

	// Plain types
	public static final String CHECK_LIST = "checklist";
	public static final String BOOL = "bool";
	public static final String DOUBLE = "float";
	public static final String INTEGER = "int";
	public static final String STRING = "string";
	public static final String DURATION = "duration";
	public static final String INSTANT = "datetime";

	/** Name of the argument pointing to the JSON configuration file */
	public static final String CONFIGURATION_ARGUMENT_NAME = "configuration_file";

	/** JSON properties **/
	private String name;
	private String type;
	private String key;
	private String longKey;
	private String help;

	/**
	 * Cardinatity.
	 * <ul>
	 * <li>Value is "1" when only one value is expected</li>
	 * <li>Value is "+" when only several values is allowed</li>
	 * <li>Value is "+|1", for output file page when all case are possible (merge...)</li>
	 * </ul>
	 */
	private String nargs;
	private String[] choices;

	@SerializedName("default")
	@JsonAdapter(ArgumentValueAdapter.class)
	private String defaultValue;
	/** Json process able to provide a value for this argument */
	private String function;

	/** value (not provided by JSON) but fill by Globe **/
	private String value;

	/**
	 * Copy constructor
	 */
	public GwsServiceArgument(GwsServiceArgument serviceArgument) {
		name = serviceArgument.name;
		type = serviceArgument.type;
		key = serviceArgument.key;
		longKey = serviceArgument.longKey;
		help = serviceArgument.help;
		nargs = serviceArgument.nargs;
		choices = serviceArgument.choices;
		defaultValue = serviceArgument.defaultValue;
		function = serviceArgument.function;
		value = serviceArgument.value;
	}

	// GETTERS & SETTERS

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public String getKey() {
		return key;
	}

	public String getLongKey() {
		return longKey;
	}

	public String getHelp() {
		return help;
	}

	public String getNargs() {
		return nargs;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the {@link #function}
	 */
	public String getFunction() {
		return function;
	}

	/**
	 * @param function the {@link #function} to set
	 */
	public void setFunction(String function) {
		this.function = function;
	}

	/**
	 * @return the {@link #choices}
	 */
	public String[] getChoices() {
		return choices;
	}

	/**
	 * @param choices the {@link #choices} to set
	 */
	public void setChoices(String[] choices) {
		this.choices = choices;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return name.hashCode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object other) {
		return this == other || other instanceof GwsServiceArgument && ((GwsServiceArgument) other).name.equals(name);
	}

	/**
	 * @return true when argument expect a list as value
	 */
	public boolean requireListAsValue() {
		return getNargs() != null && !"1".equals(getNargs()) && !"?".equals(getNargs());
	}

	/**
	 * Parse choices as a list of int
	 *
	 * @return null when no choice is defined
	 */
	public int[] getIntChoices() {
		int[] result = null;
		if (choices != null && choices.length > 0) {
			result = Arrays.stream(choices).mapToInt(NumberUtils::toInt).toArray();
		}
		return result;
	}

	/**
	 * @return the label to represent the argument
	 */
	public String getDecoredLabel() {
		return help != null && !help.isEmpty() ? help : name;
	}

	/**
	 * Decode the types and provides all decoded informations<br>
	 * Several types can be specified. For example, a set of files may participate to the input files page and an other
	 * to sort them (by CDI...). <br>
	 * These types are separated with an &
	 *
	 * @return an array of DecodedType or null when no type is defined
	 */
	public DecodedType[] decodeTypes() {
		DecodedType[] result = null;
		if (type != null) {
			String[] subType = type.split("&");
			result = new DecodedType[subType.length];
			for (int i = 0; i < subType.length; i++) {
				DecodedType decodedType = new DecodedType();
				decodedType.refinedType = getRefinedType(subType[i]);
				decodedType.parameters = getTypeParameter(subType[i]);
				decodedType.range = getDoubleRange(subType[i]);
				decodedType.equalsCondition = getEqualsCondition(subType[i]);
				result[i] = decodedType;

				String[] contentTypes = decodedType.parameters.getOrDefault("content_type", "").split("\\|");
				decodedType.contentTypes = Arrays.stream(contentTypes).map(ContentType::getContentTypeSetByName)
						.flatMap(Set::stream).collect(Collectors.toSet());
			}
		}
		return result;
	}

	/**
	 * @return the subtype without any range (ie [0,10]) or option (ie (*.nc))
	 */
	protected String getRefinedType(String subtype) {
		String result = "";
		Matcher matcher = TYPE_PATTERN.matcher(subtype);
		if (matcher.find()) {
			result = matcher.group(1);
		}
		return result;
	}

	/**
	 * Parse the range of int contained in the type
	 *
	 * @return null when no range is defined
	 */
	protected DoubleRange getDoubleRange(String subtype) {
		DoubleRange result = null;
		Matcher matcher = RANGE_PATTERN.matcher(subtype);
		if (matcher.find()) {
			result = new DoubleRange(NumberUtils.toDouble(matcher.group(1)), NumberUtils.toDouble(matcher.group(2)));
		}
		return result;
	}

	/**
	 * Extract the equals condition contained in the type
	 *
	 * @return an array of 2 Strings containing the operands or null when no condition is defined
	 */
	protected String[] getEqualsCondition(String subtype) {
		String[] result = null;
		Matcher matcher = EQUALS_CONDITION_PATTERN.matcher(subtype);
		if (matcher.find() && matcher.groupCount() == 2) {
			result = new String[] { matcher.group(1), matcher.group(2) };
		}
		return result;
	}

	/**
	 * Extract the String contained in the type and surrounded by parenthesis
	 */
	protected Map<String, String> getTypeParameter(String subtype) {
		Map<String, String> result = new HashMap<>();
		if (subtype != null) {
			Matcher matcher = PARAMETER_PATTERN.matcher(subtype);
			if (matcher.find()) {
				String[] parametersGroup = matcher.group(1).split(",");
				Arrays.stream(parametersGroup)//
						.map(p -> p.split("="))//
						.filter(o -> o.length == 2)//
						.forEach(o -> result.put(o[0].toLowerCase(), o[1]));
			}
		}
		return result;
	}

	public static class DecodedType {
		protected String refinedType;
		protected Map<String, String> parameters;
		protected Set<ContentType> contentTypes = EnumSet.noneOf(ContentType.class);
		protected Range range;
		protected String[] equalsCondition;

		/**
		 * @return the {@link #refinedType}
		 */
		public String getRefinedType() {
			return refinedType;
		}

		/**
		 * @return the {@link #parameters}
		 */
		public Map<String, String> getParameters() {
			return parameters;
		}

		/**
		 * @return the {@link #contentTypes}
		 */
		public Set<ContentType> getContentTypes() {
			return contentTypes;
		}

		/**
		 * @return the {@link #range}
		 */
		public Range getRange() {
			return range;
		}

		/**
		 * @return the {@link #equalsCondition}
		 */
		public String[] getEqualsCondition() {
			return equalsCondition;
		}
	}

	/**
	 * Adapter of a value (as String, List, Map...)
	 */
	public class ArgumentValueAdapter extends TypeAdapter<String> {
		@Override
		public void write(JsonWriter writer, String value) throws IOException {
			// Never called, see JsonArgumentSerializer
		}

		@Override
		public String read(JsonReader reader) throws IOException {
			var token = reader.peek();
			if (token == JsonToken.STRING || token == JsonToken.NUMBER) {
				return reader.nextString();
			}
			Gson gson = new GsonBuilder().create();
			if (token == JsonToken.BOOLEAN) {
				return gson.toJson(reader.nextBoolean());
			}
			if (token == JsonToken.BEGIN_ARRAY) {
				List<String> values = gson.fromJson(reader, List.class);
				return StringUtils.join(values, ',');
			}
			if (token == JsonToken.BEGIN_OBJECT) {
				Map<String, String> values = gson.fromJson(reader, Map.class);
				return gson.toJson(values);
			}
			reader.skipValue();
			return null;
		}
	}

	/**
	 * @param nargs the {@link #nargs} to set
	 */
	public void setNargs(String nargs) {
		this.nargs = nargs;
	}

}
