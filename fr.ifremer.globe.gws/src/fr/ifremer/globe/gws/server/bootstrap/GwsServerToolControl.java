package fr.ifremer.globe.gws.server.bootstrap;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.core.runtime.gws.GwsBootstrapService;
import fr.ifremer.globe.core.runtime.gws.event.GwsEventTopics;
import fr.ifremer.globe.core.runtime.gws.event.GwsState;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.image.Icons;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

/**
 * 
 */
public class GwsServerToolControl {

	@Inject
	private GwsBootstrapService gwsBootstrapService;

	private Label statusIcon;
	private Label statusLabel;

	/**
	 * Initialize the control widget
	 * 
	 * @param parent Composite initialized by StatusBarControl class
	 */
	@PostConstruct
	public void postConstruct(Composite parent) {
		Label separator = new Label(parent, SWT.SEPARATOR);
		GridData gdSeparator = new GridData(SWT.RIGHT, SWT.FILL, true, false, 1, 1);
		gdSeparator.heightHint = 10;
		separator.setLayoutData(gdSeparator);

		statusIcon = new Label(parent, SWT.NONE);
		statusLabel = new Label(parent, SWT.NONE);

		statusIcon.addMouseListener(MouseListener.mouseDoubleClickAdapter(e -> manageDoubleClick()));
		statusLabel.addMouseListener(MouseListener.mouseDoubleClickAdapter(e -> manageDoubleClick()));

		manageGwsStat(gwsBootstrapService.getServerState());
	}

	private void manageDoubleClick() {
		if (gwsBootstrapService.getServerState() == GwsState.NOT_AVAILABLE
				&& Messages.openSyncQuestionMessage("Toobox server is not running", "Restart toolbox server ?")) {
			gwsBootstrapService.startServing();
			gwsBootstrapService.startDriverServices();
		}
	}

	/**
	 * Event handler for TOPIC_SAVE : saving of context is required
	 */
	@Inject
	@Optional
	public void manageGwsStat(@UIEventTopic(GwsEventTopics.TOPIC_SERVER_STATE) GwsState gwsState) {
		switch (gwsState) {
		case NOT_AVAILABLE:
			statusIcon.setImage(Icons.UNAVAILABLE.toImage());
			statusLabel.setText("Toolbox off.");
			break;
		case STARTING:
			statusIcon.setImage(Icons.SAND_TIMER.toImage());
			statusLabel.setText("Toolbox loading...");
			break;

		default: // Running
			statusIcon.setImage(Icons.OK.toImage());
			statusLabel.setText("Toolbox on");
			break;
		}
		statusLabel.getParent().layout();
	}

}
