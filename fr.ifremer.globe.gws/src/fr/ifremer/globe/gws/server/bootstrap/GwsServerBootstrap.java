package fr.ifremer.globe.gws.server.bootstrap;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.runtime.gws.GwsBootstrapService;
import fr.ifremer.globe.core.runtime.gws.event.GwsEventTopics;
import fr.ifremer.globe.core.runtime.gws.event.GwsState;
import fr.ifremer.globe.core.runtime.job.GProcess;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.gws.ContextInitializer;
import fr.ifremer.globe.gws.preferences.GwsPreferences;
import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.process.driver.adcp.AdcpDriverProcess;
import fr.ifremer.globe.gws.server.process.driver.adcp.AdcpInfoSupplier;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.gws.server.process.driver.xsf.XsfDriverProcess;
import fr.ifremer.globe.gws.server.process.driver.xsf.XsfNavigationSupplierExtended;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.cache.WorkspaceCache;
import fr.ifremer.globe.utils.osgi.OsgiUtils;
import io.rsocket.RSocket;
import io.rsocket.core.RSocketServer;
import io.rsocket.transport.netty.server.CloseableChannel;
import io.rsocket.transport.netty.server.TcpServerTransport;
import reactor.core.publisher.Mono;

/**
 * Launcher GWS server
 */
@Component(name = "globe_gws_bootstrap_service", service = { GwsBootstrapService.class, GwsServerBootstrap.class })
public class GwsServerBootstrap implements GwsBootstrapService {

	/** Logger */
	private static final Logger log = LoggerFactory.getLogger(GwsServerBootstrap.class);

	@Reference
	private AdcpInfoSupplier adcpInfoSupplier;

	@Reference
	private XsfNavigationSupplierExtended xsfNavigationSupplier;

	/** Unique instance of GwsServerProxy */
	private GwsPreferences gwsPreferences;
	/** Unique instance of GwsServerProxy */
	private GwsServerProxy gwsServerProxy;

	/** RSocket connection with GWS to maintain a keep alive signal */
	private Optional<CloseableChannel> keepAliveConnection = Optional.empty();

	/** Process of GWS */
	private Optional<Process> gwsProcess = Optional.empty();
	private GwsState gwsState = GwsState.NOT_AVAILABLE;

	/** {@inheritDoc} */
	@Override
	public void startServing() {
		// gwsPreferences null when this is the first call
		if (gwsPreferences == null) {
			gwsPreferences = new GwsPreferences(PreferenceRegistry.getInstance().getRootNode());
			gwsServerProxy = new GwsServerProxy();

			if (gwsPreferences.getLaunchServerAtStartup())
				runGwsProcess();
		} else if (gwsState == GwsState.NOT_AVAILABLE)
			runGwsProcess();
	}

	private void runGwsProcess() {
		setGwsState(GwsState.STARTING);
		IProcessService processService = IProcessService.grab();
		GProcess process = processService.createProcess("Toolbox", this::startServingInBackground);
		process.addJobChangeListener(IJobChangeListener.onDone(e -> whenGwsStopped(process)));
		processService.runInForeground(process);
	}

	@Override
	public void stopServing() {
		gwsProcess.ifPresent(Process::destroy);
		gwsProcess = Optional.empty();
		setGwsState(GwsState.NOT_AVAILABLE);
	}

	/** Starting all drivers services (for ADCP files...) */
	@Override
	public void startDriverServices() {
		startAdcpDriverService();
		startXsfDriverService();
	}

	/** Starting adcp driver service (for ADCP files...) */
	private void startAdcpDriverService() {
		log.info("Running '" + AdcpDriverProcess.ADCP_DRIVER_SERVICE + "'");
		var optAdcpService = gwsServerProxy.findPyatDriverService(AdcpDriverProcess.ADCP_DRIVER_SERVICE);
		if (optAdcpService.isPresent()) {
			// Store the driver service to the bundle context, not visible from the rest of application
			AdcpDriverProcess adcpDriverService = new AdcpDriverProcess(optAdcpService.get());
			ContextInitializer.inject(adcpDriverService);
			adcpInfoSupplier.setAdcpDriverProcess(adcpDriverService);
			reloadFiles(ContentType.ADCP_STA);
		} else {
			log.warn("'" + AdcpDriverProcess.ADCP_DRIVER_SERVICE
					+ "' service not configured in GWS. ADCP files will not be managed");
		}
	}
	
	/** Starting xsf driver service (to load additional XSF files functionalities like backscatter acquisition mode...) */
	private void startXsfDriverService() {
		log.info("Running '" + XsfDriverProcess.XSF_DRIVER_SERVICE + "'");
		var optXsfService = gwsServerProxy.findPyatDriverService(XsfDriverProcess.XSF_DRIVER_SERVICE);
		if (optXsfService.isPresent()) {
			// Store the driver service to the bundle context, not visible from the rest of application
			XsfDriverProcess xsfDriverService = new XsfDriverProcess(optXsfService.get());
			ContextInitializer.inject(xsfDriverService);
			xsfNavigationSupplier.setXsfDriverProcess(xsfDriverService);
			reloadFiles(ContentType.XSF_NETCDF_4);
		} else {
			log.warn("'" + XsfDriverProcess.XSF_DRIVER_SERVICE
					+ "' service not configured in GWS. Xsf file extended will not be managed");
		}
	}

	@Override
	public GwsState getServerState() {
		return gwsState;
	}

	/** Start the server */
	private IStatus startServingInBackground(IProgressMonitor monitor, Logger logger) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, "Launching Toolbox...", 100);
		subMonitor.subTask("Starting Toolbox server (GWS)...");
		try {
			openKeepAlivePort();
			runGwsServer();

			// GWS run in local ?
			String debugPort = System.getProperty("GWS_PORT");
			if (debugPort != null && gwsServerProxy.isRunning().onErrorReturn(false).block()) {
				setGwsState(GwsState.RUNNING);
				return Status.OK_STATUS;
			}

			if (gwsProcess.isPresent()) {
				Process process = gwsProcess.get();
				subMonitor.worked(50);
				subMonitor.subTask("Loading Toolbox processes and drivers...");

				// Wait for ADCP Driver Service before continue...
				for (int attempt = 0; process.isAlive() && attempt < 10; attempt++) {
					subMonitor.worked(5);
					if (gwsServerProxy.findPyatDriverService(AdcpDriverProcess.ADCP_DRIVER_SERVICE).isPresent())
						break;
					try {
						Thread.sleep(2000l); // Suspend a while before retrying
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
					}
				}
				subMonitor.done();
				return process.isAlive() ? Status.OK_STATUS : Status.error("Toolbox server died");
			} else {
				setGwsState(GwsState.NOT_AVAILABLE);
				return Status.error("Unable to run the toolbox server");
			}
		} catch (Exception e) {
			setGwsState(GwsState.NOT_AVAILABLE);
			log.error("Unable to launch toolbox server", e);
			return Status.error("Unable to launch toolbox server", e);
		}
	}

	/** Open a TCP socket server to maintain a keep alive signal with GWS */
	private void openKeepAlivePort() {
		var tcpServerTransport = TcpServerTransport.create(0);

		keepAliveConnection = Optional.of(RSocketServer.create() //
				// Create an acceptor to catch the moment when the connection is lost
				.acceptor((setup, gwsSocket) -> {
					// GWS is connected on keep alive port
					setGwsState(GwsState.RUNNING);

					// Catch the moment when the connection is lost
					gwsSocket.onClose().doFinally(s -> stopServing()).subscribe();

					return Mono.just(new RSocket() {
					});
				})
				// Bind to TCP server
				.bindNow(tcpServerTransport));

		keepAliveConnection.ifPresent(c -> log.info("Keep alive port is {}", c.address().getPort()));
	}

	/** Choose a free port for GWS */
	private int choosePort() {
		// Force using a port ?
		String debugPort = System.getProperty("GWS_PORT");
		if (debugPort != null) {
			try {
				return Integer.valueOf(debugPort);
			} catch (NumberFormatException e) {
				log.debug("Property GWS_PORT is not an integer {}", debugPort);
			}
		}

		try (ServerSocket serverSocket = new ServerSocket(0)) {
			return serverSocket.getLocalPort();
		} catch (IOException e) {
			log.warn("Error when choosing a port for GWS. Using default one (8081)", e);
			return 8081;
		}
	}

	/** Run a subprocess to deploy the GWS server */
	private void runGwsServer() throws URISyntaxException, IOException {
		// Get the GWS jar file
		URL gwsJarUrl = FileLocator.resolve(FrameworkUtil.getBundle(getClass()).getEntry("/libraries/gws.jar"));
		File gwsjarfile = FileUtils.toFile(gwsJarUrl);

		// Launch the process
		List<String> cmdLine = new LinkedList<>();
		cmdLine.add("javaw");

		// HTTP port
		int port = choosePort();
		gwsServerProxy.setHttpPort(port);
		cmdLine.add("-Dserver.port=" + port);

		// Set temporary folder
		File tempDir = WorkspaceCache.getCacheDirectory("gws");
		cmdLine.add("-Djava.io.tmpdir=" + tempDir.getAbsolutePath());

		cmdLine.add("-jar");
		cmdLine.add(gwsjarfile.getAbsolutePath());

		// Conda environment
		String condaEnv = gwsPreferences.getCondaEnv();
		File condaEnvPrefix = new File(condaEnv);
		if (condaEnvPrefix.exists() && !condaEnvPrefix.isAbsolute())
			condaEnv = condaEnvPrefix.getCanonicalPath();
		log.info("Using conda env '{}'", condaEnv);

		cmdLine.add(condaEnv);

		// Keep alive port
		keepAliveConnection.ifPresent(c -> cmdLine.add("--keep_alive_port=" + c.address().getPort()));

		ProcessBuilder builder = new ProcessBuilder(cmdLine);
		builder.directory(tempDir);

		// Required environment variables
		builder.environment().put("CONDA_PATH", getCondaPath());
		builder.inheritIO();

		// Launch process
		gwsProcess = Optional.of(builder.start());
	}

	/** Change and notify the server state */
	private void setGwsState(GwsState newState) {
		if (newState != gwsState) {
			gwsState = newState;
			OsgiUtils.getService(IEventBroker.class).post(GwsEventTopics.TOPIC_SERVER_STATE, gwsState);
		}
	}

	/**
	 * @return the {@link #gwsPreferences}
	 */
	public GwsPreferences getGwsPreferences() {
		return gwsPreferences;
	}

	/**
	 * @return the {@link #gwsServerProxy}
	 */
	public GwsServerProxy getGwsServerProxy() {
		return gwsServerProxy;
	}

	/** Compute conda path. Checks that conda executable exists before launching GWS */
	private String getCondaPath() throws IOException {
		File condaDir = gwsPreferences.getCondaDirPath().toFile();
		if (!condaDir.isDirectory())
			throw new IOException("Bad value for preference '" + gwsPreferences.getCondaDirPathPref().getDisplayedName()
					+ "', unable to run Toolbox server");
		String condaPath = condaDir.getCanonicalPath();
		log.info("Using conda path {}", condaPath);

		// Check conda executable
		String condaExec = condaPath;
		String os = System.getProperty("os.name").toLowerCase();
		if (os.contains("win"))
			condaExec += "\\condabin\\conda.bat";
		else
			condaExec += "/condabin/conda";
		if (!new File(condaExec).canExecute()) {
			log.warn("{} not found or not executable", condaExec);
			throw new IOException(
					"Conda executable not found, unable to run the toolbox server.\nCheck the configuration (Windows -> Preferences -> '"
							+ GwsPreferences.NAME + "') and restart the toolbox manually from the status bar.");

		}

		return condaPath;
	}

	/** React when GWS process died */
	private void whenGwsStopped(GProcess process) {
		IStatus result = process.getResult();
		if (!result.isOK()) {
			Throwable exception = result.getException();
			if (exception != null)
				Messages.openErrorMessage(result.getMessage(), exception.getMessage());
			else
				Messages.openErrorMessage("Toolbox server stopped", result.getMessage());
			setGwsState(GwsState.NOT_AVAILABLE);
		}
	}

	/**
	 * Invoked when toolbox driver is now available.<br>
	 * Browse file model and request the loading of files of the specified type
	 */
	private void reloadFiles(ContentType fileType) {
		IFileLoadingService fileLoadingService = IFileLoadingService.grab();
		List<String> adcpFiles = fileLoadingService.getFileInfoModel().getAll().stream()//
				.filter(f -> f.getContentType() == fileType) //
				.map(IFileInfo::getPath) //
				.toList();
		if (!adcpFiles.isEmpty())
			fileLoadingService.reload(adcpFiles, false);
	}

}
