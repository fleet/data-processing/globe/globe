package fr.ifremer.globe.gws.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import fr.ifremer.globe.gws.ContextInitializer;
import fr.ifremer.globe.gws.server.job.GwsJob;
import fr.ifremer.globe.gws.server.logbook.GwsLogBookEvent;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.GwsServiceGroup;
import fr.ifremer.globe.gws.server.service.json.JsonArgumentSerializer;
import fr.ifremer.globe.utils.exception.GIOException;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpResponseStatus;
import reactor.core.publisher.Mono;
import reactor.netty.ByteBufMono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.http.client.HttpClientResponse;

/**
 * Eclipse component to manage requests of GWS server
 */
public class GwsServerProxy {

	private static final Logger logger = LoggerFactory.getLogger(GwsServerProxy.class);

	/** Group names in GWS service hierarchy */
	private static final String TOOLBOX_SERVICES_GROUP = "toolbox_services";
	private static final String WIZARD_FUNCTIONS_GROUP = "wizard_functions";
	private static final String SPECIFIC_SERVICES_GROUP = "specific_services";
	private static final String DRIVER_SERVICES_GROUP = "driver_services";

	/** Cache of detailed service */
	private final Map<Integer, GwsService> detailedServiceCache = new HashMap<>();

	/** Http port of GWS */
	private int httpPort = 0;

	/** Return the details of a service */
	public Optional<GwsService> detailService(int id) {
		GwsService result = detailedServiceCache.get(id);
		if (result == null) {
			HttpClient client = gainHttpClient();
			InputStream response = client.get()//
					.uri("/gws/services/" + id)//
					.responseContent()//
					.aggregate()//
					.asInputStream()//
					.onErrorComplete()// Return null on error
					.block();
			if (response != null) {
				try (JsonReader reader = new JsonReader(new InputStreamReader(response))) {
					Gson gson = new GsonBuilder() //
							.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)//
							.create();
					result = gson.fromJson(reader, GwsService.class);
					detailedServiceCache.put(id, result);
				} catch (IOException e) {
					logger.warn("Error while trying to get the details of a service ", e);
				}
			}
		}
		return Optional.ofNullable(result);
	}

	private HttpClient gainHttpClient() {
		return HttpClient.create().host("localhost").port(httpPort);
	}

	/**
	 * Retrieve the hierachy of services from GWS.
	 */
	public Optional<GwsServiceGroup> getAllServices() {
		HttpClient client = gainHttpClient();
		InputStream response = client.get()//
				.uri("/gws/services")//
				.responseContent()//
				.aggregate()//
				.asInputStream()//
				.onErrorComplete()// Return null on error
				.block();

		if (response != null) {
			try (JsonReader reader = new JsonReader(new InputStreamReader(response))) {
				Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
						.create();
				return Optional.ofNullable(gson.fromJson(reader, GwsServiceGroup.class));
			} catch (IOException e) {
				logger.warn("Error while trying to get service hierarchy", e);
			}
		}
		return Optional.empty();
	}

	/** Search the service by its name */
	public Optional<GwsService> findService(String serviceName) {
		Optional<GwsServiceGroup> rootServiceGroup = getAllServices();
		return rootServiceGroup.flatMap(g -> findService(g, serviceName));
	}

	/** Search the service in the specified group */
	public Optional<GwsService> findService(GwsServiceGroup parentGroup, String serviceName) {
		Optional<GwsService> result = parentGroup.services().stream()
				.filter(s -> serviceName.equalsIgnoreCase(s.name())).findFirst();
		if (result.isEmpty()) {
			for (GwsServiceGroup subServiceGroup : parentGroup.subGroups()) {
				result = findService(subServiceGroup, serviceName);
				if (result.isPresent())
					break;
			}
		}
		return result;
	}

	/** Search the "pyat_toolbox_services" group in the hierarchy */
	public Optional<GwsServiceGroup> findToolboxServicesGroup() {
		Optional<GwsServiceGroup> rootServiceGroup = getAllServices();
		return rootServiceGroup.flatMap(g -> findGroup(g, TOOLBOX_SERVICES_GROUP));
	}

	/** Search the "pyat_wizard_functions" group in the hierarchy */
	public Optional<GwsServiceGroup> findPyatWizardFunctionsGroup() {
		Optional<GwsServiceGroup> rootServiceGroup = getAllServices();
		return rootServiceGroup.flatMap(g -> findGroup(g, WIZARD_FUNCTIONS_GROUP));
	}

	/** Search the "pyat_specific_services" group in the hierarchy */
	public Optional<GwsServiceGroup> findPyatSpecificServicesGroup() {
		Optional<GwsServiceGroup> rootServiceGroup = getAllServices();
		return rootServiceGroup.flatMap(g -> findGroup(g, SPECIFIC_SERVICES_GROUP));
	}

	/** Search the "pyat_driver_services" group in the hierarchy */
	public Optional<GwsServiceGroup> findPyatDriverServicesGroup() {
		Optional<GwsServiceGroup> rootServiceGroup = getAllServices();
		return rootServiceGroup.flatMap(g -> findGroup(g, DRIVER_SERVICES_GROUP));
	}

	/** Search the service by its name in the specific service group */
	public Optional<GwsService> findPyatSpecificService(String serviceName) {
		return findPyatSpecificServicesGroup().flatMap(g -> findService(g, serviceName));
	}

	/** Search the service by its name in the driver service group */
	public Optional<GwsService> findPyatDriverService(String serviceName) {
		return findPyatDriverServicesGroup().flatMap(g -> findService(g, serviceName));
	}

	/** Returns the detailed GwsService of the pyat function holding to the specified configuration path */
	public Optional<GwsService> detailFunction(String configurationPath) {
		GwsService result = null;
		GwsServiceGroup rootGroup = findPyatWizardFunctionsGroup().orElse(null);
		if (rootGroup != null) {
			// Trick to avoid path separator problem
			String securedConfPath = configurationPath.replaceAll("\\W+", "_");
			result = rootGroup.services().stream()
					.filter(s -> s.configurationPath().replaceAll("\\W+", "_").endsWith(securedConfPath)).findFirst()
					.orElse(null);
		}

		return result != null ? detailService(result.id()) : Optional.empty();
	}

	/** Search a service group in the hierarchy */
	private Optional<GwsServiceGroup> findGroup(GwsServiceGroup serviceGroup, String groupName) {
		if (groupName.equalsIgnoreCase(serviceGroup.name()))
			return Optional.of(serviceGroup);
		Optional<GwsServiceGroup> result = Optional.empty();
		for (GwsServiceGroup subServiceGroup : serviceGroup.subGroups()) {
			result = findGroup(subServiceGroup, groupName);
			if (result.isPresent())
				break;
		}
		return result;
	}

	/**
	 * Post a request to GWS for launching a service execution.
	 * 
	 * @param recordInLogBook true to record the execution in the log book of GWS
	 */
	public Optional<GwsJob> launchService(GwsService gwsService, GwsServiceConf configuration,
			boolean recordInLogBook) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try (var stringWriter = new StringWriter(); var jsonWriter = gson.newJsonWriter(stringWriter)) {
			new JsonArgumentSerializer().serialize(configuration.arguments(), jsonWriter);
			HttpClient client = gainHttpClient();
			GwsJob gwsJob = client.post()//
					.uri("/gws/services/" + gwsService.id() + (recordInLogBook ? "?logbook=true" : ""))//
					.send((req, channel) -> {
						req.addHeader(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON);
						req.addHeader(HttpHeaderValues.CHARSET, StandardCharsets.UTF_8.name());
						return channel.sendString(Mono.just(stringWriter.toString()), StandardCharsets.UTF_8);
					}) //
					.responseSingle(this::parseLaunchResponse) //
					.onErrorComplete() //
					.block();
			return Optional.ofNullable(gwsJob);
		} catch (IOException e) {
			logger.warn("Error while generationg JSon parameters : ", e);
		}

		return Optional.empty();
	}

	/** Parse the text contained in the response and instantiate the corresponding Job */
	private Mono<GwsJob> parseLaunchResponse(HttpClientResponse response, ByteBufMono content) {
		if (HttpResponseStatus.OK.equals(response.status())) {
			return content.asString().map(rawText -> {
				Gson gson = new GsonBuilder()//
						.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)//
						.create();
				return gson.fromJson(rawText, GwsJob.class);
			});
		}
		return Mono.empty();

	}

	/** @return the unique instance of GwsServerProxy as soon as it is created */
	public static GwsServerProxy grab() {
		return ContextInitializer.getInContext(GwsServerProxy.class);
	}

	/** @return true if GWS is up */
	public Mono<Boolean> isRunning() {
		HttpClient client = gainHttpClient();
		return client.get()//
				.uri("/gws/version")//
				.responseContent()//
				.next()//
				.hasElement();
	}

	/**
	 * Retrieve the complete log book from GWS.
	 */
	public List<GwsLogBookEvent> getAllLogBookEvent() {
		HttpClient client = gainHttpClient();
		InputStream response = client.get()//
				.uri("/gws/logbook")//
				.responseContent()//
				.aggregate()//
				.asInputStream()//
				.onErrorComplete()// Return null on error
				.block();

		if (response != null) {
			try (JsonReader reader = new JsonReader(new InputStreamReader(response))) {
				Gson gson = makeLogBookGson();
				TypeToken<LinkedList<GwsLogBookEvent>> eventListType = new TypeToken<>() {
				};
				return gson.fromJson(reader, eventListType);
			} catch (IOException e) {
				logger.warn("Error while trying to get service hierarchy", e);
			}
		}
		return List.of();
	}

	/** Return the json report of a log book event */
	public Optional<InputStream> reportLogBookEvent(long id) {
		HttpClient client = gainHttpClient();
		InputStream response = client.get()//
				.uri("/gws/logbook/" + id)//
				.responseContent()//
				.aggregate()//
				.asInputStream()//
				.doOnError(e -> logger.warn(e.getMessage())) //
				.onErrorComplete()// Return null on error
				.block();
		return Optional.ofNullable(response);
	}

	/** Return the details of a log book event */
	public Optional<GwsLogBookEvent> detailLogBookEvent(long id) throws GIOException, IOException {
		var inputStream = reportLogBookEvent(id);
		return inputStream.isPresent() ? parseReport(inputStream.get()) : Optional.empty();
	}

	/** Parse a report to a GwsLogBookEvent */
	public Optional<GwsLogBookEvent> parseReport(InputStream report) throws IOException, GIOException {
		try (JsonReader reader = new JsonReader(new InputStreamReader(report))) {
			Gson gson = makeLogBookGson();
			GwsLogBookEvent gwsLogBookEvent = gson.fromJson(reader, GwsLogBookEvent.class);

			// Check if service described in report matches an existing service.
			var eventService = gwsLogBookEvent.service();
			var currentService = findService(eventService.name());
			if (currentService.isEmpty()) {
				throw new GIOException("Service described in report not found : " + eventService.name());
			} else {
				/// Override report service with current (to handle case where ID has changed...)
				gwsLogBookEvent = gwsLogBookEvent.copyWith(currentService.get());
			}
			return Optional.ofNullable(gwsLogBookEvent);
		}
	}

	/** Make a Gson suitable to parse a GwsLogBookEvent */
	private Gson makeLogBookGson() {
		return new GsonBuilder()//
				.registerTypeAdapter(LocalDateTime.class,
						(JsonDeserializer<LocalDateTime>) (json, type, jsonDeserializationContext) -> LocalDateTime
								.parse(json.getAsJsonPrimitive().getAsString()))//
				.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)//
				.create();
	}

	/** Request GWS to delete all events */
	public void deleteAllLogBookEvent() {
		HttpClient client = gainHttpClient();
		client.delete()//
				.uri("/gws/logbook")//
				.response()//
				.doOnError(e -> logger.warn(e.getMessage())) //
				.block();
	}

	/** Request GWS to delete the specified events */
	public void deleteLogBookEvent(long id) {
		HttpClient client = gainHttpClient();
		client.delete()//
				.uri("/gws/logbook/" + id)//
				.response()//
				.doOnError(e -> logger.warn(e.getMessage())) //
				.block();
	}

	public int getHttpPort() {
		return httpPort;
	}

	public void setHttpPort(int httpPort) {
		this.httpPort = httpPort;
	}

}
