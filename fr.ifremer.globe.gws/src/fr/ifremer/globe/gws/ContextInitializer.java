package fr.ifremer.globe.gws;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.di.extensions.EventTopic;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.osgi.service.event.Event;

import fr.ifremer.globe.core.runtime.gws.GwsBootstrapService;
import fr.ifremer.globe.gws.server.bootstrap.GwsServerBootstrap;

/**
 * Application Add-on used to initialize the IEclipseContext of the bundle
 */
public class ContextInitializer {

	/** Static reference of Eclipse context initialized by E4Application */
	protected static IEclipseContext eclipseContext;

	/**
	 * Entry point, when E4 model is deployed
	 */
	@Inject
	@Optional
	public void applicationStarted(@EventTopic(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE) Event event,
			IEclipseContext injectedEclipseContext, GwsServerBootstrap gwsServerBootstrap,
			GwsBootstrapService gwsBootstrapService) {
		eclipseContext = injectedEclipseContext;

		// Contextualize objects initialized during bootstrap
		setInContext(gwsServerBootstrap.getGwsPreferences());
		setInContext(gwsServerBootstrap.getGwsServerProxy());

		// Start GWS drivers (done here to get properly all injected dependencies).
		gwsBootstrapService.startDriverServices();
	}

	/**
	 * Getter of {@link #eclipseContext}
	 */
	public static IEclipseContext getEclipseContext() {
		return eclipseContext;
	}

	/**
	 * Returns the context value associated with the given name
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getInContext(String name) {
		return (T) getEclipseContext().get(name);
	}

	/**
	 * Sets a value to be associated with the class name in this context
	 */
	@SuppressWarnings("unchecked")
	public static <T> void setInContext(T object) {
		getEclipseContext().set(((Class<T>) object.getClass()), object);
	}

	public static <T> T getInContext(Class<T> clazz) {
		return getEclipseContext().get(clazz);
	}

	/**
	 * Sets a value to be associated with a given name in this context
	 */
	public static void setInContext(String name, Object value) {
		getEclipseContext().set(name, value);
	}

	/**
	 * Removes the given name and any corresponding value from this context
	 */
	public static void removeFromContext(String name) {
		getEclipseContext().remove(name);
	}

	/**
	 * @return an instance of the specified class and inject it with the context.
	 */
	public static <T> T make(Class<T> clazz) {
		return ContextInjectionFactory.make(clazz, eclipseContext);
	}

	/**
	 * Injects a context into a domain object.
	 */
	public static void inject(Object object) {
		ContextInjectionFactory.inject(object, getEclipseContext());
	}
}
