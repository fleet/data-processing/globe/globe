package fr.ifremer.globe.gws.ui.tree;

import java.util.ArrayList;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.gws.server.service.GwsServiceGroup;
import fr.ifremer.globe.ui.tree.ITreeNode;
import fr.ifremer.globe.ui.tree.TreeNode;

/**
 * TreeNode representing a GwsServiceGroup
 */
public class GwsServiceGroupNode extends TreeNode<GwsServiceGroup> {

	public GwsServiceGroupNode(TreeViewer parent, GwsServiceGroup model) {
		super(parent, model);
	}

	public GwsServiceGroupNode(GwsServiceGroupNode parent, GwsServiceGroup model) {
		super(parent, model);
	}

	@Override
	public String getName() {
		return model.name();
	}

	@Override
	protected void createChildren() {
		if (children == null) {
			children = new ArrayList<ITreeNode>();
		}
		model.subGroups().forEach(subGroup -> children.add(new GwsServiceGroupNode(this, subGroup)));
		model.services().forEach(service -> children.add(new GwsServiceNode(this, service)));
	}

	@Override
	public boolean hasChildren() {
		return !model.services().isEmpty() || !model.subGroups().isEmpty();
	}

	@Override
	public Image getImage() {
		return null;
	}

}
