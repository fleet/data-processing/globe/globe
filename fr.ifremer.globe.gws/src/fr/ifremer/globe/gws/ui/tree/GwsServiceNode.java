package fr.ifremer.globe.gws.ui.tree;

import org.eclipse.swt.graphics.Image;

import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.ui.tree.ITreeNode;
import fr.ifremer.globe.ui.tree.TreeNode;

/**
 * TreeNode representing a GwsService
 */
public class GwsServiceNode extends TreeNode<GwsService> {

	public GwsServiceNode(ITreeNode parent, GwsService model) {
		super(parent, model);
	}

	@Override
	public String getName() {
		return model.name();
	}

	@Override
	protected void createChildren() {
	}

	@Override
	public boolean hasChildren() {
		return false;
	}

	@Override
	public Image getImage() {
		// TODO : provide an image
		return null; // par defaut : aucune image
		// Image img = ImageResources.getImage("/icons/16/tool.png", getClass());
		// return img;
	}
}
