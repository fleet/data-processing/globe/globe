package fr.ifremer.globe.gws.ui.wizard;

import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CSV_DECIMALPOINT;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CSV_DELIMITER;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CSV_DEPTHSIGN;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CSV_HEADERS_TYPES;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CSV_INDEXES;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CSV_MANDATORY_HEADERS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CSV_OPTIONAL_HEADERS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CSV_SKIPROWS;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections.primitives.IntList;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.file.IxyzFile;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueGetter;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.ui.databinding.observable.WritableString;

/**
 * This class aim at map
 * <ul>
 * <li>GEOBOX_COORDS argument to the Geobox displayed in Wizard page GenericGeoboxPage</li>
 * <li>GEOBOX_RESOLUTION argument to the Geobox displayed in Wizard page GenericGeoboxPage</li>
 * <li>PROJECTION argument to the projection's settings displayed in Wizard page ProjectionWizardPage
 * (ProjectionDialog)</li>
 * </ul>
 * Projection edited in ProjectionWizardPage is automatically transfered in the GenericGeoboxPage
 *
 */
public class CsvAdapterDelegate {

	protected static Logger logger = LoggerFactory.getLogger(CsvAdapterDelegate.class);

	/** Name of the CSV conf page */
	protected static final String CONF_PAGE_NAME = "CSV parsing parameters";

	/** Argument setter and getter */
	protected ArgumentValueGetter argumentValueGetter = new ArgumentValueGetter();
	protected ArgumentValueSetter argumentValueSetter = new ArgumentValueSetter();

	/** Considered process */
	protected GwsServiceConf process;
	/** Input files */
	protected List<File> inputFiles;

	/** Edited parameters */
	protected CsvParameters csvParameters;

	protected WritableString writableHeaders = new WritableString();

	/**
	 * Constructor
	 */
	public CsvAdapterDelegate(GwsServiceConf process, List<File> inputFiles) {
		this.process = process;
		this.inputFiles = inputFiles;
		csvParameters = new CsvParameters(process);
	}

	public IxyzFile getCsvParameters() {
		return csvParameters;
	}

	/**
	 * Class adapting parameters of IxyzFile to pyat arguments
	 */
	class CsvParameters implements IxyzFile {

		private Optional<FileInfoSettings> defaultSettings = Optional.empty();

		/** Current process */
		protected GwsServiceConf process;

		/**
		 * Constructor
		 */
		public CsvParameters(GwsServiceConf process) {
			this.process = process;
		}

		/** {@inheritDoc} */
		@Override
		public String getPath() {
			String result = !inputFiles.isEmpty() ? inputFiles.get(0).getPath() : null;

			// Try to get default settings for the file
			if (defaultSettings.isEmpty() && result != null) {
				defaultSettings = IFileService.grab().evaluateSettings(result);
			}
			return result;
		}

		/**
		 * @return the {@link #delimiter}
		 */
		@Override
		public char getDelimiter() {
			Character result = null;

			// Known file ? Get value from session
			var delimiter = getSessionProperty(FileInfoSettings.KEY_DELIMITER);
			if (!delimiter.isEmpty()) {
				result = delimiter.charAt(0);
			}

			// Default settings exits ?
			if (result == null) {
				result = defaultSettings.isPresent() ? defaultSettings.get().getDelimiter() : null;
			}

			// Default value from script ?
			if (result == null) {
				var delimiterArgument = process.getArgumentOfType(CSV_DELIMITER);
				if (delimiterArgument.isPresent()) {
					result = argumentValueGetter.getCharacter(delimiterArgument.get());
				} else {
					result = ';';
				}
			}
			return result.charValue();
		}

		/**
		 * @param delimiter the {@link #delimiter} to set
		 */
		@Override
		public void setDelimiter(char value) {
			var delimiterArgument = process.getArgumentOfType(CSV_DELIMITER);
			if (delimiterArgument.isPresent()) {
				argumentValueSetter.setCharacter(value, delimiterArgument.get());
			}
		}

		/**
		 * @return the {@link #decimalPoint}
		 */
		@Override
		public char getDecimalPoint() {
			Character result = null;
			// Known file ? Get value from session
			var languageTag = getSessionProperty(FileInfoSettings.KEY_LOCALE);
			if (!languageTag.isEmpty()) {
				result = Locale.FRANCE.equals(Locale.forLanguageTag(languageTag)) ? ',' : '.';
			}

			// Default settings exits ?
			if (result == null && defaultSettings.isPresent()) {
				result = Locale.FRANCE.equals(defaultSettings.get().getLocale()) ? ',' : '.';
			}

			// Default value from script ?
			if (result == null) {
				var decimalPointArgument = process.getArgumentOfType(CSV_DECIMALPOINT);
				result = decimalPointArgument.isPresent() ? argumentValueGetter.getCharacter(decimalPointArgument.get())
						: '.';
			}
			return result.charValue();
		}

		/**
		 * @param decimalPoint the {@link #decimalPoint} to set
		 */
		@Override
		public void setDecimalPointLabel(char value) {
			var decimalPointArgument = process.getArgumentOfType(CSV_DECIMALPOINT);
			if (decimalPointArgument.isPresent()) {
				argumentValueSetter.setCharacter(value, decimalPointArgument.get());
			}
		}

		/**
		 * @return the {@link #depthPositiveBelowSurface}
		 */
		@Override
		public boolean isDepthPositiveBelowSurface() {
			// Known file ? Get value from session
			Boolean result = null;
			double scaleFactor = NumberUtils.toDouble(getSessionProperty(FileInfoSettings.KEY_ELEVATION_SCALE_FACTOR));
			if (scaleFactor != 0d) {
				result = scaleFactor < 0d;
			}

			// Default settings exits ?
			if (result == null && defaultSettings.isPresent()) {
				scaleFactor = defaultSettings.get().getElevationScaleFactor();
				if (scaleFactor != 0d) {
					result = scaleFactor < 0d;
				}
			}

			// Default value from script ?
			if (result == null) {
				var signArgument = process.getArgumentOfType(CSV_DEPTHSIGN);
				result = Boolean.TRUE;
				if (signArgument.isPresent()) {
					result = argumentValueGetter.getDouble(signArgument.get()) < 0d;
				}
			}
			return result;
		}

		/**
		 * @param depthPositiveBelowSurface the {@link #depthPositiveBelowSurface} to set
		 */
		@Override
		public void setDepthPositiveBelowSurface(boolean value) {
			var signArgument = process.getArgumentOfType(CSV_DEPTHSIGN);
			if (signArgument.isPresent()) {
				argumentValueSetter.setDouble(value ? -1d : 1d, signArgument.get());
			}
		}

		/**
		 * @return the {@link #startingLine}
		 */
		@Override
		public int getStartingLine() {
			// Known file ? Get value from session
			Integer result = null;
			int skippedRows = NumberUtils.toInt(getSessionProperty(FileInfoSettings.KEY_ROW_COUNT_TO_SKIP), -1);
			if (skippedRows >= 0) {
				result = skippedRows;
			}

			// Default settings exits ?
			if (result == null && defaultSettings.isPresent()) {
				skippedRows = defaultSettings.get().getRowCountToSkip();
				if (skippedRows >= 0) {
					result = skippedRows;
				}
			}

			// Default value from script ?
			if (result == null) {
				var skipLinesArgument = process.getArgumentOfType(CSV_SKIPROWS);
				result = skipLinesArgument.isPresent() ? argumentValueGetter.getInt(skipLinesArgument.get()) : 0;
			}
			return result;
		}

		/**
		 * @param startingLine the {@link #startingLine} to set
		 */
		@Override
		public void setStartingLine(int value) {
			var skipLinesArgument = process.getArgumentOfType(CSV_SKIPROWS);
			if (skipLinesArgument.isPresent()) {
				argumentValueSetter.setInt(value, skipLinesArgument.get());
			}
		}

		/**
		 * @return the {@link #headerIndex}
		 */
		@Override
		public Map<String, Integer> getHeaderIndex() {
			Map<String, Integer> result = new LinkedHashMap<>();
			getMandatoryHeader().stream().forEach(header -> result.computeIfAbsent(header, this::getHeaderIndex));
			getOptionalHeader().stream().forEach(header -> result.computeIfAbsent(header, this::getHeaderIndex));
			return result;
		}

		/**
		 * Return the index of the header in the Csv file.
		 */
		private int getHeaderIndex(String header) {
			// Get column index from session for known file
			int result = getColumnIndexInSession(header);
			if (result == -1) {
				// Default settings exits ?
				result = getDefaultColumnIndex(header);
			}
			if (result == -1) {
				// From json cache file
				var indexesArgument = process.getArgumentOfType(CSV_INDEXES);
				if (indexesArgument.isPresent()) {
					var dictionary = argumentValueGetter.getDictionary(indexesArgument.get());
					if (dictionary.containsKey(header)) {
						result = NumberUtils.toInt(dictionary.get(header), -1);
					}
				}
			}

			// +1 because there is an "Dummy" column in CsvComposite
			return result >= 0 ? result + 1 : -1;
		}

		/**
		 * @param headerIndex the {@link #headerIndex} to set
		 */
		@Override
		public void setHeaderIndex(Map<String, Integer> headerIndex) {
			var indexesArgument = process.getArgumentOfType(CSV_INDEXES);
			if (indexesArgument.isPresent()) {
				var dictionary = new LinkedHashMap<String, String>();
				for (var entry : headerIndex.entrySet()) {
					dictionary.put(entry.getKey(), entry.getValue().toString());
				}
				argumentValueSetter.setDictionary(dictionary, indexesArgument.get());
			}
		}

		/** {@inheritDoc} */
		@Override
		public List<String> getMandatoryHeader() {
			var mheadersArgument = process.getArgumentOfType(CSV_MANDATORY_HEADERS);
			return mheadersArgument.isPresent() ? argumentValueGetter.getStrings(mheadersArgument.get()) : List.of();
		}

		@Override
		public List<String> getOptionalHeader() {
			var oheadersArgument = process.getArgumentOfType(CSV_OPTIONAL_HEADERS);
			return oheadersArgument.isPresent() ? argumentValueGetter.getStrings(oheadersArgument.get()) : List.of();
		}

		/** {@inheritDoc} */
		@Override
		public Class<?> getType(String headerName) {
			var typesArgument = process.getArgumentOfType(CSV_HEADERS_TYPES);
			if (typesArgument.isPresent()) {
				var currentTypes = argumentValueGetter.getDictionary(typesArgument.get());
				var type = currentTypes.get(headerName);
				if ("float".equalsIgnoreCase(type)) {
					return Double.class;
				}
				if ("int".equalsIgnoreCase(type)) {
					return Integer.class;
				}
			}
			return String.class;
		}

		/** {@inheritDoc} */
		@Override
		public void setType(String headerName, Class<?> type) {
			var typesArgument = process.getArgumentOfType(CSV_HEADERS_TYPES);
			if (typesArgument.isPresent()) {
				var currentTypes = argumentValueGetter.getDictionary(typesArgument.get());
				if (type == Double.class) {
					currentTypes.put(headerName, "float");
				} else if (type == Integer.class) {
					currentTypes.put(headerName, "int");
				} else {
					currentTypes.put(headerName, "str");
				}
				argumentValueSetter.setDictionary(currentTypes, typesArgument.get());
			}
		}

		private int getColumnIndexInSession(String header) {
			if (header.toLowerCase().contains("longitude")) {
				return NumberUtils.toInt(getSessionProperty(FileInfoSettings.KEY_COLUMN_LONGITUDE_INDEX), -1);
			} else if (header.toLowerCase().contains("latitude")) {
				return NumberUtils.toInt(getSessionProperty(FileInfoSettings.KEY_COLUMN_LATITUDE_INDEX), -1);
			} else if (header.toLowerCase().contains("elevation")) {
				return NumberUtils.toInt(getSessionProperty(FileInfoSettings.KEY_COLUMN_ELEVATION_INDEX), -1);
			} else if (header.toLowerCase().contains("depth")) {
				return NumberUtils.toInt(getSessionProperty(FileInfoSettings.KEY_COLUMN_ELEVATION_INDEX), -1);
			}
			return -1;
		}

		private int getDefaultColumnIndex(String header) {
			if (defaultSettings.isEmpty()) {
				return -1;
			}
			FileInfoSettings settings = defaultSettings.get();
			int result = NumberUtils.toInt(settings.get(header), -1);
			if (result == -1) {
				if (header.toLowerCase().contains("longitude")) {
					result = settings.getLongitudeIndex(-1);
				} else if (header.toLowerCase().contains("latitude")) {
					result = settings.getLatitudeIndex(-1);
				} else if (header.equalsIgnoreCase("elevation")) {
					result = settings.getElevationIndex(-1);
				} else if (header.toLowerCase().contains("depth")) {
					result = settings.getElevationIndex(-1);
				}
			}
			return result;
		}

		/** Read a property in session for the first input file */
		protected String getSessionProperty(String propertyName) {
			String result = "";
			// Known file ?
			var path = getPath();
			if (path != null) {
				Map<String, String> fileSettings = ISessionService.grab().getPropertiesContainer().get(path,
						FileInfoSettings.REALM_PROPERTIES_FILE_SETTINGS);
				var property = fileSettings.get(propertyName);
				result = property != null && !property.isEmpty() ? property : "";
			}
			return result;
		}

		/** {@inheritDoc} */
		@Override
		public boolean allowsMultipleValues() {
			return false; // Not supported
		}

		/** {@inheritDoc} */
		@Override
		public void setValueIndexes(IntList indexes) {
			// Not supported
		}

	}
}
