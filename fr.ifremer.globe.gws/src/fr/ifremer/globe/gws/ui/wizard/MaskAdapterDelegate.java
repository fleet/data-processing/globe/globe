package fr.ifremer.globe.gws.ui.wizard;

import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.MASK_ENABLE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.MASK_FILES;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.MASK_SIZE;

import java.io.File;

import fr.ifremer.globe.gws.server.service.GwsServiceArgument;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument.DecodedType;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueGetter;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableInt;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;

/**
 * This class aim to map
 * <ul>
 * <li>MASK_ENABLE argument to the check box displayed in Wizard page MaskPage</li>
 * <li>MASK_SIZE argument to the spinner displayed in Wizard page MaskPage</li>
 * <li>MASK_FILES argument to the list of files displayed in Wizard page MaskPage</li>
 * </ul>
 *
 */
public class MaskAdapterDelegate {

	/** Argument setter and getter */
	protected ArgumentValueGetter argumentValueGetter = new ArgumentValueGetter();
	protected ArgumentValueSetter argumentValueSetter = new ArgumentValueSetter();

	/** The boolean indicating the mask page is activated */
	protected WritableBoolean maskEnable = new WritableBoolean(false);
	/** The number of cell composing a mask */
	protected WritableInt maskSize = new WritableInt(3);
	/** Edited spatialesolution in the GenericGeoboxPage */
	protected WritableObjectList<File> maskFiles = new WritableObjectList<>();

	/**
	 * Prepare the attributes to display a mask wizard page
	 */
	public void bindMaskArgument(GwsServiceArgument maskArgument, DecodedType decodedType) {
		String argumentType = decodedType.getRefinedType().toLowerCase();
		if (MASK_ENABLE.equals(argumentType)) {
			bindEnable(maskArgument);
		} else if (MASK_SIZE.equals(argumentType)) {
			bindSize(maskArgument);
		} else if (MASK_FILES.equals(argumentType)) {
			bindFiles(maskArgument);
		}
	}

	/** Bind maskEnable value to service "mask#enable" argument */
	protected void bindEnable(GwsServiceArgument enableArgument) {
		// Init initial value
		maskEnable.set(argumentValueGetter.getBoolean(argumentValueGetter.getValue(enableArgument)));
		// Send value of maskEnable to the service argument when changed
		maskEnable.addChangeListener(event -> argumentValueSetter.setBoolean(maskEnable.getValue(), enableArgument));
	}

	/** Bind maskSize value to service "mask#size" argument */
	protected void bindSize(GwsServiceArgument sizeArgument) {
		// Init initial value
		maskSize.set(argumentValueGetter.getInt(argumentValueGetter.getValue(sizeArgument)));
		// Send value of maskEnable to the service argument when changed
		maskSize.addChangeListener(event -> argumentValueSetter.setInt(maskSize.getValue(), sizeArgument));
	}

	/** Bind maskFiles value to service "mask#files" argument */
	protected void bindFiles(GwsServiceArgument filesArgument) {
		// Init initial value
		maskFiles.addAll(argumentValueGetter.getFiles(filesArgument.getValue()));
		// Send value of maskFiles to the service argument when changed
		maskFiles.addChangeListener(event -> argumentValueSetter.setFiles(maskFiles, filesArgument));
	}

	/**
	 * @return the {@link #maskEnable}
	 */
	public WritableBoolean getMaskEnable() {
		return maskEnable;
	}

	/**
	 * @return the {@link #maskSize}
	 */
	public WritableInt getMaskSize() {
		return maskSize;
	}

	/**
	 * @return the {@link #maskFiles}
	 */
	public WritableObjectList<File> getMaskFiles() {
		return maskFiles;
	}

}
