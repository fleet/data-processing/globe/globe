package fr.ifremer.globe.gws.ui.wizard;

import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.GEOBOX_COORDS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.GEOBOX_RESOLUTION;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.PROJECTION;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.dtm.SounderDataDtmGeoboxComputer;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.file.IGeographicInfoSupplier.SpatialResolution;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.gws.server.process.function.FunctionFactory;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueGetter;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.function.MapOfDoubleConsumer;

/**
 * This class aim to map
 * <ul>
 * <li>GEOBOX_COORDS argument to the Geobox displayed in Wizard page GenericGeoboxPage</li>
 * <li>GEOBOX_RESOLUTION argument to the Geobox displayed in Wizard page GenericGeoboxPage</li>
 * <li>PROJECTION argument to the projection's settings displayed in Wizard page ProjectionWizardPage
 * (ProjectionDialog)</li>
 * </ul>
 * Projection edited in ProjectionWizardPage is automatically transfered in the GenericGeoboxPage
 *
 */
public class GeoboxAdapterDelegate {

	protected static Logger logger = LoggerFactory.getLogger(GeoboxAdapterDelegate.class);

	/** Cache of IInfoStore created from input files */
	protected Map<File, IFileInfo> inputFileInfos = new HashMap<>();

	/** Argument setter and getter */
	protected ArgumentValueGetter argumentValueGetter = new ArgumentValueGetter();
	protected ArgumentValueSetter argumentValueSetter = new ArgumentValueSetter();

	/** Considered process */
	protected GwsServiceConf process;

	/** Geoboxes of all input files */
	protected WritableList<GeoBox> geoboxes = new WritableList<>();
	/** Edited geobox in the GenericGeoboxPage */
	protected WritableObject<GeoBox> geoboxCoords = new WritableObject<>(null);
	/** Edited spatialesolution in the GenericGeoboxPage */
	protected Optional<WritableDouble> spatialResolution = Optional.empty();

	/** Projection settings edited in ProjectionWizardPage */
	protected WritableObject<ProjectionSettings> projectionSettings = new WritableObject<>(null);
	/** Projection built with the Projection settings edited in ProjectionWizardPage */
	protected WritableObject<Projection> projection = new WritableObject<>(null);

	/**
	 * Constructor
	 */
	public GeoboxAdapterDelegate(GwsServiceConf process, Map<File, IFileInfo> inputFileInfos) {
		this.process = process;
		this.inputFileInfos = inputFileInfos;

		// Update projection in the Geobox Wizard page when the one edited in the Projection Wizard page
		projectionSettings.addChangeListener(event -> projection.set(new Projection(projectionSettings.get())));
	}

	/**
	 * Prepare model for editing the geobox, projection and spatial resolution
	 *
	 * @return the the python functions to call for computing the best spatial resolution and geobox of the input files.
	 */
	public Pair<Optional<Consumer<MapOfDoubleConsumer>>, Optional<Consumer<MapOfDoubleConsumer>>> prepareGeoBoxModel(
			GwsServiceArgument argGeoboxCoords) {

		var resolutionEvaluator = prepareSpatialResolutionModel(GEOBOX_RESOLUTION);

		Consumer<MapOfDoubleConsumer> geoboxEvaluator = null;
		GwsServiceArgument coordsArg = process.getArgumentOfType(GEOBOX_COORDS).orElse(null);
		if (coordsArg != null && coordsArg.getFunction() != null) {
			geoboxEvaluator = FunctionFactory.forMapOfDouble(Optional.of(process), coordsArg.getFunction());
		}

		// first set or restore the argument
		if (geoboxCoords.isNotNull() && !geoboxCoords.get().isEmpty())
			argumentValueSetter.setGeobox(geoboxCoords.get(), argGeoboxCoords);
		else {
			GeoBox geobox = argumentValueGetter.getGeobox(argGeoboxCoords.getValue());
			if (geobox != null) {
				var proj = projection.isNotNull() ? projection.doGetValue().getProjectionSettings()
						: StandardProjection.LONGLAT;
				geoboxCoords.doSetValue(
						new GeoBox(geobox.getTop(), geobox.getBottom(), geobox.getLeft(), geobox.getRight(), proj));
			}
		}

		IChangeListener geoboxUpdater = event -> argumentValueSetter.setGeobox(geoboxCoords.get(), argGeoboxCoords);
		geoboxCoords.addChangeListener(geoboxUpdater);
		projection.addChangeListener(geoboxUpdater);

		return new Pair<>(resolutionEvaluator, Optional.ofNullable(geoboxEvaluator));
	}

	/**
	 * Prepare model for editing the projection and spatial resolution
	 * 
	 * @param spatialResolutionType : type of argument pointing to the spatial resolution argument
	 *
	 * @return the python function to call to compute the best spatial resolution for the input files.
	 */
	public Optional<Consumer<MapOfDoubleConsumer>> prepareSpatialResolutionModel(String spatialResolutionType) {

		Consumer<MapOfDoubleConsumer> resolutionEvaluator = null;
		GwsServiceArgument resolutionArg = process.getArgumentOfType(spatialResolutionType).orElse(null);
		if (resolutionArg != null) {
			var writable = new WritableDouble(argumentValueGetter.getDouble(resolutionArg));
			spatialResolution = Optional.of(writable);
			// Transfert value of spatial resolution to the pyat argument
			writable.addValueChangeListener(event -> {
				// Only valid double is acceptable by pyat
				if (writable.isValid() && writable.get() != 0d)
					resolutionArg.setValue(String.valueOf(writable.get()));
				else
					resolutionArg.setValue(null);
			});
			if (resolutionArg.getFunction() != null) {
				resolutionEvaluator = FunctionFactory.forMapOfDouble(Optional.of(process), resolutionArg.getFunction());
			}
		}

		GwsServiceArgument projectionArg = process.getArgumentOfType(PROJECTION).orElse(null);
		if (projectionArg != null && projectionArg.getValue() != null) {
			// Get projection from argument
			projection.set(Projection.createFromProj4String(projectionArg.getValue()));
		} else if (!inputFileInfos.isEmpty()) {
			// Read projection and spatial resolution from the first file
			IFileInfo fileInfo = inputFileInfos.values().iterator().next();
			IFileService fileService = IFileService.grab();
			projection.set(fileService.getProjection(fileInfo).orElse(new Projection(StandardProjection.LONGLAT)));
			spatialResolution.ifPresent(writable -> {
				var spatialResolutionOpt = fileService.getSpatialResolution(fileInfo);
				if (spatialResolutionOpt.isPresent())
					writable.set(spatialResolutionOpt.map(SpatialResolution::getxResolution).get());
			});
		}

		return Optional.ofNullable(resolutionEvaluator);
	}

	/**
	 * Add a wizard page to select the projection
	 */
	public WritableObject<ProjectionSettings> prepareProjectionPage(GwsServiceArgument projectionArg) {
		if (projectionArg.getDefaultValue() != null) {
			ProjectionSettings settings = projection.isNotNull() ? projection.get().getProjectionSettings()
					: StandardProjection.MERCATOR;
			String proj4String = argumentValueGetter.getValue(projectionArg);
			if (proj4String == null || proj4String.isEmpty()) {
				argumentValueSetter.setProjection(settings, projectionArg);
			} else {
				settings = new ProjectionSettings(proj4String);
			}
			projectionSettings.set(settings);
		} else {
			// Specific case !
			// When projection argument has no default value, user must define and check the projection settings
			argumentValueSetter.setProjection(null, projectionArg);
			projectionSettings.setValue(null);
		}
		return projectionSettings;
	}

	/**
	 * Called by the WizardApdapter when input file changed. In this case, we have to manage a new set of Geoboxes
	 */
	public void updateGeoboxes() {
		geoboxes.clear();
		for (IFileInfo fileInfo : inputFileInfos.values()) {
			GeoBox geoBox = null;
			if (fileInfo instanceof ISounderNcInfo) {
				SounderDataDtmGeoboxComputer geoboxComputer = new SounderDataDtmGeoboxComputer(
						List.of((ISounderNcInfo) fileInfo));
				try {
					geoBox = geoboxComputer.computeValidGeobox();
				} catch (GIOException e) {
					logger.error(e.getMessage(), e);
					geoBox = fileInfo.getGeoBox();
				}
			} else {
				geoBox = fileInfo.getGeoBox();
			}
			if (geoBox != null)
				geoboxes.add(geoBox);
		}
		// Compute an initial value of the geobox from union of input geoboxes
		if (!geoboxes.isEmpty() && (geoboxCoords.isNull() || geoboxCoords.get().isEmpty()))
			geoboxCoords.setValue(GeoBox.englobe(geoboxes));
	}

	/**
	 * @return the {@link #geoboxes}
	 */
	public WritableList<GeoBox> getGeoboxes() {
		return geoboxes;
	}

	/**
	 * @return the {@link #geoboxCoords}
	 */
	public WritableObject<GeoBox> getGeoboxCoords() {
		return geoboxCoords;
	}

	/**
	 * @return the {@link #spatialResolution}
	 */
	public Optional<WritableDouble> getSpatialResolution() {
		return spatialResolution;
	}

	/**
	 * @return the {@link #projectionSettings}
	 */
	public WritableObject<ProjectionSettings> getProjectionSettings() {
		return projectionSettings;
	}

	/**
	 * @return the {@link #projection}
	 */
	public WritableObject<Projection> getProjection() {
		return projection;
	}
}
