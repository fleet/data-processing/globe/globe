package fr.ifremer.globe.gws.ui.wizard;

import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.BOOL;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CDI_FILTER;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CDI_MODIFY;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CDI_OPERATOR;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CDI_SET;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CDI_SORT;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CHECK_LIST;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.CSV_DELIMITER;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.DEPTH_CORRECTION_TIDE_TYPE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.DOUBLE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.DURATION;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.GEOBOX_COORDS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.GEOBOX_RESOLUTION;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.INSTANT;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.INTEGER;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.IN_FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.IN_FOLDER;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.LAYERS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.MASK_ENABLE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.MASK_FILES;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.MASK_POLYGONS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.MASK_POLYGONS_REVERSE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.MASK_SIZE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.OUT_FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.OVERWRITE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.PROJECTION;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.SENSORS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.SINGLE_FOLDER;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.SPATIAL_RESOLUTION;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.STRING;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.TIME_INTERVAL_CUT_FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.WC_FILTERS;

import java.io.File;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.databinding.observable.set.WritableSet;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.dtm.filter.FilteringOperator;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.ICdiService;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.gws.server.process.function.FunctionFactory;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument.DecodedType;
import fr.ifremer.globe.gws.server.service.GwsServiceArgumentPage;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueGetter;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableDuration;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableInstant;
import fr.ifremer.globe.ui.databinding.observable.WritableInt;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.service.wizard.IWizardBuilder;
import fr.ifremer.globe.ui.service.wizard.IWizardService;
import fr.ifremer.globe.utils.function.MapOfDoubleConsumer;

/**
 * This class aim to adapt service arguments to Wizard page.
 */
public class WizardAdapter {

	private static Logger logger = LoggerFactory.getLogger(WizardAdapter.class);

	/** Services */
	private final IWizardService wizardService = IWizardService.grab();
	private final IFileService fileService = IFileService.grab();

	private final ArgumentValueGetter argumentValueGetter = new ArgumentValueGetter();
	private final ArgumentValueSetter argumentValueSetter = new ArgumentValueSetter();

	/** Configuration (list of arguments) of the service to edit */
	private final GwsServiceConf serviceConf;
	private final Optional<String> helpPath;
	private final Shell shell;

	/** Main projection argument */
	private GwsServiceArgument argProjection = null;
	/** Cdi filter page need two arguments */
	private GwsServiceArgument argCdiFilterOperator = null;
	private GwsServiceArgument argCdiFilterFilter = null;

	/** Delegate class for file arguments */
	private final FileAdapterDelegate fileAdapterDelegate;
	/** Delegate class for adapting Geobox arguments */
	private final GeoboxAdapterDelegate geoboxAdapterDelegate;
	/** Delegate class for adapting CSV arguments */
	private final CsvAdapterDelegate csvAdapterDelegate;
	/** Delegate class for adapting masking arguments */
	private final MaskAdapterDelegate maskAdapterDelegate;
	/** Delegate class for adapting time interval arguments */
	private Optional<TimeIntervalAdapterDelegate> timeIntervalAdapterDelegate = Optional.empty();
	/** Delegate class for adapting depth correction arguments */
	private Optional<DepthCorrectionAdapterDelegate> depthCorrectionAdapterDelegate = Optional.empty();

	/** True when summary page is required */
	private boolean withSummary = true;

	/**
	 * Constructor
	 */
	public WizardAdapter(GwsServiceConf serviceConf, Optional<String> helpPath, Shell shell) {
		this.serviceConf = serviceConf;
		this.helpPath = helpPath;
		this.shell = shell;

		fileAdapterDelegate = new FileAdapterDelegate(serviceConf, fileService);
		geoboxAdapterDelegate = new GeoboxAdapterDelegate(serviceConf, fileAdapterDelegate.getInputFileInfos());
		csvAdapterDelegate = new CsvAdapterDelegate(serviceConf, getInputFiles());
		maskAdapterDelegate = new MaskAdapterDelegate();

		// Update geoboxes list when input files change
		getInputFiles().addChangeListener(event -> {
			fileAdapterDelegate.updateInputFileInfos();
			geoboxAdapterDelegate.updateGeoboxes();
			timeIntervalAdapterDelegate.ifPresent(TimeIntervalAdapterDelegate::onInputFilesChanged);
		});
		fileAdapterDelegate.makeBindings();
	}

	/**
	 * Open the selected process in a wizard
	 *
	 * @return false when canceled
	 */
	public boolean openWizard() {
		return openWizard(false) == Window.OK;
	}

	/**
	 * Open the selected process in a wizard
	 * 
	 * @param firstPageIsSummary true to open the wizard with only one page : summary
	 *
	 * @return false when cancelled
	 */
	public int openWizard(boolean firstPageIsSummary) {
		IWizardBuilder wizardBuilder = wizardService.newWizardBuilder();
		wizardBuilder.setWindowTitle(serviceConf.name());

		boolean hasGeobox = serviceConf.arguments().stream()//
				.filter(serviceArgument -> serviceArgument != null && serviceArgument.getType() != null)//
				.anyMatch(serviceArgument -> serviceArgument.getType().contains(GwsServiceArgument.GEOBOX_COORDS));

		Wizard wizard = buildWizard(firstPageIsSummary);
		GwsWizardDialog wizardDialog = new GwsWizardDialog(shell, wizard, helpPath);
		wizardDialog.setModal(!hasGeobox);
		wizardDialog.setFirstPageIsSummary(firstPageIsSummary);
		wizardDialog.setMinimumPageSize(620, 310);
		return wizardDialog.open();
	}

	/**
	 * Build and configure the instance of Wizard to edit all arguments
	 */
	public Wizard buildWizard(boolean firstPageIsSummary) {
		IWizardBuilder wizardBuilder = wizardService.newWizardBuilder();
		wizardBuilder.setWindowTitle(serviceConf.name());

		serviceConf.arguments().stream()//
				.filter(serviceArgument -> serviceArgument != null && serviceArgument.getType() != null)//
				.forEach(serviceArgument -> addWizardPage(wizardBuilder, serviceArgument));

		if (isWithSummary()) {
			wizardBuilder.addSummaryPage();
			if (firstPageIsSummary)
				wizardBuilder.firstPageIsSummary();
		}
		Wizard wizard = wizardBuilder.build();
		wizard.setHelpAvailable(helpPath.isPresent());

		return wizard;
	}

	/**
	 * Manage an argument to generate a new Wizard page
	 */
	private void addWizardPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument) {
		DecodedType[] decodeTypes = serviceArgument.decodeTypes();
		Arrays.stream(decodeTypes).forEach(decodedType -> addWizardPage(wizardBuilder, serviceArgument, decodedType));
	}

	/**
	 * Manage an argument to generate a new Wizard page
	 */
	private void addWizardPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {

		String refinedType = decodedType.getRefinedType().toLowerCase();
		switch (refinedType) {
		case IN_FILE, IN_FOLDER, FILE, SINGLE_FOLDER, MASK_POLYGONS, MASK_POLYGONS_REVERSE:
			fileAdapterDelegate.addWizardPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case OVERWRITE, OUT_FILE:
			if (timeIntervalAdapterDelegate.isPresent()) {
				// Specific way of processing output files when TimeInterval is required
				addTimeIntervalOutputPage(wizardBuilder, serviceArgument, decodedType);
			} else {
				fileAdapterDelegate.addWizardPage(wizardBuilder, serviceArgument, decodedType);
			}
			break;

		case CDI_SORT:
			addSortFilesByCdiPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case GEOBOX_COORDS:
			addGeoBoxWizardPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case GEOBOX_RESOLUTION:
			// argument managed in geobox wizard page
			break;

		case PROJECTION:
			addProjectionPage(wizardBuilder, serviceArgument);
			break;

		case SPATIAL_RESOLUTION:
			addSpatialResolutionWizardPage(wizardBuilder, serviceArgument);
			break;

		case CSV_DELIMITER:
			addCsvWizardPage(wizardBuilder, serviceArgument);
			break;

		case CHECK_LIST:
			addCheckListPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case BOOL:
			addBoolToGenericPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case DOUBLE:
			addFloatToGenericPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case INTEGER:
			addIntToGenericPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case STRING:
			addStringToGenericPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case DURATION:
			addDurationToGenericPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case INSTANT:
			addInstantToGenericPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case CDI_SET:
			addSetCDIPage(wizardBuilder, serviceArgument, decodedType);
			break;
		case CDI_MODIFY:
			addModifyCDIPage(wizardBuilder, serviceArgument);
			break;

		case LAYERS:
			addLayersPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case SENSORS:
			addSensorsPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case CDI_OPERATOR:
			argCdiFilterOperator = serviceArgument;
			if (argCdiFilterFilter != null) {
				addMultiFilterWizardPage(wizardBuilder);
			}
			break;
		case CDI_FILTER:
			argCdiFilterFilter = serviceArgument;
			if (argCdiFilterOperator != null) {
				addMultiFilterWizardPage(wizardBuilder);
			}
			break;

		case MASK_ENABLE, MASK_SIZE, MASK_FILES:
			addMaskPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case WC_FILTERS:
			addWcFiltersPage(wizardBuilder, serviceArgument);
			break;

		default:
			if (refinedType.startsWith(TIME_INTERVAL_CUT_FILE.split("#")[0]))
				addTimeIntervalPage(wizardBuilder, serviceArgument, decodedType);
			else if (refinedType.startsWith(DEPTH_CORRECTION_TIDE_TYPE.split("#")[0]))
				addDepthCorrectionPage(wizardBuilder, serviceArgument, decodedType);
			else
				logger.warn("Unmanaged argument {}", serviceArgument.getName());
			break;
		}
	}

	/**
	 * Add a wizard page to select some files. Files can be sorted by CDI
	 */
	private void addSortFilesByCdiPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		if (getInputFiles().isEmpty()) {
			getInputFiles().addAll(argumentValueGetter.getFiles(serviceArgument.getValue()));
			getInputFiles().addChangeListener(event -> argumentValueSetter.setFiles(getInputFiles(), serviceArgument));
		}

		wizardBuilder.addSortFilesByCdiPage(serviceArgument.getDecoredLabel(), getInputFiles(),
				makeEnabledSupplier(decodedType));
	}

	/**
	 * Add a wizard page to set CDIs
	 */
	private void addSetCDIPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		String pageTitle = serviceArgument.getDecoredLabel();

		if (!serviceArgument.requireListAsValue()) {
			// Only one file...
			WritableObjectList<File> theFirstFile = new WritableObjectList<>();
			getOutputFiles().addChangeListener(event -> {
				if (!getOutputFiles().isEmpty()) {
					theFirstFile.clear();
					theFirstFile.add(getOutputFiles().get(0));
				}
			});
			WritableList<String> cdis = new WritableList<>();
			cdis.addChangeListener(event -> argumentValueSetter.setStrings(cdis, serviceArgument));
			String cdiValue = argumentValueGetter.getValue(serviceArgument);
			wizardBuilder.addSetCDIPage(pageTitle, theFirstFile, cdis, cdiValue, makeEnabledSupplier(decodedType));
		} else {
			// Map input files to a CDI. CDI is extracted from argument (default or restored value)
			var cdiOfFiles = new LinkedHashMap<String, String>();
			getInputFiles().forEach(file -> cdiOfFiles.put(file.getName(), ""));
			Map<String, String> defaultCdis = argumentValueGetter.getDictionary(serviceArgument);
			for (var cdiEntry : defaultCdis.entrySet()) {
				cdiOfFiles.replace(cdiEntry.getKey(), cdiEntry.getValue());
			}

			WritableList<String> cdis = new WritableList<>();
			cdis.addAll(cdiOfFiles.values());
			cdis.addChangeListener(event -> {
				var dictionary = new LinkedHashMap<String, String>();
				for (int i = 0; i < cdis.size(); i++) {
					String cdi = cdis.get(i);
					if (cdi != null && !cdi.isEmpty()) {
						dictionary.put(getInputFiles().get(i).getName(), cdis.get(i));
					}
				}
				argumentValueSetter.setDictionary(dictionary, serviceArgument);
			});
			wizardBuilder.addSetCDIPage(pageTitle, getInputFiles(), cdis, "", makeEnabledSupplier(decodedType));
		}
	}

	/**
	 * Add a wizard page to modify CDIs
	 */
	private void addModifyCDIPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument) {

		Map<String, String> modifiedCdis = argumentValueGetter
				.getModifiedCdiValue(argumentValueGetter.getValue(serviceArgument));

		ICdiService cdiService = ICdiService.grab();
		var allCdis = fileAdapterDelegate.getInputFileInfos().values()//
				.stream()//
				.flatMap(fileInfo -> cdiService.getCdis(fileInfo).stream()) //
				.collect(Collectors.toSet());
		WritableList<String> originalCdis = new WritableList<>(new ArrayList<>(allCdis), String.class);

		WritableList<String> cdis = new WritableList<>();
		cdis.addAll(originalCdis.stream().map(cdi -> modifiedCdis.getOrDefault(cdi, cdi)).toList());

		originalCdis.addChangeListener(
				event -> argumentValueSetter.setModifiedCdiValue(originalCdis, cdis, serviceArgument));
		cdis.addChangeListener(event -> argumentValueSetter.setModifiedCdiValue(originalCdis, cdis, serviceArgument));

		wizardBuilder.addModifyCDIPage(serviceArgument.getDecoredLabel(), getInputFiles(), originalCdis, cdis);
	}

	/**
	 * Add a wizard page to edit a CDI
	 */
	private void addMultiFilterWizardPage(IWizardBuilder wizardBuilder) {
		WritableObject<FilteringOperator> operator = new WritableObject<>(FilteringOperator.AND);
		try {
			operator.set(FilteringOperator.valueOf(argumentValueGetter.getValue(argCdiFilterOperator)));
		} catch (IllegalArgumentException e) {
			// Argument is not suitable
			argumentValueSetter.setString(FilteringOperator.AND.toString(), argCdiFilterOperator);
		}
		operator.addChangeListener(
				event -> argumentValueSetter.setString(operator.get().toString(), argCdiFilterOperator));

		WritableList<String> filters = new WritableObjectList<>(
				argumentValueGetter.getStrings(argCdiFilterFilter.getValue()));
		filters.addChangeListener(event -> argumentValueSetter.setStrings(filters, argCdiFilterFilter));

		String pageTitle = argCdiFilterFilter.getDecoredLabel();
		wizardBuilder.addMultiFilterWizardPage(pageTitle, getInputFiles(), filters, operator);
	}

	/**
	 * Add a wizard page to select the GeoBox and the spatial resolution
	 */
	private void addGeoBoxWizardPage(IWizardBuilder wizardBuilder, GwsServiceArgument argGeoboxCoords,
			DecodedType decodedType) {
		var evaluators = geoboxAdapterDelegate.prepareGeoBoxModel(argGeoboxCoords);
		wizardBuilder.addGeoBoxWizardPage(//
				argGeoboxCoords.getDecoredLabel(), //
				geoboxAdapterDelegate.getGeoboxes(), //
				geoboxAdapterDelegate.getGeoboxCoords(), //
				geoboxAdapterDelegate.getProjection(), //
				geoboxAdapterDelegate.getSpatialResolution(), //
				evaluators.getFirst(), evaluators.getSecond(), //
				makeEnabledSupplier(decodedType));
	}

	/**
	 * Add a wizard page to select the GeoBox and the spatial resolution
	 */
	private void addSpatialResolutionWizardPage(IWizardBuilder wizardBuilder, GwsServiceArgument argGeoboxCoords) {
		Optional<Consumer<MapOfDoubleConsumer>> resolutionEvaluator = geoboxAdapterDelegate
				.prepareSpatialResolutionModel(SPATIAL_RESOLUTION);
		wizardBuilder.addSpatialResolutionWizardPage(//
				argGeoboxCoords.getDecoredLabel(), //
				geoboxAdapterDelegate.getProjection(), //
				geoboxAdapterDelegate.getSpatialResolution(), //
				resolutionEvaluator);
	}

	/**
	 * Add a wizard page to select the CSV parsing parameters
	 */
	private void addCsvWizardPage(IWizardBuilder wizardBuilder, GwsServiceArgument delimiterArgument) {
		wizardBuilder.addAsciiDelimiterPage(delimiterArgument.getHelp(), csvAdapterDelegate.getCsvParameters());
	}

	/**
	 * Add a wizard page to select the projection
	 */
	private void addProjectionPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument) {
		WritableObject<ProjectionSettings> projectionSettings = null;
		if (argProjection == null) {
			// The first projection page is linked to the GeoBox one
			argProjection = serviceArgument;
			projectionSettings = geoboxAdapterDelegate.prepareProjectionPage(serviceArgument);
		} else {
			// Secondary projection page
			projectionSettings = new WritableObject<>(null);
			String proj4String = argumentValueGetter.getValue(serviceArgument);
			if (proj4String != null && !proj4String.isEmpty()) {
				projectionSettings.set(new ProjectionSettings(proj4String));
			}
		}

		var finalProjectionSettings = projectionSettings;
		wizardBuilder.addProjectionPage(//
				serviceArgument.getDecoredLabel(), //
				finalProjectionSettings, // Settings to edit
				geoboxAdapterDelegate.getGeoboxCoords()// Geobox to guess some settings
		);
		projectionSettings.addChangeListener(
				event -> argumentValueSetter.setProjection(finalProjectionSettings.get(), serviceArgument));

	}

	/**
	 * Add a generic wizard page to edit a boolean
	 */
	private void addCheckListPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		// LinkedHashSet To keep the order of the list
		var choicesModel = new WritableList<>(List.of(serviceArgument.getChoices()), String.class);
		var values = new WritableList<>(argumentValueGetter.getStrings(argumentValueGetter.getValue(serviceArgument)),
				String.class);
		values.addChangeListener(event -> argumentValueSetter.setStrings(values, serviceArgument));

		wizardBuilder.addGenericCheckListPage(serviceArgument.getHelp(), "Double-click on selection to uncheck",
				choicesModel, values, makeEnabledSupplier(decodedType));
	}

	/**
	 * Add a generic wizard page to select layers
	 */
	private void addLayersPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		WritableSet<String> selectedLayers = new WritableSet<>(
				argumentValueGetter.getStrings(serviceArgument.getValue()), String.class);
		selectedLayers.addChangeListener(event -> argumentValueSetter.setStrings(selectedLayers, serviceArgument));
		wizardBuilder.addLayersPage("Layers to process", getInputFiles(), selectedLayers,
				makeEnabledSupplier(decodedType));
	}

	/**
	 * Add a generic wizard page to select layers
	 */
	private void addMaskPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		maskAdapterDelegate.bindMaskArgument(serviceArgument, decodedType);
		if (MASK_ENABLE.equalsIgnoreCase(decodedType.getRefinedType())) {
			wizardBuilder.addMaskWizardPage(//
					serviceArgument.getHelp(), //
					maskAdapterDelegate.getMaskEnable(), //
					maskAdapterDelegate.getMaskSize(), //
					maskAdapterDelegate.getMaskFiles() //
			);
		}
	}

	/**
	 * Add a wizard page to select a time interval or a cut file.
	 */
	private void addTimeIntervalPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		if (timeIntervalAdapterDelegate.isEmpty())
			timeIntervalAdapterDelegate = Optional
					.of(new TimeIntervalAdapterDelegate(serviceConf, fileAdapterDelegate.getInputFileInfos()));

		timeIntervalAdapterDelegate.ifPresent(delegate -> {
			delegate.bindArgument(serviceArgument, decodedType);
			if (decodedType.getRefinedType().equalsIgnoreCase(TIME_INTERVAL_CUT_FILE)) {
				wizardBuilder.addTimeIntervalPage(//
						serviceArgument.getHelp(), //
						delegate.referenceTimeInterval, //
						delegate.isMergeSimple, //
						delegate.isCutFile, delegate.cutFile, //
						delegate.isManually, delegate.startDate, delegate.endDate, //
						delegate.isGeoMaskFile, delegate.geoMaskFile, delegate.reverseGeoMask);
			}
		});
	}

	/**
	 * Add a wizard page to edit arguments of depth correction process.
	 */
	private void addDepthCorrectionPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		if (depthCorrectionAdapterDelegate.isEmpty())
			depthCorrectionAdapterDelegate = Optional
					.of(new DepthCorrectionAdapterDelegate(fileAdapterDelegate.getInputFiles()));

		depthCorrectionAdapterDelegate.ifPresent(delegate -> {
			delegate.bindArgument(serviceArgument, decodedType);
			if (decodedType.getRefinedType().equalsIgnoreCase(DEPTH_CORRECTION_TIDE_TYPE)) {
				wizardBuilder.addDepthCorrectionPage(//
						serviceArgument.getHelp(), delegate.getModel());
			}
		});
	}

	/**
	 * Add a wizard page to select some output files
	 */
	private void addTimeIntervalOutputPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		timeIntervalAdapterDelegate.ifPresent(delegate -> {
			delegate.bindArgument(serviceArgument, decodedType);
			if (decodedType.getRefinedType().equalsIgnoreCase(OUT_FILE)) {
				var outputFiles = delegate.outputFiles;
				outputFiles.addChangeListener(event -> argumentValueSetter.setFiles(outputFiles, serviceArgument));

				// File Extensions
				List<String> fileExtensions = AdapterUtils.getFileExtensions(decodedType);

				// Extend Techsas file extensions with .nc
				if (decodedType.getContentTypes().contains(ContentType.TECHSAS_NETCDF)) {
					fileExtensions = fileExtensions.stream() //
							.map(extension -> extension.endsWith(".nc") ? extension : extension + ".nc") //
							.toList();
				}

				wizardBuilder.addSelectOutputFilesPage(serviceArgument.getDecoredLabel(), //
						delegate.profileFiles, //
						new WritableObjectList<>(fileExtensions), //
						outputFiles, //
						delegate.loadFilesAfter, //
						delegate.whereToloadFiles, //
						delegate.overwriteExistingFiles);
			}
		});
	}

	/**
	 * Add a generic wizard page to select sensors
	 */
	private void addSensorsPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		WritableSet<String> sensorFilters = new WritableSet<>(
				Arrays.asList(decodedType.getParameters().getOrDefault("filters", "").split(";")), String.class);

		WritableObjectList<File> inputs = new WritableObjectList<>();
		if (getInputFolder().get() != null) {
			inputs.add(getInputFolder().get());
		}

		getInputFolder().addChangeListener(event -> {
			inputs.clear();
			if (getInputFolder().get() != null) {
				inputs.add(getInputFolder().get());
			}
		});
		if (serviceArgument.requireListAsValue()) {
			WritableList<String> selectedSensors = new WritableList<>(
					argumentValueGetter.getStrings(serviceArgument.getValue()), String.class);
			selectedSensors
					.addChangeListener(event -> argumentValueSetter.setStrings(selectedSensors, serviceArgument));

			wizardBuilder.addSensorsCheckListPage(serviceArgument.getHelp(), "Double-click on selection to uncheck",
					inputs, selectedSensors, sensorFilters, makeEnabledSupplier(decodedType));
		} else {
			WritableString selectedSensor = new WritableString(argumentValueGetter.getValue(serviceArgument));
			selectedSensor
					.addChangeListener(event -> argumentValueSetter.setString(selectedSensor.get(), serviceArgument));

			Optional<GwsServiceArgumentPage> page = serviceConf.getPageFor(serviceArgument);
			if (page.isEmpty() || page.get().visible().booleanValue())
				wizardBuilder.addSensorToGenericPage(AdapterUtils.inferPageTitle(serviceConf, page),
						AdapterUtils.inferPageHelp(page), serviceArgument.getHelp(), inputs, selectedSensor,
						sensorFilters, makeEnabledSupplier(decodedType));
		}

	}

	/**
	 * Add a generic wizard page to setup wc filters
	 */
	private void addWcFiltersPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument) {
		WritableString filters = new WritableString(argumentValueGetter.getValue(serviceArgument));
		filters.addChangeListener(event -> argumentValueSetter.setString(filters.get(), serviceArgument));
		wizardBuilder.addWcFiltersPage("WC Filters", getInputFiles(), filters);
	}

	/**
	 * Add a generic wizard page to edit a boolean
	 */
	private void addBoolToGenericPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		WritableBoolean value = new WritableBoolean(
				argumentValueGetter.getBoolean(argumentValueGetter.getValue(serviceArgument)));
		value.addChangeListener(event -> argumentValueSetter.setBoolean(value.getValue(), serviceArgument));
		Optional<GwsServiceArgumentPage> page = serviceConf.getPageFor(serviceArgument);
		if (page.isEmpty() || page.get().visible().booleanValue())
			wizardBuilder.addToGenericPage(AdapterUtils.inferPageTitle(serviceConf, page),
					AdapterUtils.inferPageHelp(page), serviceArgument.getHelp(), value, //
					null, // * no Configuration file defined with this kind of evaluator. always null
					makeEnabledSupplier(decodedType));
	}

	/**
	 * Add a generic wizard page to edit a float
	 */
	private void addFloatToGenericPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		WritableDouble value = new WritableDouble(
				argumentValueGetter.getDouble(argumentValueGetter.getValue(serviceArgument)));
		value.addChangeListener(event -> argumentValueSetter.setDouble(value.getValue(), serviceArgument));

		// create an evaluator if a function is present
		Supplier<Double> evaluator = null;
		if (serviceArgument.getFunction() != null) {
			evaluator = () -> {
				AtomicReference<Double> evaluatedValue = new AtomicReference<>();
				Consumer<MapOfDoubleConsumer> doubleFunction = FunctionFactory.forMapOfDouble(Optional.of(serviceConf),
						serviceArgument.getFunction());
				// Launch function and retrieve the double for the current argument
				doubleFunction.accept(result -> evaluatedValue.set(result.get(serviceArgument.getName())));
				return evaluatedValue.get();
			};
		}

		Optional<GwsServiceArgumentPage> page = serviceConf.getPageFor(serviceArgument);
		if (page.isEmpty() || page.get().visible().booleanValue())
			wizardBuilder.addToGenericPage(AdapterUtils.inferPageTitle(serviceConf, page),
					AdapterUtils.inferPageHelp(page), serviceArgument.getHelp(), value, evaluator,
					decodedType.getRange(), makeEnabledSupplier(decodedType));
	}

	/**
	 * Add a generic wizard page to edit an int
	 */
	private void addIntToGenericPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		WritableInt value = new WritableInt(argumentValueGetter.getInt(serviceArgument));
		value.addChangeListener(event -> argumentValueSetter.setInt(value.getValue(), serviceArgument));

		int[] choices = serviceArgument.getIntChoices();
		Optional<GwsServiceArgumentPage> page = serviceConf.getPageFor(serviceArgument);
		if (page.isEmpty() || page.get().visible().booleanValue()) {
			if (choices == null) {
				wizardBuilder.addToGenericPage(AdapterUtils.inferPageTitle(serviceConf, page),
						AdapterUtils.inferPageHelp(page), serviceArgument.getHelp(), value, //
						null, // no Configuration file defined with this kind of evaluator. always null
						decodedType.getRange(), makeEnabledSupplier(decodedType));
			} else {
				wizardBuilder.addToGenericPage(AdapterUtils.inferPageTitle(serviceConf, page),
						AdapterUtils.inferPageHelp(page), serviceArgument.getHelp(), value, //
						null, // no Configuration file defined with this kind of evaluator. always null
						makeEnabledSupplier(decodedType), choices);
			}
		}
	}

	/**
	 * Add a generic wizard page to edit a String
	 */
	private void addStringToGenericPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		WritableString value = new WritableString(argumentValueGetter.getValue(serviceArgument));
		value.addChangeListener(event -> argumentValueSetter.setString(value.get(), serviceArgument));
		Optional<GwsServiceArgumentPage> page = serviceConf.getPageFor(serviceArgument);
		if (page.isEmpty() || page.get().visible().booleanValue())
			wizardBuilder.addToGenericPage(AdapterUtils.inferPageTitle(serviceConf, page),
					AdapterUtils.inferPageHelp(page), serviceArgument.getHelp(), value, //
					null, // * no Configuration file defined with this kind of evaluator. always null
					makeEnabledSupplier(decodedType), serviceArgument.getChoices());
	}

	/**
	 * Add a generic wizard page to edit a duration
	 */
	private void addDurationToGenericPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		WritableDuration value = new WritableDuration(argumentValueGetter.getDuration(serviceArgument));
		value.addChangeListener(event -> argumentValueSetter.setDuration(value.getValue(), serviceArgument));
		Optional<GwsServiceArgumentPage> page = serviceConf.getPageFor(serviceArgument);
		if (page.isEmpty() || page.get().visible().booleanValue())
			wizardBuilder.addToGenericPage(AdapterUtils.inferPageTitle(serviceConf, page),
					AdapterUtils.inferPageHelp(page), serviceArgument.getHelp(), value, //
					null, // * no Configuration file defined with this kind of evaluator. always null
					makeEnabledSupplier(decodedType));
	}

	/**
	 * Add a generic wizard page to edit a duration
	 */
	private void addInstantToGenericPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		WritableInstant value = new WritableInstant(
				argumentValueGetter.getInstant(serviceArgument).orElse(Instant.EPOCH));
		argumentValueSetter.setInstant(value.getValue(), serviceArgument);
		value.addChangeListener(event -> argumentValueSetter.setInstant(value.getValue(), serviceArgument));
		Optional<GwsServiceArgumentPage> page = serviceConf.getPageFor(serviceArgument);
		if (page.isEmpty() || page.get().visible().booleanValue())
			wizardBuilder.addToGenericPage(AdapterUtils.inferPageTitle(serviceConf, page),
					AdapterUtils.inferPageHelp(page), serviceArgument.getHelp(), value, //
					null, // * no Configuration file defined with this kind of evaluator. always null
					makeEnabledSupplier(decodedType));
	}

	/**
	 * Makes a BooleanSupplier corresponding to the equals condition defined for the argument if any.
	 */
	private BooleanSupplier makeEnabledSupplier(DecodedType decodedType) {
		return AdapterUtils.makeEnabledSupplier(serviceConf, decodedType);
	}

	/**
	 * @return the {@link #loadFilesAfter}
	 */
	public boolean haveToLoadFilesAfter() {
		return fileAdapterDelegate.getLoadFilesAfter().isTrue()
				|| timeIntervalAdapterDelegate.isPresent() && timeIntervalAdapterDelegate.get().loadFilesAfter.isTrue();
	}

	/**
	 * @return the {@link #loadFilesAfter}
	 */
	public String getWhereToloadFiles() {
		var result = fileAdapterDelegate.getWhereToloadFiles().get();
		// Specific case of page with "Time interval" (Cut/Merge process)
		if (result.isEmpty() && timeIntervalAdapterDelegate.isPresent())
			result = timeIntervalAdapterDelegate.get().whereToloadFiles.get();
		return result;
	}

	/**
	 * @return the {@link #withSummary}
	 */
	public boolean isWithSummary() {
		return withSummary;
	}

	/**
	 * @param withSummary the {@link #withSummary} to set
	 */
	public void setWithSummary(boolean withSummary) {
		this.withSummary = withSummary;
	}

	/**
	 * @return the {@link #inputFiles}
	 */
	public WritableObjectList<File> getInputFiles() {
		return fileAdapterDelegate.getInputFiles();
	}

	/**
	 * @return the {@link #inputFolder}
	 */
	public WritableFile getInputFolder() {
		return fileAdapterDelegate.getInputFolder();
	}

	/**
	 * @return the {@link #outputFiles}
	 */
	public WritableObjectList<File> getOutputFiles() {
		return fileAdapterDelegate.getOutputFiles();
	}

	/**
	 * @return the {@link #serviceConf}
	 */
	public GwsServiceConf getServiceConf() {
		return serviceConf;
	}

	/**
	 * @return the {@link #computeOutputFiles}
	 */
	public boolean isComputeOutputFiles() {
		return fileAdapterDelegate.isComputeOutputFiles();
	}

	/**
	 * @param computeOutputFiles the {@link #computeOutputFiles} to set
	 */
	public void setComputeOutputFiles(boolean computeOutputFiles) {
		fileAdapterDelegate.setComputeOutputFiles(computeOutputFiles);
	}

}
