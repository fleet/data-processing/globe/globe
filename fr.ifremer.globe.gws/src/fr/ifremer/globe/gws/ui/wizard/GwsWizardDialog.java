package fr.ifremer.globe.gws.ui.wizard;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.utils.image.Icons;

/** WizardDialog with Relaunch and Relaunch... buttons */
public class GwsWizardDialog extends WizardDialog {

	private static Logger logger = LoggerFactory.getLogger(GwsWizardDialog.class);

	/** Path to the html help page */
	private final Optional<String> helpPath;

	/** True to manage Relaunch and Relaunch... buttons */
	private boolean firstPageIsSummary = false;

	/**
	 * Constructor
	 */
	public GwsWizardDialog(Shell parentShell, IWizard newWizard, Optional<String> helpPath) {
		super(parentShell, newWizard);
		this.helpPath = helpPath;
	}

	/** Redefined to add Relaunch and Relaunch... buttons */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);
		// Use Finish button to manage Run
		var relaunchButton = getButton(IDialogConstants.FINISH_ID);
		relaunchButton.setImage(Icons.RUN.toImage());
		relaunchButton.setText("Run");

		if (firstPageIsSummary) {
			// Mask Previous and Next buttons
			getButton(IDialogConstants.BACK_ID).setVisible(false);
			getButton(IDialogConstants.NEXT_ID).setVisible(false);

			// Add Edit and run... button
			var launchButton = createButton(parent, IDialogConstants.CLIENT_ID, "Edit and run...", false);
			launchButton.moveAbove(relaunchButton);
		}
	}

	/** The Help button has been pressed. */
	@Override
	protected void helpPressed() {
		helpPath.ifPresent(path -> {
			try {
				File htmlFile = new File(path);
				Desktop.getDesktop().browse(htmlFile.toURI());
			} catch (IOException e) {
				logger.error("Unable to display help file", e);
			}
		});
	}

	/** Redefined to manage extra client buttons */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId >= IDialogConstants.CLIENT_ID) {
			// Hard close the dialog.
			setReturnCode(buttonId);
			close();
		} else
			super.buttonPressed(buttonId);
	}

	/**
	 * @param firstPageIsSummary the {@link #firstPageIsSummary} to set
	 */
	public void setFirstPageIsSummary(boolean firstPageIsSummary) {
		this.firstPageIsSummary = firstPageIsSummary;
	}

}