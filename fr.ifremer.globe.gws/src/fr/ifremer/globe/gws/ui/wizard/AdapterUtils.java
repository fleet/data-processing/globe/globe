package fr.ifremer.globe.gws.ui.wizard;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.BooleanSupplier;
import java.util.stream.Collectors;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument.DecodedType;
import fr.ifremer.globe.gws.server.service.GwsServiceArgumentPage;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueGetter;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableFloat;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.observable.WritableString;

/**
 * Common functions of Adapter classes
 */
public class AdapterUtils {

	/**
	 * Constructor
	 */
	private AdapterUtils() {
	}

	/**
	 * Makes a BooleanSupplier corresponding to the equals condition defined for the argument if any.
	 */
	public static BooleanSupplier makeEnabledSupplier(GwsServiceConf process, DecodedType decodedType) {
		BooleanSupplier result = null;
		String[] condition = decodedType.getEqualsCondition();
		if (condition != null) {
			Optional<GwsServiceArgument> targetArg = process.getArgumentOfName(condition[0]);
			if (targetArg.isPresent()) {
				result = () -> condition[1].equalsIgnoreCase(new ArgumentValueGetter().getValue(targetArg.get()));
			}
		}
		return result != null ? result : () -> true;
	}

	/**
	 * @return the name of the generic page
	 */
	public static String inferPageTitle(GwsServiceConf process, Optional<GwsServiceArgumentPage> page) {
		return page.map(GwsServiceArgumentPage::title).orElse("Arguments of '" + process.name() + "'");
	}

	/**
	 * @return the name of the generic page
	 */
	public static String inferPageHelp(Optional<GwsServiceArgumentPage> page) {
		return page.map(GwsServiceArgumentPage::description).orElse("Complete the arguments if necessary");
	}

	/**
	 * Extract the file extension
	 */
	public static List<String> getFileExtensions(DecodedType decodedType) {
		// Deduce file extensions from ContentType
		List<String> result = decodedType.getContentTypes().stream()//
				.flatMap(contentType -> IFileService.grab().getExtensions(contentType).stream())//
				.collect(Collectors.toList());

		// Adding explicit file extensions
		String moreExtensions = decodedType.getParameters().get(GwsServiceArgument.EXTENSIONS);
		if (moreExtensions != null && !moreExtensions.isBlank()) {
			Arrays.stream(moreExtensions.split("\\|")) //
					.map(extension -> extension.startsWith("*.") ? extension.substring(2) : extension) //
					.forEach(result::add);
		}
		// Adding explicit file extensions
		String oneMoreExtension = decodedType.getParameters().get(GwsServiceArgument.EXTENSION);
		if (oneMoreExtension != null && !oneMoreExtension.isBlank()) {
			result.add(oneMoreExtension.startsWith("*.") ? oneMoreExtension.substring(2) : oneMoreExtension);
		}

		return result.stream().distinct().toList();
	}

	/** Bind a service argument to a model */
	public static void bindArgument(GwsServiceArgument argument, WritableString model) {
		var getter = new ArgumentValueGetter();
		var initialValue = getter.getValue(argument);
		model.set(initialValue);

		model.addChangeListener(e -> new ArgumentValueSetter().setString(model.getValue(), argument));
	}

	/** Bind a service argument to a model */
	public static void bindArgument(GwsServiceArgument argument, WritableBoolean model) {
		var getter = new ArgumentValueGetter();
		var initialValue = getter.getBoolean(getter.getValue(argument));
		model.set(initialValue);

		model.addChangeListener(e -> new ArgumentValueSetter().setBoolean(model.getValue(), argument));
	}

	/** Bind a service argument to a model */
	public static void bindArgument(GwsServiceArgument argument, WritableFile model) {
		var getter = new ArgumentValueGetter();
		var initialValue = getter.getFile(getter.getValue(argument));
		model.set(initialValue);

		model.addValueChangeListener(e -> new ArgumentValueSetter().setFile(model.getValue(), argument));
	}

	/** Bind a service argument to a model */
	public static void bindArgument(GwsServiceArgument argument, WritableDouble model) {
		var getter = new ArgumentValueGetter();
		var initialValue = getter.getDouble(argument);
		model.set(initialValue);

		model.addChangeListener(e -> new ArgumentValueSetter().setDouble(model.getValue(), argument));
	}

	/** Bind a service argument to a model */
	public static void bindArgument(GwsServiceArgument argument, WritableFloat model) {
		var getter = new ArgumentValueGetter();
		var initialValue = getter.getDouble(argument);
		if (initialValue != null)
			model.set(initialValue.floatValue());

		model.addChangeListener(e -> new ArgumentValueSetter().setDouble(model.getValue().doubleValue(), argument));
	}

	/** Bind a service argument to a model */
	public static <T extends Enum<T>> void bindArgument(GwsServiceArgument argument, WritableObject<T> model,
			Class<T> enumClass, T defaultValue) {
		var getter = new ArgumentValueGetter();
		T initialValue = defaultValue;
		String stringValue = getter.getValue(argument);
		if (stringValue != null) {
			try {
				initialValue = Enum.valueOf(enumClass, stringValue);
			} catch (Exception e) {
				// Bad enum value. Keep default value
			}
		}
		model.set(initialValue);
		model.addChangeListener(e -> new ArgumentValueSetter().setString(model.getValue().name(), argument));
	}
}
