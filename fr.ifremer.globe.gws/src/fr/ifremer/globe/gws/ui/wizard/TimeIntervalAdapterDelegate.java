package fr.ifremer.globe.gws.ui.wizard;

import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.OUT_FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.OVERWRITE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.TIME_INTERVAL_CUT_FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.TIME_INTERVAL_END_DATE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.TIME_INTERVAL_GEO_MASK_FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.TIME_INTERVAL_REVERSE_GEO_MASK;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.TIME_INTERVAL_START_DATE;

import java.io.File;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument.DecodedType;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueGetter;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.utils.FileUtils;

/**
 * This class aim to map time interval arguments.
 *
 */
public class TimeIntervalAdapterDelegate {

	protected static Logger logger = LoggerFactory.getLogger(TimeIntervalAdapterDelegate.class);

	/** Argument setter and getter */
	protected final ArgumentValueGetter argumentValueGetter = new ArgumentValueGetter();
	protected final ArgumentValueSetter argumentValueSetter = new ArgumentValueSetter();

	/** Considered process */
	protected GwsServiceConf process;

	/** Cache of IInfoStore created from input files */
	protected final Map<File, IFileInfo> inputFileInfos;

	/** Model */
	protected final WritableBoolean isMergeSimple = new WritableBoolean(true);
	protected final WritableBoolean isCutFile = new WritableBoolean();
	protected final WritableFile cutFile = new WritableFile();
	protected final WritableBoolean isManually = new WritableBoolean();
	protected final WritableObject<Instant> startDate;
	protected final WritableObject<Instant> endDate;
	protected final WritableBoolean isGeoMaskFile = new WritableBoolean();
	protected final WritableFile geoMaskFile = new WritableFile();
	protected final WritableBoolean reverseGeoMask = new WritableBoolean();
	/** Start/End dates of all input files */
	protected final WritableObject<Pair<Instant, Instant>> referenceTimeInterval;

	/** Service argument time_interval#start_date */
	protected GwsServiceArgument startDateArgument;
	/** Service argument time_interval#end_date */
	protected GwsServiceArgument endDateArgument;
	/** Service argument time_interval#cut_file */
	protected GwsServiceArgument cutFileArgument;
	/** Service argument time_interval#geo_mask_file */
	protected GwsServiceArgument geoMaskFileArgument;
	/** Service argument time_interval#reverser_geo_mask_file */
	protected GwsServiceArgument reverseGeoMaskArgument;

	/** Fake input files to allow auto generation of output files from profiles in SelectOutputParametersPage */
	protected final WritableObjectList<File> profileFiles = new WritableObjectList<>();
	/** Output files */
	protected final WritableObjectList<File> outputFiles = new WritableObjectList<>();
	/** If true, output files are overwriting when exist */
	protected final WritableBoolean overwriteExistingFiles = new WritableBoolean(false);
	/** If true, output files has to be loaded after processed */
	protected final WritableBoolean loadFilesAfter = new WritableBoolean(false);
	/** Where to load the files (in witch group of project explorer) */
	protected final WritableString whereToloadFiles = new WritableString();

	/**
	 * Constructor
	 */
	public TimeIntervalAdapterDelegate(GwsServiceConf process, Map<File, IFileInfo> inputFileInfos) {
		this.process = process;
		this.inputFileInfos = inputFileInfos;

		referenceTimeInterval = new WritableObject<>(new Pair<>(Instant.now(), Instant.now()));
		updateTimeIntervals();
		startDate = new WritableObject<>(referenceTimeInterval.get().getFirst());
		endDate = new WritableObject<>(referenceTimeInterval.get().getSecond());
	}

	/**
	 * Prepare the attributes to display a mask wizard page
	 */
	public void bindArgument(GwsServiceArgument arg, DecodedType decodedType) {
		String argumentType = decodedType.getRefinedType().toLowerCase();
		if (TIME_INTERVAL_CUT_FILE.equals(argumentType)) {
			bindCutFile(arg);
		} else if (TIME_INTERVAL_START_DATE.equals(argumentType)) {
			bindStartDate(arg);
		} else if (TIME_INTERVAL_END_DATE.equals(argumentType)) {
			bindEndDate(arg);
		} else if (OUT_FILE.equals(argumentType)) {
			bindOutFile(arg);
		} else if (TIME_INTERVAL_GEO_MASK_FILE.equals(argumentType)) {
			bindGeoMaskFile(arg);
		} else if (TIME_INTERVAL_REVERSE_GEO_MASK.equals(argumentType)) {
			bindReverseGeoMask(arg);
		} else if (OVERWRITE.equals(argumentType)) {
			bindOverwrite(arg);
		}
		updateArguments();
	}

	/** Bind cutFile value to service "time_interval#cut_file" argument */
	protected void bindCutFile(GwsServiceArgument cutFileArgument) {
		this.cutFileArgument = cutFileArgument;

		// Set initial values
		File file = argumentValueGetter.getFile(argumentValueGetter.getValue(cutFileArgument));
		isCutFile.set(file != null && !file.getPath().isEmpty());
		if (isCutFile.isTrue()) {
			isMergeSimple.setValue(false);
			isManually.setValue(false);
			cutFile.set(file);
		}

		// Send value of cutFile to the service argument when changed
		cutFile.addChangeListener(e -> updateArguments());
		isCutFile.addChangeListener(e -> updateArguments());
		referenceTimeInterval.addChangeListener(e -> updateArguments());
	}

	/** Bind geographic mask file value to service "time_interval#geo_mask_file" argument */
	protected void bindGeoMaskFile(GwsServiceArgument geoMaskFileArgument) {
		this.geoMaskFileArgument = geoMaskFileArgument;

		// Set initial values
		File file = argumentValueGetter.getFile(argumentValueGetter.getValue(geoMaskFileArgument));
		isGeoMaskFile.set(file != null && !file.getPath().isEmpty());
		if (isGeoMaskFile.isTrue()) {
			isMergeSimple.setValue(false);
			isManually.setValue(false);
			geoMaskFile.set(file);
		}

		// Send value of cutFile to the service argument when changed
		geoMaskFile.addChangeListener(e -> updateArguments());
		isGeoMaskFile.addChangeListener(e -> updateArguments());
	}

	/** Bind geographic mask file value to service "time_interval#reverse_geo_mask" argument */
	protected void bindReverseGeoMask(GwsServiceArgument reverseGeoMaskArgument) {
		this.reverseGeoMaskArgument = reverseGeoMaskArgument;
		reverseGeoMask.addChangeListener(e -> updateArguments());
	}

	/**
	 * Called by the WizardApdapter when input file changed. <br>
	 * In this case, we have to manage a new set of TimeInterval and update output filename
	 */
	public void onInputFilesChanged() {
		updateArguments();
		updateTimeIntervals();
	}

	/**
	 * Called by the WizardApdapter when input file changed. <br>
	 * In this case, we have to manage a new set of TimeInterval
	 */
	private void updateTimeIntervals() {
		var identity = new Pair<>(Instant.MAX, Instant.MIN);
		var timeInterval = identity;
		for (IFileInfo fileInfo : inputFileInfos.values()) {
			var startEndDateOption = fileInfo.getStartEndDate();
			if (startEndDateOption.isPresent()) {

				Instant startTime = startEndDateOption.get().getFirst();
				if (startTime != null && startTime.compareTo(timeInterval.getFirst()) < 0) {
					timeInterval = Pair.of(startTime, timeInterval.getSecond());
				}
				Instant endTime = startEndDateOption.get().getSecond();
				if (endTime != null && endTime.compareTo(timeInterval.getSecond()) > 0) {
					timeInterval = Pair.of(timeInterval.getFirst(), endTime);
				}
			}
		}
		if (timeInterval != identity) {
			referenceTimeInterval.set(timeInterval);
		}
	}

	/** Send value of cutFile to the service argument when changed */
	protected void updateArguments() {
		if (inputFileInfos.keySet().isEmpty()) {
			return;
		}

		// Update cut file argument.
		if (cutFileArgument != null) {
			argumentValueSetter.setFile(isCutFile.isTrue() ? cutFile.getValue() : null, cutFileArgument);
		}
		// Update geographic mask arguments.
		if (geoMaskFileArgument != null) {
			argumentValueSetter.setFile(isGeoMaskFile.isTrue() ? geoMaskFile.getValue() : null, geoMaskFileArgument);
		}
		if (reverseGeoMaskArgument != null) {
			argumentValueSetter.setBoolean(isGeoMaskFile.isTrue() ? reverseGeoMask.getValue() : null,
					reverseGeoMaskArgument);
		}

		// Update output file(s) argument.
		File firstInputFile = inputFileInfos.keySet().iterator().next();
		File outputDir = firstInputFile.getParentFile();
		String extension = guessOutputfileExtension(firstInputFile);
		profileFiles.clear();
		if (isCutFile.isTrue()) {
			// Cut file : output file(s) name computed from cut lines.
			try {
				ProfileUtils.loadProfilesFromFile(cutFile.getValue()).stream()
						.map(p -> new File(outputDir, p.getId() + extension)).forEach(profileFiles::add);
			} catch (Exception e) {
				logger.debug("Cut file ignored : {}", e.getMessage());
			}
		} else if (isGeoMaskFile.isTrue()) {
			// Geographic mask file : output file(s) name computed from input files.
			inputFileInfos.keySet().stream().map(inputFile -> FileUtils.addSuffix(inputFile, "_cut"))
					.forEach(profileFiles::add);
		} else {
			// Merge simple or with custom interval = 1 output file.
			profileFiles.add(inputFileInfos.size() == 1 ? FileUtils.addSuffix(firstInputFile, "_cut")
					: new File(outputDir, "output_merge" + extension));
		}
	}

	/** Compute the file extension of the ouput file */
	protected String guessOutputfileExtension(File inputFile) {
		// Techsas file extension
		String extension = FileUtils.getExtension(inputFile);
		if (!extension.endsWith(".nc")) {
			extension += ".nc";
		}
		return "." + extension;
	}

	/** Bind startDate value to service "time_interval#start_date" argument */
	protected void bindStartDate(GwsServiceArgument startDateArgument) {
		this.startDateArgument = startDateArgument;
		updateManualInstant(startDate, startDateArgument);

		// Send value of startDate to the service argument when changed
		startDate.addChangeListener(e -> updateManualInstant(startDate, startDateArgument));
		isManually.addChangeListener(e -> updateManualInstant(startDate, startDateArgument));
	}

	/** Bind endDate value to service "time_interval#end_date" argument */
	protected void bindEndDate(GwsServiceArgument endDateArgument) {
		this.endDateArgument = endDateArgument;
		updateManualInstant(endDate, endDateArgument);

		// Send value of instant to the service argument when changed
		endDate.addChangeListener(e -> updateManualInstant(endDate, endDateArgument));
		isManually.addChangeListener(e -> updateManualInstant(endDate, endDateArgument));
	}

	/** Send value of instant to the service argument when changed */
	protected void updateManualInstant(WritableObject<Instant> oneInstant, GwsServiceArgument oneInstantArgument) {
		if (isManually.isTrue()) {
			argumentValueSetter.setInstant(oneInstant.getValue().truncatedTo(ChronoUnit.SECONDS), oneInstantArgument);
		} else {
			argumentValueSetter.setInstant(null, oneInstantArgument);
		}
	}

	/** Bind endDate value to service "time_interval#end_date" argument */
	protected void bindOutFile(GwsServiceArgument outFileArgument) {
		outputFiles.clear(); // Output files are always recomputed
		outputFiles.addChangeListener(e -> argumentValueSetter.setFiles(outputFiles, outFileArgument));
	}

	/** Bind endDate value to service "time_interval#end_date" argument */
	protected void bindOverwrite(GwsServiceArgument overwriteArgument) {
		// Init initial value
		overwriteExistingFiles.set(
				Boolean.TRUE.equals(argumentValueGetter.getBoolean(argumentValueGetter.getValue(overwriteArgument))));
		// Send value of instant to the service argument when changed
		overwriteExistingFiles.addChangeListener(
				e -> argumentValueSetter.setBoolean(overwriteExistingFiles.getValue(), overwriteArgument));
	}

}
