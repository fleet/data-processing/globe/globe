package fr.ifremer.globe.gws.ui.wizard;

import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.IN_FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.IN_FOLDER;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.MASK_POLYGONS;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.MASK_POLYGONS_REVERSE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.OUT_FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.OVERWRITE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.SINGLE_FOLDER;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BooleanSupplier;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument.DecodedType;
import fr.ifremer.globe.gws.server.service.GwsServiceArgumentPage;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueGetter;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.service.wizard.IWizardBuilder;

/**
 * This class aim at map
 * <ul>
 * <li>IN_FILE argument to select input files</li>
 * <li>IN_FOLDER argument to select input folder</li>
 * <li>OUT_FILE argument to select output files</li>
 * <li>OVERWRITE argument to indicate that output files can be overwritten</li>
 * <li>FILE argument to select some other files than the input ones</li>
 * <li>SINGLE_FOLDER to select an other folder than the input one</li>
 * <li>MASK_POLYGONS to select input files containing polygons</li>
 * <li>MASK_POLYGONS_REVERSE to reverse the polygon mask described by MASK_POLYGONS</li>
 * </ul>
 *
 */
public class FileAdapterDelegate {

	private static Logger logger = LoggerFactory.getLogger(FileAdapterDelegate.class);

	private IFileService fileService;

	/** Argument setter and getter */
	private ArgumentValueGetter argumentValueGetter = new ArgumentValueGetter();
	private ArgumentValueSetter argumentValueSetter = new ArgumentValueSetter();

	/** Considered process */
	private GwsServiceConf process;

	/** Input folder if any */
	private final WritableFile inputFolder = new WritableFile();
	/** Input files if any */
	private final WritableObjectList<File> inputFiles = new WritableObjectList<>();
	/** Output files if any */
	private final WritableObjectList<File> outputFiles = new WritableObjectList<>();

	/** Does the input files need to be preloaded or not */
	private boolean loadInputFile = true;
	/** If true, output files are overwriting when exist */
	private final WritableBoolean overwriteExistingFiles = new WritableBoolean(false);
	/** If true, output files has to be loaded after processed */
	private final WritableBoolean loadFilesAfter = new WritableBoolean(false);
	/** Where to load the files (in witch group of project explorer) */
	private final WritableString whereToloadFiles = new WritableString();

	/** Mask made up of polygon files */
	private final WritableObjectList<File> polygonMask = new WritableObjectList<>();
	/** If true, the polygon mask is reversed */
	private final WritableBoolean reversePolygonMask = new WritableBoolean();

	// By default, consider OutputFiles = InputFiles
	private IChangeListener transfertInputFilesToOutput;

	/** Cache of IInfoStore created from input files */
	private final Map<File, IFileInfo> inputFileInfos = new HashMap<>();

	/** True when output files are computed (the default) */
	private boolean computeOutputFiles = true;

	/**
	 * Constructor
	 */
	public FileAdapterDelegate(GwsServiceConf process, IFileService fileService) {
		this.process = process;
		this.fileService = fileService;
	}

	/**
	 * Manage an argument to generate a new Wizard page
	 */
	public void addWizardPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {

		switch (decodedType.getRefinedType().toLowerCase()) {
		case IN_FILE:
			loadInputFile = !decodedType.getContentTypes().contains(ContentType.UNDEFINED);
			if (serviceArgument.requireListAsValue()) {
				addSelectInputFilesPage(wizardBuilder, serviceArgument, decodedType);
			} else {
				addSelectInputFilePage(wizardBuilder, serviceArgument, decodedType);
			}
			break;

		case IN_FOLDER:
			addSelectInFolderPage(wizardBuilder, serviceArgument);
			break;

		case OUT_FILE:
			addSelectOutputFilesPage(wizardBuilder, serviceArgument, decodedType);
			break;

		case OVERWRITE:
			manageOverwriting(serviceArgument);
			break;

		case FILE:
			if (serviceArgument.requireListAsValue()) {
				addSelectFilesPage(wizardBuilder, serviceArgument, decodedType);
			} else {
				addSelectFilePage(wizardBuilder, serviceArgument, decodedType);
			}
			break;

		case SINGLE_FOLDER:
			addSelectFolderPage(wizardBuilder, serviceArgument);
			break;

		case MASK_POLYGONS:
			addSelectPolygonMaskPage(wizardBuilder, serviceArgument);
			break;

		case MASK_POLYGONS_REVERSE:
			manageReversePolygonMaskPage(wizardBuilder, serviceArgument);
			break;
		default:
			logger.error("Argument not managed by this delegate {}", decodedType.getRefinedType().toLowerCase());
			break;
		}
	}

	/**
	 * Add a wizard page to select some files
	 */
	private void addSelectInputFilesPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		if (inputFiles.isEmpty()) {
			inputFiles.addAll(argumentValueGetter.getFiles(serviceArgument.getValue()));
			inputFiles.addChangeListener(event -> argumentValueSetter.setFiles(inputFiles, serviceArgument));
		}

		List<Pair<String, String>> fileFilters = getFileFilter(decodedType);
		wizardBuilder.addSelectInputFilesPage(serviceArgument.getDecoredLabel(), fileFilters, true, inputFiles,
				makeEnabledSupplier(decodedType));

	}

	/**
	 * Add a wizard page to select a single input file
	 */
	private void addSelectInputFilePage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		WritableFile selectedFile = new WritableFile(argumentValueGetter.getFile(serviceArgument.getValue()));
		selectedFile.addChangeListener(event -> argumentValueSetter.setFile(selectedFile.get(), serviceArgument));

		List<Pair<String, String>> fileFilter = getFileFilter(decodedType);
		wizardBuilder.addSelectFilePage(serviceArgument.getDecoredLabel(), "1".equals(serviceArgument.getNargs()),
				fileFilter, selectedFile);

		if (selectedFile.isNotNull()) {
			inputFiles.clear();
			inputFiles.add(selectedFile.get());
		}
		selectedFile.addChangeListener(event -> {
			if (inputFiles.isEmpty()) {
				inputFiles.add(selectedFile.get());
			} else {
				inputFiles.set(0, selectedFile.get());
			}
		});
	}

	/**
	 * Add a wizard page to select some files
	 */
	private void addSelectFilesPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		var files = new WritableObjectList<File>();
		files.addAll(argumentValueGetter.getFiles(serviceArgument.getValue()));
		files.addChangeListener(event -> argumentValueSetter.setFiles(files, serviceArgument));

		List<Pair<String, String>> fileFilters = getFileFilter(decodedType);
		wizardBuilder.addSelectInputFilesPage(serviceArgument.getDecoredLabel(), fileFilters, false, files,
				makeEnabledSupplier(decodedType));
	}

	/**
	 * Add a wizard page to select one file
	 */
	private void addSelectFilePage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		WritableFile selectedFile = new WritableFile(argumentValueGetter.getFile(serviceArgument.getValue()));
		selectedFile.addChangeListener(event -> argumentValueSetter.setFile(selectedFile.get(), serviceArgument));

		Optional<GwsServiceArgumentPage> page = process.getPageFor(serviceArgument);
		if (page.isPresent()) {
			if (page.get().visible().booleanValue()) {
				var filters = getFileFilter(decodedType);
				var extensions = filters.stream().map(Pair::getSecond).toList();
				wizardBuilder.addToGenericPage(AdapterUtils.inferPageTitle(process, page),
						AdapterUtils.inferPageHelp(page), serviceArgument.getHelp(), selectedFile, //
						null, // * no Configuration file defined with this kind of evaluator. always null
						extensions);
			}
		} else {
			List<Pair<String, String>> fileFilter = getFileFilter(decodedType);
			wizardBuilder.addSelectFilePage(serviceArgument.getDecoredLabel(), "1".equals(serviceArgument.getNargs()),
					fileFilter, selectedFile);
		}
	}

	/**
	 * Add a wizard page to select one folder
	 */
	private void addSelectFolderPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument) {
		WritableFile folder = new WritableFile(argumentValueGetter.getFile(serviceArgument.getValue()));
		folder.addChangeListener(event -> argumentValueSetter.setFile(folder.get(), serviceArgument));
		wizardBuilder.addSelectFolderPage(serviceArgument.getDecoredLabel(), "1".equals(serviceArgument.getNargs()),
				folder);
	}

	/**
	 * Add a wizard page to select input folder
	 */
	private void addSelectInFolderPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument) {
		if (inputFolder.isNull()) {
			inputFolder.set(argumentValueGetter.getFile(serviceArgument.getValue()));
			inputFolder.addChangeListener(event -> argumentValueSetter.setFile(inputFolder.get(), serviceArgument));
		}
		wizardBuilder.addSelectFolderPage(serviceArgument.getDecoredLabel(), "1".equals(serviceArgument.getNargs()),
				inputFolder);
	}

	/**
	 * Add a wizard page to select some output files
	 */
	private void addSelectOutputFilesPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument,
			DecodedType decodedType) {
		inputFiles.removeChangeListener(transfertInputFilesToOutput);
		if (computeOutputFiles)
			outputFiles.clear(); // Output files are always recomputed
		else
			outputFiles.addAll(argumentValueGetter.getFiles(serviceArgument.getValue()));
		outputFiles.addChangeListener(event -> argumentValueSetter.setFiles(outputFiles, serviceArgument));

		WritableString prefix = new WritableString(decodedType.getParameters().getOrDefault("prefix", ""));
		WritableString suffix = new WritableString(decodedType.getParameters().getOrDefault("suffix", ""));

		// File Extensions
		List<String> fileExtensions = AdapterUtils.getFileExtensions(decodedType);

		// Extend Techsas file extensions with .nc
		if (decodedType.getContentTypes().contains(ContentType.TECHSAS_NETCDF)) {
			fileExtensions = fileExtensions.stream() //
					.map(extension -> extension.endsWith("nc") ? extension : extension + ".nc") //
					.toList();
		}

		// Single ouput file with a default name ?
		WritableString singleOutputFilename = new WritableString();
		// Only one output file expected ?
		if (!serviceArgument.requireListAsValue() && serviceArgument.getDefaultValue() != null) {
			GwsServiceArgument inputFileArg = process.getInputFilesArgument().orElse(null);
			// N input files to 1 output file (like a merge)
			if (inputFileArg != null && inputFileArg.requireListAsValue()) {
				singleOutputFilename.set(serviceArgument.getDefaultValue());
			}
		}
		// Must generate a temporary file ?
		if ("true".equalsIgnoreCase(decodedType.getParameters().get("generated"))) {
			String filename = "";
			if (!prefix.isEmpty()) {
				filename = prefix.get() + "_";
			}
			filename += System.currentTimeMillis();
			if (!suffix.isEmpty()) {
				filename += "_" + suffix.get();
			}
			if (!fileExtensions.isEmpty()) {
				filename += "." + fileExtensions.get(0);
			}
			singleOutputFilename.set(filename);

			// By default, a generated file is loaded
			loadFilesAfter.set(true);
		}

		WritableBoolean mergeOutputFiles = new WritableBoolean(!serviceArgument.requireListAsValue());
		boolean mergeAllowed = serviceArgument.getNargs() != null && serviceArgument.getNargs().contains("|1");

		wizardBuilder.addSelectOutputFilesPage(serviceArgument.getDecoredLabel(), inputFiles,
				new WritableObjectList<>(fileExtensions), outputFiles, loadFilesAfter, whereToloadFiles,
				overwriteExistingFiles, mergeOutputFiles, prefix, suffix, singleOutputFilename, mergeAllowed);
	}

	/**
	 * Map overwriting argument to the model object.
	 */
	private void manageOverwriting(GwsServiceArgument serviceArgument) {
		overwriteExistingFiles.set(
				Boolean.TRUE.equals(argumentValueGetter.getBoolean(argumentValueGetter.getValue(serviceArgument))));
		overwriteExistingFiles.addChangeListener(
				event -> argumentValueSetter.setBoolean(overwriteExistingFiles.getValue(), serviceArgument));
	}

	private void addSelectPolygonMaskPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument) {
		polygonMask.addAll(argumentValueGetter.getFiles(serviceArgument.getValue()));
		polygonMask.addChangeListener(event -> argumentValueSetter.setFiles(polygonMask, serviceArgument));
		wizardBuilder.addSelectPolygonMaskPage(polygonMask, reversePolygonMask);
	}

	private void manageReversePolygonMaskPage(IWizardBuilder wizardBuilder, GwsServiceArgument serviceArgument) {
		reversePolygonMask.set(
				Boolean.TRUE.equals(argumentValueGetter.getBoolean(argumentValueGetter.getValue(serviceArgument))));
		reversePolygonMask.addChangeListener(
				event -> argumentValueSetter.setBoolean(reversePolygonMask.getValue(), serviceArgument));
	}

	/**
	 * Extract the file filter
	 */
	private List<Pair<String, String>> getFileFilter(DecodedType decodedType) {
		List<Pair<String, String>> result = new ArrayList<>();

		List<String> extensions = AdapterUtils.getFileExtensions(decodedType);
		if (!extensions.isEmpty()) {
			String filters = StringUtils.join(extensions.stream().map(ext -> "*." + ext).toList(), ";");
			String label = extensions.stream().limit(10).collect(Collectors.joining(" ,"));
			if (extensions.size() > 10) {
				label = label + "...";
			}
			result.add(new Pair<>(label, filters));
		} else {
			result.add(new Pair<>("*.*", "*.*"));

		}
		return result;
	}

	public void makeBindings() {
		// If output files page is not claimed, we consider output = input
		transfertInputFilesToOutput = event -> {
			if (computeOutputFiles) {
				outputFiles.clear();
				outputFiles.addAll(inputFiles);
			}
		};
		inputFiles.addChangeListener(transfertInputFilesToOutput);
	}

	/** Retrieve IFileInfo of new files */
	public void updateInputFileInfos() {
		// Clear the map when ever some input files have been removed
		inputFileInfos.clear();
		inputFiles.stream()//
				.filter(file -> !inputFileInfos.containsKey(file))//
				.filter(File::exists)// File must exist
				// .filter(file -> !FilenameUtils.getExtension(filePath).endsWith("txt"))// we skip .txt extension
				// drivers, too many driver are available for these extension
				.filter(x -> loadInputFile) // we do not preload files if the flag load input file is not set
				.map(File::getPath)//
				.map(fileService::getFileInfoSilently)//
				.filter(Optional::isPresent)//
				.map(Optional::get)//
				.forEach(fileInfo -> inputFileInfos.put(fileInfo.toFile(), fileInfo));

	}

	/**
	 * Makes a BooleanSupplier corresponding to the equals condition defined for the argument if any.
	 */
	private BooleanSupplier makeEnabledSupplier(DecodedType decodedType) {
		return AdapterUtils.makeEnabledSupplier(process, decodedType);
	}

	/**
	 * @return the {@link #inputFileInfos}
	 */
	public Map<File, IFileInfo> getInputFileInfos() {
		return inputFileInfos;
	}

	/**
	 * @return the {@link #inputFiles}
	 */
	public WritableObjectList<File> getInputFiles() {
		return inputFiles;
	}

	/**
	 * @return the {@link #inputFolder}
	 */
	public WritableFile getInputFolder() {
		return inputFolder;
	}

	/**
	 * @return the {@link #outputFiles}
	 */
	public WritableObjectList<File> getOutputFiles() {
		return outputFiles;
	}

	/**
	 * @return the {@link #loadInputFile}
	 */
	public boolean isLoadInputFile() {
		return loadInputFile;
	}

	/**
	 * @return the {@link #loadFilesAfter}
	 */
	public WritableBoolean getLoadFilesAfter() {
		return loadFilesAfter;
	}

	/**
	 * @return the {@link #whereToloadFiles}
	 */
	public WritableString getWhereToloadFiles() {
		return whereToloadFiles;
	}

	/**
	 * @return the {@link #overwriteExistingFiles}
	 */
	public WritableBoolean getOverwriteExistingFiles() {
		return overwriteExistingFiles;
	}

	/**
	 * @return the {@link #computeOutputFiles}
	 */
	public boolean isComputeOutputFiles() {
		return computeOutputFiles;
	}

	/**
	 * @param computeOutputFiles the {@link #computeOutputFiles} to set
	 */
	public void setComputeOutputFiles(boolean computeOutputFiles) {
		this.computeOutputFiles = computeOutputFiles;
	}

}
