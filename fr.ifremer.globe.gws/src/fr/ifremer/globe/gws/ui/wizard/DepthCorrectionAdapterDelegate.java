package fr.ifremer.globe.gws.ui.wizard;

import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.DEPTH_CORRECTION_DRAUGHT_SOURCE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.DEPTH_CORRECTION_DRAUGHT_TTB;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.DEPTH_CORRECTION_NOMINAL_WATERLINE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.DEPTH_CORRECTION_PLATFORM_ELEVATION_TTB;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.DEPTH_CORRECTION_TIDE_SOURCE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.DEPTH_CORRECTION_TIDE_TTB;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.DEPTH_CORRECTION_TIDE_TYPE;

import java.io.File;

import fr.ifremer.globe.core.model.tide.TideType;
import fr.ifremer.globe.core.processes.depthcorrection.DepthCorrectionSource;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument.DecodedType;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.wizard.model.DepthCorrectionModel;

/**
 * This class aim to map time interval arguments.
 *
 */
public class DepthCorrectionAdapterDelegate {

	/** Model */
	private final DepthCorrectionModel model;

	/** Constructor */
	public DepthCorrectionAdapterDelegate(WritableObjectList<File> inputFiles) {
		model = DepthCorrectionModel.instance(inputFiles);
	}

	/**
	 * Prepare the attributes to display a mask wizard page
	 */
	public void bindArgument(GwsServiceArgument arg, DecodedType decodedType) {
		String argumentType = decodedType.getRefinedType().toLowerCase();
		if (DEPTH_CORRECTION_TIDE_TYPE.equals(argumentType))
			AdapterUtils.bindArgument(arg, model.tideType(), TideType.class, TideType.UNKNWON);
		else if (DEPTH_CORRECTION_TIDE_SOURCE.equals(argumentType))
			AdapterUtils.bindArgument(arg, model.tideCorrectionSource(), DepthCorrectionSource.class,
					DepthCorrectionSource.NONE);
		else if (DEPTH_CORRECTION_TIDE_TTB.equals(argumentType))
			AdapterUtils.bindArgument(arg, model.tideTtb());
		else if (DEPTH_CORRECTION_PLATFORM_ELEVATION_TTB.equals(argumentType))
			AdapterUtils.bindArgument(arg, model.platformElevationTtb());
		else if (DEPTH_CORRECTION_NOMINAL_WATERLINE.equals(argumentType))
			AdapterUtils.bindArgument(arg, model.nominalWaterLine());
		else if (DEPTH_CORRECTION_DRAUGHT_SOURCE.equals(argumentType))
			AdapterUtils.bindArgument(arg, model.draughtCorrectionSource(), DepthCorrectionSource.class,
					DepthCorrectionSource.NONE);
		else if (DEPTH_CORRECTION_DRAUGHT_TTB.equals(argumentType))
			AdapterUtils.bindArgument(arg, model.draughtTtb());
	}

	public DepthCorrectionModel getModel() {
		return model;
	}

}
