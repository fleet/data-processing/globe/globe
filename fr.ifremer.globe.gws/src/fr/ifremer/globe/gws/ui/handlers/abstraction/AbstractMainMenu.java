package fr.ifremer.globe.gws.ui.handlers.abstraction;

import java.util.List;
import java.util.Optional;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.ui.di.AboutToShow;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuFactory;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.service.GwsServiceGroup;

/**
 * Template method to generate menus to launch GWS services from the application main menu
 */
public abstract class AbstractMainMenu {

	/** Proxy to server **/
	@Inject
	private GwsServerProxy gwsServerProxy;

	/**
	 * First level of GwsServiceGroup.Empty when GWS server not available or not fully started
	 */
	private Optional<GwsServiceGroup> rootServiceGroup = Optional.empty();

	/**
	 * @return a copy of topGroup by retaining only managed groups
	 */
	protected abstract GwsServiceGroup filterServiceGroup(GwsServiceGroup topGroup);

	/**
	 * After construction, extract all processes with input file argument
	 */
	@PostConstruct
	private void postConstruct() {
		initializeRootServiceGroup();
	}

	/**
	 * Generates items before displaying the menu
	 */
	@AboutToShow
	public void aboutToShow(List<MMenuElement> items, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		rootServiceGroup.ifPresent(root -> {
			items.add(MMenuFactory.INSTANCE.createMenuSeparator());
			MenuUtils.createsMenu(items, root, true,
					service -> new LaunchWizardMenuItem(service, gwsServerProxy, null));
		});

		// Reload services in background (ensure GWS is completely started and menu fully generated)
		shell.getDisplay().asyncExec(this::initializeRootServiceGroup);
	}

	private void initializeRootServiceGroup() {
		rootServiceGroup = gwsServerProxy.findToolboxServicesGroup().map(this::filterServiceGroup);
	}

}