package fr.ifremer.globe.gws.ui.handlers;

import java.io.File;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.runtime.gws.param.ExportToNviParams;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.process.custom.ExportToNviProcess;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.utils.exception.BadParameterException;

/**
 * Handler to export a navigation file in NVI V2 format.
 */
public class ExportToNviHandler extends AbstractLaunchSpecificServiceHandler {

	@Inject
	private ESelectionService selectionService;

	/** Name of the service to launch */
	@Override
	protected String getServiceName() {
		return ExportToNviProcess.SERVICE_NAME;
	}

	/** @return list of files which can be exported to NVI V2 **/
	private List<File> getAcceptedFiles() {
		return getSelectionAsList(getSelection(selectionService), ContentType::hasNavigation)//
				.stream()//
				.map(FileInfoNode::getFileInfo) //
				// old NVI has a specific service to upgrade to .nvi.nc
				.filter(fileInfo -> fileInfo.getContentType() != ContentType.NVI_NETCDF_4) //
				.map(IFileInfo::toFile)//
				.toList();
	}

	/**
	 * Checks if there is one navigation file selected.
	 */
	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return super.computeCanExecute(selectionService) && !getAcceptedFiles().isEmpty();

	}

	/** Set navigation as input files in the configuration of the service */
	@Override
	protected ServiceLauncher getServiceLaucher(GwsService specificService) throws BadParameterException {
		var params = new ExportToNviParams(//
				getAcceptedFiles(), // inputFiles
				Optional.empty(), // navigationData, empty to use nav in input files
				Optional.empty(), // navPointsInFiles, empty to auto-generates it
				Optional.empty(), // ouputFiles, will be set up by wizard
				Optional.empty(), // overwrite, will be set up by wizard
				Optional.empty() // haveToLoadOutputFiles, will be set up by wizard
		);

		return new ExportToNviProcess(specificService, params);
	}

}