package fr.ifremer.globe.gws.ui.handlers.abstraction;

import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.FILE;
import static fr.ifremer.globe.gws.server.service.GwsServiceArgument.IN_FILE;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.gws.ContextInitializer;
import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceArgument.DecodedType;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import jakarta.inject.Named;

/**
 * Menu item to launch a GWS services on the selected files<br>
 * The name of this menu item is used to identify the service.
 */
public class LaunchWizardMenuItem {

	/** Service to launch */
	private final GwsService serviceToLaunch;

	private final GwsServerProxy gwsServerProxy;
	private final ProjectExplorerModel projectExplorerModel;

	/**
	 * Constructor
	 * 
	 * @param projectExplorerModel model to use to retrieve the input files. Max be null to skip this step
	 */
	public LaunchWizardMenuItem(GwsService serviceToLaunch, GwsServerProxy gwsServerProxy,
			ProjectExplorerModel projectExplorerModel) {
		this.serviceToLaunch = serviceToLaunch;
		this.gwsServerProxy = gwsServerProxy;
		this.projectExplorerModel = projectExplorerModel;
	}

	@CanExecute
	public boolean canExecute() {
		return true;
	}

	/**
	 * Open the wizard and launch the service
	 */
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		gwsServerProxy.detailService(serviceToLaunch.id()).ifPresent(detailedService -> {
			GwsServiceConf configuration = detailedService.description();
			if (projectExplorerModel != null) {
				setInputFiles(configuration);
			}

			ServiceLauncher launcher = new ServiceLauncher(serviceToLaunch);
			ContextInitializer.inject(launcher);
			launcher.launch(shell);
		});

	}

	/**
	 * Browse the selected files and set the suitable ones as input files of the service
	 */
	private void setInputFiles(GwsServiceConf configuration) {
		List<IFileInfo> fileInfos = projectExplorerModel.getSelectedNodes().stream()
				.filter(FileInfoNode.class::isInstance)//
				.map(FileInfoNode.class::cast)//
				.map(FileInfoNode::getFileInfo)//
				.toList();
		if (!fileInfos.isEmpty()) {
			fillFileArgument(configuration, IN_FILE, fileInfos);
			fillFileArgument(configuration, FILE, fileInfos);
		}
	}

	/**
	 * Search the argument with the specified type and set the suitable list of files
	 */
	private void fillFileArgument(GwsServiceConf configuration, String argumentType, List<IFileInfo> fileInfos) {
		List<File> inputFiles = new ArrayList<>();
		configuration.getArgumentOfType(argumentType).ifPresent(inputFilesArgument -> {
			DecodedType[] decodedTypes = inputFilesArgument.decodeTypes();
			for (DecodedType decodedType : decodedTypes) {
				Set<ContentType> contentTypes = decodedType.getContentTypes();
				fileInfos.stream()//
						.filter(fileInfo -> contentTypes.contains(fileInfo.getContentType()))//
						.map(IFileInfo::toFile)//
						.forEach(inputFiles::add);
			}
			new ArgumentValueSetter().setFiles(inputFiles, inputFilesArgument);
		});
	}

}