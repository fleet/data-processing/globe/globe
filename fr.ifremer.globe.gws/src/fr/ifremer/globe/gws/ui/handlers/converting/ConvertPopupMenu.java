package fr.ifremer.globe.gws.ui.handlers.converting;

import fr.ifremer.globe.gws.server.service.GwsServiceGroup;
import fr.ifremer.globe.gws.ui.handlers.abstraction.AbstractPopupMenu;
import fr.ifremer.globe.gws.ui.handlers.abstraction.MenuUtils;

/**
 * Generate the "Convert" pop menu to launch GWS services
 */
public class ConvertPopupMenu extends AbstractPopupMenu {

	/**
	 * @return a copy of topGroup by retaining only Convert groups
	 */
	@Override
	protected GwsServiceGroup filterServiceGroup(GwsServiceGroup topGroup) {
		return MenuUtils.filterConvertServiceGroup(topGroup);
	}
}