package fr.ifremer.globe.gws.ui.handlers;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.gws.ContextInitializer;
import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.BadParameterException;

/**
 * Handler to launch a specific GWS service after editing the arguments.
 */
abstract class AbstractLaunchSpecificServiceHandler extends AbstractNodeHandler {

	@Inject
	private GwsServerProxy gwsServerProxy;

	/** GWS service to export files to NVI */
	private GwsService specificService = null;

	/** Name of the service to search in GWS configuration */
	protected abstract String getServiceName();

	/** Provide the ServiceLauncher to execute */
	protected abstract ServiceLauncher getServiceLaucher(GwsService specificService) throws BadParameterException;

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement uiElement) {
		return checkExecution(selectionService, uiElement);
	}

	/**
	 * Checks if the service can be applied to the selection.
	 */
	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		if (specificService == null)
			specificService = gwsServerProxy.findPyatSpecificService(getServiceName()).orElse(null);
		return specificService != null;
	}

	/**
	 * Executes a ServiceLauncher for the specific service.<br>
	 */
	@Execute
	public void execute(ESelectionService selectionService, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		try {
			ServiceLauncher launcher = getServiceLaucher(specificService);
			ContextInitializer.inject(launcher);
			launcher.launch(shell);
		} catch (BadParameterException e) {
			Messages.openErrorMessage("Error with GWS", "Unable to launch the service '" + getServiceName() + "'");
		}
	}

	/**
	 * @return the {@link #gwsServerProxy}
	 */
	public GwsServerProxy getGwsServerProxy() {
		return gwsServerProxy;
	}

}