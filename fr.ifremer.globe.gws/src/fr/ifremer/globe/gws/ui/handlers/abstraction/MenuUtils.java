package fr.ifremer.globe.gws.ui.handlers.abstraction;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import org.eclipse.e4.ui.model.application.ui.menu.MDirectMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenu;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuFactory;

import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceGroup;

/**
 * Utility class to factor the behavior between main menus and context menus
 */
public class MenuUtils {

	/**
	 * @return a copy of topGroup by retaining only Execute groups (non Convert ones)
	 */
	public static GwsServiceGroup filterExecuteServiceGroup(GwsServiceGroup topGroup) {
		GwsServiceGroup result = recursiveFilteringExecuteServiceGroup(topGroup);
		return result != null ? result : topGroup;
	}

	/**
	 * @return a copy of topGroup by retaining only Execute groups
	 */
	private static GwsServiceGroup recursiveFilteringExecuteServiceGroup(GwsServiceGroup topGroup) {
		if (!topGroup.isConvertGroup()) {
			List<GwsServiceGroup> subGroups = topGroup.subGroups().stream()//
					.map(MenuUtils::recursiveFilteringExecuteServiceGroup)//
					.filter(Objects::nonNull)//
					.toList();
			return new GwsServiceGroup(topGroup.name(), topGroup.services(), subGroups);
		}
		return null;
	}

	/**
	 * @return a copy of topGroup by retaining only Convert groups
	 */
	public static GwsServiceGroup filterConvertServiceGroup(GwsServiceGroup topGroup) {

		// Getting convert group among subgroups of topGroup
		Optional<GwsServiceGroup> convertGroup = filteringConvertServiceGroup(topGroup);
		// Browse all subgroups and get all convert among subgroups
		List<GwsServiceGroup> filteredSubGroup = topGroup.subGroups().stream() //
				.map(MenuUtils::filteringConvertServiceGroup) //
				.filter(Optional::isPresent) //
				.map(Optional::get) //
				.toList();
		return new GwsServiceGroup("Convert", convertGroup.map(GwsServiceGroup::services).orElse(List.of()),
				filteredSubGroup);
	}

	/**
	 * @return a copy of topGroup by retaining only Convert groups
	 */
	private static Optional<GwsServiceGroup> filteringConvertServiceGroup(GwsServiceGroup topGroup) {
		List<GwsServiceGroup> subGroups = topGroup.subGroups().stream()//
				.filter(GwsServiceGroup::isConvertGroup) //
				.filter(convertGroup -> !convertGroup.services().isEmpty()) //
				.toList();

		Optional<GwsServiceGroup> result = Optional.empty();
		if (subGroups.size() == 1)
			result = Optional.of(new GwsServiceGroup(topGroup.name(), subGroups.get(0).services(), List.of()));
		else if (subGroups.size() > 1)
			result = Optional.of(new GwsServiceGroup(topGroup.name(), List.of(), subGroups));

		return result;
	}

	/**
	 * Browse the specified hierarchy of service and generates menu items for each service
	 */
	public static void createsMenu(List<MMenuElement> items, GwsServiceGroup serviceGroup, boolean isRoot,
			Function<GwsService, Object> handlerFactory) {
		if (serviceGroup.isDeprecatedGroup()) // Skip deprecated group
			return;

		List<MMenuElement> serviceMenuList = items;
		if (!isRoot) {
			MMenu popupMenu = MMenuFactory.INSTANCE.createMenu();
			popupMenu.setLabel(serviceGroup.name());
			popupMenu.setIconURI("platform:/plugin/fr.ifremer.globe.ui/icons/16/run_exc.png");
			items.add(popupMenu);
			serviceMenuList = popupMenu.getChildren();
		}
		// Sub menu
		for (GwsServiceGroup subGroup : serviceGroup.subGroups())
			createsMenu(serviceMenuList, subGroup, false, handlerFactory);

		// List of service
		for (GwsService service : serviceGroup.services()) {
			MDirectMenuItem dynamicItem = MMenuFactory.INSTANCE.createDirectMenuItem();
			dynamicItem.setLabel(service.name());
			dynamicItem.setIconURI("platform:/plugin/fr.ifremer.globe.ui/icons/16/run_exc.png");
			dynamicItem.setObject(handlerFactory.apply(service));
			serviceMenuList.add(dynamicItem);
		}
	}

	/**
	 * Constructor
	 */
	private MenuUtils() {
	}

}
