package fr.ifremer.globe.gws.ui.handlers.abstraction;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.ui.di.AboutToShow;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.GwsServiceGroup;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

/**
 * Template method to generate a pop menu to launch services
 */
public abstract class AbstractPopupMenu {

	@Inject
	private GwsServerProxy gwsServerProxy;

	/** Possible service sorted by acceptable input file type */
	private Map<ContentType, GwsServiceGroup> hierarchyByContentType = new EnumMap<>(ContentType.class);

	/**
	 * @return a copy of topGroup by retaining only managed groups
	 */
	protected abstract GwsServiceGroup filterServiceGroup(GwsServiceGroup topGroup);

	/**
	 * After construction, extract all services with input file argument
	 */
	@PostConstruct
	protected void postConstruct() {
		initializeHierarchyByContentType();
	}

	/** Builds a hierarchy by keeping only services that accept input files of the specified type */
	private GwsServiceGroup filterHirarchy(ContentType contentType, GwsServiceGroup rootGroup) {
		List<GwsService> services = filterService(contentType, rootGroup.services());
		List<GwsServiceGroup> children = rootGroup.subGroups().stream()
				.map(childGroup -> filterHirarchy(contentType, childGroup))//
				.toList();

		return new GwsServiceGroup(rootGroup.name(), services, children);
	}

	/** Filter the list of service keeping only the ones that accept input files of the specified type */
	private List<GwsService> filterService(ContentType contentType, List<GwsService> services) {
		List<GwsService> result = new LinkedList<>();
		for (GwsService service : services) {
			gwsServerProxy.detailService(service.id())//
					.ifPresent(detailedService -> {
						// Search in if one of the DecodedType contains the expected ContentType
						GwsServiceConf serviceConf = detailedService.description();
						serviceConf.getInputFilesArgument()
								.ifPresent(inputArgument -> Arrays.stream(inputArgument.decodeTypes()) //
										.flatMap(decodeType -> decodeType.getContentTypes().stream())//
										.filter(contentType::equals)//
										.findFirst()//
										.ifPresent(c -> result.add(detailedService)));
					});
		}
		return result;

	}

	/**
	 * Generates items before displaying the menu
	 */
	@AboutToShow
	public void aboutToShow(List<MMenuElement> items, ESelectionService selectionService,
			ProjectExplorerModel projectExplorerModel, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		ContentType selectedContentType = getContentType(selectionService);
		gererateMenuItems(items, selectedContentType, projectExplorerModel);

		// Reload services in background (ensure GWS is completely started and menu fully generated)
		shell.getDisplay().asyncExec(this::initializeHierarchyByContentType);
	}

	/**
	 * Generates items for the specified ContentType
	 */
	protected void gererateMenuItems(List<MMenuElement> items, ContentType contentType,
			ProjectExplorerModel projectExplorerModel) {
		GwsServiceGroup rootGroup = hierarchyByContentType.get(contentType);
		if (rootGroup != null)
			for (GwsServiceGroup serviceGroup : rootGroup.subGroups()) {
				MenuUtils.createsMenu(items, serviceGroup, true,
						service -> new LaunchWizardMenuItem(service, gwsServerProxy, projectExplorerModel));
			}
	}

	/**
	 * Retrieve all selected ContentType
	 */
	protected ContentType getContentType(ESelectionService selectionService) {
		if (selectionService.getSelection() instanceof StructuredSelection structuredSelection) {
			for (Object selectionItem : structuredSelection.toArray()) {
				if (selectionItem instanceof FileInfoNode fileInfoNode) {
					if (fileInfoNode.getFileInfo() != null) {
						ContentType contentType = fileInfoNode.getFileInfo().getContentType();
						if (contentType != ContentType.UNDEFINED) {
							return contentType;
						}
					}
				}
			}
		}
		return ContentType.UNDEFINED;
	}

	/** Loads services and fill hierarchyByContentType Map */
	private void initializeHierarchyByContentType() {
		gwsServerProxy.findToolboxServicesGroup().ifPresent(topGroup -> {
			GwsServiceGroup rootGroup = filterServiceGroup(topGroup);

			for (ContentType contentType : ContentType.values()) {
				GwsServiceGroup filteredGroup = filterHirarchy(contentType, rootGroup);

				// Some services are missing. GWS is not fully started
				hierarchyByContentType.put(contentType, filteredGroup);
			}
		});
	}

}