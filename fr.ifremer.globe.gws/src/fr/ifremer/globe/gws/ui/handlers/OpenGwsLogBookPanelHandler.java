
package fr.ifremer.globe.gws.ui.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.runtime.gws.GwsBootstrapService;
import fr.ifremer.globe.core.runtime.gws.event.GwsState;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.utils.Messages;
import jakarta.inject.Named;

/** Handler of the log book */
public class OpenGwsLogBookPanelHandler {

	/** Id of the toolbox in fragment */
	private static String partId = "fr.ifremer.globe.gws.partdescriptor.logbook";

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, EPartService partService,
			MApplication application, EModelService modelService, GwsBootstrapService gwsBootstrapService) {

		if (gwsBootstrapService.getServerState() != GwsState.RUNNING) {
			Messages.openErrorMessage("Toolbox server not yet available",
					"The toolbox server is still starting up.\nTry later.");
			return;
		}

		MPart viewPart = PartManager.createPart(partService, partId);
		PartManager.addPartToStack(viewPart, application, modelService, PartManager.RIGHT_BOTTOM_STACK_ID);
		PartUtil.showPartSashContainer(viewPart);
		PartManager.showPart(partService, viewPart);
	}

}