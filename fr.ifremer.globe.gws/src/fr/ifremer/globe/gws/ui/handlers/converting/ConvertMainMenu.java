package fr.ifremer.globe.gws.ui.handlers.converting;

import fr.ifremer.globe.gws.server.service.GwsServiceGroup;
import fr.ifremer.globe.gws.ui.handlers.abstraction.AbstractMainMenu;
import fr.ifremer.globe.gws.ui.handlers.abstraction.MenuUtils;

/**
 * Fill the "Convert" menu with GWS services
 */
public class ConvertMainMenu extends AbstractMainMenu {

	/**
	 * @return a copy of topGroup by retaining only Convert groups
	 */
	@Override
	protected GwsServiceGroup filterServiceGroup(GwsServiceGroup topGroup) {
		return MenuUtils.filterConvertServiceGroup(topGroup);
	}
}