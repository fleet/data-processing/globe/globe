package fr.ifremer.globe.gws.ui.handlers;

import java.io.File;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.runtime.gws.GwsBootstrapService;
import fr.ifremer.globe.core.runtime.gws.event.GwsState;
import fr.ifremer.globe.gws.preferences.GwsPreferences;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.utils.Messages;
import jakarta.inject.Named;

/** Handler of the toolbox */
public class OpenGwsToolboxPanelHandler {

	/** Id of the toolbox in fragment */
	private static String partId = "fr.ifremer.globe.gws.partdescriptor.gwstoolbox";

	/** Opening the toolbox */
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, EPartService partService,
			MApplication application, EModelService modelService, GwsPreferences preferences,
			GwsBootstrapService gwsBootstrapService) {

		if (gwsBootstrapService.getServerState() != GwsState.RUNNING) {
			Messages.openErrorMessage("Toolbox server error",
					"The toolbox server cannot be reached.\nCheck the configuration (Windows -> Preferences -> '"
							+ GwsPreferences.NAME + "') and restart the toolbox manually from the status bar.");
			return;
		}

		File condaDir = preferences.getCondaDirPath().toFile();
		if (!condaDir.exists() || !condaDir.isDirectory()) {
			Messages.openInfoMessage("Invalid conda directory",
					"Please, select a valid Conda directory in 'Windows -> Preferences -> " + GwsPreferences.NAME
							+ "'.");
			return;
		}

		MPart viewPart = PartManager.createPart(partService, partId);
		PartManager.addPartToStack(viewPart, application, modelService, PartManager.RIGHT_STACK_ID);
		PartManager.showPart(partService, viewPart);
	}

}
