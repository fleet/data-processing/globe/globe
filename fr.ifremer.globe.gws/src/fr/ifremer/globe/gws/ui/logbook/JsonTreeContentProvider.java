package fr.ifremer.globe.gws.ui.logbook;

import java.util.Map.Entry;

import org.eclipse.jface.viewers.ITreeContentProvider;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

/**
 * ITreeContentProvider to browse a JsonElement
 */
public class JsonTreeContentProvider implements ITreeContentProvider {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof JsonElement jsonElement) {
			if (jsonElement.isJsonArray()) {
				return jsonElement.getAsJsonArray().asList().toArray();
			} else if (jsonElement.isJsonObject()) {
				return jsonElement.getAsJsonObject().entrySet().toArray();
			}
		} else if (parentElement instanceof Entry<?, ?> entry) {
			Object value = entry.getValue();
			if (value instanceof JsonPrimitive primitive)
				return new Object[] { primitive };
			return getChildren(value);
		}
		return new Object[0];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getParent(Object element) {
		return null; // parent can't be computed.
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasChildren(Object element) {
		Object[] children = getChildren(element);
		return children != null && children.length > 0;
	}

}
