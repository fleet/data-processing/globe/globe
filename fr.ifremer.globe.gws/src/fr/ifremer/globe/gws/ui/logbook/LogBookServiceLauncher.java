package fr.ifremer.globe.gws.ui.logbook;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;

import fr.ifremer.globe.gws.ContextInitializer;
import fr.ifremer.globe.gws.server.logbook.GwsLogBookEvent;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.JsonArgumentDeserializer;
import fr.ifremer.globe.gws.ui.wizard.WizardAdapter;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/** Redefinition of ServiceLauncher to launch a Wizard managing Relaunch and Relaunch... capabilities */
public class LogBookServiceLauncher extends ServiceLauncher {
	private final GwsLogBookEvent logBookEvent;

	/** True to display only the summary */
	private boolean firstPageIsSummary;

	/** Set to true when user click on Run... button to edit the parameters */
	private boolean editParametersRequired;

	/**
	 * Constructor
	 */
	public LogBookServiceLauncher(GwsLogBookEvent logBookEvent) {
		this(logBookEvent, true);
	}

	/**
	 * Constructor
	 */
	public LogBookServiceLauncher(GwsLogBookEvent logBookEvent, boolean firstPageIsSummary) {
		super(logBookEvent.service());
		this.logBookEvent = logBookEvent;
		this.firstPageIsSummary = firstPageIsSummary;
		this.editParametersRequired = !firstPageIsSummary;
		ContextInitializer.inject(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void launch(Shell shell) {
		super.launch(shell);

		if (editParametersRequired) {
			// Switch to normal behavior
			firstPageIsSummary = false;
			super.launch(shell);
		}
	}

	/**
	 * Serialize the parameters before opening the wizard
	 */
	@Override
	protected void restoreArguments(GwsServiceConf configuration, File argumentFile) throws GException {
		loadArgumentsIntoConfiguration(logBookEvent, configuration);
	}

	@Override
	protected boolean openWizard(WizardAdapter wizardAdapter) {
		// Output files are maintained
		wizardAdapter.setComputeOutputFiles(false);

		// Button id used to close the wizard (Window.OK for Relaunch, IDialogConstants.CLIENT_ID for editing the
		// parameters, or Window.Cancel)
		int returnCode = wizardAdapter.openWizard(firstPageIsSummary);
		editParametersRequired = returnCode == IDialogConstants.CLIENT_ID;
		return returnCode == Window.OK;
	}

	/**
	 * Utility method to restore arguments of event in the specified configuration
	 */
	public static void loadArgumentsIntoConfiguration(GwsLogBookEvent logBookEvent, GwsServiceConf configuration)
			throws GException {
		File argumentFile = null;
		try {
			argumentFile = TemporaryCache.createTemporaryFile("log_book_", ".json");
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			argumentFile.getParentFile().mkdirs();
			try (JsonWriter writer = gson.newJsonWriter(new FileWriter(argumentFile))) {
				gson.toJson(logBookEvent.parameters(), writer);
			}
			if (argumentFile.exists())
				new JsonArgumentDeserializer().deserializeValues(configuration, argumentFile, true);
		} catch (IOException e) {
			throw GIOException.wrap(e.getMessage(), e);
		} finally {
			FileUtils.deleteQuietly(argumentFile);
		}
	}

}