package fr.ifremer.globe.gws.ui.logbook;

import java.util.Map.Entry;

import org.eclipse.jface.viewers.LabelProvider;

import com.google.gson.JsonPrimitive;

/**
 * LabelProvider to display a JsonElement provided by JsonTreeContentProvider
 */
public class JsonLabelProvider extends LabelProvider {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getText(Object element) {

		if (element instanceof Entry<?, ?> entry) {
			return entry.getKey().toString();
		} else if (element instanceof JsonPrimitive primitive)
			return primitive.getAsString();
		return element.toString();
	}

}
