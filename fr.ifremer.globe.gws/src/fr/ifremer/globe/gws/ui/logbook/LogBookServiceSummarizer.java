package fr.ifremer.globe.gws.ui.logbook;

import java.io.File;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.gws.ContextInitializer;
import fr.ifremer.globe.gws.server.logbook.GwsLogBookEvent;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.ui.wizard.WizardAdapter;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import fr.ifremer.globe.utils.exception.GException;

/** Redefinition of ServiceLauncher to launch a Wizard managing Relaunch and Relaunch... capabilities */
public class LogBookServiceSummarizer extends ServiceLauncher {
	private final GwsLogBookEvent logBookEvent;

	/** Page summary aggregation */
	private String summary;

	/**
	 * Constructor
	 */
	public LogBookServiceSummarizer(GwsLogBookEvent logBookEvent) {
		this(logBookEvent, true);
	}

	/**
	 * Constructor
	 */
	public LogBookServiceSummarizer(GwsLogBookEvent logBookEvent, boolean firstPageIsSummary) {
		super(logBookEvent.service());
		this.logBookEvent = logBookEvent;
		ContextInitializer.inject(this);
	}

	/**
	 * Serialize the parameters before opening the wizard
	 */
	@Override
	protected void restoreArguments(GwsServiceConf configuration, File argumentFile) throws GException {
		LogBookServiceLauncher.loadArgumentsIntoConfiguration(logBookEvent, configuration);
	}

	/** Prepare the wizard, aggregate the pages summary and set the result to summary attribute */
	@Override
	protected boolean openWizard(WizardAdapter wizardAdapter) {
		// Output files are maintained
		wizardAdapter.setComputeOutputFiles(false);

		// Aggregate pages summary
		StringBuilder sb = new StringBuilder();
		Wizard wizard = wizardAdapter.buildWizard(false);
		for (IWizardPage wizardPage : wizard.getPages())
			if (wizardPage instanceof ISummarizableWizardPage page) {
				String pageSummary = page.summarize();
				if (pageSummary != null && !pageSummary.isEmpty()) {
					sb.append(page.getTitle()).append("\n");
					for (String argument : pageSummary.split("\n")) {
						if (!argument.isEmpty())
							sb.append(" + ").append(argument).append("\n");
					}
				}
			}

		if (!sb.isEmpty()) {
			// substring to remove last \n
			this.summary = sb.substring(0, sb.length() - 1);
		}

		return false; // Do not launch process
	}

	public String getSummary() {
		return summary;
	}

}