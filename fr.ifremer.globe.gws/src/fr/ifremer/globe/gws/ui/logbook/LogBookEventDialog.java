package fr.ifremer.globe.gws.ui.logbook;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.time.Duration;
import java.time.LocalTime;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.logbook.GwsEnvVar;
import fr.ifremer.globe.gws.server.logbook.GwsLogBookEvent;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.utils.date.DateUtils;

public class LogBookEventDialog extends Dialog {
	/** Displayed event */
	private final GwsLogBookEvent logBookEvent;

	private Text startTxt;
	private Text completeTxt;
	private Text elapsedTxt;
	private Text errorTxt;
	private TableViewer envTV;
	private TreeViewer paramTreeViewer;
	private Text logText;

	/**
	 * Create the dialog.
	 */
	public LogBookEventDialog(Shell parentShell, GwsLogBookEvent logBookEvent) {
		super(parentShell);
		setShellStyle(SWT.RESIZE | SWT.TITLE | SWT.CLOSE);
		this.logBookEvent = logBookEvent;
	}

	/**
	 * Create contents of the dialog.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout gridLayout = (GridLayout) container.getLayout();
		gridLayout.numColumns = 2;

		Label lblStart = new Label(container, SWT.NONE);
		lblStart.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblStart.setText("Started :");

		startTxt = new Text(container, SWT.BORDER);
		startTxt.setEditable(false);
		startTxt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblCompleted = new Label(container, SWT.NONE);
		lblCompleted.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCompleted.setText("Completed :");

		completeTxt = new Text(container, SWT.BORDER);
		completeTxt.setEditable(false);
		completeTxt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblElapseTime = new Label(container, SWT.NONE);
		lblElapseTime.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblElapseTime.setText("Elapsed time :");

		elapsedTxt = new Text(container, SWT.BORDER);
		elapsedTxt.setEditable(false);
		elapsedTxt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		if (logBookEvent.error() != null && !logBookEvent.error().isEmpty()) {
			Group grpError = new Group(container, SWT.NONE);
			grpError.setText("Error");
			grpError.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
			grpError.setLayout(new GridLayout(1, false));

			errorTxt = new Text(grpError, SWT.WRAP);
			errorTxt.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			errorTxt.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
			errorTxt.setEditable(false);
		}

		TabFolder tabFolder = new TabFolder(container, SWT.NONE);
		GridData gdTabFolder = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		gdTabFolder.minimumHeight = 200;
		tabFolder.setLayoutData(gdTabFolder);

		TabItem tbtmParameters = new TabItem(tabFolder, SWT.NONE);
		tbtmParameters.setText("Parameters");

		paramTreeViewer = new TreeViewer(tabFolder, SWT.BORDER);
		Tree paramTree = paramTreeViewer.getTree();
		tbtmParameters.setControl(paramTree);
		paramTreeViewer.setContentProvider(new JsonTreeContentProvider());
		paramTreeViewer.setLabelProvider(new JsonLabelProvider());

		TabItem tbtmEnv = new TabItem(tabFolder, SWT.NONE);
		tbtmEnv.setText("Environment");

		Composite envComposite = new Composite(tabFolder, SWT.NONE);
		TableColumnLayout envColumnLayout = new TableColumnLayout();
		envComposite.setLayout(envColumnLayout);
		tbtmEnv.setControl(envComposite);

		envTV = new TableViewer(envComposite, SWT.BORDER | SWT.FULL_SELECTION);
		TableViewerColumn tvcVar = new TableViewerColumn(envTV, SWT.NONE);
		tvcVar.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((GwsEnvVar) element).variable();
			}
		});
		TableColumn tblclmnVar = tvcVar.getColumn();
		tblclmnVar.setText("Variable");
		envColumnLayout.setColumnData(tblclmnVar, new ColumnWeightData(1, 50));

		TableViewerColumn tvcVarVal = new TableViewerColumn(envTV, SWT.NONE);
		tvcVarVal.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((GwsEnvVar) element).value();
			}
		});
		TableColumn tblclmnVarVal = tvcVarVal.getColumn();
		tblclmnVarVal.setText("Value");
		envColumnLayout.setColumnData(tblclmnVarVal, new ColumnWeightData(4, 100));

		envTV.setContentProvider(ArrayContentProvider.getInstance());

		TabItem tbtmMessages = new TabItem(tabFolder, SWT.NONE);
		tbtmMessages.setText("Messages");

		logText = new Text(tabFolder, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI);
		logText.setText("No message");
		tbtmMessages.setControl(logText);

		init();

		return container;
	}

	private void init() {
		startTxt.setText(logBookEvent.start().format(DateUtils.DATE_TIME_FORMATTER));
		if (logBookEvent.end() != null) {
			completeTxt.setText(logBookEvent.end().format(DateUtils.DATE_TIME_FORMATTER));
			Duration elapsed = Duration.between(logBookEvent.start(), logBookEvent.end());
			elapsedTxt.setText(DateUtils.TIME_FORMATTER.format(LocalTime.ofNanoOfDay(elapsed.toNanos())));
		}
		if (errorTxt != null)
			errorTxt.setText(logBookEvent.error());

		paramTreeViewer.setInput(logBookEvent.parameters());
		paramTreeViewer.expandAll();
		envTV.setInput(logBookEvent.environments());

		if (logBookEvent.logs() != null)
			logText.setText(logBookEvent.logs());
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button exportButton = createButton(parent, IDialogConstants.CLIENT_ID, "Export", false);
		exportButton.setImage(Icons.EXPORT.toImage());
		Button launchButton = createButton(parent, IDialogConstants.CLIENT_ID + 1, "Launch...", false);
		launchButton.setImage(Icons.RUN.toImage());
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}

	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.CLIENT_ID)
			export();
		if (buttonId == IDialogConstants.CLIENT_ID + 1)
			launch();
		else
			super.buttonPressed(buttonId);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(600, 600);
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText(logBookEvent.description());
	}

	/** Export the report to a json file */
	private void export() {
		FileDialog dialog = new FileDialog(getShell());
		dialog.setText("Select the report file");
		dialog.setFilterNames(new String[] { "JSON (*.json)" });
		dialog.setFilterExtensions(new String[] { "*.json" });
		dialog.setFileName(
				"report_" + logBookEvent.description().replaceAll("\\W+", "_") + "_" + logBookEvent.id() + ".json");

		String reportFilepath = dialog.open();
		if (reportFilepath != null) {
			try (FileOutputStream fos = new FileOutputStream(reportFilepath)) {
				InputStream report = GwsServerProxy.grab().reportLogBookEvent(logBookEvent.id()).orElse(null);
				if (report != null)
					report.transferTo(fos);
				else
					Messages.openErrorMessage("Report generation error", "Error while getting report from server");
			} catch (Exception e) {
				Messages.openErrorMessage("Report generation error",
						"Error while writing the report :\n" + e.getMessage());
			}
		}
	}

	/** Export the report to a json file */
	private void launch() {
		Shell currentShell = getShell();
		currentShell.getDisplay().asyncExec(() -> {
			LogBookServiceLauncher serviceLauncher = new LogBookServiceLauncher(logBookEvent);
			serviceLauncher.launch(currentShell);
		});
		okPressed();

	}
}
