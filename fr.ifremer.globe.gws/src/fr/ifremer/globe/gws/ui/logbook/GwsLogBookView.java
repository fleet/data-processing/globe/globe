package fr.ifremer.globe.gws.ui.logbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.wb.swt.TableViewerColumnSorter;

import fr.ifremer.globe.gws.ContextInitializer;
import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.job.JobStatus;
import fr.ifremer.globe.gws.server.logbook.GwsLogBookEvent;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

/** Log book view to visualize the history of service execution */
public class GwsLogBookView {

	private final GwsServerProxy gwsServerProxy;

	/** Widgets */
	private final TableViewer tableViewer;
	private final Button btnRun;
	private final Button btnEditAndRun;
	private final Button btnViewReport;
	private final Button btnDeleteSelected;

	private final Color highlightColor;
	/** Thread for scheduling view refreshes */
	private final Timer refreshTimer = new Timer("Logbook Refresh Timer");

	@Inject
	public GwsLogBookView(GwsServerProxy gwsServerProxy, Composite parent) {
		this.gwsServerProxy = ContextInitializer.getInContext(GwsServerProxy.class);

		highlightColor = new Color(255, 230, 230);
		parent.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		parent.setLayout(new GridLayout(1, false));

		Composite actionComposite = new Composite(parent, SWT.NONE);
		actionComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		GridLayout glActionComposite = new GridLayout(6, true);
		glActionComposite.horizontalSpacing = 20;
		glActionComposite.marginWidth = 0;
		actionComposite.setLayout(glActionComposite);

		btnRun = new Button(actionComposite, SWT.NONE);
		btnRun.setEnabled(false);
		btnRun.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				runSelectedEvent(true);
			}
		});
		btnRun.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnRun.setText("Run...");
		btnRun.setImage(Icons.RUN.toImage());
		btnRun.setToolTipText("Run with the same parameters");

		btnEditAndRun = new Button(actionComposite, SWT.NONE);
		btnEditAndRun.setEnabled(false);
		btnEditAndRun.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				runSelectedEvent(false);
			}
		});
		btnEditAndRun.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnEditAndRun.setText("Edit and run...");
		btnEditAndRun.setToolTipText("Modify parameters before running");

		btnViewReport = new Button(actionComposite, SWT.NONE);
		btnViewReport.setEnabled(false);
		btnViewReport.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				showReportOfSelectedEvent();
			}
		});
		btnViewReport.setText("View report");
		btnViewReport.setImage(Icons.FILE.toImage());

		Button btnLoadReport = new Button(actionComposite, SWT.NONE);
		btnLoadReport.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				loadReport();
			}
		});
		btnLoadReport.setText("Load report...");
		btnLoadReport.setToolTipText("Delete all completed jobs");
		btnLoadReport.setImage(Icons.IMPORT.toImage());

		btnDeleteSelected = new Button(actionComposite, SWT.NONE);
		btnDeleteSelected.setEnabled(false);
		btnDeleteSelected.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				deleteSelectedEvent();
			}
		});
		btnDeleteSelected.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnDeleteSelected.setText("Delete");
		btnDeleteSelected.setImage(Icons.DELETE.toImage());

		Button btnDeleteAll = new Button(actionComposite, SWT.NONE);
		btnDeleteAll.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (Messages.openSyncQuestionMessage("Logbook", "Delete all events ? (cannot be undone)", "Delete all",
						"Cancel") == 0)
					deleteAllEvent();
			}
		});
		btnDeleteAll.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnDeleteAll.setText("Delete all");
		btnDeleteAll.setImage(Icons.DELETE.toImage());

		Composite tableComposite = new Composite(parent, SWT.NONE);
		TableColumnLayout tableColumnLayout = new TableColumnLayout();
		tableComposite.setLayout(tableColumnLayout);
		tableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tableViewer = new TableViewer(tableComposite, SWT.BORDER | SWT.FULL_SELECTION);
		tableViewer.addSelectionChangedListener(event -> select(event.getSelection().isEmpty() ? null
				: (GwsLogBookEvent) (event.getStructuredSelection().getFirstElement())));
		Table table = tableViewer.getTable();
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		ColumnViewerToolTipSupport.enableFor(tableViewer);

		TableViewerColumn tvcStatus = new TableViewerColumn(tableViewer, SWT.NONE);
		tvcStatus.setLabelProvider(new LogBookColumnLabelProvider(LogBookColumn.STATUS));
		TableColumn tblclmnStatus = tvcStatus.getColumn();
		tableColumnLayout.setColumnData(tblclmnStatus, new ColumnWeightData(1, 25));
		new LogBookTableViewerColumnSorter(LogBookColumn.STATUS, tvcStatus);

		TableViewerColumn tvcDescription = new TableViewerColumn(tableViewer, SWT.NONE);
		tvcDescription.setLabelProvider(new LogBookColumnLabelProvider(LogBookColumn.DESCRIPTION));
		TableColumn tblclmnDescription = tvcDescription.getColumn();
		tableColumnLayout.setColumnData(tblclmnDescription, new ColumnWeightData(20, 150));
		new LogBookTableViewerColumnSorter(LogBookColumn.DESCRIPTION, tvcDescription);
		tblclmnDescription.setText("Description");

		TableViewerColumn tvcStart = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnStart = tvcStart.getColumn();
		tvcStart.setLabelProvider(new LogBookColumnLabelProvider(LogBookColumn.START_DATE));
		tableColumnLayout.setColumnData(tblclmnStart, new ColumnWeightData(2, 150));
		var initialSorter = new LogBookTableViewerColumnSorter(LogBookColumn.START_DATE, tvcStart);
		tblclmnStart.setText("Start");

		TableViewerColumn tvcEnd = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnEnd = tvcEnd.getColumn();
		tvcEnd.setLabelProvider(new LogBookColumnLabelProvider(LogBookColumn.END_DATE));
		tableColumnLayout.setColumnData(tblclmnEnd, new ColumnWeightData(2, 150));
		new LogBookTableViewerColumnSorter(LogBookColumn.END_DATE, tvcEnd);
		tblclmnEnd.setText("End");

		TableViewerColumn tvcDuration = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn tblclmnDuration = tvcDuration.getColumn();
		tvcDuration.setLabelProvider(new LogBookColumnLabelProvider(LogBookColumn.DURATION));
		tableColumnLayout.setColumnData(tblclmnDuration, new ColumnWeightData(2, 100));
		new LogBookTableViewerColumnSorter(LogBookColumn.DURATION, tvcDuration);
		tblclmnDuration.setText("Duration");

		TableViewerColumn tvcError = new TableViewerColumn(tableViewer, SWT.NONE);
		tvcError.setLabelProvider(new LogBookColumnLabelProvider(LogBookColumn.ERROR));
		TableColumn tblclmnError = tvcError.getColumn();
		tableColumnLayout.setColumnData(tblclmnError, new ColumnWeightData(40, 150));
		new LogBookTableViewerColumnSorter(LogBookColumn.ERROR, tvcError);
		tblclmnError.setText("Error");

		tableViewer.setContentProvider(ArrayContentProvider.getInstance());

		tableViewer.setComparator(initialSorter);
		initialSorter.setSorter(TableViewerColumnSorter.DESC);

		tableViewer.refresh();
		tableViewer.addDoubleClickListener(e -> showReportOfSelectedEvent());

		refreshTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				refreshLogBook();
			}
		}, 1000, 1000);
	}

	@PreDestroy
	private void dispose() {
		refreshTimer.cancel();
		highlightColor.dispose();
	}

	/** Request log book and refresh the view */
	private void refreshLogBook() {
		UIUtils.asyncExecSafety(tableViewer, () -> tableViewer.setInput(gwsServerProxy.getAllLogBookEvent()));
	}

	/** Reacts when selection changed */
	private void select(GwsLogBookEvent logBookEvent) {
		boolean enable = logBookEvent != null && logBookEvent.status() != JobStatus.PENDING
				&& logBookEvent.status() != JobStatus.RUNNING;
		btnRun.setEnabled(enable);
		btnEditAndRun.setEnabled(enable);
		btnViewReport.setEnabled(enable);
		btnDeleteSelected.setEnabled(enable);
	}

	/** Run the selected event */
	private void runSelectedEvent(boolean startWithSummary) {
		var selection = tableViewer.getStructuredSelection();
		if (!selection.isEmpty()) {
			GwsLogBookEvent logBookEvent = (GwsLogBookEvent) selection.getFirstElement();
			BusyIndicator.showWhile(tableViewer.getTable().getDisplay(), () -> requestEvent(logBookEvent, false)//
					.ifPresent(detailedEvent ->
					// Launch a ServiceLauncher to open a wizard with only the summary page
					new LogBookServiceLauncher(detailedEvent, startWithSummary).launch(btnRun.getShell())));
		}
	}

	/** Delete the selected event */
	@SuppressWarnings("unchecked")
	private void deleteSelectedEvent() {
		var selection = tableViewer.getStructuredSelection();
		selection.forEach(event -> {
			GwsLogBookEvent logBookEvent = (GwsLogBookEvent) event;
			gwsServerProxy.deleteLogBookEvent(logBookEvent.id());
		});
		refreshLogBook();
	}

	/** Request GWS to delete all events */
	private void deleteAllEvent() {
		gwsServerProxy.deleteAllLogBookEvent();
		refreshLogBook();
	}

	/** Open a Dialog to display the report of the event */
	private void showReportOfSelectedEvent() {
		var selection = tableViewer.getStructuredSelection();
		if (!selection.isEmpty()) {
			GwsLogBookEvent logBookEvent = (GwsLogBookEvent) selection.getFirstElement();
			BusyIndicator.showWhile(tableViewer.getTable().getDisplay(), () -> {
				GwsLogBookEvent detailedEvent = requestEvent(logBookEvent, false).orElse(null);
				if (detailedEvent != null) {
					LogBookEventDialog dialog = new LogBookEventDialog(btnRun.getShell(), detailedEvent);
					btnRun.getDisplay().asyncExec(dialog::open);
				}
			});
		}
	}

	/** Open a Dialog to display the report of the event of a selected file */
	private void loadReport() {
		FileDialog fileDialog = new FileDialog(btnViewReport.getShell());
		fileDialog.setText("Select the report file");
		fileDialog.setFilterNames(new String[] { "JSON (*.json)" });
		fileDialog.setFilterExtensions(new String[] { "*.json" });
		String reportFilepath = fileDialog.open();
		if (reportFilepath != null) {
			try (FileInputStream fis = new FileInputStream(reportFilepath)) {
				gwsServerProxy.parseReport(fis).ifPresent(logBookEvent -> {
					LogBookEventDialog dialog = new LogBookEventDialog(btnRun.getShell(), logBookEvent);
					btnRun.getDisplay().asyncExec(dialog::open);
				});
			} catch (Exception e) {
				Messages.openErrorMessage("Report parsing error",
						"Error while reading the report :\n" + e.getMessage());
			}
		}
	}

	/** Request event to GWS. Check the result */
	private Optional<GwsLogBookEvent> requestEvent(GwsLogBookEvent logBookEvent, boolean quietly) {
		try {
			Optional<GwsLogBookEvent> detailedEvent = gwsServerProxy.detailLogBookEvent(logBookEvent.id());
			if (detailedEvent.isEmpty()) {
				if (!quietly)
					Messages.openErrorMessage("Log book", "Unable to relaunch the service : no detail available");
				return Optional.empty();
			}

			if (detailedEvent.get().service() == null) {
				if (!quietly)
					Messages.openErrorMessage("Log book", "Unknown service associated to the event. Launch aborted");
				return Optional.empty();
			}
			return detailedEvent;
		} catch (GIOException | IOException e) {
			Messages.openErrorMessage("Log book", "Error while getting logbook event : " + e.getMessage(), e);
			return Optional.empty();
		}
	}

	enum LogBookColumn {
		STATUS,
		DESCRIPTION,
		START_DATE,
		END_DATE,
		DURATION,
		ERROR
	}

	/** Helper for sorting GwsLogBookEvent in a table */
	private class LogBookTableViewerColumnSorter extends TableViewerColumnSorter {

		/** Column managed by this sorter */
		private final LogBookColumn column;

		/**
		 * Constructor
		 */
		private LogBookTableViewerColumnSorter(LogBookColumn column, TableViewerColumn tvc) {
			super(tvc);
			this.column = column;
		}

		@Override
		protected Object getValue(Object element) {
			GwsLogBookEvent event = (GwsLogBookEvent) element;
			return switch (column) {
			case STATUS -> event.status();
			case DESCRIPTION -> event.description() != null ? event.description() : "";
			case START_DATE -> event.start() != null ? event.start() : LocalDateTime.MAX;
			case END_DATE -> event.end() != null ? event.end() : LocalDateTime.MAX;
			case DURATION -> {
				var start = event.start();
				var end = event.end();
				yield (start != null && end != null) ? Duration.between(start, end) : Duration.ZERO;
			}
			case ERROR -> event.error() != null ? event.error() : "";
			default -> super.getValue(element);
			};
		}
	}

	/** ColumnLabelProvider providing display information for GwsLogBookEvent in a table */
	private class LogBookColumnLabelProvider extends ColumnLabelProvider {

		/** Column managed by this provider */
		private final LogBookColumn column;

		/** Constructor */
		private LogBookColumnLabelProvider(LogBookColumn column) {
			this.column = column;
		}

		/** Return the text of the cell */
		@Override
		public String getText(Object element) {
			GwsLogBookEvent event = (GwsLogBookEvent) element;
			return switch (column) {
			case DESCRIPTION -> event.description();
			case START_DATE -> event.start() != null ? event.start().format(DateUtils.DATE_TIME_FORMATTER) : "";
			case END_DATE -> event.end() != null ? event.end().format(DateUtils.DATE_TIME_FORMATTER) : "";
			case DURATION -> {
				var start = event.start();
				var end = event.end();
				if (start != null && end != null) {
					Duration duration = Duration.between(start, end);
					yield DateUtils.TIME_FORMATTER.format(duration.addTo(Instant.EPOCH));
				}
				yield "";
			}
			case ERROR -> event.error();
			default -> "";
			};
		}

		/** Return the image representing the event status */
		@Override
		public Image getImage(Object element) {
			if (column == LogBookColumn.STATUS) {
				JobStatus status = ((GwsLogBookEvent) element).status();
				return switch (status) {
				case PENDING -> Icons.SAND_TIMER.toImage();
				case RUNNING -> Icons.RUN.toImage();
				case ERROR -> Icons.READING_ERROR.toImage();
				case CANCELLED -> Icons.CANCEL.toImage();
				default -> Icons.OK.toImage();
				};
			}
			return super.getImage(element); // Image only for Status column
		}

		/** Background color depending on the event status */
		@Override
		public Color getBackground(Object element) {
			return column != LogBookColumn.STATUS && ((GwsLogBookEvent) element).status() == JobStatus.ERROR
					? highlightColor
					: super.getBackground(element);
		}

		/** text displayed in the tool tip for the event */
		@Override
		public String getToolTipText(Object element) {
			GwsLogBookEvent logBookEvent = (GwsLogBookEvent) element;
			AtomicReference<LogBookServiceSummarizer> summarizer = new AtomicReference<>();
			requestEvent(logBookEvent, true).ifPresent(detailedEvent -> {
				// Launch a ServiceLauncher to open a wizard with only the summary page
				LogBookServiceSummarizer summarizerLauncher = new LogBookServiceSummarizer(detailedEvent);
				summarizer.set(summarizerLauncher);
				summarizerLauncher.launch(btnRun.getShell());
			});

			return summarizer.get() != null ? summarizer.get().getSummary() : "No summary";
		}

	}

}