package fr.ifremer.globe.gws.ui;

import java.util.List;
import java.util.Optional;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gws.ContextInitializer;
import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceGroup;
import fr.ifremer.globe.gws.ui.tree.GwsServiceGroupNode;
import fr.ifremer.globe.gws.ui.tree.GwsServiceNode;
import fr.ifremer.globe.ui.providers.TreeContentProvider;
import fr.ifremer.globe.ui.providers.TreeLabelProvider;
import fr.ifremer.globe.ui.tree.ITreeNode;
import fr.ifremer.globe.ui.utils.UIUtils;

public class GwsToolboxExplorer {

	protected Logger logger = LoggerFactory.getLogger(GwsToolboxExplorer.class);

	@Inject
	private GwsServerProxy gwsServerProxy;

	/** UI components **/
	private Browser browser;
	private Button runButton;
	private TreeViewer treeViewer;
	private Composite buttonComposite;

	/**
	 * Initialization method: creates UI
	 */
	@PostConstruct
	public void createUI(Composite parent) {
		parent.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		parent.setLayout(new GridLayout(3, false));

		// tree view
		Composite treeComposite = new Composite(parent, SWT.NONE);
		treeComposite.setLayout(new FillLayout(SWT.HORIZONTAL));
		GridData gdTreeComposite = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 2);
		gdTreeComposite.widthHint = 350;
		treeComposite.setLayoutData(gdTreeComposite);

		treeViewer = new TreeViewer(treeComposite, SWT.SINGLE);
		treeViewer.setLabelProvider(new TreeLabelProvider());
		treeViewer.setContentProvider(new TreeContentProvider());
		treeViewer.setAutoExpandLevel(2);

		// divider
		Label label = new Label(parent, SWT.SEPARATOR | SWT.VERTICAL);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 2));

		// browser
		browser = new Browser(parent, SWT.NONE);
		browser.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		UIUtils.setVisible(browser, false);

		// button(s)
		buttonComposite = new Composite(parent, SWT.NONE);
		buttonComposite.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, true, false));
		GridLayout glButtonComposite = new GridLayout(1, true);
		glButtonComposite.horizontalSpacing = 10;
		buttonComposite.setLayout(glButtonComposite);
		UIUtils.setVisible(buttonComposite, false);

		runButton = new Button(buttonComposite, SWT.NONE);
		runButton.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.gws", "icons/run_exc.png"));
		runButton.setText("Run...");
		GridData gdRunButton = new GridData(SWT.FILL, SWT.FILL, false, true);
		gdRunButton.widthHint = 200;
		runButton.setLayoutData(gdRunButton);

		String errorMessage = updateTree();
		if (!errorMessage.isEmpty()) {
			browser.setText("<h2>Error occured :<br>" + errorMessage + "</h2>");
			UIUtils.setVisible(browser, true);
		} else
			makeBindings();
	}

	private void makeBindings() {
		treeViewer.getTree().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TreeItem item = (TreeItem) e.item;
				if (item.getData() instanceof GwsServiceNode serviceNode) {
					displayProcess(serviceNode);
				}
			}
		});

		// Wizard
		runButton.addListener(SWT.Selection, e -> runServiceWithWizard());
		treeViewer.addDoubleClickListener(event -> runServiceWithWizard());
	}

	/**
	 * Open the selected service in a wizard
	 */
	private void runServiceWithWizard() {
		getSelectedService().ifPresent(gwsService -> {
			ServiceLauncher launcher = new ServiceLauncher(gwsService);
			ContextInitializer.inject(launcher);
			launcher.launch(browser.getShell());
		});
	}

	/**
	 * Updates the tree with available GWS services.
	 * 
	 * @return the error message if any
	 */
	private String updateTree() {
		GwsServiceGroup rootGroup = gwsServerProxy.findToolboxServicesGroup().orElse(null);
		if (rootGroup != null) {
			if (!rootGroup.services().isEmpty() || !rootGroup.subGroups().isEmpty()) {
				UIUtils.asyncExecSafety(treeViewer, () -> {
					List<ITreeNode> treeNodes = rootGroup.subGroups().stream()
							.map(group -> new GwsServiceGroupNode(treeViewer, group)).map(ITreeNode.class::cast)
							.toList();
					treeViewer.setInput(treeNodes.toArray());
					treeViewer.expandAll();
				});
			} else
				return "no service available on server";
		} else
			return "server not available";
		return "";
	}

	/**
	 * Displays help of a {@link GwsService}
	 */
	private void displayProcess(GwsServiceNode serviceNode) {
		GwsService gwsService = serviceNode.getModel();
		gwsServerProxy.detailService(gwsService.id()).ifPresent(details -> {
			if (details.helpPath() != null && !details.helpPath().isBlank()) {
				browser.setUrl(details.helpPath());
				UIUtils.setVisible(browser, true);
			} else if (details.help() != null) {
				browser.setText(details.help());
				UIUtils.setVisible(browser, true);
			} else
				UIUtils.setVisible(browser, false);

			UIUtils.setVisible(buttonComposite, true);
		});
	}

	/**
	 * @return the service of the the selected GwsServiceNode in the tree
	 */
	private Optional<GwsService> getSelectedService() {
		Optional<GwsService> result = Optional.empty();
		IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
		if (!selection.isEmpty() && selection.getFirstElement() instanceof GwsServiceNode serviceNode)
			result = Optional.ofNullable(serviceNode.getModel());
		return result;
	}

}