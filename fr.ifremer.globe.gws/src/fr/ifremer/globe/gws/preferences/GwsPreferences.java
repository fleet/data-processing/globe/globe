package fr.ifremer.globe.gws.preferences;

import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.GlobeVersion;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.DirectoryPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;

public class GwsPreferences extends PreferenceComposite {

	protected static Logger logger = LoggerFactory.getLogger(GwsPreferences.class);

	/** Nane of the preference node */
	public static final String NAME = "Toolbox";

	/** Default preferences **/
	private static final Path CONDA_PATH = Path.of(".", "miniconda");

	private BooleanPreferenceAttribute launchServerAtStartup;
	private StringPreferenceAttribute condaEnv;
	private DirectoryPreferenceAttribute condaDirPath;

	/**
	 * Constructor
	 */
	public GwsPreferences(PreferenceComposite father) {
		super(father, NAME);

		launchServerAtStartup = new BooleanPreferenceAttribute("launchServerAtStartup", "Launch server at startup",
				true);
		condaDirPath = new DirectoryPreferenceAttribute("condaPath", "Conda directory path", CONDA_PATH);
		condaEnv = new StringPreferenceAttribute("convaEnv", "Conda environment", CONDA_PATH.toString());

		declareAttribute(launchServerAtStartup);
		declareAttribute(condaEnv);
		declareAttribute(condaDirPath);

		load();
	}

	@Override
	public void load() {
		// Check if a new version of Globe have been installed
		String globeVersionNode = PreferenceRegistry.getGlobeVersion();
		if (GlobeVersion.VERSION.equals(globeVersionNode)) {
			logger.info("Loading Conda/Pyat settings from preferences");
			super.load();
		} else {
			logger.info("Fresh install of Globe : using defaut GWS settings");
		}
	}

	/**
	 * @return the {@link #condaEnv}
	 */
	public String getCondaEnv() {
		return condaEnv.getValue();
	}

	/**
	 * @return the {@link #condaDirPath}
	 */
	public Path getCondaDirPath() {
		return condaDirPath.getValue();
	}

	public DirectoryPreferenceAttribute getCondaDirPathPref() {
		return condaDirPath;
	}

	/**
	 * @return the {@link #launchServerAtStartup}
	 */
	public boolean getLaunchServerAtStartup() {
		return launchServerAtStartup.getValue();
	}

}
