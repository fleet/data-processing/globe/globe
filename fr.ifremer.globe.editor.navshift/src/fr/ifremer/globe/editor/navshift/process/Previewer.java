/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.process;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.IntRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.model.dtm.SounderDataDtmGeoboxComputer;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.model.navigation.utils.NavigationApplier;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.commons.SounderNcInfoUtility;
import fr.ifremer.globe.editor.navshift.controller.IsobathController;
import fr.ifremer.globe.editor.navshift.model.INavigationDataUpdater;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.impl.NavigationSupplierWrapper;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Processing class of preview
 */
@Creatable
public class Previewer {

	/** Factory of {@link IArray} */
	@Inject
	protected IArrayFactory arrayFactory;

	/** CycleContainer service */
	@Inject
	protected IDataContainerFactory dataContainerFactory;

	/** NviInfoSupplier service */
	@Inject
	private INavigationService navigationService;

	/** Session to preview */
	@Inject
	@Optional
	@Named(ContextNames.SESSION_CONTEXT)
	protected ISessionContext sessionContext;

	/** Temporary folder */
	@Inject
	@Optional
	@Named(ContextNames.TMP_FOLDER)
	protected File tmpDir;

	/**
	 * Mercator projection.
	 */
	@Inject
	@Optional
	@Named(ContextNames.MERCATOR_PROJECTION)
	protected Projection projection;

	@Inject
	@Optional
	@Named(ContextNames.ISOBATH_CONTROLLER)
	protected IsobathController isobathController;

	/** Writer of NVI files. */
	@Inject
	protected INviWriter nviWriter;

	/** Resulting shifted navigation */
	protected INavigationModel shiftedNavigationData;
	/** Resulting GeoBox */
	protected GeoBox geoBox;
	/** Resulting DEM files */
	protected List<File> shiftedSounderFiles = new ArrayList<>();

	/** Constructor */
	public Previewer() {
		super();
	}

	/** Generating isobaths for the preview mode */
	public void process(IProgressMonitor monitor) throws IOException, GException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, "Preparing preview", 100);
		// Clean previous preview
		dispose();
		getWorkingFolder().mkdirs();

		// Shifting
		shiftedNavigationData = shiftNav(subMonitor.split(5));
		// Exporting NVI
		INavigationDataSupplier navigationDataSupplier = saveNvi(subMonitor.split(10));

		// Copy Mbg files
		Map<IProfile, File> sounderfiles = duplicateSounderFiles(subMonitor.split(15));
		shiftedSounderFiles = new ArrayList<>(sounderfiles.values());

		// Apply the new nav
		applyNav(sounderfiles, navigationDataSupplier, subMonitor.split(25));
		// Compute the BathyDtmModel
		List<ISounderNcInfo> sounderNcInfos = computeSounderNcInfo(sounderfiles);
		geoBox = computeGeobox(sounderNcInfos);

		// Generate isobath layers
		isobathController.generatePreviewIsobaths(subMonitor.split(5), this, sounderNcInfos);
	}

	/**
	 * Shift the navigation.
	 */
	protected INavigationModel shiftNav(IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		NavShifter shifter = new NavShifter();

		shifter.prepare(sessionContext.getNavigationDataSupplier(), sessionContext.getShiftPeriod(),
				subMonitor.split(10));
		INavigationDataUpdater shiftedNavigationModel = sessionContext.getNavigationDataSupplier()
				.duplicate(arrayFactory, subMonitor.split(20));
		shifter.shift(shiftedNavigationModel, subMonitor.split(70));
		return shiftedNavigationModel;

	}

	/**
	 * Shift the navigation.
	 */
	protected INavigationDataSupplier saveNvi(IProgressMonitor monitor) throws GException {
		File workingFolder = getWorkingFolder();
		File nviFile = new File(workingFolder, "preview.nvi");
		nviWriter.write(new NavigationSupplierWrapper(shiftedNavigationData), nviFile.getAbsolutePath());
		monitor.done();
		return navigationService.getNavigationDataProvider(nviFile.getAbsolutePath())
				.orElseThrow(() -> new GIOException("New NVI file not loadable: " + nviFile.getName()));
	}

	/**
	 * Collect profiles included in the shifd period.
	 */
	protected List<IProfile> collectProfiles() {
		IShiftPeriod shiftPeriod = sessionContext.getShiftPeriod();
		IntRange periodRange = shiftPeriod.getIndexRange();
		return sessionContext.getProfiles().stream()
				.filter(profile -> periodRange.overlapsRange(profile.getIndexRange())).collect(Collectors.toList());
	}

	/**
	 * Copy all files in a temporary folder.
	 */
	protected Map<IProfile, File> duplicateSounderFiles(IProgressMonitor monitor) throws IOException {
		IntRange shiftIndexRange = sessionContext.getShiftPeriod().getIndexRange();

		// keep only profiles within the shift profile
		List<IProfile> profiles = sessionContext.getProfiles().stream()
				.filter(p -> p.getIndexRange().containsRange(shiftIndexRange)
						|| shiftIndexRange.containsInteger(p.getFirstIndex())
						|| shiftIndexRange.containsInteger(p.getLastIndex()))
				.collect(Collectors.toList());

		SubMonitor subMonitor = SubMonitor.convert(monitor, profiles.size());
		File workingFolder = getWorkingFolder();
		Map<IProfile, File> result = new HashMap<>();
		for (IProfile profile : profiles) {
			result.put(profile, SounderNcInfoUtility.copy(profile.getSounderNcInfo(), workingFolder));
			subMonitor.worked(1);
		}
		return result;
	}

	/**
	 * Copy all files in a temporary folder.
	 */
	protected void applyNav(Map<IProfile, File> sounderfiles, INavigationDataSupplier nviInfo, IProgressMonitor monitor)
			throws GIOException {
		// apply nav only on profiles covered by IShiftPeriod
		List<IProfile> profiles = collectProfiles();
		SubMonitor subMonitor = SubMonitor.convert(monitor, profiles.size());
		NavigationApplier navigationApplier = new NavigationApplier();
		for (IProfile profile : profiles) {
			File sounderFile = sounderfiles.get(profile);
			java.util.Optional<IFileInfo> infoStore = IFileService.grab().getFileInfo(sounderFile.getAbsolutePath());
			if (infoStore.isPresent() && infoStore.get() instanceof ISounderNcInfo) {
				navigationApplier.apply((ISounderNcInfo) infoStore.get(), nviInfo);
			}
			subMonitor.worked(1);
		}
	}
	
	protected GeoBox computeGeobox(List<ISounderNcInfo> sounderNcInfos) throws GIOException {
		SounderDataDtmGeoboxComputer geoboxComputer = new SounderDataDtmGeoboxComputer(sounderNcInfos);
		return geoboxComputer.computeValidGeobox();
	}

	protected List<ISounderNcInfo> computeSounderNcInfo(Map<IProfile, File> sounderfiles) {
		List<ISounderNcInfo> result = new ArrayList<>();
		for (File sounderfile : sounderfiles.values()) {
			IFileService.grab().getSounderNcInfo(sounderfile).ifPresent(result::add);
		}
		return result;
	}

	/**
	 * Setter of {@link #sessionContext}. Called by DI only
	 */
	@Inject
	private void setSessionContext(@Optional @Named(ContextNames.SESSION_CONTEXT) ISessionContext newSessionContext) {
		dispose();
	}

	/** Free ressources */
	public void dispose() {
		if (shiftedNavigationData != null) {
			shiftedNavigationData.close();
			shiftedNavigationData = null;
		}

		geoBox = null;
		shiftedSounderFiles = new ArrayList<>();
		FileUtils.deleteQuietly(getWorkingFolder());
	}

	/** Working folder */
	public File getWorkingFolder() {
		return new File(tmpDir, "preview");
	}

	/** Getter of {@link #geoBox} */
	public GeoBox getGeoBox() {
		return geoBox;
	}

	/** Getter of {@link #shiftedSounderFiles} */
	public List<File> getShiftedSounderFiles() {
		return shiftedSounderFiles;
	}

}
