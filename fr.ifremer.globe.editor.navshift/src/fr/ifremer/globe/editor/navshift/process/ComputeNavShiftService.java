package fr.ifremer.globe.editor.navshift.process;

import java.io.File;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.controller.IsobathController;
import fr.ifremer.globe.editor.navshift.controller.Viewer2DLayerController;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.impl.ShiftVectorImpl;
import fr.ifremer.globe.editor.navshift.viewer2d.NavShift2DViewer;
import fr.ifremer.globe.gws.ContextInitializer;
import fr.ifremer.globe.gws.server.GwsServerProxy;
import fr.ifremer.globe.gws.server.process.PayloadIntercepter;
import fr.ifremer.globe.gws.server.process.ServiceLauncher;
import fr.ifremer.globe.gws.server.process.ServiceProcess;
import fr.ifremer.globe.gws.server.service.GwsService;
import fr.ifremer.globe.gws.server.service.GwsServiceConf;
import fr.ifremer.globe.gws.server.service.json.ArgumentValueSetter;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.array.impl.LargeArray;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.viewer3d.layers.lineisobaths.IsobathLayer;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 * Compute Navigation Shift GWS service.
 */
public class ComputeNavShiftService extends ServiceLauncher {

	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	@Named(ContextNames.SESSION_CONTEXT)
	private ISessionContext sessionContext;

	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	@Named(ContextNames.ISOBATH_CONTROLLER)
	private IsobathController isobathController;

	/** Controller of all layers displayed in {@link NavShift2DViewer}. */
	@Inject
	@Named(ContextNames.VIEWER_2D_LAYER_CONTROLLER)
	private Viewer2DLayerController viewer2DlayerController;

	/** Temporary folder */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	@Named(ContextNames.TMP_FOLDER)
	private File tmpDir;

	public static final String SERVICE_NAME = "Compute Navigation Shift";

	/**
	 * @return the process
	 */
	public static Optional<ComputeNavShiftService> get() {
		return GwsServerProxy.grab().findService(ComputeNavShiftService.SERVICE_NAME).map(gwsService -> {
			var process = new ComputeNavShiftService(gwsService);
			ContextInitializer.inject(process);
			return process;
		});
	}

	/**
	 * Constructor
	 */
	private ComputeNavShiftService(GwsService specificService) {
		super(specificService);
	}

	/**
	 * Sets process arguments from {@link ISessionContext}.
	 */
	@Override
	protected void aboutToEditArgument(GwsServiceConf configuration) throws GException {
		// Get parameters from current session.
		var shiftPeriod = sessionContext.getShiftPeriod();
		var navData = sessionContext.getNavigationDataSupplier();
		var latitudeFile = ((LargeArray) navData.getLatitudes()).getFile();
		var longitudeFile = ((LargeArray) navData.getLongitudes()).getFile();
		var datetimeFile = ((LargeArray) navData.getTimes()).getFile();
		var refShpFile = viewer2DlayerController.getReferenceDtmIsobathLayer().stream()
				.map(IsobathLayer::getShpContourFile).toList();
		var jsonProfiles = sessionContext.getProfiles().stream()
				.map(p -> new JsonProfile(p.getSounderNcInfo().getBaseName(),
						isobathController.getLayersOfProfile().get(p).getShpContourFile().getAbsolutePath(),
						p.getFirstIndex(), p.getLastIndex()))
				.toList();

		// Configure service arguments with parameters.
		ArgumentValueSetter setter = new ArgumentValueSetter();
		setter.setInstant(Instant.ofEpochMilli(shiftPeriod.getStartTime()),
				configuration.getArgumentOfNameOrThrow("start_period"));
		setter.setInstant(Instant.ofEpochMilli(shiftPeriod.getEndTime()),
				configuration.getArgumentOfNameOrThrow("end_period"));
		setter.setFile(latitudeFile, configuration.getArgumentOfNameOrThrow("lat_file_path"));
		setter.setFile(longitudeFile, configuration.getArgumentOfNameOrThrow("lon_file_path"));
		setter.setFile(datetimeFile, configuration.getArgumentOfNameOrThrow("datetime_file_path"));
		setter.setFiles(refShpFile, configuration.getArgumentOfNameOrThrow("ref_contour_shp_path"));
		setter.setObjects(jsonProfiles, configuration.getArgumentOfNameOrThrow("profiles"));

		super.aboutToEditArgument(configuration);
	}

	/**
	 * Record to send profile to process as JSON.
	 */
	private record JsonProfile(String name, String contourFilePath, int startNavIndex, int endNavIndex) {

	}

	/**
	 * Record to parse shift vectors received as result of the service.
	 */
	private record JsonShiftVector(String datetime, double x, double y) {

	}

	/**
	 * Sets a {@link PayloadIntercepter} to handle compute service result.
	 */
	@Override
	protected void aboutToSetPayloadIntercepter(ServiceProcess process) {
		process.setPayloadIntercepter(PayloadIntercepter.forString((String stringPayload) -> {
			var shiftPeriod = sessionContext.getShiftPeriod();

			/// Read vectors from payload.
			List<JsonShiftVector> jsonShiftVectors = new Gson().fromJson(stringPayload,
					new TypeToken<List<JsonShiftVector>>() {
					}.getType());
			var computedVectors = jsonShiftVectors.stream()//
					.map(jsonVector -> ShiftVectorImpl.fromDrift(shiftPeriod,
							DateUtils.parseIsoDate(jsonVector.datetime), -jsonVector.x, -jsonVector.y))
					.toList();

			if (computedVectors.isEmpty()) {
				Messages.openInfoMessage(SERVICE_NAME, "No shift vector computed.");
				return;
			}

			// Load by adding or replacing current vector.
			if (shiftPeriod.getShiftVectors().isEmpty()) {
				Messages.openInfoMessage(SERVICE_NAME, String.format("%s shift vector%s computed.",
						computedVectors.size(), computedVectors.size() > 1 ? "s" : ""));
				computedVectors.forEach(shiftPeriod::addShiftVector);
			} else {
				var resp = Messages
						.openSyncQuestionMessage(SERVICE_NAME,
								String.format("%s shift vector%s computed.%nAdd to or replace current vectors ?",
										computedVectors.size(), computedVectors.size() > 1 ? "s" : ""),
								"Add", "Replace", true);
				if (resp == 0 || resp == 1) {
					if (resp == 1)
						shiftPeriod.removeAllShiftVector();
					computedVectors.forEach(shiftPeriod::addShiftVector);
				}
			}
		}));
	}

}
