package fr.ifremer.globe.editor.navshift.process;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.editor.navshift.model.INavigationDataUpdater;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.IShiftVector;

/**
 * 
 * @author cguychard
 *
 */
@Creatable
public class NavShifter {

	protected static final Logger logger = LoggerFactory.getLogger(NavShifter.class);

	private static final int TIME_START_IDX = 0;
	private static final int DLONG_IDX = 1;
	private static final int DLAT_IDX = 2;
	private static final int DLONGITUDE_IDX = 3;
	private static final int DLATITUDE_IDX = 4;

	private List<double[]> driftMatrix = null;
	private long shiftPeriodStart = -1;
	private long shiftPeriodEnd = -1;

	/** @throws IllegalArgumentException */
	public void shift(INavigationDataUpdater navModel, IProgressMonitor progress) {

		// Checks
		Assert.isLegal(driftMatrix != null && shiftPeriodStart >= 0 && shiftPeriodEnd >= 0,
				"Invalid data provided, maybe shifting parameters have not been pre-processed");

		if (driftMatrix.isEmpty()) {
			return;
		}

		SubMonitor subMonitor = SubMonitor.convert(progress, navModel.size());

		// Algo
		Iterator<Integer> naviter = navModel.iterator();
		int idx = 0;
		double[] currVec = null;
		double aLat, aLong;

		int maxIndex = driftMatrix.size() - 1;
		int currentVecIdx = -1;
		long aTime = navModel.getTime(0);
		long nextMilestone = shiftPeriodStart;
		long endMilestone = shiftPeriodEnd;

		java.util.TimeZone gmtZone = java.util.TimeZone.getTimeZone("GMT");
		SimpleDateFormat timeFormat = new SimpleDateFormat(" dd/MM/yyyy HH:mm:ss.SSS");
		timeFormat.setTimeZone(gmtZone);

		while (naviter.hasNext() && nextMilestone >= aTime) {
			idx = naviter.next();
			aTime = navModel.getTime(idx);
			if (aTime <= endMilestone) {

				if (nextMilestone <= aTime) {
					currentVecIdx += 1;
					if (currentVecIdx <= maxIndex) {
						currVec = driftMatrix.get(currentVecIdx);
						if (currentVecIdx <= maxIndex - 1) {
							nextMilestone = (long) driftMatrix.get(currentVecIdx + 1)[TIME_START_IDX];
						} else {
							nextMilestone = shiftPeriodEnd;
						}
					} else {
						currVec = null;
					}
				}
				if (currVec != null) {
					aLat = navModel.getLatitude(idx) + currVec[DLATITUDE_IDX]
							+ currVec[DLAT_IDX] * (aTime - currVec[TIME_START_IDX]);
					aLong = navModel.getLongitude(idx) + currVec[DLONGITUDE_IDX]
							+ currVec[DLONG_IDX] * (aTime - currVec[TIME_START_IDX]);
					navModel.updateNavPoint(idx, navModel.getTime(idx), aLong, aLat);
				}
			}
			subMonitor.worked(1);
		}

	}

	/**
	 * pre-process List of Drift Vectors to provide the values needed to process data
	 * 
	 * @throws IllegalArgumentException
	 */
	public void prepare(INavigationModel source, IShiftPeriod period, IProgressMonitor progress) {
		// data checks
		Assert.isLegal(
				source != null && period != null && period.getEndIndex() > period.getStartIndex()
						&& period.getEndTime() > period.getStartTime(),
				"Invalid data provided to the Navigation Shifter processor");

		shiftPeriodStart = period.getStartTime();
		shiftPeriodEnd = period.getEndTime();

		SubMonitor subMonitor = SubMonitor.convert(progress, period.getShiftVectors().size() + 1);

		// go ahead and prepare drift vectors data for processing
		if (driftMatrix == null) {
			driftMatrix = new ArrayList<double[]>();
			IShiftVector previous = null;
			double[] driftVariables = null;
			long DT;

			for (IShiftVector v : period.getOrderedShiftVectors()) {
				driftVariables = new double[5];
				if (previous == null) {
					driftVariables[DLONGITUDE_IDX] = 0.0;
					driftVariables[DLATITUDE_IDX] = 0.0;

					driftVariables[TIME_START_IDX] = shiftPeriodStart;

					DT = v.getTimeOrigin() - shiftPeriodStart; // DT
					if (DT != 0) {
						driftVariables[DLONG_IDX] = (v.getEndLongitude() - v.getOriginLongitude()) / DT; // Dlongitude
						driftVariables[DLAT_IDX] = (v.getEndLatitude() - v.getOriginLatitude()) / DT; // Dlatitude
						driftMatrix.add(driftVariables);
					}
				} else {
					driftVariables[DLONGITUDE_IDX] = previous.getEndLongitude() - previous.getOriginLongitude();
					driftVariables[DLATITUDE_IDX] = previous.getEndLatitude() - previous.getOriginLatitude();

					driftVariables[TIME_START_IDX] = previous.getTimeOrigin();

					DT = v.getTimeOrigin() - previous.getTimeOrigin(); // DT
					if (DT != 0) {
						driftVariables[DLONG_IDX] = (v.getEndLongitude() - v.getOriginLongitude()
								- previous.getEndLongitude() + previous.getOriginLongitude()) / DT; // Dlongitude
						driftVariables[DLAT_IDX] = (v.getEndLatitude() - v.getOriginLatitude()
								- previous.getEndLatitude() + previous.getOriginLatitude()) / DT; // Dlongitude
						driftMatrix.add(driftVariables);
					}

				}
				previous = v;
				subMonitor.worked(1);
			}
			if (previous != null) {
				driftVariables = new double[5];

				driftVariables[DLONGITUDE_IDX] = previous.getEndLongitude() - previous.getOriginLongitude();
				driftVariables[DLATITUDE_IDX] = previous.getEndLatitude() - previous.getOriginLatitude();

				driftVariables[TIME_START_IDX] = previous.getTimeOrigin();

				DT = shiftPeriodEnd - previous.getTimeOrigin(); // DT
				if (DT != 0) {
					driftVariables[DLONG_IDX] = (previous.getOriginLongitude() - previous.getEndLongitude()) / DT; // Dlongitude
					driftVariables[DLAT_IDX] = (previous.getOriginLatitude() - previous.getEndLatitude()) / DT; // Dlongitude
				}
				driftMatrix.add(driftVariables);
			}
			subMonitor.worked(1);
		}
	}

	/**
	 * resets internal state
	 */

	public void reset() {
		driftMatrix = null;
		shiftPeriodStart = -1;
		shiftPeriodEnd = -1;
	}
}