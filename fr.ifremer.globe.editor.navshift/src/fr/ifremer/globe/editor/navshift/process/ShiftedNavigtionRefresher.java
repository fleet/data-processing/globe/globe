/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.process;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.application.event.NavigationLineInfos;
import fr.ifremer.globe.editor.navshift.commons.NavShiftPreference;
import fr.ifremer.globe.editor.navshift.model.INavigationDataUpdater;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.viewer3d.Viewer3D;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

/**
 * Trigger the update of shifted navigation
 */
public class ShiftedNavigtionRefresher {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(ShiftedNavigtionRefresher.class);

	/** True to refresh the view after computation */
	protected boolean viewUpdatingEnabled = false;

	/** Factory of {@link IArray}. */
	@Inject
	protected IArrayFactory arrayFactory;

	/** Event broker service */
	@Inject
	protected IEventBroker eventBroker;

	/** Saved preferences */
	@Inject
	@Optional
	@Named(ContextNames.NAVSHIFT_PREFERENCE)
	protected NavShiftPreference preferences;

	/** currently used sessionContext */
	@Optional
	@Inject
	@Named(ContextNames.SESSION_CONTEXT)
	protected ISessionContext sessionContext;

	/** A Subject that emits IProgressMonitor to currently subscribed observers (computeSubscriber) */
	protected PublishSubject<IProgressMonitor> computePublisher = PublishSubject.create();
	/** Observer subscribed to mouseEventPublisher */
	protected Disposable computeSubscriber = null;

	/** the resulting model currently being processed */
	protected INavigationDataUpdater shiftedNavigationModel;

	/**
	 * Setter of {@link #sessionContext}. Called by DI only
	 */
	@Inject
	private void setSessionContext(@Optional @Named(ContextNames.SESSION_CONTEXT) ISessionContext newSessionContext) {
		if (computeSubscriber != null) {
			computeSubscriber.dispose();
			computeSubscriber = null;
		}
		if (newSessionContext != null) {
			computeSubscriber = computePublisher.toFlowable(BackpressureStrategy.LATEST)
					.subscribe(this::computeNavshift);
		}
		sessionContext = newSessionContext;
		shiftedNavigationModel = null;
	}

	/**
	 * Applies Navigation Shift algorithm to active Shift Period defined in {@link ISessionContext}
	 */
	public INavigationDataUpdater computeNavshift(IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		try {
			if (sessionContext != null && sessionContext.getShiftPeriod() != null) {

				initializeShiftedNavigationModel(subMonitor.split(20));

				// Shifting
				NavShifter shifter = new NavShifter();
				shifter.prepare(sessionContext.getNavigationDataSupplier(), sessionContext.getShiftPeriod(),
						subMonitor.split(10));
				shifter.shift(shiftedNavigationModel, subMonitor.split(60));

				// Project result in mercator
				Projection projection = ContextInitializer.getInContext(ContextNames.MERCATOR_PROJECTION);
				if (projection != null) {
					projection.project(shiftedNavigationModel::getLongitude, shiftedNavigationModel::getLatitude,
							shiftedNavigationModel::setX, shiftedNavigationModel::setY, shiftedNavigationModel.size(),
							subMonitor.split(10));
				}

				refreshView();

				return shiftedNavigationModel;
			}
		} catch (OperationCanceledException e) {
			// Operation canceled... never mind
		} catch (Exception e) {
			logger.warn("Error while computing navigation : ", e);
		}
		return null;
	}

	/** refresh Viewer3D */
	protected void refreshView() {
		if (viewUpdatingEnabled && Viewer3D.isUp()) {
			NavigationLineInfos lineInfos = new NavigationLineInfos(preferences.getShiftedNavigationColor(),
					preferences.getShiftedNavigationPointSize());

			lineInfos.setNavigationDataSupplier(shiftedNavigationModel);

			eventBroker.send(NavShiftEventTopics.TOPIC_DRAW_EXTRA_NAVIGATION_LINE, lineInfos);
			Viewer3D.refreshRequired();
		}
	}

	/** Assign attribute {@link #shiftedNavigationModel} if needed */
	public void initializeShiftedNavigationModel(IProgressMonitor monitor) {
		if (shiftedNavigationModel == null) {
			shiftedNavigationModel = sessionContext.getNavigationDataSupplier().duplicate(arrayFactory, monitor);
		} else {
			shiftedNavigationModel.copyFrom(sessionContext.getNavigationDataSupplier(), arrayFactory, monitor);
		}
	}

	/**
	 * Event handler for TOPIC_COMPUTE_SHIFT_ENABLED
	 */
	@Inject
	@Optional
	protected void autoShiftComputation(
			@UIEventTopic(NavShiftEventTopics.TOPIC_COMPUTE_SHIFT_ENABLED) Boolean enabled) {
		viewUpdatingEnabled = enabled.booleanValue();
		if (viewUpdatingEnabled) {
			computePublisher.onNext(new NullProgressMonitor());
		} else {
			// dereference the resulting model we are working on
			shiftedNavigationModel = null;
			eventBroker.send(NavShiftEventTopics.TOPIC_DRAW_EXTRA_NAVIGATION_LINE, null);
			Viewer3D.refreshRequired();
		}
	}

	/**
	 * Event handler for COMPUTE_SHIFT_REQUIRED
	 */
	@Inject
	@Optional
	protected void allowShiftComputation(@UIEventTopic(NavShiftEventTopics.TOPIC_COMPUTE_SHIFT_REQUIRED) Event event) {
		if (viewUpdatingEnabled) {
			computePublisher.onNext(new NullProgressMonitor());
		}
	}

}