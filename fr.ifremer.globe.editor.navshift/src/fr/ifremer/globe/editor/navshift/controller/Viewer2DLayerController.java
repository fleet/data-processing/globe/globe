/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.controller;

import java.util.ArrayList;
import java.util.List;

import jakarta.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;

import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.viewer2d.scene.SceneController;
import fr.ifremer.viewer3d.layers.CompoundLayer;
import fr.ifremer.viewer3d.layers.label.LabelsLayer;
import fr.ifremer.viewer3d.layers.lineisobaths.IsobathLayer;
import fr.ifremer.viewer3d.layers.lines.LinesLayer;
import gov.nasa.worldwind.layers.Layer;

/**
 * Controller of all layers displayed in 2D viewer.
 */
public class Viewer2DLayerController {

	/** Isobaths layers */
	protected List<IsobathLayer> profilesIsobathLayers;
	protected List<IsobathLayer> referenceDtmIsobathLayers;
	protected List<IsobathLayer> previewIsobathLayers;

	/** LinesLayer used to display the navigation line */
	protected LinesLayer navigationLayer = null;

	/** LabelsLayer used to display S and E labels */
	protected LabelsLayer labelsLayer = null;

	/** LinesLayer used to display the vetor's lines */
	protected LinesLayer vectorsLayer = null;

	/** Composite layer of navigationLayer, vectorsLayer and labelsLayer */
	protected Layer shiftLayerComposite;

	/** Not null when {@link #initialize()} has been called */
	protected ISessionContext sessionContext = null;

	@Inject
	@Optional
	protected SceneController sceneController;

	/**
	 * Constructor
	 */
	public Viewer2DLayerController() {
		super();
	}

	/**
	 * Setter of {@link #sessionContext}. Called by DI only
	 */
	protected void setSessionContext(ISessionContext newSessionContext) {
		if (sessionContext != newSessionContext) {
			sessionContext = newSessionContext;
			dispose();
			if (newSessionContext != null) {
				profilesIsobathLayers = new ArrayList<>();
				referenceDtmIsobathLayers = new ArrayList<>();
				previewIsobathLayers = new ArrayList<>();
				navigationLayer = new LinesLayer("Shift period & vectors", sessionContext.getGeoBox());
				labelsLayer = new LabelsLayer("Labels");
				vectorsLayer = new LinesLayer("Vectors", sessionContext.getGeoBox());
				shiftLayerComposite = new CompoundLayer(navigationLayer, vectorsLayer, labelsLayer);
			}
		}
	}

	/** Free all layers */
	protected void dispose() {
		if (profilesIsobathLayers != null) {
			disposeIsobathLayer(profilesIsobathLayers);
			profilesIsobathLayers = null;
		}
		if (referenceDtmIsobathLayers != null) {
			disposeIsobathLayer(referenceDtmIsobathLayers);
			referenceDtmIsobathLayers = null;
		}
		if (previewIsobathLayers != null) {
			disposeIsobathLayer(previewIsobathLayers);
			profilesIsobathLayers = null;
		}
		disposeNavigationLayer();
		disposeLabelLayer();
	}

	/** Dispose IsobathLayer */
	protected void disposeIsobathLayer(List<IsobathLayer> isobathLayers) {
		for (IsobathLayer isobathLayer : isobathLayers) {
			isobathLayer.removeAllLines();
			isobathLayer.dispose();
		}
	}

	/** Dispose the navigation Layer */
	protected void disposeNavigationLayer() {
		if (navigationLayer != null) {
			navigationLayer.removeAllLines();
			navigationLayer.dispose();
			navigationLayer = null;
		}
	}

	/** Dispose the navigation Layer */
	protected void disposeLabelLayer() {
		if (labelsLayer != null) {
			labelsLayer.dispose();
			labelsLayer = null;
		}
	}

	/** Dispose the navigation Layer */
	protected void disposeVectorsLayer() {
		if (vectorsLayer != null) {
			vectorsLayer.removeAllLines();
			vectorsLayer.dispose();
			vectorsLayer = null;
		}
	}

	/** Getter of {@link #profilesIsobathLayers} */
	public List<IsobathLayer> getProfilesIsobathLayer() {
		return profilesIsobathLayers;
	}

	/** Getter of {@link #referenceDtmIsobathLayers} */
	public List<IsobathLayer> getReferenceDtmIsobathLayer() {
		return referenceDtmIsobathLayers;
	}

	/** Getter of {@link #previewIsobathLayers} */
	public List<IsobathLayer> getPreviewIsobathLayer() {
		return previewIsobathLayers;
	}

	/** Getter of {@link #navigationLayer} */
	public LinesLayer getNavigationLayer() {
		return navigationLayer;
	}

	/** Getter of {@link #labelsLayer} */
	public LabelsLayer getLabelsLayer() {
		return labelsLayer;
	}

	/** Getter of {@link #vectorsLayer} */
	public LinesLayer getVectorsLayer() {
		return vectorsLayer;
	}

	/** Getter of {@link #shiftLayerComposite} */
	public Layer getShiftLayerComposite() {
		return shiftLayerComposite;
	}

}
