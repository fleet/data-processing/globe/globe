/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.lang.math.IntRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.dtm.SounderDataElevationRasterComputer;
import fr.ifremer.globe.core.model.dtm.SounderDataElevationRasterComputer.RasterComputerParameters;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.impl.ProfileImpl;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Controller of the considered {@link IProfile} in the navigation shift editor
 */
public class ProfileController extends AbstractController {

	protected static final Logger logger = LoggerFactory.getLogger(ProfileController.class);

	/** Current session */
	@Inject
	@Named(ContextNames.SESSION_CONTEXT)
	@org.eclipse.e4.core.di.annotations.Optional
	protected ISessionContext sessionContext;

	/**
	 * Mercator projection.
	 */
	@Inject
	@Named(ContextNames.MERCATOR_PROJECTION)
	protected Projection projection;

	/**
	 * Constructor
	 */
	public ProfileController() {
		super();
	}

	/**
	 * Perform the building of the navigation data with given list of sounder files
	 */
	public List<ProfileImpl> load(IProgressMonitor monitor) throws GIOException {
		List<ProfileImpl> result = null;
		if (getSessionContext().getSounderNcInfos() != null) {
			result = makeProfiles(getSessionContext().getSounderNcInfos(), monitor);
		}
		return result;
	}

	/**
	 * Fill the profiles list with NcInfos explicitly provided
	 */
	protected List<ProfileImpl> makeProfiles(List<ISounderNcInfo> sounderNcInfos, IProgressMonitor monitor)
			throws GIOException {
		List<ProfileImpl> result = new ArrayList<>();
		SubMonitor subMonitor = SubMonitor.convert(monitor, sounderNcInfos.size());

		int positionCount = 0;
		for (ISounderNcInfo sounderNcInfo : sounderNcInfos) {
			ProfileImpl profile = new ProfileImpl();
			profile.setSounderNcInfo(sounderNcInfo);

			try (INavigationData navigationDataProxy = sounderNcInfo.getNavigationData(true)) {
				profile.setNavigationIndexes(
						new IntRange(positionCount, positionCount + navigationDataProxy.size() - 1));
				positionCount += navigationDataProxy.size();
			} catch (IOException e) {
				throw new GIOException(e.getMessage(), e);
			}
			result.add(profile);
			subMonitor.worked(1);
		}

		subMonitor.done();
		return result;
	}

	/**
	 * 
	 * Generates elevation TiFF from {@link IProfile}.
	 */
	public void generateElevationTiFFFile(ISounderNcInfo sounderNcInfo, String outputFilePath, IProgressMonitor monitor)
			throws ProjectionException, OperationCanceledException, GIOException, IOException {
		var params = new RasterComputerParameters(projection, sessionContext.getGeoBox(),
				sessionContext.getDtmResolution(), outputFilePath);
		new SounderDataElevationRasterComputer().generateElevationTiFFFile(sounderNcInfo, params, monitor);
	}

}
