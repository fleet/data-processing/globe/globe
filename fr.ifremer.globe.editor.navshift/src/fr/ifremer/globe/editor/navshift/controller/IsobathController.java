/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.swt.widgets.Display;
import org.gdal.gdal.Dataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.drivers.dtm.netcdf4.service.worldwind.DemSupplier;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.application.event.VectorInfos;
import fr.ifremer.globe.editor.navshift.commons.IsobathParametersFactory;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.process.Previewer;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.viewer3d.layers.lineisobaths.IsobathLayer;
import fr.ifremer.viewer3d.layers.lineisobaths.parameters.IsobathParameters;
import fr.ifremer.viewer3d.layers.lineisobaths.tools.IsobathLayerFiller;
import fr.ifremer.viewer3d.layers.lineisobaths.tools.IsobathLayerFiller.LinePropertiesOfLayer;
import fr.ifremer.viewer3d.layers.lines.ILineProperties;

/**
 * MVC Controller of the navigation shift editor.
 */
public class IsobathController extends AbstractController {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(IsobathController.class);

	/** Factory of {@link IArray} */
	@Inject
	protected IArrayFactory arrayFactory;

	/** Mercator projection */
	@Inject
	@Named(ContextNames.MERCATOR_PROJECTION)
	protected Projection projection;

	/** Isobath parameters factory */
	@Inject
	@Named(ContextNames.ISOBATH_PARAM_FACTORY)
	protected IsobathParametersFactory isobathParametersFactory;

	@Inject
	@Named(ContextNames.PROFILE_CONTROLLER)
	protected ProfileController profileController;

	/** Isobath layer of profiles */
	protected Map<IProfile, IsobathLayer> layersOfProfile = new HashMap<>();

	/** Current profile dragged while a vector is edited */
	protected IProfile draggedProfile = null;

	private IsobathLayerFiller isobathLayerFiller;
	private GeoBox geoBox;

	/**
	 * Constructor
	 */
	public IsobathController() {
		super();
	}

	/**
	 * After injection
	 */
	@PostConstruct
	public void postConstruct() {
		isobathLayerFiller = new IsobathLayerFiller(arrayFactory, false);
	}

	/** Prepares the controller for a new edition */
	public void load(IProgressMonitor monitor)
			throws ProjectionException, OperationCanceledException, GIOException, IOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, "Generate isobaths...", 100);
		geoBox = getSessionContext().getGeoBox();
		generateIsobathsFromProfiles(getSessionContext().getProfiles(), subMonitor.split(80));
		if (getSessionContext().getReferenceRasterInfo() != null)
			generateIsobathsFromRasters(getSessionContext().getReferenceRasterInfo(), subMonitor.split(20));
	}

	/**
	 * Generates {@link IsobathLayer} from a {@link IProfile} list.
	 */
	private void generateIsobathsFromProfiles(List<IProfile> profiles, IProgressMonitor monitor)
			throws IOException, ProjectionException, OperationCanceledException, GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 2 * profiles.size());
		for (IProfile profile : profiles) {
			String layerName = new File(profile.getSounderNcInfo().getPath()).getName();
			// Generate data elevation model (DEM) file
			File demFile = TemporaryCache.createTemporaryFile("NAVSHIFT_" + layerName, ".tiff");
			profileController.generateElevationTiFFFile(profile.getSounderNcInfo(), demFile.getAbsolutePath(),
					subMonitor.split(1));

			// Create isobath layer
			IsobathLayer isobathLayer = new IsobathLayer(layerName, geoBox, demFile, isobathLayerFiller);
			isobathLayer.setShortName(layerName);
			ContextInjectionFactory.inject(isobathLayer, ContextInitializer.getEclipseContext());
			isobathLayer.setParameters(isobathParametersFactory.get());
			isobathLayer.apply(subMonitor.split(1), logger);
			viewer2DlayerController.getProfilesIsobathLayer().add(isobathLayer);
			layersOfProfile.put(profile, isobathLayer);
		}
	}

	/**
	 * Generates {@link IsobathLayer} from a {@link IProfile} list.
	 */
	public void updateIsobathsForProfile(IProfile profile, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor,
				"Generate isobaths of " + profile.getSounderNcInfo().getFilename() + "...", 100);
		IsobathLayer isobathLayer = layersOfProfile.get(profile);
		if (isobathLayer == null) {
			logger.warn("IsobathLayer not found for profile {}.", profile.getSounderNcInfo().getFilename());
			return;
		}

		String layerName = new File(profile.getSounderNcInfo().getPath()).getName();
		try {
			// Generate data elevation model (DEM) file
			File demFile = isobathLayer.getDemFile();
			profileController.generateElevationTiFFFile(profile.getSounderNcInfo(), demFile.getAbsolutePath(),
					subMonitor.split(100));
			isobathLayer.apply(subMonitor, logger);
		} catch (GException | IOException e) {
			logger.error("Error with " + layerName + " isobaths generation : " + e.getMessage(), e);
		}
	}

	/**
	 * Generates {@link IsobathLayer} from a {@link RasterInfo} list.
	 */
	private void generateIsobathsFromRasters(List<RasterInfo> rasterList, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, "Generate isobaths from reference DTM...",
				rasterList.size());
		for (RasterInfo rasterInfo : rasterList) {
			Dataset dataset = null;
			Dataset reprojectedDataset = null;
			String layerName = new File(rasterInfo.getFilePath()).getName();
			try {
				// setup DEM supplier
				var demSupplier = new DemSupplier(rasterInfo, projection.getSpatialReference(), "NAVSHIFT_DEM_PROJ",
						monitor);

				// Create isobath layer
				IsobathLayer isobathLayer = new IsobathLayer(layerName, geoBox, demSupplier, isobathLayerFiller);
				ContextInjectionFactory.inject(isobathLayer, ContextInitializer.getEclipseContext());
				isobathLayer.setShortName(layerName);
				isobathLayer.setParameters(isobathParametersFactory.getReference());
				isobathLayer.apply(subMonitor.split(1), logger);
				viewer2DlayerController.getReferenceDtmIsobathLayer().add(isobathLayer);
			} catch (GException e) {
				logger.error("Error with " + layerName + " isobaths generation : " + e.getMessage(), e);
			} finally {
				if (dataset != null) {
					dataset.delete();
				}
				if (reprojectedDataset != null) {
					reprojectedDataset.delete();
				}
			}
		}
	}

	/**
	 * Generates preview {@link IsobathLayer}.
	 */
	public void generatePreviewIsobaths(IProgressMonitor monitor, Previewer previewer, List<ISounderNcInfo> profiles) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, profiles.size() * 100);

		Display.getDefault().syncExec(() -> {
			for (var profile : profiles) {
				String layerName = "Preview of " + profile.getBaseName();

				// Retrieve parameters from initial profiles
				IsobathParameters parameters = isobathParametersFactory.getPreview();
				for (IsobathLayer isobathLayer : viewer2DlayerController.getProfilesIsobathLayer()) {
					if (isobathLayer.getName().equals(profile.getBaseName())) {
						parameters.load(isobathLayer.getIsobathParameters());
					}
				}

				try {
					// Generate data elevation model (DEM) file
					File demFile = TemporaryCache.createTemporaryFile("NAVSHIFT_" + layerName, ".tiff");
					profileController.generateElevationTiFFFile(profile, demFile.getAbsolutePath(),
							subMonitor.split(100));

					IsobathLayer isobathLayer = new IsobathLayer(layerName, getSessionContext().getGeoBox(),
							() -> demFile, isobathLayerFiller);
					isobathLayer.setShortName(layerName);
					isobathLayer.setParameters(parameters);
					viewer2DlayerController.getPreviewIsobathLayer().add(isobathLayer);

					isobathLayer.apply(subMonitor, logger);
				} catch (GIOException | IOException | ProjectionException | OperationCanceledException e) {
					logger.error("Error with " + layerName + " isobaths preview generation : " + e.getMessage(), e);
				}
			}
		});
	}

	/**
	 * Event handler for preview mode : remove layers after preview closed
	 */
	@Inject
	@Optional
	public void generateIsobathsOnPreview(
			@UIEventTopic(NavShiftEventTopics.PREVIEW) java.util.Optional<Previewer> option) {
		if (!option.isPresent()) {
			viewer2DlayerController.getPreviewIsobathLayer().forEach(IsobathLayer::removeAllLines);
			viewer2DlayerController.getPreviewIsobathLayer().clear();
		}
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.controller.AbstractController#onContextReset()
	 */
	@Override
	protected void onContextReset() {
		layersOfProfile = new HashMap<>();
	}

	/**
	 * Event handler for DRAW_VECTOR : moves isobath layers by editing theirs {@link ILineProperties}.
	 */
	@Inject
	@Optional
	protected void drawVector(@UIEventTopic(NavShiftEventTopics.TOPIC_DRAW_VECTOR) VectorInfos vectorInfos) {
		if (vectorInfos != null) {
			if (draggedProfile == null) {
				java.util.Optional<IProfile> option = layersOfProfile.keySet().stream()
						.filter(profile -> vectorInfos.getStartPositionIndex() >= profile.getFirstIndex()
								&& vectorInfos.getStartPositionIndex() <= profile.getLastIndex())
						.findFirst();
				if (option.isPresent()) {
					draggedProfile = option.get();
				}
			}
			if (draggedProfile != null) {
				INavigationModel navigationDataSupplier = sessionContext.getNavigationDataSupplier();
				double startX = navigationDataSupplier.getX(vectorInfos.getStartPositionIndex());
				double startY = navigationDataSupplier.getY(vectorInfos.getStartPositionIndex());
				double stopX = vectorInfos.getStopPoint().x;
				double stopY = vectorInfos.getStopPoint().y;

				LinePropertiesOfLayer linePropertiesOfLayer = layersOfProfile.get(draggedProfile)
						.getLinePropertiesOfLayer();
				ILineProperties mainLineProperties = linePropertiesOfLayer.getMainLineProperties();
				if (mainLineProperties != null) {
					mainLineProperties.setxOffset((float) (stopX - startX));
					mainLineProperties.setyOffset((float) (stopY - startY));
				}
				ILineProperties otherLineProperties = linePropertiesOfLayer.getOtherLineProperties();
				if (otherLineProperties != null) {
					otherLineProperties.setxOffset((float) (stopX - startX));
					otherLineProperties.setyOffset((float) (stopY - startY));
				}
			}
		} else {
			LinePropertiesOfLayer linePropertiesOfLayer = layersOfProfile.get(draggedProfile)
					.getLinePropertiesOfLayer();
			if (linePropertiesOfLayer != null) {
				ILineProperties mainLineProperties = linePropertiesOfLayer.getMainLineProperties();
				if (mainLineProperties != null) {
					mainLineProperties.resetOffsets();
				}
				ILineProperties otherLineProperties = linePropertiesOfLayer.getOtherLineProperties();
				if (otherLineProperties != null) {
					otherLineProperties.resetOffsets();
				}
			}
			draggedProfile = null;
		}
	}

	public Map<IProfile, IsobathLayer> getLayersOfProfile() {
		return layersOfProfile;
	}

}
