/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.controller;

import java.util.ArrayList;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.impl.NavigationModel;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Controller of the considered {@link NavigationModel} in the navigation shift editor
 */
public class NavigationModelController extends AbstractController {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(NavigationModelController.class);

	/** Factory of {@link IArray}. */
	@Inject
	protected IArrayFactory arrayFactory;

	/** Current used model */
	protected NavigationModel navigationModel;

	/**
	 * Mercator projection.
	 */
	@Inject
	@Named(ContextNames.MERCATOR_PROJECTION)
	protected Projection projection;

	/**
	 * Constructor
	 */
	public NavigationModelController() {
		super();
	}

	/**
	 * Perform the building of the navigation data with the sounder files hold by session context
	 */
	public INavigationModel load(IProgressMonitor monitor) throws GIOException {
		monitor.setTaskName("Load navigation data...");
		onContextReset();
		List<ISounderNcInfo> sounderNcInfos = getSessionContext().getSounderNcInfos();
		if (sounderNcInfos != null) {
			load(sounderNcInfos, monitor);
		}
		return navigationModel;
	}

	/**
	 * Update of the navigation data with the sounder files hold by session context
	 */
	public INavigationModel reload(IProgressMonitor monitor) throws GIOException {

		List<ISounderNcInfo> sounderNcInfos = getSessionContext().getSounderNcInfos();
		if (sounderNcInfos != null) {
			List<INavigationData> navigationDataProxies = new ArrayList<>();
			try {
				for (ISounderNcInfo sounderNcInfo : sounderNcInfos) {
					navigationDataProxies.add(sounderNcInfo.getNavigationData(true));
				}
				loadNavigationData(navigationDataProxies, monitor);
			} finally {
				navigationDataProxies.forEach(IOUtils::closeQuietly);
			}
		}
		return navigationModel;
	}

	/**
	 * Perform the building of the navigation data with given list of sounder files
	 */
	public void load(List<ISounderNcInfo> sounderNcInfos, IProgressMonitor monitor) throws GIOException {
		onContextReset();

		SubMonitor subMonitor = SubMonitor.convert(monitor, 10);

		List<INavigationData> navigationDataProxies = new ArrayList<>();

		try {
			for (ISounderNcInfo sounderNcInfo : sounderNcInfos) {
				navigationDataProxies.add(sounderNcInfo.getNavigationData(true));
			}
			instanciateNavigationModel();
			loadNavigationData(navigationDataProxies, subMonitor.split(8));
		} finally {
			navigationDataProxies.forEach(IOUtils::closeQuietly);
		}
	}

	/**
	 * Create or replace the current NavigationModel
	 */
	protected void instanciateNavigationModel() throws GIOException {
		List<IProfile> profiles = getSessionContext().getProfiles();
		navigationModel = new NavigationModel(arrayFactory, profiles.get(profiles.size() - 1).getLastIndex() + 1);
	}

	/**
	 * Perform the loading of navigation data
	 */
	protected void loadNavigationData(List<INavigationData> navigationDataProxies, IProgressMonitor monitor)
			throws GIOException {
		List<IProfile> profiles = getSessionContext().getProfiles();
		SubMonitor subMonitor = SubMonitor.convert(monitor, profiles.size());
		for (int i = 0; i < profiles.size(); i++) {
			IProfile profile = profiles.get(i);
			INavigationData navigationDataProxy = navigationDataProxies.get(i);
			loadNavigationModelForProfile(profile, navigationDataProxy, subMonitor.split(1));
		}
		subMonitor.done();
	}

	/**
	 * Perform the loading of the navigation for the specified profile
	 */
	protected void loadNavigationModelForProfile(IProfile profile, INavigationData navigationDataProxy,
			IProgressMonitor monitor) throws GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 2);
		load(profile, navigationDataProxy, subMonitor.split(1));
		try {
			project(subMonitor.split(1));
		} catch (ProjectionException e) {
			throw GIOException.wrap("Projection error while extracting navigation data", e);
		}
	}

	/**
	 * Perform the loading of the navigation for the specified profile
	 */
	protected void load(IProfile profile, INavigationData navigationDataProxy, IProgressMonitor monitor)
			throws GIOException {
		int positionIndex = profile.getFirstIndex();
		int size = navigationDataProxy.size();
		SubMonitor subMonitor = SubMonitor.convert(monitor, size);
		for (int index = 0; index < size; index++, positionIndex++) {
			double longitude = navigationDataProxy.getLongitude(index);
			double latitude = navigationDataProxy.getLatitude(index);
			double elevation = navigationDataProxy.getMetadataValue(NavigationMetadataType.ELEVATION, index).orElse(0f);
			navigationModel.setValues(positionIndex, navigationDataProxy.getTime(index), longitude, latitude,
					elevation);
			subMonitor.worked(1);
		}
	}

	/**
	 * Project all navigation's points
	 */
	protected void project(IProgressMonitor monitor) throws ProjectionException {
		if (projection != null) {
			projection.project(navigationModel::getLongitude, navigationModel::getLatitude, navigationModel::setX,
					navigationModel::setY, navigationModel.size(), monitor);
		}
	}

	/**
	 * Getter for NavigationModel {@link NavigationModel}
	 *
	 * @return
	 */
	public NavigationModel getNavigationModel() {
		return navigationModel;
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.controller.AbstractController#onContextReset()
	 */
	@Override
	protected void onContextReset() {
		super.onContextReset();
		if (navigationModel != null) {
			navigationModel.close();
			navigationModel = null;
		}
	}
}
