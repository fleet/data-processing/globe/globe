package fr.ifremer.globe.editor.navshift.controller;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;

import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.viewer2d.NavShift2DViewer;

public abstract class AbstractController implements PropertyChangeListener {

	/** Event broker service */
	@Inject
	@Optional
	protected IEventBroker eventBroker;

	/** Controller of all layers displayed in {@link NavShift2DViewer}. */
	@Inject
	@Named(ContextNames.VIEWER_2D_LAYER_CONTROLLER)
	protected Viewer2DLayerController viewer2DlayerController;

	/** Shared contexe data */
	protected ISessionContext sessionContext;

	/**
	 * Constructor
	 */
	protected AbstractController() {
		super();
	}

	/**
	 * Override if listening session's property is a supported operation
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		// Override if listening session's property is a supported operation
		if (ISessionContext.PROPERTY_SHIFTPERIOD.equals(event.getPropertyName())) {
			onShiftPeriodChanged((IShiftPeriod) event.getOldValue(), (IShiftPeriod) event.getNewValue());
		}
	}

	/**
	 * Getter of sessionContext
	 */
	public ISessionContext getSessionContext() {
		return sessionContext;
	}

	/**
	 * Setter of {@link #sessionContext}. Called by DI only
	 */
	@Inject
	private void setSessionContext(@Optional @Named(ContextNames.SESSION_CONTEXT) ISessionContext newSessionContext) {
		if (this.sessionContext != newSessionContext) {
			viewer2DlayerController.setSessionContext(newSessionContext);

			if (this.sessionContext != null) {
				removeSessionPropertyChangeListener();
			}
			onContextReset();

			this.sessionContext = newSessionContext;
			if (newSessionContext != null) {
				addSessionPropertyChangeListener();
				onContextSet();
			}
		}
	}

	protected void addSessionPropertyChangeListener() {
		sessionContext.addPropertyChangeListener(this);
		IShiftPeriod shiftPeriod = this.sessionContext.getShiftPeriod();
		if (shiftPeriod != null) {
			shiftPeriod.addPropertyChangeListener(this);
		}
	}

	protected void removeSessionPropertyChangeListener() {
		this.sessionContext.removePropertyChangeListener(this);
		IShiftPeriod shiftPeriod = this.sessionContext.getShiftPeriod();
		if (shiftPeriod != null) {
			shiftPeriod.removePropertyChangeListener(this);
		}
	}

	/** Handle the session context set event */
	protected void onContextSet() {
		// Override if this operation is supported
	}

	/** Handle the reset session event */
	protected void onContextReset() {
		// Override if reseting is a supported operation
	}

	/** IShiftPeriod has changed. */
	protected void onShiftPeriodChanged(IShiftPeriod oldShiftPeriod, IShiftPeriod newShiftPeriod) {
		if (oldShiftPeriod != null) {
			oldShiftPeriod.removePropertyChangeListener(this);
		}
		if (newShiftPeriod != null) {
			newShiftPeriod.addPropertyChangeListener(this);
		}
	}

}
