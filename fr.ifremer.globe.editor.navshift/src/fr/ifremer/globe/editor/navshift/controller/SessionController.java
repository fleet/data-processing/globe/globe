/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.AssertionFailedException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.runtime.job.GProcess;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.commons.NavShiftPreference;
import fr.ifremer.globe.editor.navshift.commons.SounderNcInfoUtility;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.impl.SessionContextImpl;
import fr.ifremer.globe.editor.navshift.wizard.save.SaveSessionContextWizard;
import fr.ifremer.globe.editor.navshift.xstream.XStreamFactory;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Session Controller of the Navigation Shift Editor.
 *
 * Controller is in charge of session lifecycle, (de)serialization of SessionContext triggering of events related to
 * global state change (session reset,start & end) and organize the cooperation between specialized controllers
 *
 * This one does not extend {@link AbstractController} as it's singleton instance is injected through AbstractController
 *
 */
public final class SessionController {

	/** Logger */
	private static final Logger logger = LoggerFactory.getLogger(SessionController.class);

	/**
	 * Strings used
	 */
	public static final String SAVE_WINDOW_TITLE = "Save Context";

	/** Event broker service */
	@Inject
	@Optional
	private IEventBroker eventBroker;

	/** Temporary folder */
	@Inject
	@Named(ContextNames.TMP_FOLDER)
	private File tmpDir;

	/** Saved preferences */
	@Inject
	@Named(ContextNames.NAVSHIFT_PREFERENCE)
	private NavShiftPreference preferences;

	/**
	 * Managed Session model interested by the list of {@link ISounderNcInfo} and the {@link INavigationModel}
	 */
	private SessionContextImpl sessionContext;

	/**
	 * Set the new session as active and tell every controller that it is
	 */
	public void activateSession(SessionContextInitializer sessionContextInitializer) {
		sessionContext = new SessionContextImpl();
		try {
			if (sessionContextInitializer.init(this, sessionContext)) {
				IProcessService.grab().runInForeground("Navigation Shift opening", (monitor, l) -> {
					try {
						ContextInitializer.setInContext(ContextNames.SESSION_CONTEXT, sessionContext);
						tmpDir.mkdirs();
						loadFiles(monitor);
						// Clean if cancel
						if (monitor.isCanceled())
							throw new OperationCanceledException();

						eventBroker.post(NavShiftEventTopics.SESSION_ACTIVATED, sessionContext);
					} catch (OperationCanceledException e) {
						sessionContext = null;
						ContextInitializer.removeFromContext(ContextNames.SESSION_CONTEXT);
						FileUtils.deleteQuietly(tmpDir);
						return Status.CANCEL_STATUS;
					} catch (ProjectionException | GIOException | IOException e) {
						sessionContext = null;
						ContextInitializer.removeFromContext(ContextNames.SESSION_CONTEXT);
						FileUtils.deleteQuietly(tmpDir);
						Messages.openErrorMessage("Session start", "Unable to load input file : " + e.getMessage());
						return Status.CANCEL_STATUS;
					}
					return Status.OK_STATUS;
				});
			} else {
				sessionContext = null;
				Messages.openErrorMessage("Session start", "Unable to start or restore session: initialization error");
			}
		} catch (Exception e) {
			sessionContext = null;
			Messages.openErrorMessage("Session start", "Unable to start or restore session: " + e.getMessage());
		}
	}

	/**
	 * Prepare the editor by loading the sounder files, generating the navigation model, generating the DTMs and then
	 * generating the Isobaths
	 *
	 * @throws OperationCanceledException if the monitor has been cancelled
	 */
	private void loadFiles(IProgressMonitor monitor)
			throws ProjectionException, OperationCanceledException, GIOException, IOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, "Loading files in Navigation Shift Editor", 100);
		ProfileController profileController = getController(ContextNames.PROFILE_CONTROLLER);
		sessionContext.setProfiles(profileController.load(subMonitor.split(20)));

		var navigationModel = ((NavigationModelController) getController(ContextNames.NAVIGATION_MODEL_CONTROLLER))
				.load(subMonitor.split(35));
		sessionContext.setNavigationDataSupplier(navigationModel);

		((IsobathController) getController(ContextNames.ISOBATH_CONTROLLER)).load(subMonitor.split(40));

		((NavigationLayerController) getController(ContextNames.NAVIGATION_LAYER_CONTROLLER)).load(subMonitor.split(5));
	}

	/**
	 * Set the new session as inactive and tell every controller
	 *
	 * @throws AssertionFailedException if SessionContext is null
	 */
	public void disposeSession() {
		Assert.isNotNull(sessionContext, "FATAL: attempt to dispose session before it has been created");
		eventBroker.send(NavShiftEventTopics.TOPIC_IS_DIRTY, Boolean.FALSE);
		sessionContext = null;
		ContextInitializer.removeFromContext(ContextNames.SESSION_CONTEXT);
		preferences.save();
		FileUtils.deleteQuietly(tmpDir);
	}

	/**
	 * @return a reference to current active context
	 */
	public ISessionContext getSessionContext() {
		return sessionContext;
	}

	/**
	 * Event handler for TOPIC_SAVE : saving of context is required
	 */
	@Inject
	@Optional
	public void saveSessionContext(@UIEventTopic(NavShiftEventTopics.TOPIC_SAVE) IProgressMonitor monitor,
			@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {

		SaveSessionContextWizard saveSessionWizard = ContextInitializer.make(SaveSessionContextWizard.class);
		saveSessionWizard.setWindowTitle(SAVE_WINDOW_TITLE);

		// Any session file in preferences ?
		if (preferences.getSessionFile().getValue() != null) {
			WritableFile aFile = saveSessionWizard.getSaveSessionModel().getSelectedFile();
			aFile.set(preferences.getSessionFile().getValue().toFile());
		}

		WizardDialog wizardDialog = new WizardDialog(shell, saveSessionWizard);
		wizardDialog.setMinimumPageSize(300, 510);
		// save as local because of dispose method which set it null
		ISessionContext sessionContextToSave = sessionContext;
		shell.getDisplay().asyncExec(() -> {
			if (wizardDialog.open() == Window.OK) {
				if (sessionContextToSave != null) {
					WritableFile aFile = saveSessionWizard.getSaveSessionModel().getSelectedFile();
					if (aFile.isNotNull()) {
						saveContextFile(sessionContextToSave, aFile.getValue());
						preferences.getSessionFile().setValue(aFile.getValue().toPath(), false);
						preferences.save();
					}
					monitor.done();
				}

				// Saving was successful
				if (eventBroker != null) {
					eventBroker.send(NavShiftEventTopics.TOPIC_IS_DIRTY, Boolean.FALSE);
				}
			}
		});
	}

	/**
	 * Context (de-)serialization support using Xstream
	 */

	public void loadContextFile(String path) throws GIOException {
		File fileToLoad = new File(path);
		if (fileToLoad.exists()) {
			loadContextFile(fileToLoad);
		} else {
			throw new GIOException("Error while loading Context File, file does not exist: " + fileToLoad.getName());
		}
	}

	/**
	 * @throws AssertionFailedException if SessionContext is null
	 */
	public void loadContextFile(File fileToLoad) throws GIOException {
		Assert.isNotNull(sessionContext, "Session should be created before restoring");
		XStream xstream = XStreamFactory.create();

		try {
			xstream.fromXML(fileToLoad, sessionContext);
		} catch (Exception e) {
			throw new GIOException("Error while loading Context File " + fileToLoad.getName(), e);
		}
	}

	/**
	 * @throws AssertionFailedException if SessionContext is null
	 */
	public void saveContextFile(String path) {
		Assert.isNotNull(sessionContext, "Session is not activated");
		XStream xstream = XStreamFactory.create();
		try (FileOutputStream fileOutputStream = new FileOutputStream(path)) {
			fileOutputStream.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".getBytes(StandardCharsets.UTF_8));
			xstream.toXML(sessionContext, fileOutputStream);
		} catch (IOException e) {
			logger.warn("Error while saving Context File " + path, e);
		}
	}

	/**
	 * @throws AssertionFailedException if SessionContext is null
	 */
	public void saveContextFile(ISessionContext sessionContext, File aFile) {
		Assert.isNotNull(sessionContext, "Session is not activated");
		XStream xstream = XStreamFactory.create();
		try (FileOutputStream fileOutputStream = new FileOutputStream(aFile)) {
			fileOutputStream.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n".getBytes(StandardCharsets.UTF_8));
			xstream.toXML(sessionContext, fileOutputStream);
			Messages.openInfoMessage("Navigation shift", "Session saved in : " + aFile.getAbsolutePath());
		} catch (IOException e) {
			logger.warn("Error while saving Context File " + aFile.getName(), e);
		}
	}

	/**
	 * @return a named controller
	 */
	@SuppressWarnings("unchecked")
	public <C extends AbstractController> C getController(String controllerName) {
		return (C) ContextInitializer.getInContext(controllerName);
	}

	/**
	 * Double consumer for a specific index
	 */
	@FunctionalInterface
	public interface SessionContextInitializer {
		boolean init(SessionController sessionController, ISessionContext sessionContext);
	}

	/**
	 * Event handler for TOPIC_FILE_SHIFTED<br>
	 * Some files have been shifted. We reload this files, generating new isobaths and navigation line
	 */
	@Inject
	@Optional
	private void onFilesShifted(@UIEventTopic(NavShiftEventTopics.TOPIC_FILES_SHIFTED) List<File> files) {
		var reloadedFilenames = new ArrayList<String>();

		GProcess process = IProcessService.grab().createProcess("Navigation shift update", (monitor, l) -> {
			SubMonitor subMonitor = SubMonitor.convert(monitor, files.size() * 100);
			// Before performing any update, reload all SounderNcInfo
			reloadSounderNcInfo();

			for (File file : files) {
				IProfile profile = getProfile(file).orElse(null);
				try {
					if (profile != null && file.equals(profile.getSounderNcInfo().toFile())) {
						reloadProfile(profile, subMonitor);
						reloadedFilenames.add(file.getName());
					} else {
						subMonitor.worked(100);
					}
				} catch (Exception e) {
					throw new GIOException("Unable to refresh the NavShift editor", e);
				}
			}
			return Status.OK_STATUS;
		});

		process.whenIsDone(event -> {
			String message = "Shift aborted : nothing to do";
			if (reloadedFilenames.size() > 1) {
				message = "Following files have been modified and reloaded :\n - "//
						+ reloadedFilenames.stream().collect(Collectors.joining("\n - "));
			} else if (reloadedFilenames.size() == 1) {
				message = "Following file has been modified and reloaded : " //
						+ reloadedFilenames.get(0);
			} else if (files.size() > 1) {
				message = "Shift has been applied to files :\n - "
						+ files.stream().map(File::getName).collect(Collectors.joining("\n - "));
			} else if (files.size() == 1) {
				message = "Shift has been applied to file : " + files.get(0).getName();
			}
			Messages.openInfoMessage("Navigation shift", message);
		});

		IProcessService.grab().runInForeground(process);
	}

	/**
	 * Reload a profile (after shifting)
	 */
	private void reloadProfile(IProfile profile, SubMonitor subMonitor) throws Exception {
		// update navigation model
		NavigationModelController navigationModelController = getController(ContextNames.NAVIGATION_MODEL_CONTROLLER);
		sessionContext.setNavigationDataSupplier(navigationModelController.reload(subMonitor.split(20)));
		// update isobaths
		IsobathController isobathController = getController(ContextNames.ISOBATH_CONTROLLER);
		isobathController.updateIsobathsForProfile(profile, subMonitor.split(10));
		// update navigation line
		NavigationLayerController navigationLayerController = getController(ContextNames.NAVIGATION_LAYER_CONTROLLER);
		navigationLayerController.load(subMonitor.split(5));
	}

	/**
	 * Reload all the ISounderNcInfo to compute the global Geobox
	 * 
	 * @throws GIOException
	 */
	private void reloadSounderNcInfo() throws GIOException {
		// Reload all the ISounderNcInfo
		for (var it = sessionContext.getSounderNcInfos().listIterator(); it.hasNext();) {
			var sounderNcInfo = it.next();
			var fileInfo = IFileService.grab().getFileInfo(sounderNcInfo.getPath(),
					true /* force to have the new geobox */
			);
			fileInfo.stream()//
					.filter(ISounderNcInfo.class::isInstance)// Sanity check
					.map(ISounderNcInfo.class::cast)//
					.forEach(ncInfo -> {
						// Set the NcInfo in the context
						it.set(ncInfo);
						// Set the NcInfo in the profile
						getProfile(ncInfo.toFile()).ifPresent(profile -> profile.setSounderNcInfo(ncInfo));
					});
		}

		// Compute the global Geobox and set the result in the context...
		SounderNcInfoUtility.processNCInfos(sessionContext.getSounderNcInfos(), sessionContext::setGeoBox,
				java.util.Optional.empty());
	}

	private java.util.Optional<IProfile> getProfile(File file) {
		return sessionContext.getProfiles().stream()//
				.filter(p -> file.equals(p.getSounderNcInfo().toFile()))//
				.findFirst();
	}
}
