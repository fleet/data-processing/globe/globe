/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.controller;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.lang.math.IntRange;
import org.eclipse.core.runtime.Assert;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.HighlightPointInfos;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.application.event.VectorInfos;
import fr.ifremer.globe.editor.navshift.application.handlers.CommandExecutor;
import fr.ifremer.globe.editor.navshift.commons.NavShiftPreference;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.IShiftVector;
import fr.ifremer.globe.editor.navshift.model.impl.ShiftPeriodImpl;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.lines.ILineProperties;

/** Controller of the shift period and vectors */
public final class ShiftPeriodController extends AbstractController {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(ShiftPeriodController.class);

	/**
	 * Mercator projection.
	 */
	@Inject
	@Named(ContextNames.MERCATOR_PROJECTION)
	protected Projection projection;

	/** Saved preferences */
	@Inject
	@Named(ContextNames.NAVSHIFT_PREFERENCE)
	protected NavShiftPreference preferences;

	/** ILineProperties reflecting the vector's lines in the vectorsLayer */
	protected Map<IShiftVector, ILineProperties> vectorLineProperties = new HashMap<>();
	/** ILineProperties reflecting the current edited vector in the vectorsLayer */
	protected ILineProperties tmpVectorLineProperties = null;

	/** Private Data */
	protected List<IShiftPeriod> shiftPeriods = new ArrayList<>();

	public ShiftPeriodController() {
		super();
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.controller.AbstractController#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		super.propertyChange(event);

		if (ISessionContext.PROPERTY_NAVIGATION.equals(event.getPropertyName())) {
			INavigationModel newNavData = (INavigationModel) event.getNewValue();
			/*
			 * Unset active shiftPeriod if navigationData changed (ShiftPeriod is potentially invalid)
			 */
			if (getSessionContext() != null) {
				IShiftPeriod currentShiftPeriod = getSessionContext().getShiftPeriod();
				if (currentShiftPeriod != null) {
					if (currentShiftPeriod.getNavigationDataSupplier() != newNavData
							&& currentShiftPeriod.getNavigationDataSupplier() != null) {
						getSessionContext().setShiftPeriod(null);
					}
				}
				setUpNavigationData(newNavData);
			}
		} else if (ISessionContext.PROPERTY_SHIFTPERIOD.equals(event.getPropertyName())) {
			manageShiftPeriodModification(event);
		} else if (IShiftPeriod.PROPERTY_VECTOR.equals(event.getPropertyName())) {
			manageVectorModification(event);
		} else if (IShiftPeriod.PROPERTY_VECTORS.equals(event.getPropertyName())) {
			eventBroker.send(NavShiftEventTopics.TOPIC_COMPUTE_SHIFT_REQUIRED, null);
		} else if (IShiftVector.PROPERTY_END.equals(event.getPropertyName())
				|| IShiftVector.PROPERTY_ORIGIN.equals(event.getPropertyName())) {
			manageVectorModification((IShiftVector) event.getSource(), true);
		}
	}

	/**
	 * Set up navigation Data for all ShiftPeriod where navigation is null (post-deserialization)
	 */
	protected void setUpNavigationData(INavigationModel navigationData) {
		for (IShiftPeriod shiftPeriod : shiftPeriods) {
			if (shiftPeriod.getNavigationDataSupplier() == null) {
				shiftPeriod.setNavigationDataSupplier(navigationData);
				shiftPeriod.setProjection(projection);

				// highlight the shiftPeriod
				eventBroker.send(NavShiftEventTopics.TOPIC_HIGHLIGHT_POINTS,
						new HighlightPointInfos(new IntRange(shiftPeriod.getStartIndex(), shiftPeriod.getEndIndex()),
								preferences.getPeriodColor(), preferences.getPeriodPointSize()));

				List<IShiftVector> shiftVectors = shiftPeriod.getShiftVectors();
				if (shiftVectors != null && !shiftVectors.isEmpty()) {
					setUpVectors(shiftVectors);
					eventBroker.post(NavShiftEventTopics.TOPIC_COMPUTE_SHIFT_REQUIRED, true);
				}

				Viewer3D.refreshRequired();
			}
		}
	}

	/**
	 * Set up missing properties of vectors (post-deserialization)
	 */
	protected void setUpVectors(List<IShiftVector> shiftVectors) {
		for (IShiftVector shiftVector : shiftVectors) {
			shiftVector.project();
			manageVectorCreation(shiftVector, false);

		}
	}

	/**
	 * Creates a new ShiftPeriod
	 */
	public IShiftPeriod createShiftPeriod() {
		IShiftPeriod period = new ShiftPeriodImpl();
		period.setNavigationDataSupplier(getSessionContext().getNavigationDataSupplier());
		period.setProjection(projection);
		shiftPeriods.add(period);
		return period;
	}

	public void removeShiftPeriod(IShiftPeriod wkz) {
		if (shiftPeriods.contains(wkz)) {
			shiftPeriods.remove(wkz);
		}
	}

	public List<IShiftPeriod> getShiftPeriods() {
		return Collections.unmodifiableList(shiftPeriods);
	}

	public INavigationModel getNavigationData() {
		return getSessionContext().getNavigationDataSupplier();
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.controller.AbstractController#onContextReset()
	 */
	@Override
	protected void onContextReset() {
		vectorLineProperties = new HashMap<>();
		tmpVectorLineProperties = null;
		shiftPeriods = new ArrayList<>();

		eventBroker.post(NavShiftEventTopics.TOPIC_IS_DIRTY, Boolean.FALSE);
	}

	/**
	 * Set up the NavigationLayer
	 */
	@Override
	protected void onContextSet() {
		// post-deserialization, retrieves the ShiftPeriod
		if (getSessionContext().getShiftPeriod() != null) {
			shiftPeriods.add(getSessionContext().getShiftPeriod());
		}
	}

	/**
	 * Event handler for DRAW_VECTOR
	 */
	@Inject
	@Optional
	protected void drawVector(@UIEventTopic(NavShiftEventTopics.TOPIC_DRAW_VECTOR) VectorInfos vectorInfos) {
		if (viewer2DlayerController.getVectorsLayer() != null) {
			if (vectorInfos != null) {
				INavigationModel navigationDataSupplier = sessionContext.getNavigationDataSupplier();
				double startX = navigationDataSupplier.getX(vectorInfos.getStartPositionIndex());
				double startY = navigationDataSupplier.getY(vectorInfos.getStartPositionIndex());
				double z = navigationDataSupplier.getZ(vectorInfos.getStartPositionIndex());
				double stopX = vectorInfos.getStopPoint().x;
				double stopY = vectorInfos.getStopPoint().y;

				if (tmpVectorLineProperties != null) {
					tmpVectorLineProperties.updatePoint(1, vectorInfos.getStopPoint());
				} else {
					tmpVectorLineProperties = viewer2DlayerController.getVectorsLayer().acceptLine(2,
							index -> index == 0 ? startX : stopX, index -> index == 0 ? startY : stopY, index -> z,
							vectorInfos.getColor(), vectorInfos.getPointSize());
				}
			} else if (tmpVectorLineProperties != null) {
				viewer2DlayerController.getVectorsLayer().removeLine(tmpVectorLineProperties);
				tmpVectorLineProperties = null;
			}
		}
	}

	/**
	 * Event handler for DELETE_VECTOR
	 */
	@Inject
	@Optional
	protected void deleteVector(@UIEventTopic(NavShiftEventTopics.TOPIC_DELETE_VECTOR) Event event) {
		for (Entry<IShiftVector, ILineProperties> entry : vectorLineProperties.entrySet()) {
			if (entry.getValue().getColor() == preferences.getSelectedVectorColor()) {
				if (Messages.openSyncQuestionMessage("Delete vector...", "Delete the vector ?")) {
					CommandExecutor.executeCommand(CommandExecutor.DELETE_VECTOR_COMMAND, entry.getKey());
				}
				break;
			}
		}
	}

	/**
	 * A modification of vector has been notified...
	 */
	protected void manageVectorModification(PropertyChangeEvent event) {
		IShiftVector oldShiftVector = (IShiftVector) event.getOldValue();
		IShiftVector newShiftVector = (IShiftVector) event.getNewValue();
		if (oldShiftVector == null) {
			manageVectorCreation(newShiftVector, true);
		} else if (newShiftVector == null) {
			manageVectorDeletion(oldShiftVector);
		} else {
			manageVectorModification(newShiftVector, true);
		}
	}

	/**
	 * Modification of an existing vector has been notified...
	 */
	protected void manageVectorModification(IShiftVector shiftVector, boolean manageEvents) {
		if (viewer2DlayerController.getVectorsLayer() != null) {
			ILineProperties lineProperties = vectorLineProperties.get(shiftVector);
			if (lineProperties != null) {
				lineProperties.updatePoint(1, shiftVector.getOriginX() - shiftVector.getDriftX(),
						shiftVector.getOriginY() - shiftVector.getDriftY(), shiftVector.getOriginZ());
				Viewer3D.refreshRequired();
				if (manageEvents) {
					eventBroker.send(NavShiftEventTopics.TOPIC_IS_DIRTY, Boolean.TRUE);
					eventBroker.send(NavShiftEventTopics.TOPIC_COMPUTE_SHIFT_REQUIRED, null);
				}
			}
		}
	}

	/**
	 * A deletion of vector has been notified...
	 */
	protected void manageVectorDeletion(IShiftVector shiftVector) {
		if (viewer2DlayerController.getVectorsLayer() != null) {
			ILineProperties lineProperties = vectorLineProperties.remove(shiftVector);
			if (lineProperties != null) {
				viewer2DlayerController.getVectorsLayer().removeLine(lineProperties);
				eventBroker.send(NavShiftEventTopics.TOPIC_IS_DIRTY, Boolean.TRUE);
				eventBroker.send(NavShiftEventTopics.TOPIC_COMPUTE_SHIFT_REQUIRED, null);
			}

		}
	}

	/**
	 * A creation of vector has been notified...
	 */
	protected void manageVectorCreation(IShiftVector shiftVector, boolean manageEvents) {
		if (viewer2DlayerController.getVectorsLayer() != null) {
			Assert.isTrue(!vectorLineProperties.containsKey(shiftVector), "Attempt to add the vector twice");
			ILineProperties lineProperties = viewer2DlayerController.getVectorsLayer().acceptLine(2,
					index -> index == 0 ? shiftVector.getOriginX() : shiftVector.getOriginX() - shiftVector.getDriftX(),
					index -> index == 0 ? shiftVector.getOriginY() : shiftVector.getOriginY() - shiftVector.getDriftY(),
					index -> shiftVector.getOriginZ(), preferences.getVectorColor(), preferences.getVectorPointSize());
			vectorLineProperties.put(shiftVector, lineProperties);

			if (manageEvents) {
				eventBroker.send(NavShiftEventTopics.TOPIC_IS_DIRTY, Boolean.TRUE);
				eventBroker.send(NavShiftEventTopics.TOPIC_COMPUTE_SHIFT_REQUIRED, null);
			}
		}
	}

	/**
	 * Event handler for DRAW_VECTOR
	 */
	@Inject
	@Optional
	protected void onVectorSelected(
			@UIEventTopic(NavShiftEventTopics.TOPIC_VECTORS_SELECTED) List<IShiftVector> shiftVectors) {
		// Unselect all vectors
		for (ILineProperties lineProperties : vectorLineProperties.values()) {
			lineProperties.setWidth(preferences.getVectorPointSize());
			lineProperties.setColor(preferences.getVectorColor());
			lineProperties.setPointColor(null);
		}

		shiftVectors.stream() //
				.map(vectorLineProperties::get) //
				.filter(Objects::nonNull)//
				.forEach(lineProperties -> {
					lineProperties.setColor(preferences.getSelectedVectorColor());
					lineProperties.setPointColor(preferences.getSelectedVectorColor());
					lineProperties.setPointWidth(preferences.getHighlightVectorPointSize());
				});
	}

	/**
	 * Event handler for HIGHLIGHT_FOR_SELECTING_VECTOR
	 */
	@Inject
	@Optional
	protected void onAboutToSelectVector(
			@UIEventTopic(NavShiftEventTopics.TOPIC_HIGHLIGHT_FOR_SELECTING_VECTOR) IShiftVector shiftVector) {
		// Unhighlight all vectors
		for (ILineProperties lineProperties : vectorLineProperties.values()) {
			lineProperties.setWidth(preferences.getVectorPointSize());
		}

		ILineProperties lineProperties = vectorLineProperties.get(shiftVector);
		if (lineProperties != null) {
			lineProperties.setWidth(preferences.getHighlightVectorPointSize());
		}
	}

	/**
	 * Event handler for HIGHLIGHT_FOR_SELECTING_VECTOR
	 */
	@Inject
	@Optional
	protected void onAboutToModifyVector(
			@UIEventTopic(NavShiftEventTopics.TOPIC_HIGHLIGHT_FOR_MODIFYING_VECTOR) IShiftVector shiftVector) {
		ILineProperties lineProperties = vectorLineProperties.get(shiftVector);
		if (lineProperties != null) {
			lineProperties.setWidth(preferences.getHighlightVectorPointSize());
		}
	}

	/**
	 * A modification of ShiftPeriod has been notified...
	 */
	protected void manageShiftPeriodModification(PropertyChangeEvent event) {
		// ShiftPeriod has been added
		if (event.getNewValue() != null) {
			eventBroker.post(NavShiftEventTopics.TOPIC_IS_DIRTY, Boolean.TRUE);
		} else if (viewer2DlayerController.getVectorsLayer() != null) {
			// ShiftPeriod has been deleted
			viewer2DlayerController.getVectorsLayer().removeAllLines();
			vectorLineProperties.clear();
			eventBroker.post(NavShiftEventTopics.TOPIC_IS_DIRTY, Boolean.FALSE);
		}
	}

}
