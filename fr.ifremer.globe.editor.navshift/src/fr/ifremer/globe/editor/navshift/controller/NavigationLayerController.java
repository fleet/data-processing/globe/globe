/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.controller;

import java.beans.PropertyChangeEvent;

import org.apache.commons.lang.math.IntRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.utils.color.ColorProvider;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.HighlightPointInfos;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.application.event.NavigationLineInfos;
import fr.ifremer.globe.editor.navshift.commons.NavShiftPreference;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.ILabelProperties;
import fr.ifremer.viewer3d.Viewer3D;
import fr.ifremer.viewer3d.layers.lines.ILineProperties;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 * Controller Nasa layer to display navigations
 */
public class NavigationLayerController extends AbstractController {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(NavigationLayerController.class);

	/** Saved preferences */
	@Inject
	@Named(ContextNames.NAVSHIFT_PREFERENCE)
	protected NavShiftPreference preferences;

	/** ILineProperties reflecting the navigation line in the navigationLayer */
	protected ILineProperties navigationLineProperties;

	/** ILineProperties reflecting the extra navigation line in the navigationLayer */
	protected ILineProperties extraNavigationLineProperties;

	/**
	 * Constructor
	 */
	public NavigationLayerController() {
		super();
	}

	/** Prepares the controller for a new edition */
	public void load(IProgressMonitor monitor) {
		drawNavigationLine(getSessionContext().getNavigationDataSupplier());
		if (getSessionContext().getShiftPeriod() != null) {
			showShiftPeriod(getSessionContext().getShiftPeriod());
		}
		monitor.done();
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.controller.AbstractController#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		super.propertyChange(event);

		if (ISessionContext.PROPERTY_SHIFTPERIOD.equals(event.getPropertyName()) && navigationLineProperties != null) {
			IShiftPeriod shiftPeriod = (IShiftPeriod) event.getNewValue();
			showHideShiftPeriod(shiftPeriod);
			drawExtraNavigationLine(null);
			Viewer3D.refreshRequired();
		}
	}

	/** Draw the shift Period */
	protected void showHideShiftPeriod(IShiftPeriod shiftPeriod) {
		if (shiftPeriod != null) {
			showShiftPeriod(shiftPeriod);
		} else {
			navigationLineProperties.unHighlightPoints();
			viewer2DlayerController.getLabelsLayer().removeAllLabels();
		}
	}

	/** Draw the shift Period */
	protected void showShiftPeriod(IShiftPeriod shiftPeriod) {
		if (shiftPeriod.getStartIndex() != -1 && shiftPeriod.getEndIndex() != -1) {
			navigationLineProperties.highlightPoints(
					new IntRange(shiftPeriod.getStartIndex(), shiftPeriod.getEndIndex()), preferences.getPeriodColor(),
					preferences.getPeriodPointSize());
			createLabel("S", shiftPeriod.getStartIndex());
			createLabel("E", shiftPeriod.getEndIndex());
		}
	}

	protected void createLabel(String label, int index) {
		INavigationModel navigationModel = getSessionContext().getNavigationDataSupplier();
		ILabelProperties properties = viewer2DlayerController.getLabelsLayer().acceptLabel(label,
				navigationModel.getLongitude(index), navigationModel.getLatitude(index), 0d);
		properties.setColor(preferences.getPeriodColor());
		properties.setUserFacing(true);
	}

	/** Draw the navigation */
	protected void drawNavigationLine(INavigationModel navigationModel) {
		if (viewer2DlayerController.getNavigationLayer() != null) {
			ColorProvider colorProvider = new ColorProvider();
			viewer2DlayerController.getNavigationLayer().removeAllLines();
			navigationLineProperties = viewer2DlayerController.getNavigationLayer().acceptLine(navigationModel.size(),
					navigationModel::getX, navigationModel::getY, navigationModel::getZ, colorProvider.get(), 2f);

			// Affect one color per profile
			for (IProfile profile : getSessionContext().getProfiles()) {
				navigationLineProperties.split(profile.getFirstIndex(), profile.getLastIndex(), colorProvider.get(),
						2f);
			}
		}
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.controller.AbstractController#onContextReset()
	 */
	@Override
	protected void onContextReset() {
		navigationLineProperties = null;
		extraNavigationLineProperties = null;
	}

	/**
	 * Event handler for TOPIC_HIGHLIGHT_POINT
	 */
	@Inject
	@Optional
	protected void highlightPoint(@UIEventTopic(NavShiftEventTopics.TOPIC_HIGHLIGHT_POINT) HighlightPointInfos infos) {
		if (navigationLineProperties != null) {
			navigationLineProperties.highlightPoint(infos.getIndexes().getMinimumInteger(), infos.getColor(),
					infos.getPointSize());
		}
	}

	/**
	 * Event handler for TOPIC_HIGHLIGHT_POINT
	 */
	@Inject
	@Optional
	protected void unhighlightPoint(@UIEventTopic(NavShiftEventTopics.TOPIC_UNHIGHLIGHT_POINT) Event event) {
		if (navigationLineProperties != null) {
			navigationLineProperties.unHighlightPoint();
		}
	}

	/**
	 * Event handler for TOPIC_HIGHLIGHT_POINTS
	 */
	@Inject
	@Optional
	protected void highlightPoints(
			@UIEventTopic(NavShiftEventTopics.TOPIC_HIGHLIGHT_POINTS) HighlightPointInfos infos) {
		if (navigationLineProperties != null) {
			navigationLineProperties.highlightPoints(infos.getIndexes(), infos.getColor(), infos.getPointSize());
		}
	}

	/**
	 * Event handler for TOPIC_UNHIGHLIGHT_POINTS
	 */
	@Inject
	@Optional
	protected void unhighlightPoints(@UIEventTopic(NavShiftEventTopics.TOPIC_UNHIGHLIGHT_POINTS) Event event) {
		if (navigationLineProperties != null) {
			navigationLineProperties.unHighlightPoints();
		}
	}

	/**
	 * Event handler for TOPIC_DRAW_EXTRA_NAVIGATION_LINE
	 */
	@Inject
	@Optional
	protected void drawExtraNavigationLine(
			@UIEventTopic(NavShiftEventTopics.TOPIC_DRAW_EXTRA_NAVIGATION_LINE) NavigationLineInfos navigationLineInfos) {
		if (extraNavigationLineProperties != null) {
			viewer2DlayerController.getNavigationLayer().removeLine(extraNavigationLineProperties);
		}

		if (navigationLineInfos != null) {
			INavigationModel navigationDataSupplier = navigationLineInfos.getNavigationDataSupplier();
			extraNavigationLineProperties = viewer2DlayerController.getNavigationLayer().acceptLine(
					navigationDataSupplier.size(), navigationDataSupplier::getX, navigationDataSupplier::getY,
					navigationDataSupplier::getZ, navigationLineInfos.getColor(), navigationLineInfos.getPointSize());

			// Split nav per profile
			for (IProfile profile : getSessionContext().getProfiles()) {
				extraNavigationLineProperties.split(profile.getFirstIndex(), profile.getLastIndex(),
						navigationLineInfos.getColor(), navigationLineInfos.getPointSize());
			}
		}
	}
}
