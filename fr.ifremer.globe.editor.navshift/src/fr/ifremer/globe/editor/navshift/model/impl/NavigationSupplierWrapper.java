/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.model.impl;

import java.util.List;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.NavigationMetadata;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.utils.exception.GIOException;

public class NavigationSupplierWrapper implements INavigationData {

	INavigationModel wrappedSupplier;

	/** Metadata **/
	private final List<NavigationMetadata<?>> metadata = List.of(
			NavigationMetadataType.TIME.withValueSupplier(this::getTime2),
			NavigationMetadataType.ELEVATION.withValueSupplier(this::getElevation));

	public NavigationSupplierWrapper(INavigationModel supplier) {
		wrappedSupplier = supplier;
	}

	@Override
	public int size() {
		return wrappedSupplier.size();
	}

	@Override
	public String getFileName() {
		return null;
	}

	private long getTime2(int index) throws GIOException {
		return wrappedSupplier.getTime(index);
	}

	@Override
	public double getLatitude(int index) throws GIOException {
		return wrappedSupplier.getLatitude(index);
	}

	@Override
	public double getLongitude(int index) throws GIOException {
		return wrappedSupplier.getLongitude(index);
	}

	private float getElevation(int index) {
		return (float) wrappedSupplier.getZ(index);
	}

	@Override
	public List<NavigationMetadata<?>> getMetadata() {
		return metadata;
	}

}
