/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.model.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.stream.IntStream;

import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.INavigationDataUpdater;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.IDoubleArray;
import fr.ifremer.globe.utils.array.ILongArray;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * All navigation data extracted from a INavigationDataProvider
 */
public class NavigationModel implements INavigationDataUpdater {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(NavigationModel.class);

	/** Number of elements */
	protected int positionCount;
	/** Array of time values */
	protected ILongArray times;
	/** Array of latitude values */
	protected IDoubleArray latitudes;
	/** Array of longitude values */
	protected IDoubleArray longitudes;
	/** Array of abscissa (mercator) */
	protected IDoubleArray x;
	/** Array of ordinate (mercator) */
	protected IDoubleArray y;
	/** Array of depth */
	protected IDoubleArray z;

	/**
	 * Constructor
	 */
	public NavigationModel(IArrayFactory arrayFactory, int positionCount) throws GIOException {
		this.positionCount = positionCount;
		try {
			times = arrayFactory.makeLongArray(positionCount);
			latitudes = arrayFactory.makeDoubleArray(positionCount);
			longitudes = arrayFactory.makeDoubleArray(positionCount);
			x = arrayFactory.makeDoubleArray(positionCount);
			y = arrayFactory.makeDoubleArray(positionCount);
			z = arrayFactory.makeDoubleArray(positionCount);
		} catch (IOException e) {
			close();
			throw GIOException.wrap("Not enough memory", e);
		}
	}

	/**
	 * @see java.io.Closeable#close()
	 */
	@Override
	public void close() {
		IOUtils.closeQuietly(times);
		times = null;
		IOUtils.closeQuietly(latitudes);
		latitudes = null;
		IOUtils.closeQuietly(longitudes);
		longitudes = null;
		IOUtils.closeQuietly(x);
		x = null;
		IOUtils.closeQuietly(y);
		y = null;
		IOUtils.closeQuietly(z);
		z = null;
	}

	/**
	 * Set all values for the index
	 */
	public void setValues(int index, long time, double longitude, double latitude, double depth) {
		setValues(index, time, longitude, latitude);
		z.putDouble(index, depth);
	}

	/**
	 * Set all values for the index
	 */
	protected void setValues(int index, long time, double longitude, double latitude) {
		if (index < positionCount) {
			times.putLong(index, time);
			latitudes.putDouble(index, latitude);
			longitudes.putDouble(index, longitude);
		} else {
			throw new IndexOutOfBoundsException("Trying to set Navpoint with index > positioncount");
		}
	}

	@Override
	public void updateNavPoint(int index, long time, double longitude, double latitude) {
		setValues(index, time, longitude, latitude);
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.model.ISounderNcFileHolder#getTime(int)
	 */
	@Override
	public long getTime(int index) {
		return times.getLong(index);
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.model.ISounderNcFileHolder#getLatitude(int)
	 */
	@Override
	public double getLatitude(int index) {
		return latitudes.getDouble(index);
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.model.ISounderNcFileHolder#getLongitude(int)
	 */
	@Override
	public double getLongitude(int index) {
		return longitudes.getDouble(index);
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.model.ISounderNcFileHolder#size()
	 */
	@Override
	public int size() {
		return positionCount;
	}

	/** {@inheritDoc} */
	@Override
	public int getNearestPointIndex(double[] point) {
		double minDistance = Double.POSITIVE_INFINITY;
		int result = -1;
		for (int i = 0; i < size(); i++) {
			double distance = Math.hypot(getX(i)-point[0], getY(i)-point[1]);
			if (minDistance >= distance) {
				minDistance = distance;
				result = i;
			}
		}
		return result;
	}


	/**
	 * Getter of x
	 */
	@Override
	public double getX(int index) {
		return x.getDouble(index);
	}

	/**
	 * Getter of y
	 */
	@Override
	public double getY(int index) {
		return y.getDouble(index);
	}

	/**
	 * Getter of z
	 */
	@Override
	public double getZ(int index) {
		return z.getDouble(index);
	}

	/**
	 * Setter of z
	 */
	public void setZ(int index, double value) {
		z.putDouble(index, value);
	}

	/**
	 * Getter of latitudes
	 */
	public IDoubleArray getLatitudes() {
		return latitudes;
	}

	/**
	 * Getter of longitudes
	 */
	public IDoubleArray getLongitudes() {
		return longitudes;
	}

	/**
	 * Getter of dates
	 */
	public ILongArray getTimes() {
		return times;
	}

	/**
	 * Getter of x
	 */
	public IDoubleArray getX() {
		return x;
	}

	/**
	 * Getter of y
	 */
	public IDoubleArray getY() {
		return y;
	}

	/**
	 * find a specific index for a date in Navigation Data
	 * 
	 * @param dateval
	 * @return
	 */
	@Override
	public int getNearestPointIndexForDate(long dateval, MATCH_POLICY matchPolicy) {

		int curr_end = positionCount - 1;
		int curr_start = 0;
		long curr_endTime = getTime(curr_end);
		long curr_startTime = getTime(curr_start);
		int curr_idx = (curr_end + curr_start) / 2;
		long curr_midTime = getTime(curr_idx);
		/*
		 * First deal with out of bound values
		 */
		if (dateval > curr_endTime || dateval < curr_startTime) {
			return -1;
		}
		/*
		 * Then search
		 */
		while (curr_idx > 1 && curr_start < curr_idx && curr_end > curr_idx) {
			if (dateval < curr_midTime) {
				curr_end = curr_idx;
				curr_endTime = getTime(curr_idx);
			} else {
				curr_start = curr_idx;
				curr_startTime = getTime(curr_idx);
			}
			curr_idx = (curr_end + curr_start) / 2;
			curr_midTime = getTime(curr_idx);
		}
		/*
		 * Default: if we have an exact match => return
		 */
		if (dateval == curr_startTime) {
			return curr_start;
		}
		if (dateval == curr_endTime) {
			return curr_end;
		}
		/*
		 * Else..depend on policy
		 */
		switch (matchPolicy) {
		case exact: {
			// No exact value found
			return -1;
		}
		case nearest: {
			if ((dateval - curr_startTime) < (curr_endTime - dateval)) {
				return curr_start;
			} else {
				return curr_end;
			}
		}
		case lower_value: {
			return curr_start;
		}
		case higher_value: {
			return curr_end;
		}
		}
		// Whatever...
		return -1;
	}

	@Override
	public boolean isEqual(INavigationModel other) {
		if (other == null || positionCount != other.size()) {
			return false;
		}
		boolean result = true;
		IntStream iterable = IntStream.range(0, positionCount);

		result = !iterable.anyMatch(idx -> ((getLatitude(idx) != other.getLatitude(idx))
				|| (getLongitude(idx) != other.getLongitude(idx)) || (getTime(idx) != other.getTime(idx))));

		return result;
	}

	@Override
	public NavPointDifferences compare(INavigationModel other, double coordPrecision) {

		NavPointDifferences differences = new NavPointDifferences();

		IntStream iterable = IntStream.range(0, positionCount);

		java.util.TimeZone GMT_ZONE = java.util.TimeZone.getTimeZone("GMT");
		SimpleDateFormat timeFormat = new SimpleDateFormat(" dd/MM/yyyy HH:mm:ss.SSS");
		timeFormat.setTimeZone(GMT_ZONE);

		iterable.forEach(idx -> {

			int diffResult = 0;
			if (Math.abs(getLatitude(idx) - other.getLatitude(idx)) > coordPrecision) {
				diffResult = diffResult | NavPointDifferences.DIFF_LAT;
			}
			if (Math.abs(getLongitude(idx) - other.getLongitude(idx)) > coordPrecision) {
				diffResult = diffResult | NavPointDifferences.DIFF_LONG;

			}
			if (Math.abs(getTime(idx) - other.getTime(idx)) > 1) {
				diffResult = diffResult | NavPointDifferences.DIFF_TIME;

			}
			if (diffResult != 0) {
				differences.addDiff(idx, diffResult);
			}
		});

		return differences;
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.model.INavigationDataUpdater#setX(int, double)
	 */
	@Override
	public void setX(int index, double value) {
		x.putDouble(index, value);
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.model.INavigationDataUpdater#setY(int, double)
	 */
	@Override
	public void setY(int index, double value) {
		y.putDouble(index, value);
	}

	/**
	 * Duplicates a NavigationModel, all data will be copied to new instance
	 * 
	 * @param arrayFactory
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T extends INavigationModel> T duplicate(IArrayFactory arrayFactory, IProgressMonitor progress) {
		try {

			SubMonitor subMonitor = SubMonitor.convert(progress, this.size());

			NavigationModel newNM = new NavigationModel(arrayFactory, this.size());

			IntStream iterable = IntStream.range(0, this.size());
			iterable.forEach(idx -> {
				newNM.setValues(idx, this.getTime(idx), this.getLongitude(idx), this.getLatitude(idx), this.getZ(idx));
				subMonitor.worked(1);
			});
			return (T) newNM;
		} catch (GIOException e) {
			logger.debug("NavigationModel duplication error ", e);
			return null;
		}
	}

	/**
	 * Duplicates a NavigationModel, all data will be copied to new instance
	 * 
	 * @param arrayFactory
	 */
	@Override
	public void copyFrom(INavigationModel input, IArrayFactory arrayFactory, IProgressMonitor progress) {

		SubMonitor subMonitor = SubMonitor.convert(progress, input.size());

		IntStream iterable = IntStream.range(0, input.size());
		iterable.forEach(idx -> {
			setValues(idx, input.getTime(idx), input.getLongitude(idx), input.getLatitude(idx), input.getZ(idx));
			subMonitor.worked(1);
		});

	}

}
