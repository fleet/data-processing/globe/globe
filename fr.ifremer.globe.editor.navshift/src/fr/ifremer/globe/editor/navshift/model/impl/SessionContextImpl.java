/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.model.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.commons.SounderNcInfoUtility;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.INavigationModel.MATCH_POLICY;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.IShiftVector;
import fr.ifremer.globe.editor.navshift.xstream.SessionContextConverter;
import fr.ifremer.globe.editor.navshift.xstream.XStreamFactory;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.FileUtils;
import jakarta.inject.Singleton;

/**
 *
 * Session Context holds information about the global state of the Navigation shifter tool and shared information
 * between controllers
 *
 */

@XStreamConverter(SessionContextConverter.class)
@Singleton
@Creatable
public class SessionContextImpl implements ISessionContext {

	/**
	 * Technical fields
	 */
	@XStreamOmitField
	protected static final Logger logger = LoggerFactory.getLogger(SessionContextImpl.class);

	@XStreamOmitField
	protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	/**
	 * Current active (displayed) ShiftPeriod
	 */
	@XStreamOmitField
	protected IShiftPeriod shiftPeriod = null;

	/**
	 * List of Files used to build Navigation data accessed through {link @INavigationDataSupplier}
	 */
	@XStreamOmitField
	protected List<File> currentFileset = new ArrayList<>();

	/**
	 * Fields related to Navigation data processing
	 */
	// bounding box for the current Navigation data
	@XStreamOmitField
	protected GeoBox geoBox;

	// Navigation data sources
	@XStreamOmitField
	protected List<ISounderNcInfo> sounderNcInfos;

	// Reference DTM
	@XStreamOmitField
	protected List<RasterInfo> referenceRasterInfos;

	// Profiles on navigation data sources
	@XStreamOmitField
	protected List<IProfile> profiles;

	// Navigation Model
	@XStreamOmitField
	protected INavigationModel navigationData;

	/** Grid DTM resolution */
	protected double dtmResolution = 1d;

	/** Isobath - Main line (every x lines) */
	protected int isobathMainLine = 10;

	/** Isobath - Distance between two secondary lines. */
	protected double isobathStep = 10.0d;

	/** Isobath - % of points expected per lines. */
	protected int isobathSampling = 50;

	/**
	 * Constructor
	 */
	public SessionContextImpl() {
		super();
	}

	/*
	 * Current fileset: all files used to build Navigation data
	 *
	 */
	@Override
	public List<File> getCurrentFileset() {
		return currentFileset;
	}

	@Override
	public void setCurrentFileset(List<File> currentFileset) {
		if (!Objects.equals(this.currentFileset, currentFileset)) {
			pcs.firePropertyChange(PROPERTY_FILESET_DATA, this.currentFileset, this.currentFileset = currentFileset);
		}
	}

	/**
	 * Navigation Data supplier
	 *
	 * @see fr.ifremer.globe.editor.navshift.model.INavigationModel
	 */
	@Override
	public INavigationModel getNavigationDataSupplier() {
		return navigationData;
	}

	@Override
	public void setNavigationDataSupplier(INavigationModel navData) {
		if (!Objects.equals(navigationData, navData)) {
			pcs.firePropertyChange(PROPERTY_NAVIGATION, navigationData, navigationData = navData);
		}
	}

	/*
	 * ShiftPeriod
	 */
	@Override
	public IShiftPeriod getShiftPeriod() {
		return shiftPeriod;
	}

	@Override
	public void setShiftPeriod(IShiftPeriod wkz) {
		if (!Objects.equals(shiftPeriod, wkz)) {
			pcs.firePropertyChange(PROPERTY_SHIFTPERIOD, shiftPeriod, shiftPeriod = wkz);
		}
	}

	/*
	 * geoBox
	 */
	@Override
	public GeoBox getGeoBox() {
		return geoBox;
	}

	@Override
	public void setGeoBox(GeoBox geoBox) {
		if (!Objects.equals(this.geoBox, geoBox)) {
			pcs.firePropertyChange(PROPERTY_GEOBOX_DATA, this.geoBox, this.geoBox = geoBox);
		}
	}

	/*
	 * SounderNcInfos
	 */
	@Override
	public void setSounderNcInfos(List<ISounderNcInfo> sounderNcInfos) {
		if (navigationData != null) {
			setNavigationDataSupplier(null);
		}

		if (!Objects.equals(this.sounderNcInfos, sounderNcInfos)) {
			List<ISounderNcInfo> oldSounderNcInfos = this.sounderNcInfos;
			this.sounderNcInfos = SounderNcInfoUtility.chronologicalSort(sounderNcInfos);
			pcs.firePropertyChange(PROPERTY_NCINFOS_DATA, oldSounderNcInfos, this.sounderNcInfos);
		}
	}

	@Override
	public List<ISounderNcInfo> getSounderNcInfos() {
		return sounderNcInfos;
	}

	@Override
	public List<RasterInfo> getReferenceRasterInfo() {
		return referenceRasterInfos;
	}

	@Override
	public void setReferenceRasterInfo(List<RasterInfo> inputRasterInfo) {
		referenceRasterInfos = inputRasterInfo;
	}

	/**
	 * Property Change supporting methods {@see @link{PropertyChangeSupport}}
	 */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	/**
	 * Property Change supporting methods {@see @link{PropertyChangeSupport}}
	 */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	/**
	 * Getter of {@link #profiles}
	 */
	@Override
	public List<IProfile> getProfiles() {
		return profiles == null ? Collections.emptyList() : Collections.unmodifiableList(profiles);
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.model.ISessionContext#setProfiles(java.util.List)
	 */
	@Override
	public void setProfiles(List<? extends IProfile> profiles) {
		if (!Objects.equals(this.profiles, profiles)) {
			pcs.firePropertyChange(PROPERTY_PROFILES, this.profiles, this.profiles = new ArrayList<>(profiles));
		}
	}

	/**
	 * Getter of {@link #dtmResolution}
	 */
	@Override
	public double getDtmResolution() {
		return dtmResolution;
	}

	/**
	 * Setter of {@link #dtmResolution}
	 */
	@Override
	public void setDtmResolution(double dtmResolution) {
		this.dtmResolution = dtmResolution;
	}

	/**
	 * Getter of {@link #isobathMainLine}
	 */
	@Override
	public int getIsobathMainLine() {
		return isobathMainLine;
	}

	/**
	 * Setter of {@link #isobathMainLine}
	 */
	@Override
	public void setIsobathMainLine(int isobathMainLine) {
		this.isobathMainLine = isobathMainLine;
	}

	/**
	 * Getter of {@link #isobathStep}
	 */
	@Override
	public double getIsobathStep() {
		return isobathStep;
	}

	/**
	 * Setter of {@link #isobathStep}
	 */
	@Override
	public void setIsobathStep(double isobathStep) {
		this.isobathStep = isobathStep;
	}

	/**
	 * Getter of {@link #isobathSampling}
	 */
	@Override
	public int getIsobathSampling() {
		return isobathSampling;
	}

	/**
	 * Setter of {@link #isobathSampling}
	 */
	@Override
	public void setIsobathSampling(int isobathSampling) {
		this.isobathSampling = isobathSampling;
	}

	@Override
	public void importShiftVectors(File file) {
		// Read shift vectors.
		List<IShiftVector> importedVectors = FileUtils.getExtension(file) == "xml" ? getShiftVectorsFromXml(file)
				: getShiftVectorsFromJson(file);

		// Load by adding or replacing current vector.
		var resp = Messages.openSyncQuestionMessage("Import vectors",
				String.format("%s vector%s to load. Add to or replace current vectors ?", importedVectors.size(),
						importedVectors.size() > 1 ? "s" : ""),
				"Add", "Replace", true);
		if (resp == 0 || resp == 1) {
			if (resp == 1)
				shiftPeriod.removeAllShiftVector();
			importedVectors.forEach(shiftPeriod::addShiftVector);
		}
	}

	/**
	 * Reads {@link IShiftVector} from XML session file.
	 */
	private List<IShiftVector> getShiftVectorsFromXml(File xmlFile) {
		// Load XML file in temporary session.
		SessionContextImpl tmpSessionContext = new SessionContextImpl();
		XStreamFactory.create().fromXML(xmlFile, tmpSessionContext);

		// Import shift vectors.
		return tmpSessionContext.getShiftPeriod().getShiftVectors().stream()
				.filter(vector -> shiftPeriod.getStartTime() <= vector.getTimeOrigin()
						|| vector.getTimeOrigin() <= shiftPeriod.getEndTime())
				.map(vector -> {
					int index = navigationData.getNearestPointIndexForDate(vector.getTimeOrigin(),
							MATCH_POLICY.nearest);
					return shiftPeriod.createShiftVectorWithLonLat(index, vector.getEndLongitude(),
							vector.getEndLatitude());
				}).toList();

	}

	/** Record used to deserialize JSON file with Shift Vectors **/
	private record JsonShiftVector(long time, double lat, double lon) {
	}

	/**
	 * Reads {@link IShiftVector} from JSON file.
	 */
	private List<IShiftVector> getShiftVectorsFromJson(File jsonFile) {
		var result = new ArrayList<IShiftVector>();
		try (FileReader reader = new FileReader(jsonFile)) {
			List<JsonShiftVector> jsonShiftVectors = new Gson().fromJson(reader,
					new TypeToken<List<JsonShiftVector>>() {
					}.getType());
			jsonShiftVectors.forEach(jsonVector -> {
				int index = navigationData.getNearestPointIndexForDate(jsonVector.time(), MATCH_POLICY.nearest);
				result.add(shiftPeriod.createShiftVectorWithLonLat(index, jsonVector.lon(), jsonVector.lat()));
			});
		} catch (IOException e) {
			logger.error("Error while loading shift vectors from JSON file : " + jsonFile.getName(), e);
		}
		return result;
	}

}