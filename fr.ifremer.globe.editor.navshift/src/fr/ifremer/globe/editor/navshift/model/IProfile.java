/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.model;

import org.apache.commons.lang.math.IntRange;

import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;

/**
 * Part of the navigation, where a ISounderNcInfo data has been loaded
 */
public interface IProfile {

	/**
	 * Getter of INavigationDataProvider
	 */
	INavigationDataSupplier getNavProvider();

	/**
	 * Getter of ISounderNcInfo
	 */
	ISounderNcInfo getSounderNcInfo();

	/**
	 * Setter of ISounderNcInfo
	 */
	void setSounderNcInfo(ISounderNcInfo sounderNcInfo);

	/**
	 * Getter of the index of the first point in the whole navigation
	 */
	int getFirstIndex();

	/**
	 * Getter of the index of the last point in the whole navigation
	 */
	int getLastIndex();

	/** Getter of range of indexes */
	default IntRange getIndexRange() {
		return new IntRange(getFirstIndex(), getLastIndex());
	}

}