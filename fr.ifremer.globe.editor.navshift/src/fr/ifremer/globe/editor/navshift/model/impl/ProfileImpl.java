/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.model.impl;

import org.apache.commons.lang.math.IntRange;

import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.model.IProfile;

/**
 * Implementation of IProfile
 */
public class ProfileImpl implements IProfile {

	/** Sounder files loaded for this profile */
	protected ISounderNcInfo sounderNcInfo;

	/** Indexes of the first and last points in the whole navigation */
	protected IntRange navigationIndexes;

	/**
	 * Constructor
	 */
	public ProfileImpl() {
		super();
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.model.IProfile#getFirstIndex()
	 */
	@Override
	public int getFirstIndex() {
		return navigationIndexes.getMinimumInteger();
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.model.IProfile#getLastIndex()
	 */
	@Override
	public int getLastIndex() {
		return navigationIndexes.getMaximumInteger();
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.model.IProfile#getNavProvider()
	 */
	@Override
	public INavigationDataSupplier getNavProvider() {
		return sounderNcInfo;
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.model.IProfile#getSounderNcInfo()
	 */
	@Override
	public ISounderNcInfo getSounderNcInfo() {
		return sounderNcInfo;
	}

	/**
	 * Getter of navigationIndexes
	 */
	public IntRange getNavigationIndexes() {
		return navigationIndexes;
	}

	/**
	 * Setter of navigationIndexes
	 */
	public void setNavigationIndexes(IntRange navigationIndexes) {
		this.navigationIndexes = navigationIndexes;
	}

	/**
	 * Setter of {@link #sounderNcInfo}
	 */
	public void setSounderNcInfo(ISounderNcInfo sounderNcInfo) {
		this.sounderNcInfo = sounderNcInfo;
	}

}
