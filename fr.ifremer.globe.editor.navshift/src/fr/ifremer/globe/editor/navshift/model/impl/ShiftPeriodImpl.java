/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.model.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.eclipse.e4.core.di.annotations.Creatable;

import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.INavigationModel.MATCH_POLICY;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.IShiftVector;
import fr.ifremer.globe.editor.navshift.xstream.LongToDateConverter;

@Creatable
public class ShiftPeriodImpl implements IShiftPeriod {

	@XStreamOmitField
	private List<ShiftVectorImpl> shiftVectors = new ArrayList<>();

	@XStreamOmitField
	protected int startIndex = -1;
	@XStreamConverter(LongToDateConverter.class)
	protected long startTime = -1;

	@XStreamOmitField
	protected int endIndex = -1;
	@XStreamConverter(LongToDateConverter.class)
	protected long endTime = -1;

	@XStreamOmitField
	protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	@XStreamOmitField
	protected INavigationModel navigationData;

	@XStreamOmitField
	private Projection mercatorProj;

	/**
	 * default constructor
	 */
	public ShiftPeriodImpl() {
		super();
	}

	@Override
	public long getStartTime() {
		return startTime;
	}

	@Override
	public void setStartTime(long val) {
		long oldval = startTime;
		startTime = val;
		if (navigationData != null) {
			setStartIndex(navigationData.getNearestPointIndexForDate(val, MATCH_POLICY.exact));
		}
		if (!Objects.equals(oldval, startTime)) {
			pcs.firePropertyChange("startTime", oldval, startTime);
		}
	}

	@Override
	public long getEndTime() {
		return endTime;
	}

	@Override
	public void setEndTime(long val) {
		long oldval = endTime;
		endTime = val;
		if (navigationData != null) {
			setEndIndex(navigationData.getNearestPointIndexForDate(val, MATCH_POLICY.exact));
		}

		if (!Objects.equals(oldval, endTime)) {
			pcs.firePropertyChange("endTime", oldval, endTime);
		}
	}

	@Override
	public int getStartIndex() {
		if (navigationData != null && startIndex < 0 && startTime > 0) {
			startIndex = navigationData.getNearestPointIndexForDate(startTime, MATCH_POLICY.exact);
		}
		return startIndex;
	}

	@Override
	public void setStartIndex(int val) {
		int oldval = startIndex;
		long oldtime = startTime;

		startIndex = val;
		startTime = navigationData.getTime(val);

		pcs.firePropertyChange("endTime", endTime, oldval);
		if (!Objects.equals(startTime, oldtime)) {
			pcs.firePropertyChange("startTime", oldtime, startTime);
		}
	}

	@Override
	public int getEndIndex() {
		if (navigationData != null && endIndex < 0 && endTime > 0) {
			endIndex = navigationData.getNearestPointIndexForDate(endTime, MATCH_POLICY.exact);
		}
		return endIndex;
	}

	@Override
	public void setEndIndex(int val) {
		int oldval = endIndex;
		double oldtime = endTime;

		endIndex = val;
		endTime = navigationData.getTime(val);

		pcs.firePropertyChange("endIndex", endIndex, oldval);
		if (oldtime != endTime) {
			pcs.firePropertyChange("endTime", endTime, oldtime);
		}

	}

	@Override
	public IShiftVector createAndAddShiftVector(int navpointOriginIndex, double x, double y) {
		ShiftVectorImpl shiftVector = new ShiftVectorImpl(this);
		shiftVector.setEndCoord(x, y);
		shiftVector.setIndexOrigin(navpointOriginIndex);
		addShiftVector(shiftVector);
		return shiftVector;
	}

	@Override
	public IShiftVector createShiftVectorWithLonLat(int navpointOriginIndex, double lon, double lat) {
		ShiftVectorImpl shiftVector = new ShiftVectorImpl(this);
		shiftVector.setIndexOrigin(navpointOriginIndex);
		shiftVector.setEndLonLatCoord(lon, lat);
		return shiftVector;
	}

	@Override
	public void addShiftVector(IShiftVector shiftVector) {
		if (shiftVector != null) {
			shiftVectors.add((ShiftVectorImpl) shiftVector);
			((ShiftVectorImpl) shiftVector).setShiftPeriod(this);
			for (PropertyChangeListener listener : pcs.getPropertyChangeListeners()) {
				shiftVector.addPropertyChangeListener(listener);
			}
			pcs.firePropertyChange(IShiftPeriod.PROPERTY_VECTOR, null, shiftVector);
			pcs.firePropertyChange(IShiftPeriod.PROPERTY_VECTORS, null, shiftVectors);
		}
	}

	@Override
	public void removeShiftVector(IShiftVector shiftVector) {
		if (shiftVector != null) {
			for (PropertyChangeListener listener : pcs.getPropertyChangeListeners()) {
				shiftVector.removePropertyChangeListener(listener);
			}
			if (shiftVectors.remove(shiftVector)) {
				pcs.firePropertyChange(IShiftPeriod.PROPERTY_VECTOR, shiftVector, null);
				pcs.firePropertyChange(IShiftPeriod.PROPERTY_VECTORS, null, shiftVectors);
			}
		}
	}

	@Override
	public void removeAllShiftVector() {
		for (var vector : List.copyOf(shiftVectors)) {
			removeShiftVector(vector);
		}
	}

	@Override
	public List<IShiftVector> getShiftVectors() {
		return Collections.unmodifiableList(shiftVectors);
	}

	@Override
	public List<IShiftVector> getOrderedShiftVectors() {
		shiftVectors.sort((IShiftVector a, IShiftVector b) -> a.getIndexOrigin() - b.getIndexOrigin());
		return Collections.unmodifiableList(shiftVectors);

	}

	@Override
	public INavigationModel getNavigationDataSupplier() {
		return navigationData;
	}

	@Override
	public void setNavigationDataSupplier(INavigationModel data) {
		INavigationModel olddata = navigationData;
		if (olddata != data) {
			navigationData = data;

			if (startIndex == -1 && startTime != -1) {
				startIndex = navigationData.getNearestPointIndexForDate(startTime, MATCH_POLICY.exact);
			}
			if (endIndex == -1 && endTime != -1) {
				endIndex = navigationData.getNearestPointIndexForDate(endTime, MATCH_POLICY.exact);
			}

			pcs.firePropertyChange(ISessionContext.PROPERTY_NAVIGATION, olddata, navigationData);

			/*
			 * when deserializing, olddata will be null, but shiftVectors already exist, so we only reset shiftVectors
			 * list when actually changing (not setting from null) navigationData
			 */
			if (olddata != null) {
				shiftVectors = new ArrayList<>();
				pcs.firePropertyChange(IShiftPeriod.PROPERTY_VECTORS, shiftVectors, null);
			} else {
				for (ShiftVectorImpl shiftVector : shiftVectors) {
					if (shiftVector.navpointOriginIndex == -1 && shiftVector.timeOrigin != -1) {

						shiftVector.navpointOriginIndex = navigationData
								.getNearestPointIndexForDate(shiftVector.timeOrigin, MATCH_POLICY.exact);
						shiftVector.latlongOriginCoord[0] = data.getLongitude(shiftVector.navpointOriginIndex);
						shiftVector.latlongOriginCoord[1] = data.getLatitude(shiftVector.navpointOriginIndex);
					}
				}
			}
		}

	}

	/**
	 * centered on GeoBox (from navigationModel) Mercator Projection
	 * 
	 * @return
	 */
	@Override
	public Projection getProjection() {
		return mercatorProj;
	}

	@Override
	public void setProjection(Projection proj) {
		mercatorProj = proj;
	}

	/**
	 * Property Change supporting methods {@see @link{PropertyChangeSupport}}
	 */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
		shiftVectors.forEach(shiftVector -> shiftVector.addPropertyChangeListener(listener));
	}

	/**
	 * Property Change supporting methods {@see @link{PropertyChangeSupport}}
	 */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
		shiftVectors.forEach(shiftVector -> shiftVector.removePropertyChangeListener(listener));

	}

}
