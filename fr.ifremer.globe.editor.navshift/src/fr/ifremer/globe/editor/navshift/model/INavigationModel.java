package fr.ifremer.globe.editor.navshift.model;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.IDoubleArray;
import fr.ifremer.globe.utils.array.ILongArray;

/**
 * This interface provides methods to get navigation data.
 */
public interface INavigationModel extends Closeable, Iterable<Integer> {

	public enum MATCH_POLICY {
		exact,
		nearest,
		lower_value,
		higher_value
	}

	public int size();

	//// GETTERS

	public long getTime(int index);

	public double getLatitude(int index);

	public double getLongitude(int index);

	double getX(int index);

	double getY(int index);

	double getZ(int index);

	ILongArray getTimes();

	IDoubleArray getLatitudes();

	IDoubleArray getLongitudes();

	@Override
	public default Iterator<Integer> iterator() {
		return IntStream.range(0, size()).iterator();
	}

	/** Declare the close method without exception **/
	@Override
	public void close();

	/**
	 * @return the index of the nearest point in the navigation. Less than 0 if none
	 */
	int getNearestPointIndex(double[] point);

	/**
	 * find a specific index for a date in Navigation Data
	 * 
	 * @param dateval
	 * @param matchPolicy returned value depend on the matchPolicy {@link MATCH_POLICY}
	 * @return the index of the point with date == val, -1 if none
	 */
	public int getNearestPointIndexForDate(long dateval, MATCH_POLICY matchPolicy);

	/**
	 * Simple comparison method that returns True if first & second Navigation Data set are equals
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public boolean isEqual(INavigationModel other);

	/**
	 * Duplicates a NavigationModel, all data will be copied to new instance
	 * 
	 * @param arrayFactory
	 */
	public <T extends INavigationModel> T duplicate(IArrayFactory arrayFactory, IProgressMonitor progress);

	/**
	 * Copies data from another {@link INavigationModel}, all data will be copied to provided instance
	 * 
	 * @param arrayFactory
	 */
	public void copyFrom(INavigationModel input, IArrayFactory arrayFactory, IProgressMonitor progress);

	/**
	 * Simple comparison method that returns True if first & second Navigation Data set are equals
	 * 
	 * @param other a {@link INavigationModel} to compare to current one
	 * @param coordPrecision => the maximum tolerance to use to compare coordinates (diff between values must be < to
	 *            coordPrecision)
	 * @return {@link NavPointDifferences}, containing result and
	 */
	public NavPointDifferences compare(INavigationModel other, double coordPrecision);

	/**
	 * 
	 */
	class NavPointDifferences {

		public static final int DIFF_LONG = 1;
		public static final int DIFF_LAT = 1 << 1;
		public static final int DIFF_TIME = 1 << 2;

		List<Integer> differingIndexes;
		List<Integer> differingValues;

		public NavPointDifferences() {
			differingIndexes = new ArrayList<Integer>();
			differingValues = new ArrayList<Integer>();
		}

		public void addDiff(int index, int diffValues) {
			differingIndexes.add(index);
			differingValues.add(diffValues);
		}

		public int size() {
			return differingIndexes.size();
		}

		public int getDiff(int index) {
			return differingValues.get(index);
		}

		public int getIndex(int idx) {
			return differingIndexes.get(idx);
		}

	}

}
