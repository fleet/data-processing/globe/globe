package fr.ifremer.globe.editor.navshift.model.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.time.Instant;
import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.editor.navshift.application.event.VectorInfos;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.INavigationModel.MATCH_POLICY;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.IShiftVector;
import fr.ifremer.globe.editor.navshift.xstream.DoublesToLongLatConverter;
import fr.ifremer.globe.editor.navshift.xstream.LongToDateConverter;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.number.NumberUtils;

@XStreamAlias("ShiftVector")
public class ShiftVectorImpl implements IShiftVector {

	@XStreamOmitField
	protected int navpointOriginIndex = -1;
	@XStreamConverter(DoublesToLongLatConverter.class)
	@XStreamAlias("GeoCoord")
	protected double[] lonlatEndCoord = new double[2];
	@XStreamOmitField
	protected double[] latlongOriginCoord = new double[2];
	@XStreamConverter(LongToDateConverter.class)
	@XStreamAlias("NavPoint")
	protected long timeOrigin;
	@XStreamOmitField
	protected IShiftPeriod shiftPeriod;

	/**
	 * Variables use to prevent re-computation of mercator projection each time coordinates are accessed
	 */
	@XStreamOmitField
	protected double[] xyCoordOrigin = new double[3];
	@XStreamOmitField
	protected double azimut;
	@XStreamOmitField
	protected double amplitude;
	@XStreamOmitField
	protected double driftX;
	@XStreamOmitField
	protected double driftY;

	@XStreamOmitField
	protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	/**
	 * Constructors
	 */
	public ShiftVectorImpl() {
		super();
	}

	public ShiftVectorImpl(IShiftPeriod shiftPeriod) {
		this.shiftPeriod = shiftPeriod;
	}

	public ShiftVectorImpl(IShiftPeriod shiftPeriod, int navpointOriginIndex, double x, double y) {
		this(shiftPeriod);
		setEndCoord(x, y);
		setIndexOrigin(navpointOriginIndex);
	}

	public ShiftVectorImpl(IShiftPeriod shiftPeriod, VectorInfos vectorInfos) {
		this(shiftPeriod, vectorInfos.getStartPositionIndex(), vectorInfos.getStopPoint().x,
				vectorInfos.getStopPoint().y);
	}

	public static ShiftVectorImpl fromDrift(IShiftPeriod shiftPeriod, Instant time, double x, double y) {
		ShiftVectorImpl vector = new ShiftVectorImpl(shiftPeriod);
		vector.setTimeOrigin(time.toEpochMilli());
		vector.setDriftX(x);
		vector.setDriftY(y);
		return vector;
	}

	public ShiftVectorImpl(IShiftPeriod shiftPeriod, int navpointOriginIndex, double x, double y, String a) {
		this(shiftPeriod);
		setEndCoord(x, y);
		setIndexOrigin(navpointOriginIndex);
	}

	/**
	 * Adds a change listener to this object.
	 * 
	 * @param listener the listener to add
	 */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	/**
	 * Removes a change listener from this object.
	 * 
	 * @param listener the listener to remove
	 */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	@Override
	public double getOriginX() {
		return xyCoordOrigin[0];
	}

	@Override
	public double getOriginY() {
		return xyCoordOrigin[1];
	}

	@Override
	public double getOriginZ() {
		return xyCoordOrigin[2];
	}

	@Override
	public double getOriginLongitude() {
		return latlongOriginCoord[0];
	}

	@Override
	public double getOriginLatitude() {
		return latlongOriginCoord[1];
	}

	@Override
	public long getTimeOrigin() {
		return timeOrigin;
	}

	@Override
	public void setTimeOrigin(long time) {
		if (shiftPeriod != null && shiftPeriod.getNavigationDataSupplier() != null && time <= shiftPeriod.getEndTime()
				&& time >= shiftPeriod.getStartTime()) {
			int newOriginIndex = shiftPeriod.getNavigationDataSupplier().getNearestPointIndexForDate(time,
					MATCH_POLICY.lower_value);
			if (newOriginIndex != -1) {
				setIndexOrigin(newOriginIndex);
			}
		}
	}

	@Override
	public int getIndexOrigin() {

		if (shiftPeriod != null && shiftPeriod.getNavigationDataSupplier() != null && navpointOriginIndex < 0
				&& timeOrigin > 0) {
			navpointOriginIndex = shiftPeriod.getNavigationDataSupplier().getNearestPointIndexForDate(timeOrigin,
					MATCH_POLICY.exact);
		}
		return navpointOriginIndex;
	}

	@Override
	public void setIndexOrigin(int value) {
		int oldval = navpointOriginIndex;
		navpointOriginIndex = value;

		if (shiftPeriod != null) {

			INavigationModel navData = shiftPeriod.getNavigationDataSupplier();
			latlongOriginCoord[0] = navData.getLongitude(navpointOriginIndex);
			latlongOriginCoord[1] = navData.getLatitude(navpointOriginIndex);
			xyCoordOrigin[0] = navData.getX(navpointOriginIndex);
			xyCoordOrigin[1] = navData.getY(navpointOriginIndex);
			timeOrigin = navData.getTime(navpointOriginIndex);

			project();

			pcs.firePropertyChange(IShiftVector.PROPERTY_ORIGIN, oldval, value);
		}

	}

	@Override
	public double getDriftX() {
		return driftX;
	}

	@Override
	public void setDriftX(double drift) {
		double[] oldval = lonlatEndCoord;
		driftX = drift;
		compute(true);

		pcs.firePropertyChange(IShiftVector.PROPERTY_END, oldval, lonlatEndCoord);
	}

	@Override
	public double getDriftY() {
		return driftY;
	}

	@Override
	public void setDriftY(double drift) {

		double[] oldval = lonlatEndCoord;
		driftY = drift;

		compute(true);

		pcs.firePropertyChange(IShiftVector.PROPERTY_END, oldval, lonlatEndCoord);
	}

	@Override
	public double getAzimut() {
		return azimut;
	}

	@Override
	public void setAzimut(double newazimut) {

		double[] oldval = lonlatEndCoord;

		driftX = -amplitude * (Math.sin(Math.toRadians(newazimut)));
		driftY = -amplitude * (Math.cos(Math.toRadians(newazimut)));

		compute(true);

		pcs.firePropertyChange(IShiftVector.PROPERTY_END, oldval, lonlatEndCoord);
	}

	@Override
	public double getAmplitude() {
		return amplitude;
	}

	@Override
	public void setAmplitude(double newamplitude) {

		double[] oldval = lonlatEndCoord;
		double fact = newamplitude / amplitude;
		driftX = driftX * fact;
		driftY = driftY * fact;

		compute(true);

		pcs.firePropertyChange(IShiftVector.PROPERTY_END, oldval, lonlatEndCoord);
	}

	@Override
	public double[] getLonLatEndCoord() {
		return lonlatEndCoord;
	}

	@Override
	public double getEndLongitude() {
		return lonlatEndCoord[0];
	}

	@Override
	public double getEndLatitude() {
		return lonlatEndCoord[1];
	}

	@Override
	public void setEndCoord(double x, double y) {
		double[] oldval = lonlatEndCoord;

		driftX = xyCoordOrigin[0] - x;
		driftY = xyCoordOrigin[1] - y;

		compute(true);

		pcs.firePropertyChange(IShiftVector.PROPERTY_END, oldval, lonlatEndCoord);
	}
	
	@Override
	public void setEndLonLatCoord(double lon, double lat) {
		double[] oldval = lonlatEndCoord;
		lonlatEndCoord[0] = lon;
		lonlatEndCoord[1] = lat;
		compute(false);
		pcs.firePropertyChange(IShiftVector.PROPERTY_END, oldval, lonlatEndCoord);
	}

	/** {@inheritDoc} */
	@Override
	public void project() {
		INavigationModel navData = shiftPeriod.getNavigationDataSupplier();
		xyCoordOrigin[0] = navData.getX(navpointOriginIndex);
		xyCoordOrigin[1] = navData.getY(navpointOriginIndex);
		xyCoordOrigin[2] = navData.getZ(navpointOriginIndex);

		compute(false);
	}

	private void compute(boolean unprojectEndCoord) {

		if (shiftPeriod != null) {
			Projection projection = shiftPeriod.getProjection();

			if (unprojectEndCoord) {
				lonlatEndCoord = projection.unprojectQuietly(getOriginX() - driftX, getOriginY() - driftY);
			} else {
				double[] endXY = projection.projectQuietly(getEndLongitude(), getEndLatitude());
				driftX = xyCoordOrigin[0] - endXY[0];
				driftY = xyCoordOrigin[1] - endXY[1];
			}
		}

		amplitude = Math.sqrt(driftX * driftX + driftY * driftY);

		double ang1 = Math.toDegrees(Math.asin(driftX / amplitude));
		double ang2 = Math.toDegrees(Math.acos(-driftY / amplitude));
		if (ang1 > 0) {
			azimut = -ang2;
		} else {
			azimut = ang2;
		}

	}

	/**
	 * Used for setting shiftPeriod when deserializing
	 * 
	 * @param period
	 */
	public void setShiftPeriod(ShiftPeriodImpl period) {
		shiftPeriod = period;
	}

	public String getFormattedStringTime() {
		return DateUtils.formatDate(new Date(timeOrigin));
	}

	public String getFormattedStringAmplitude() {
		return NumberUtils.getStringMetersDouble(amplitude) + " m";
	}

	public String getFormattedStringAzimut() {
		return NumberUtils.getStringAngle(azimut);
	}

	@Override
	public String toString() {
		return "Shift vector [time origin=" + getFormattedStringTime() + ", azimut=" + getFormattedStringAzimut()
				+ ", amplitude=" + getFormattedStringAmplitude() + "]";
	}

}
