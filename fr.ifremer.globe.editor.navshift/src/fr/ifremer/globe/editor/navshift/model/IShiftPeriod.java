/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.model;

import java.beans.PropertyChangeListener;
import java.util.List;

import org.apache.commons.lang.math.IntRange;

import fr.ifremer.globe.core.model.projection.Projection;

public interface IShiftPeriod {

	String PROPERTY_VECTORS = "shiftVectors";
	String PROPERTY_VECTOR = "shiftVector";

	/*
	 * Times of start and end points for shiftPeriod
	 */
	long getStartTime();

	void setStartTime(long val);

	long getEndTime();

	void setEndTime(long val);

	/*
	 * Indexes of start and end points for shiftPeriod in navigation data
	 */
	int getStartIndex();

	void setStartIndex(int val);

	int getEndIndex();

	void setEndIndex(int val);

	default IntRange getIndexRange() {
		return new IntRange(getStartIndex(), getEndIndex());
	}

	void addShiftVector(IShiftVector shiftVector);

	/*
	 * Shift vectors manipulation
	 */
	IShiftVector createAndAddShiftVector(int navpointOriginIndex, double x, double y);

	IShiftVector createShiftVectorWithLonLat(int navpointOriginIndex, double x, double y);

	void removeShiftVector(IShiftVector vect);

	void removeAllShiftVector();

	List<IShiftVector> getShiftVectors();

	List<IShiftVector> getOrderedShiftVectors();

	/*
	 * Contextual data
	 */
	INavigationModel getNavigationDataSupplier();

	void setNavigationDataSupplier(INavigationModel data);

	Projection getProjection();

	void setProjection(Projection proj);

	/** Add a PropertyChangeListener to the listener list */
	void addPropertyChangeListener(PropertyChangeListener listener);

	/** Remove a PropertyChangeListener from the listener list */
	void removePropertyChangeListener(PropertyChangeListener listener);

}
