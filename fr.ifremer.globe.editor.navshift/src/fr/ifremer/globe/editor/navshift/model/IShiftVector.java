package fr.ifremer.globe.editor.navshift.model;

import java.beans.PropertyChangeListener;

/**
 * 
 * @author cguychard
 *
 */

public interface IShiftVector {

	/**
	 * Property names
	 */
	String PROPERTY_ORIGIN = "vectorOrigin";
	String PROPERTY_END = "vectorEnd";

	/**
	 * 
	 * @return
	 */
	double getOriginX();

	double getOriginY();

	double getOriginZ();

	double getOriginLongitude();

	double getOriginLatitude();

	long getTimeOrigin();

	void setTimeOrigin(long time);

	int getIndexOrigin();

	void setIndexOrigin(int value);

	double getDriftX();

	void setDriftX(double drift);

	double getDriftY();

	void setDriftY(double drift);

	double getAzimut();

	void setAzimut(double azimut);

	double getAmplitude();

	void setAmplitude(double amplitude);

	double[] getLonLatEndCoord();

	double getEndLongitude();

	double getEndLatitude();

	void setEndCoord(double x, double y);
	
	void setEndLonLatCoord(double lon, double lat);
	
	String getFormattedStringTime();
	
	String getFormattedStringAmplitude();
	
	String getFormattedStringAzimut();

	/** Adapt this vector using shiftPeriod provided projection */
	void project();

	/** Add a PropertyChangeListener to the listener list */
	void addPropertyChangeListener(PropertyChangeListener listener);

	/** Remove a PropertyChangeListener from the listener list */
	void removePropertyChangeListener(PropertyChangeListener listener);

}
