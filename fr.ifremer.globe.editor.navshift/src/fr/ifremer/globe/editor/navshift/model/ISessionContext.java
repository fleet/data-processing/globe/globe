/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.model;

import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.xstream.SessionContextConverter;

@XStreamAlias("ManualNavShift")
@XStreamConverter(SessionContextConverter.class)
public interface ISessionContext {

	String PROPERTY_NAVIGATION = "navigationData";
	String PROPERTY_NCINFOS_DATA = "sounderNcInfos";
	String PROPERTY_FILESET_DATA = "currentFileset";
	String PROPERTY_SHIFTPERIOD = "activeShiftPeriod";
	String PROPERTY_GEOBOX_DATA = "geoBox";
	String PROPERTY_PROFILES = "profiles";

	/**
	 * Getter for current active ShiftPeriod
	 *
	 * @return
	 */

	IShiftPeriod getShiftPeriod();

	void setShiftPeriod(IShiftPeriod wkz);

	/**
	 * Getter for the list of data files used to build the current Navigation Data
	 *
	 * @return
	 */
	List<File> getCurrentFileset();

	void setCurrentFileset(List<File> currentFileset);

	/**
	 * Getter of the considered set of {@link ISounderNcInfo}
	 */
	List<ISounderNcInfo> getSounderNcInfos();

	void setSounderNcInfos(List<ISounderNcInfo> sounderNcInfos);

	/**
	 * Getter of the considered set of {@link RasterInfo}
	 */
	List<RasterInfo> getReferenceRasterInfo();

	void setReferenceRasterInfo(List<RasterInfo> inputRasterInfo);

	/**
	 * Getter of the considered instance of {@link INavigationModel}
	 */
	INavigationModel getNavigationDataSupplier();

	void setNavigationDataSupplier(INavigationModel navData);

	/**
	 * Getter of geoBox
	 */
	GeoBox getGeoBox();

	void setGeoBox(GeoBox geoBox);

	/**
	 * Getter of profiles
	 */
	List<IProfile> getProfiles();

	/**
	 * Getter of Grid DTM resolution
	 */
	double getDtmResolution();

	/**
	 * Setter of Grid DTM resolution
	 */
	void setDtmResolution(double dtmResolution);

	/**
	 * Getter of Isobath main line (every x lines)
	 */
	int getIsobathMainLine();

	/**
	 * Setter of Isobath main line (every x lines)
	 */
	void setIsobathMainLine(int isobathMainLine);

	/**
	 * Getter of Distance between two secondary lines
	 */
	double getIsobathStep();

	/**
	 * Setter of Distance between two secondary lines
	 */
	void setIsobathStep(double isobathStep);

	/**
	 * Getter of % of points expected per lines.
	 */
	int getIsobathSampling();

	/**
	 * Setter of % of points expected per lines.
	 */
	void setIsobathSampling(int isobathSampling);

	void setProfiles(List<? extends IProfile> profiles);

	/** Add a PropertyChangeListener to the listener list */
	void addPropertyChangeListener(PropertyChangeListener listener);

	/** Remove a PropertyChangeListener from the listener list */
	void removePropertyChangeListener(PropertyChangeListener listener);

	/**
	 * Imports {@link IShiftVector} from an XML session file or a JSON file (created by Python process).
	 * 
	 * @param xmlFilePath : input file with vectors.
	 * @return imported vectors count.
	 */
	void importShiftVectors(File xmlFile);

}
