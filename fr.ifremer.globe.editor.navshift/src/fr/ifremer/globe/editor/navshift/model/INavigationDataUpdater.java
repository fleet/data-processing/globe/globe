/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.model;

/**
 * 
 * refinement of the INavigationDataSupplier interface in the case where the data provider can also update data
 * 
 * @author APSIDE
 *
 */
public interface INavigationDataUpdater extends INavigationModel {

	void updateNavPoint(int index, long time, double longitude, double latitude);

	/** Set the value of projected longitude */
	void setX(int index, double value);

	/** Set the value of projected latitude */
	void setY(int index, double value);

}
