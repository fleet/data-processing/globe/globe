/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.viewer2d.toolbox.components;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.Assert;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.utils.latlon.FormatLatitudeLongitude;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.application.handlers.CommandExecutor;
import fr.ifremer.globe.editor.navshift.commons.Constants;
import fr.ifremer.globe.editor.navshift.commons.NavShiftPreference;
import fr.ifremer.globe.editor.navshift.controller.SessionController;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.IShiftVector;
import fr.ifremer.globe.editor.navshift.process.ComputeNavShiftService;
import fr.ifremer.globe.editor.navshift.process.Previewer;
import fr.ifremer.globe.editor.navshift.wizard.restore.RestoreSessionContextModel;
import fr.ifremer.globe.editor.navshift.wizard.restore.RestoreSessionContextWizard;
import fr.ifremer.globe.ui.TableViewerColumnSorter;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.number.NumberUtils;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

/**
 * Composite used to display shift vectors.
 */
public class ShiftVectorComposite extends Composite implements PropertyChangeListener {

	/** Event broker service */
	@Inject
	private IEventBroker eventBroker;

	/** session controller **/
	@Named(ContextNames.SESSION_CONTROLLER)
	@Inject
	SessionController sessionController;

	/** Saved preferences */
	@Inject
	@Named(ContextNames.NAVSHIFT_PREFERENCE)
	private NavShiftPreference preferences;

	/** Current session */
	private ISessionContext sessionContext;

	private Button btnPreview;
	private Button btnDelete;
	private Button btnDeleteAll;
	private Button btnCompute;
	private Button btnLoadFromContext;
	private Table vectorsTable;
	private TableViewer vectorsTableViewer;
	private Group grpShiftVectors;

	/**
	 * Listeners
	 */
	private SelectionListener btnPreviewListener = SelectionListener
			.widgetSelectedAdapter(selectionEvent -> CommandExecutor.executeCommand(CommandExecutor.PREVIEW_COMMAND));
	private ISelectionChangedListener vectorsSelectionListener = event -> onVectorsSelection(
			event.getStructuredSelection());
	private KeyListener vectorsTableKeyListener = KeyListener.keyPressedAdapter(this::onKeyPressed);

	/** True when {@link #hookListeners()} has been called */
	private boolean listenersHooked = false;

	private static final String LOAD_FROM_CONTEXT_TITLE = "Import vectors from context file...";
	private static final String SETTING_SELECTION = "SETTING_SELECTION";

	/**
	 * Create the composite.
	 */
	public ShiftVectorComposite(Composite parent) {
		super(parent, SWT.NONE);
		createUI();
		// Injection
		if (ContextInitializer.getEclipseContext() != null) {
			ContextInjectionFactory.inject(this, ContextInitializer.getEclipseContext());
		}
	}

	/**
	 * Creates the UI.
	 */
	private void createUI() {
		FillLayout fillLayout = new FillLayout(SWT.HORIZONTAL);
		fillLayout.marginHeight = 3;
		fillLayout.marginWidth = 3;
		setLayout(fillLayout);

		grpShiftVectors = new Group(this, SWT.NONE);
		grpShiftVectors.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpShiftVectors.setLayout(new GridLayout(5, false));
		grpShiftVectors.setText("Step 2 - Shift vectors");

		vectorsTableViewer = createVectorsTable(grpShiftVectors);

		btnDelete = new Button(grpShiftVectors, SWT.NONE);
		btnDelete.setSize(60, 25);
		btnDelete.setImage(Icons.DELETE.toImage());
		btnDelete.setEnabled(false);
		btnDelete.setText("Delete");
		btnDelete.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> deleteSelectedVector()));

		btnDeleteAll = new Button(grpShiftVectors, SWT.NONE);
		btnDeleteAll.setSize(60, 25);
		btnDeleteAll.setImage(Icons.DELETE.toImage());
		btnDeleteAll.setEnabled(false);
		btnDeleteAll.setText("Delete all");
		btnDeleteAll.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> deleteAllVectors()));

		btnLoadFromContext = new Button(grpShiftVectors, SWT.NONE);
		btnLoadFromContext.setText("Import...");
		btnLoadFromContext.setToolTipText(LOAD_FROM_CONTEXT_TITLE);
		btnLoadFromContext.setImage(Icons.IMPORT.toImage());
		btnLoadFromContext.setEnabled(false);
		btnLoadFromContext
				.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> loadVectorsFromContextFile()));

		btnCompute = new Button(grpShiftVectors, SWT.NONE);
		btnCompute.setImage(Icons.PYTHON.toImage());
		btnCompute.setText("Compute...");
		btnCompute.setToolTipText("Compute shift with PyAT");
		btnCompute.setEnabled(false);
		btnCompute.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> onComputeNavShift()));

		btnPreview = new Button(grpShiftVectors, SWT.NONE);
		btnPreview.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnPreview.setImage(ResourceManager.getPluginImage(Constants.PLUGIN_ID, "icons/vector.png"));
		btnPreview.setText("Preview");
	}

	@Override
	public void setEnabled(boolean enabled) {
		grpShiftVectors.setFont(SWTResourceManager.getFont("Segoe UI", 9, enabled ? SWT.BOLD : SWT.NORMAL));
		boolean vectorsExist = sessionContext.getShiftPeriod() != null
				&& !sessionContext.getShiftPeriod().getShiftVectors().isEmpty();
		btnLoadFromContext.setEnabled(enabled);
		btnCompute.setEnabled(enabled);
		btnDelete.setEnabled(enabled && vectorsExist);
		btnDeleteAll.setEnabled(enabled && vectorsExist);
		btnPreview.setEnabled(enabled && vectorsExist);
		vectorsTable.setEnabled(enabled);
		if (enabled) {
			setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		} else {
			setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		}
	}

	/**
	 * Hook all needed listeners
	 */
	private void hookListeners() {
		Assert.isNotNull(!listenersHooked, "hookListeners() called twice");
		Assert.isNotNull(sessionContext, "Invalid state, sessionContext can not be null");

		sessionContext.addPropertyChangeListener(this);
		if (sessionContext.getShiftPeriod() != null) {
			sessionContext.getShiftPeriod().addPropertyChangeListener(this);
		}

		btnPreview.addSelectionListener(btnPreviewListener);

		vectorsTableViewer.addSelectionChangedListener(vectorsSelectionListener);
		vectorsTableViewer.getTable().addKeyListener(vectorsTableKeyListener);

		listenersHooked = true;
	}

	/** shift vectors have been designated in the table */
	private void onVectorsSelection(IStructuredSelection structuredSelection) {
		if (!Boolean.TRUE.equals(vectorsTable.getData(SETTING_SELECTION))) {
			eventBroker.send(NavShiftEventTopics.TOPIC_VECTORS_SELECTED, getSelectedVectors());
		}
	}

	/** Called to compute shift with Python process */
	private void onComputeNavShift() {
		// Check if reference DTM is available
		if (sessionController.getSessionContext().getReferenceRasterInfo().isEmpty()) {
			Messages.openInfoMessage("Compute shift", "Compute shift process requires a reference DTM.");
			return;
		}
		ComputeNavShiftService.get().ifPresentOrElse(ComputeNavShiftService::launchInForeground, () -> Messages
				.openInfoMessage("Compute shift process not available", "Compute shift process not found in toolbox."));
	}

	/**
	 * @see org.eclipse.swt.widgets.Widget#dispose()
	 */
	@Override
	public void dispose() {
		unhookListener();
		super.dispose();
	}

	/**
	 * Unhook all listeners set in sendConstruct
	 */
	@PreDestroy
	private void unhookListener() {
		if (sessionContext != null) {
			sessionContext.removePropertyChangeListener(this);
			sessionContext = null;
		}

		Display.getDefault().syncExec(() -> {
			if (!isDisposed()) {
				btnPreview.removeSelectionListener(btnPreviewListener);
				vectorsTableViewer.removeSelectionChangedListener(vectorsSelectionListener);
				vectorsTableViewer.getTable().removeKeyListener(vectorsTableKeyListener);
			}
		});

		listenersHooked = false;
	}

	/**
	 * @see org.eclipse.swt.widgets.Composite#checkSubclass()
	 */
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	/**
	 * SessionContext changed. Called by DI only
	 */
	@Inject
	public void setSessionContext(@Optional @Named(ContextNames.SESSION_CONTEXT) ISessionContext newSessionContext) {
		// Session closed. Empty the composite
		if (newSessionContext == null) {
			unhookListener();
			// Display.getDefault().syncExec(() -> {
			// vectorsTableViewer.setInput(null);
			// });
		}
		sessionContext = newSessionContext;
	}

	/** Session has been activated successfully */
	@Inject
	@Optional
	public void onContextActivated(
			@UIEventTopic(NavShiftEventTopics.SESSION_ACTIVATED) ISessionContext activatedSessionContext) {
		Assert.isNotNull(sessionContext, "Invalid state, sessionContext can not be null");
		Assert.isTrue(sessionContext == activatedSessionContext, "Invalid state, wrong activated session");
		Display.getDefault().syncExec(() -> {
			if (!isDisposed()) {
				hookListeners();
				if (sessionContext.getShiftPeriod() != null) {
					vectorsTableViewer.setInput(sessionContext.getShiftPeriod().getShiftVectors());
				}
				onPreview(java.util.Optional.empty());
			}
		});
	}

	/**
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		Display.getDefault().asyncExec(() -> {
			if (ISessionContext.PROPERTY_NAVIGATION.equals(event.getPropertyName())) {

			} else if (ISessionContext.PROPERTY_SHIFTPERIOD.equals(event.getPropertyName())) {
				if (event.getNewValue() != null) {
					IShiftPeriod period = (IShiftPeriod) event.getNewValue();
					period.addPropertyChangeListener(this);
					vectorsTableViewer.setInput(((IShiftPeriod) event.getNewValue()).getShiftVectors());
					setEnabled(true);
				}
				if (event.getOldValue() != null) {
					IShiftPeriod old = (IShiftPeriod) event.getOldValue();
					old.removePropertyChangeListener(this);
				}
			} else if (IShiftPeriod.PROPERTY_VECTOR.equals(event.getPropertyName())) {
				vectorsTableViewer.refresh(true);
				// Vector added...
				if (event.getNewValue() != null) {
					vectorsTableViewer.setSelection(new StructuredSelection(event.getNewValue()), true);
				}
			} else if (IShiftVector.PROPERTY_END.equals(event.getPropertyName())) {
				// Vector modified
				if (!vectorsTableViewer.isCellEditorActive()) {
					vectorsTableViewer.refresh(true);
				}
			} else if (IShiftPeriod.PROPERTY_VECTORS.equals(event.getPropertyName())) {
				@SuppressWarnings("unchecked")
				List<IShiftVector> shiftVectors = (List<IShiftVector>) event.getNewValue();
				btnPreview.setEnabled(shiftVectors != null && !shiftVectors.isEmpty());
				btnDeleteAll.setEnabled(shiftVectors != null && !shiftVectors.isEmpty());
			}
		});
	}

	/**
	 * Creates table viewer for Vectors list
	 */
	private TableViewer createVectorsTable(Composite parent) {

		Label lblDefineShiftVectors = new Label(grpShiftVectors, SWT.NONE);
		lblDefineShiftVectors.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 5, 1));
		lblDefineShiftVectors.setText("Define shift vectors .");
		TableViewer tableViewer = new TableViewer(parent, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
		vectorsTable = tableViewer.getTable();
		vectorsTable.setLinesVisible(true);
		vectorsTable.setHeaderVisible(true);
		initContextMenu(vectorsTable);
		GridData gdVectorsTable = new GridData(SWT.FILL, SWT.FILL, true, true, 5, 1);
		gdVectorsTable.minimumHeight = 120;
		vectorsTable.setLayoutData(gdVectorsTable);
		tableViewer.setContentProvider(ArrayContentProvider.getInstance());

		defineVectorsTableColumns(tableViewer);

		return tableViewer;
	}

	/** Delete the selected vector required */
	private void onKeyPressed(KeyEvent event) {
		if (event.keyCode == SWT.DEL) {
			deleteSelectedVector();
		}
	}

	/** Launch the command to delete the selected vector */
	private void deleteSelectedVector() {
		List<IShiftVector> vectors = getSelectedVectors();

		if (!vectors.isEmpty() && Messages.openSyncQuestionMessage("Delete vector...",
				"Delete the vector" + (vectors.size() > 1 ? "s ?" : " ?"))) {
			vectors.forEach(v -> CommandExecutor.executeCommand(CommandExecutor.DELETE_VECTOR_COMMAND, v));
		}

	}

	/**
	 * @return the list of selected vectors in the table
	 */
	protected List<IShiftVector> getSelectedVectors() {
		return Arrays.stream(vectorsTableViewer.getStructuredSelection().toArray()) //
				.filter(IShiftVector.class::isInstance) //
				.map(IShiftVector.class::cast) //
				.collect(Collectors.toList());
	}

	/**
	 * Setup a contextual menu on {@link IShiftVector} table
	 */
	private void initContextMenu(Table table) {
		Menu menuTable = new Menu(table);
		table.setMenu(menuTable);

		// Create menu items
		MenuItem deleteMenu = new MenuItem(menuTable, SWT.NONE);
		deleteMenu.setText("Delete vector");
		deleteMenu.addSelectionListener(
				SelectionListener.widgetSelectedAdapter(selectionEvent -> deleteSelectedVector()));

		MenuItem deleteAllMenu = new MenuItem(menuTable, SWT.NONE);
		deleteAllMenu.setText("Delete all vectors");
		deleteAllMenu
				.addSelectionListener(SelectionListener.widgetSelectedAdapter(selectionEvent -> deleteAllVectors()));

		table.addListener(SWT.MenuDetect, event -> {
			if (table.getSelectionCount() <= 0) {
				event.doit = false;
			} else {
				deleteMenu.setText("Delete selected " + (table.getSelectionCount() > 1 ? "vectors" : "vector"));
			}
		});

	}

	private void deleteAllVectors() {
		boolean result = Messages.openSyncQuestionMessage("Delete vector...",
				"Are you sure you want to delete ALL vectors ?");
		if (result) {
			new ArrayList<>(sessionContext.getShiftPeriod().getShiftVectors()).forEach(v -> {
				CommandExecutor.executeCommand(CommandExecutor.DELETE_VECTOR_COMMAND, v);
			});
		}
	}

	/** Defines the columns of the vectorsTableViewer */
	private void defineVectorsTableColumns(TableViewer vectorsTableViewer) {
		defineEndLongitudeColumn(vectorsTableViewer);
		defineEndLatitudeColumn(vectorsTableViewer);
		defineVectorOriginTimeColumn(vectorsTableViewer);
		defineAmplitudeColumn(vectorsTableViewer);
		defineAzimuthColumn(vectorsTableViewer);
		defineDriftXColumn(vectorsTableViewer);
		defineDriftYColumn(vectorsTableViewer);
	}

	/** Defines the time column of the vectorsTableViewer */
	private void defineVectorOriginTimeColumn(TableViewer tableViewer) {
		TableViewerColumn timeOfOriginViewerColumn = new TableViewerColumn(tableViewer, SWT.RIGHT);
		timeOfOriginViewerColumn.getColumn().setWidth(140);
		timeOfOriginViewerColumn.getColumn().setText("Start Time");
		timeOfOriginViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return DateUtils.formatDate(new Date(((IShiftVector) element).getTimeOrigin()));
			}
		});
		new TableViewerColumnSorter(timeOfOriginViewerColumn);
	}

	/** Defines the latitude column of the vectorsTableViewer */
	private void defineEndLatitudeColumn(TableViewer tableViewer) {
		TableViewerColumn latitudeTableViewerColumn = new TableViewerColumn(tableViewer, SWT.RIGHT);
		latitudeTableViewerColumn.getColumn().setWidth(100);
		latitudeTableViewerColumn.getColumn().setText("End Latitude");
		latitudeTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return FormatLatitudeLongitude.latitudeToString(((IShiftVector) element).getEndLatitude());
			}
		});
		new TableViewerColumnSorter(latitudeTableViewerColumn);
	}

	/** Defines the longitude column of the vectorsTableViewer */
	private void defineEndLongitudeColumn(TableViewer tableViewer) {
		TableViewerColumn longitudeTableViewerColumn = new TableViewerColumn(tableViewer, SWT.RIGHT);
		longitudeTableViewerColumn.getColumn().setWidth(100);
		longitudeTableViewerColumn.getColumn().setText("End Longitude");
		longitudeTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return FormatLatitudeLongitude.longitudeToString(((IShiftVector) element).getEndLongitude());
			}
		});
		new TableViewerColumnSorter(longitudeTableViewerColumn);
	}

	private Runnable editingListener = () -> eventBroker.send(NavShiftEventTopics.TOPIC_VECTOR_EDITED, null);

	/** Defines the longitude column of the vectorsTableViewer */
	private void defineAmplitudeColumn(TableViewer tableViewer) {
		TableViewerColumn amplitudeTableViewerColumn = new TableViewerColumn(tableViewer, SWT.RIGHT);
		amplitudeTableViewerColumn.getColumn().setWidth(70);
		amplitudeTableViewerColumn.getColumn().setText("Amplitude");
		amplitudeTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((IShiftVector) element).getFormattedStringAmplitude();
			}
		});
		new TableViewerColumnSorter(amplitudeTableViewerColumn);
		amplitudeTableViewerColumn.setEditingSupport(new DoubleEditingSupport<>(tableViewer, 1d, 500d, 1d, 3,
				IShiftVector::getAmplitude, IShiftVector::setAmplitude, editingListener));
	}

	/** Defines the longitude column of the vectorsTableViewer */
	private void defineAzimuthColumn(TableViewer tableViewer) {
		TableViewerColumn azimutTableViewerColumn = new TableViewerColumn(tableViewer, SWT.RIGHT);
		azimutTableViewerColumn.getColumn().setWidth(70);
		azimutTableViewerColumn.getColumn().setText("Azimut");
		azimutTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return ((IShiftVector) element).getFormattedStringAzimut();
			}
		});
		new TableViewerColumnSorter(azimutTableViewerColumn);
		azimutTableViewerColumn.setEditingSupport(new DoubleEditingSupport<>(tableViewer, -180d, 180, 1d, 2,
				IShiftVector::getAzimut, IShiftVector::setAzimut, editingListener));
	}

	/** Defines the longitude column of the vectorsTableViewer */
	private void defineDriftXColumn(TableViewer tableViewer) {
		TableViewerColumn driftTableViewerColumn = new TableViewerColumn(tableViewer, SWT.RIGHT);
		driftTableViewerColumn.getColumn().setWidth(70);
		driftTableViewerColumn.getColumn().setText("DriftX");
		driftTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return NumberUtils.getStringMetersDouble(((IShiftVector) element).getDriftX());
			}
		});
		new TableViewerColumnSorter(driftTableViewerColumn);
		driftTableViewerColumn.setEditingSupport(new DoubleEditingSupport<>(tableViewer, -5000d, 5000d, 0.1d, 3,
				IShiftVector::getDriftX, IShiftVector::setDriftX, editingListener));
	}

	/** Defines the longitude column of the vectorsTableViewer */
	private void defineDriftYColumn(TableViewer tableViewer) {
		TableViewerColumn driftTableViewerColumn = new TableViewerColumn(tableViewer, SWT.RIGHT);
		driftTableViewerColumn.getColumn().setWidth(70);
		driftTableViewerColumn.getColumn().setText("DriftY");
		driftTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return NumberUtils.getStringMetersDouble(((IShiftVector) element).getDriftY());
			}
		});
		new TableViewerColumnSorter(driftTableViewerColumn);
		driftTableViewerColumn.setEditingSupport(new DoubleEditingSupport<>(tableViewer, -5000d, 5000d, 0.1d, 3,
				IShiftVector::getDriftY, IShiftVector::setDriftY, editingListener));
	}

	/**
	 * Event handler for DRAW_VECTOR
	 */
	@Inject
	@Optional
	private void onVectorSelected(
			@UIEventTopic(NavShiftEventTopics.TOPIC_VECTORS_SELECTED) List<IShiftVector> shiftVectors) {
		if (isDisposed()) {
			return;
		}

		if (!shiftVectors.isEmpty()) {
			vectorsTable.getDisplay().syncExec(() -> {
				vectorsTable.setData(SETTING_SELECTION, Boolean.TRUE);
				vectorsTableViewer.setSelection(new StructuredSelection(shiftVectors), true);
				vectorsTable.setData(SETTING_SELECTION, Boolean.FALSE);
			});
			btnDelete.setEnabled(true);
		} else {
			btnDelete.setEnabled(false);
		}
	}

	/** Handle the SHIFTPERIOD_SELECTION_STOP event */
	@Inject
	@Optional
	private void onStartSelectingShiftPeriod(
			@UIEventTopic(NavShiftEventTopics.TOPIC_SHIFTPERIOD_SELECTION_START) Object poster) {
		if (isDisposed()) {
			return;
		}
		setEnabled(false);
	}

	/** Handle the SHIFTPERIOD_SELECTION_STOP event */
	@Inject
	@Optional
	private void stopSelectingShiftPeriod(
			@UIEventTopic(NavShiftEventTopics.TOPIC_SHIFTPERIOD_SELECTION_STOP) Object poster) {
		if (isDisposed()) {
			return;
		}
		setEnabled(true);
	}

	/**
	 * Event handler for preview mode
	 */
	@Inject
	@Optional
	private void onPreview(@UIEventTopic(NavShiftEventTopics.PREVIEW) java.util.Optional<Previewer> option) {
		if (isDisposed()) {
			return;
		}
		setEnabled(!option.isPresent());
	}

	/**
	 * Opens a wizard to load vectors from a context file.
	 */
	private void loadVectorsFromContextFile() {
		RestoreSessionContextModel restoreSessionModel = new RestoreSessionContextModel();
		// Restore from preferences
		Path prefSessionFilename = preferences.getSessionFile().getValue();
		if (prefSessionFilename != null) {
			File sessionFile = prefSessionFilename.toFile();
			if (sessionFile.exists()) {
				restoreSessionModel.getSelectedFile().set(sessionFile);
			}
		}
		RestoreSessionContextWizard restoreSessionWizard = new RestoreSessionContextWizard(restoreSessionModel);
		restoreSessionWizard.setWindowTitle(LOAD_FROM_CONTEXT_TITLE);

		WizardDialog wizardDialog = new WizardDialog(getShell(), restoreSessionWizard);
		wizardDialog.setMinimumPageSize(300, 510);

		if (wizardDialog.open() == Window.OK) {
			var xmlFile = restoreSessionModel.getSelectedFile().get();
			try {
				sessionContext.importShiftVectors(restoreSessionModel.getSelectedFile().get());
			} catch (Exception e) {
				Messages.openErrorMessage("File Error", "Unable to load vector from file : " + xmlFile.getName(), e);
			}
		}
	}

	/**
	 * Event handler for TOPIC_FILES_SHIFTED<br>
	 * Some files have been shifted. Delete all vectors if necessary
	 */
	@Inject
	@Optional
	private void onFilesShifted(@UIEventTopic(NavShiftEventTopics.TOPIC_FILES_SHIFTED) List<File> files) {
		// Search if a profile has been modified
		boolean oneProfileModified = sessionContext.getProfiles().stream()//
				.map(IProfile::getSounderNcInfo)//
				.map(ISounderNcInfo::toFile)//
				.anyMatch(files::contains);
		if (oneProfileModified) {
			getDisplay().syncExec(() -> {
				vectorsTableViewer.setInput(null);
			});
		}
	}

}
