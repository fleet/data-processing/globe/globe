package fr.ifremer.globe.editor.navshift.viewer2d.toolbox.components.explorer;

import java.util.ArrayList;
import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeViewerEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.commons.IsobathParametersFactory;
import fr.ifremer.globe.editor.navshift.controller.Viewer2DLayerController;
import fr.ifremer.globe.editor.navshift.process.Previewer;
import fr.ifremer.globe.editor.navshift.viewer2d.NavShift2DViewer;
import fr.ifremer.globe.ui.providers.LayerCheckProvider;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNodeContentProvider;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerBaseNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNodeLabelProvider;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode.CheckState;
import fr.ifremer.viewer3d.layers.lineisobaths.IsobathLayer;
import fr.ifremer.viewer3d.layers.lineisobaths.tools.IsobathTopics;

public class ExplorerComposite extends Composite {

	/** Event broker service */
	@Inject
	protected IEventBroker eventBroker;

	@Inject
	@Named(ContextNames.VIEWER_2D_LAYER_CONTROLLER)
	protected Viewer2DLayerController viewer2DLayerController;

	/** Isobath parameters factory */
	@Inject
	@Named(ContextNames.ISOBATH_PARAM_FACTORY)
	protected IsobathParametersFactory isobathParametersFactory;

	private CheckboxTreeViewer treeViewer;
	private TreeNode lastNodeChecked = null;
	private boolean lastValue;
	protected List<TreeNode> treeNodes = new ArrayList<>();
	protected List<TreeNode> nodeStateBeforePreview;
	private IsobathGroupNode dtmGroupNode;
	private IsobathGroupNode processedIsobathGroupNode;
	private DisposeListener disposeListener = e -> onDispose();

	/**
	 * TODO : due to a bug, selection service is provided by the {@link NavShift2DViewer} instead of retrieve it by
	 * injection. (not the same injected here than in {@link NavShift2DViewer}). To fix.
	 */
	private ESelectionService selectionService;

	private IsobathGroupNode previewIsobathGroupNode;

	/**
	 * Constructor
	 */
	public ExplorerComposite(Composite parent) {
		super(parent, SWT.NONE);
		setBackground(SWTResourceManager.getColor(SWT.COLOR_LIST_BACKGROUND));
		createUI();
		// Injection
		if (ContextInitializer.getEclipseContext() != null) {
			ContextInjectionFactory.inject(this, ContextInitializer.getEclipseContext());
		}
		addDisposeListener(disposeListener);
	}

	/**
	 * Creates the UI.
	 */
	private void createUI() {
		FillLayout fillLayout = new FillLayout(SWT.HORIZONTAL);
		fillLayout.marginHeight = 3;
		fillLayout.marginWidth = 3;
		setLayout(fillLayout);

		Group grpDisplay = new Group(this, SWT.NONE);
		grpDisplay.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		grpDisplay.setText("Display");
		GridLayout glGrpDisplay = new GridLayout(1, false);
		glGrpDisplay.marginWidth = 0;
		grpDisplay.setLayout(glGrpDisplay);

		treeViewer = new CheckboxTreeViewer(grpDisplay, SWT.NONE);
		Tree tree = treeViewer.getTree();
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		treeViewer.setCheckStateProvider(new LayerCheckProvider());
		treeViewer.setUseHashlookup(true);
		treeViewer.setContentProvider(new TreeNodeContentProvider());
		treeViewer.setCellEditors(new CellEditor[] { new TextCellEditor(treeViewer.getTree()) });
		treeViewer.setColumnProperties(new String[] { "name", "value" });
		treeViewer.setLabelProvider(new TreeNodeLabelProvider());

		TreeViewerEditor.create(treeViewer, new ColumnViewerEditorActivationStrategy(treeViewer) {
			@Override
			protected boolean isEditorActivationEvent(ColumnViewerEditorActivationEvent event) {
				return event.eventType == ColumnViewerEditorActivationEvent.PROGRAMMATIC;
			}
		}, ColumnViewerEditor.DEFAULT);

		treeViewer.setAutoExpandLevel(3);
		treeViewer.getControl().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1));

		treeViewer.addSelectionChangedListener(event -> {
			if (selectionService != null) {
				selectionService.setSelection(event.getSelection());
			}
		});
		treeViewer.addCheckStateListener(event -> setChecked(event.getElement(), event.getChecked()));
	}

	/**
	 * After injection, sets the treeViewer inputs.
	 */
	@PostConstruct
	public void intialize() {
		// Shift period
		treeNodes.add(new PeriodNode(viewer2DLayerController.getShiftLayerComposite()));

		// Isobath group node
		IsobathGroupNode isobathGroupNode = new IsobathGroupNode("Isobaths", isobathParametersFactory.get());
		treeNodes.add(isobathGroupNode);

		// Reference DTM
		if (!viewer2DLayerController.getReferenceDtmIsobathLayer().isEmpty()) {
			dtmGroupNode = new IsobathGroupNode("Reference DTM", isobathParametersFactory.getReferenceSynch());
			viewer2DLayerController.getReferenceDtmIsobathLayer()
					.forEach(il -> dtmGroupNode.addChild(new LayerNode(il)));
			isobathGroupNode.addChild(dtmGroupNode);
		}

		// Processed profile isobaths
		processedIsobathGroupNode = new IsobathGroupNode("Processed profiles", isobathParametersFactory.getSynch());
		viewer2DLayerController.getProfilesIsobathLayer()
				.forEach(il -> processedIsobathGroupNode.addChild(new LayerNode(il)));
		isobathGroupNode.addChild(processedIsobathGroupNode);

		treeViewer.setInput(treeNodes.toArray());

		// Preview isobaths group node
		previewIsobathGroupNode = new IsobathGroupNode("Preview", isobathParametersFactory.getPreviewSynch());
	}

	/**
	 * Event handler for preview mode
	 */
	@Inject
	@Optional
	protected void onPreview(@UIEventTopic(NavShiftEventTopics.PREVIEW) java.util.Optional<Previewer> previewer,
			MApplication application, EPartService partService) {
		if (isDisposed()) {
			return;
		}
		if (previewer.isPresent()) {
			// save checked node
			nodeStateBeforePreview = new ArrayList<>();

			if (processedIsobathGroupNode.getState() != CheckState.FALSE) {
				processedIsobathGroupNode.setChecked(false);
				nodeStateBeforePreview.add(processedIsobathGroupNode);
			}

			treeNodes.stream().filter(node -> !(node instanceof GroupNode) && node.getState() == CheckState.TRUE)
					.forEach(node -> {
						node.setChecked(false);
						nodeStateBeforePreview.add(node);
					});

			// recompute preview isobath nodes
			previewIsobathGroupNode.getChildren().clear();
			for (IsobathLayer isobathLayer : viewer2DLayerController.getPreviewIsobathLayer()) {
				previewIsobathGroupNode.addChild(new LayerNode(isobathLayer));
			}
			treeNodes.add(previewIsobathGroupNode);

			// refresh
			treeViewer.setInput(treeNodes.toArray());
			treeViewer.refresh();
			if (eventBroker != null) {
				eventBroker.post(IsobathTopics.TOPIC_ISOBATH_UPDATED, null);
			}

		} else {
			nodeStateBeforePreview.forEach(node -> node.setChecked(true));
			nodeStateBeforePreview = null;
			treeNodes.remove(previewIsobathGroupNode);
			treeViewer.setInput(treeNodes.toArray());
			treeViewer.refresh();
			if (eventBroker != null) {
				eventBroker.post(IsobathTopics.TOPIC_ISOBATH_UPDATED, null);
			}
		}
	}

	public void setChecked(Object elt, boolean checked) {
		if (lastNodeChecked == elt && lastNodeChecked.getState() == CheckState.PARTIAL) {
			checked = !lastValue;
		}

		parseChild((TreeNode) elt, checked);
		lastNodeChecked = (TreeNode) elt;
		lastValue = checked;
		if (eventBroker != null) {
			eventBroker.post(IsobathTopics.TOPIC_ISOBATH_UPDATED, null);
		}
	}

	private void parseChild(TreeNode elt, boolean checked) {
		if (elt instanceof GroupNode) {
			for (TreeNode child : ((GroupNode) elt).getChildren()) {
				parseChild(child, checked);
			}
		} else if (elt instanceof LayerBaseNode) {
			elt.setChecked(checked);
		}
		treeViewer.refresh();
	}

	public void setSelectionService(ESelectionService selectionService) {
		this.selectionService = selectionService;
	}

	/** Clean resource on dispose */
	private void onDispose() {
		removeDisposeListener(disposeListener);
		treeNodes.forEach(node -> node.traverseDown(TreeNode::delete));
		if (previewIsobathGroupNode != null) {
			previewIsobathGroupNode.traverseDown(TreeNode::delete);
			previewIsobathGroupNode = null;
		}
		if (dtmGroupNode != null) {
			dtmGroupNode.traverseDown(TreeNode::delete);
			dtmGroupNode = null;
		}
		if (processedIsobathGroupNode != null) {
			processedIsobathGroupNode.traverseDown(TreeNode::delete);
			processedIsobathGroupNode = null;
		}
	}

}
