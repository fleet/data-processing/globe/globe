package fr.ifremer.globe.editor.navshift.viewer2d.scene;

import fr.ifremer.globe.editor.navshift.model.IShiftVector;
import gov.nasa.worldwind.geom.Line;
import gov.nasa.worldwind.geom.Vec4;

/** Useful geometries computed from a vector */
public class VectorGeometry {

	private static final int DISTANCE_NEAR_IN_PIXEL = 10;

	/** Vector */
	protected IShiftVector shiftVector;

	/** Original coordinates of the vector */
	protected Vec4 startingPoint;

	/** Final coordinates of the vector */
	protected Vec4 finalPoint;

	/** Constructor */
	public VectorGeometry(IShiftVector shiftVector) {
		this.shiftVector = shiftVector;
		startingPoint = new Vec4(shiftVector.getOriginX(), shiftVector.getOriginY());
		computeFinalPoint();
	}

	/**
	 * @return Distance of the target from the vector. Double.POSITIVE_INFINITY when target is outside the segment
	 *         defined by the vector
	 */
	public double distanceTo(Vec4 vec4) {
		Vec4 nearestPoint = Line.nearestPointOnSegment(startingPoint, finalPoint, vec4);
		return nearestPoint != startingPoint && nearestPoint != finalPoint ? nearestPoint.distanceTo3(vec4)
				: Double.POSITIVE_INFINITY;
	}

	/** Compute the final point coordinates */
	public void computeFinalPoint() {
		finalPoint = new Vec4(startingPoint.x - shiftVector.getDriftX(), startingPoint.y - shiftVector.getDriftY());
	}

	/** Modify the final point coordinates of the vector */
	public void modifyFinalPoint(Vec4 vec) {
		shiftVector.setEndCoord(vec.x, vec.y);
	}

	/**
	 * @return true if point is near the vector
	 */
	public boolean isNearTo(Vec4 vec, double pixelSize) {
		return distanceTo(vec) <  DISTANCE_NEAR_IN_PIXEL * pixelSize;
	}

	/**
	 * @return true if line is near the vector
	 */
	public boolean isFinalPointNearTo(Vec4 vec, double pixelSize) {
		return finalPoint.distanceTo2(vec) < DISTANCE_NEAR_IN_PIXEL * pixelSize;
	}
	
	/** Getter of {@link #shiftVector} */
	public IShiftVector getShiftVector() {
		return shiftVector;
	}

	/** Getter of {@link #startingPoint} */
	public Vec4 getStartingPoint() {
		return startingPoint;
	}

	/** Getter of {@link #finalPoint} */
	public Vec4 getFinalPoint() {
		return finalPoint;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return shiftVector.hashCode();
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		return obj instanceof VectorGeometry && hashCode() == obj.hashCode();
	}

}
