package fr.ifremer.globe.editor.navshift.viewer2d.handlers;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;

public class SaveHandler {

	/** Event broker service */
	@Inject
	protected IEventBroker eventBroker;

	@Execute
	public void execute(EPartService partService) {
		eventBroker.send(NavShiftEventTopics.TOPIC_SAVE, new NullProgressMonitor());
	}
	
}
