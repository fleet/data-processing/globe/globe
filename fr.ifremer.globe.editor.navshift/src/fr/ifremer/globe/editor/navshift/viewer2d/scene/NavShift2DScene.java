package fr.ifremer.globe.editor.navshift.viewer2d.scene;

import java.awt.Canvas;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL2ES1;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jogamp.opengl.util.awt.TextRenderer;

import fr.ifremer.globe.ogl.GLComponent;
import fr.ifremer.globe.ogl.util.CoordinatesProjection;
import fr.ifremer.viewer3d.util.SSVCursor;

public class NavShift2DScene implements GLEventListener, MouseListener, MouseMotionListener, MouseWheelListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(NavShift2DScene.class);

	private static final double DEFAULT_ZOOM = 1;

	private double[] cursorSceneCoords = new double[4];
	private double[] clickedSceneCoords = new double[4];
	private double zoom = DEFAULT_ZOOM;
	private double xtranslation = 0.0;
	private double ytranslation = 0.0;
	private double savedxtranslation = 0.0;
	private double savedytranslation = 0.0;
	private int clickedX;
	private int clickedY;
	private int x;
	private int y;
	private boolean isDraggingScene;
	private boolean isDragEnabled = true;
	private double sceneWidth;
	private double scenecHeight;
	private double pixelSize;
	private double[] center = new double[2];

	private SceneBounds bounds;
	private GLCanvas canvas;
	private TextRenderer textRenderer;
	private List<GLComponent> components = new CopyOnWriteArrayList<>();
	private boolean allowNewPoint;

	/**
	 * Constructor
	 */
	public NavShift2DScene(GLCanvas canvas) {
		// setup canvas
		this.canvas = canvas;
		canvas.addGLEventListener(this);
		canvas.addMouseListener(this);
		canvas.addMouseMotionListener(this);
		canvas.addMouseWheelListener(this);

		// create controller
		new SceneController(this);
	}

	/**
	 * Adds a new openGl component to the scene.
	 */
	protected void addComponent(GLComponent component) {
		components.add(component);
	}

	/**
	 * Removes the component from the scene.
	 */
	protected void removeComponent(GLComponent component) {
		components.remove(component);
	}

	/**
	 * Enables or disables drag.
	 */
	protected void enableDragging(boolean b) {
		isDragEnabled = b;
	}

	public TextRenderer getTextRenderer() {
		if (textRenderer == null) {
			textRenderer = new TextRenderer(new Font("SansSerif", Font.BOLD, 12));
		}
		return textRenderer;
	}

	public void refresh() {
		SwingUtilities.invokeLater(() -> {
			try {
				canvas.repaint();
			} catch (RuntimeException t) {
				LOGGER.error("Error while refreshing WaterColumn 2D scene : " + t.getMessage(), t);
			}
		});
	}

	@Override
	public void dispose(GLAutoDrawable glad) {
		// dispose components
		components.forEach(c -> c.dispose(glad));
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		// not used
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		// Enable z- (depth) buffer for hidden surface removal.
		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glDepthFunc(GL.GL_LEQUAL);

		// Define "clear" color.
		gl.glClearColor(0f, 0f, 0f, 0f);

		// We want a nice perspective.
		gl.glHint(GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);
	}

	@Override
	public void display(GLAutoDrawable glad) {
		if (bounds == null) {
			return;
		}
		GL2 gl = glad.getGL().getGL2();

		int w = glad.getSurfaceWidth();
		int h = glad.getSurfaceHeight();
		gl.glViewport(0, 0, w, h);

		// clear the color and depth buffer
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		// reset projection matrix
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glLoadIdentity();

		// reset model view matrix
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glLoadIdentity();

		keepAspectRatio(glad);

		// Compute cursor scene coordinates
		CoordinatesProjection.fromScreenToScene(glad, x, y, cursorSceneCoords);

		handleSceneDrag(glad);

		// draw components
		components.forEach(c -> c.draw(glad));
	}

	/**
	 * Applies transformations to correctly display the scene (zoom, translation, clipping...)
	 */
	private void keepAspectRatio(GLAutoDrawable glad) {
		GL2 gl = glad.getGL().getGL2();
		double windowRatio = glad.getSurfaceWidth() / (double) glad.getSurfaceHeight();
		double waterColumnRatio = sceneWidth / scenecHeight;

		double computedSceneWidth;
		double computedSceneHeight;
		if (windowRatio < waterColumnRatio) {
			computedSceneWidth = sceneWidth * zoom;
			computedSceneHeight = computedSceneWidth / windowRatio;
		} else {
			computedSceneHeight = scenecHeight * zoom;
			computedSceneWidth = computedSceneHeight * windowRatio;
		}
		pixelSize = computedSceneWidth / glad.getSurfaceWidth();

		gl.glOrtho(-computedSceneWidth / 2, computedSceneWidth / 2, -computedSceneHeight / 2, computedSceneHeight / 2,
				-100, 100);
		gl.glTranslated(xtranslation - center[0], ytranslation - center[1], 0.0);
	}

	/**
	 * Displays 2D string with scene coordinate (with zoom and translation correction and screen coordinate offset
	 * (dx,dy)).
	 */
	public void draw2DStringOnScene(GLAutoDrawable glad, String text, double x, double y, int dx, int dy) {
		// define text coordinates
		double[] textScreenCoords = new double[3];
		CoordinatesProjection.fromSceneToScreen(glad, x, y, 0, textScreenCoords);
		textScreenCoords[0] += dx;
		// projection in text renderer coordinate system
		textScreenCoords[1] = glad.getSurfaceHeight() - textScreenCoords[1] + dy;

		getTextRenderer().beginRendering(glad.getSurfaceWidth(), glad.getSurfaceHeight());
		getTextRenderer().draw(text, (int) textScreenCoords[0], (int) textScreenCoords[1]);
		getTextRenderer().endRendering();
	}

	private void handleSceneDrag(GLAutoDrawable glad) {
		if (isDragEnabled && isDraggingScene) {
			CoordinatesProjection.fromScreenToScene(glad, clickedX, clickedY, clickedSceneCoords);
			xtranslation = savedxtranslation + cursorSceneCoords[0] - clickedSceneCoords[0];
			ytranslation = savedytranslation + cursorSceneCoords[1] - clickedSceneCoords[1];
		}
	}

	public void setAllowNewPoint(boolean b) {
		allowNewPoint = b;
		canvas.setCursor(b ? SSVCursor.getPredefinedCursor(SSVCursor.CURSOR_POINT) : null);
	}

	public void resetView() {
		zoom = DEFAULT_ZOOM;
		xtranslation = 0.0;
		ytranslation = 0.0;
		savedxtranslation = 0.0;
		savedytranslation = 0.0;
		refresh();
	}

	/**
	 * Sets water column bounds and recomputes scene center and size if necessary.
	 */
	public void setBounds(SceneBounds newBounds) {
		double topScene = center[1] + scenecHeight / 2d;
		double bottomScene = center[1] - scenecHeight / 2d;
		double leftScene = center[0] - sceneWidth / 2d;
		double rightScene = center[0] + sceneWidth / 2d;

		// recompute scene center and size if necessary
		if (bounds == null || newBounds.getTop() > topScene || newBounds.getBottom() < bottomScene
				|| newBounds.getLeft() < leftScene || newBounds.getRight() > rightScene) {
			sceneWidth = 2.2 * Math.max(newBounds.getLeft(), newBounds.getRight());
			scenecHeight = 1.1 * newBounds.getHeight();
			center = new double[] { 0, newBounds.getTop() - newBounds.getHeight() / 2d };
		}
		bounds = newBounds;
	}

	public double[] getCursorSceneCoordonates() {
		return cursorSceneCoords;
	}

	public double[] getCursorScreenCoordonates() {
		return new double[] { x, y };
	}

	/**
	 * @return the current pixel size (computed by sceneWidth/screenWidth)
	 */
	public double getPixelSize() {
		return pixelSize;
	}

	/**********************************************************************************************************
	 * MOUSE EVENTS MANAGEMENT
	 **********************************************************************************************************/

	@Override
	public void mouseEntered(MouseEvent evt) {
		// not used
	}

	@Override
	public void mouseExited(MouseEvent evt) {
		// not used
	}

	@Override
	public void mouseClicked(MouseEvent evt) {
		if (evt.getButton() == MouseEvent.BUTTON1 && allowNewPoint) {

		}
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		if (evt.getButton() == MouseEvent.BUTTON1 && isDragEnabled) {
			clickedX = evt.getX();
			clickedY = evt.getY();
			x = clickedX;
			y = clickedY;

			isDraggingScene = true;
			refresh();
		}
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		if (evt.getButton() == MouseEvent.BUTTON1) {
			if (isDraggingScene) {
				savedxtranslation = xtranslation;
				savedytranslation = ytranslation;
				isDraggingScene = false;
			}
			refresh();
		}
	}

	@Override
	public void mouseDragged(MouseEvent evt) {
		if (SwingUtilities.isLeftMouseButton(evt)) {
			x = evt.getX();
			y = evt.getY();
			refresh();
		}
	}

	@Override
	public void mouseMoved(MouseEvent evt) {
		x = evt.getX();
		y = evt.getY();
		refresh();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		zoom *= e.getWheelRotation() / 10.0 + 1;
		zoom = Math.max(zoom, 0.00001);
		zoom = Math.min(zoom, 10000.0);
		refresh();
	}

	public Canvas getCanvas() {
		return canvas;

	}

}
