/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.viewer2d.toolbox.components.explorer;

import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.viewer3d.layers.lineisobaths.IsobathLayer;
import fr.ifremer.viewer3d.layers.lineisobaths.parameters.IsobathParameters;
import fr.ifremer.viewer3d.layers.lineisobaths.parametresexplorer.IsobathParametersComposite;

/**
 * Group TreeNode which can contains {@link IsobathNode}.
 */
public class IsobathGroupNode extends GroupNode {

	/** Isobath parameter */
	private final IsobathParameters isobathParameters;

	/** Isobath parameter composite **/
	private IsobathParametersComposite isobathParametersComposite;

	private IChangeListener changeListener = this::onParametersChanged;

	/**
	 * Constructor.
	 */
	public IsobathGroupNode(String title, IsobathParameters isobathParameters) {
		super(title, Icons.ISOBATHS.toImage());
		this.isobathParameters = isobathParameters;

		Display.getDefault().syncExec(() -> {
			this.isobathParameters.getStep().addChangeListener(changeListener);
			this.isobathParameters.getMainLine().addChangeListener(changeListener);
			this.isobathParameters.getSampling().addChangeListener(changeListener);

			this.isobathParameters.getLineWidth().addChangeListener(changeListener);
			this.isobathParameters.getMainLineColor().addChangeListener(changeListener);
			this.isobathParameters.getMainLineVisible().addChangeListener(changeListener);

			this.isobathParameters.getOtherLineVisible().addChangeListener(changeListener);
			this.isobathParameters.getIsOtherLineMultiColor().addChangeListener(changeListener);
			this.isobathParameters.getOtherLineColors().addChangeListener(changeListener);
			this.isobathParameters.getOtherLineUnicolor().addChangeListener(changeListener);
		});
	}

	@Override
	public void delete() {
		super.delete();
		Display.getDefault().syncExec(() -> {
			isobathParameters.getStep().removeChangeListener(changeListener);
			isobathParameters.getMainLine().removeChangeListener(changeListener);
			isobathParameters.getSampling().removeChangeListener(changeListener);

			isobathParameters.getLineWidth().removeChangeListener(changeListener);
			isobathParameters.getMainLineColor().removeChangeListener(changeListener);
			isobathParameters.getMainLineVisible().removeChangeListener(changeListener);

			isobathParameters.getOtherLineVisible().removeChangeListener(changeListener);
			isobathParameters.getIsOtherLineMultiColor().removeChangeListener(changeListener);
			isobathParameters.getOtherLineColors().removeChangeListener(changeListener);
			isobathParameters.getOtherLineUnicolor().removeChangeListener(changeListener);
		});
	}

	/**
	 * Updates child nodes when parameter changed.
	 */
	protected void onParametersChanged(ChangeEvent e) {
		for (TreeNode treeNode : getChildren()) {
			IsobathParameters currentIsobathParameters = null;
			if (treeNode instanceof LayerNode layerNode && layerNode.getLayer().isPresent()) {
				if (layerNode.getLayer().get() instanceof IsobathLayer isobathLayer) {
					currentIsobathParameters = isobathLayer.getIsobathParameters();
				}
			} else if (treeNode instanceof IsobathGroupNode) {
				currentIsobathParameters = ((IsobathGroupNode) treeNode).getIsobathParameters();
			}

			if (currentIsobathParameters == null) {
				continue;
			}

			// step
			if (e.getSource() == isobathParameters.getStep()) {
				currentIsobathParameters.getStep().setValue(isobathParameters.getStep().get());
			}
			// main line occurrence
			else if (e.getSource() == isobathParameters.getMainLine()) {
				currentIsobathParameters.getMainLine().setValue(isobathParameters.getMainLine().get());
			}
			// sampling
			else if (e.getSource() == isobathParameters.getSampling()) {
				currentIsobathParameters.getSampling().setValue(isobathParameters.getSampling().get());
			}
			// main line visible
			else if (e.getSource() == isobathParameters.getMainLineVisible()) {
				currentIsobathParameters.getMainLineVisible().setValue(isobathParameters.getMainLineVisible().get());
			}
			// main line color
			else if (e.getSource() == isobathParameters.getMainLineColor()) {
				currentIsobathParameters.getMainLineColor().setValue(isobathParameters.getMainLineColor().get());
			}
			// other line visible
			else if (e.getSource() == isobathParameters.getOtherLineVisible()) {
				currentIsobathParameters.getOtherLineVisible().setValue(isobathParameters.getOtherLineVisible().get());
			}
			// other IsOtherLineMultiColor
			else if (e.getSource() == isobathParameters.getIsOtherLineMultiColor()) {
				currentIsobathParameters.getIsOtherLineMultiColor()
						.setValue(isobathParameters.getIsOtherLineMultiColor().get());
			}
			// other line unicolor
			else if (e.getSource() == isobathParameters.getOtherLineUnicolor()) {
				currentIsobathParameters.getOtherLineUnicolor()
						.setValue(isobathParameters.getOtherLineUnicolor().get());
			}
			// other line multi color list
			else if (e.getSource() == isobathParameters.getOtherLineColors()) {
				for (int i = 0; i < isobathParameters.getOtherLineColors().size(); i++) {
					currentIsobathParameters.getOtherLineColors().set(i, isobathParameters.getOtherLineColors().get(i));
				}
			}
			// line width
			else if (e.getSource() == isobathParameters.getLineWidth()) {
				currentIsobathParameters.getLineWidth().setValue(isobathParameters.getLineWidth().get());
			}
		}
	}

	/**
	 * Method called after click on "compute" button.
	 */
	protected void onIsobathsRecomputeRequested() {
		for (TreeNode treeNode : getChildren()) {
			if (treeNode instanceof LayerNode layerNode && layerNode.getLayer().isPresent()
					&& layerNode.getLayer().get() instanceof IsobathLayer isobathLayer) {
				var currentIsobathParameters = isobathLayer.getIsobathParameters();
				currentIsobathParameters.getStep().setValue(isobathParameters.getStep().get());
				currentIsobathParameters.getMainLine().setValue(isobathParameters.getMainLine().get());
				currentIsobathParameters.getSampling().setValue(isobathParameters.getSampling().get());
				isobathLayer.compute();
			} else if (treeNode instanceof IsobathGroupNode isobathGroupNode) {
				var currentIsobathParameters = isobathGroupNode.getIsobathParameters();
				currentIsobathParameters.getStep().setValue(isobathParameters.getStep().get());
				currentIsobathParameters.getMainLine().setValue(isobathParameters.getMainLine().get());
				currentIsobathParameters.getSampling().setValue(isobathParameters.getSampling().get());
				isobathGroupNode.onIsobathsRecomputeRequested();
			}
		}
	}

	/**
	 * {@link IsobathParameters} getter
	 */
	public IsobathParameters getIsobathParameters() {
		return isobathParameters;
	}

	/**
	 * @return {@link IsobathParametersComposite}
	 */
	public IsobathParametersComposite getParametersComposite(Composite parent) {
		if (isobathParametersComposite == null || isobathParametersComposite.isDisposed()) {
			isobathParametersComposite = new IsobathParametersComposite(parent, isobathParameters,
					this::onIsobathsRecomputeRequested);
		}
		return isobathParametersComposite;
	}

}
