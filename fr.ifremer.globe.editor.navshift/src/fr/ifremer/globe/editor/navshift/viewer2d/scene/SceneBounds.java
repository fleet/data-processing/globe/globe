package fr.ifremer.globe.editor.navshift.viewer2d.scene;

import fr.ifremer.globe.core.model.projection.Projection;

/**
 * Class use to defines scene bounds.
 */
public class SceneBounds {

	private final double bottom;
	private final double top;
	private final double left;
	private final double right;

	/**
	 * Constructor
	 */
	public SceneBounds(double top, double bottom, double left, double right) {
		this.top = top;
		this.bottom = bottom;
		this.left = left;
		this.right = right;
	}

	/**
	 * Constructor from a {@link Projection}.
	 */
	public SceneBounds(Projection projection) {
		this.top = projection.getMaxProjectedY();
		this.bottom = projection.getMinProjectedY();
		this.left = projection.getMinProjectedX();
		this.right = projection.getMaxProjectedX();
	}

	/**
	 * @return true if the point coordinates are inside bounds
	 */
	public boolean contains(double[] coords) {
		return contains(coords[0], coords[1]);
	}

	/**
	 * @return true if the coordinates are inside bounds
	 */
	public boolean contains(double x, double y) {
		return getLeft() < x && x < getRight() && getBottom() < y && y < getTop();
	}

	// GETTERS

	public double getTop() {
		return top;
	}

	public double getBottom() {
		return bottom;
	}

	public double getLeft() {
		return left;
	}

	public double getRight() {
		return right;
	}

	public double getWidth() {
		return right - left;
	}

	public double getHeight() {
		return top - bottom;
	}

}
