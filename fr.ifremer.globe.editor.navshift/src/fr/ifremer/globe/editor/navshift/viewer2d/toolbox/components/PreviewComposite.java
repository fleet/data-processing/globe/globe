/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.viewer2d.toolbox.components;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.CommandManager;
import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.application.handlers.CommandExecutor;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.process.Previewer;
import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Composite to configure preview isobaths.
 */
public class PreviewComposite extends Composite {

	/** Event broker service */
	@Inject
	private IEventBroker eventBroker;
	@Inject
	private CommandManager commandManager;
	@Inject
	private EHandlerService handlerService;
	/** Current session */
	@Inject
	@Named(ContextNames.SESSION_CONTEXT)
	@org.eclipse.e4.core.di.annotations.Optional
	private ISessionContext sessionContext;

	private Group grpPreview;
	private Button btnApplyToInput;
	private Button btnExportNvi;
	private Button btnNviType;
	private Button btnClose;

	/** Selected nvi type to export to */
	private ExportType exportType = ExportType.NVI_V2;

	/**
	 * Constructor.
	 */
	public PreviewComposite(Composite parent) {
		super(parent, SWT.NONE);
		createsUI();
		setEnabled(false);
		// Injection
		if (ContextInitializer.getEclipseContext() != null) {
			ContextInjectionFactory.inject(this, ContextInitializer.getEclipseContext());
		}
	}

	/**
	 * Creates the UI.
	 */
	public void createsUI() {
		FillLayout fillLayout = new FillLayout(SWT.HORIZONTAL);
		fillLayout.marginHeight = 3;
		fillLayout.marginWidth = 3;
		setLayout(fillLayout);

		grpPreview = new Group(this, SWT.NONE);
		grpPreview.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpPreview.setText("Step 3 - Preview");
		grpPreview.setLayout(new GridLayout(3, false));

		btnApplyToInput = new Button(grpPreview, SWT.NONE);
		btnApplyToInput.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnApplyToInput.setText("Apply to input files");
		btnApplyToInput.addSelectionListener(SelectionListener.widgetSelectedAdapter(
				event -> CommandExecutor.executeCommand(CommandExecutor.APPLY_TO_SOUNDER_COMMAND)));

		Composite cmpExport = new Composite(grpPreview, SWT.NONE);
		cmpExport.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		GridLayout glCmpExport = new GridLayout(2, false);
		glCmpExport.marginWidth = 0;
		glCmpExport.horizontalSpacing = 0;
		glCmpExport.marginHeight = 0;
		glCmpExport.verticalSpacing = 0;
		cmpExport.setLayout(glCmpExport);

		btnExportNvi = new Button(cmpExport, SWT.NONE);
		btnExportNvi.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnExportNvi.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.editor.navshift", "icons/vector.png"));
		btnExportNvi.setText(exportType.label);
		btnExportNvi.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> doExportToNvi()));

		btnNviType = new Button(cmpExport, SWT.NONE);
		btnNviType.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnNviType.setImage(Icons.VIEW_MENU.toImage());

		Menu exportTypeMenu = new Menu(btnNviType);
		btnNviType.setMenu(exportTypeMenu);
		btnNviType.addListener(SWT.Selection, e -> exportTypeMenu.setVisible(true));

		MenuItem miExportNviV2 = new MenuItem(exportTypeMenu, SWT.NONE);
		miExportNviV2.setText(ExportType.NVI_V2.label);
		miExportNviV2.setImage(Icons.PYTHON.toImage());
		miExportNviV2.addListener(SWT.Selection, e -> {
			exportType = ExportType.NVI_V2;
			btnExportNvi.setText(exportType.label);
			doExportToNvi();
		});

		MenuItem miExportNviLegacy = new MenuItem(exportTypeMenu, SWT.NONE);
		miExportNviLegacy.setText(ExportType.NVI_LEGACY.label);
		miExportNviLegacy.addListener(SWT.Selection, e -> {
			exportType = ExportType.NVI_LEGACY;
			btnExportNvi.setText(exportType.label);
			doExportToNvi();
		});

		btnClose = new Button(grpPreview, SWT.NONE);
		btnClose.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnClose.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.editor.navshift", "icons/close.png"));
		btnClose.setText("Close preview");
		btnClose.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> closePreview()));
	}

	/** Launche the export command with the nvi parameter set to th expected type */
	private void doExportToNvi() {
		Command command = commandManager.getCommand(CommandExecutor.EXPORT_COMMAND);
		ParameterizedCommand parameterizedCommand = ParameterizedCommand.generateCommand(command,
				Map.of(CommandExecutor.EXPORT_PARAM_NVI_TYPE, exportType.nviType.name()));
		handlerService.executeHandler(parameterizedCommand);
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		grpPreview.setFont(SWTResourceManager.getFont("Segoe UI", 9, enabled ? SWT.BOLD : SWT.NORMAL));
		btnApplyToInput.setEnabled(enabled);
		btnExportNvi.setEnabled(enabled);
		btnNviType.setEnabled(enabled);
		btnClose.setEnabled(enabled);
		if (enabled) {
			setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		} else {
			setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		}
	}

	/**
	 * Closes the preview mode.
	 */
	private void closePreview() {
		BusyIndicator.showWhile(getDisplay(), () -> eventBroker.send(NavShiftEventTopics.PREVIEW, Optional.empty()));
	}

	/**
	 * Event handler for preview mode
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onPreview(@UIEventTopic(NavShiftEventTopics.PREVIEW) Optional<Previewer> option) {
		if (isDisposed()) {
			return;
		}
		setEnabled(option.isPresent());
	}

	/**
	 * Event handler for TOPIC_FILES_SHIFTED<br>
	 * Some files have been shifted. Close preview if necessary
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onFilesShifted(@UIEventTopic(NavShiftEventTopics.TOPIC_FILES_SHIFTED) List<File> files) {
		// Search if a profile has been modified
		boolean oneProfileModified = sessionContext.getProfiles().stream()//
				.map(IProfile::getSounderNcInfo)//
				.map(ISounderNcInfo::toFile)//
				.anyMatch(files::contains);
		if (oneProfileModified) {
			// Differate the closing of the preview
			eventBroker.post(NavShiftEventTopics.PREVIEW, Optional.empty());
		}
	}

	/** Type of NVI to export */
	private enum ExportType {
		NVI_V2("Export as .nvi.nc", ContentType.NVI_V2_NETCDF_4), //
		NVI_LEGACY("Export as .nvi", ContentType.NVI_NETCDF_4);

		private final String label;
		private final ContentType nviType;

		ExportType(String label, ContentType nviType) {
			this.label = label;
			this.nviType = nviType;
		}
	}

}
