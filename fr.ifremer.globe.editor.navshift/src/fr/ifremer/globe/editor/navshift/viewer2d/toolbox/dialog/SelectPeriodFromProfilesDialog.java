package fr.ifremer.globe.editor.navshift.viewer2d.toolbox.dialog;

import java.io.File;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.controller.ShiftPeriodController;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.ui.providers.LayerCheckProvider;

/**
 * Dialog to define a new shift period from profiles.
 */
public class SelectPeriodFromProfilesDialog extends Dialog {

	private CheckboxTableViewer tableViewer;

	/** currently used sessionContext */
	@Inject
	@Named(ContextNames.SESSION_CONTEXT)
	protected ISessionContext sessionContext;

	@Inject
	@Named(ContextNames.SHIFTPERIOD_CONTROLLER)
	protected ShiftPeriodController shiftPeriodController;

	/**
	 * Constructor
	 */
	public SelectPeriodFromProfilesDialog(Shell parentShell) {
		super(parentShell);
		ContextInitializer.inject(this);
	}

	/**
	 * Sets the dialog title
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Define the shift period from input profiles...");
	}

	/**
	 * Creates UI
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		GridLayout parentLayout = new GridLayout(1, false);
		parent.setLayout(parentLayout);

		Composite mainComposite = new Composite(parent, SWT.NONE);
		mainComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		mainComposite.setLayout(new GridLayout(1, false));

		Label lblNewLabel = new Label(mainComposite, SWT.NONE);
		lblNewLabel.setText("Shift period will be defined by the first and the last dates of selected profiles.");

		tableViewer = CheckboxTableViewer.newCheckList(mainComposite, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
		tableViewer.setCheckStateProvider(new LayerCheckProvider());
		Table table = tableViewer.getTable();
		GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_table.widthHint = 350;
		table.setLayoutData(gd_table);

		TableViewerColumn profileTableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		profileTableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return new File(((IProfile) element).getSounderNcInfo().getPath()).getName();
			}
		});
		TableColumn profileTableColumn = profileTableViewerColumn.getColumn();
		profileTableColumn.setWidth(350);
		profileTableColumn.setText("Profile");

		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setInput(sessionContext.getProfiles());

		return parent;
	}

	/**
	 * Called when "OK" button is pressed : defines a new shift period.
	 */
	@Override
	protected void okPressed() {
		int minIndex = -1;
		int maxIndex = -1;

		for (Object selection : tableViewer.getCheckedElements()) {
			IProfile selectedProfile = (IProfile) selection;
			minIndex = minIndex == -1 ? selectedProfile.getFirstIndex()
					: Math.min(minIndex, selectedProfile.getFirstIndex());
			maxIndex = maxIndex == -1 ? selectedProfile.getLastIndex()
					: Math.max(maxIndex, selectedProfile.getLastIndex());

		}

		if (minIndex != -1) {
			IShiftPeriod shiftPeriod = shiftPeriodController.createShiftPeriod();
			shiftPeriod.setStartIndex(minIndex);
			shiftPeriod.setEndIndex(maxIndex);
			sessionContext.setShiftPeriod(shiftPeriod);
		}

		super.okPressed();
	}

}
