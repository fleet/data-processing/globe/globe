/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.viewer2d.parametersexplorer.service;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.controller.Viewer2DLayerController;
import fr.ifremer.globe.editor.navshift.viewer2d.parametersexplorer.composite.ShiftPeriodComposite;
import fr.ifremer.globe.ui.service.parametersview.IParametersViewCompositeFactory;

/**
 * Make a Composite to edit the DtmLayer parameters
 */
@Component(name = "globe_navshift_shift_period_composite_factory", service = IParametersViewCompositeFactory.class)
public class ShiftPeriodCompositeFactory implements IParametersViewCompositeFactory {

	@Override
	public Optional<Composite> getComposite(Object selection, Composite parent) {
		Viewer2DLayerController layerController = ContextInitializer
				.getInContext(ContextNames.VIEWER_2D_LAYER_CONTROLLER);

		return layerController != null && selection == layerController.getShiftLayerComposite()
				? Optional.of(new ShiftPeriodComposite(parent))
				: Optional.empty();
	}
}
