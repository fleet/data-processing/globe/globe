/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.viewer2d.scene;

import java.awt.Canvas;
import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.lang.math.IntRange;
import org.eclipse.core.runtime.Assert;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.HighlightPointInfos;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.application.event.VectorInfos;
import fr.ifremer.globe.editor.navshift.commons.NavShiftPreference;
import fr.ifremer.globe.editor.navshift.controller.AbstractController;
import fr.ifremer.globe.editor.navshift.controller.ShiftPeriodController;
import fr.ifremer.globe.editor.navshift.controller.Viewer2DLayerController;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.IShiftVector;
import fr.ifremer.globe.editor.navshift.process.Previewer;
import fr.ifremer.viewer3d.layers.lineisobaths.IsobathLayer;
import fr.ifremer.viewer3d.layers.lineisobaths.parameters.IsobathParameters;
import fr.ifremer.viewer3d.layers.lineisobaths.tools.IsobathTopics;
import gov.nasa.worldwind.geom.Vec4;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

/**
 * Manager of interactions with the Viewer3D
 */
public class SceneController extends AbstractController implements MouseMotionListener, MouseListener, KeyListener {

	/** Listening behavior */
	enum Behavior {
		NONE, SELECT_SHIFTPERIOD_1, SELECT_SHIFTPERIOD_2, SELECT_VECTOR_1, SELECT_VECTOR_2
	}

	/** Expected mouse behavior */
	protected Behavior behavior = Behavior.NONE;

	/** Controller of the shiftPeriod */
	@Inject
	@Named(ContextNames.SHIFTPERIOD_CONTROLLER)
	protected ShiftPeriodController shiftPeriodController;

	/** Mercator projection. */
	@Inject
	@Named(ContextNames.MERCATOR_PROJECTION)
	protected Projection projection;

	/** Saved preferences */
	@Inject
	@Named(ContextNames.NAVSHIFT_PREFERENCE)
	protected NavShiftPreference preferences;

	/** Controller of all layers displayed in Viewer3d. */
	@Inject
	@Named(ContextNames.VIEWER_2D_LAYER_CONTROLLER)
	protected Viewer2DLayerController layerController;

	/** A Subject that emits MouseEvent to currently subscribed observers (mouseEventSubscriber) */
	protected PublishSubject<MouseEvent> mouseEventPublisher = PublishSubject.create();

	/** Observer subscribed to mouseEventPublisher */
	protected Disposable mouseEventSubscriber;

	/** Clicked point on the navigation */
	protected int selectedNavPointIndex = -1;
	/** Vector informations of the current edited one */
	protected VectorInfos vectorInfos;

	/** Usefull geometries computed from vectors */
	protected List<VectorGeometry> vectorGeometries = new ArrayList<>();
	/** Vector currently selected */
	protected List<VectorGeometry> selectedVectors = new ArrayList<>();
	/** Vector currently edited */
	protected VectorGeometry editedVector = null;

	private final NavShift2DScene scene;

	private List<IsobathLayer> previewIsobathLayer = new ArrayList<>();

	/**
	 * Constructor
	 */
	public SceneController(NavShift2DScene scene) {
		super();
		this.scene = scene;
		// Injection
		ContextInjectionFactory.inject(this, ContextInitializer.getEclipseContext());
	}

	/** After injection... */
	@PostConstruct
	public void postConstruct() {
		vectorInfos = new VectorInfos(preferences.getHighlightVectorColor(), preferences.getVectorPointSize());

		scene.setBounds(new SceneBounds(projection));

		layerController.getReferenceDtmIsobathLayer().forEach(scene::addComponent);
		layerController.getProfilesIsobathLayer().forEach(scene::addComponent);
		layerController.getProfilesIsobathLayer()
				.forEach(layer -> layer.addPropertyChangeListener(e -> scene.refresh()));
		scene.addComponent(layerController.getNavigationLayer());
		scene.addComponent(layerController.getVectorsLayer());
	}

	/**
	 * @see fr.ifremer.globe.editor.navshift.controller.AbstractController#onContextReset()
	 */
	@Override
	protected void onContextReset() {
		desactivate();
	}

	/** Session has been activated successfully */
	@Inject
	@Optional
	protected void onContextActivated(
			@UIEventTopic(NavShiftEventTopics.SESSION_ACTIVATED) ISessionContext sessionContext) {
		computeVectorGeometries();
	}

	/** Handle the SHIFTPERIOD_SELECTION_START event */
	@Inject
	@Optional
	protected void startSelectingShiftPeriod(
			@UIEventTopic(NavShiftEventTopics.TOPIC_SHIFTPERIOD_SELECTION_START) Object poster) {
		activate(Behavior.SELECT_SHIFTPERIOD_1);
	}

	/** Handle the SHIFTPERIOD_SELECTION_STOP event */
	@Inject
	@Optional
	protected void stopSelectingShiftPeriod(
			@UIEventTopic(NavShiftEventTopics.TOPIC_SHIFTPERIOD_SELECTION_STOP) Object poster) {
		desactivate();
	}

	/** Handle the VECTOR_CREATION_START event */
	@Inject
	@Optional
	protected void startCreatingVectors(@UIEventTopic(NavShiftEventTopics.TOPIC_VECTOR_CREATION_START) Object poster) {
		activate(Behavior.SELECT_VECTOR_1);
	}

	/** Handle the VECTOR_CREATION_STOP event */
	@Inject
	@Optional
	protected void stopCreatingVectors(@UIEventTopic(NavShiftEventTopics.TOPIC_VECTOR_CREATION_STOP) Object poster) {
		desactivate();
	}

	/** Handles the TOPIC_ISOBATH_UPDATED event : refresh the scene. */
	@Inject
	@Optional
	protected void onIsobathsRecomputeRequested(
			@UIEventTopic(IsobathTopics.TOPIC_ISOBATH_UPDATED) IsobathParameters ip) {
		scene.refresh();
	}

	/** Handles the TOPIC_VECTOR_EDITED event : refresh the scene. */
	@Inject
	@Optional
	protected void onVectorEdited(@UIEventTopic(NavShiftEventTopics.TOPIC_VECTOR_EDITED) IShiftVector v) {
		scene.refresh();
	}

	/** Hook all listeners on WorldWindow */
	protected void activate(Behavior behavior) {
		Assert.isTrue(this.behavior == Behavior.NONE, "Invalid state, activated() called twice");
		this.behavior = behavior;
		hookListeners();
		computeVectorGeometries();
	}

	/** Remove all listeners on WorldWindow */
	protected void desactivate() {
		if (behavior != Behavior.NONE) {
			abortCurrentSelection();
			behavior = Behavior.NONE;
			selectedNavPointIndex = -1;

			unhookListeners();

			vectorGeometries = new ArrayList<>();
			selectedVectors = List.of();
		}
	}

	protected void hookListeners() {
		if (scene != null) {
			Canvas canvas = scene.getCanvas();
			canvas.addMouseListener(this);
			canvas.addMouseMotionListener(this);
			canvas.addKeyListener(this);
		}

		// Only keep the last mouse position
		mouseEventSubscriber = mouseEventPublisher.toFlowable(BackpressureStrategy.LATEST)
				.subscribe(this::onMouseMoved);
	}

	protected void unhookListeners() {
		if (scene != null) {
			Canvas canvas = scene.getCanvas();
			canvas.removeMouseListener(this);
			canvas.removeMouseMotionListener(this);
			canvas.removeKeyListener(this);
		}

		if (mouseEventSubscriber != null) {
			mouseEventSubscriber.dispose();
			mouseEventSubscriber = null;
		}
	}

	protected void abortCurrentSelection() {
		selectedNavPointIndex = -1;

		switch (behavior) {
		case SELECT_SHIFTPERIOD_1:
		case SELECT_VECTOR_1:
			eventBroker.send(NavShiftEventTopics.TOPIC_UNHIGHLIGHT_POINT, this);
			break;
		case SELECT_SHIFTPERIOD_2:
			eventBroker.send(NavShiftEventTopics.TOPIC_UNHIGHLIGHT_POINTS, this);
			eventBroker.send(NavShiftEventTopics.TOPIC_UNHIGHLIGHT_POINT, this);
			break;
		case SELECT_VECTOR_2:
			eventBroker.send(NavShiftEventTopics.TOPIC_UNHIGHLIGHT_POINT, this);
			notifyVectorStoppedChanging();
			break;

		default:
			// Ignored
			break;
		}

		behavior = Behavior.NONE;
	}

	/**
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3 && e.getClickCount() == 1 && behavior == Behavior.SELECT_VECTOR_2) {
			cancelSelectVector();
		} else if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 1) {
			onMouseClicked(e);
		}
	}

	/** Perform the creation of the shiftPeriod */
	protected boolean createShiftPeriod(MouseEvent e) {
		boolean result = false;
		int secondShiftPeriodPointIndex = getNearestPointIndex();
		if (secondShiftPeriodPointIndex >= 0) {
			IShiftPeriod shiftPeriod = shiftPeriodController.createShiftPeriod();
			IntRange indexRange = new IntRange(selectedNavPointIndex, secondShiftPeriodPointIndex);
			shiftPeriod.setStartIndex(indexRange.getMinimumInteger());
			shiftPeriod.setEndIndex(indexRange.getMaximumInteger());
			getSessionContext().setShiftPeriod(shiftPeriod);
			result = true;
		}
		return result;
	}

	/** Perform the creation of the shiftVector */
	protected boolean createShiftVector(MouseEvent e) {
		boolean result = false;
		IShiftPeriod shiftPeriod = sessionContext.getShiftPeriod();
		if (shiftPeriod != null && vectorInfos.getStopPoint() != null) {
			IShiftVector newVector = shiftPeriod.createAndAddShiftVector(vectorInfos.getStartPositionIndex(),
					vectorInfos.getStopPoint().x, vectorInfos.getStopPoint().y);
			vectorGeometries.add(new VectorGeometry(newVector));
			eventBroker.send(NavShiftEventTopics.TOPIC_VECTORS_SELECTED, List.of(newVector));
			highlightVector();
			notifyVectorStoppedChanging();
			result = true;
		}
		return result;
	}

	protected void notifyVectorStoppedChanging() {
		eventBroker.send(NavShiftEventTopics.TOPIC_DRAW_VECTOR, null);
	}

	/** Prepare a new period creation */
	protected boolean preparePeriodCreation() {
		boolean result = false;
		if (selectedNavPointIndex >= 0 && getSessionContext().getShiftPeriod() == null) {
			highlightPoint(selectedNavPointIndex, preferences.getHighlightPeriodColor(),
					preferences.getHighlightPeriodPointSize());
			getSessionContext().setShiftPeriod(null);
			result = true;
		}
		return result;
	}

	/** Prepare a new vector creation */
	protected boolean prepareVectorCreation() {
		// Unselect the current vector before creating a new one
		if (!selectedVectors.isEmpty()) {
			eventBroker.send(NavShiftEventTopics.TOPIC_VECTORS_SELECTED, List.of());
		}

		int index = getNearestPointIndexInShiftPeriod();
		// No vector at this point ?
		boolean result = vectorGeometries.stream().noneMatch(vg -> vg.getShiftVector().getIndexOrigin() == index);
		if (result) {
			selectedNavPointIndex = index;
			vectorInfos.setStartPositionIndex(selectedNavPointIndex);
			vectorInfos.setStopPoint(getCursorPosition());
			eventBroker.send(NavShiftEventTopics.TOPIC_DRAW_VECTOR, vectorInfos);
		}
		return result;
	}

	/** @return true when a vector could be selected */
	protected boolean trySelectingVector() {
		boolean result = false;
		VectorGeometry vectorGeometry = getVectorUnderTheMouse();
		if (vectorGeometry != null && !selectedVectors.contains(vectorGeometry)) {
			eventBroker.send(NavShiftEventTopics.TOPIC_VECTORS_SELECTED, List.of(vectorGeometry.getShiftVector()));
			scene.getCanvas().setCursor(Cursor.getDefaultCursor());
			result = true;
		}
		return result;
	}

	/** @return the VectorGeometry under the mouse */
	protected VectorGeometry getVectorUnderTheMouse() {
		return vectorGeometries.stream()
				.filter(vectorGeometry -> vectorGeometry.isNearTo(getCursorPosition(), scene.getPixelSize()))
				.findFirst().orElse(null);
	}

	/**
	 * @see java.awt.event.MouseMotionListener#mouseMoved(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		mouseEventPublisher.onNext(e);
	}

	/**
	 * Invoked when the mouse cursor has been moved
	 */
	protected void onMouseMoved(MouseEvent e) {
		switch (behavior) {
		case SELECT_SHIFTPERIOD_1:
			int index = getNearestPointIndex();
			highlightPoint(index, preferences.getHighlightPeriodColor(), preferences.getHighlightPeriodPointSize());
			break;
		case SELECT_SHIFTPERIOD_2:
			index = getNearestPointIndex();
			highlightPoints(index);
			break;
		case SELECT_VECTOR_1:
			// Unhighlight current vector
			eventBroker.send(NavShiftEventTopics.TOPIC_HIGHLIGHT_FOR_SELECTING_VECTOR, null);
			scene.getCanvas().setCursor(Cursor.getDefaultCursor());

			highlightVector();

			// Highlight navigation point
			index = getNearestPointIndexInShiftPeriod();
			highlightPoint(index, preferences.getHighlightVectorColor(), preferences.getHighlightVectorPointSize());

			break;
		case SELECT_VECTOR_2:
			vectorInfos.setStopPoint(getCursorPosition());
			eventBroker.send(NavShiftEventTopics.TOPIC_DRAW_VECTOR, vectorInfos);
			break;

		default:
			// Ignored
			break;
		}
	}

	/** Highlight vector under the mouse */
	protected void highlightVector() {
		VectorGeometry vectorGeometry = getVectorUnderTheMouse();
		if (vectorGeometry != null) {
			if (selectedVectors.contains(vectorGeometry)) {
				// Vector under the mouse is the selected one
				if (vectorGeometry.isFinalPointNearTo(getCursorPosition(), scene.getPixelSize())) {
					// Mouse around the final point ?
					eventBroker.send(NavShiftEventTopics.TOPIC_HIGHLIGHT_FOR_MODIFYING_VECTOR,
							vectorGeometry.getShiftVector());
					scene.getCanvas().setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
				}
			} else {
				eventBroker.send(NavShiftEventTopics.TOPIC_HIGHLIGHT_FOR_SELECTING_VECTOR,
						vectorGeometry.getShiftVector());
				scene.getCanvas().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
		}

	}

	/** Manage mouse click */
	protected void onMouseClicked(MouseEvent e) {
		switch (behavior) {
		case SELECT_SHIFTPERIOD_1:
			selectedNavPointIndex = getNearestPointIndex();
			if (preparePeriodCreation()) {
				behavior = Behavior.SELECT_SHIFTPERIOD_2;
			}
			break;
		case SELECT_SHIFTPERIOD_2:
			if (createShiftPeriod(e)) {
				behavior = Behavior.SELECT_VECTOR_1;
			}
			break;
		case SELECT_VECTOR_1:
			// Vector under the mouse, select it
			if (!trySelectingVector() && prepareVectorCreation()) {
				behavior = Behavior.SELECT_VECTOR_2;
			}
			break;
		case SELECT_VECTOR_2:
			if (createShiftVector(e)) {
				behavior = Behavior.SELECT_VECTOR_1;
			}
			break;

		default:
			// Ignored
			break;
		}
	}

	protected IShiftVector hasVectorAtIndex(int index) {
		IShiftVector result = null;
		IShiftPeriod shiftPeriod = sessionContext.getShiftPeriod();
		if (shiftPeriod.getStartIndex() <= index && shiftPeriod.getEndIndex() >= index) {
			result = shiftPeriod.getShiftVectors().stream().filter(shiftVector -> shiftVector.getIndexOrigin() == index)
					.findFirst().orElse(null);
		}
		return result;
	}

	/** HighlightPoint a point on the navgation. The point is the closest one from the mouse position */
	protected int highlightPoint(int index, GColor color, float pointSize) {
		if (index >= 0) {
			eventBroker.send(NavShiftEventTopics.TOPIC_HIGHLIGHT_POINT,
					new HighlightPointInfos(index, color, pointSize));
		}
		return index;
	}

	/**
	 * @return the closest point of the navigation and within the period from the mouse position
	 */
	protected int getNearestPointIndexInShiftPeriod() {
		int index = getNearestPointIndex();
		if (index == -1) {
			return index;
		}
		IShiftPeriod period = sessionContext.getShiftPeriod();
		if (period == null) {
			return -1;
		}
		if (index < period.getStartIndex()) {
			index = period.getStartIndex();
		}
		if (index > period.getEndIndex()) {
			index = period.getEndIndex();
		}
		return index;
	}

	/**
	 * @return the closest point of the navigation from the mouse position
	 */
	protected int getNearestPointIndex() {
		int index = -1;
		if (sessionContext != null && sessionContext.getNavigationDataSupplier() != null) {
			double[] cursorSceneCoords = scene.getCursorSceneCoordonates();
			index = sessionContext.getNavigationDataSupplier().getNearestPointIndex(cursorSceneCoords);
		}
		return index;
	}

	/** HighlightPoint the points on the navigation for representing the shiftPeriod */
	protected void highlightPoints(int index) {
		if (index >= 0) {
			eventBroker.send(NavShiftEventTopics.TOPIC_HIGHLIGHT_POINTS,
					new HighlightPointInfos(new IntRange(selectedNavPointIndex, index),
							preferences.getHighlightPeriodColor(), preferences.getHighlightPeriodPointSize()));
		}
	}

	/**
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE && behavior == Behavior.SELECT_VECTOR_2) {
			cancelSelectVector();
		} else if (e.getKeyCode() == KeyEvent.VK_DELETE) {
			if (behavior == Behavior.SELECT_VECTOR_2) {
				cancelSelectVector();
			} else if (behavior == Behavior.SELECT_VECTOR_1) {
				deleteSelectVector();
			}
		}
	}

	/** Cancel current vector editing */
	protected void cancelSelectVector() {
		notifyVectorStoppedChanging();
		highlightPoint(vectorInfos.getStartPositionIndex(), preferences.getHighlightVectorColor(),
				preferences.getHighlightVectorPointSize());
		behavior = Behavior.SELECT_VECTOR_1;
	}

	/** Delete current selected vector */
	protected void deleteSelectVector() {
		eventBroker.send(NavShiftEventTopics.TOPIC_DELETE_VECTOR, null);
	}

	/**
	 * Compute nearest position on the line from the point
	 */
	protected Vec4 getCursorPosition() {
		double[] cursorSceneCoords = scene.getCursorSceneCoordonates();
		return new Vec4(cursorSceneCoords[0], cursorSceneCoords[1]);
	}

	/** {@inheritDoc} */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		super.propertyChange(event);

		// Vectors has changed ?
		if (IShiftPeriod.PROPERTY_VECTORS.equals(event.getPropertyName())
				|| ISessionContext.PROPERTY_SHIFTPERIOD.equals(event.getPropertyName())) {
			computeVectorGeometries();
			behavior = Behavior.SELECT_VECTOR_1;
		} else if (IShiftVector.PROPERTY_END.equals(event.getPropertyName())) {
			updateSelectedVectorGeometries();
		} else if (IShiftPeriod.PROPERTY_VECTOR.equals(event.getPropertyName())) {
			updateSelectedVectorGeometries();
		}
	}

	/** Update the VectorGeometry map */
	protected void computeVectorGeometries() {
		if (getSessionContext().getShiftPeriod() != null) {
			List<IShiftVector> shiftVectors = getSessionContext().getShiftPeriod().getShiftVectors();
			vectorGeometries = shiftVectors.stream().map(VectorGeometry::new)
					.collect(Collectors.toCollection(ArrayList::new));
		}
	}

	/** Update the current VectorGeometry */
	protected void updateSelectedVectorGeometries() {
		selectedVectors.forEach(VectorGeometry::computeFinalPoint);
	}

	/**
	 * Event handler for DRAW_VECTOR
	 */
	@Inject
	@Optional
	protected void onVectorSelected(
			@UIEventTopic(NavShiftEventTopics.TOPIC_VECTORS_SELECTED) List<IShiftVector> shiftVectors) {
		selectedVectors = vectorGeometries.stream() //
				.filter(vg -> shiftVectors.contains(vg.getShiftVector()))//
				.collect(Collectors.toList());
		scene.refresh();
	}

	/**
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(KeyEvent e) {
		// Event not considered
	}

	/**
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		// Event not considered
	}

	/**
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1 //
				&& behavior == Behavior.SELECT_VECTOR_1 //
				&& !selectedVectors.isEmpty()) {

			editedVector = selectedVectors.stream()//
					.filter(v -> v.isFinalPointNearTo(getCursorPosition(), scene.getPixelSize())).findFirst() //
					.orElse(null);
			if (editedVector != null) {
				vectorInfos.setStartPositionIndex(editedVector.getShiftVector().getIndexOrigin());
				vectorInfos.setStopPoint(editedVector.getFinalPoint());
				eventBroker.send(NavShiftEventTopics.TOPIC_DRAW_VECTOR, vectorInfos);

				behavior = Behavior.SELECT_VECTOR_2;
				scene.enableDragging(false);
				e.consume();
			}
		}
	}

	/**
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		// Stop modifying the vector
		if (behavior == Behavior.SELECT_VECTOR_2 && editedVector != null) {
			behavior = Behavior.SELECT_VECTOR_1;
			editedVector = null;
			notifyVectorStoppedChanging();
			eventBroker.send(NavShiftEventTopics.TOPIC_VECTORS_SELECTED,
					selectedVectors.stream().map(VectorGeometry::getShiftVector).collect(Collectors.toList()));
			highlightVector();
			scene.enableDragging(true);
			// Notify something changed
			eventBroker.send(NavShiftEventTopics.TOPIC_IS_DIRTY, Boolean.TRUE);
			eventBroker.send(NavShiftEventTopics.TOPIC_COMPUTE_SHIFT_REQUIRED, this);
		}
	}

	/**
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
		// Event not considered
	}

	/**
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		// Event not considered
	}

	/**
	 * @see java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		// Modify the vector
		if (behavior == Behavior.SELECT_VECTOR_2 && editedVector != null) {
			editedVector.modifyFinalPoint(getCursorPosition());
			vectorInfos.setStopPoint(editedVector.getFinalPoint());
			eventBroker.send(NavShiftEventTopics.TOPIC_DRAW_VECTOR, vectorInfos);
			e.consume();
		}
	}

	/**
	 * Event handler for preview.
	 */
	@Inject
	@Optional
	protected void onPreview(@UIEventTopic(NavShiftEventTopics.PREVIEW) java.util.Optional<Previewer> option) {
		previewIsobathLayer.forEach(scene::removeComponent);
		previewIsobathLayer.clear();
		if (option.isPresent()) {
			previewIsobathLayer.addAll(layerController.getPreviewIsobathLayer());
			layerController.getPreviewIsobathLayer().forEach(scene::addComponent);
			layerController.getPreviewIsobathLayer()
					.forEach(layer -> layer.addPropertyChangeListener(e -> scene.refresh()));
		}

		// events management are not allowed in preview mode
		if (behavior != Behavior.NONE) {
			if (option.isPresent()) {
				unhookListeners();
			} else {
				// Preview closed
				if (sessionContext.getShiftPeriod() == null) {
					// After shift, period may have been reseted
					desactivate();
					activate(Behavior.SELECT_SHIFTPERIOD_1);
				} else {
					hookListeners();
				}
			}
		}
	}
}
