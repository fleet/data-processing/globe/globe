package fr.ifremer.globe.editor.navshift.viewer2d;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import fr.ifremer.globe.core.model.ISaveable;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.application.handlers.CommandExecutor;
import fr.ifremer.globe.editor.navshift.controller.SessionController;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.viewer2d.scene.NavShift2DScene;
import fr.ifremer.globe.editor.navshift.viewer2d.scene.StatusBarComposite;
import fr.ifremer.globe.editor.navshift.viewer2d.toolbox.NavShiftToolboxComposite;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Water column 2D viewer.
 */
public class NavShift2DViewer extends Composite implements ISaveable {

	public static final String PART_ID = "fr.ifremer.globe.editor.navshift.2d.part";

	@Inject
	private ESelectionService selectionService;

	@Inject
	EPartService partService;

	/** MPart representing the NavShift2DViewer */
	@Inject
	@Active
	protected MPart part;

	@Inject
	@Named(ContextNames.SESSION_CONTROLLER)
	protected SessionController sessionController;

	/** Event broker service **/
	@Inject
	protected IEventBroker eventBroker;

	private NavShiftToolboxComposite toolboxComposite;

	/**
	 * Static method to create an {@link NavShift2DViewer} part.
	 */
	public static void createPart(EPartService partService, MApplication application, EModelService modelService) {
		MPart part = partService.createPart(NavShift2DViewer.PART_ID);
		PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_STACK_ID);
		PartManager.showPart(partService, part);
	}

	/**
	 * Constructor
	 *
	 * @throws GIOException
	 */
	@Inject
	public NavShift2DViewer(Composite parent,
			@Optional @Named(ContextNames.SESSION_CONTEXT) ISessionContext newSessionContext) {
		super(parent, SWT.NONE);
		createUI();
	}

	/**
	 * Creates UI
	 */
	public void createUI() {
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.horizontalSpacing = 0;
		gridLayout.verticalSpacing = 0;
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		setLayout(gridLayout);

		SashForm sashForm = new SashForm(this, SWT.NONE);
		sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		// OpenGL 2D scene
		GLCanvas canvas = new GLCanvas(new GLCapabilities(GLProfile.getDefault()));
		new NavShift2DScene(canvas);
		Composite canvasContainer = new Composite(sashForm, SWT.EMBEDDED);
		SWT_AWT.new_Frame(canvasContainer).add(canvas);

		// Toolbox
		toolboxComposite = new NavShiftToolboxComposite(sashForm, SWT.NONE);
		sashForm.setWeights(new int[] { 3, 2 });

		// Status bar
		StatusBarComposite statusBar = new StatusBarComposite(this, SWT.NONE);
		statusBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
	}

	@PostConstruct
	public void postConstruct() {
		toolboxComposite.getExplorerComposite().setSelectionService(selectionService);
	}

	@Persist
	public void save(IProgressMonitor monitor) {
		doSave(monitor);
	}

	/**
	 * @see fr.ifremer.globe.model.ISaveable#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public boolean doSave(IProgressMonitor monitor) {
		Display.getDefault().syncExec(() -> {
			sessionController.saveSessionContext(monitor, getShell());
		});
		return true;
	}

	@PreDestroy
	public void closePart() {
		CommandExecutor.executeCommand(CommandExecutor.CLOSE_COMMAND);
	}

	/**
	 * Event handler for TOPIC_IS_DIRTY
	 */
	@Inject
	@Optional
	protected void setDirty(@UIEventTopic(NavShiftEventTopics.TOPIC_IS_DIRTY) Boolean dirty) {
		if (!isDisposed()) {
			part.setDirty(dirty.booleanValue());
		}
	}

	/**
	 * @see fr.ifremer.globe.model.ISaveable#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return part.isDirty();
	}

}
