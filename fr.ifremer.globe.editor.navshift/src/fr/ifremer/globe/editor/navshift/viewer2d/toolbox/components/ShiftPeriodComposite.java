/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.viewer2d.toolbox.components;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Date;
import java.util.List;

import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.Assert;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.viewer2d.toolbox.dialog.SelectPeriodFromProfilesDialog;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.date.DateUtils;

/**
 * Composite used to define the shift period.
 */
public class ShiftPeriodComposite extends Composite implements PropertyChangeListener {

	/** Event broker service */
	@Inject
	private IEventBroker eventBroker;

	/** Current session */
	private ISessionContext sessionContext;
	private Label startTimeLabel;
	private Label endTimeLabel;

	/** True when {@link #hookListeners()} has been called */
	private boolean listenersHooked = false;
	private Group grpShiftPeriod;
	private Button btnSelectFromProfiles;

	/**
	 * Constructor
	 */
	public ShiftPeriodComposite(Composite parent) {
		super(parent, SWT.NONE);
		createsUI();
		// Injection
		if (ContextInitializer.getEclipseContext() != null) {
			ContextInjectionFactory.inject(this, ContextInitializer.getEclipseContext());
		}
	}

	/**
	 * Creates the UI.
	 */
	private void createsUI() {
		FillLayout fillLayout = new FillLayout(SWT.HORIZONTAL);
		fillLayout.marginHeight = 3;
		fillLayout.marginWidth = 3;
		setLayout(fillLayout);

		grpShiftPeriod = new Group(this, SWT.NONE);
		grpShiftPeriod.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		grpShiftPeriod.setText("Step 1 - Shift period");
		grpShiftPeriod.setLayout(new GridLayout(3, false));

		btnSelectFromProfiles = new Button(grpShiftPeriod, SWT.NONE);
		btnSelectFromProfiles.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnSelectFromProfiles.setToolTipText("Define a new cut line with start and end dates");
		btnSelectFromProfiles.setText("Select period from profiles...");

		btnSelectFromProfiles.addListener(SWT.Selection, e -> new SelectPeriodFromProfilesDialog(getShell()).open());

		Button btnEdit = new Button(grpShiftPeriod, SWT.NONE);
		btnEdit.setText("Edit");
		btnEdit.addListener(SWT.Selection, e -> switchToShiftPeriod());

		Label lblNewLabel = new Label(grpShiftPeriod, SWT.NONE);
		lblNewLabel.setSize(51, 15);
		lblNewLabel.setText("Start time");

		startTimeLabel = new Label(grpShiftPeriod, SWT.NONE);
		startTimeLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		startTimeLabel.setSize(0, 15);
		new Label(grpShiftPeriod, SWT.NONE);

		Label lblEndTime = new Label(grpShiftPeriod, SWT.NONE);
		lblEndTime.setSize(47, 15);
		lblEndTime.setText("End time");

		endTimeLabel = new Label(grpShiftPeriod, SWT.NONE);
		endTimeLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		endTimeLabel.setSize(0, 15);
		new Label(grpShiftPeriod, SWT.NONE);

	}

	/**
	 * @see org.eclipse.swt.widgets.Widget#dispose()
	 */
	@Override
	public void dispose() {
		unhookListener();
		super.dispose();
	}

	@Override
	public void setEnabled(boolean enabled) {
		btnSelectFromProfiles.setEnabled(enabled);
		grpShiftPeriod.setFont(SWTResourceManager.getFont("Segoe UI", 9, enabled ? SWT.BOLD : SWT.NORMAL));
		if (enabled) {
			setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		} else {
			setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BACKGROUND));
		}
	}

	/**
	 * Hook all needed listeners
	 */
	private void hookListeners() {
		Assert.isNotNull(!listenersHooked, "hookListeners() called twice");
		Assert.isNotNull(sessionContext, "Invalid state, sessionContext can not be null");

		sessionContext.addPropertyChangeListener(this);
		if (sessionContext.getShiftPeriod() != null) {
			sessionContext.getShiftPeriod().addPropertyChangeListener(this);
		}

		listenersHooked = true;
	}

	/**
	 * Unhook all listeners set in sendConstruct
	 */
	@PreDestroy
	private void unhookListener() {
		if (sessionContext != null) {
			sessionContext.removePropertyChangeListener(this);
			if (sessionContext.getShiftPeriod() != null) {
				sessionContext.getShiftPeriod().removePropertyChangeListener(this);
			}
			sessionContext = null;
		}
		listenersHooked = false;
	}

	/** Click on ShiftPeriod button to designate the working zone */
	private void switchToShiftPeriod() {
		if (Messages.openSyncQuestionMessage("Navigation Shift",
				"Define a new shift period ? (Removes current vectors) ")) {
			resetPeriod();
		}
	}

	/** Remove period and vectors */
	private void resetPeriod() {
		sessionContext.setShiftPeriod(null);
		acceptShiftPeriod(null);
		eventBroker.send(NavShiftEventTopics.TOPIC_VECTOR_CREATION_STOP, this);
		eventBroker.send(NavShiftEventTopics.TOPIC_SHIFTPERIOD_SELECTION_START, this);
	}

	/**
	 * SessionContext changed. Called by DI only
	 */
	@Inject
	public void setSessionContext(@Optional @Named(ContextNames.SESSION_CONTEXT) ISessionContext newSessionContext) {
		// Session closed. Empty the composite
		if (newSessionContext == null) {
			unhookListener();
		}
		sessionContext = newSessionContext;
	}

	/** Session has been activated successfully */
	@Inject
	@Optional
	public void onContextActivated(
			@UIEventTopic(NavShiftEventTopics.SESSION_ACTIVATED) ISessionContext activatedSessionContext) {
		Assert.isNotNull(sessionContext, "Invalid state, sessionContext can not be null");
		Assert.isTrue(sessionContext == activatedSessionContext, "Invalid state, wrong activated session");
		if (!isDisposed()) {
			getDisplay().syncExec(() -> {
				hookListeners();
				if (sessionContext.getShiftPeriod() != null) {
					eventBroker.send(NavShiftEventTopics.TOPIC_VECTOR_CREATION_START, this);
				} else {
					eventBroker.send(NavShiftEventTopics.TOPIC_SHIFTPERIOD_SELECTION_START, this);
				}
				acceptShiftPeriod(sessionContext.getShiftPeriod());
			});
		}
	}

	private void acceptShiftPeriod(IShiftPeriod shiftPeriod) {
		if (shiftPeriod != null) {
			startTimeLabel.setText(DateUtils.formatDate(new Date(shiftPeriod.getStartTime())));
			endTimeLabel.setText(DateUtils.formatDate(new Date(shiftPeriod.getEndTime())));
		} else {
			startTimeLabel.setText("");
			endTimeLabel.setText("");
		}
	}

	/** Handle the SHIFTPERIOD_SELECTION_START event */
	@Inject
	@Optional
	private void startSelectingShiftPeriod(
			@UIEventTopic(NavShiftEventTopics.TOPIC_SHIFTPERIOD_SELECTION_START) Object poster) {
		if (isDisposed()) {
			return;
		}
		setEnabled(true);
	}

	/**
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		getDisplay().asyncExec(() -> {
			if (ISessionContext.PROPERTY_SHIFTPERIOD.equals(event.getPropertyName())) {
				if (event.getNewValue() != null) {
					// New shift period defined
					IShiftPeriod period = (IShiftPeriod) event.getNewValue();
					period.addPropertyChangeListener(this);
					acceptShiftPeriod(period);
					setEnabled(false);
				}
				if (event.getOldValue() != null) {
					IShiftPeriod old = (IShiftPeriod) event.getOldValue();
					old.removePropertyChangeListener(this);
				}
			}
		});
	}

	/**
	 * Event handler for TOPIC_FILES_SHIFTED<br>
	 * Some files have been shifted. Reset period if necessary
	 */
	@Inject
	@Optional
	private void onFilesShifted(@UIEventTopic(NavShiftEventTopics.TOPIC_FILES_SHIFTED) List<File> files) {
		// Search if a profile has been modified
		boolean oneProfileModified = sessionContext.getProfiles().stream()//
				.map(IProfile::getSounderNcInfo)//
				.map(ISounderNcInfo::toFile)//
				.anyMatch(files::contains);
		if (oneProfileModified) {
			getDisplay().syncExec(() -> {
				sessionContext.setShiftPeriod(null);
				acceptShiftPeriod(null);
			});
		}
	}

}
