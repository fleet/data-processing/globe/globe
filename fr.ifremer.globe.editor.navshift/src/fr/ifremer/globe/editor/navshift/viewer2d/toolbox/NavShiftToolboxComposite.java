package fr.ifremer.globe.editor.navshift.viewer2d.toolbox;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.viewer2d.toolbox.components.PreviewComposite;
import fr.ifremer.globe.editor.navshift.viewer2d.toolbox.components.ShiftPeriodComposite;
import fr.ifremer.globe.editor.navshift.viewer2d.toolbox.components.ShiftVectorComposite;
import fr.ifremer.globe.editor.navshift.viewer2d.toolbox.components.explorer.ExplorerComposite;

public class NavShiftToolboxComposite extends Composite {

	private ExplorerComposite explorerComposite;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public NavShiftToolboxComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		setLayout(gridLayout);

		explorerComposite = new ExplorerComposite(this);
		GridData gdExplorerComposite = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gdExplorerComposite.heightHint = 300;
		explorerComposite.setLayoutData(gdExplorerComposite);

		ShiftPeriodComposite shiftPeriodComposite = new ShiftPeriodComposite(this);
		shiftPeriodComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		ShiftVectorComposite shiftVectorComposite = new ShiftVectorComposite(this);
		shiftVectorComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		PreviewComposite previewComposite = new PreviewComposite(this);
		previewComposite.setLayoutData( new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		// asyncExec to ensure the composite is fully initialized and injected
		getDisplay().asyncExec(() -> {
			// Force the initialization of the composite
			ISessionContext sessionContext = ContextInitializer.getInContext(ContextNames.SESSION_CONTEXT);
			if (sessionContext != null) {
				shiftVectorComposite.setSessionContext(sessionContext);
				shiftVectorComposite.onContextActivated(sessionContext);
				shiftPeriodComposite.setSessionContext(sessionContext);
				shiftPeriodComposite.onContextActivated(sessionContext);
			}
		});
	}
	
	public  ExplorerComposite getExplorerComposite() {
		return explorerComposite;
	}
}
