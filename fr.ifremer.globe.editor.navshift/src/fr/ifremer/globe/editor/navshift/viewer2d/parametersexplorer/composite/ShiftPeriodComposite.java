/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.viewer2d.parametersexplorer.composite;

import jakarta.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;

/**
 * Composite display in the parameters explorer when Shift period node is selected in the project explorer
 */
public class ShiftPeriodComposite extends Composite {

	/** Event broker service */
	@Inject
	protected IEventBroker eventBroker;

	/** Widget */
	protected Button btnShowShiftedNav;

	/**
	 * Create the composite.
	 */
	public ShiftPeriodComposite(Composite parent) {
		super(parent, SWT.NONE);
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.verticalSpacing = 20;
		setLayout(gridLayout);

		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		GridLayout glComposite = new GridLayout(1, false);
		glComposite.verticalSpacing = 10;
		glComposite.marginHeight = 0;
		glComposite.marginWidth = 0;
		composite.setLayout(glComposite);

		btnShowShiftedNav = new Button(composite, SWT.CHECK);
		btnShowShiftedNav.addSelectionListener(SelectionListener.widgetSelectedAdapter(event -> eventBroker
				.send(NavShiftEventTopics.TOPIC_COMPUTE_SHIFT_ENABLED, btnShowShiftedNav.getSelection())));
		btnShowShiftedNav.setText("Show shifted navigation");
		
		ContextInitializer.inject(this);
	}

	/** Session has been activated successfully */
	@Inject
	@Optional
	public void onContextActivated(
			@UIEventTopic(NavShiftEventTopics.SESSION_ACTIVATED) ISessionContext activatedSessionContext) {
		eventBroker.send(NavShiftEventTopics.TOPIC_COMPUTE_SHIFT_ENABLED, false);
	}

}
