package fr.ifremer.globe.editor.navshift.viewer2d.scene;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.HighlightPointInfos;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.application.event.VectorInfos;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.impl.ShiftVectorImpl;
import fr.ifremer.globe.utils.date.DateUtils;

public class StatusBarComposite extends Composite {
	private Label lblCenter;

	/** Session to preview */
	@Inject
	@Optional
	@Named(ContextNames.SESSION_CONTEXT)
	protected ISessionContext sessionContext;

	/**
	 * Constructor
	 */
	public StatusBarComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new FillLayout(SWT.HORIZONTAL));

		lblCenter = new Label(this, SWT.NONE);
		lblCenter.setText("");

		// Injection
		ContextInjectionFactory.inject(this, ContextInitializer.getEclipseContext());
	}

	/**
	 * Displays a message during the specified delay (ms).
	 */
	public void diplayMessage(String msg, int delay) {
		lblCenter.setText(msg);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				lblCenter.setText("");
			}
		}, delay);
	}

	/**
	 * Event handler for DRAW_VECTOR
	 */
	@Inject
	@Optional
	protected void onTopicDraw(@UIEventTopic(NavShiftEventTopics.TOPIC_DRAW_VECTOR) VectorInfos vectorInfos) {
		if (isDisposed())
			return;

		lblCenter.setText(
				vectorInfos != null ? new ShiftVectorImpl(sessionContext.getShiftPeriod(), vectorInfos).toString()
						: "");
	}

	/**
	 * Event handler for TOPIC_HIGHLIGHT_POINT : one point is highlighted
	 */
	@Inject
	@Optional
	protected void onPointHightlight(
			@UIEventTopic(NavShiftEventTopics.TOPIC_HIGHLIGHT_POINT) HighlightPointInfos highlightPointInfos) {
		if (isDisposed())
			return;
		INavigationModel navigationDataSupplier = sessionContext.getNavigationDataSupplier();
		int index = highlightPointInfos.getIndexes().getMinimumInteger();
		String dateStr = DateUtils.formatDate(new Date(navigationDataSupplier.getTime(index)));
		lblCenter.setText(dateStr);
	}

	/**
	 * Event handler for TOPIC_HIGHLIGHT_POINTS : several points highlighted (= period selection)
	 */
	@Inject
	@Optional
	protected void onPointsHightlight(
			@UIEventTopic(NavShiftEventTopics.TOPIC_HIGHLIGHT_POINTS) HighlightPointInfos highlightPointInfos) {
		if (isDisposed())
			return;

		StringBuilder stringBuiler = new StringBuilder("Shift period [start: ");
		INavigationModel navigationDataSupplier = sessionContext.getNavigationDataSupplier();
		int startIndex = highlightPointInfos.getIndexes().getMinimumInteger();
		int endIndex = highlightPointInfos.getIndexes().getMaximumInteger();

		String dateStr = DateUtils.formatDate(new Date(navigationDataSupplier.getTime(startIndex)));
		// navigationDataSupplier.get
		stringBuiler.append(dateStr);
		if (startIndex != endIndex) {
			dateStr = DateUtils.formatDate(new Date(navigationDataSupplier.getTime(endIndex)));
			stringBuiler.append(" ;end: " + dateStr + "]");
		}

		lblCenter.setText(stringBuiler.toString());
	}

}
