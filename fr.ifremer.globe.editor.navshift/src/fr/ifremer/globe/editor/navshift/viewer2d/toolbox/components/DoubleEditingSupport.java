/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.viewer2d.toolbox.components;

import java.util.function.ObjDoubleConsumer;
import java.util.function.ToDoubleFunction;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.ui.widget.SpinnerCellEditor;

/** Support for cell editing of any double property using a spinner */
class DoubleEditingSupport<T> extends EditingSupport {

	protected ToDoubleFunction<T> valueSupplier;
	protected ObjDoubleConsumer<T> valueConsumer;
	int minimum;
	int maximum;
	int increment;
	int digits;
	private Runnable editListener;

	public DoubleEditingSupport(TableViewer tableViewer, double minimum, double maximum, double increment, int digits,
			ToDoubleFunction<T> valueSupplier, ObjDoubleConsumer<T> valueConsumer, Runnable editListener) {
		super(tableViewer);
		this.minimum = (int) (minimum * Math.pow(10d, digits));
		this.maximum = (int) (maximum * Math.pow(10d, digits));
		this.increment = (int) (increment * Math.pow(10d, digits));
		this.digits = digits;
		this.valueConsumer = valueConsumer;
		this.valueSupplier = valueSupplier;
		this.editListener = editListener;
	}

	/**
	 * @see org.eclipse.jface.viewers.EditingSupport#setValue(java.lang.Object, java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void setValue(Object element, Object value) {
		valueConsumer.accept((T) element, ((Number) value).doubleValue() / Math.pow(10d, digits));
		getViewer().refresh(element);
		editListener.run();
	}

	/**
	 * @see org.eclipse.jface.viewers.EditingSupport#getValue(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected Object getValue(Object element) {
		return Integer.valueOf((int) (valueSupplier.applyAsDouble((T) element) * Math.pow(10d, digits)));
	}

	/**
	 * @see org.eclipse.jface.viewers.EditingSupport#getCellEditor(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected CellEditor getCellEditor(Object element) {
		SpinnerCellEditor result = new SpinnerCellEditor(((TableViewer) getViewer()).getTable(), SWT.NONE, minimum,
				maximum, increment, digits);
		Spinner spinner = (Spinner) result.getControl();
		spinner.addListener(SWT.Selection, event -> {
			valueConsumer.accept((T) element, spinner.getSelection() / Math.pow(10d, digits));
			editListener.run();
		});

		// Cancel editing with ESC
		double initialValue = valueSupplier.applyAsDouble((T) element);
		spinner.addKeyListener(KeyListener.keyPressedAdapter(event -> {
			if (event.keyCode == SWT.ESC) {
				valueConsumer.accept((T) element, initialValue);
				editListener.run();
			}
		}));
		return result;
	}

	/**
	 * @see org.eclipse.jface.viewers.EditingSupport#canEdit(java.lang.Object)
	 */
	@Override
	protected boolean canEdit(Object element) {
		return true;
	}
}