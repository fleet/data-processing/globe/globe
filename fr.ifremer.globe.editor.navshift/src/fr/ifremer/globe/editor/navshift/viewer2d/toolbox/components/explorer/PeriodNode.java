package fr.ifremer.globe.editor.navshift.viewer2d.toolbox.components.explorer;

import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import gov.nasa.worldwind.layers.Layer;

/** TreeNode to hold Period, Vectors and navigation */
public class PeriodNode extends LayerNode {

	/** Constructor */
	public PeriodNode(Layer layer) {
		super(layer);
		image = ImageResources.getImage("icons/vector.png", getClass());
	}

}
