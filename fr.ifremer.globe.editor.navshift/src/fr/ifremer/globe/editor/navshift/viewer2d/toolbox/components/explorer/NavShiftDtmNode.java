/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.viewer2d.toolbox.components.explorer;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;

/**
 * TreeNode displayed in layer explorer and representing the Navigation Shift Editor
 */
public class NavShiftDtmNode extends GroupNode {

	/** Parameter composite */
	protected Composite composite = null;

	/**
	 * currently used sessionContext
	 */
	@Optional
	@Inject
	@Named(ContextNames.SESSION_CONTEXT)
	ISessionContext sessionContext;

	/** Event broker service */
	@Inject
	protected IEventBroker eventBroker;

	/**
	 * Constructor.
	 */
	public NavShiftDtmNode() {
		super("Input files DTM", Icons.DTM.toImage());
		// Injection
		ContextInjectionFactory.inject(this, ContextInitializer.getEclipseContext());
	}
}
