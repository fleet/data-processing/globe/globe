/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.handlers;

import java.io.File;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectMenuItem;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.commons.NavShiftPreference;
import fr.ifremer.globe.editor.navshift.controller.SessionController;
import fr.ifremer.globe.editor.navshift.viewer2d.NavShift2DViewer;
import fr.ifremer.globe.editor.navshift.wizard.open.NavShiftEditorWizard;
import fr.ifremer.globe.editor.navshift.wizard.open.NavShiftEditorWizardModel;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.utils.exception.GException;

/**
 * Eclipse Handler invoked by user to open the Navigation Shift Editor
 */
public class OpenNavigationShiftEditorHandler extends AbstractNodeHandler {

	/** Saved preferences */
	@Inject
	@Optional
	@Named(ContextNames.NAVSHIFT_PREFERENCE)
	protected NavShiftPreference preferences;

	/** Saved preferences */
	@Inject
	protected IFileService fileService;
	@Inject
	protected IRasterService rasterService;

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService, EPartService partService,
			@Optional @Named(ContextNames.SESSION_CONTROLLER) SessionController sessionController,
			@Optional @Active MDirectMenuItem item) {
		boolean result = true;
		if (sessionController != null) {
			result = sessionController.getSessionContext() == null;
		}
		if (item == null) {
			return result && checkExecution(selectionService, modelService);
		} else {
			return result;
		}
	}

	/** {@inheritDoc} */
	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		Object[] selection = getSelection(selectionService);
		return !getSelectionAsList(selection, //
				contentType -> contentType.isOneOf(//
						ContentType.MBG_NETCDF_3, //
						ContentType.XSF_NETCDF_4, //
						ContentType.DTM_NETCDF_3, //
						ContentType.DTM_NETCDF_4, //
						ContentType.RASTER_GDAL//
				)).isEmpty();
	}

	/**
	 * Execution required...<br>
	 * Creates and launch the wizard.
	 */
	@Execute
	public void execute(@Named(ContextNames.SESSION_CONTROLLER) SessionController sessionController,
			@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, @Optional @Active MDirectMenuItem item,
			ESelectionService selectionService, EPartService partService, MApplication application,
			EModelService modelService) {

		Object[] selection = getSelection(selectionService);
		AtomicReference<NavShiftEditorWizardModel> modelRef = new AtomicReference<>();
		AtomicReference<NavShiftEditorWizard> wizardRef = new AtomicReference<>();

		if (selection != null && selection.length > 0 && item == null) {
			loadSelectedFiles(shell, selection, modelRef, wizardRef);
		} else if (item != null) {
			// Called from menu Tools
			loadFilesFromPreferences(shell, modelRef, wizardRef);
		}

		// Launch wizard
		if (wizardRef.get() != null) {
			wizardRef.get().setWindowTitle(item != null ? item.getLabel() : "Navigation shift");

			WizardDialog wizardDialog = new WizardDialog(shell, wizardRef.get());
			wizardDialog.addPageChangingListener(wizardRef.get()::pageChanging);
			wizardDialog.setMinimumPageSize(300, 300);
			if (wizardDialog.open() == Window.OK) {
				updateModel(modelRef.get());
				savePreferences(modelRef.get());
				launchNavigationShiftEditor(sessionController, modelRef.get());
				modelRef.get().dispose();

				NavShift2DViewer.createPart(partService, application, modelService);
			}
		}
	}

	/** Dispatch all input files in a ISounderNcInfo list or DtmInfo one */
	protected void updateModel(NavShiftEditorWizardModel model) {
		List<ISounderNcInfo> infoStores = model.getInputFiles().stream() //
				.map(File::getPath) //
				.map(fileService::getFileInfo)//
				.filter(java.util.Optional::isPresent)//
				.map(java.util.Optional::get) //
				.filter(ISounderNcInfo.class::isInstance) //
				.map(ISounderNcInfo.class::cast)//
				.collect(Collectors.toList());
		model.setInputSounderNcInfo(infoStores);

		List<RasterInfo> rasterInfos = model.getInputFiles().stream() //
				.map(File::getAbsolutePath) //
				.map(rasterService::getRasterInfos)//
				.map(/* List<RasterInfo> */ list -> list.stream()
						.filter(rasterInfo -> RasterInfo.RASTER_ELEVATION.equalsIgnoreCase(rasterInfo.getDataType()))
						.findFirst())
				.filter(java.util.Optional::isPresent)//
				.map(java.util.Optional::get) //
				.collect(Collectors.toList());

		model.setInputRasterInfo(rasterInfos);
	}

	/** Serialize model to preferences */
	protected void savePreferences(NavShiftEditorWizardModel model) {
		preferences.getWizardInputFiles().setValue(model.getInputFiles(), false);

		// overwrite isobath setting in preferences
		List.of(preferences.getIsobathMasterLine(), preferences.getReferenceIsobathMasterLine(),
				preferences.getPreviewMasterLine()).forEach(pref -> pref.setValue(model.getMainLine().get(), false));
		List.of(preferences.getIsobathStep(), preferences.getReferenceIsobathStep(), preferences.getPreviewStep())
				.forEach(pref -> pref.setValue(model.getStep().get(), false));
		List.of(preferences.getIsobathSampling(), preferences.getReferenceIsobathSampling(),
				preferences.getPreviewSampling()).forEach(pref -> pref.setValue(model.getSampling().get(), false));

		preferences.save();
	}

	/** Loading files selected from the project explorer */
	protected void loadSelectedFiles(Shell shell, Object[] selection,
			AtomicReference<NavShiftEditorWizardModel> modelRef, AtomicReference<NavShiftEditorWizard> wizardRef) {

		// set input sounder files
		NavShiftEditorWizardModel model = new NavShiftEditorWizardModel();
		modelRef.set(model);

		List<File> files = getSelectionAsList(selection, //
				contentType -> contentType.isOneOf(ContentType.MBG_NETCDF_3, ContentType.XSF_NETCDF_4,
						ContentType.DTM_NETCDF_3, ContentType.DTM_NETCDF_4, ContentType.RASTER_GDAL))//
								.stream()//
								.map(FileInfoNode::getFileInfo)//
								.map(IFileInfo::toFile)//
								.collect(Collectors.toList());
		model.getInputFiles().addAll(files);

		NavShiftEditorWizard wizard = new NavShiftEditorWizard(model);
		wizardRef.set(wizard);
		ContextInitializer.inject(wizard);
		loadFiles(shell, modelRef, wizardRef, wizard);
	}

	/** Open files */
	protected void loadFiles(Shell shell, AtomicReference<NavShiftEditorWizardModel> modelRef,
			AtomicReference<NavShiftEditorWizard> wizardRef, NavShiftEditorWizard wizard) {
		BusyIndicator.showWhile(shell.getDisplay(), () -> {
			try {
				wizard.loadFiles();
			} catch (GException e) {
				Messages.openErrorMessage("Navigation Shift - Fatal",
						String.format("Unable to open Navigation Shift editor: %s", e.getMessage()));
				modelRef.set(null);
				wizardRef.set(null);
			}
		});
	}

	/** Loading files present in preferences if any */
	protected void loadFilesFromPreferences(Shell shell, AtomicReference<NavShiftEditorWizardModel> modelRef,
			AtomicReference<NavShiftEditorWizard> wizardRef) {
		NavShiftEditorWizardModel model = new NavShiftEditorWizardModel();
		modelRef.set(model);

		List<File> inputFiles = preferences.getWizardInputFiles().getValue();
		if (inputFiles != null && !inputFiles.isEmpty()) {
			inputFiles = inputFiles.stream().filter(File::exists).collect(Collectors.toList());
			model.getInputFiles().addAll(inputFiles);
		}

		NavShiftEditorWizard wizard = new NavShiftEditorWizard(model);
		wizardRef.set(wizard);
		ContextInitializer.inject(wizard);
		if (!model.getInputFiles().isEmpty()) {
			loadFiles(shell, modelRef, wizardRef, wizard);
			model.syncDtmCellSize(preferences.getWizardDtmCellSize());
			model.syncMainLine(preferences.getIsobathMasterLine());
			model.syncStep(preferences.getIsobathStep());
			model.syncSampling(preferences.getIsobathSampling());
		}
	}

	/**
	 * Launch the editor.
	 */
	protected void launchNavigationShiftEditor(SessionController sessionController,
			NavShiftEditorWizardModel navigationShiftEditorWizardModel) {
		sessionController.activateSession((sessionCtl, sessionContext) -> {
			sessionContext.setCurrentFileset(navigationShiftEditorWizardModel.getInputFiles().asList());
			sessionContext.setGeoBox(navigationShiftEditorWizardModel.getGeoBox().get());
			sessionContext.setSounderNcInfos(navigationShiftEditorWizardModel.getInputSounderNcInfo());
			sessionContext.setReferenceRasterInfo(navigationShiftEditorWizardModel.getInputRasterInfo());
			sessionContext.setDtmResolution(navigationShiftEditorWizardModel.getDtmCellSize().doubleValue());
			sessionContext.setIsobathMainLine(navigationShiftEditorWizardModel.getMainLine().intValue());
			sessionContext.setIsobathStep(navigationShiftEditorWizardModel.getStep().doubleValue());
			sessionContext.setIsobathSampling(navigationShiftEditorWizardModel.getSampling().intValue());

			// Save in preference
			preferences.getWizardDtmCellSize().setValue(navigationShiftEditorWizardModel.getDtmCellSize().doGetValue(),
					true);
			preferences.getIsobathMasterLine().setValue(navigationShiftEditorWizardModel.getMainLine().intValue(),
					true);
			preferences.getIsobathStep().setValue(navigationShiftEditorWizardModel.getStep().doubleValue(), true);
			preferences.getIsobathSampling().setValue(navigationShiftEditorWizardModel.getSampling().intValue(), true);
			return true;
		});
	}

}