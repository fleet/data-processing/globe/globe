/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.context;

import java.io.File;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.osgi.service.event.Event;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.editor.navshift.commons.IsobathParametersFactory;
import fr.ifremer.globe.editor.navshift.commons.NavShiftPreference;
import fr.ifremer.globe.editor.navshift.controller.IsobathController;
import fr.ifremer.globe.editor.navshift.controller.NavigationLayerController;
import fr.ifremer.globe.editor.navshift.controller.NavigationModelController;
import fr.ifremer.globe.editor.navshift.controller.ProfileController;
import fr.ifremer.globe.editor.navshift.controller.SessionController;
import fr.ifremer.globe.editor.navshift.controller.ShiftPeriodController;
import fr.ifremer.globe.editor.navshift.controller.Viewer2DLayerController;
import fr.ifremer.globe.editor.navshift.process.ShiftedNavigtionRefresher;
import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * Application Addon used to initialize the IEclipseContext
 */
public class ContextInitializer {

	/** Static reference of Eclipse context initialized by E4Application */
	protected static IEclipseContext eclipseContext;

	/**
	 * Invoked when Globe application is started.<br>
	 * Fill the context with needed components
	 * 
	 * @param injectedEclipseContext Eclipse context initialized by E4Application
	 */
	@Inject
	@Optional
	public void activate(@UIEventTopic(UIEvents.UILifeCycle.APP_STARTUP_COMPLETE) Event event,
			IEclipseContext injectedEclipseContext) {
		activate(injectedEclipseContext);
	}

	/**
	 * Fill the context with needed components and initialize the context
	 */
	public static void activate(IEclipseContext eclipseContext) {
		setEclipseContext(eclipseContext);
		init();
	}

	/**
	 * Init the singleton instance, create by DI, it registers reference instances
	 * of objects used througout the NavShift application with current context
	 */
	protected static void init() {
		eclipseContext.set(ContextNames.SOUNDER_DATA_CONTAINER_OWNER, new IDataContainerOwner() {
			@Override
			public String getName() {
				return "Navigation Shift Editor";
			}
		});
		fitTempFolder();
		fitProjections();
		fitPreferences();
		fitControllers();
	}

	/** Set controllers in context */
	protected static void fitControllers() {
		Viewer2DLayerController viewer2DLayerController = ContextInjectionFactory.make(Viewer2DLayerController.class,
				eclipseContext);
		eclipseContext.set(ContextNames.VIEWER_2D_LAYER_CONTROLLER, viewer2DLayerController);

		ShiftPeriodController shiftperiodController = ContextInjectionFactory.make(ShiftPeriodController.class,
				eclipseContext);
		eclipseContext.set(ContextNames.SHIFTPERIOD_CONTROLLER, shiftperiodController);

		NavigationModelController navigationModelController = ContextInjectionFactory
				.make(NavigationModelController.class, eclipseContext);
		eclipseContext.set(ContextNames.NAVIGATION_MODEL_CONTROLLER, navigationModelController);

		ProfileController profileController = ContextInjectionFactory.make(ProfileController.class, eclipseContext);
		eclipseContext.set(ContextNames.PROFILE_CONTROLLER, profileController);

		IsobathController isobathController = ContextInjectionFactory.make(IsobathController.class, eclipseContext);
		eclipseContext.set(ContextNames.ISOBATH_CONTROLLER, isobathController);

		NavigationLayerController navigationLayerController = ContextInjectionFactory
				.make(NavigationLayerController.class, eclipseContext);
		eclipseContext.set(ContextNames.NAVIGATION_LAYER_CONTROLLER, navigationLayerController);

		SessionController sessionController = ContextInjectionFactory.make(SessionController.class, eclipseContext);
		eclipseContext.set(ContextNames.SESSION_CONTROLLER, sessionController);

		ShiftedNavigtionRefresher shiftedNavigtionRefresher = ContextInjectionFactory
				.make(ShiftedNavigtionRefresher.class, eclipseContext);
		eclipseContext.set(ContextNames.SHIFTED_NAVIGTION_REFRESHER, shiftedNavigtionRefresher);
	}

	/** Set preferences in context */
	protected static void fitPreferences() {
		// In case of JUnit tests, preference may be instanciated twice.
		PreferenceComposite rootNode = PreferenceRegistry.getInstance().getEditorsSettingsNode();
		PreferenceComposite navShiftPreference = rootNode.getChild(NavShiftPreference.NAVSHIFT_PREF_NAME);
		eclipseContext.set(ContextNames.NAVSHIFT_PREFERENCE,
				navShiftPreference != null ? navShiftPreference : new NavShiftPreference(rootNode));

		// Creates isobath parameter factory (based on preferences)
		IsobathParametersFactory isobathParametersFactory = ContextInjectionFactory.make(IsobathParametersFactory.class,
				eclipseContext);
		eclipseContext.set(ContextNames.ISOBATH_PARAM_FACTORY, isobathParametersFactory);
	}

	/** Set projections in context */
	protected static void fitProjections() {
		eclipseContext.set(ContextNames.LONGLAT_PROJECTION, new Projection(StandardProjection.LONGLAT));
		eclipseContext.set(ContextNames.MERCATOR_PROJECTION, new Projection(StandardProjection.MERCATOR));
	}

	/**
	 * Getter of {@link #eclipseContext}
	 */
	public static IEclipseContext getEclipseContext() {
		return eclipseContext;
	}

	/**
	 * Setter of {@link #eclipseContext}
	 */
	public static void setEclipseContext(IEclipseContext newEclipseContext) {
		eclipseContext = newEclipseContext;
	}

	/**
	 * Returns the context value associated with the given name
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getInContext(String name) {
		return (T) getEclipseContext().get(name);
	}

	/**
	 * Sets a value to be associated with a given name in this context
	 */
	public static void setInContext(String name, Object value) {
		getEclipseContext().set(name, value);
	}

	/**
	 * Removes the given name and any corresponding value from this context
	 */
	public static void removeFromContext(String name) {
		getEclipseContext().remove(name);
	}

	/**
	 * @return an instance of the specified class and inject it with the context.
	 */
	public static <T> T make(Class<T> clazz) {
		return ContextInjectionFactory.make(clazz, eclipseContext);
	}

	/**
	 * Injects a context into a domain object.
	 */
	public static void inject(Object object) {
		ContextInjectionFactory.inject(object, getEclipseContext());
	}

	/**
	 * Initalize the MERCATOR_PROJECTION context object
	 * 
	 * @throws ProjectionException
	 */
	public static Projection computeMercatorProjection(GeoBox geoBox) throws ProjectionException {
		GeoPoint center = geoBox.getCenter();
		Projection mercatorProjection = new Projection(
				StandardProjection.getTransverseMercatorProjectionSettings(center.getLong(), center.getLat()));

		mercatorProjection.setProjectedGeoBox(geoBox);
		eclipseContext.set(ContextNames.MERCATOR_PROJECTION, mercatorProjection);
		return mercatorProjection;
	}

	/** Fit the temporary folder */
	public static void fitTempFolder() {
		// Create temporary folder
		String prefix = "NavShift_";
		File tmpDir = new File(TemporaryCache.getRootPath(), prefix + System.currentTimeMillis());
		eclipseContext.set(ContextNames.TMP_FOLDER, tmpDir);
	}

}
