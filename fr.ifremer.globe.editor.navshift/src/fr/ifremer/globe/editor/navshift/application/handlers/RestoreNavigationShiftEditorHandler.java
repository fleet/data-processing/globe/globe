/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.handlers;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectMenuItem;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.commons.NavShiftPreference;
import fr.ifremer.globe.editor.navshift.commons.SounderNcInfoUtility;
import fr.ifremer.globe.editor.navshift.controller.SessionController;
import fr.ifremer.globe.editor.navshift.viewer2d.NavShift2DViewer;
import fr.ifremer.globe.editor.navshift.wizard.restore.RestoreSessionContextModel;
import fr.ifremer.globe.editor.navshift.wizard.restore.RestoreSessionContextWizard;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Eclipse Handler invoked by user to open the Navigation Shift Editor
 */

public class RestoreNavigationShiftEditorHandler {

	/** Raster service **/
	@Inject
	protected IRasterService rasterService;

	/** Saved preferences */
	@Inject
	@Named(ContextNames.NAVSHIFT_PREFERENCE)
	protected NavShiftPreference preferences;

	/**
	 * Execution required...<br>
	 * Creates and launch the wizard.
	 */
	@Execute
	public void execute(@Named(ContextNames.SESSION_CONTROLLER) SessionController sessionController,
			@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, @Active MDirectMenuItem item, EPartService partService,
			MApplication application, EModelService modelService) {

		RestoreSessionContextModel restoreSessionModel = new RestoreSessionContextModel();

		// Restore from preferences
		Path prefSessionFilename = preferences.getSessionFile().getValue();
		if (prefSessionFilename != null) {
			File sessionFile = prefSessionFilename.toFile();
			if (sessionFile.exists()) {
				restoreSessionModel.getSelectedFile().set(sessionFile);
			}
		}

		RestoreSessionContextWizard restoreSessionWizard = new RestoreSessionContextWizard(restoreSessionModel);
		restoreSessionWizard.setWindowTitle(item.getLabel());

		WizardDialog wizardDialog = new WizardDialog(shell, restoreSessionWizard);
		wizardDialog.setMinimumPageSize(300, 510);
		shell.getDisplay().asyncExec(() -> {
			if (wizardDialog.open() == Window.OK) {
				launchNavigationShiftEditor(sessionController, restoreSessionModel);
				preferences.getSessionFile().setValue(restoreSessionModel.getSelectedFile().get().toPath(), false);
				preferences.save();

				NavShift2DViewer.createPart(partService, application, modelService);
			}
		});
	}

	/**
	 * Launch the editor.
	 */
	protected void launchNavigationShiftEditor(SessionController sessionController,
			RestoreSessionContextModel restoreSessionModel) {
		sessionController.activateSession((sessionCtl, sessionContext) -> {
			try {
				// Load the given context file
				sessionController.loadContextFile(restoreSessionModel.getSelectedFile().get());

				// get sounder files (xsf, mbg...)
				List<ISounderNcInfo> sounderNcInfos = new ArrayList<>();
				SounderNcInfoUtility.load(sessionContext.getCurrentFileset(), sounderNcInfos::addAll,
						sessionController.getSessionContext()::setGeoBox, Optional.empty());
				sessionController.getSessionContext().setSounderNcInfos(sounderNcInfos);

				// get reference rasters
				List<RasterInfo> rasterInfos = sessionContext.getCurrentFileset().stream() //
						.map(File::getAbsolutePath) //
						.map(rasterService::getRasterInfos)//
						.map(/* List<RasterInfo> */ list -> list.stream()
								.filter(rasterInfo -> RasterInfo.RASTER_ELEVATION
										.equalsIgnoreCase(rasterInfo.getDataType()))
								.findFirst())
						.filter(java.util.Optional::isPresent)//
						.map(java.util.Optional::get) //
						.collect(Collectors.toList());
				sessionController.getSessionContext().setReferenceRasterInfo(rasterInfos);

				ContextInitializer.computeMercatorProjection(sessionController.getSessionContext().getGeoBox());
			} catch (GIOException | ProjectionException e) {
				Messages.openErrorMessage("File Error",
						String.format("Unable to load selected Context file: %s", e.getMessage()));
				return false;
			}

			return true;
		});
	}

}