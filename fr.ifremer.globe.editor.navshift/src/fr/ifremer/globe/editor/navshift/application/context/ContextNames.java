/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.context;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.editor.navshift.commons.IsobathParametersFactory;
import fr.ifremer.globe.editor.navshift.controller.IsobathController;
import fr.ifremer.globe.editor.navshift.controller.NavigationLayerController;
import fr.ifremer.globe.editor.navshift.controller.NavigationModelController;
import fr.ifremer.globe.editor.navshift.controller.ProfileController;
import fr.ifremer.globe.editor.navshift.controller.SessionController;
import fr.ifremer.globe.editor.navshift.controller.ShiftPeriodController;
import fr.ifremer.globe.editor.navshift.controller.Viewer2DLayerController;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.process.ShiftedNavigtionRefresher;

/**
 * All names of injectable objects stored in the IEclipseContext
 */
public class ContextNames {

	/** Root name of all objects that can be injected */
	public static final String BASE_NAME = "fr.ifremer.globe.editor.navshift";

	/** Name of the instance {@link ISessionContext} */
	public static final String SESSION_CONTEXT = BASE_NAME + ".SESSION_CONTEXT";

	/** Name of the owner of all booked SounderDataContainer */
	public static final String SOUNDER_DATA_CONTAINER_OWNER = BASE_NAME + ".SOUNDER_DATA_CONTAINER_OWNER";

	/** Name of the singleton {@link SessionController} */
	public static final String SESSION_CONTROLLER = BASE_NAME + ".SESSION_CONTROLLER";

	/** Name of the {@link Projection} WGS84, EPSG: 4326 */
	public static final String LONGLAT_PROJECTION = BASE_NAME + ".LONGLAT_PROJECTION";

	/** Name of the {@link Projection} WGS84, MERCATOR */
	public static final String MERCATOR_PROJECTION = BASE_NAME + ".MERCATOR_PROJECTION";

	/** Name of the current/active {@link PreferenceComposite} representing the editor's preferences */
	public static final String NAVSHIFT_PREFERENCE = BASE_NAME + ".NAVSHIFT_PREFERENCE";

	/** Name of the current/active {@link IsobathParametersFactory} */
	public static final String ISOBATH_PARAM_FACTORY = BASE_NAME + ".ISOBATH_PARAM_FACTORY";

	/** Name of the current/active {@link ShiftPeriodController} */
	public static final String SHIFTPERIOD_CONTROLLER = BASE_NAME + ".SHIFTPERIOD_CONTROLLER";

	/** Name of the current/active {@link IsobathController} */
	public static final String ISOBATH_CONTROLLER = BASE_NAME + ".ISOBATH_CONTROLLER";

	/** Name of the current/active {@link PreviewController} */
	public static final String PREVIEW_CONTROLLER = BASE_NAME + ".PREVIEW_CONTROLLER";

	/** Name of the current/active {@link ProfileController} */
	public static final String PROFILE_CONTROLLER = BASE_NAME + ".PROFILE_CONTROLLER";

	/** Name of the current/active {@link NavigationModelController} */
	public static final String NAVIGATION_MODEL_CONTROLLER = BASE_NAME + ".NAVIGATION_MODEL_CONTROLLER";

	/** Name of the current/active {@link Viewer3dListener} */
	public static final String VIEWER3D_LISTENER = BASE_NAME + ".VIEWER3D_LISTENER";

	/** Name of the current/active {@link Viewer2DLayerController} */
	public static final String VIEWER_2D_LAYER_CONTROLLER = BASE_NAME + ".VIEWER2D_LAYER_CONTROLLER";

	/** Name of the current/active {@link NavigationLayerController} */
	public static final String NAVIGATION_LAYER_CONTROLLER = BASE_NAME + ".NAVIGATION_LAYER_CONTROLLER";

	/** Name of the current/active {@link ShiftedNavigtionRefresher} */
	public static final String SHIFTED_NAVIGTION_REFRESHER = BASE_NAME + ".SHIFTED_NAVIGTION_REFRESHER";

	/** Name of the current file pointing on the temporary folder */
	public static final String TMP_FOLDER = BASE_NAME + ".TMP_FOLDER";

	/**
	 * Constructor
	 */
	private ContextNames() {
		// private constructor to hide the implicit public one.
	}
}
