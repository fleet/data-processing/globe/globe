/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.handlers;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.model.IShiftVector;
import fr.ifremer.globe.ui.handler.CommandUtils;
import fr.ifremer.globe.utils.exception.GException;

/**
 * Command launcher.
 */
public class CommandExecutor {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(CommandExecutor.class);

	/** ID of the export to nvi command */
	public static final String EXPORT_COMMAND = "fr.ifremer.globe.editor.navshift.command.export";
	/** ID of the parameter of the export to nvi command */
	public static final String EXPORT_PARAM_NVI_TYPE = "fr.ifremer.globe.editor.navshift.commandparameter.nviType";

	/** ID of the preview command */
	public static final String PREVIEW_COMMAND = "fr.ifremer.globe.editor.navshift.previewcommand";

	/** ID of the close command */
	public static final String CLOSE_COMMAND = "fr.ifremer.globe.editor.navshift.command.closenavigationshifteditorcommand";

	/** ID of the deletevector command */
	public static final String DELETE_VECTOR_COMMAND = "fr.ifremer.globe.editor.navshift.command.deletevectorcommand";

	/** ID of the save command */
	public static final String SAVE_COMMAND = "fr.ifremer.globe.save";

	/** ID of the save command */
	public static final String APPLY_TO_SOUNDER_COMMAND = "fr.ifremer.globe.editor.navshift.command.applytosounder";

	/** Constructor */
	private CommandExecutor() {
	}

	/**
	 * Executes the specified command for the vector
	 */
	public static void executeCommand(String commandId, IShiftVector selectedVector) {
		try {
			if (selectedVector != null) {
				IEclipseContext cmdContext = ContextInitializer.getEclipseContext().createChild();
				cmdContext.set("selectedVector", selectedVector);
				cmdContext.set(IShiftVector.class, selectedVector);
				CommandUtils.executeCommand(cmdContext, commandId);
			}
		} catch (GException e) {
			logger.debug("Unable to execute command {} : {}", commandId, e.getMessage());
		}
	}

	/**
	 * Executes the specified command
	 */
	public static void executeCommand(String commandId) {
		try {
			CommandUtils.executeCommand(ContextInitializer.getEclipseContext(), commandId);
		} catch (GException e) {
			logger.debug("Unable to execute command {} : {}", commandId, e.getMessage());
		}
	}

}
