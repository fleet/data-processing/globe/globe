/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.handlers;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.controller.SessionController;
import fr.ifremer.globe.ui.parts.PartUtil;

/**
 * Handler invoked to close the editor
 */
public class CloseHandler {

	protected boolean dirty = false;

	/**
	 * Execution required
	 */
	@Execute
	public void execute(@Named(ContextNames.SESSION_CONTROLLER) SessionController sessionController,
			MApplication application, EPartService partService) {
		sessionController.disposeSession();

		// Usefull to remove the cursor from Parameter View to allow the hide of its content
		PartUtil.activatePart(application, partService, "fr.ifremer.globe.ui.projectExplorer");
	}

	/**
	 * Event handler for TOPIC_IS_DIRTY
	 */
	@Inject
	@Optional
	protected void setDirty(@UIEventTopic(NavShiftEventTopics.TOPIC_IS_DIRTY) Boolean dirty) {
		this.dirty = dirty.booleanValue();
	}

}