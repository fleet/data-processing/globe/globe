/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.handlers;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.runtime.gws.GwsServiceAgent;
import fr.ifremer.globe.core.runtime.gws.param.ExportToNviParams;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.commons.Constants;
import fr.ifremer.globe.editor.navshift.model.INavigationDataUpdater;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.impl.NavigationSupplierWrapper;
import fr.ifremer.globe.editor.navshift.process.Previewer;
import fr.ifremer.globe.editor.navshift.process.ShiftedNavigtionRefresher;
import fr.ifremer.globe.editor.navshift.wizard.export.ExportWizardModel;
import fr.ifremer.globe.editor.navshift.wizard.export.SaveShiftedNavigationWizard;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.wizard.OutputFileNameComputer;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Eclipse Handler invoked by user to export shifted navigation as nvi
 */
public class ExportHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExportHandler.class);

	/** currently used sessionContext */
	@Optional
	@Inject
	@Named(ContextNames.SESSION_CONTEXT)
	private ISessionContext sessionContext;

	/** currently used sessionContext */
	@Optional
	@Inject
	@Named(ContextNames.SHIFTED_NAVIGTION_REFRESHER)
	private ShiftedNavigtionRefresher shiftedNavigtionRefresher;

	/** Writer of NVI files. */
	@Inject
	private INviWriter nviWriter;
	@Inject
	private IProcessService processService;
	@Inject
	private IFileLoadingService fileLoadingService;
	@Inject
	private ITreeNodeFactory treeNodeFactory;
	@Inject
	private GwsServiceAgent gwsServiceAgent;

	private java.util.Optional<Previewer> previewer;

	/**
	 * Event handler for preview.
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	private void onPreview(@UIEventTopic(NavShiftEventTopics.PREVIEW) java.util.Optional<Previewer> previewer) {
		this.previewer = previewer;
	}

	/**
	 *
	 * When asked (button pushed), applies Navigation Shift algorithm to active Shift Period defined in
	 * {@link ISessionContext} and export/save the result to disk
	 */
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell,
			@Named(CommandExecutor.EXPORT_PARAM_NVI_TYPE) String nviType) {
		if (!previewer.isPresent()) {
			return;
		}
		SaveShiftedNavigationWizard saveShiftedNavWizard = ContextInitializer.make(SaveShiftedNavigationWizard.class);
		if (nviType.equals(ContentType.NVI_V2_NETCDF_4.name()))
			saveShiftedNavWizard.getExportWizardModel().getOutputFormatExtensions().set(0, "nvi.nc");

		saveShiftedNavWizard.setWindowTitle("Save Shifted Navigation");
		saveShiftedNavWizard.getExportWizardModel().getInputFiles().addAll(previewer.get().getShiftedSounderFiles());
		saveShiftedNavWizard.getExportWizardModel().getOutputDirectory()
				.doSetValue(new File(sessionContext.getSounderNcInfos().get(0).getParent()));

		WizardDialog wizardDialog = new WizardDialog(shell, saveShiftedNavWizard);
		wizardDialog.setMinimumPageSize(300, 510);

		if (wizardDialog.open() == Window.OK) {
			if (nviType.equals(ContentType.NVI_V2_NETCDF_4.name())) {
				if (gwsServiceAgent != null)
					exportNviV2(saveShiftedNavWizard.getExportWizardModel());
			} else
				exportNviLegacy(saveShiftedNavWizard.getExportWizardModel());
		}
	}

	/** Export model in NVI Legacy files */
	private void exportNviLegacy(ExportWizardModel saveShiftedNavigationWizardModel) {
		processService.runInForeground("Export to NVI", (monitor, logger) -> {
			INavigationDataUpdater shiftedNavigtion = shiftedNavigtionRefresher
					.computeNavshift(SubMonitor.convert(monitor, "Export to NVI in progress", 100));

			if (hasToMergeProfiles(saveShiftedNavigationWizardModel)) {
				saveSingleFile(shiftedNavigtion, saveShiftedNavigationWizardModel);
			} else {
				saveMultiFiles(shiftedNavigtion, saveShiftedNavigationWizardModel);
			}
			return Status.OK_STATUS;
		});
	}

	/** Export to NVI V2 format with GWS */
	private void exportNviV2(ExportWizardModel model) {
		processService.runInBackground("Export to NVI", (monitor, logger) -> {
			INavigationDataUpdater shiftedNavigtion = shiftedNavigtionRefresher
					.computeNavshift(SubMonitor.convert(monitor, "Export to NVI in progress", 100));
			try {
				List<INavigationData> allNavigationData = List.of(new NavigationSupplierWrapper(shiftedNavigtion));
				int[] navPointsInFiles = null;
				if (!hasToMergeProfiles(model)) {
					var profiles = sessionContext.getProfiles();
					navPointsInFiles = new int[profiles.size()];
					for (int i = 0; i < navPointsInFiles.length; i++) {
						IProfile profile = profiles.get(i);
						navPointsInFiles[i] = profile.getLastIndex() - profile.getFirstIndex() + 1;
					}
				}

				gwsServiceAgent.exportToNvi(//
						new ExportToNviParams(//
								model.getInputFiles().asList(), // inputFiles
								java.util.Optional.of(allNavigationData), // navigationData
								java.util.Optional.ofNullable(navPointsInFiles), // navPointsInFiles, empty to
																					// auto-generates it
								java.util.Optional.of(model.getOutputFiles().asList()), // ouputFiles
								java.util.Optional.of(model.getOverwriteExistingFiles().doGetValue()), // overwrite
								java.util.Optional.ofNullable(model.getWhereToloadFiles().isEmpty() ? null
										: model.getWhereToloadFiles().doGetValue()) // whereToLoadOutputFiles
				), monitor, logger);

			} catch (GException e) {
				logger.warn("Error when executing process", e);
			}
			return Status.OK_STATUS;
		}, true);
	}

	/** Save NVI Legacy as a single files */
	private void saveSingleFile(INavigationDataUpdater shiftedNavigtion,
			ExportWizardModel saveShiftedNavigationWizardModel) {
		try {
			File outputFile = saveShiftedNavigationWizardModel.getOutputFiles().asList().get(0);
			NavigationSupplierWrapper proxy = new NavigationSupplierWrapper(shiftedNavigtion);
			nviWriter.write(proxy, outputFile.getAbsolutePath());

			loadOutputFile(saveShiftedNavigationWizardModel, outputFile);
		} catch (GIOException e) {
			Messages.openErrorMessage("Unable to save NAvigation", e.getMessage());
		}
	}

	/** Save NVI Legacy as a single files */
	private void saveMultiFiles(INavigationDataUpdater shiftedNavigtion,
			ExportWizardModel saveShiftedNavigationWizardModel) {
		NavigationSupplierWrapper proxy = new NavigationSupplierWrapper(shiftedNavigtion);
		OutputFileNameComputer outputFileNameComputer = new OutputFileNameComputer();

		List<String> outputFilenames = new ArrayList<>();
		for (File f : saveShiftedNavigationWizardModel.getOutputFiles().asList()) {
			outputFilenames.add(f.getAbsolutePath());
		}

		try {
			for (IProfile p : sessionContext.getProfiles()) {
				File outputFile = outputFileNameComputer.compute(new File(p.getSounderNcInfo().getPath()),
						saveShiftedNavigationWizardModel, Constants.NVI);

				if (!outputFilenames.contains(outputFile.getAbsolutePath())) {
					continue;
				}

				List<Integer> indexesToExport = IntStream.range(p.getFirstIndex(), p.getLastIndex() + 1).boxed()
						.toList();
				nviWriter.write(proxy, outputFile.getAbsolutePath(), indexesToExport);

				loadOutputFile(saveShiftedNavigationWizardModel, outputFile);
			}
		} catch (GIOException e) {
			Messages.openErrorMessage("Unable to save NAvigation", e.getMessage());
		}
	}

	/** Return true if user wants to merge the nvi files */
	private boolean hasToMergeProfiles(ExportWizardModel saveShiftedNavigationWizardModel) {
		return saveShiftedNavigationWizardModel.canMergeOutputFiles()
				|| (saveShiftedNavigationWizardModel.getSingleOutputFilename().isNotNull()
						&& !saveShiftedNavigationWizardModel.getSingleOutputFilename().isEmpty());
	}

	/**
	 * Loads output file.
	 */
	private void loadOutputFile(ExportWizardModel saveShiftedNavigationWizardModel, File outputFile) {
		Display.getDefault().asyncExec(() -> {
			if (saveShiftedNavigationWizardModel.getLoadFilesAfter().getValue().booleanValue()) {
				treeNodeFactory.createFileInfoNode(new BasicFileInfo(outputFile));
				fileLoadingService.loadFiles(Collections.singletonList(outputFile), false);
			}
		});
	}

}