/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.handlers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.process.Previewer;
import fr.ifremer.globe.editor.navshift.wizard.export.ExportToSounderWizard;
import fr.ifremer.globe.editor.navshift.wizard.export.ExportWizardModel;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.wizard.OutputFileNameComputer;
import fr.ifremer.globe.utils.FileUtils;

/**
 * Eclipse Handler invoked by user to apply shifted navigation on Sounder files
 */
public class ApplyToSounderHandler {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(ApplyToSounderHandler.class);

	/** currently used sessionContext */
	@Inject
	@Optional
	@Named(ContextNames.SESSION_CONTEXT)
	protected ISessionContext sessionContext;

	/** File services */
	@Inject
	protected IFileService fileService;
	@Inject
	protected IFileLoadingService fileLoadingService;
	@Inject
	protected ITreeNodeFactory treeNodeFactory;
	@Inject
	@Optional
	protected IEventBroker eventBroker;

	/** Current active Previewer if any */
	protected java.util.Optional<Previewer> previewer = java.util.Optional.empty();

	/** Run... */
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		if (previewer.isPresent()) {
			List<File> sounderFiles = previewer.get().getShiftedSounderFiles();
			if (sounderFiles != null && !sounderFiles.isEmpty()) {
				shell.getDisplay().asyncExec(() -> {
					openWizard(shell, sounderFiles);
				});
			}
		}
	}

	/** Open the wizard to select the output files */
	protected void openWizard(Shell shell, List<File> previewedFiles) {
		ExportToSounderWizard exportToSounderWizard = ContextInitializer.make(ExportToSounderWizard.class);
		exportToSounderWizard.setWindowTitle("Apply to input files...");

		ExportWizardModel wizardModel = exportToSounderWizard.getExportWizardModel();
		wizardModel.getInputFiles().addAll(previewedFiles);
		wizardModel.getOutputDirectory().doSetValue(new File(sessionContext.getSounderNcInfos().get(0).getParent()));

		WizardDialog wizardDialog = new WizardDialog(shell, exportToSounderWizard);
		wizardDialog.setMinimumPageSize(300, 510);

		if (wizardDialog.open() == Window.OK) {
			saveFiles(shell, previewedFiles, wizardModel);
		}
	}

	/** Save previewed files according the wizard model */
	protected void saveFiles(Shell shell, List<File> previewedFiles, ExportWizardModel wizardModel) {
		BusyIndicator.showWhile(shell.getDisplay(), () -> {
			OutputFileNameComputer outputFileNameComputer = new OutputFileNameComputer();

			try {
				var changedFiles = new ArrayList<File>();
				for (File sounderFile : previewedFiles) {
					WritableObjectList<String> outputFormatExtensions = wizardModel.getOutputFormatExtensions();
					// Same extension as input file ?
					String extension = outputFormatExtensions.isEmpty() ? FileUtils.getExtension(sounderFile)
							: outputFormatExtensions.get(0);
					File outputFile = outputFileNameComputer.compute(sounderFile, wizardModel, extension);
					duplicateSounderFiles(sounderFile, outputFile, wizardModel.getOverwriteExistingFiles().isTrue());
					changedFiles.add(outputFile);
					if (wizardModel.getLoadFilesAfter().getValue().booleanValue()) {
						treeNodeFactory.createFileInfoNode(new BasicFileInfo(outputFile));
						fileLoadingService.load(outputFile);
					}
				}
				// Notifying files has changed
				eventBroker.send(NavShiftEventTopics.TOPIC_FILES_SHIFTED, changedFiles);
			} catch (Exception e) {
				Messages.openErrorMessage("Unable to apply the navigation", e.getMessage());
			}
		});
	}

	/**
	 * Copy all files in a temporary folder.
	 */
	protected void duplicateSounderFiles(File inputFile, File outputFile, boolean overwrite) throws IOException {
		if (overwrite || !outputFile.exists()) {
			logger.info("Copy file {} to {}", inputFile.getAbsolutePath(), outputFile.getAbsolutePath());
			outputFile.getParentFile().mkdirs();
			Files.copy(inputFile.toPath(), outputFile.toPath(), StandardCopyOption.COPY_ATTRIBUTES,
					StandardCopyOption.REPLACE_EXISTING);
		}
	}

	/**
	 * Event handler for preview.
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	protected void onPreview(@UIEventTopic(NavShiftEventTopics.PREVIEW) java.util.Optional<Previewer> previewer) {
		this.previewer = previewer;
	}

}