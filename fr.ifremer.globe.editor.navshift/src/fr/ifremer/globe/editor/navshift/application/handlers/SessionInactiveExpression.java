/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.editor.navshift.application.handlers;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Evaluate;

import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.controller.SessionController;

/**
 * Expression class used to deactivate menus when a session is active
 */
public class SessionInactiveExpression {

	@Inject
	@Named(ContextNames.SESSION_CONTROLLER)
	SessionController sessionController;

	@Evaluate
	public boolean evaluate() {
		return !(sessionController != null && sessionController.getSessionContext() != null);
	}
}
