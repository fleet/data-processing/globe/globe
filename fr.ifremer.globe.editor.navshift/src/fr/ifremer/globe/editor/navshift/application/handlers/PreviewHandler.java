/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.handlers;

import java.io.IOException;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;

import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.application.event.NavShiftEventTopics;
import fr.ifremer.globe.editor.navshift.controller.SessionController;
import fr.ifremer.globe.editor.navshift.process.Previewer;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Handler invoked to preview the navigation shift computation
 */
public class PreviewHandler {

	/** Service to launch processes */
	@Inject
	protected IProcessService processService;

	/** Event broker service */
	@Inject
	protected IEventBroker eventBroker;

	/** Processing class of preview */
	@Inject
	protected Previewer previewer;

	/**
	 * Execution required
	 */
	@Execute
	public void execute(@Named(ContextNames.SESSION_CONTROLLER) SessionController sessionController) {
		processService.runInForeground("Preview", (monitor, logger) -> {
			try {
				execute(monitor);
			} catch (IOException e) {
				previewer.dispose();
				throw GIOException.wrap("Error while preparing preview\n\n" + e.getMessage(), e);
			} catch (GException e) {
				previewer.dispose();
				throw e;
			} catch (OperationCanceledException e) {
				previewer.dispose();
				return Status.CANCEL_STATUS;
			}
			return Status.OK_STATUS;
		});
	}

	public void execute(IProgressMonitor monitor) throws IOException, GException {
		previewer.process(monitor);
		eventBroker.send(NavShiftEventTopics.PREVIEW, Optional.of(previewer));
	}

	/**
	 * Event handler for preview.
	 */
	@Inject
	@org.eclipse.e4.core.di.annotations.Optional
	protected void onPreview(@UIEventTopic(NavShiftEventTopics.PREVIEW) java.util.Optional<Previewer> option) {
		if (!option.isPresent()) {
			// Preview closed
			previewer.dispose();
		}
	}

}