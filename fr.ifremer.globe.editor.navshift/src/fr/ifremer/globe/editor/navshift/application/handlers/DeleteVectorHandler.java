/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.IShiftVector;

/**
 * Eclipse Handler invoked by user to delete the selected vector in the Navigation Shift Editor
 */
public class DeleteVectorHandler {
	/**
	 * Execution required
	 */
	@Execute
	public void execute(@Optional @Named(ContextNames.SESSION_CONTEXT) ISessionContext sessionContext,
			@Named(IServiceConstants.ACTIVE_SHELL) Shell shell,
			@Optional @Named("selectedVector") IShiftVector selectedVector) {

		if (sessionContext != null && selectedVector != null) {
			IShiftPeriod period = sessionContext.getShiftPeriod();
			period.removeShiftVector(selectedVector);
		}
	}

	/** Determines if this Handler's {@link Execute} method can be called */
	@CanExecute
	public boolean canExecute() {
		return true;
	}

}