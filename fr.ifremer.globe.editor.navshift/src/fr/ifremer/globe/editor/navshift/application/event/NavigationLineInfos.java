/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.event;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;

/**
 * Navgation line informations used to display it in the viewer 3Dr
 */
public class NavigationLineInfos extends StyleInfos {

	/** Navigation model */
	protected INavigationModel navigationDataSupplier;

	/**
	 * Constructor
	 */
	public NavigationLineInfos(GColor color, float pointSize) {
		super(color, pointSize);
	}

	/**
	 * Getter of {@link #navigationDataSupplier}
	 */
	public INavigationModel getNavigationDataSupplier() {
		return navigationDataSupplier;
	}

	/**
	 * Setter of {@link #navigationDataSupplier}
	 */
	public void setNavigationDataSupplier(INavigationModel navigationDataSupplier) {
		this.navigationDataSupplier = navigationDataSupplier;
	}

}
