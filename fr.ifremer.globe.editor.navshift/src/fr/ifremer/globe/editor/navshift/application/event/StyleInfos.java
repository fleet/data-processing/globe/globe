/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.event;

import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Style informations like color and point size
 */
public class StyleInfos {

	/** Highlighting color */
	protected GColor color;
	/** Highlighting point size */
	protected float pointSize;

	/**
	 * Constructor
	 */
	public StyleInfos(GColor color, float pointSize) {
		super();
		this.color = color;
		this.pointSize = pointSize;
	}

	/**
	 * Getter of {@link #color}
	 */
	public GColor getColor() {
		return color;
	}

	/**
	 * Getter of {@link #pointSize}
	 */
	public float getPointSize() {
		return pointSize;
	}

}