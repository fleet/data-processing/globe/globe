/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.event;

import org.apache.commons.lang.math.IntRange;

import fr.ifremer.globe.core.utils.color.GColor;

/**
 * Points informations used to highlight them in the navigation layer
 */
public class HighlightPointInfos extends StyleInfos {

	/** Index of the point in the navigation */
	protected IntRange indexes;

	/**
	 * Constructor
	 */
	public HighlightPointInfos(int index, GColor color, float pointSize) {
		super(color, pointSize);
		this.indexes = new IntRange(index);
	}

	/**
	 * Constructor
	 */
	public HighlightPointInfos(IntRange indexes, GColor color, float pointSize) {
		super(color, pointSize);
		this.indexes = indexes;
	}

	/**
	 * Getter of {@link #indexes}
	 */
	public IntRange getIndexes() {
		return indexes;
	}
}
