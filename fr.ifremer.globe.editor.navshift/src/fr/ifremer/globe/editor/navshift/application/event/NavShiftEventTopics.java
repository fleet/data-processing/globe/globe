/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.event;

import org.eclipse.e4.ui.workbench.UIEvents;

/**
 * All topics managed in the navigation shift editor throw the IEventBroker
 */
public class NavShiftEventTopics {

	/** Root label of all topics that can be subscribed to */
	public static final String TOPIC_BASE = "fr/ifremer/globe/editor/navshift";

	/** Used to register for changes on all the attributes */
	public static final String TOPIC_ALL = TOPIC_BASE + UIEvents.TOPIC_SEP + UIEvents.ALL_SUB_TOPICS;

	/**
	 * Topic to inform that the dirty flag has have changed<br>
	 * Event poster place on the event an instance of Boolean
	 */
	public static final String TOPIC_IS_DIRTY = TOPIC_BASE + UIEvents.TOPIC_SEP + "EDITOR_IS_DIRTY";

	/**
	 * Topic to inform every controller that a save action is needed<br>
	 * Event poster place on the event an instance of IProgressMonitor
	 */
	public static final String TOPIC_SAVE = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_SAVE";

	/**
	 * Topic to inform that the shiftperiod selection has started<br>
	 * Event poster place on the event an instance of itself
	 */
	public static final String TOPIC_SHIFTPERIOD_SELECTION_START = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "TOPIC_SHIFTPERIOD_SELECTION_START";
	/**
	 * Topic to inform that the shiftperiod selection has stopped<br>
	 * Event poster place on the event an instance of itself
	 */
	public static final String TOPIC_SHIFTPERIOD_SELECTION_STOP = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "TOPIC_SHIFTPERIOD_SELECTION_STOP";

	/**
	 * Topic to inform that the a set of shift vectors have been designated<br>
	 * Event poster place on the event a list of IShiftPeriod
	 */
	public static final String TOPIC_VECTORS_SELECTED = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_VECTORS_SELECTED";

	/**
	 * Topic to inform that the shift vector has been edited <br>
	 * Event poster place on the event an instance of IShiftVector
	 */
	public static final String TOPIC_VECTOR_EDITED = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_VECTOR_EDITED";

	/**
	 * Topic to inform that the shift vector is about to be designated<br>
	 * Event poster place on the event an instance of IShiftPeriod
	 */
	public static final String TOPIC_HIGHLIGHT_FOR_SELECTING_VECTOR = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "TOPIC_HIGHLIGHT_FOR_SELECTING_VECTOR";

	/**
	 * Topic to inform that the shift vector is about to be modified<br>
	 * Event poster place on the event an instance of IShiftPeriod
	 */
	public static final String TOPIC_HIGHLIGHT_FOR_MODIFYING_VECTOR = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "TOPIC_HIGHLIGHT_FOR_MODIFYING_VECTOR";

	/**
	 * Topic to inform that the vetors creation has started<br>
	 * Event poster place on the event an instance of itself
	 */
	public static final String TOPIC_VECTOR_CREATION_START = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "TOPIC_VECTOR_CREATION_START";
	/**
	 * Topic to inform that the vetors creation has stopped<br>
	 * Event poster place on the event an instance of itself
	 */
	public static final String TOPIC_VECTOR_CREATION_STOP = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "TOPIC_VECTOR_CREATION_STOP";

	/**
	 * Topic to request the highlighting of a point<br>
	 * Event poster place on the event an instance of HighlightPointInfos
	 */
	public static final String TOPIC_HIGHLIGHT_POINT = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_HIGHLIGHT_POINT";

	/**
	 * Topic to request the unhighlighting of the previous highlighted point<br>
	 * Event poster place on the event an instance of itself
	 */
	public static final String TOPIC_UNHIGHLIGHT_POINT = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_UNHIGHLIGHT_POINT";

	/**
	 * Topic to request the highlighting of some points<br>
	 * Event poster place on the event an instance of HighlightPointInfos
	 */
	public static final String TOPIC_HIGHLIGHT_POINTS = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_HIGHLIGHT_POINTS";

	/**
	 * Topic to request the unhighlighting of all points<br>
	 * Event poster place on the event an instance of itself
	 */
	public static final String TOPIC_UNHIGHLIGHT_POINTS = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_UNHIGHLIGHT_POINTS";

	/**
	 * Topic to request the drawing of a vector<br>
	 * Event poster place on the event an instance of {@link VectorInfos}
	 */
	public static final String TOPIC_DRAW_VECTOR = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_DRAW_VECTOR";

	/**
	 * Topic to request the deletion of the current selected vector<br>
	 * Event poster place on the event an instance of itself.
	 */
	public static final String TOPIC_DELETE_VECTOR = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_DELETE_VECTOR";

	/**
	 * Topic to request the drawing of a navigation line<br>
	 * Event poster place on the event an instance of {@link NavigationLineInfos}
	 */
	public static final String TOPIC_DRAW_EXTRA_NAVIGATION_LINE = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "TOPIC_DRAW_EXTRA_NAVIGATION_LINE";

	/**
	 * Topic to advertize that a shifted navigation has to be refreshed
	 */
	public static final String TOPIC_COMPUTE_SHIFT_REQUIRED = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "TOPIC_COMPUTE_SHIFT_REQUIRED";

	/**
	 * Topic to advertize that a shift period can be computed<br>
	 * Event poster place on the event an instance of Boolean
	 */
	public static final String TOPIC_COMPUTE_SHIFT_ENABLED = TOPIC_BASE + UIEvents.TOPIC_SEP
			+ "TOPIC_COMPUTE_SHIFT_ENABLED";

	/**
	 * * Topic to advertize that a the session has been activated successfully<br>
	 * Event poster place on the event an instance of activated ISessionContext
	 */
	public static final String SESSION_ACTIVATED = TOPIC_BASE + UIEvents.TOPIC_SEP + "SESSION_ACTIVATED";

	/**
	 * * Topic to advertize that we enter/exit the preview mode<br>
	 * Event poster place on the event an instance of Optional<Previewer>.
	 */
	public static final String PREVIEW = TOPIC_BASE + UIEvents.TOPIC_SEP + "PREVIEW";

	/**
	 * Topic to inform that the shift has been apply to some files<br>
	 * Event poster place on the event an instance of list of Files
	 */
	public static final String TOPIC_FILES_SHIFTED = TOPIC_BASE + UIEvents.TOPIC_SEP + "TOPIC_FILES_SHIFTED";

	/**
	 * Constructor
	 */
	private NavShiftEventTopics() {
		// private constructor to hide the implicit public one.
	}
}
