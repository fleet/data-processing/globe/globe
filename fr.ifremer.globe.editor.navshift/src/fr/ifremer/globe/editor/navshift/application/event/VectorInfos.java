/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application.event;

import fr.ifremer.globe.core.utils.color.GColor;
import gov.nasa.worldwind.geom.Vec4;

/**
 * Vector informations used to display it in the shiftPeriod layer
 */
public class VectorInfos extends StyleInfos {

	/** Index in the navigation model of the starting point */
	protected int startPointIndex;
	/** Coods XY of the end point */
	protected Vec4 stopPoint;

	/**
	 * Constructor
	 */
	public VectorInfos(GColor color, float pointSize) {
		super(color, pointSize);
	}

	/**
	 * Getter of {@link #startPointIndex}
	 */
	public int getStartPositionIndex() {
		return startPointIndex;
	}

	/**
	 * Setter of {@link #startPointIndex}
	 */
	public void setStartPositionIndex(int startPositionIndex) {
		this.startPointIndex = startPositionIndex;
	}

	/**
	 * Getter of {@link #stopPoint}
	 */
	public Vec4 getStopPoint() {
		return stopPoint;
	}

	/**
	 * Setter of {@link #stopPoint}
	 */
	public void setStopPoint(Vec4 stopPosition) {
		this.stopPoint = stopPosition;
	}

}
