package fr.ifremer.globe.editor.navshift.commons;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.viewer3d.layers.lineisobaths.parameters.IsobathParameters;

/**
 * Factory used to build {@link IsobathParameters}.
 */
public class IsobathParametersFactory {

	/** Preferences */
	@Inject
	@Named(ContextNames.NAVSHIFT_PREFERENCE)
	protected NavShiftPreference preferences;

	/**
	 * @return normal isobath parameters
	 */
	public IsobathParameters get() {
		IsobathParameters isobathParameters = new IsobathParameters();
		isobathParameters.getMainLine().set(preferences.getIsobathMasterLine().getValue());
		isobathParameters.getStep().set(preferences.getIsobathStep().getValue());
		isobathParameters.getSampling().set(preferences.getIsobathSampling().getValue());
		isobathParameters.getMainLineColor().set(preferences.getIsobathMasterLineColor().getValue());
		isobathParameters.getMainLineVisible().set(preferences.getIsobathMasterLineVisible().getValue());
		isobathParameters.getOtherLineUnicolor().set(preferences.getIsobathOtherLineColor().getValue());
		isobathParameters.getOtherLineVisible().set(preferences.getIsobathOtherLineVisible().getValue());
		isobathParameters.getLineWidth().set(preferences.getIsobathLineSize().getValue());
		return isobathParameters;
	}

	/**
	 * @return normal isobath parameters synchronized with preferences
	 */
	public IsobathParameters getSynch() {
		IsobathParameters isobathParameters = new IsobathParameters();

		// Also sync with preview : preview must generate isobaths with the same parameters than profile
		isobathParameters.syncMainLine(preferences.getIsobathMasterLine(), preferences.getPreviewMasterLine());
		isobathParameters.syncStep(preferences.getIsobathStep(), preferences.getPreviewStep());
		isobathParameters.syncSampling(preferences.getIsobathSampling(), preferences.getPreviewSampling());
		isobathParameters.syncMainLineColor(preferences.getIsobathMasterLineColor());
		isobathParameters.syncMainLineVisible(preferences.getIsobathMasterLineVisible());
		isobathParameters.syncOtherLineColor(preferences.getIsobathOtherLineColor());
		isobathParameters.syncOtherLineVisible(preferences.getIsobathOtherLineVisible());
		isobathParameters.syncLineWidth(preferences.getIsobathLineSize());
		return isobathParameters;
	}

	/**
	 * @return reference isobath parameters
	 */
	public IsobathParameters getReference() {
		IsobathParameters isobathParameters = new IsobathParameters();
		isobathParameters.getMainLine().set(preferences.getReferenceIsobathMasterLine().getValue());
		isobathParameters.getStep().set(preferences.getReferenceIsobathStep().getValue());
		isobathParameters.getSampling().set(preferences.getReferenceIsobathSampling().getValue());
		isobathParameters.getMainLineColor().set(preferences.getReferenceIsobathMasterLineColor().getValue());
		isobathParameters.getMainLineVisible().set(preferences.getReferenceIsobathMasterLineVisible().getValue());
		isobathParameters.getOtherLineUnicolor().set(preferences.getReferenceIsobathOtherLineColor().getValue());
		isobathParameters.getOtherLineVisible().set(preferences.getReferenceIsobathOtherLineVisible().getValue());
		isobathParameters.getLineWidth().set(preferences.getReferenceIsobathLineSize().getValue());
		return isobathParameters;
	}

	/**
	 * @return reference isobath parameters synchronized with preferences
	 */
	public IsobathParameters getReferenceSynch() {
		IsobathParameters isobathParameters = new IsobathParameters();
		isobathParameters.syncMainLine(preferences.getReferenceIsobathMasterLine());
		isobathParameters.syncStep(preferences.getReferenceIsobathStep());
		isobathParameters.syncSampling(preferences.getReferenceIsobathSampling());
		isobathParameters.syncMainLineColor(preferences.getReferenceIsobathMasterLineColor());
		isobathParameters.syncMainLineVisible(preferences.getReferenceIsobathMasterLineVisible());
		isobathParameters.syncOtherLineColor(preferences.getReferenceIsobathOtherLineColor());
		isobathParameters.syncOtherLineVisible(preferences.getReferenceIsobathOtherLineVisible());
		isobathParameters.syncLineWidth(preferences.getReferenceIsobathLineSize());
		return isobathParameters;
	}

	/**
	 * @return preview isobath parameters
	 */
	public IsobathParameters getPreview() {
		IsobathParameters isobathParameters = new IsobathParameters();
		isobathParameters.getMainLine().set(preferences.getPreviewMasterLine().getValue());
		isobathParameters.getStep().set(preferences.getPreviewStep().getValue());
		isobathParameters.getSampling().set(preferences.getPreviewSampling().getValue());
		isobathParameters.getMainLineColor().set(preferences.getPreviewMasterLineColor().getValue());
		isobathParameters.getMainLineVisible().set(preferences.getPreviewMasterLineVisible().getValue());
		isobathParameters.getOtherLineUnicolor().set(preferences.getPreviewOtherLineColor().getValue());
		isobathParameters.getOtherLineVisible().set(preferences.getPreviewOtherLineVisible().getValue());
		isobathParameters.getLineWidth().set(preferences.getPreviewLineSize().getValue());
		return isobathParameters;
	}

	/**
	 * @return preview isobath parameters synchronized with preferences
	 */
	public IsobathParameters getPreviewSynch() {
		IsobathParameters isobathParameters = new IsobathParameters();
		isobathParameters.syncMainLine(preferences.getPreviewMasterLine());
		isobathParameters.syncStep(preferences.getPreviewStep());
		isobathParameters.syncSampling(preferences.getPreviewSampling());
		isobathParameters.syncMainLineColor(preferences.getPreviewMasterLineColor());
		isobathParameters.syncMainLineVisible(preferences.getPreviewMasterLineVisible());
		isobathParameters.syncOtherLineColor(preferences.getPreviewOtherLineColor());
		isobathParameters.syncOtherLineVisible(preferences.getPreviewOtherLineVisible());
		isobathParameters.syncLineWidth(preferences.getPreviewLineSize());
		return isobathParameters;
	}

}
