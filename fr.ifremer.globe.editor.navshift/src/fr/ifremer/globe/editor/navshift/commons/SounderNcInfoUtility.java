/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.commons;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.core.model.dtm.SounderDataDtmDepthRangeComputer;
import fr.ifremer.globe.core.model.dtm.SounderDataDtmGeoboxComputer;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Utility class to load sounder files
 */
public class SounderNcInfoUtility {

	/**
	 * This class is not meant to be instantiated.
	 */
	private SounderNcInfoUtility() {
	}

	/**
	 * Load all files as ISounderNcInfo and compute data needed for displaying navigation & DTM layers
	 */
	public static void load(List<File> inputFiles, Consumer<List<ISounderNcInfo>> sounderNcInfoConsumer,
			Consumer<GeoBox> geoBoxConsumer, Optional<Consumer<DoubleRange>> depthConsumer) throws GIOException {
		List<ISounderNcInfo> sounderNcInfos = new ArrayList<>();
		for (File inputFile : inputFiles) {
			IFileInfo fileInfo = IFileService.grab().getFileInfo(inputFile.getAbsolutePath()).orElse(null);
			if (fileInfo instanceof ISounderNcInfo) {
				ISounderNcInfo sounderNcInfo = (ISounderNcInfo) fileInfo;
				sounderNcInfos.add(sounderNcInfo);
			} else if (fileInfo == null || !fileInfo.getContentType().isOneOf(//
					ContentType.DTM_NETCDF_3, //
					ContentType.DTM_NETCDF_4, //
					ContentType.RASTER_GDAL //
			)) {
				throw new GIOException("File " + inputFile.getName() + " is not supported by Globe");
			}
		}
		sounderNcInfoConsumer.accept(sounderNcInfos);
		processNCInfos(sounderNcInfos, geoBoxConsumer, depthConsumer);
	}

	/**
	 * process NCInfos to provide data needed to display navigation & DTM layers, including GeoBox
	 * 
	 * @throws GIOException
	 */

	public static void processNCInfos(List<ISounderNcInfo> sounderNcInfos, Consumer<GeoBox> geoBoxConsumer,
			Optional<Consumer<DoubleRange>> depthConsumer) throws GIOException {

		SounderDataDtmGeoboxComputer geoboxComputer = new SounderDataDtmGeoboxComputer(sounderNcInfos);
		GeoBox geoBox = geoboxComputer.computeValidGeobox();
		geoBoxConsumer.accept(geoBox);

		if (depthConsumer.isPresent()) {
			SounderDataDtmDepthRangeComputer depthRangeComputer = new SounderDataDtmDepthRangeComputer(sounderNcInfos);
			DoubleRange depthRange = depthRangeComputer.computeValidDepthRange();
			depthConsumer.get().accept(depthRange);
		}
	}

	/**
	 * Copy the specified sounder file into the given folder
	 */
	public static File copy(ISounderNcInfo sounderNcInfo, File outFolder) throws IOException {
		File inFile = new File(sounderNcInfo.getPath());
		File result = new File(outFolder, inFile.getName());
		Files.copy(inFile.toPath(), result.toPath(), StandardCopyOption.REPLACE_EXISTING);
		return result;
	}

	/**
	 * Sort the list of ISounderNcInfo in chronological order
	 */
	public static List<ISounderNcInfo> chronologicalSort(List<ISounderNcInfo> inputSounderNcInfos) {
		List<ISounderNcInfo> sounderNcInfos = new ArrayList<>(inputSounderNcInfos);
		Collections.sort(sounderNcInfos, (sounderNcInfo1, ISounderNcInfo2) -> sounderNcInfo1.getFirstPingDate()
				.compareTo(ISounderNcInfo2.getFirstPingDate()));
		return sounderNcInfos;
	}
}
