/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.commons;

import java.awt.Color;
import java.nio.file.FileSystems;

import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.ColorPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.FileListPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.FilePreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.NumberPreferenceAttribute;
import fr.ifremer.globe.ui.utils.color.ColorUtils;

/**
 * All preferences used in this editor
 * 
 * TODO: refactor preference titles 
 */
public class NavShiftPreference extends PreferenceComposite {

	public static final String NAVSHIFT_PREF_NAME = "Navigation Shift editor";

	/** Preference attributes */
	protected FileListPreferenceAttribute wizardInputFiles;
	protected NumberPreferenceAttribute wizardDtmCellSize;
	protected FilePreferenceAttribute sessionFile;
	protected FilePreferenceAttribute outputMbgFolder;

	// Period
	protected ColorPreferenceAttribute periodColor;
	protected NumberPreferenceAttribute periodPointSize;
	protected ColorPreferenceAttribute highlightPeriodColor;
	protected NumberPreferenceAttribute highlightPeriodPointSize;

	// Vector
	protected ColorPreferenceAttribute vectorColor;
	protected NumberPreferenceAttribute vectorPointSize;
	protected ColorPreferenceAttribute highlightVectorColor;
	protected NumberPreferenceAttribute highlightVectorPointSize;
	protected ColorPreferenceAttribute selectedVectorColor;

	// Shifted nav
	protected ColorPreferenceAttribute shiftedNavigationColor;
	protected NumberPreferenceAttribute shiftedNavigationPointSize;

	// Isobaths
	protected NumberPreferenceAttribute isobathLineSize;
	protected NumberPreferenceAttribute isobathStep;
	protected NumberPreferenceAttribute isobathSampling;
	protected ColorPreferenceAttribute isobathMasterLineColor;
	protected BooleanPreferenceAttribute isobathMasterLineVisible;
	protected ColorPreferenceAttribute isobathOtherLineColor;
	protected BooleanPreferenceAttribute isobathOtherLineVisible;
	protected NumberPreferenceAttribute isobathMasterLine;

	// Reference Isobathss
	protected NumberPreferenceAttribute referenceIsobathLineSize;
	protected NumberPreferenceAttribute referenceIsobathStep;
	protected NumberPreferenceAttribute referenceIsobathSampling;
	protected ColorPreferenceAttribute referenceIsobathMasterLineColor;
	protected BooleanPreferenceAttribute referenceIsobathMasterLineVisible;
	protected ColorPreferenceAttribute referenceIsobathOtherLineColor;
	protected BooleanPreferenceAttribute referenceIsobathOtherLineVisible;
	protected NumberPreferenceAttribute referenceIsobathMasterLine;

	// Preview
	protected NumberPreferenceAttribute previewLineSize;
	protected NumberPreferenceAttribute previewStep;
	protected NumberPreferenceAttribute previewSampling;
	protected ColorPreferenceAttribute previewMasterLineColor;
	protected BooleanPreferenceAttribute previewMasterLineVisible;
	protected ColorPreferenceAttribute previewOtherLineColor;
	protected BooleanPreferenceAttribute previewOtherLineVisible;
	protected NumberPreferenceAttribute previewMasterLine;

	/**
	 * Constructor
	 */
	public NavShiftPreference(PreferenceComposite superNode) {
		super(superNode, NAVSHIFT_PREF_NAME);

		declareHiddenPreferences();
		declarePeriodPreferences();
		declareVectorPreferences();
		declareShiftedPreferences();
		declareIsobathsPreferences();
		declareReferenceIsobathsPreferences();
		declarePreviewPreferences();

		load();
	}

	protected void declarePreviewPreferences() {
		previewMasterLine = new NumberPreferenceAttribute("200_previewMasterLine", "Main line (every x lines)", 10);
		previewStep = new NumberPreferenceAttribute("200_previewStep", "Distance between two lines", 10.0d);
		previewLineSize = new NumberPreferenceAttribute("200_isolineSize", "Preview line size", 2);
		previewSampling = new NumberPreferenceAttribute("201_isoBathOffSampling", "Preview sampling", 60);
		previewMasterLineVisible = new BooleanPreferenceAttribute("201_previewMasterLineVisible", "Main line visible",
				true);
		previewMasterLineColor = new ColorPreferenceAttribute("202_previewMasterColor", "Isobath master line color",
				new GColor(255, 50, 50));
		previewOtherLineVisible = new BooleanPreferenceAttribute("201_previewOtherLineVisible", "Other line visible",
				true);
		previewOtherLineColor = new ColorPreferenceAttribute("203_previewColor", "Isobath line color",
				new GColor(255, 220, 220));

		PreferenceComposite previewsComposite = new PreferenceComposite(this, "Preview isobaths");
		previewsComposite.declareAttribute(previewStep);
		previewsComposite.declareAttribute(previewMasterLine);
		previewsComposite.declareAttribute(previewLineSize);
		previewsComposite.declareAttribute(previewSampling);
		previewsComposite.declareAttribute(previewMasterLineVisible);
		previewsComposite.declareAttribute(previewMasterLineColor);
		previewsComposite.declareAttribute(previewOtherLineVisible);
		previewsComposite.declareAttribute(previewOtherLineColor);

	}

	protected void declareIsobathsPreferences() {
		isobathMasterLine = new NumberPreferenceAttribute("100_isobathMasterLine", "Main line (every x lines)", 5);
		isobathStep = new NumberPreferenceAttribute("100_isobathStep", "Distance between two lines", 10.0d);
		isobathLineSize = new NumberPreferenceAttribute("100_isolineSize", "Isobath line size", 2);
		isobathSampling = new NumberPreferenceAttribute("101_isoBathOffSampling", "Isobath sampling", 60);
		isobathMasterLineVisible = new BooleanPreferenceAttribute("101_isobathMasterLineVisible", "Main line visible",
				true);
		isobathMasterLineColor = new ColorPreferenceAttribute("102_isobathMasterColor", "Isobath master line color",
				new GColor(255, 0, 0));
		isobathOtherLineVisible = new BooleanPreferenceAttribute("101_isobathOtherLineVisible", "Other line visible",
				true);
		isobathOtherLineColor = new ColorPreferenceAttribute("103_isobathColor", "Isobath line color",
				new GColor(255, 255, 255));

		PreferenceComposite isobathsComposite = new PreferenceComposite(this, "Isobaths");
		isobathsComposite.declareAttribute(isobathStep);
		isobathsComposite.declareAttribute(isobathMasterLine);
		isobathsComposite.declareAttribute(isobathLineSize);
		isobathsComposite.declareAttribute(isobathSampling);
		isobathsComposite.declareAttribute(isobathMasterLineVisible);
		isobathsComposite.declareAttribute(isobathMasterLineColor);
		isobathsComposite.declareAttribute(isobathOtherLineVisible);
		isobathsComposite.declareAttribute(isobathOtherLineColor);

	}

	protected void declareReferenceIsobathsPreferences() {
		referenceIsobathMasterLine = new NumberPreferenceAttribute("100_referenceIsobathsMasterLine",
				"Main line (every x lines)", 10);
		referenceIsobathStep = new NumberPreferenceAttribute("100_referenceIsobathsStep", "Distance between two lines",
				10.0d);
		referenceIsobathLineSize = new NumberPreferenceAttribute("100_isolineSize", "Isobaths line size", 2);
		referenceIsobathSampling = new NumberPreferenceAttribute("101_referenceIsobathsOffSampling",
				"Isobaths sampling", 60);
		referenceIsobathMasterLineVisible = new BooleanPreferenceAttribute("101_referenceIsobathsMasterLineVisible",
				"Main line visible", true);
		referenceIsobathMasterLineColor = new ColorPreferenceAttribute("102_referenceIsobathsMasterColor",
				"Isobaths master line color", new GColor(0, 255, 0));
		referenceIsobathOtherLineVisible = new BooleanPreferenceAttribute("101_referenceIsobathsOtherLineVisible",
				"Other line visible", true);
		referenceIsobathOtherLineColor = new ColorPreferenceAttribute("103_referenceIsobathsColor",
				"Isobaths line color", new GColor(255, 255, 255));

		PreferenceComposite referenceIsobathssComposite = new PreferenceComposite(this, "Reference isobaths");
		referenceIsobathssComposite.declareAttribute(referenceIsobathStep);
		referenceIsobathssComposite.declareAttribute(referenceIsobathMasterLine);
		referenceIsobathssComposite.declareAttribute(referenceIsobathLineSize);
		referenceIsobathssComposite.declareAttribute(referenceIsobathSampling);
		referenceIsobathssComposite.declareAttribute(referenceIsobathMasterLineVisible);
		referenceIsobathssComposite.declareAttribute(referenceIsobathMasterLineColor);
		referenceIsobathssComposite.declareAttribute(referenceIsobathOtherLineVisible);
		referenceIsobathssComposite.declareAttribute(referenceIsobathOtherLineColor);
	}

	protected void declareShiftedPreferences() {
		shiftedNavigationColor = new ColorPreferenceAttribute("10_shiftedNavigationColor", "Shifted navigation color",
				new GColor(25, 239, 255));
		shiftedNavigationPointSize = new NumberPreferenceAttribute("11_shiftedNavigationPointsize",
				"Shifted navigation point size", 3d);

		PreferenceComposite navComposite = new PreferenceComposite(this, "Shifted navigation");
		navComposite.declareAttribute(shiftedNavigationColor);
		navComposite.declareAttribute(shiftedNavigationPointSize);
	}

	protected void declareVectorPreferences() {
		vectorColor = new ColorPreferenceAttribute("05_vectorColor", "Vector color",
				ColorUtils.convertAWTtoGColor(Color.YELLOW));
		vectorPointSize = new NumberPreferenceAttribute("06_vectorPointsize", "Vector point size", 4d);

		highlightVectorColor = new ColorPreferenceAttribute("07_highlightvectorColor", "Highlight vector color",
				ColorUtils.convertAWTtoGColor(Color.YELLOW.brighter()));
		highlightVectorPointSize = new NumberPreferenceAttribute("08_highlightvectorPointsize",
				"Highlight vector point size", getVectorPointSize() * 2.5);
		selectedVectorColor = new ColorPreferenceAttribute("09_selectedVectorColor", "Selected vector color",
				ColorUtils.convertAWTtoGColor(Color.WHITE));
		PreferenceComposite vecorComposite = new PreferenceComposite(this, "Vector");
		vecorComposite.declareAttribute(vectorColor);
		vecorComposite.declareAttribute(vectorPointSize);
		vecorComposite.declareAttribute(highlightVectorColor);
		vecorComposite.declareAttribute(highlightVectorPointSize);
		vecorComposite.declareAttribute(selectedVectorColor);
	}

	protected void declareHiddenPreferences() {
		wizardInputFiles = new FileListPreferenceAttribute("wizardInputFiles", null);
		wizardDtmCellSize = new NumberPreferenceAttribute("wizardDtmCellSize", null, 1d);
		sessionFile = new FilePreferenceAttribute("SessionFile", null,
				FileSystems.getDefault().getPath(System.getProperty("user.home")));
		outputMbgFolder = new FilePreferenceAttribute("NavShiftExportDir", null,
				FileSystems.getDefault().getPath(System.getProperty("user.home")));
		declareAttribute(wizardInputFiles);
		declareAttribute(sessionFile);
		declareAttribute(outputMbgFolder);
	}

	protected void declarePeriodPreferences() {
		periodColor = new ColorPreferenceAttribute("00_periodColor", "Period color",
				ColorUtils.convertAWTtoGColor(Color.ORANGE));
		periodPointSize = new NumberPreferenceAttribute("01_periodPointsize", "Period point size", 7d);

		highlightPeriodColor = new ColorPreferenceAttribute("02_highlight_period_color", "Highlight period color",
				ColorUtils.convertAWTtoGColor(Color.ORANGE.brighter()));
		highlightPeriodPointSize = new NumberPreferenceAttribute("04_highlightPeriodPointsize",
				"Highlight period point size", getPeriodPointSize() * 1.5);
		PreferenceComposite periodComposite = new PreferenceComposite(this, "Period");
		periodComposite.declareAttribute(periodColor);
		periodComposite.declareAttribute(periodPointSize);
		periodComposite.declareAttribute(highlightPeriodColor);
		periodComposite.declareAttribute(highlightPeriodPointSize);
	}

	/** Getter of {@link #wizardInputFiles} */
	public FileListPreferenceAttribute getWizardInputFiles() {
		return wizardInputFiles;
	}

	/** Getter of {@link #sessionFile} */
	public FilePreferenceAttribute getSessionFile() {
		return sessionFile;
	}

	/** Getter of {@link #outputMbgFolder} */
	public FilePreferenceAttribute getOutputMbgFolder() {
		return outputMbgFolder;
	}

	/** Getter of {@link #periodColor} */
	public GColor getPeriodColor() {
		return periodColor.getValue();
	}

	/** Getter of {@link #highlightPeriodColor} */
	public GColor getHighlightPeriodColor() {
		return highlightPeriodColor.getValue();
	}

	/** Getter of {@link #periodPointSize} */
	public float getPeriodPointSize() {
		return periodPointSize.getValue().floatValue();
	}

	/** Getter of {@link #highlightPeriodPointSize} */
	public float getHighlightPeriodPointSize() {
		return highlightPeriodPointSize.getValue().floatValue();
	}

	/** Getter of {@link #vectorColor} */
	public GColor getVectorColor() {
		return vectorColor.getValue();
	}

	/** Getter of {@link #vectorPointSize} */
	public float getVectorPointSize() {
		return vectorPointSize.getValue().floatValue();
	}

	/** Getter of {@link #highlightVectorColor} */
	public GColor getHighlightVectorColor() {
		return highlightVectorColor.getValue();
	}

	/** Getter of {@link #highlightVectorPointSize} */
	public float getHighlightVectorPointSize() {
		return highlightVectorPointSize.getValue().floatValue();
	}

	/** Getter of {@link #selectedVectorColor} */
	public GColor getSelectedVectorColor() {
		return selectedVectorColor.getValue();
	}

	/** Getter of {@link #shiftedNavigationColor} */
	public GColor getShiftedNavigationColor() {
		return shiftedNavigationColor.getValue();
	}

	/** Getter of {@link #shiftedNavigationPointSize} */
	public float getShiftedNavigationPointSize() {
		return shiftedNavigationPointSize.getValue().floatValue();
	}

	/** Getter of {@link #periodColor} */
	public ColorPreferenceAttribute getPeriodColorAttribute() {
		return periodColor;
	}

	/** Getter of {@link #periodPointSize} */
	public NumberPreferenceAttribute getPeriodPointSizeAttribute() {
		return periodPointSize;
	}

	/** Getter of {@link #highlightPeriodColor} */
	public ColorPreferenceAttribute getHighlightPeriodColorAttribute() {
		return highlightPeriodColor;
	}

	/** Getter of {@link #highlightPeriodPointSize} */
	public NumberPreferenceAttribute getHighlightPeriodPointSizeAttribute() {
		return highlightPeriodPointSize;
	}

	/** Getter of {@link #vectorColor} */
	public ColorPreferenceAttribute getVectorColorAttribute() {
		return vectorColor;
	}

	/** Getter of {@link #vectorPointSize} */
	public NumberPreferenceAttribute getVectorPointSizeAttribute() {
		return vectorPointSize;
	}

	/** Getter of {@link #highlightVectorColor} */
	public ColorPreferenceAttribute getHighlightVectorColorAttribute() {
		return highlightVectorColor;
	}

	/** Getter of {@link #highlightVectorPointSize} */
	public NumberPreferenceAttribute getHighlightVectorPointSizeAttribute() {
		return highlightVectorPointSize;
	}

	/** Getter of {@link #selectedVectorColor} */
	public ColorPreferenceAttribute getSelectedVectorColorAttribute() {
		return selectedVectorColor;
	}

	/** Getter of {@link #shiftedNavigationColor} */
	public ColorPreferenceAttribute getShiftedNavigationColorAttribute() {
		return shiftedNavigationColor;
	}

	/** Getter of {@link #shiftedNavigationPointSize} */
	public NumberPreferenceAttribute getShiftedNavigationPointSizeAttribute() {
		return shiftedNavigationPointSize;
	}

	//

	/** Getter of {@link #isobathLineSize} */
	public NumberPreferenceAttribute getIsobathLineSize() {
		return isobathLineSize;
	}

	/** Getter of {@link #isobathSampling} */
	public NumberPreferenceAttribute getIsobathSampling() {
		return isobathSampling;
	}

	/** Getter of {@link #isobathMasterLineColor} */
	public ColorPreferenceAttribute getIsobathMasterLineColor() {
		return isobathMasterLineColor;
	}

	/** Getter of {@link #isobathMasterLine} */
	public NumberPreferenceAttribute getIsobathMasterLine() {
		return isobathMasterLine;
	}

	/** Getter of {@link #isobathStep} */
	public NumberPreferenceAttribute getIsobathStep() {
		return isobathStep;
	}

	/** Getter of {@link #isobathMasterLineVisible} */
	public BooleanPreferenceAttribute getIsobathMasterLineVisible() {
		return isobathMasterLineVisible;
	}

	/** Getter of {@link #isobathOtherLineVisible} */
	public BooleanPreferenceAttribute getIsobathOtherLineVisible() {
		return isobathOtherLineVisible;
	}

	/** Getter of {@link #isobathOtherLineColor} */
	public ColorPreferenceAttribute getIsobathOtherLineColor() {
		return isobathOtherLineColor;
	}

	/** Getter of {@link #wizardDtmCellSize} */
	public NumberPreferenceAttribute getWizardDtmCellSize() {
		return wizardDtmCellSize;
	}

	/** Setter of {@link #wizardDtmCellSize} */
	public void setWizardDtmCellSize(NumberPreferenceAttribute wizardDtmCellSize) {
		this.wizardDtmCellSize = wizardDtmCellSize;
	}

	/** Getter of {@link #previewLineSize} */
	public NumberPreferenceAttribute getPreviewLineSize() {
		return previewLineSize;
	}

	/** Getter of {@link #previewStep} */
	public NumberPreferenceAttribute getPreviewStep() {
		return previewStep;
	}

	/** Getter of {@link #previewSampling} */
	public NumberPreferenceAttribute getPreviewSampling() {
		return previewSampling;
	}

	/** Getter of {@link #previewMasterLineColor} */
	public ColorPreferenceAttribute getPreviewMasterLineColor() {
		return previewMasterLineColor;
	}

	/** Getter of {@link #previewMasterLineVisible} */
	public BooleanPreferenceAttribute getPreviewMasterLineVisible() {
		return previewMasterLineVisible;
	}

	/** Getter of {@link #previewOtherLineColor} */
	public ColorPreferenceAttribute getPreviewOtherLineColor() {
		return previewOtherLineColor;
	}

	/** Getter of {@link #previewOtherLineVisible} */
	public BooleanPreferenceAttribute getPreviewOtherLineVisible() {
		return previewOtherLineVisible;
	}

	/** Getter of {@link #previewMasterLine} */
	public NumberPreferenceAttribute getPreviewMasterLine() {
		return previewMasterLine;
	}

	public NumberPreferenceAttribute getReferenceIsobathLineSize() {
		return referenceIsobathLineSize;
	}

	public NumberPreferenceAttribute getReferenceIsobathStep() {
		return referenceIsobathStep;
	}

	public NumberPreferenceAttribute getReferenceIsobathSampling() {
		return referenceIsobathSampling;
	}

	public ColorPreferenceAttribute getReferenceIsobathMasterLineColor() {
		return referenceIsobathMasterLineColor;
	}

	public BooleanPreferenceAttribute getReferenceIsobathMasterLineVisible() {
		return referenceIsobathMasterLineVisible;
	}

	public ColorPreferenceAttribute getReferenceIsobathOtherLineColor() {
		return referenceIsobathOtherLineColor;
	}

	public BooleanPreferenceAttribute getReferenceIsobathOtherLineVisible() {
		return referenceIsobathOtherLineVisible;
	}

	public NumberPreferenceAttribute getReferenceIsobathMasterLine() {
		return referenceIsobathMasterLine;
	}
}
