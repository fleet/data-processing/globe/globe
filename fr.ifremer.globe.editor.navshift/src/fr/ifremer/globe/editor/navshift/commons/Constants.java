/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.commons;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;

/**
 *
 */
public class Constants {

	/** The plug-in ID */
	public static final String PLUGIN_ID = "fr.ifremer.globe.editor.navshift"; //$NON-NLS-1$

	/** Tiff file extension */
	public static final String TIFF = "tif";

	/** Tiff file extension */
	public static final String NVI = IFileService.grab().getExtensions(ContentType.NVI_NETCDF_4).get(0);

	/** Constructor */
	private Constants() {
	}

}
