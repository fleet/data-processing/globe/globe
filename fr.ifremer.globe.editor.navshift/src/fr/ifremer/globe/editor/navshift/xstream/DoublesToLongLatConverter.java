/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.xstream;

import java.util.Optional;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import fr.ifremer.globe.core.utils.latlon.CoordinateDisplayType;
import fr.ifremer.globe.core.utils.latlon.ILatLongFormatter;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;

public class DoublesToLongLatConverter implements Converter {

	private static final ILatLongFormatter formatter = LatLongFormater.getFormatter(CoordinateDisplayType.DECIMAL);
	private static final ILatLongFormatter deprecatedFormatter = LatLongFormater
			.getFormatter(CoordinateDisplayType.DEG_MIN_DEC);

	private static final String LONG_ATTR_NAME = "longitude";

	private static final String LAT_ATTR_NAME = "latitude";

	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
		double[] coords = (double[]) source;
		writer.addAttribute(LONG_ATTR_NAME, formatter.formatLongitude(coords[0]));
		writer.addAttribute(LAT_ATTR_NAME, formatter.formatLatitude(coords[1]));
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		double[] coords = new double[2];
		String longitudeString = reader.getAttribute(LONG_ATTR_NAME);
		coords[0] = Optional.ofNullable(formatter.parseLongitude(longitudeString))
				.orElse(deprecatedFormatter.parseLongitude(longitudeString));
		String latitudeString = reader.getAttribute(LAT_ATTR_NAME);
		coords[1] = Optional.ofNullable(formatter.parseLatitude(latitudeString))
				.orElse(deprecatedFormatter.parseLatitude(latitudeString));
		return coords;
	}

	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class type) {
		return type != null && type.isArray();
	}
}
