/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.xstream;

import java.util.Date;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class LongToDateConverter implements Converter {

	private static final String DATE_ATTR_NAME = "date";
	private static final String TIME_ATTR_NAME = "time";

	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
		long time = (long) source;
		Date aDate = new Date(time);
		writer.addAttribute(DATE_ATTR_NAME, FormatData.dateToDateString(aDate));
		writer.addAttribute(TIME_ATTR_NAME, FormatData.dateToTimeString(aDate));
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		return FormatData.twoStringsToLongTime(reader.getAttribute(DATE_ATTR_NAME),
				reader.getAttribute(TIME_ATTR_NAME));

	}

	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class type) {
		return type == long.class || type == Long.class;
	}
}
