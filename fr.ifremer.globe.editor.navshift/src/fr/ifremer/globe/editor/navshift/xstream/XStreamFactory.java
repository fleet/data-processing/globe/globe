package fr.ifremer.globe.editor.navshift.xstream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;

import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.IShiftVector;
import fr.ifremer.globe.editor.navshift.model.impl.SessionContextImpl;
import fr.ifremer.globe.editor.navshift.model.impl.ShiftPeriodImpl;
import fr.ifremer.globe.editor.navshift.model.impl.ShiftVectorImpl;

/**
 * Provides a common XStream configuration
 */
public class XStreamFactory {

	/**
	 * Constructor
	 */
	private XStreamFactory() {
		// utility class
	}

	/**
	 * Creates common XStream configuration
	 * 
	 * @return
	 */
	public static XStream create() {
		/*
		 * need to provide pureJavaReflectionProvider so that xstream uses default constructor
		 */
		XStream xstream = new XStream(new PureJavaReflectionProvider());
		xstream.setClassLoader(SessionContextImpl.class.getClassLoader());
		xstream.addDefaultImplementation(ShiftVectorImpl.class, IShiftVector.class);
		xstream.addDefaultImplementation(ShiftPeriodImpl.class, IShiftPeriod.class);
		xstream.addDefaultImplementation(SessionContextImpl.class, ISessionContext.class);
		xstream.autodetectAnnotations(true);
		xstream.processAnnotations(IShiftPeriod.class);
		xstream.processAnnotations(ISessionContext.class);
		xstream.processAnnotations(ShiftVectorImpl.class);
		xstream.processAnnotations(ShiftPeriodImpl.class);
		xstream.processAnnotations(SessionContextImpl.class);
		return xstream;
	}
}
