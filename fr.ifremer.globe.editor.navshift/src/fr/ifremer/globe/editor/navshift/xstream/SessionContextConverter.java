/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.editor.navshift.xstream;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftVector;
import fr.ifremer.globe.editor.navshift.model.impl.SessionContextImpl;
import fr.ifremer.globe.editor.navshift.model.impl.ShiftPeriodImpl;
import fr.ifremer.globe.editor.navshift.model.impl.ShiftVectorImpl;

public class SessionContextConverter implements Converter {

	private static final String NAVSHIFT_PERIOD_STR = "NavShiftPeriod";
	private static final String FILESET_STR = "ModelGeneratedFrom";

	private static final String DTMPARAMS_STR = "DtmParameters";
	private static final String DTMRESOLUTION_ATTR = "Resolution";

	private static final String ISOBATHPARAMS_STR = "IsobathParameters";
	private static final String ISOBATHMAINLINE_ATTR = "MainLine";
	private static final String ISOBATHSTEP_ATTR = "Step";
	private static final String ISOBATHSAMPLING_ATTR = "Sampling";

	private static final String SHIFTVECTS_LIST_STR = "ShiftVectors";
	private static final String SHIFTVEC_STR = "ShiftVector";

	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
		ISessionContext sessionContext = (ISessionContext) source;

		writer.startNode(FILESET_STR);
		context.convertAnother(sessionContext.getCurrentFileset());
		writer.endNode();

		writer.startNode(DTMPARAMS_STR);
		writer.addAttribute(DTMRESOLUTION_ATTR, String.valueOf(sessionContext.getDtmResolution()));
		writer.endNode();

		writer.startNode(ISOBATHPARAMS_STR);
		writer.addAttribute(ISOBATHMAINLINE_ATTR, String.valueOf(sessionContext.getIsobathMainLine()));
		writer.addAttribute(ISOBATHSTEP_ATTR, String.valueOf(sessionContext.getIsobathStep()));
		writer.addAttribute(ISOBATHSAMPLING_ATTR, String.valueOf(sessionContext.getIsobathSampling()));
		writer.endNode();

		if (sessionContext.getShiftPeriod() != null) {
			writer.startNode(NAVSHIFT_PERIOD_STR);
			context.convertAnother(sessionContext.getShiftPeriod());
			writer.endNode();

			writer.startNode(SHIFTVECTS_LIST_STR);
			for (IShiftVector vec : sessionContext.getShiftPeriod().getShiftVectors()) {
				writer.startNode(SHIFTVEC_STR);
				context.convertAnother(vec);
				writer.endNode();
			}
			writer.endNode();
		}

	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

		ISessionContext sessionCtx = (ISessionContext) context.currentObject();
		ShiftPeriodImpl currentPeriod = null;
		while (reader.hasMoreChildren()) {
			reader.moveDown();
			if (reader.getNodeName().equals(FILESET_STR)) {
				List<File> fileset = new ArrayList<>();
				while (reader.hasMoreChildren()) {
					reader.moveDown();
					File another = (File) context.convertAnother(sessionCtx, File.class);
					fileset.add(another);
					reader.moveUp();
				}
				sessionCtx.setCurrentFileset(fileset);
			}
			if (reader.getNodeName().equals(DTMPARAMS_STR)) {
				String val = reader.getAttribute(DTMRESOLUTION_ATTR);
				if (val != null) {
					sessionCtx.setDtmResolution(Double.parseDouble(val));
				}
			}
			if (reader.getNodeName().equals(ISOBATHPARAMS_STR)) {
				String val = reader.getAttribute(ISOBATHMAINLINE_ATTR);
				if (val != null) {
					sessionCtx.setIsobathMainLine(Integer.parseInt(val));
				}
				val = reader.getAttribute(ISOBATHSTEP_ATTR);
				if (val != null) {
					sessionCtx.setIsobathStep(Double.parseDouble(val));
				}
				val = reader.getAttribute(ISOBATHSAMPLING_ATTR);
				if (val != null) {
					sessionCtx.setIsobathSampling(Integer.parseInt(val));
				}

			}
			if (reader.getNodeName().equals(NAVSHIFT_PERIOD_STR)) {

				currentPeriod = (ShiftPeriodImpl) context.convertAnother(sessionCtx, ShiftPeriodImpl.class);
				sessionCtx.setShiftPeriod(currentPeriod);
			}
			if (reader.getNodeName().equals(SHIFTVECTS_LIST_STR)) {
				if (currentPeriod != null) {
					while (reader.hasMoreChildren()) {
						reader.moveDown();
						ShiftVectorImpl another = (ShiftVectorImpl) context.convertAnother(sessionCtx,
								ShiftVectorImpl.class);
						currentPeriod.addShiftVector(another);
						reader.moveUp();
					}
				} else {
					throw new ConversionException("No ShiftPeriod found before vectors");
				}

			}
			reader.moveUp();

		}
		return sessionCtx;
	}

	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class type) {
		return type == SessionContextImpl.class || type == ISessionContext.class;
	}
}
