/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.xstream;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utility class to format data
 */
public class FormatData {

	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	public static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss.SSS");

	static {
		java.util.TimeZone gmtZone = java.util.TimeZone.getTimeZone("GMT");
		dateFormat.setTimeZone(gmtZone);
		timeFormat.setTimeZone(gmtZone);
	}

	/**
	 * Translate date to String (date part)
	 */
	public static String dateToDateString(Date aDate) {
		if (aDate != null) {
			return dateFormat.format(aDate);
		} else {
			return "";
		}
	}

	/**
	 * Translate date to String (time part)
	 */
	public static String dateToTimeString(Date aDate) {
		if (aDate != null) {
			return timeFormat.format(aDate);
		} else {
			return "";
		}
	}

	/**
	 * Translate a pair of Strings to a Datetime representaiton as a 'long' value
	 */
	public static long twoStringsToLongTime(String dateStr, String timeStr) {
		Date aDate;
		Date aTime;
		try {
			aDate = dateFormat.parse(dateStr);
			aTime = timeFormat.parse(timeStr);
		} catch (ParseException e) {
			return -1;
		}
		return aDate.getTime() + aTime.getTime();

	}

}
