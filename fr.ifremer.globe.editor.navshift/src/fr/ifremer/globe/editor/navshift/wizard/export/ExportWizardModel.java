package fr.ifremer.globe.editor.navshift.wizard.export;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPageModel;

/**
 * Model of export wizards
 */

public class ExportWizardModel implements SelectOutputParametersPageModel {

	/** Map of input files and data proxy */
	private final Map<File, ISounderNcInfo> inputs;

	/** Output file name */
	private final WritableString singleOutputFilename = new WritableString();
	/** Output directory */
	private final WritableFile outputDirectory;

	/** List of input Files */
	private final WritableObjectList<File> inputFiles;

	/** List of output Files */
	private final WritableObjectList<File> outputFiles;

	/** Extension of output files */
	private final WritableObjectList<String> outputFormatExtensions;
	/** Prefix of output files */
	private final WritableString prefix;

	/** Suffix of output files */
	private final WritableString suffix;

	/** Overwrite existing files */
	private final WritableBoolean overwriteExistingFiles;

	/** True to load files at the end of process */
	private final WritableBoolean loadFilesAfter;
	/** Where to load the files (in witch group of project explorer) */
	private final WritableString whereToloadFiles;

	/** is Navigtion merged in a single file */
	private Optional<WritableBoolean> mergeOutputFiles;

	/**
	 * Constructor
	 * 
	 */
	public ExportWizardModel() {
		inputs = new HashMap<>();
		outputDirectory = new WritableFile();
		inputFiles = new WritableObjectList<>();
		outputFiles = new WritableObjectList<>();
		outputFormatExtensions = new WritableObjectList<>();
		prefix = new WritableString();
		suffix = new WritableString();
		overwriteExistingFiles = new WritableBoolean(true);
		loadFilesAfter = new WritableBoolean(false);
		whereToloadFiles = new WritableString();
	}

	/**
	 * Adds an input file and its {@link INavigationData} to the wizard model
	 */
	public void addInput(File file, ISounderNcInfo ncInfo) {
		inputs.put(file, ncInfo);
		inputFiles.add(file);
	}

	// GETTERS

	public Map<File, ISounderNcInfo> getInputs() {
		return inputs;
	}

	@Override
	public WritableFile getOutputDirectory() {
		return outputDirectory;
	}

	@Override
	public WritableString getPrefix() {
		return prefix;
	}

	@Override
	public WritableString getSuffix() {
		return suffix;
	}

	@Override
	public WritableObjectList<File> getInputFiles() {
		return inputFiles;
	}

	@Override
	public WritableObjectList<String> getOutputFormatExtensions() {
		return outputFormatExtensions;
	}

	@Override
	public WritableObjectList<File> getOutputFiles() {
		return outputFiles;
	}

	@Override
	public WritableBoolean getOverwriteExistingFiles() {
		return overwriteExistingFiles;
	}

	@Override
	public WritableBoolean getLoadFilesAfter() {
		return loadFilesAfter;
	}

	/**
	 * Getter of mergeOutputFiles
	 */
	@Override
	public Optional<WritableBoolean> getMergeOutputFiles() {
		return mergeOutputFiles;

	}

	/** Setter of {@link #mergeOutputFiles} */
	public void setMergeOutputFiles(Optional<WritableBoolean> mergeOutputFiles) {
		this.mergeOutputFiles = mergeOutputFiles;
	}

	/** Getter of {@link #singleOutputFilename} */
	@Override
	public WritableString getSingleOutputFilename() {
		return singleOutputFilename;
	}

	/**
	 * @return the {@link #whereToloadFiles}
	 */
	@Override
	public WritableString getWhereToloadFiles() {
		return whereToloadFiles;
	}

}