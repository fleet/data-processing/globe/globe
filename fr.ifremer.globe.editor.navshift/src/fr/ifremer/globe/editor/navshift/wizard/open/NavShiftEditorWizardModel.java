/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.wizard.open;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.core.utils.preference.attributes.NumberPreferenceAttribute;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.commons.IsobathParametersFactory;
import fr.ifremer.globe.editor.navshift.wizard.open.NavShiftEditorParametersWizardPage.NavShiftEditorParametersModel;
import fr.ifremer.globe.ui.databinding.model.PreferenceBasedModel;
import fr.ifremer.globe.ui.databinding.observable.WritableNumber;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage.SelectInputFilesPageModel;
import fr.ifremer.viewer3d.layers.lineisobaths.parameters.IsobathParameters;

/**
 * Model of the wizard page
 */
public class NavShiftEditorWizardModel extends PreferenceBasedModel
		implements SelectInputFilesPageModel, NavShiftEditorParametersModel {

	/** Isobath parameters factory */
	@Inject
	@Named(ContextNames.ISOBATH_PARAM_FACTORY)
	protected IsobathParametersFactory isobathParametersFactory;

	/** File service */
	@Inject
	protected IFileService fileService;

	/** List of Files */
	protected WritableObjectList<File> inputFiles = new WritableObjectList<>();
	/** List of ISounderNcInfo opened on inputFiles */
	protected List<ISounderNcInfo> inputSounderNcInfo;
	/** List of Rasters from NetCdf4 DTM file opened on inputFiles */
	protected List<RasterInfo> inputReferenceRasterInfo;
	/** List of file Extensions */
	protected List<Pair<String, String>> inputFilesFilterExtensions = new ArrayList<>();
	/** Cell size for computing the spatial resolution of DTM */
	protected WritableNumber dtmCellSize = new WritableNumber(1.0);
	/** GeoBox of the considered DTM */
	protected WritableObject<GeoBox> geoBox = new WritableObject<>(null);
	/** Projection of the considered DTM */
	protected WritableObject<Projection> projection = new WritableObject<>(null);

	/** Isobath - Distance between two secondary lines. */
	protected WritableNumber step = new WritableNumber(0);
	/** Isobath - Main line (every x lines) */
	protected WritableNumber mainLine = new WritableNumber(0);
	/** Isobath - % of points extected per lines. */
	protected WritableNumber sampling = new WritableNumber(0);

	/**
	 * Constructor
	 */
	public NavShiftEditorWizardModel() {
		// Inject this to the current context
		ContextInjectionFactory.inject(this, ContextInitializer.getEclipseContext());

		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.MBG_NETCDF_3));
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.XSF_NETCDF_4));
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.DTM_NETCDF_3));
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.DTM_NETCDF_4));
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.RASTER_GDAL));
	}

	/**
	 * After injection
	 */
	@PostConstruct
	public void intialize() {
		IsobathParameters isobathParameters = isobathParametersFactory.get();
		step = isobathParameters.getStep();
		mainLine = isobathParameters.getMainLine();
		sampling = isobathParameters.getSampling();
	}

	/**
	 * Getter of inputFiles
	 */
	@Override
	public WritableObjectList<File> getInputFiles() {
		return inputFiles;
	}

	/**
	 * Getter of inputFilesFilterExtensions
	 */
	@Override
	public List<Pair<String, String>> getInputFilesFilterExtensions() {
		return inputFilesFilterExtensions;
	}

	/**
	 * Getter of dtmCellSize
	 */
	@Override
	public WritableNumber getDtmCellSize() {
		return dtmCellSize;
	}

	/**
	 * Getter of geoBox
	 */
	@Override
	public WritableObject<GeoBox> getGeoBox() {
		return geoBox;
	}

	/**
	 * Getter of step
	 */
	@Override
	public WritableNumber getStep() {
		return step;
	}

	/**
	 * Getter of mainLine
	 */
	@Override
	public WritableNumber getMainLine() {
		return mainLine;
	}

	/**
	 * Getter of sampling
	 */
	@Override
	public WritableNumber getSampling() {
		return sampling;
	}

	/**
	 * Getter of inputSounderNcInfo
	 */
	public List<ISounderNcInfo> getInputSounderNcInfo() {
		return inputSounderNcInfo;
	}

	/**
	 * Setter of inputSounderNcInfo
	 */
	public void setInputSounderNcInfo(List<ISounderNcInfo> inputSounderNcInfo) {
		this.inputSounderNcInfo = inputSounderNcInfo;
	}

	/**
	 * Getter of inputReferenceDtmNcInfo
	 */
	public List<RasterInfo> getInputRasterInfo() {
		return inputReferenceRasterInfo;
	}

	/**
	 * Setter of inputReferenceDtmNcInfo
	 */
	public void setInputRasterInfo(List<RasterInfo> inputReferenceRasterInfo) {
		this.inputReferenceRasterInfo = inputReferenceRasterInfo;
	}

	/** Sync attribute on preference */
	public void syncDtmCellSize(NumberPreferenceAttribute preference) {
		sync(preference, dtmCellSize);
	}

	/** Sync attribute on preference */
	public void syncMainLine(NumberPreferenceAttribute preference) {
		sync(preference, mainLine);
	}

	/** Sync attribute on preference */
	public void syncStep(NumberPreferenceAttribute preference) {
		sync(preference, step);
	}

	/** Sync attribute on preference */
	public void syncSampling(NumberPreferenceAttribute preference) {
		sync(preference, sampling);
	}

}