/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.wizard.open;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.jface.dialogs.PageChangingEvent;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.custom.BusyIndicator;

import fr.ifremer.globe.core.model.dtm.SounderDataDtmResolutionComputer;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.commons.NavShiftPreference;
import fr.ifremer.globe.editor.navshift.commons.SounderNcInfoUtility;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Wizard to select the input parameters for opening the navigation shift editor
 */
public class NavShiftEditorWizard extends Wizard {

	/** Model */
	protected NavShiftEditorWizardModel navigationShiftEditorWizardModel;

	/** True when {@link #loadFiles()} has to be invoked */
	protected boolean mayLoadFiles = false;

	/** Pages */
	protected SelectInputFilesPage selectInputFilesPage;
	protected NavShiftEditorParametersWizardPage spatialResolutionWizardPage;

	/** Booker of DataContainer. */
	@Inject
	protected IDataContainerFactory dataContainerFactory;

	/** Preferences */
	@Inject
	@Named(ContextNames.NAVSHIFT_PREFERENCE)
	protected NavShiftPreference navShiftPreference;

	/** File service */
	@Inject
	protected IFileService fileService;

	/**
	 * Constructor
	 */
	public NavShiftEditorWizard(NavShiftEditorWizardModel navigationShiftEditorWizardModel) {
		this.navigationShiftEditorWizardModel = navigationShiftEditorWizardModel;
		setNeedsProgressMonitor(true);

	}

	@PostConstruct
	protected void postConstruct() {
		navigationShiftEditorWizardModel.getSampling().set(navShiftPreference.getIsobathSampling().getValue());
		navigationShiftEditorWizardModel.getInputFiles().addChangeListener(event -> mayLoadFiles = true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(navigationShiftEditorWizardModel);
		addPage(selectInputFilesPage);

		spatialResolutionWizardPage = new NavShiftEditorParametersWizardPage(navigationShiftEditorWizardModel);
		addPage(spatialResolutionWizardPage);
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#canFinish()
	 */
	@Override
	public boolean canFinish() {
		return !mayLoadFiles && super.canFinish();
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		if (mayLoadFiles) {
			try {
				loadFiles();
				mayLoadFiles = false;
				selectInputFilesPage.setErrorMessage(null);
			} catch (GException e) {
				selectInputFilesPage.setErrorMessage(e.getMessage());
			}
		}
		return !mayLoadFiles;
	}

	/**
	 * Wizard is about to change the current page. <br>
	 * Compute the spatial resolution if needed
	 */
	public void pageChanging(PageChangingEvent event) {
		Object currentPage = event.getCurrentPage();
		if (mayLoadFiles && currentPage == selectInputFilesPage) {
			BusyIndicator.showWhile(getShell().getDisplay(), () -> {
				try {
					loadFiles();
					mayLoadFiles = false;
					selectInputFilesPage.setErrorMessage(null);
				} catch (GException e) {
					selectInputFilesPage.setErrorMessage(e.getMessage());
					// Do not chande the page
					event.doit = false;
				}
			});
		}
	}

	/**
	 * Load input files. <br>
	 * Compute DtmCellSize according to the selected sounder files
	 */
	public void loadFiles() throws GException {
		List<ISounderNcInfo> sounderNcInfos = new ArrayList<>();

		SounderNcInfoUtility.load(navigationShiftEditorWizardModel.getInputFiles().asList(), sounderNcInfos::addAll,
				geoBox -> navigationShiftEditorWizardModel.getGeoBox().set(geoBox), Optional.empty());

		if (sounderNcInfos.isEmpty()) {
			throw new GIOException("No suitable file to continue");
		}

		Projection projection = ContextInitializer
				.computeMercatorProjection(navigationShiftEditorWizardModel.getGeoBox().get());
		SounderDataDtmResolutionComputer computer = new SounderDataDtmResolutionComputer(sounderNcInfos, projection);

		navigationShiftEditorWizardModel.getDtmCellSize().set(computer.computeGridDefaultResolution());
		navigationShiftEditorWizardModel.setInputSounderNcInfo(sounderNcInfos);

	}

}
