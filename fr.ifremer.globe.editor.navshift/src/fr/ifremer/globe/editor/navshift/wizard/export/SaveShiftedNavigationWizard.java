/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.wizard.export;

import java.util.Optional;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.editor.navshift.commons.Constants;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Wizard to export as NVI
 */
@Creatable
public class SaveShiftedNavigationWizard extends Wizard {

	/** Model */
	protected ExportWizardModel exportWizardModel;

	/**
	 * Wizard pages
	 */
	protected SelectOutputParametersPage selectOutputFilesPage;

	SaveShiftedNavigationWizard() {
		exportWizardModel = new ExportWizardModel();
		exportWizardModel.getOutputFormatExtensions().add(Constants.NVI);
		exportWizardModel.setMergeOutputFiles(Optional.of(new WritableBoolean(false)));
	}

	/**
	 * Model getter
	 */
	public ExportWizardModel getExportWizardModel() {
		return exportWizardModel;
	}

	/**
	 *
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {

		return true;
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectOutputFilesPage = new SelectOutputParametersPage(exportWizardModel);
		selectOutputFilesPage.enableLoadFilesAfter(true);
		addPage(selectOutputFilesPage);
	}
}
