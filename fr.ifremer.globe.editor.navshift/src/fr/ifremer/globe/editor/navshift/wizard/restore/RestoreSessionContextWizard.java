/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.wizard.restore;

import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.wizard.SelectFilePage;

/**
 * Wizard to select the file from which retoring the session
 */
public class RestoreSessionContextWizard extends Wizard {

	/** Model */
	protected RestoreSessionContextModel restoreSessionModel;

	/**
	 * Wizard pages
	 */
	protected SelectFilePage selectInputFilePage;

	public RestoreSessionContextWizard(RestoreSessionContextModel sessionModel) {
		restoreSessionModel = sessionModel;
	}

	/**
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {

		if (restoreSessionModel.getSelectedFile().exists()) {
			return true;
		}
		// show error dialog
		Messages.openErrorMessage("File Error",
				String.format("Selected File %s does not exist. %n Please select another one",
						restoreSessionModel.getSelectedFile().getName()));
		return false;

	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilePage = new SelectFilePage(restoreSessionModel);
		addPage(selectInputFilePage);
	}

}
