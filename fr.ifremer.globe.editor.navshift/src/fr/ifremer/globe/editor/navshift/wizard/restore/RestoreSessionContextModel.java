
/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.wizard.restore;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.e4.core.di.annotations.Creatable;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.validation.FileValidator;
import fr.ifremer.globe.ui.widget.FileComposite.SelectFileCompositeModel;

/**
 * Model used by the {@link RestoreSessionContextWizard}
 */

@Creatable
public class RestoreSessionContextModel implements SelectFileCompositeModel {

	/**
	 * Static values to host file extension to use
	 */
	public static final String XML_EXTENSION = "xml";
	static List<Pair<String, String>> filterExtensions = new ArrayList<>();

	static {
		filterExtensions.add(new Pair<>("XML File", "*." + XML_EXTENSION));
	}

	/**
	 * file to be set
	 */
	private WritableFile inputFile;

	public RestoreSessionContextModel() {
		inputFile = new WritableFile();
	}

	@Override
	public WritableFile getSelectedFile() {
		return inputFile;
	}

	@Override
	public String getSelectedFileFilterExtension() {
		return XML_EXTENSION;
	}

	@Override
	public int getFileRequirements() {
		return FileValidator.FILE;
	}

	@Override
	public List<Pair<String, String>> getSelectedFileFilterExtensions() {
		return filterExtensions;
	}

}