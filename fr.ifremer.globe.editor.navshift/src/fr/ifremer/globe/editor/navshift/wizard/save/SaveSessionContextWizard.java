/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.wizard.save;

import java.util.ArrayList;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.controller.SessionController;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.validation.FileValidator;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.widget.FileComposite.SelectFileCompositeModel;
import fr.ifremer.globe.ui.wizard.SelectFilePage;

/**
 * Wizard to select the file from which to save the session
 */
@Creatable
public class SaveSessionContextWizard extends Wizard {

	/**
	 * Static values to host file extension to use
	 */
	public static final String XML_EXTENSION = "xml";
	private static List<Pair<String, String>> filterExtensions = new ArrayList<>();

	static {
		filterExtensions.add(new Pair<>("XML File", "*." + XML_EXTENSION));
	}

	/** Session controller, used to control application's state */
	@Inject
	@Named(ContextNames.SESSION_CONTROLLER)
	protected SessionController sessionController;

	/** Model */
	protected SaveSessionModel saveSessionModel;

	/**
	 * Wizard pages
	 */
	protected SelectFilePage selectOutputFilePage;

	SaveSessionContextWizard() {
		saveSessionModel = new SaveSessionModel();
	}

	/**
	 * Model getter
	 * 
	 * @return
	 */
	public SaveSessionModel getSaveSessionModel() {
		return saveSessionModel;
	}

	/**
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		if (saveSessionModel.getSelectedFile().exists()) {

			return Messages.openSyncQuestionMessage("Overwrite File?", "File already exists. Overwrite?");
		}
		return true;
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectOutputFilePage = new SelectFilePage(saveSessionModel);
		addPage(selectOutputFilePage);
	}

	/**
	 * Inner class used to host file name selected
	 */

	public class SaveSessionModel implements SelectFileCompositeModel {

		private WritableFile outputFile;

		SaveSessionModel() {
			outputFile = new WritableFile();
		}

		@Override
		public WritableFile getSelectedFile() {
			return outputFile;
		}

		@Override
		public String getSelectedFileFilterExtension() {
			return XML_EXTENSION;
		}

		@Override
		public int getFileRequirements() {
			return FileValidator.FILE;
		}

		@Override
		public List<Pair<String, String>> getSelectedFileFilterExtensions() {
			return filterExtensions;
		}

	}
}
