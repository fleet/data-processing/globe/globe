/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.wizard.export;

import java.util.Optional;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Wizard to export previewed SounderFile
 */
@Creatable
public class ExportToSounderWizard extends Wizard {

	/** File services */
	@Inject
	protected IFileService fileService;

	/** Model */
	protected ExportWizardModel exportWizardModel;

	/**
	 * Wizard pages
	 */
	protected SelectOutputParametersPage selectOutputFilesPage;

	@PostConstruct
	public void postConstruct() {
		exportWizardModel = new ExportWizardModel();
		exportWizardModel.setMergeOutputFiles(Optional.empty());
	}

	/**
	 * Model getter
	 */
	public ExportWizardModel getExportWizardModel() {
		return exportWizardModel;
	}

	/**
	 *
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		return true;
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectOutputFilesPage = new SelectOutputParametersPage(exportWizardModel);
		selectOutputFilesPage.enableLoadFilesAfter(true);
		addPage(selectOutputFilesPage);
	}
}
