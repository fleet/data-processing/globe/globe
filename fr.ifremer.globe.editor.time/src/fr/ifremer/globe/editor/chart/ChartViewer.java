package fr.ifremer.globe.editor.chart;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesStatusHandler;
import fr.ifremer.globe.core.model.chart.impl.ChartFileInfoDataSource;
import fr.ifremer.globe.core.model.chart.impl.LoadableChartSeries;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.chart.dialog.toolbox.ChartEditorToolbox;
import fr.ifremer.globe.editor.chart.dnd.ChartViewerDropListener;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel.ChartViewerModelEvent;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel.ChartViewerModelEventType;
import fr.ifremer.globe.editor.chart.ui.FileLegendGroupComposite;
import fr.ifremer.globe.editor.chart.ui.VariableLegendGroupComposite;
import fr.ifremer.globe.ui.javafxchart.FXChart;
import fr.ifremer.globe.ui.javafxchart.utils.FXChartException;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.ChartCursor;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.SelectionMode;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.service.globalcursor.GlobalCursor;
import fr.ifremer.globe.ui.service.globalcursor.IGlobalCursorService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.WWNavigationLayerParameters;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.number.NumberUtils;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import javafx.scene.paint.Color;

/**
 * Chart viewer part.
 */
public class ChartViewer {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChartViewer.class);

	/** PART ID **/
	public static final String PART_ID = "fr.ifremer.globe.editor.chart.part";

	private final IEventBroker eventBroker;
	private final IGlobalCursorService globalCursorService;
	private final WWFileLayerStoreModel fileLayerStoreModel;

	// model
	private final ChartViewerModel model;

	// composites
	private FXChart chart;
	private Composite legendParentComposite = null;

	// organization mode
	public enum OrganizationMode {
		ORGANIZE_BY_FILE,
		ORGANIZE_BY_VARIABLE
	}

	private OrganizationMode organizationMode;

	// Edition & selection mode
	public enum EditionMode {
		VALIDATE,
		INVALIDATE,
		ZOOM;
	}

	private EditionMode editionMode = EditionMode.INVALIDATE;

	// selection mode
	private SelectionMode selectionMode = SelectionMode.RECTANGLE_SELECTION;

	private final Composite parent;

	// cursor service and listener to update chart with global cursor

	private Consumer<GlobalCursor> cursorServiceListener = cursor -> {
		if (cursor.time().isPresent() && (cursor.source().isEmpty() || cursor.source().get() != this))
			chart.moveCursor(cursor.time().get().toEpochMilli(), 0);
	};

	private MPart part;

	/**
	 * Constructor
	 */
	@Inject
	public ChartViewer(@Named("chartViewerModel") ChartViewerModel chartViewerModel, Composite parent, MPart mPart,
			IEventBroker eventBroker, IGlobalCursorService globalCursorService,
			WWFileLayerStoreModel fileLayerStoreModel) {
		model = chartViewerModel;
		part = mPart;
		this.eventBroker = eventBroker;
		this.globalCursorService = globalCursorService;
		this.fileLayerStoreModel = fileLayerStoreModel;
		this.parent = parent;

		try {
			createUI();
		} catch (Exception e) {
			LOGGER.warn("Error during Chart Editor initialization", e);
			Messages.openErrorMessage("Chart Editor", "Error during Chart Editor initialization", e);
		}
	}

	/**
	 * Initialization method: creates UI
	 */
	private void createUI() throws FXChartException, IOException {
		parent.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginWidth = 0;
		gridLayout.verticalSpacing = 0;
		gridLayout.marginHeight = 0;
		parent.setLayout(gridLayout);

		ChartViewerDropListener dropListener = new ChartViewerDropListener(model);
		dropListener.addTo(parent);

		// chart
		Composite compositeChart = new Composite(parent, SWT.NONE);
		compositeChart.setLayout(new FillLayout(SWT.HORIZONTAL));
		compositeChart.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		chart = new FXChart(compositeChart, "");
		// dropListener.addTo(chart.getCanvas()); DOESN'T WORK
		chart.setAutoZoom(false);
		updateChartSelectionMode();

		// legend composites
		setOrganizationMode(model.getBySource().size() == 2 ? OrganizationMode.ORGANIZE_BY_FILE
				: OrganizationMode.ORGANIZE_BY_VARIABLE);

		setCursor(ChartCursor.LINE_CURSOR);

		// listener
		chart.addCursorListener(chartData -> {
			if (chartData == null) {
				globalCursorService.submitEmpty();
				return;
			}
			var datetime = Instant.ofEpochMilli((long) chartData.getX());
			model.getNavigationData(chartData.getSeries()).ifPresent(navData -> {
				try {
					// Search if a navigation layer exists and lays on the ground
					boolean clampToGround = fileLayerStoreModel.get(navData.getNavigationData().getFileName())//
							.stream()//
							.flatMap(store -> store.getLayers(IWWNavigationLayer.class))//
							.map(IWWNavigationLayer::getDisplayParameters)//
							.anyMatch(WWNavigationLayerParameters::displayGroundPath);

					var lon = navData.getLongitude(datetime);
					var lat = navData.getLatitude(datetime);
					var elev = navData.getNavigationData().getMetadataTypes().contains(NavigationMetadataType.ELEVATION)
							? navData.getMetadata(NavigationMetadataType.ELEVATION, datetime)
							: 0.0;
					var cursor = new GlobalCursor().withSource(this).withPosition(lat, lon, elev, clampToGround)
							.withTime(datetime).withDescription(chartData.getTooltip());
					globalCursorService.submit(cursor);
				} catch (GIOException e) {
					LOGGER.debug("Error in global cursor management", e);
				}
			});
		});
		chart.addSelectionListener(this::onDataSelected);
		model.addListener(this::onModelEvent);
		globalCursorService.addListener(cursorServiceListener);

		reloadChartData();
		resetZoom();
	}

	private void onModelEvent(ChartViewerModelEvent event) {
		if (event.type() == ChartViewerModelEventType.SERIES_ORDER_CHANGED) {
			// remove all series to force a complete reload
			chart.setData(Collections.emptyList());
		} else if (event.type() == ChartViewerModelEventType.UPDATE_CHART_SERIES) {
			// remove updated series to force their reload (force the recompute of bounds because data coordinates may
			// have changed...)
			var chartsToDisplay = model.getAll().stream().filter(IChartSeries::isEnabled)
					.filter(series -> !event.series().contains(series)).toList();
			chart.setData(chartsToDisplay);
			updateDirty();
		}
		reloadChartData();
	}

	private void reloadChartData() {
		var chartsToDisplay = model.getAll().stream().filter(IChartSeries::isEnabled).toList();

		// check if X axis is time
		if (!chartsToDisplay.isEmpty() && chartsToDisplay.get(0).size() > 0
				&& chartsToDisplay.get(0).getData(0).getX() > 323883645) {
			chart.setXAxisTimeLabel();
			chart.setVerticalZoomEnabled(false);
		} else {
			chart.setXAxisLabelFormatter(d -> NumberUtils.getStringDouble(d.doubleValue()));
			chart.setVerticalZoomEnabled(true);
		}

		LOGGER.debug("Reload ChartEditor with {} series", chartsToDisplay.size());
		chart.setData(chartsToDisplay);
	}

	@Persist
	public void save(IProgressMonitor monitor) {
		doSave();
	}

	/**
	 * Dispose method
	 */
	@PreDestroy
	public void dispose(MApplication application, EPartService partService, MPart part) {
		model.close();
		globalCursorService.removeListener(cursorServiceListener);
		PartUtil.hidePart(application, partService, ChartEditorToolbox.PART_ID);
	}

	public ChartViewerModel getModel() {
		return model;
	}

	/**
	 * Resets chart zoom
	 */
	public void resetZoom() {
		chart.resetZoom();
	}

	public void setShowDisabledData(boolean b) {
		chart.setShowDisabledData(b);
		chart.refresh();
	}

	public EditionMode getEditionMode() {
		return editionMode;
	}

	public SelectionMode getSelectionMode() {
		return selectionMode;
	}

	public ChartCursor getCursor() {
		return chart.getCursor();
	}

	public void setCursor(ChartCursor cursor) {
		chart.setCursor(cursor);
	}

	/**
	 * Sets the selection mode
	 */
	public void setEditionMode(EditionMode mode) {
		this.editionMode = mode;
		updateChartSelectionMode();
	}

	public void setSelectionMode(FXChartController.SelectionMode mode) {
		this.selectionMode = mode;
		updateChartSelectionMode();
	}

	private void updateChartSelectionMode() {
		switch (editionMode) {
		case VALIDATE -> chart.setSelectionMode(selectionMode, Color.FORESTGREEN);
		case INVALIDATE -> chart.setSelectionMode(selectionMode, Color.INDIANRED);
		case ZOOM -> chart.setSelectionMode(SelectionMode.ZOOM);
		}
	}

	private void onDataSelected(List<IChartData> data) {
		switch (editionMode) {
		case VALIDATE -> {
			data.forEach(d -> d.setEnabled(true));
			updateDirty();
		}
		case INVALIDATE -> {
			data.forEach(d -> d.setEnabled(false));
			updateDirty();
		}
		case ZOOM -> { // nothing to do
		}
		}
		chart.refresh();
	}

	public OrganizationMode getOrganizationMode() {
		return organizationMode;
	}

	public void setOrganizationMode(OrganizationMode organizationMode) {
		this.organizationMode = organizationMode;

		// update legend composites
		if (legendParentComposite != null) {
			Stream.of(legendParentComposite.getChildren()).forEach(Control::dispose);
			legendParentComposite.dispose();
		}

		// if switch to "organize by variable" : get one legend max per variable
		if (organizationMode == OrganizationMode.ORGANIZE_BY_VARIABLE) {
			var disctinctSeriesList = new ArrayList<String>();
			model.getEnabledSeries().forEach(series -> {
				if (disctinctSeriesList.contains(series.getTitle()))
					series.setEnabled(false);
				else
					disctinctSeriesList.add(series.getTitle());
			});
		}

		legendParentComposite = organizationMode == OrganizationMode.ORGANIZE_BY_FILE
				? new FileLegendGroupComposite(parent, SWT.NONE, model)
				: new VariableLegendGroupComposite(parent, SWT.NONE, model);
		legendParentComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		legendParentComposite.layout();
		legendParentComposite.getParent().layout();
	}

	public void updateDirty() {
		part.setDirty(model.isDirty());
		eventBroker.send(UIEvents.REQUEST_ENABLEMENT_UPDATE_TOPIC, UIEvents.ALL_ELEMENT_ID); // updates save icon
	}

	public void doSave() {
		IProcessService.grab().runInBackground("Save Chart Editor...", (monitor, logger) -> {
			var seriesToSave = model.getSaveableSeries();
			try {
				List<String> editedSeries = new ArrayList<>();
				// save series
				for (var series : seriesToSave)
					if (series instanceof LoadableChartSeries loadableSeries && loadableSeries.isDirty()) {
						monitor.setTaskName("Save  : " + loadableSeries.getTitle() + "...");
						loadableSeries.flush();
						loadableSeries.setDirty(false);
						editedSeries.add(loadableSeries.getTitle());
					}

				// keep only series with dirty status
				seriesToSave = seriesToSave.stream()
						.filter(s -> s.getStatusHandler().map(IChartSeriesStatusHandler::isDirty).orElse(false))
						.toList();

				// save status variable
				monitor.setTaskName("Save status...");
				seriesToSave.stream().map(IChartSeries::getStatusHandler).filter(Optional::isPresent).map(Optional::get)
						.distinct().filter(IChartSeriesStatusHandler::isDirty) // get dirty status handlers
						.forEach(IChartSeriesStatusHandler::flush);

				// update navigation layers
				seriesToSave.stream()
						// get associated navigation layers
						.map(IChartSeries::getSource).filter(ChartFileInfoDataSource.class::isInstance)
						.map(ChartFileInfoDataSource.class::cast).map(source -> source.getFileInfo())
						.map(fileInfo -> IWWLayerService.grab().getFileLayerStoreModel().get(fileInfo))
						.filter(Optional::isPresent).map(Optional::get)
						.flatMap(layerStore -> layerStore.getLayers(IWWNavigationLayer.class))
						// reload navigation layers
						.forEach(IWWNavigationLayer::reload);

				var resultMessage = "";
				if (!seriesToSave.isEmpty() && !editedSeries.isEmpty()) {
					var seriesListString = editedSeries.stream().collect(Collectors.joining(";"));
					resultMessage = "Status and variable" + (editedSeries.size() > 1 ? "s" : "") + " ("
							+ seriesListString + ") have been successfully saved.";
				} else if (!seriesToSave.isEmpty())
					resultMessage = "Status have been successfully saved.";
				else if (!editedSeries.isEmpty()) {
					var seriesListString = editedSeries.stream().collect(Collectors.joining(";"));
					resultMessage = "Variable" + (editedSeries.size() > 1 ? "s" : "") + " (" + seriesListString
							+ ") have been successfully saved.";
				}

				if (!resultMessage.isEmpty())
					Messages.openInfoMessage("Chart Editor", resultMessage);

				updateDirty();
				return Status.OK_STATUS;
			} catch (GIOException e) {
				return Status.error("Error with Chart Editor save : " + e.getMessage(), e);
			}
		});

	}

}