package fr.ifremer.globe.editor.chart.dnd;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import jakarta.inject.Inject;

import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.widgets.Control;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.services.IChartService;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.projectexplorer.dnd.NodeConverter;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Drop listener : allows to import data with a drag & drop.
 */
public class ChartViewerDropListener extends DropTargetAdapter {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChartViewerDropListener.class);

	@Inject
	private ProjectExplorerModel projectExplorerModel;

	private final ChartViewerModel model;

	/**
	 * Constructor
	 */
	public ChartViewerDropListener(ChartViewerModel chartViewerModel) {
		ContextInitializer.inject(this);
		this.model = chartViewerModel;
	}

	/**
	 * @param control
	 */
	public void addTo(Control control) {
		DropTarget target = new DropTarget(control, DND.DROP_NONE);
		target.setTransfer(TextTransfer.getInstance(), FileTransfer.getInstance());
		target.addDropListener(this);
	}

	@Override
	public void drop(DropTargetEvent event) {
		List<IFileInfo> fileInfos = null;

		// DND of node from project explorer
		if (event.data instanceof String dataString) {
			String[] uuids = NodeConverter.convert(dataString); // get dropped node UUIDs (universally unique
			try {
				fileInfos = Stream.of(uuids).map(uuid -> projectExplorerModel.getNode(UUID.fromString(uuid)))
						.filter(Optional::isPresent).map(Optional::get).filter(FileInfoNode.class::isInstance)
						.map(FileInfoNode.class::cast).map(FileInfoNode::getFileInfo).toList();
			} catch (IllegalArgumentException e) {
				LOGGER.debug("Illegal drop in chart viewer : {} ", e.getMessage(), e);
			}
		}
		// DND of file
		else if (FileTransfer.getInstance().isSupportedType(event.currentDataType))
			fileInfos = IFileService.grab().getFileInfos(List.of((String[]) event.data));

		// load dropped files
		if (fileInfos != null && !fileInfos.isEmpty())
			try {
				for (var fileInfo : fileInfos)
					model.addAll(IChartService.grab().getChartSeries(fileInfo));
			} catch (GIOException e) {
				Messages.openErrorMessage("Chart editor", "Error while loading file.", e);
			}

	}

}
