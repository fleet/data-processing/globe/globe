package fr.ifremer.globe.editor.chart.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel.ChartViewerModelEvent;

public class VariableLegendGroupComposite extends AbstractLegendGroupComposite {

	private Button btnNewVariable;

	private final AtomicBoolean isReceivingEvent = new AtomicBoolean();

	/**
	 * Constructor
	 */
	public VariableLegendGroupComposite(Composite parent, int style, ChartViewerModel model) {
		super(parent, style, model);
		RowLayout legendParentCompositeLayout = new RowLayout(SWT.HORIZONTAL);
		legendParentCompositeLayout.justify = true;
		setLayout(legendParentCompositeLayout);

		// button to add a new variable
		btnNewVariable = new Button(this, SWT.NONE);
		btnNewVariable.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.editor.time", "icons/add.png"));
		btnNewVariable.setText("New variable");
		btnNewVariable.addListener(SWT.Selection, e -> onNewVariable());

		// initialize widget(s) to select a variable (= series name) from enabled series
		onSeriesAdded(model.getAll());

		// no series enabled ? add one variable
		if (model.getAll().stream().noneMatch(IChartSeries::isEnabled))
			onNewVariable();

		// listen model
		Consumer<ChartViewerModelEvent> listener = this::onModelEvent;
		model.addListener(listener);
		addDisposeListener(e -> model.removeListener(listener));
	}

	private void onModelEvent(ChartViewerModelEvent event) {
		if (isDisposed())
			return;
		isReceivingEvent.set(true);
		switch (event.type()) {
		case ADD_CHART_SERIES -> onSeriesAdded(event.series());
		case UPDATE_CHART_SERIES -> onSeriesUpdated(event.series());
		case REMOVE_CHART_SERIES -> onSeriesRemoved(event.series());
		case SERIES_ORDER_CHANGED -> {
			/* nothing to do */ }
		}
		isReceivingEvent.set(false);
	}

	private void onSeriesAdded(List<IChartSeries<?>> series) {
		getDisplay().syncExec(() -> {
			// add legend composite for enabled series
			var alreadyAdded = new ArrayList<IChartSeries<? extends IChartData>>();
			streamLegendComposites().map(LegendComposite::getSelectedSeries).forEach(alreadyAdded::addAll);
			series.stream().filter(IChartSeries::isEnabled).filter(s -> !alreadyAdded.contains(s))
					.forEach(this::addLegendItem);
			streamLegendComposites().forEach(lc -> lc.setSeriesList(model.getAll()));
		});
	}

	private void onSeriesUpdated(List<IChartSeries<?>> series) {
		// update series lists
		getDisplay().syncExec(() -> streamLegendComposites().forEach(lc -> lc.setSeriesList(model.getAll())));
		// add legend for updated series which are enabled
		onSeriesAdded(series.stream().filter(IChartSeries::isEnabled).toList());
	}

	/**
	 * Removes obsolete legend composites.
	 */
	private void onSeriesRemoved(List<IChartSeries<?>> series) {
		streamLegendComposites()
				.filter(legendComposite -> legendComposite.getSelectedSeries().stream().allMatch(series::contains))
				.forEach(Control::dispose);
		layout();
		getParent().layout();
		streamLegendComposites().forEach(lc -> lc.setSeriesList(model.getAll()));
	}

	private void onNewVariable() {
		var series = model.getAll();
		if (!series.isEmpty()) {
			var newSelectedSeries = series.stream().filter(s -> !s.isEnabled()).findFirst().orElse(series.get(0));
			addLegendItem(newSelectedSeries);
			onSelectionUpdate();
		}
	}

	private void addLegendItem(IChartSeries<? extends IChartData> newSelectedSeries) {
		// create new legend composite
		newSelectedSeries.setColor(getAvailableColor());
		var newLegendItem = new LegendComposite(this, model, model.getAllSorted(), newSelectedSeries,
				lc -> onSelectionUpdate());
		newLegendItem.getSelectedSeries().addChangeListener(evt -> onSelectionUpdate());
		btnNewVariable.moveBelow(newLegendItem);
		layout();
		getParent().layout();
	}

	/**
	 * Computes which series to display or not.
	 */
	private void onSelectionUpdate() {
		if (!isReceivingEvent.get()) {
			var enabledSeries = streamLegendComposites() //
					.filter(LegendComposite::isCheckboxEnabled)//
					.flatMap(item -> item.getSelectedSeries().stream())//
					.distinct()//
					.toList();

			model.getAll().stream().filter(series -> series.isEnabled() != enabledSeries.contains(series))
					.forEach(seriesToUpdate -> {
						seriesToUpdate.setEnabled(enabledSeries.contains(seriesToUpdate));
						model.notifyUpdate(seriesToUpdate);
					});
		}
	}

}
