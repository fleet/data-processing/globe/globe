package fr.ifremer.globe.editor.chart.ui;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.IntStream;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.nebula.widgets.ctreecombo.CTreeCombo;
import org.eclipse.nebula.widgets.ctreecombo.CTreeComboItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.impl.LoadableChartSeries2D;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.chart.dialog.saveascsv.SaveWizard;
import fr.ifremer.globe.editor.chart.dialog.toolbox.ChartEditorToolHistory;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.image.ImageResources;
import fr.ifremer.globe.ui.widget.color.ColorPicker;
import fr.ifremer.globe.utils.exception.GIOException;

public class LegendComposite extends Composite {

	/** Key used to store the title of series in CTreeComboItem data */
	private static final String SERIES_TITLE_IN_DATA = "SERIES_TITLE_IN_DATA";

	private final ChartViewerModel model;

	/** Combo list series **/
	private List<IChartSeries<? extends IChartData>> seriesList;

	/** Observable list of selected series **/
	private final WritableObjectList<IChartSeries<? extends IChartData>> selectedSeries = new WritableObjectList<>();

	/** Callback to run after a delete **/
	private final Consumer<LegendComposite> deleteCallback;

	/** Widget **/
	private final ColorPicker colorPicker;
	private final Button checkbox;
	private final ComboViewer comboViewerSecondIndex;
	private final CTreeCombo treeCombo;
	private final Label lblSecondIndex;
	private MenuItem mntmApplyToOrigin;
	private final Menu actionMenu;

	/**
	 * Constructor
	 */
	public LegendComposite(Composite parent, ChartViewerModel model,
			List<IChartSeries<? extends IChartData>> seriesList, IChartSeries<? extends IChartData> selection,
			Consumer<LegendComposite> deleteCallback) {
		this(parent, model, seriesList, selection, deleteCallback, "");
	}

	/**
	 * Constructor
	 * 
	 * @wbp.parser.constructor
	 */
	public LegendComposite(Composite parent, ChartViewerModel model,
			List<IChartSeries<? extends IChartData>> seriesList, IChartSeries<? extends IChartData> selection,
			Consumer<LegendComposite> deleteCallback, String subtitle) {
		super(parent, SWT.NONE);
		this.model = model;
		setSeriesList(seriesList);
		this.deleteCallback = deleteCallback;

		GridLayout gridLayout = new GridLayout(4, false);
		gridLayout.verticalSpacing = 1;
		setLayout(gridLayout);

		// color picker
		colorPicker = new ColorPicker(this, SWT.None, GColor.DEFAULT_BLUE, newColor -> {
			selectedSeries.forEach(s -> s.setColor(newColor));
			model.notifyUpdate();
		});
		GridData gdColorPicker = new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1);
		gdColorPicker.heightHint = 6;
		colorPicker.setLayoutData(gdColorPicker);
		colorPicker.setColor(selection.getColor());

		// subtitle
		if (!subtitle.isEmpty()) {
			Label bottomLabel = new Label(this, SWT.WRAP);
			bottomLabel.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BORDER));
			bottomLabel.setFont(SWTResourceManager.getFont("Segoe UI", 8, SWT.ITALIC));
			bottomLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
			bottomLabel.setText(subtitle);
		}

		// checkbox & label
		checkbox = new Button(this, SWT.CHECK);
		checkbox.setSelection(true);
		checkbox.addListener(SWT.Selection, e -> updateSelectedSeries());

		// variable combo box
		treeCombo = new CTreeCombo(this, SWT.READ_ONLY | SWT.BORDER);
		updateComboList();
		computeTreeComboLayouData();

		// contextual menu
		Button menuButton = new Button(this, SWT.NONE);
		menuButton.setImage(ImageResources.getImage("icons/16/view_menu.png"));
		actionMenu = new Menu(menuButton);
		menuButton.setMenu(actionMenu);
		menuButton.addListener(SWT.Selection, e -> actionMenu.setVisible(true));

		MenuItem mntmSaveAsCsv = new MenuItem(actionMenu, SWT.NONE);
		mntmSaveAsCsv
				.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.editor.time", "icons/document-save.png"));
		mntmSaveAsCsv.setText("Save as CSV");
		mntmSaveAsCsv.addListener(SWT.Selection,
				e -> new SaveWizard(model).open(Display.getDefault().getActiveShell(), getSelectedSeries().asList()));

		MenuItem mntmMoveToFront = new MenuItem(actionMenu, SWT.NONE);
		mntmMoveToFront.setImage(ImageResources.getImage("icons/16/move_up_2.png"));
		mntmMoveToFront.setText("Move to front");
		mntmMoveToFront.addListener(SWT.Selection, evt -> getSelectedSeries().asList().forEach(model::moveToFront));

		MenuItem mntmDelete = new MenuItem(actionMenu, SWT.NONE);
		mntmDelete.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.editor.time", "icons/delete.png"));
		mntmDelete.setText("Delete");
		mntmDelete.addListener(SWT.Selection, evt -> delete());

		// second index
		lblSecondIndex = new Label(this, SWT.NONE);
		lblSecondIndex.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 2, 1));
		lblSecondIndex.setText("Second dimension index : ");

		comboViewerSecondIndex = new ComboViewer(this, SWT.READ_ONLY);
		Combo comboSecondDimension = comboViewerSecondIndex.getCombo();
		comboSecondDimension.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		comboViewerSecondIndex.setContentProvider(ArrayContentProvider.getInstance());

		// hook selection listener
		treeCombo.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> updateSelectedSeries()));
		setSelection(selection.getTitle());
		updateSelectedSeries();
		comboViewerSecondIndex.addSelectionChangedListener(this::onSecondIndexChanged);
	}

	private void computeTreeComboLayouData() {
		GridData gdTreeCombo = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gdTreeCombo.widthHint = 100;
		GC gc = new GC(treeCombo);
		for (var chartSeries : getSeries()) {
			String[] path = inferSeriesPath(chartSeries);
			if (path.length > 0)
				gdTreeCombo.widthHint = Math.max(gc.stringExtent(path[path.length - 1]).x, gdTreeCombo.widthHint);
		}
		gc.dispose();
		gdTreeCombo.widthHint += 50;
		treeCombo.setLayoutData(gdTreeCombo);
	}

	private void setApplyMenuItemVisible(boolean visible) {
		if (visible && mntmApplyToOrigin == null) {
			mntmApplyToOrigin = new MenuItem(actionMenu, SWT.NONE);
			mntmApplyToOrigin.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.ui", "icons/16/bullet_go.png"));
			mntmApplyToOrigin.setText("Apply to origin series");
			mntmApplyToOrigin.addListener(SWT.Selection, e -> applyToOrigin());
		} else if (!visible && mntmApplyToOrigin != null) {
			mntmApplyToOrigin.dispose();
			mntmApplyToOrigin = null;
		}
	}

	private void delete() {
		if (!isDisposed()) {
			var parent = getParent();
			dispose();
			parent.layout();
			parent.getParent().layout();
			deleteCallback.accept(this);
		}
	}

	/** Return the title of the selected series in treeCombo */
	public Optional<String> getSelection() {
		CTreeComboItem[] selection = treeCombo.getSelection();
		if (selection != null && selection.length > 0)
			return Optional.ofNullable((String) selection[0].getData(SERIES_TITLE_IN_DATA));
		return Optional.empty();
	}

	/** Select the CTreeComboItem with the specified seriesTitle in data */
	public void setSelection(String seriesTitle) {
		if (seriesTitle != null) {
			setSelection(seriesTitle, treeCombo.getItems());
			updateSelectedSeries();
		}
	}

	/** Recursive method searching the CTreeComboItem with seriesTitle in data and select it in treeCombo */
	private void setSelection(String seriesTitle, CTreeComboItem[] comboItems) {
		if (comboItems == null || comboItems.length == 0)
			return;

		for (CTreeComboItem comboItem : comboItems)
			if (seriesTitle.equals(comboItem.getData(SERIES_TITLE_IN_DATA)))
				treeCombo.select(comboItem);
			else
				setSelection(seriesTitle, comboItem.getItems());
	}

	private void updateSelectedSeries() {
		treeCombo.getParent().layout(true);

		selectedSeries.clear();
		// select all series with the selected title
		if (checkbox.isDisposed() || checkbox.getSelection()) {
			Optional<String> selection = getSelection();
			if (selection.isPresent()) {
				var seriesToSelect = getSeries().stream().filter(s -> s.getTitle().equals(selection.get())).toList();
				seriesToSelect.stream().filter(LoadableChartSeries2D.class::isInstance).findAny()
						.map(LoadableChartSeries2D.class::cast).ifPresentOrElse(
								// 2D variable : setup second index combo viewer
								series2D -> {
									lblSecondIndex.setVisible(true);
									comboViewerSecondIndex.getCombo().setVisible(true);
									comboViewerSecondIndex
											.setInput(IntStream.range(0, series2D.getSecondDimSize()).boxed().toList());
									comboViewerSecondIndex.getCombo().select(0);
								},
								// 1D variable : hide second index selection
								() -> {
									lblSecondIndex.setVisible(false);
									comboViewerSecondIndex.getCombo().setVisible(false);
								});
				selectedSeries.addAll(seriesToSelect);
				selectedSeries.forEach(s -> s.setColor(colorPicker.getColor()));
				setApplyMenuItemVisible(
						selectedSeries.stream().anyMatch(s -> s.getSource() instanceof ChartEditorToolHistory));
			}
		}
	}

	private void onSecondIndexChanged(SelectionChangedEvent event) {
		selectedSeries.stream().filter(LoadableChartSeries2D.class::isInstance).map(LoadableChartSeries2D.class::cast)
				.forEach(s -> {
					try {
						s.setSecondIndex((int) comboViewerSecondIndex.getStructuredSelection().getFirstElement());
						model.notifyUpdate();
					} catch (GIOException | ClassCastException e) {
						Messages.openErrorMessage("Error while loading data for " + s.getTitle(), e.getMessage());
					}
				});
	}

	private void updateComboList() {
		// Get title of current selected series
		Optional<String> selectedSeriesTitle = getSelection();

		// Creates all CTreeComboItem
		treeCombo.clearAll(true);
		for (IChartSeries<? extends IChartData> series : getSeries()) {
			String[] path = inferSeriesPath(series);
			if (path.length > 0) {
				addComboItem(series, path, null);
			}
		}

		// Restore selection
		if (selectedSeriesTitle.isPresent())
			setSelection(selectedSeriesTitle.get());
	}

	/** Infer the path of the series */
	private String[] inferSeriesPath(IChartSeries<? extends IChartData> series) {
		String title = series.getTitle();
		// Removing leading slash
		if (title.startsWith("/"))
			title = title.substring(1);

		// Removing unit if any (can contain / like m/s, dB/km...)
		int startBracket = title.indexOf('[');
		String unit = "";
		if (startBracket > 0 && title.endsWith("]")) {
			unit = title.substring(startBracket);
			title = title.substring(0, startBracket);
		}

		// Splitting to compose the path
		String[] result = title.split("/");
		// Add unit to the last item
		result[result.length - 1] = result[result.length - 1] + unit;

		return result;
	}

	/** Recursive method creating a CTreeComboItem to store the series in the CTreeCombo */
	private void addComboItem(IChartSeries<? extends IChartData> series, String[] path, CTreeComboItem parentItem) {
		// Check if path[0] already exists among children of parent item or creates a new one
		CTreeComboItem[] children = parentItem != null ? parentItem.getItems() : treeCombo.getItems();
		CTreeComboItem result = Arrays.stream(children)//
				.filter(item -> item.getText().equals(path[0]))//
				.findFirst()//
				.orElseGet(() -> parentItem != null ? new CTreeComboItem(parentItem, SWT.NONE)
						: new CTreeComboItem(treeCombo, SWT.NONE));
		result.setText(path[0]);
		result.setExpanded(true);

		// Leaf ?
		if (path.length == 1)
			result.setData(SERIES_TITLE_IN_DATA, series.getTitle());
		else
			// Next level
			addComboItem(series, Arrays.copyOfRange(path, 1, path.length), result);
	}

	public List<IChartSeries<? extends IChartData>> getSeries() {
		return seriesList;
	}

	public void setSeriesList(List<IChartSeries<? extends IChartData>> seriesList) {
		if (this.seriesList == null || seriesList.size() != this.seriesList.size()
				|| !seriesList.stream().allMatch(this.seriesList::contains)) {
			this.seriesList = seriesList;
			if (treeCombo != null)
				updateComboList();
		}
	}

	public WritableObjectList<IChartSeries<? extends IChartData>> getSelectedSeries() {
		selectedSeries.forEach(s -> s.setColor(colorPicker.getColor()));
		return selectedSeries;
	}

	public GColor getColor() {
		return colorPicker.getColor();
	}

	public void setColor(GColor color) {
		colorPicker.setColor(color);
	}

	public boolean isCheckboxEnabled() {
		return checkbox.getSelection();
	}

	private void applyToOrigin() {
		selectedSeries.stream().forEach(s -> {
			if (s.getSource() instanceof ChartEditorToolHistory chartEditorToolHistory) {
				var originSeries = chartEditorToolHistory.originSeries();
				s.forEach(d -> {
					var originData = originSeries.getData(d.getIndex());
					if (originData.getY() != d.getY())
						originData.setY(d.getY());
					if (originData.isEnabled() != d.isEnabled())
						originData.setEnabled(d.isEnabled());
				});
				delete();
			}
		});
		model.notifyUpdate();
	}

}
