package fr.ifremer.globe.editor.chart.ui;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel.ChartViewerModelEvent;

public class FileLegendGroupComposite extends AbstractLegendGroupComposite {

	/**
	 * Constructor
	 */
	public FileLegendGroupComposite(Composite parent, int style, ChartViewerModel model) {
		super(parent, style, model);

		RowLayout legendParentCompositeLayout = new RowLayout(SWT.HORIZONTAL);
		legendParentCompositeLayout.justify = true;
		setLayout(legendParentCompositeLayout);
		onSeriesAdded(model.getAllSorted());
		model.notifyUpdate();

		// listen model
		Consumer<ChartViewerModelEvent> listener = this::onModelEvent;
		model.addListener(listener);
		addDisposeListener(e -> model.removeListener(listener));
	}

	private void onModelEvent(ChartViewerModelEvent event) {
		if (isDisposed())
			return;
		switch (event.type()) {
		case ADD_CHART_SERIES -> onSeriesAdded(event.series());
		case REMOVE_CHART_SERIES -> onSeriesRemoved(event.series());
		case UPDATE_CHART_SERIES -> {
			onSeriesRemoved(event.series());
			onSeriesAdded(event.series());
		}
		default -> { // nothing
		}
		}
	}

	/**
	 * Handles new series.
	 */
	private void onSeriesAdded(List<IChartSeries<?>> series) {
		getDisplay().syncExec(() -> {
			// one legend composite by source (=file)
			series.stream().collect(Collectors.groupingBy(IChartSeries::getSource)) //
					.forEach((source, seriesList) -> {
						var selection = seriesList.stream().filter(IChartSeries::isEnabled).findFirst()
								.orElse(seriesList.get(0));

						// If a legend composite already exists for this source, use it
						var optLegendComposite = streamLegendComposites().filter(
								legend -> legend.getSeries().stream().anyMatch(s -> s.getSource().equals(source)))
								.findFirst();
						if (optLegendComposite.isPresent()) {
							var legend = optLegendComposite.get();
							legend.setSeriesList(model.getBySource(source).orElseThrow());
							legend.setSelection(selection.getTitle());
							return;
						}

						// Else, create a new legend composite
						var title = selection.getSource().getId();
						seriesList.forEach(s -> s.setEnabled(s == selection));
						selection.setColor(getAvailableColor());
						var legendComposite = new LegendComposite(this, model, seriesList, selection,
								lc -> model.removeAll(lc.getSeries()), title);
						legendComposite.getSelectedSeries().addChangeListener(evt -> {
							var currentSelectedSeries = legendComposite.getSelectedSeries().asList();
							legendComposite.getSeries().forEach(s -> s.setEnabled(currentSelectedSeries.contains(s)));
							model.notifyUpdate();
						});
					});
			layout();
			getParent().layout();
		});
	}

	/**
	 * Removes obsolete legend composites.
	 */
	private void onSeriesRemoved(List<IChartSeries<?>> series) {
		getDisplay().syncExec(() -> {
			streamLegendComposites()
					.filter(legendComposite -> legendComposite.getSeries().isEmpty()
							|| legendComposite.getSeries().stream().noneMatch(model::contains))
					.forEach(Control::dispose);

			streamLegendComposites()
					.filter(legendComposite -> !legendComposite.getSelectedSeries().isEmpty()
							&& (legendComposite.getSelectedSeries().stream().allMatch(series::contains)))
					.forEach(Control::dispose);
			layout();
			getParent().layout();
		});
	}

}
