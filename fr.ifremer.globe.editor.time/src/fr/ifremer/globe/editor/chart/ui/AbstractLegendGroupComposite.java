package fr.ifremer.globe.editor.chart.ui;

import java.util.stream.Stream;

import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.utils.color.ColorProvider;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel;

public class AbstractLegendGroupComposite extends Composite {

	protected final ChartViewerModel model;

	private final ColorProvider colorProvider = new ColorProvider();

	/** Constructor **/
	protected AbstractLegendGroupComposite(Composite parent, int style, ChartViewerModel model) {
		super(parent, style);
		this.model = model;
	}

	protected Stream<LegendComposite> streamLegendComposites() {
		return Stream.of(getChildren()).filter(LegendComposite.class::isInstance).map(LegendComposite.class::cast);
	}

	protected GColor getAvailableColor() {
		var usedColors = streamLegendComposites().map(LegendComposite::getColor).toList();
		return colorProvider.getColor(c -> !usedColors.contains(c)).orElse(colorProvider.get());
	}

}
