package fr.ifremer.globe.editor.chart.handlers;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.editor.chart.ChartViewer;
import fr.ifremer.globe.editor.chart.dialog.calculator.CalculatorDialog;

public class OpenCalculatorDialogHandler {

	@Execute
	public void execute(@Active Shell shell, MPart part) {
		new CalculatorDialog(shell, ((ChartViewer) part.getObject()).getModel()).open();
	}

}
