package fr.ifremer.globe.editor.chart.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.chart.ChartViewer;
import fr.ifremer.globe.editor.chart.ChartViewer.EditionMode;

public class SelectionZoomModeHandler {

	@Execute
	public void execute(EPartService partService) {
		ChartViewer part = (ChartViewer) partService.getActivePart().getObject();
		part.setEditionMode(EditionMode.ZOOM);
	}

}
