package fr.ifremer.globe.editor.chart.handlers;

import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.chart.ChartViewer;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.ChartCursor;

public class CursorTypeHandler {

	private AtomicBoolean initDone = new AtomicBoolean();

	@CanExecute
	public boolean init(EPartService partService, MItem item) {
		if (initDone.compareAndSet(false, true)) {
			var chartViewer = (ChartViewer) partService.getActivePart().getObject();
			var cursor = chartViewer.getCursor();
			var isPointSelected = item.getLabel().contains("Point");
			item.setSelected((isPointSelected && cursor == ChartCursor.POINT_CURSOR)
					|| (!isPointSelected && cursor == ChartCursor.LINE_CURSOR));
		}
		return true;
	}

	@Execute
	public void execute(EPartService partService, @Active MItem item) {
		ChartViewer part = (ChartViewer) partService.getActivePart().getObject();
		part.setCursor(item.getLabel().contains("Point") ? ChartCursor.POINT_CURSOR : ChartCursor.LINE_CURSOR);
	}

}
