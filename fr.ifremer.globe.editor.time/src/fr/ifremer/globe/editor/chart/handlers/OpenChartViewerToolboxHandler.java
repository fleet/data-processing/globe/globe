package fr.ifremer.globe.editor.chart.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.editor.chart.toolbox.CharToolboxPart;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.parts.PartUtil;

/**
 * Handler of "Show / Hide toolbox" command.
 */
public class OpenChartViewerToolboxHandler {

	/** Id of command defined in fragment.e4xmi */
	public static final String COMMAND_SHOW_SOUNDINGS_TOOLBOX = "fr.ifremer.globe.editor.chart.command.opencharttoolbox";

	/**
	 * Execution of this handler.
	 */
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_PART) MPart part, MApplication application,
			EPartService partService, EModelService modelService) {
		Display.getDefault().syncExec(() -> {
			MPart toolboxView = PartUtil.findPart(application, partService, CharToolboxPart.PART_ID);
			// If part doesn't exist : create it
			if (toolboxView == null) {
				toolboxView = partService.createPart(CharToolboxPart.PART_ID);
				modelService.findElements(application, PartManager.RIGHT_RIGHT_STACK_ID, MPartStack.class, null).get(0)
						.setVisible(true);
				PartManager.addPartToStack(toolboxView, application, modelService, PartManager.RIGHT_RIGHT_STACK_ID);
				PartUtil.activatePart(application, partService, CharToolboxPart.PART_ID);
			}
			// If part exists but it's not displayed: show it
			else if (!toolboxView.isToBeRendered())
				PartUtil.activatePart(application, partService, toolboxView);
			// If part exists and it's visible: hide it
			else {
				PartUtil.hidePart(application, partService, toolboxView);
			}
		});
	}

}