package fr.ifremer.globe.editor.chart.handlers;

import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.chart.ChartViewer;
import fr.ifremer.globe.editor.chart.ChartViewer.OrganizationMode;

public class OrganizeHandler {

	private AtomicBoolean initDone = new AtomicBoolean();

	@CanExecute
	public boolean init(EPartService partService, MItem item) {
		if (initDone.compareAndSet(false, true)) {
			var chartViewer = (ChartViewer) partService.getActivePart().getObject();
			var mode = chartViewer.getOrganizationMode();
			var isFileMenuItem = item.getLabel().contains("file");
			item.setSelected((isFileMenuItem && mode == OrganizationMode.ORGANIZE_BY_FILE)
					|| (!isFileMenuItem && mode == OrganizationMode.ORGANIZE_BY_VARIABLE));
		}
		return true;
	}

	@Execute
	public void execute(EPartService partService, @Active MItem item) {
		var chartViewer = (ChartViewer) partService.getActivePart().getObject();
		chartViewer.setOrganizationMode(item.getLabel().contains("file") ? OrganizationMode.ORGANIZE_BY_FILE
				: OrganizationMode.ORGANIZE_BY_VARIABLE);
	}

}
