package fr.ifremer.globe.editor.chart.handlers;

import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.chart.services.IChartService;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.chart.ChartViewer;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.parts.PartUtil;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Handler to open the {@link ChartViewer}.
 */
public class OpenChartViewerHandler extends AbstractNodeHandler {

	@Inject
	private IChartService chartService;

	private MItem callerItem;

	/**
	 * @return true if the handler is called from the project explorer
	 */
	private boolean openedFromProjectExplorer() {
		return callerItem.getParent().getElementId().equals("fr.ifremer.globe.ui.projectexplorer.popupmenu.openwith");
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return !openedFromProjectExplorer()
				|| !getSelectionAsList(getSelection(selectionService), IChartService.grab()::isAccepted).isEmpty();
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService, EPartService partService,
			@Active MItem item) {
		this.callerItem = item;
		return checkExecution(selectionService, modelService);
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, ESelectionService selectionService,
			MApplication application, EModelService modelService, EPartService partService, @Active MItem item) {

		if (!openedFromProjectExplorer()) // Else open a FileDialog to select input files
			return; // TODO implement a generic class to load files (and use it with Line / Navigation editors)

		List<IFileInfo> inputFileInfos = getSelectionAsList(getSelection(selectionService), chartService::isAccepted)
				.stream().map(FileInfoNode::getFileInfo).toList();

		// Open part
		if (!inputFileInfos.isEmpty()) {
			IProcessService.grab().runInForeground("Open Chart Editor...", (monitor, logger) -> {

				// 1) load input files
				monitor.beginTask("Load input files...", 3 * inputFileInfos.size());
				monitor.worked(inputFileInfos.size());
				var model = new ChartViewerModel();
				try {
					for (var fileInfo : inputFileInfos) {
						monitor.subTask("Load file : " + fileInfo.getFilename() + "...");
						model.addAll(chartService.getChartSeries(fileInfo));
						monitor.worked(1);
					}

				} catch (GIOException e) {
					model.close();
					return Status.error("Error while loading file from status", e);
				}

				// 2) open editor

				// create chart viewer
				monitor.subTask("Open Chart Editor...");
				MPart part = partService.createPart(ChartViewer.PART_ID);
				IEclipseContext context = application.getContext();
				context.set("chartViewerModel", model);

				// open and show part
				Display.getDefault().asyncExec(() -> {
					PartManager.addEditableBindingContextToPart(part, application);
					modelService.findElements(application, PartManager.RIGHT_BOTTOM_STACK_ID, MPartStack.class, null)
							.get(0).setVisible(true);

					PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_BOTTOM_STACK_ID);
					PartUtil.activatePart(application, partService, part);
				});

				return Status.OK_STATUS;
			});

		}
	}

}