package fr.ifremer.globe.editor.chart.handlers;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;

import fr.ifremer.globe.editor.chart.ChartViewer;

public class ShowDisabledDataHandler {

	@Execute
	public void execute(MPart part, @Active MItem item) {
		((ChartViewer) part.getObject()).setShowDisabledData(item.isSelected());
	}

}
