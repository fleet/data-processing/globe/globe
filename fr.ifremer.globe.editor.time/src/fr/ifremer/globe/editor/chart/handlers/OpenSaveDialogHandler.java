package fr.ifremer.globe.editor.chart.handlers;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.editor.chart.ChartViewer;
import fr.ifremer.globe.editor.chart.dialog.saveascsv.SaveWizard;

public class OpenSaveDialogHandler {

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, MPart part) {
		new SaveWizard(((ChartViewer) part.getObject()).getModel()).open(shell);
	}

}
