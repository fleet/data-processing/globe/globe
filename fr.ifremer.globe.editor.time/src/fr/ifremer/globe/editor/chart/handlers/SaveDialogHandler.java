package fr.ifremer.globe.editor.chart.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

import fr.ifremer.globe.editor.chart.ChartViewer;

public class SaveDialogHandler {

	@CanExecute
	public boolean canExecute(MPart part) {
		return ((ChartViewer) part.getObject()).getModel().isDirty();
	}

	@Execute
	public void execute(MPart part) {
		((ChartViewer) part.getObject()).doSave();
	}

}
