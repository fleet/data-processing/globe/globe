package fr.ifremer.globe.editor.chart.handlers;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.editor.chart.ChartViewer;
import fr.ifremer.globe.editor.chart.dialog.toolbox.ChartEditorToolbox;
import fr.ifremer.globe.ui.parts.PartUtil;

public class OpenToolsDialogHandler {

	@Execute
	public void execute(@Active Shell shell, MPart part, EPartService partService, EModelService modelService) {
		var toolboxPart = (ChartEditorToolbox) PartUtil
				.openDetachedPart(partService, modelService, ChartEditorToolbox.PART_ID, 600, 550).getObject();
		toolboxPart.initialize(((ChartViewer) part.getObject()).getModel());
	}

}
