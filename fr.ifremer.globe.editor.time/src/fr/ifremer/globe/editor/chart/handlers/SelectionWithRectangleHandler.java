package fr.ifremer.globe.editor.chart.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.chart.ChartViewer;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.SelectionMode;

public class SelectionWithRectangleHandler {

	@Execute
	public void execute(EPartService partService) {
		ChartViewer part = (ChartViewer) partService.getActivePart().getObject();
		part.setSelectionMode(SelectionMode.RECTANGLE_SELECTION);
	}

}
