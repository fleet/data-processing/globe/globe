package fr.ifremer.globe.editor.chart.dialog.toolbox.tools;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.lang.math.LongRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.editor.chart.dialog.toolbox.ChartEditorToolComposite;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.validation.FileValidator;
import fr.ifremer.globe.ui.widget.FileComposite;
import fr.ifremer.globe.utils.exception.FileFormatException;
import fr.ifremer.globe.utils.exception.GException;

public class CutFileFilterTool extends ChartEditorToolComposite {

	public static final String NAME = "Filter with cut file (.cut)";

	private IChartSeries<? extends IChartData> inputSeries;
	private WritableFile cutFile = new WritableFile();

	public CutFileFilterTool(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		Label lblNewLabel = new Label(this, SWT.WRAP);
		lblNewLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		lblNewLabel.setText("Filters value outside cut lines");

		// Validity mode
		Label lblRobustnessIters = new Label(this, SWT.NONE);
		lblRobustnessIters.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblRobustnessIters.setText("Cut file (.cut)");

		new FileComposite(this, SWT.NONE, cutFile, FileValidator.FILE);
	}

	@Override
	public String getDisplayName() {
		return NAME;
	}

	@Override
	public void setInput(IChartSeries<? extends IChartData> inputSeries) {
		this.inputSeries = inputSeries;
	}

	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		if (inputSeries == null)
			return Status.CANCEL_STATUS;
		if (cutFile.isNull() || !cutFile.get().exists())
			return Status.error("Missing or invalid cut file.");
		monitor.setTaskName("Apply filter...");

		// CutLine
		try {
			var cutLines = new ArrayList<LongRange>();
			ProfileUtils.loadProfilesFromFile(cutFile.get())
					.forEach(p -> cutLines.add(new LongRange(p.getStartDate(), p.getEndDate())));
			logger.debug("Input series : {}", inputSeries.getTitle());
			logger.debug("Cut file read with {} lines", cutLines.size());
			monitor.beginTask("Filter data...", inputSeries.size());
			inputSeries.stream().peek(data -> monitor.worked(1))
					.filter(data -> cutLines.stream().noneMatch(range -> range.containsDouble(data.getX())))
					.forEach(data -> data.setEnabled(false));
		} catch (FileFormatException | IOException e) {
			e.printStackTrace();
		}
		return Status.OK_STATUS;
	}

}
