package fr.ifremer.globe.editor.chart.dialog.toolbox.tools;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.editor.chart.dialog.toolbox.ChartEditorToolComposite;
import fr.ifremer.globe.ui.widget.inputs.DoubleWidget;
import fr.ifremer.globe.utils.exception.GException;

public class LinearTransformTool extends ChartEditorToolComposite {

	public static final String NAME = "Linear transform";

	private IChartSeries<? extends IChartData> inputSeries;

	/** Inner widgets **/
	private DoubleWidget addOffsetWidget;
	private DoubleWidget scaleFactorWidget;

	/**
	 * Constructor
	 */
	public LinearTransformTool(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		Label lblNewLabel = new Label(this, SWT.WRAP);
		lblNewLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblNewLabel.setText("Define parameters to apply a linear transformation.");

		Group grpTransformY = new Group(this, SWT.NONE);
		grpTransformY.setLayout(new GridLayout(4, false));
		grpTransformY.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpTransformY.setText("Transform Y");

		Label lblScaleFactor = new Label(grpTransformY, SWT.NONE);
		lblScaleFactor.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblScaleFactor.setText("Scale factor : ");

		scaleFactorWidget = new DoubleWidget(grpTransformY, SWT.NONE, 1.0);
		GridData gd_scaleFactorWidget = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_scaleFactorWidget.widthHint = 128;
		scaleFactorWidget.setLayoutData(gd_scaleFactorWidget);

		Label lblAddOffset = new Label(grpTransformY, SWT.NONE);
		lblAddOffset.setText("Add offset : ");

		addOffsetWidget = new DoubleWidget(grpTransformY, SWT.NONE);
		GridData gd_addOffsetWidget = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_addOffsetWidget.widthHint = 128;
		addOffsetWidget.setLayoutData(gd_addOffsetWidget);

		Label lblHelp = new Label(grpTransformY, SWT.NONE);
		lblHelp.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lblHelp.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
		lblHelp.setText("y  = y * scale_factor + add_offset ");
	}

	@Override
	public String getDisplayName() {
		return NAME;
	}

	@Override
	public void setInput(IChartSeries<? extends IChartData> inputSeries) {
		this.inputSeries = inputSeries;
	}

	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		if (inputSeries == null)
			return Status.CANCEL_STATUS;
		monitor.setTaskName("Apply filter...");

		var yAddOffset = addOffsetWidget.get().get();
		var yAddOffsetValid = !Double.isNaN(yAddOffset) && yAddOffset != 0.0d;
		var yScaleFactor = scaleFactorWidget.get().get();
		var yScaleFactorValid = !Double.isNaN(yScaleFactor) && yScaleFactor != 1.0d;

		logger.debug("Apply linear tranform with : add_offset = {} and scale_factor = {} ", yAddOffset, yScaleFactor);

		for (var data : inputSeries) {
			// modify Y
			if (yAddOffsetValid && yScaleFactorValid)
				data.setY(data.getY() * yScaleFactor + yAddOffset);
			else if (yAddOffsetValid)
				data.setY(data.getY() + yAddOffset);
			else if (yScaleFactorValid)
				data.setY(data.getY() * yScaleFactor);
		}
		return Status.OK_STATUS;
	}
}
