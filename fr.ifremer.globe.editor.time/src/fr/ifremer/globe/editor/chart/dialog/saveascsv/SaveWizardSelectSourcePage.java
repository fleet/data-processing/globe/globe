package fr.ifremer.globe.editor.chart.dialog.saveascsv;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

/**
 * Wizard page used to define the CSV export options.
 */
public class SaveWizardSelectSourcePage extends WizardPage {

	private SaveWizardModel model;

	/**
	 * Constructor
	 */
	public SaveWizardSelectSourcePage(SaveWizardModel model) {
		super(SaveWizardSelectSourcePage.class.getName(), "Select variable(s) to save", null);
		this.model = model;
	}

	/**
	 * Creates the design of the input files selection page
	 */
	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout glComposite = new GridLayout(1, false);
		composite.setLayout(glComposite);
		setControl(composite);

		Group grpFiles = new Group(composite, SWT.NONE);
		grpFiles.setLayout(new GridLayout(1, false));
		grpFiles.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpFiles.setText("File to save");

		model.getSelectionBySource().forEach((source, selected) -> {
			Button btnCheckButton = new Button(grpFiles, SWT.CHECK);
			btnCheckButton.setText(source.getId());
			btnCheckButton.setSelection(selected);
			btnCheckButton.addListener(SWT.Selection, e -> {
				model.setSourceSelected(source, btnCheckButton.getSelection());
				setPageComplete(isPageComplete());
			});
		});

	}

	@Override
	public boolean isPageComplete() {
		return model.getSelectionBySource().values().stream().anyMatch(Boolean::booleanValue);
	}

}
