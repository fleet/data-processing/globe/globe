package fr.ifremer.globe.editor.chart.dialog.toolbox.tools;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.editor.chart.dialog.toolbox.ChartEditorToolComposite;
import fr.ifremer.globe.ui.widget.spinner.EnabledNumberModel;
import fr.ifremer.globe.ui.widget.spinner.SpinnerWidget;
import fr.ifremer.globe.utils.exception.GException;

public class ValueFilterTool extends ChartEditorToolComposite {

	public final static String NAME = "Filter from value";
	private IChartSeries<? extends IChartData> inputSeries;

	private EnabledNumberModel maxValueModel = new EnabledNumberModel(true, 10);
	private EnabledNumberModel minValueModel = new EnabledNumberModel(true, -10);
	private boolean valitidyMode;

	public ValueFilterTool(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		Label lblNewLabel = new Label(this, SWT.WRAP);
		lblNewLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblNewLabel.setText("Define filter parameters to validate or invalidate data.");

		Label label_2 = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		// bandwidth
		Label lblBandwidth = new Label(this, SWT.NONE);
		lblBandwidth.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblBandwidth.setText("Thresholds");

		var maxSpinner = new SpinnerWidget("Maximum value", this, maxValueModel, 2);
		GridData gd_maxSpinner = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		maxSpinner.setLayoutData(gd_maxSpinner);
		maxSpinner.subscribe(model -> maxValueModel = model);

		var minSpinner = new SpinnerWidget("Minimum value", this, minValueModel, 2);
		GridData gd_minSpinner = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		minSpinner.setLayoutData(gd_minSpinner);
		minSpinner.subscribe(model -> minValueModel = model);

		Label label_1 = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayout(new GridLayout(3, false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));

		// Validity mode
		Label lblRobustnessIters = new Label(composite, SWT.NONE);
		lblRobustnessIters.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblRobustnessIters.setText("Set validity to ");

		Button btnRadioButtonTrue = new Button(composite, SWT.RADIO);
		btnRadioButtonTrue.setText("true");
		btnRadioButtonTrue.addListener(SWT.Selection, evt -> valitidyMode = true);

		Button btnRadioButtonFalse = new Button(composite, SWT.RADIO);
		btnRadioButtonFalse.setText("false");
		btnRadioButtonFalse.addListener(SWT.Selection, evt -> valitidyMode = false);
		btnRadioButtonFalse.setSelection(true);
	}

	@Override
	public String getDisplayName() {
		return NAME;
	}

	@Override
	public void setInput(IChartSeries<? extends IChartData> inputSeries) {
		this.inputSeries = inputSeries;
	}

	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		if (inputSeries == null)
			return Status.CANCEL_STATUS;
		monitor.setTaskName("Apply filter...");
		double min = minValueModel.enable ? minValueModel.value.doubleValue() : Double.MIN_VALUE;
		double max = maxValueModel.enable ? maxValueModel.value.doubleValue() : Double.MAX_VALUE;
		logger.debug("Filter data with min = {} and max = {} ", min, max);
		inputSeries.stream().filter(data -> data.getY() < min || data.getY() > max)
				.forEach(data -> data.setEnabled(valitidyMode));
		return Status.OK_STATUS;
	}

}
