package fr.ifremer.globe.editor.chart.dialog.saveascsv;

import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.ui.wizard.DefaultSelectOutputParametersPageModel;

public class SaveWizardModel extends DefaultSelectOutputParametersPageModel {

	/**
	 * Map to link selection state with {@link IChartSeries}
	 */
	private Map<IChartSeries<? extends IChartData>, AtomicBoolean> selectionBySeries = new HashMap<>();

	/** Constructor **/
	public SaveWizardModel(List<IChartSeries<? extends IChartData>> inputSeries) {
		super();
		getOutputFormatExtensions().add("csv");
		inputSeries.forEach(series -> selectionBySeries.put(series, new AtomicBoolean(series.isEnabled())));
		inputSeries.stream().map(IChartSeries::getSource).map(IChartSeriesDataSource::getFilePath).findFirst()
				.ifPresent(path -> getOutputDirectory().doSetValue(new File(path.toString()).getParentFile()));
		updateFiles();
	}

	/**
	 * @return all sources (= input file) and if they are selected (= at least one of their series is selected).
	 */
	public Map<IChartSeriesDataSource, Boolean> getSelectionBySource() {
		var result = new HashMap<IChartSeriesDataSource, Boolean>();
		selectionBySeries.keySet().stream()//
				.map(IChartSeries::getSource)//
				.distinct()//
				.forEach(source -> result.put(source, selectionBySeries.entrySet().stream()
						.anyMatch(entry -> entry.getKey().getSource().equals(source) && entry.getValue().get())));
		return result;
	}

	/**
	 * Sets a source selected or not. (update the list of series which can be selected)
	 */
	public void setSourceSelected(IChartSeriesDataSource source, boolean selected) {
		if (selected)
			selectionBySeries.keySet().stream().filter(series -> series.getSource().equals(source)).forEach(series -> {
				if (!selectionBySeries.containsKey(series))
					selectionBySeries.put(series, new AtomicBoolean(true));
			});
		else
			selectionBySeries.keySet().removeIf(series -> series.getSource().equals(source));
		updateFiles();
	}

	public Map<IChartSeries<? extends IChartData>, AtomicBoolean> getSelectableCharSeries() {
		return selectionBySeries;
	}

	public void updateFiles() {
		getInputFiles().clear();
		List<File> inputFile = selectionBySeries.entrySet().stream()//
				.filter(e -> e.getValue().get()) //
				.map(Entry::getKey)//
				.map(IChartSeries::getSource)//
				.distinct()//
				.map(IChartSeriesDataSource::getFilePath).filter(Optional::isPresent).map(Optional::get)
				.map(Path::toFile)//
				.toList();

		if (!inputFile.isEmpty()) {
			getInputFiles().addAll(inputFile);
			getOutputDirectory().doSetValue(inputFile.get(0).getParentFile());
		}
	}

	/**
	 * @return the list of selected series.
	 */
	public List<IChartSeries<? extends IChartData>> getSeriesToSave() {
		return selectionBySeries.entrySet().stream().filter(e -> e.getValue().get()).map(Entry::getKey)
				.collect(Collectors.toList());
	}

	public void selectSeries(String title, boolean selection) {
		selectionBySeries.entrySet().stream() //
				.filter(entry -> entry.getKey().getTitle().equals(title))
				.forEach(entry -> entry.getValue().set(selection));
	}

}
