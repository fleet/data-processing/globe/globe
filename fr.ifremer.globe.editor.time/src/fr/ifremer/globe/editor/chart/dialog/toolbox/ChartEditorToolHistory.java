package fr.ifremer.globe.editor.chart.dialog.toolbox;

import java.nio.file.Path;
import java.time.Instant;
import java.util.Optional;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;

/**
 * Records with all information about a process executed by a {@link ChartEditorToolComposite}.
 */
public record ChartEditorToolHistory(String description, Instant date, IChartSeries<? extends IChartData> originSeries)
		implements IChartSeriesDataSource {

	@Override
	public String getId() {
		return description;
	}

	@Override
	public Optional<Path> getFilePath() {
		return originSeries.getSource().getFilePath();
	}

}
