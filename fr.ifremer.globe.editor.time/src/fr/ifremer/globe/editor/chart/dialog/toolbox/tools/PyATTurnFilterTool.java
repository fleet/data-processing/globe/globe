package fr.ifremer.globe.editor.chart.dialog.toolbox.tools;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.slf4j.Logger;

import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.navigation.utils.NavigationTurnCutter;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.core.runtime.gws.GwsServiceAgent;
import fr.ifremer.globe.core.runtime.gws.param.turnfilter.TurnFilterMethods;
import fr.ifremer.globe.core.runtime.gws.param.turnfilter.TurnFilterParams;
import fr.ifremer.globe.editor.chart.dialog.toolbox.ChartEditorToolComposite;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.widget.navigation.TurnFilterParametersComposite;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

public class PyATTurnFilterTool extends ChartEditorToolComposite {

	public static final String NAME = "Filter from navigation";

	@Inject
	private GwsServiceAgent gwsServiceAgent;

	private final ChartViewerModel chartViewerModel;

	private IChartSeries<? extends IChartData> inputSeries;

	private TurnFilterParametersComposite turnFilterParametersComposite;

	private Spinner minDurationSpinner;

	private ComboViewer methodComboViewer;

	public PyATTurnFilterTool(Composite parent, int style, ChartViewerModel chartViewerModel) {
		super(parent, style);
		this.chartViewerModel = chartViewerModel;
		ContextInitializer.inject(this);
		setLayout(new GridLayout(3, false));

		Label lblAlgorithm = new Label(this, SWT.NONE);
		lblAlgorithm.setText("Filter method :");

		methodComboViewer = new ComboViewer(this, SWT.READ_ONLY);
		Combo combo = methodComboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		methodComboViewer.setContentProvider(new ArrayContentProvider());
		methodComboViewer.setInput(TurnFilterMethods.values());
		methodComboViewer
				.setLabelProvider(LabelProvider.createTextProvider((method) -> ((TurnFilterMethods) method).label));
		methodComboViewer.setSelection(new StructuredSelection(TurnFilterMethods.STD));

		Group grpParameters = new Group(this, SWT.NONE);
		grpParameters.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		grpParameters.setLayout(new GridLayout(1, false));
		grpParameters.setText("Observed variables");
		turnFilterParametersComposite = new TurnFilterParametersComposite(grpParameters, SWT.NONE);
		turnFilterParametersComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		Label lblMinimumDuration = new Label(this, SWT.NONE);
		lblMinimumDuration.setText("Minimum duration of computed lines : ");
		lblMinimumDuration.setToolTipText("Minimum duration of result lines (min)");
		minDurationSpinner = new Spinner(this, SWT.BORDER);
		minDurationSpinner.setMaximum(999999);
		minDurationSpinner.setSelection((int) NavigationTurnCutter.DEFAULT_MIN_LINE_DURATION.toMinutes());
		Label lblMin = new Label(this, SWT.NONE);
		lblMin.setText("min");
	}

	private TurnFilterMethods getMethod() {
		AtomicReference<TurnFilterMethods> result = new AtomicReference<>();
		getDisplay().syncExec(
				() -> result.set((TurnFilterMethods) methodComboViewer.getStructuredSelection().getFirstElement()));
		return result.get();
	}

	private Optional<Duration> getMinDuration() {
		AtomicReference<Duration> result = new AtomicReference<>();
		getDisplay().syncExec(() -> result.set(Duration.ofMinutes(minDurationSpinner.getSelection())));
		return result.get().isZero() ? Optional.empty() : Optional.of(result.get());
	}

	@Override
	public String getDisplayName() {
		return NAME;
	}

	@Override
	public void setInput(IChartSeries<? extends IChartData> inputSeries) {
		this.inputSeries = inputSeries;
	}

	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		if (inputSeries == null)
			return Status.CANCEL_STATUS;

		try {
			monitor.setTaskName("Apply : " + NAME + "...");
			monitor.beginTask(NAME, 3);

			// get navigation
			var nav = chartViewerModel.getNavigationData(inputSeries)
					.orElseThrow(() -> new GIOException("Navigation data not available.")).getNavigationData();
			monitor.worked(1);

			// use temporary file to send navigation data
			var inputFile = TemporaryCache.createTemporaryFile("nav_cut", ".nvi");
			INviWriter.grab().write(nav, inputFile.getAbsolutePath());
			var outputFilePath = new File(inputFile.getAbsolutePath().replace(".nvi", ".cut"));

			var params = new TurnFilterParams(//
					inputFile, //
					outputFilePath, //
					getMethod(), //
					turnFilterParametersComposite.getHeadingParameters(), //
					turnFilterParametersComposite.getSpeedParameters(), //
					getMinDuration());

			// run
			var result = gwsServiceAgent.filterTurn(params, monitor, logger);
			monitor.worked(1);

			// handle output
			if (result.isOK()) {
				var cutLines = ProfileUtils.loadProfilesFromFile(outputFilePath);
				inputSeries.stream()
						.filter(data -> cutLines.stream().noneMatch(line -> line.contains(Math.round(data.getX()))))
						.forEach(data -> data.setEnabled(false));
			}
			monitor.worked(1);
			return result;
		} catch (IOException | GIOException e) {
			return Status.error("Error in " + NAME + ": " + e.getMessage(), e);
		}
	}

}
