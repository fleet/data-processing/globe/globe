package fr.ifremer.globe.editor.chart.dialog.toolbox.tools;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.editor.chart.dialog.toolbox.ChartEditorToolComposite;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.algo.Smoothing;
import fr.ifremer.globe.utils.exception.GException;

public class SmoothingTool extends ChartEditorToolComposite {

	public static final String NAME = "Smoother";

	private Spinner spinnerRobustnessIters;
	private Spinner spinnerAccuracy;

	/** Help from apache java doc **/
	private static final String BANDWITH_HELP = "When computing the loess fit at a particular point, this fraction of source points closest to the current point is taken into account for computing a least-squares regression."
			+ "\r\nThe smoothing will be stronger if this value is higher, and inversely.";
	private static final String ROBUSTNESS_HELP = "This many robustness iterations are done.\r\nA sensible value is usually 0 (just the initial fit without any robustness iterations) to 4.";
	private static final String ACCURACY_HELP = "If the median residual at a certain robustness iteration is less than this amount, no more iterations are done.";
	private Text bandwidthTextfield;

	private IChartSeries<? extends IChartData> inputSeries;

	public SmoothingTool(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		Label lblNewLabel = new Label(this, SWT.WRAP);
		lblNewLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
		lblNewLabel.setText(
				"LOESS smoothing alogrithm (LOcally Estimated Scatterplot Smoothing). \r\nSee https://en.wikipedia.org/wiki/Local_regression.");

		Label label_2 = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));

		// bandwidth

		Label lblBandwidth = new Label(this, SWT.NONE);
		lblBandwidth.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblBandwidth.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblBandwidth.setText("Bandwidth");
		new Label(this, SWT.NONE);

		Label lblNewLabel_1 = new Label(this, SWT.WRAP);
		lblNewLabel_1.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		lblNewLabel_1.setText(BANDWITH_HELP);

		bandwidthTextfield = new Text(this, SWT.BORDER);
		GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_text.widthHint = 65;
		bandwidthTextfield.setLayoutData(gd_text);
		bandwidthTextfield.setText(Double.toString(Smoothing.DEFAULT_BANDWIDTH));

		Label label_1 = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));

		// robustness

		Label lblRobustnessIters = new Label(this, SWT.NONE);
		lblRobustnessIters.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblRobustnessIters.setText("Robustness iters");
		spinnerRobustnessIters = new Spinner(this, SWT.BORDER);
		spinnerRobustnessIters.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 2));
		spinnerRobustnessIters.setSelection(Smoothing.DEFAULT_ROBUSTNESS_ITERS);

		Label lblNewLabel_1_1 = new Label(this, SWT.WRAP);
		lblNewLabel_1_1.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lblNewLabel_1_1.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblNewLabel_1_1.setText(ROBUSTNESS_HELP);

		Label label = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		// accuracy

		Label lblAccuracy = new Label(this, SWT.NONE);
		lblAccuracy.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblAccuracy.setText("Accuracy");
		spinnerAccuracy = new Spinner(this, SWT.BORDER);
		spinnerAccuracy.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 2));
		spinnerAccuracy.setDigits(2);
		spinnerAccuracy.setSelection((int) (Math.pow(10, spinnerAccuracy.getDigits()) * Smoothing.DEFAULT_ACCURACY));

		Label lblNewLabel_1_2 = new Label(this, SWT.WRAP);
		lblNewLabel_1_2.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lblNewLabel_1_2.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		lblNewLabel_1_2.setText(ACCURACY_HELP);
	}

	@Override
	public String getDisplayName() {
		return NAME;
	}

	@Override
	public void setInput(IChartSeries<? extends IChartData> inputSeries) {
		this.inputSeries = inputSeries;
	}

	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		monitor.setTaskName("Apply smoothing...");

		// get parameters from display thread
		AtomicReference<Double> bandwidth = new AtomicReference<Double>(Double.NaN);
		AtomicInteger robustnessIters = new AtomicInteger();
		AtomicReference<Double> accuracy = new AtomicReference<Double>(Double.NaN);
		getDisplay().syncExec(() -> {
			bandwidth.set(Double.parseDouble(bandwidthTextfield.getText()));
			robustnessIters.set(spinnerRobustnessIters.getSelection());
			accuracy.set(spinnerAccuracy.getSelection() / Math.pow(10, spinnerAccuracy.getDigits()));
		});
		var xyArrays = inputSeries.getValidXYArrays();

		// bandwidth is limited to a maximum value to avoid performance issues
		var maximumBandwidth = 1000d / xyArrays[0].length;
		if (bandwidth.get() > maximumBandwidth) {
			var result = Messages.openSyncQuestionMessage(
					"Apply smoothing : bandwidth parameter over limits", "The bandwidth parameter can not exceed "
							+ maximumBandwidth + " for input series with size : " + xyArrays[0].length,
					"Continue with maximum value", "Cancel");
			if (result != 0)
				return Status.CANCEL_STATUS;
			bandwidth.set(maximumBandwidth);
		}

		logger.debug("Apply smoothing on {} (size={}) with parameters : bandwidth={}; robustnessIters={}, accuracy={}",
				inputSeries.getTitle(), xyArrays[0].length, bandwidth, robustnessIters, accuracy);
		var yResult = Smoothing.apply(xyArrays[0], xyArrays[1], bandwidth.get(), robustnessIters.get(), accuracy.get());
		inputSeries.setValidYArray(yResult);
		return Status.OK_STATUS;
	}

}
