package fr.ifremer.globe.editor.chart.dialog.calculator;

import java.time.Instant;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.model.chart.ChartSeriesUtils;
import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.impl.SimpleChartSeriesBuilder;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.chart.dialog.toolbox.ChartEditorToolHistory;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This dialog allows user to apply operation on series (as a "Calculator").
 */
public class CalculatorDialog extends Dialog {

	/**
	 * Operators than can be apply on series. An operator provide 2 functions : one with a {@link Double} parameter,
	 * another with a {@link IChartSeries<? extends IChartData>} parameter.
	 */
	enum Operators {
		/** OPERATORS **/
		ADD("+", ChartSeriesUtils::applyOffset, ChartSeriesUtils::add), //
		MINUS("-", (series, value) -> ChartSeriesUtils.applyOffset(series, -value), ChartSeriesUtils::minus);

		/** Label (displayed on button & expression) **/
		public final String label;
		/** Function with double **/
		public final BiFunction<IChartSeries<? extends IChartData>, Double, IChartSeries<? extends IChartData>> functionWithDouble;
		/** Function with series **/
		public final BinaryOperator<IChartSeries<? extends IChartData>> functionWithSeries;

		Operators(String string,
				BiFunction<IChartSeries<? extends IChartData>, Double, IChartSeries<? extends IChartData>> function,
				BinaryOperator<IChartSeries<? extends IChartData>> functionWithSeries) {
			this.label = string;
			this.functionWithDouble = function;
			this.functionWithSeries = functionWithSeries;
		}

		static Optional<Operators> get(String name) {
			for (Operators op : values()) {
				if (op.label.equals(name))
					return Optional.of(op);
			}
			return Optional.empty();
		}
	}

	private final ChartViewerModel chartViewerModel;

	/** Inner composites **/
	private Text newOutputSeriesTitle;
	private Text expressionWidget;
	private WritableString expression = new WritableString();
	private ListViewer listViewer;

	/**
	 * Constructor
	 */
	public CalculatorDialog(Shell parentShell, ChartViewerModel chartViewerModel) {
		super(parentShell);
		this.chartViewerModel = chartViewerModel;
	}

	// overriding this methods allows you to set the title of the custom dialog
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Chart Editor Calculator");
	}

	// sets window size
	@Override
	protected Point getInitialSize() {
		return new Point(700, 750);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);

		// series
		Group grpInputData = new Group(container, SWT.NONE);
		grpInputData.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpInputData.setLayout(new GridLayout(1, false));
		grpInputData.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpInputData.setText("Series");

		Label lblDoubleClickTo = new Label(grpInputData, SWT.NONE);
		lblDoubleClickTo.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lblDoubleClickTo.setText("Double click to add series in expression");

		listViewer = new ListViewer(grpInputData, SWT.BORDER | SWT.V_SCROLL);
		org.eclipse.swt.widgets.List list = listViewer.getList();
		GridData gd_list = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_list.heightHint = 150;
		list.setLayoutData(gd_list);

		listViewer.setContentProvider(ArrayContentProvider.getInstance());
		listViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				var series = ((IChartSeries<? extends IChartData>) element);
				var label = series.getTitle();
				if (chartViewerModel != null && chartViewerModel.getBySource().size() > 1)
					label += " (" + series.getSource().getId() + ")";
				return label;
			}
		});
		listViewer.setInput(chartViewerModel.getAllSorted());
		listViewer.addDoubleClickListener(event -> {
			IChartSeries<? extends IChartData> selectedSeries = (IChartSeries<? extends IChartData>) ((StructuredSelection) event
					.getSelection()).getFirstElement();
			addToExpression(selectedSeries);
		});

		// operators
		Group grpProcess = new Group(container, SWT.NONE);
		grpProcess.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpProcess.setLayout(new GridLayout(Operators.values().length, false));
		grpProcess.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpProcess.setText("Operators");

		for (var operator : Operators.values()) {
			Button addButton = new Button(grpProcess, SWT.NONE);
			GridData gdAddButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
			gdAddButton.widthHint = 100;
			addButton.setLayoutData(gdAddButton);
			addButton.setText(operator.label);
			addButton.addListener(SWT.Selection, e -> addToExpression(operator.label));
		}

		// expression
		Group grpExpression = new Group(container, SWT.NONE);
		grpExpression.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpExpression.setLayout(new GridLayout(1, false));
		grpExpression.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpExpression.setText("Expression");

		expressionWidget = new Text(grpExpression, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		GridData gdText = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdText.heightHint = 100;
		expressionWidget.setLayoutData(gdText);

		Button btnClear = new Button(grpExpression, SWT.NONE);
		GridData gdBtnClear = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gdBtnClear.widthHint = 100;
		btnClear.setLayoutData(gdBtnClear);
		btnClear.setText("Clear");
		btnClear.addListener(SWT.Selection, e -> expression.doSetValue(""));

		// outuput
		Group grpOutput = new Group(container, SWT.NONE);
		grpOutput.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpOutput.setLayout(new GridLayout(1, false));
		grpOutput.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpOutput.setText("Output");

		newOutputSeriesTitle = new Text(grpOutput, SWT.BORDER);
		newOutputSeriesTitle.setText("computed");
		newOutputSeriesTitle.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		initDataBindings();
		return container;
	}

	// override method to use "Login" as label for the OK button
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Apply", true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Apply the selected process.
	 */
	@Override
	protected void okPressed() {
		IProcessService.grab().runInForeground("Compute : " + expression.doGetValue(), (monitor, logger) -> {
			logger.info("Compute series with expression : {}", expression.doGetValue());
			Display.getDefault().syncExec(() -> {
				var title = newOutputSeriesTitle.getText();
				IChartSeries<? extends IChartData> resultSeries;
				try {
					resultSeries = parseExpression(title);
				} catch (Exception e) {
					Messages.openErrorMessage("Error", "Invalid expression : " + e.getMessage());
					return;
				}
				resultSeries.setEnabled(true); // force display
				chartViewerModel.update(resultSeries);
				listViewer.setInput(chartViewerModel.getAll());
			});
			return Status.OK_STATUS;
		});
	}

	protected DataBindingContext initDataBindings() {
		var bindingContext = new DataBindingContext();
		//
		IObservableValue<?> observeTextTextObserveWidget = WidgetProperties.text(SWT.Modify).observe(expressionWidget);
		bindingContext.bindValue(observeTextTextObserveWidget, expression, null, null);
		//
		return bindingContext;
	}

	private void addToExpression(IChartSeries<? extends IChartData> series) {
		addToExpression("\"" + chartViewerModel.getKey(series) + "\"");
	}

	private void addToExpression(String value) {
		expression.doSetValue(expression.doGetValue() + value);
	}

	/** Expression parsing (with regex) **/

	// group & pattern for series
	private static final String SERIES_GRP = "series";
	private static final String SERIES_PATTERN = "\\\"(?<" + SERIES_GRP + ">[^\\\"]*)\\\"";
	// group & pattern for operators
	private static final String OPERATOR_GRP = "operator";
	private static final String OPERATOR_PATTERN = "(?<" + OPERATOR_GRP + ">["
			+ Stream.of(Operators.values()).map(op -> op.label).collect(Collectors.joining()) + "])";
	// group & pattern for operands as double
	private static final String OPERAND_GRP = "operandValue";
	private static final String OPERAND_PATTERN = "(?<" + OPERAND_GRP + ">\\d+\\.?\\d*)";

	// pattern to get the first series
	private static final Pattern PATTERN_FIRST_SERIES = Pattern.compile(SERIES_PATTERN + OPERATOR_PATTERN + ".*");

	// pattern for operators & operands ect...
	private static final Pattern PATTERN = Pattern
			.compile(OPERATOR_PATTERN + "(" + SERIES_PATTERN + ")?" + OPERAND_PATTERN + "?");

	/**
	 * Parses the current expression and apply it. Creates a new {@link IChartSeries<? extends IChartData>} with result.
	 */
	private IChartSeries<? extends IChartData> parseExpression(String resultTitle) throws GIOException {
		String exp = expression.doGetValue();
		var firstSeriesMatcher = PATTERN_FIRST_SERIES.matcher(exp);
		IChartSeries<? extends IChartData> resultSeries = null;
		if (firstSeriesMatcher.find()) {
			var inputSeriesString = Optional.of(firstSeriesMatcher.group(SERIES_GRP)).orElseThrow();
			var firstInputSeries = chartViewerModel.get(inputSeriesString).orElseThrow();
			resultSeries = new SimpleChartSeriesBuilder(resultTitle) //
					.withSource(new ChartEditorToolHistory("calculator", Instant.now(), firstInputSeries)) //
					.withData(firstInputSeries)//
					.build();
			exp = exp.replace("\"" + inputSeriesString + "\"", "");
		} else {
			throw new GIOException("expression must start with a series.");
		}

		Function<IChartSeries<? extends IChartData>, IChartSeries<? extends IChartData>> function = Function.identity();
		var operationMatcher = PATTERN.matcher(exp);
		while (operationMatcher.find()) {
			// parse operator and operand (operand can be a value or a serie)
			Operators operator = Operators.get(operationMatcher.group(OPERATOR_GRP)).orElseThrow();
			Optional<String> optValue = Optional.ofNullable(operationMatcher.group(OPERAND_GRP));
			Optional<IChartSeries<? extends IChartData>> optSeries = Optional
					.ofNullable(operationMatcher.group(SERIES_GRP))
					.map(series -> chartViewerModel.get(series).orElseThrow());

			// create a composed function
			if (optValue.isPresent()) // with double value
				function = function.andThen(
						series -> operator.functionWithDouble.apply(series, Double.parseDouble(optValue.get())));
			else if (optSeries.isPresent()) // with other series
				function = function.andThen(series -> operator.functionWithSeries.apply(series, optSeries.get()));
			else
				throw new GIOException("operand missing after operator : '" + operator.label + "'");

		}
		return function.apply(resultSeries);
	}
}