package fr.ifremer.globe.editor.chart.dialog.toolbox;

import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;

public abstract class ChartEditorToolComposite extends Composite implements IProcessFunction {

	protected ChartEditorToolComposite(Composite parent, int style) {
		super(parent, style);
	}

	public abstract String getDisplayName();

	public abstract void setInput(IChartSeries<? extends IChartData> inputSeries);

}