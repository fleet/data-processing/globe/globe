/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.chart.dialog.saveascsv;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.chart.ChartSeriesUtils;
import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;

@Creatable
public class SaveWizard extends Wizard {

	/** Chart Viewer model **/
	private final ChartViewerModel chartModel;

	/** Wizard model */
	protected SaveWizardModel saveWizardModel;

	public SaveWizard(ChartViewerModel chartModel) {
		this.chartModel = chartModel;
		setWindowTitle("Chart Editor : save as CSV");
	}

	/**
	 * Creates dialog.
	 */
	private WizardDialog createsWizardDialog(Shell shell) {
		WizardDialog wizardDialog = new WizardDialog(shell, this);
		wizardDialog.setPageSize(700, 500);
		return wizardDialog;
	}

	/**
	 * Opens this wizard with all available series.
	 */
	public void open(Shell shell) {
		saveWizardModel = new SaveWizardModel(chartModel.getAll());
		createsWizardDialog(shell).open();
	}

	/**
	 * Opens this wizard with an input to save.
	 */
	public void open(Shell shell, List<IChartSeries<? extends IChartData>> series) {
		saveWizardModel = new SaveWizardModel(series);
		createsWizardDialog(shell).open();
	}

	/**
	 * Model getter
	 */
	public SaveWizardModel getExportWizardModel() {
		return saveWizardModel;
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		if (saveWizardModel.getSelectionBySource().size() > 1)
			addPage(new SaveWizardSelectSourcePage(saveWizardModel));

		if (saveWizardModel.getSelectableCharSeries().size() > 1)
			addPage(new SaveWizardSelectSeriesPage(saveWizardModel));

		SelectOutputParametersPage selectOutputFilesPage = new SelectOutputParametersPage(saveWizardModel);
		selectOutputFilesPage.enableLoadFilesAfter(true);
		addPage(selectOutputFilesPage);
	}

	/**
	 *
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		var seriesToSave = saveWizardModel.getSeriesToSave();
		var inputFiles = saveWizardModel.getInputFiles();
		var outputFiles = saveWizardModel.getOutputFiles();

		// check if output file(s) exist or can be overwritten
		for (var outputFile : outputFiles) {
			if (outputFile.exists() && saveWizardModel.getOverwriteExistingFiles().isFalse()) {
				Messages.openWarningMessage("Save", String
						.format("File %s already exists and overwrite option not enabled.", outputFile.getName()));
				return false;
			}
		}

		var seriesBySource = seriesToSave.stream().collect(Collectors.groupingBy(IChartSeries::getSource));

		for (var entry : seriesBySource.entrySet()) {
			try {
				var inputFile = entry.getKey().getFilePath().orElseThrow().toFile();
				var outputFile = outputFiles.get(inputFiles.indexOf(inputFile));

				// if output file doesn't exist & is same type as input file : copy input to get a base file to edit
				if (!outputFile.exists()
						&& FileUtils.getExtension(inputFile).equals(FileUtils.getExtension(outputFile)))
					FileUtils.copy(inputFile, outputFile);

				var outputSeries = entry.getValue();
				ChartSeriesUtils.saveSeriesToCsv(outputSeries, outputFile.getAbsolutePath());

				// load file after save
				if (Boolean.TRUE.equals(saveWizardModel.getLoadFilesAfter().get()))
					loadOutputFile(outputFile);

			} catch (GIOException e) {
				Messages.openErrorMessage("Save error", "Error : " + e.getMessage(), e);
				return false;
			}

		}
		Messages.openInfoMessage("Save", "Successful save.");
		return true;
	}

	/**
	 * Loads output file.
	 */
	protected void loadOutputFile(File outputFile) {
		if (outputFile != null && outputFile.exists() && (saveWizardModel.getLoadFilesAfter().isTrue()
				|| IFileLoadingService.grab().getFileInfoModel().contains(outputFile.getAbsolutePath()))) {
			ITreeNodeFactory.grab().createFileInfoNode(new BasicFileInfo(outputFile));
			IFileLoadingService.grab().reload(outputFile);
		}
	}

}
