package fr.ifremer.globe.editor.chart.dialog.toolbox.tools;

import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.datacontainer.DataContainerChartSeries1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.core.runtime.gws.GwsServiceAgent;
import fr.ifremer.globe.core.runtime.gws.param.SmootherParams;
import fr.ifremer.globe.editor.chart.dialog.toolbox.ChartEditorToolComposite;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.impl.LargeArray;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

public class PyATSmoothingTool extends ChartEditorToolComposite {

	public static final String NAME = "Smoother (PyAT)";

	@Inject
	private GwsServiceAgent gwsServiceAgent;

	/**
	 * Filter algorithms
	 */
	private enum Algorithms {
		SAVITZKY_GOLAY("savgol", "Savitzky-Golay filter (scipy.signal.savgol_filter)"), //
		BUTTERWORTH("butter", "Butterworth digital filter (scipy.signal.butter + scipy.signal.filtfilt)"), //
		BESSEL("bessel", "Bessel/Thomson digital filter (scipy.signal.bessel + scipy.signal.filtfilt)"), //
		BESSEL_Q("bessel_q", "");

		public final String name;
		public final String help;

		Algorithms(String name, String help) {
			this.name = name;
			this.help = help;
		}
	}

	/** {@link IChartSeries} **/
	private IChartSeries<? extends IChartData> inputSeries;

	/** Widgets **/
	private Spinner windowSizeSpinner;
	private Spinner orderSpinner;
	private Text criticalFreqText;
	private Text samplingFreqText;
	private ComboViewer algoComboViewer;
	private Label lblHelp;
	private Group grpAlgortihmParameters;

	/**
	 * Constructor
	 */
	public PyATSmoothingTool(Composite parent, int style) {
		super(parent, style);
		ContextInitializer.inject(this);
		setLayout(new GridLayout(2, false));

		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		composite.setLayout(new GridLayout(2, false));

		Label lblNewLabel = new Label(composite, SWT.WRAP);
		lblNewLabel.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.ui", "icons/16/python.png"));

		Label lblPyatSmoother = new Label(composite, SWT.NONE);
		lblPyatSmoother.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblPyatSmoother.setText("PyAT Smoother");

		Label lblAlgorithm = new Label(this, SWT.NONE);
		lblAlgorithm.setText("Filter algorithm");

		algoComboViewer = new ComboViewer(this, SWT.READ_ONLY);
		Combo combo = algoComboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		algoComboViewer.setContentProvider(new ArrayContentProvider());
		algoComboViewer.setInput(Algorithms.values());

		algoComboViewer.setSelection(new StructuredSelection(Algorithms.SAVITZKY_GOLAY));
		createAlgoParamComposite(Algorithms.SAVITZKY_GOLAY);

		lblHelp = new Label(this, SWT.NONE);
		lblHelp.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		algoComboViewer.addSelectionChangedListener(event -> {
			var selectedAlgo = (Algorithms) ((IStructuredSelection) event.getSelection()).getFirstElement();
			if (selectedAlgo != null)
				lblHelp.setText(selectedAlgo.help);
			createAlgoParamComposite(selectedAlgo);
		});

	}

	private void createAlgoParamComposite(Algorithms algo) {
		if (grpAlgortihmParameters != null && !grpAlgortihmParameters.isDisposed())
			grpAlgortihmParameters.dispose();

		if (algo == Algorithms.BESSEL_Q)
			return; // no parameters

		grpAlgortihmParameters = new Group(this, SWT.NONE);
		grpAlgortihmParameters.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		grpAlgortihmParameters.setText("Algortihm parameters");
		grpAlgortihmParameters.setLayout(new GridLayout(2, false));

		Label lblOrder = new Label(grpAlgortihmParameters, SWT.NONE);
		lblOrder.setText("Filter order");
		orderSpinner = new Spinner(grpAlgortihmParameters, SWT.BORDER);
		GridData gd_orderSpinner = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_orderSpinner.widthHint = 50;
		orderSpinner.setLayoutData(gd_orderSpinner);
		orderSpinner.setMaximum(10);
		orderSpinner.setSelection(2);

		if (algo == Algorithms.SAVITZKY_GOLAY) {
			Label lblWindowSize = new Label(grpAlgortihmParameters, SWT.NONE);
			lblWindowSize.setText("Window size");
			windowSizeSpinner = new Spinner(grpAlgortihmParameters, SWT.BORDER);
			windowSizeSpinner.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
			windowSizeSpinner.setMaximum(10000);
			windowSizeSpinner.setSelection(9);
		}

		if (algo == Algorithms.BESSEL || algo == Algorithms.BUTTERWORTH) {
			Label lblCriticalFrequency = new Label(grpAlgortihmParameters, SWT.NONE);
			lblCriticalFrequency.setText("Critical frequency");
			criticalFreqText = new Text(grpAlgortihmParameters, SWT.BORDER);
			criticalFreqText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
			criticalFreqText.setText("0.2");

			Label lblSamplingFrequency = new Label(grpAlgortihmParameters, SWT.NONE);
			lblSamplingFrequency.setText("Sampling frequency");
			samplingFreqText = new Text(grpAlgortihmParameters, SWT.BORDER);
			samplingFreqText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
			samplingFreqText.setText("1");
		}

		this.layout();
	}

	@Override
	public String getDisplayName() {
		return NAME;
	}

	@Override
	public void setInput(IChartSeries<? extends IChartData> inputSeries) {
		this.inputSeries = inputSeries;
	}

	/**
	 * @return the input file with data to process.
	 */
	private LargeArray getInputFile() throws GIOException {
		try {
			if (inputSeries instanceof DataContainerChartSeries1D<?> dataContainerSeries
					&& dataContainerSeries.getLayer() instanceof ILoadableLayer<?> dataContainerLayer) {
				// get mapped file from datacontainer
				return dataContainerLayer.getData().getLargeArray();
			} else {
				// create a new mapped file (only with enabled data)
				var array = IArrayFactory.grab().makeLargeArray(inputSeries.size(), Double.BYTES, "ce_");
				array.setByteOrder(ByteOrder.nativeOrder());
				for (int i = 0; i < inputSeries.size(); i++) {
					var data = inputSeries.getData(i);
					array.putDouble(i, data.isEnabled() ? data.getY() : Double.NaN);
				}
				return array;
			}
		} catch (IOException ex) {
			throw new GIOException("PyAT process error : invalid input file.", ex);
		}
	}

	/**
	 * Handles the output file returned by PyAT process, and update input series.
	 */
	private void acceptOutputFile(String outputFilePath, Class<? extends Number> dataType) throws GIOException {
		try {
			var elementSize = dataType == Double.class ? Double.BYTES : Float.BYTES;
			var outputMappedFile = IArrayFactory.grab().openLargeArray(new File(outputFilePath), elementSize);
			outputMappedFile.setByteOrder(ByteOrder.nativeOrder());
			outputMappedFile.setDeleteOnClose(true);

			if (inputSeries instanceof DataContainerChartSeries1D<?> dataContainerSeries
					&& dataContainerSeries.getLayer() instanceof ILoadableLayer<?> dataContainerLayer) {
				dataContainerLayer.getData().replaceLargeArray(outputMappedFile);
				dataContainerSeries.load();
				dataContainerSeries.setDirty(true);
			} else {
				inputSeries.stream().filter(IChartData::isEnabled)
						.forEach(data -> data.setY(outputMappedFile.getDouble(data.getIndex())));
				outputMappedFile.close();
			}
		} catch (IndexOutOfBoundsException | IOException ex) {
			throw new GIOException("PyAT process error : invalid output file.", ex);
		}
	}

	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		monitor.beginTask(NAME, 4);
		if (inputSeries != null) {

			// get parameters
			monitor.setTaskName("Compute parameters...");
			monitor.worked(1);
			var inputMappedFile = getInputFile();
			var inputFilePath = inputMappedFile.getFile();
			var inputType = inputMappedFile.getShift() == 3 ? Double.class : Float.class;
			var outputFilePath = inputMappedFile.getFile().getAbsolutePath().replace(".tmp", "_output.tmp");

			AtomicReference<Algorithms> algo = new AtomicReference<>();
			AtomicInteger windowSize = new AtomicInteger();
			AtomicInteger order = new AtomicInteger();
			WritableDouble criticalFreq = new WritableDouble();
			WritableDouble samplingFreq = new WritableDouble();
			getDisplay().syncExec(() -> {
				algo.set((Algorithms) algoComboViewer.getStructuredSelection().getFirstElement());
				if (windowSizeSpinner != null && !windowSizeSpinner.isDisposed())
					windowSize.set(windowSizeSpinner.getSelection());
				if (orderSpinner != null && !orderSpinner.isDisposed())
					order.set(orderSpinner.getSelection());
				if (criticalFreqText != null && !criticalFreqText.isDisposed() && !criticalFreqText.getText().isEmpty())
					criticalFreq.set(Double.parseDouble(criticalFreqText.getText()));
				if (samplingFreqText != null && !samplingFreqText.isDisposed() && !samplingFreqText.getText().isEmpty())
					samplingFreq.set(Double.parseDouble(samplingFreqText.getText()));
			});
			monitor.worked(1);

			monitor.setTaskName("Run process...");
			var params = new SmootherParams(//
					inputFilePath, //
					new File(outputFilePath), //
					inputType == Double.class ? "double" : "float", // Type
					algo.get().name, // algorithm
					windowSize.get(), //
					order.get(), //
					criticalFreq.get(), //
					samplingFreq.get());
			var result = gwsServiceAgent.smoother(params, monitor, logger);
			if (result.isOK()) {
				monitor.worked(1);
				monitor.setTaskName("Load ouput file...");
				acceptOutputFile(outputFilePath, inputType);
			}

			return result;
		}
		return Status.CANCEL_STATUS;
	}
}
