package fr.ifremer.globe.editor.chart.dialog.toolbox;

import java.time.Instant;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jakarta.annotation.PostConstruct;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.core.io.bsar.BsarChartSeries;
import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.impl.SimpleChartSeriesBuilder;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.chart.dialog.toolbox.tools.CutFileFilterTool;
import fr.ifremer.globe.editor.chart.dialog.toolbox.tools.LinearTransformTool;
import fr.ifremer.globe.editor.chart.dialog.toolbox.tools.PyATSmoothingTool;
import fr.ifremer.globe.editor.chart.dialog.toolbox.tools.PyATTurnFilterTool;
import fr.ifremer.globe.editor.chart.dialog.toolbox.tools.SmoothingTool;
import fr.ifremer.globe.editor.chart.dialog.toolbox.tools.ValueFilterTool;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel;

public class ChartEditorToolbox {

	/** Id of the view defined in e4 model */
	public static final String PART_ID = "fr.ifremer.globe.editor.time.partdescriptor.charteditortoolbox";

	private ChartViewerModel chartViewerModel;
	private ComboViewer inputComboViewer;

	/** Map of tools (name & constructors) **/
	Map<String, BiFunction<Composite, Integer, ChartEditorToolComposite>> tools = new TreeMap<>();
	{
		tools.put(LinearTransformTool.NAME, LinearTransformTool::new);
		tools.put(PyATSmoothingTool.NAME, PyATSmoothingTool::new);
		tools.put(SmoothingTool.NAME, SmoothingTool::new);
		tools.put(PyATTurnFilterTool.NAME, (parent, style) -> new PyATTurnFilterTool(parent, style, chartViewerModel));
		tools.put(ValueFilterTool.NAME, ValueFilterTool::new);
		tools.put(CutFileFilterTool.NAME, CutFileFilterTool::new);
	}

	private ChartEditorToolComposite currentTool;

	private Text newOutputSeriesTitle;
	private Button updateInputRadioButton;

	private ComboViewer toolComboViewer;

	/**
	 * Post contruction, after injection
	 */
	@PostConstruct
	public void postConstruct(Composite parent) {

		parent.setLayout(new GridLayout(1, false));

		// Input
		Group grpInputData = new Group(parent, SWT.NONE);
		grpInputData.setLayout(new GridLayout(1, false));
		grpInputData.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpInputData.setText("Input");
		inputComboViewer = new ComboViewer(grpInputData, SWT.READ_ONLY);
		Combo combo = inputComboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		inputComboViewer.setContentProvider(ArrayContentProvider.getInstance());
		inputComboViewer.setLabelProvider(LabelProvider.createTextProvider(o -> {
			var series = ((IChartSeries<? extends IChartData>) o);
			var label = series.getTitle();
			if (chartViewerModel != null && chartViewerModel.getBySource().size() > 1)
				label += " (" + series.getSource().getId() + ")";
			return label;
		}));

		// Process
		Group grpProcess = new Group(parent, SWT.NONE);
		grpProcess.setLayout(new GridLayout(1, false));
		grpProcess.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpProcess.setText("Process");

		toolComboViewer = new ComboViewer(grpProcess, SWT.READ_ONLY);
		toolComboViewer.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolComboViewer.setContentProvider(ArrayContentProvider.getInstance());
		toolComboViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((Entry<String, BiFunction<Composite, Integer, ChartEditorToolComposite>>) element).getKey();
			}
		});
		toolComboViewer.setInput(tools.entrySet());

		Group grpParameters = new Group(parent, SWT.NONE);
		grpParameters.setLayout(new FillLayout(SWT.HORIZONTAL));
		grpParameters.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpParameters.setText("Parameters");
		toolComboViewer.addSelectionChangedListener(event -> {
			var selection = (Entry<String, BiFunction<Composite, Integer, ChartEditorToolComposite>>) ((IStructuredSelection) event
					.getSelection()).getFirstElement();
			if (selection != null) {
				Stream.of(grpParameters.getChildren()).forEach(Control::dispose);
				currentTool = selection.getValue().apply(grpParameters, SWT.NONE);
				currentTool.layout();
				grpParameters.layout();
			}
		});

		Group grpOutput = new Group(parent, SWT.NONE);
		grpOutput.setLayout(new GridLayout(3, false));
		grpOutput.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpOutput.setText("Output");

		updateInputRadioButton = new Button(grpOutput, SWT.RADIO);
		updateInputRadioButton.setText("Update input");

		Button newOutputSeriesRadioButton = new Button(grpOutput, SWT.RADIO);
		newOutputSeriesRadioButton.setText("New series :");
		newOutputSeriesRadioButton.addListener(SWT.Selection,
				e -> newOutputSeriesTitle.setEnabled(newOutputSeriesRadioButton.getSelection()));
		newOutputSeriesRadioButton.setSelection(true);

		newOutputSeriesTitle = new Text(grpOutput, SWT.BORDER);
		newOutputSeriesTitle.setText("processed");
		newOutputSeriesTitle.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Button runButton = new Button(parent, SWT.NONE);
		GridData gd_runButton = new GridData(SWT.RIGHT, SWT.BOTTOM, false, false, 1, 1);
		gd_runButton.widthHint = 150;
		runButton.setLayoutData(gd_runButton);
		runButton.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.ui", "icons/16/run_exc.png"));
		runButton.setText("Run");
		runButton.addListener(SWT.Selection, evt -> execute());

	}

	public void initialize(ChartViewerModel chartViewerModel) {
		this.chartViewerModel = chartViewerModel;
		var elements = chartViewerModel.getAllSorted();
		inputComboViewer.setInput(elements);
		if (!elements.isEmpty())
			inputComboViewer.setSelection(new StructuredSelection(
					elements.stream().filter(IChartSeries::isEnabled).findFirst().orElse(elements.get(0))));
		toolComboViewer.setSelection(new StructuredSelection(tools.entrySet().iterator().next()));
	}

	/**
	 * Apply the selected process.
	 */
	protected void execute() {
		// get selected input series & process to apply
		IChartSeries<? extends IChartData> inputSeries = (IChartSeries<? extends IChartData>) inputComboViewer
				.getStructuredSelection().getFirstElement();
		
		if (inputSeries == null || currentTool == null)
			return;

		//Check input type to apply process on each section 
		boolean processBySection = (inputSeries instanceof BsarChartSeries);
		
		if (!updateInputRadioButton.getSelection()) {
			// create new series
			var title = newOutputSeriesTitle.getText();
			inputSeries = new SimpleChartSeriesBuilder(title).withData(inputSeries)
					.withSource(new ChartEditorToolHistory(currentTool.getDisplayName(), Instant.now(), inputSeries))
					.build();
			inputSeries.setEnabled(true);
		}

		if(processBySection) {
			// Specific case for .bsar.nc data (could be extended to every case where sectionIds are present)
			// split by section
			List<String> sectionIds = inputSeries.stream().filter(IChartData::isEnabled).map(IChartData::getSectionId)
					.filter(Optional::isPresent).map(Optional::get).distinct().toList();
			IChartSeries<? extends IChartData> fInputSeries = inputSeries;
			IProcessService.grab().runInForeground(currentTool.getDisplayName(), (monitor, logger) -> {
				// backup enabled status
				List<Integer> enabledIndices = fInputSeries.stream().filter(IChartData::isEnabled)
						.map(IChartData::getIndex).toList();
				IStatus result = Status.OK_STATUS;
				for (String id : sectionIds) {
					monitor.subTask(id);
					// disable other sections
					fInputSeries.stream()
							.filter(data -> !(data.getSectionId().isPresent() && data.getSectionId().get().equals(id)))
							.forEach(data -> data.setEnabled(false));
					if (fInputSeries.stream().anyMatch(IChartData::isEnabled)) {
						// run process
						currentTool.setInput(fInputSeries);
						result = currentTool.apply(monitor, logger);
					}
					// re-enable backup status
					enabledIndices.stream().forEach(idx -> fInputSeries.getData(idx).setEnabled(true));
				}
				if (result != null && result.isOK() && fInputSeries.size() > 0)
					chartViewerModel.update(fInputSeries);
				return result;
			});			
		} else {
			// General case
			currentTool.setInput(inputSeries);
			// run process
			var fInputSeries = inputSeries;
			IProcessService.grab().runInForeground(currentTool.getDisplayName(), (monitor, logger) -> {
				var result = currentTool.apply(monitor, logger);
				if (result.isOK() && fInputSeries.size() > 0)
					chartViewerModel.update(fInputSeries);
				return result;
			});			
		}
	}
}