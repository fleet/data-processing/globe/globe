package fr.ifremer.globe.editor.chart.dialog.saveascsv;

import java.util.stream.Stream;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import fr.ifremer.globe.core.model.chart.IChartSeries;

/**
 * Wizard page used to define the CSV export options.
 */
public class SaveWizardSelectSeriesPage extends WizardPage {

	private SaveWizardModel model;
	private ScrolledComposite scrolledComposite;
	private Composite seriesListComposite;

	/**
	 * Constructor
	 */
	public SaveWizardSelectSeriesPage(SaveWizardModel model) {
		super(SaveWizardSelectSeriesPage.class.getName(), "Select series to save", null);
		this.model = model;
	}

	/**
	 * Creates the design of the input files selection page
	 */
	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout glComposite = new GridLayout(2, false);
		composite.setLayout(glComposite);
		setControl(composite);

		Group grpSeries = new Group(composite, SWT.NONE);
		grpSeries.setText("Series");
		grpSeries.setLayout(new FillLayout(SWT.HORIZONTAL));
		grpSeries.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));

		scrolledComposite = new ScrolledComposite(grpSeries, SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);

		var selectAllButton = new Button(composite, SWT.NONE);
		selectAllButton.setText("Select all");
		selectAllButton.addListener(SWT.Selection, e -> {
			Stream.of(seriesListComposite.getChildren()).filter(Button.class::isInstance).map(Button.class::cast)
					.forEach(button -> {
						button.setSelection(true);
						model.selectSeries(button.getText(), true);
					});
			model.updateFiles();

		});

		var unselectAllButton = new Button(composite, SWT.NONE);
		unselectAllButton.setText("Unselect all");
		unselectAllButton.addListener(SWT.Selection, e -> {
			Stream.of(seriesListComposite.getChildren()).filter(Button.class::isInstance).map(Button.class::cast)
					.forEach(button -> {
						button.setSelection(false);
						model.selectSeries(button.getText(), false);
					});
			model.updateFiles();
		});

		refreshPage();

	}

	public void refreshPage() {
		if (seriesListComposite != null)
			seriesListComposite.dispose();
		seriesListComposite = new Composite(scrolledComposite, SWT.NONE);
		seriesListComposite.setLayout(new GridLayout(1, false));

		model.getSelectableCharSeries().keySet().stream()//
				.map(IChartSeries::getTitle)//
				.distinct().sorted().forEach(title -> {
					Button btnCheckButton = new Button(seriesListComposite, SWT.CHECK);
					btnCheckButton.setText(title);
					btnCheckButton.setSelection(model.getSelectableCharSeries().entrySet().stream()
							.anyMatch(entry -> entry.getKey().getTitle().equals(title) && entry.getValue().get()));
					btnCheckButton.addListener(SWT.Selection, e -> {
						model.selectSeries(title, btnCheckButton.getSelection());
						model.updateFiles();
					});
				});

		scrolledComposite.setContent(seriesListComposite);
		scrolledComposite.setMinSize(seriesListComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	@Override
	public boolean isPageComplete() {
		return !model.getSeriesToSave().isEmpty();
	}

	@Override
	public IWizardPage getNextPage() {
		refreshPage();
		return super.getNextPage();
	}

}
