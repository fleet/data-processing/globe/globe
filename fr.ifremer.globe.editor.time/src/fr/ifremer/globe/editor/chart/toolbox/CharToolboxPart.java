package fr.ifremer.globe.editor.chart.toolbox;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.editor.chart.ChartViewer;
import fr.ifremer.globe.editor.chart.ChartViewer.EditionMode;
import fr.ifremer.globe.editor.chart.dialog.toolbox.ChartEditorToolbox;
import fr.ifremer.globe.editor.time.view.VariableGroupComposite;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.SelectionMode;

/**
 * Time viewer
 */
@Deprecated
public class CharToolboxPart {

	public static final String PART_ID = "fr.ifremer.globe.editor.chart.toolbox.part";

	@Inject
	private ChartViewer chartViewer;

	private Button btnValidate;
	private Button btnInvalidate;
	private Button buttonZoom;
	private Button btnPolygonSelection;
	private Button btnRectangularSelection;

	/**
	 * Initialization method: creates UI
	 */
	@PostConstruct
	public void createUI(Composite parent) {
		parent.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		parent.setLayout(new FillLayout(SWT.HORIZONTAL));

		ExpandBar expandBar = new ExpandBar(parent, SWT.NONE);

		ExpandItem xpndtmData = new ExpandItem(expandBar, SWT.NONE);
		xpndtmData.setExpanded(true);
		xpndtmData.setText("Data");

		VariableGroupComposite variableListComposite = new VariableGroupComposite(expandBar, SWT.FILL, null);
		xpndtmData.setControl(variableListComposite);
		xpndtmData.setHeight(xpndtmData.getControl().computeSize(SWT.DEFAULT, SWT.DEFAULT).y);

		ExpandItem xpndtmDisplay = new ExpandItem(expandBar, SWT.NONE);
		xpndtmDisplay.setExpanded(true);
		xpndtmDisplay.setText("Display");

		Composite compositeDisplay = new Composite(expandBar, SWT.NONE);
		xpndtmDisplay.setControl(compositeDisplay);
		compositeDisplay.setLayout(new GridLayout(2, false));

		Label lblColorMode = new Label(compositeDisplay, SWT.NONE);
		lblColorMode.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblColorMode.setText("Color mode");

		ComboViewer comboViewer = new ComboViewer(compositeDisplay, SWT.READ_ONLY);
		Combo combo = comboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Button btnResetZoom = new Button(compositeDisplay, SWT.NONE);
		btnResetZoom.setText("Reset zoom");
		new Label(compositeDisplay, SWT.NONE);
		xpndtmDisplay.setHeight(xpndtmDisplay.getControl().computeSize(SWT.DEFAULT, SWT.DEFAULT).y);

		// INTERACTORS
		ExpandItem xpndtmInteractors = new ExpandItem(expandBar, SWT.NONE);
		xpndtmInteractors.setExpanded(true);
		xpndtmInteractors.setText("Interactors");

		Composite compositeInteractors = new Composite(expandBar, SWT.NONE);
		xpndtmInteractors.setControl(compositeInteractors);
		compositeInteractors.setLayout(new GridLayout(2, false));

		// edition mode
		Group grpMode = new Group(compositeInteractors, SWT.NONE);
		grpMode.setLayout(new GridLayout(3, false));
		grpMode.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpMode.setText("Mode");

		btnValidate = new Button(grpMode, SWT.TOGGLE);
		btnValidate.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.editor.line", "icons/selection_add.png"));
		btnValidate.addListener(SWT.Selection, e -> setEditionMode(EditionMode.VALIDATE));

		btnInvalidate = new Button(grpMode, SWT.TOGGLE);
		btnInvalidate.setImage(ResourceManager.getPluginImage("fr.ifremer.globe", "icons/16/edit-delete.png"));
		btnInvalidate.addListener(SWT.Selection, e -> setEditionMode(EditionMode.INVALIDATE));

		buttonZoom = new Button(grpMode, SWT.TOGGLE);
		buttonZoom.setImage(ResourceManager.getPluginImage("fr.ifremer.globe", "icons/16/zoom_in.png"));
		buttonZoom.addListener(SWT.Selection, e -> setEditionMode(EditionMode.ZOOM));

		// selection mode
		Group grpSelection = new Group(compositeInteractors, SWT.NONE);
		grpSelection.setLayout(new GridLayout(2, false));
		grpSelection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpSelection.setText("Selection");

		btnPolygonSelection = new Button(grpSelection, SWT.TOGGLE);
		btnPolygonSelection.setImage(
				ResourceManager.getPluginImage("fr.ifremer.globe.editor.swath", "icons/16/selection_polygon.png"));
		btnPolygonSelection.addListener(SWT.Selection, e -> setSelectionMode(SelectionMode.POLYGON_SELECTION));

		btnRectangularSelection = new Button(grpSelection, SWT.TOGGLE);
		btnRectangularSelection.setImage(
				ResourceManager.getPluginImage("fr.ifremer.globe.editor.swath", "icons/16/selection_rectangular.png"));
		btnRectangularSelection.addListener(SWT.Selection, e -> setSelectionMode(SelectionMode.RECTANGLE_SELECTION));

		xpndtmInteractors.setHeight(xpndtmInteractors.getControl().computeSize(SWT.DEFAULT, SWT.DEFAULT).y);

		// tools
		ExpandItem xpndtmTools = new ExpandItem(expandBar, SWT.NONE);
		xpndtmTools.setExpanded(true);
		xpndtmTools.setText("Tools");

		Composite compositeTools = new Composite(expandBar, SWT.NONE);
		xpndtmTools.setControl(compositeTools);
		compositeTools.setLayout(new GridLayout(1, false));

		Button btnLinearFunction = new Button(compositeTools, SWT.NONE);
		btnLinearFunction.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnLinearFunction.setText("Calculator");

		Button btnSpline = new Button(compositeTools, SWT.NONE);
		btnSpline.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnSpline.setText("Spline");
		btnSpline.addListener(SWT.Selection, e -> {
//			ToolboxDialog dialog = new ToolboxDialog(Display.getDefault().getActiveShell(), null);
//			dialog.create();
//			if (dialog.open() == Window.OK) {
//
//			}
		});

		Button btnLinearFunction_1_1 = new Button(compositeTools, SWT.NONE);
		btnLinearFunction_1_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnLinearFunction_1_1.setText("Interpolation");

		Button btnLinearFunction_1_1_1 = new Button(compositeTools, SWT.NONE);
		btnLinearFunction_1_1_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnLinearFunction_1_1_1.setText("Reduction");
		xpndtmTools.setHeight(xpndtmTools.getControl().computeSize(SWT.DEFAULT, SWT.DEFAULT).y);

		setSelectionMode(chartViewer.getSelectionMode());
		setEditionMode(chartViewer.getEditionMode());

	}

	private void setSelectionMode(SelectionMode selectionMode) {
		chartViewer.setSelectionMode(selectionMode);
		switch (selectionMode) {
		case POLYGON_SELECTION:
			btnPolygonSelection.setSelection(true);
			btnRectangularSelection.setSelection(false);
			break;
		case RECTANGLE_SELECTION:
			btnPolygonSelection.setSelection(false);
			btnRectangularSelection.setSelection(true);
			break;
		default:
			btnPolygonSelection.setSelection(false);
			btnRectangularSelection.setSelection(false);
		}
	}

	private void setEditionMode(EditionMode editionMode) {
		chartViewer.setEditionMode(editionMode);
		switch (editionMode) {
		case VALIDATE:
			btnValidate.setSelection(true);
			btnInvalidate.setSelection(false);
			buttonZoom.setSelection(false);
			break;
		case INVALIDATE:
			btnValidate.setSelection(false);
			btnInvalidate.setSelection(true);
			buttonZoom.setSelection(false);
			break;
		case ZOOM:
			btnValidate.setSelection(false);
			btnInvalidate.setSelection(false);
			buttonZoom.setSelection(true);
			break;
		}
	}

	/**
	 * Dispose method
	 */
	@PreDestroy
	public void dispose() {
		/*
		 * Launcher.launchSyncRunnableWithProgress(monitor -> { monitor.beginTask("Time viewer closing...",
		 * inputFiles.asList().size()); for (ITimedVariableProvider input : inputFiles.asList()) {
		 * monitor.subTask("Close: " + input.getTitle() + "..."); if (input instanceof AutoCloseable) try {
		 * ((AutoCloseable) input).close(); } catch (Exception e) { LOGGER.error("Time viewer: close error: " +
		 * e.getMessage(), e); } monitor.worked(1); } });
		 */

		// if (mPart != null && partService != null)
		// partService.hidePart(mPart, true);
	}
}