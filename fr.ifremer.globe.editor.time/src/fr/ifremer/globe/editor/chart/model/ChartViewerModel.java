package fr.ifremer.globe.editor.chart.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.AbstractModelPublisher;
import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.core.model.chart.IChartSeriesStatusHandler;
import fr.ifremer.globe.core.model.chart.impl.LoadableChartSeries;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.model.navigation.utils.NavigationInterpolator;
import fr.ifremer.globe.editor.chart.dialog.toolbox.ChartEditorToolHistory;
import fr.ifremer.globe.editor.chart.model.ChartViewerModel.ChartViewerModelEvent;
import fr.ifremer.globe.utils.exception.GIOException;

public class ChartViewerModel
		extends AbstractModelPublisher<String, IChartSeries<? extends IChartData>, ChartViewerModelEvent>
		implements AutoCloseable {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChartViewerModel.class);

	private final Map<IChartSeriesDataSource, NavigationInterpolator> navigationBySource = new HashMap<>();

	/**
	 * Constructor
	 */
	public ChartViewerModel() {
		// use LinkedHashMap to keep insertion-order
		this.elements = new LinkedHashMap<>();
	}

	public void close() {
		removeAll(getAll());
	}

	/**
	 * @return key for a specific {@link IChartSeries}.
	 */
	@Override
	public String getKey(IChartSeries<? extends IChartData> series) {
		return series.getSource().getId() + "_" + series.getTitle();
	}

	/**
	 * @return all {@link IChartSeries} sorted by {@link IChartSeriesDataSource} and alphabetic order.
	 */
	public List<IChartSeries<? extends IChartData>> getAllSorted() {
		List<IChartSeries<? extends IChartData>> result = getAll();
		result.sort((IChartSeries<? extends IChartData> s1, IChartSeries<? extends IChartData> s2) -> {
			if (s1.getSource() != s2.getSource())
				return s1.getSource().getId().compareTo(s2.getSource().getId());
			return s1.getTitle().compareTo(s2.getTitle());
		});
		return result;
	}

	/**
	 * @return list of {@link IChartSeries} grouped by {@link IChartSeriesDataSource}.
	 */
	public Map<IChartSeriesDataSource, List<IChartSeries<? extends IChartData>>> getBySource() {
		return getAll().stream().collect(Collectors.groupingBy(IChartSeries::getSource));
	}

	/**
	 * @return {@link IChartSeries} with the specified source.
	 */
	public Optional<List<IChartSeries<? extends IChartData>>> getBySource(IChartSeriesDataSource source) {
		return Optional.ofNullable(getBySource().get(source));
	}

	public boolean contains(IChartSeries<? extends IChartData> series) {
		return elements.values().contains(series);
	}

	// ADD

	/**
	 * Overrides add method to get navigation if exists.
	 */
	@Override
	public boolean add(String key, IChartSeries<? extends IChartData> series) {
		addNavigationProvider(series);
		return super.add(key, series);
	}

	public void addAll(List<? extends IChartSeries<? extends IChartData>> seriesList) {
		var addedSeries = new ArrayList<IChartSeries<? extends IChartData>>();
		for (var series : seriesList) {
			boolean result = elements.putIfAbsent(getKey(series), series) == null;
			if (result) {
				addedSeries.add(series);
				addNavigationProvider(series);
			}
		}
		if (!addedSeries.isEmpty())
			submit(new ChartViewerModelEvent(ChartViewerModelEventType.ADD_CHART_SERIES, addedSeries));
	}

	// UPDATE

	@Override
	public void update(IChartSeries<? extends IChartData> series) {
		var key = getKey(series);
		get(key).ifPresent(k -> series.setColor(k.getColor()));
		update(key, series);
	}

	// REMOVE

	@Override
	public Optional<IChartSeries<? extends IChartData>> remove(String key) {
		var result = super.remove(key);
		result.map(IChartSeries::getSource).ifPresent(this::closeResources);
		return result;
	}

	/**
	 * Removes a series (without raise event).
	 */
	private void removeSilently(IChartSeries<? extends IChartData> series) {
		// no direct call to remove(key) to avoid multiple events
		elements.entrySet().stream().filter(entry -> entry.getValue().equals(series)).findFirst().map(Entry::getKey)
				.ifPresent(elements::remove);
	}

	/**
	 * Removes a list of {@link IChartSeries}
	 */
	public void removeAll(List<IChartSeries<? extends IChartData>> seriesList) {
		seriesList.forEach(this::removeSilently);
		seriesList.stream().map(IChartSeries::getSource).distinct().forEach(this::closeResources);
		submit(new ChartViewerModelEvent(ChartViewerModelEventType.REMOVE_CHART_SERIES, seriesList));
	}

	/**
	 * Removes all {@link IChartSeries} with the specified source.
	 */
	public void removeSource(IChartSeriesDataSource source) {
		getBySource(source).ifPresent(this::removeAll);
	}

	private void closeResources(IChartSeriesDataSource source) {
		if (getAll().stream().noneMatch(s -> s.getSource().equals(source))) {
			if (source instanceof AutoCloseable closeable)
				try {
					closeable.close();
				} catch (Exception e) {
					LOGGER.error("Error while closing series " + source.getId() + " : " + e.getMessage(), e);
				}

			Optional.ofNullable(navigationBySource.remove(source)).ifPresent(navData -> {
				try {
					navData.getNavigationData().close();
				} catch (IOException e) {
					LOGGER.error("Error while closing navigation : " + e.getMessage(), e);
				}
			});
		}
	}

	// MOVE

	public void moveToFront(IChartSeries<? extends IChartData> series) {
		removeSilently(series);
		elements.put(getKey(series), series);
		submit(new ChartViewerModelEvent(ChartViewerModelEventType.SERIES_ORDER_CHANGED, List.of(series)));
	}

	public Optional<NavigationInterpolator> getNavigationData(IChartSeries<? extends IChartData> series) {
		var source = series.getSource() instanceof ChartEditorToolHistory chartEditorToolHistory
				? chartEditorToolHistory.originSeries().getSource()
				: series.getSource();
		return Optional.ofNullable(navigationBySource.get(source));
	}

	/**
	 * @return saveable series (with a dirty {@link IChartSeriesStatusHandler} , or if it is a dirty
	 *         {@link LoadableChartSeries})
	 */
	public List<IChartSeries<? extends IChartData>> getSaveableSeries() {
		return getAll().stream()
				.filter(series -> series.getStatusHandler().map(IChartSeriesStatusHandler::isDirty).orElse(false)
						|| (series instanceof LoadableChartSeries loadableChartSeries && loadableChartSeries.isDirty()))
				.toList();
	}

	public List<IChartSeries<? extends IChartData>> getEnabledSeries() {
		return getAll().stream().filter(IChartSeries::isEnabled).toList();
	}

	public boolean isDirty() {
		return !getSaveableSeries().isEmpty();
	}

	public String print() {
		return "[" + elements.entrySet().stream().map(entry -> entry.getKey() + "=" + entry.getValue().isEnabled())
				.collect(Collectors.joining("\n")) + "]";
	}

	// Navigation providers

	private void addNavigationProvider(IChartSeries<? extends IChartData> series) {
		if (!navigationBySource.containsKey(series.getSource()))
			series.getSource().getFilePath().ifPresent(filePath -> INavigationService.grab()
					.getNavigationDataProvider(filePath.toString()).ifPresent(navSupplier -> {
						try {
							navigationBySource.put(series.getSource(),
									new NavigationInterpolator(navSupplier.getNavigationData(true)));
						} catch (GIOException e) {
							LOGGER.error("Error while getting navigation : " + e.getMessage(), e);
						}
					}));
	}

	// Events

	/** Event types **/
	public enum ChartViewerModelEventType {
		ADD_CHART_SERIES, UPDATE_CHART_SERIES, REMOVE_CHART_SERIES, SERIES_ORDER_CHANGED
	}

	/**
	 * Event is a record defined by a {@link ChartViewerModelEventType} and {@link IChartSeries} list.
	 */
	public record ChartViewerModelEvent(ChartViewerModelEventType type,
			List<IChartSeries<? extends IChartData>> series) {
	}

	public void notifyUpdate() {
		submit(new ChartViewerModelEvent(ChartViewerModelEventType.UPDATE_CHART_SERIES, Collections.emptyList()));
	}

	public void notifyUpdate(IChartSeries<? extends IChartData> series) {
		newUpdateEvent(series).ifPresent(this::submit);
	}

	@Override
	protected Optional<ChartViewerModelEvent> newAddEvent(IChartSeries<? extends IChartData> element) {
		return Optional.of(new ChartViewerModelEvent(ChartViewerModelEventType.ADD_CHART_SERIES, List.of(element)));
	}

	@Override
	protected Optional<ChartViewerModelEvent> newUpdateEvent(IChartSeries<? extends IChartData> element) {
		return Optional.of(new ChartViewerModelEvent(ChartViewerModelEventType.UPDATE_CHART_SERIES, List.of(element)));
	}

	@Override
	protected Optional<ChartViewerModelEvent> newRemovedEvent(IChartSeries<? extends IChartData> element) {
		return Optional.of(new ChartViewerModelEvent(ChartViewerModelEventType.REMOVE_CHART_SERIES, List.of(element)));
	}

}
