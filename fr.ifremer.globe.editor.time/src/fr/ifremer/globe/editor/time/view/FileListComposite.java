package fr.ifremer.globe.editor.time.view;

import java.util.List;
import java.util.function.Function;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.editor.time.TimeViewer;
import fr.ifremer.globe.editor.time.model.ITimedVariableProvider;

public class FileListComposite extends Composite {

	private TableViewer tableViewer;
	private Table table;
	private TableColumn profileTableColumn;
	private Label title;

	/**
	 * Constructor
	 */
	public FileListComposite(Composite parent, int style, TimeViewer timeViewer) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		List<ITimedVariableProvider> variableProviders = timeViewer.getVariableProviders();
		
		title = new Label(this, SWT.NONE);
		title.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.BOLD));
		title.setText("Files (" + variableProviders.size() + ")" );

		tableViewer = new TableViewer(this, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		TableViewerColumn profileTableViewerColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		profileTableViewerColumn.setLabelProvider(buildColumnLabelProvider(ITimedVariableProvider::getTitle));
		profileTableColumn = profileTableViewerColumn.getColumn();
		profileTableColumn.setWidth(350);
		profileTableColumn.setText("Name");
		
		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setInput(variableProviders);
	}
	
	private ColumnLabelProvider buildColumnLabelProvider(Function<ITimedVariableProvider, String> labelProvider) {
		return new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				return labelProvider.apply((ITimedVariableProvider) element);
			}

			@Override
			public Color getForeground(Object element) {
				return SWTResourceManager.getColor(0, 0, 0);
			}
		};
	}
}
