package fr.ifremer.globe.editor.time.view;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.ui.javafxchart.FXChart;
import fr.ifremer.globe.ui.javafxchart.utils.FXChartException;
import fr.ifremer.globe.ui.javafxchart.view.FXChartController.SelectionMode;

public class ChartComposite extends Composite {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChartComposite.class);

	/** Properties **/
	private FXChart chart;

	/** Constructor **/
	public ChartComposite(Composite parent, int style) {
		super(parent, style);
		createUI();
	}

	/**
	 * Creates user interface
	 */
	private void createUI() {
		try {
			// Create charts
			chart = new FXChart(this, "");
			DateFormat format = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
			format.setTimeZone(TimeZone.getTimeZone("GMT"));
			chart.setXAxisLabelFormatter(d -> format.format(new Date(d.longValue())));
			chart.setVerticalZoomEnabled(false);
			chart.setSelectionMode(SelectionMode.ZOOM);
			
		} catch (IOException | FXChartException e) {
			LOGGER.error("Error during chart creation", e);
		}
	}

	/**
	 * Reloads chart
	 */
	public void refresh() {
		chart.refresh();
	}

	/**
	 * Resets chart zoom
	 */
	public void resetZoom() {
		chart.resetZoom();
	}

	/**
	 * Sets chart data
	 */
	public void setChartData(List<IChartSeries<? extends IChartData>> series) {
		chart.setData(series);
	}

	public void setTitle(String title) {
		chart.setTitle(title);
	}
}
