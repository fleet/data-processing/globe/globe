package fr.ifremer.globe.editor.time.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.editor.time.TimeViewer;

public class VariableGroupComposite extends Composite {

	private Composite variableGroup;
	private Button btnNewVariable;

	/**
	 * Constructor
	 */
	public VariableGroupComposite(Composite parent, int style, TimeViewer timeViewer) {
		super(parent, style);
		setLayout(new GridLayout(1, false));

		// title
		Label lblTitle = new Label(this, SWT.NONE);
		lblTitle.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.BOLD));
		lblTitle.setText("Variables");

		variableGroup = new Composite(this, SWT.NONE);
		GridLayout glVariableCompositeContainer = new GridLayout(1, false);
		glVariableCompositeContainer.marginWidth = 0;
		variableGroup.setLayout(glVariableCompositeContainer);
		variableGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		VariableComposite variableComposite1 = new VariableComposite(variableGroup, SWT.NONE, timeViewer);
		variableComposite1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		// button new variable
		btnNewVariable = new Button(this, SWT.NONE);
		btnNewVariable.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.editor.time", "icons/add.png"));
		btnNewVariable.setText("New variable");
		btnNewVariable.addListener(SWT.Selection, e -> {
			VariableComposite variableComposite = new VariableComposite(variableGroup, SWT.NONE, timeViewer);
			variableComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			resize();
		});

	}

	protected void resize() {
		getParent().layout();
		btnNewVariable.setVisible(variableGroup.getChildren().length < 5);
	}

}
