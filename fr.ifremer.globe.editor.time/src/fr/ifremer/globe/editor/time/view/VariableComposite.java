package fr.ifremer.globe.editor.time.view;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.editor.time.TimeViewer;
import fr.ifremer.globe.editor.time.model.TimedVariable;
import fr.ifremer.globe.editor.time.model.TimedVariable2D;
import fr.ifremer.globe.ui.utils.color.ColorUtils;

public class VariableComposite extends Composite {

	/** Properties **/
	private final TimeViewer timeViewer;
	private ComboViewer comboViewerVariable;
	private Label lblDescription;
	private Button btnNewButton;
	private Combo comboSecondDimIndex;
	private ComboViewer comboViewerSecondDimIndex;
	private Composite colorSelectorcomposite;
	private Label lbldIndex;

	private TimedVariable selectedVariable;
	private Color color = SWTResourceManager.getColor(SWT.COLOR_DARK_BLUE);

	/**
	 * Constructor : creates UI
	 */
	public VariableComposite(Composite parent, int style, TimeViewer timeViewer) {
		super(parent, SWT.BORDER);
		this.timeViewer = timeViewer;
		setLayout(new GridLayout(4, false));

		// Get variables IDs
		Set<TimedVariable> variables = new HashSet<>();
		if (timeViewer != null) {
			this.color = ColorUtils.convertJavaFXToSWT(timeViewer.getAvailableColor());
			timeViewer.getVariableProviders().forEach(vp -> {
				loop: for (TimedVariable newVar : vp.getVariables()) {
					for (TimedVariable var : variables) {
						if (newVar.getTitle().equals(var.getTitle()))
							continue loop;
					}
					variables.addAll(vp.getVariables());
				}
			});
		}

		// color selector
		colorSelectorcomposite = new Composite(this, SWT.BORDER);
		colorSelectorcomposite.setCursor(SWTResourceManager.getCursor(SWT.CURSOR_HAND));
		GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_composite.widthHint = 35;
		gd_composite.heightHint = 10;
		colorSelectorcomposite.setLayoutData(gd_composite);
		colorSelectorcomposite.setBackground(color);
		colorSelectorcomposite.addListener(SWT.MouseDown, e -> openChangeColorDialog());

		// combo box to select variable
		comboViewerVariable = new ComboViewer(this, SWT.READ_ONLY);
		Combo combo = comboViewerVariable.getCombo();
		GridData gd_combo = new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1);
		combo.setLayoutData(gd_combo);
		comboViewerVariable.setContentProvider(ArrayContentProvider.getInstance());
		comboViewerVariable.setComparator(new ViewerComparator());
		comboViewerVariable.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return element instanceof TimedVariable ? ((TimedVariable) element).getTitle() : super.getText(element);
			}
		});
		comboViewerVariable.setInput(variables);
		comboViewerVariable.addSelectionChangedListener(this::onVariableSelectionChange);

		// delete button
		btnNewButton = new Button(this, SWT.NONE);
		btnNewButton.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
		btnNewButton.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.editor.time", "icons/delete.png"));
		btnNewButton.addListener(SWT.Selection, e -> {
			VariableComposite.this.dispose();
			timeViewer.onVariableSelectionChanged(this);
			((VariableGroupComposite) parent.getParent()).resize();
		});

		// second dimension index selection
		lbldIndex = new Label(this, SWT.NONE);
		lbldIndex.setEnabled(false);
		lbldIndex.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 2, 1));
		lbldIndex.setText("Second dimension index (optional): ");
		comboViewerSecondDimIndex = new ComboViewer(this, SWT.READ_ONLY);
		comboViewerSecondDimIndex.setContentProvider(ArrayContentProvider.getInstance());
		comboSecondDimIndex = comboViewerSecondDimIndex.getCombo();
		comboSecondDimIndex.setEnabled(false);
		comboSecondDimIndex.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		comboViewerSecondDimIndex.addSelectionChangedListener(e -> timeViewer.onVariableSelectionChanged(this));
		new Label(this, SWT.NONE);

		// description label
		lblDescription = new Label(this, SWT.WRAP);
		lblDescription.setForeground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_BORDER));
		GridData gd_lblDescription = new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1);
		gd_lblDescription.widthHint = 320;
		gd_lblDescription.minimumWidth = 100;
		lblDescription.setLayoutData(gd_lblDescription);
	}

	/**
	 * Called when variable selection changes.
	 */
	private void onVariableSelectionChange(SelectionChangedEvent e) {
		IStructuredSelection selection = (IStructuredSelection) e.getSelection();
		if (selection.size() > 0) {
			selectedVariable = ((TimedVariable) selection.getFirstElement());
			lblDescription.setText(selectedVariable.getDescription());
			// lblDescription.pack();
			((VariableGroupComposite) getParent().getParent()).resize();
			VariableComposite.this.layout();

			if (selectedVariable instanceof TimedVariable2D) {
				lbldIndex.setEnabled(true);
				comboSecondDimIndex.setEnabled(true);
				List<Integer> values = IntStream.range(0, ((TimedVariable2D) selectedVariable).getSecondDimensionSize())
						.boxed().collect(Collectors.toList());
				comboViewerSecondDimIndex.setInput(values);
			} else {
				lbldIndex.setEnabled(false);
				comboSecondDimIndex.setEnabled(false);
			}
			comboViewerSecondDimIndex.getCombo().select(0);

			timeViewer.onVariableSelectionChanged(this);
		}
	}

	/**
	 * Opens change color dialog.
	 */
	private void openChangeColorDialog() {
		ColorDialog colorDialog = new ColorDialog(getShell());
		colorDialog.setRGB(color.getRGB());
		RGB rgb = colorDialog.open();
		if (rgb != null)
			setColor(new Color(getDisplay(), rgb));
	}

	// PUBLIC API

	public TimedVariable getSelectedVariable() {
		return selectedVariable;
	}

	public int getSecondDimIndex() {
		return comboSecondDimIndex.isEnabled()
				? (int) comboViewerSecondDimIndex.getStructuredSelection().getFirstElement()
				: 0;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color c) {
		color = c;
		colorSelectorcomposite.setBackground(color);
		timeViewer.onVariableSelectionChanged(this);
	}

}
