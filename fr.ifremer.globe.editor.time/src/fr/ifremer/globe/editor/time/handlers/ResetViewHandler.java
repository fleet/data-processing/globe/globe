package fr.ifremer.globe.editor.time.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.editor.time.TimeViewer;

public class ResetViewHandler {

	@Execute
	public void execute(EPartService partService) {
		TimeViewer part = (TimeViewer) partService.getActivePart().getObject();
		part.resetZoom();
	}

}
