package fr.ifremer.globe.editor.time.handlers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.inject.Named;

import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.io.techsas.TechsasInfo;
import fr.ifremer.globe.core.io.tide.TideFileInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.editor.time.TimeViewer;
import fr.ifremer.globe.editor.time.model.ITimedVariableProvider;
import fr.ifremer.globe.editor.time.model.impl.SounderFileTimedVariableProvider;
import fr.ifremer.globe.editor.time.model.impl.TechsasTimedVariableProvider;
import fr.ifremer.globe.editor.time.model.impl.TideTimedVariableProvider;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.StoreUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

/**
 * Handler to open the Time Viewer.
 */
public class OpenTimeViewerHandler extends AbstractNodeHandler {

	private MItem callerItem;

	/**
	 * @return true if the handler is called from the project explorer
	 */
	private boolean openedFromProjectExplorer() {
		return callerItem.getParent().getElementId().equals("fr.ifremer.globe.ui.projectexplorer.popupmenu.openwith");
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return !openedFromProjectExplorer()
				|| !getSelectionAsList(getSelection(selectionService), ContentType::isManageableInSwathEditor).isEmpty()
				|| !getSelectionAsList(getSelection(selectionService), ContentType.TIDE_CSV::equals).isEmpty()
				|| !getSelectionAsList(getSelection(selectionService), ContentType.TECHSAS_NETCDF::equals).isEmpty();
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService, EPartService partService,
			@Active MItem item) {
		this.callerItem = item;
		return checkExecution(selectionService, modelService);
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, ESelectionService selectionService,
			MApplication application, EModelService modelService, EPartService partService, @Active MItem item) {

		// Get input files
		List<ISounderNcInfo> inputs;
		// Get selected files if launch with "execute with"
		if (openedFromProjectExplorer()) {
			List<FileInfoNode> nodeList = getSelectionAsList(getSelection(selectionService),
					ContentType::isManageableInSwathEditor);
			inputs = StoreUtils.getInfoStoresFromNodes(nodeList).stream().map(info -> (ISounderNcInfo) info)
					.collect(Collectors.toList());
		}
		// Else open a FileDialog to select input files
		else {
			return; // TODO implement a generic class to load files (and use it with Line / Navigation editors)
		}

		// import mbg/xsf files
		List<ITimedVariableProvider> inputVariableProvider = new ArrayList<>();
		IProcessService.grab().runInForeground("Time viewer opening...", (monitor, l) -> {
			monitor.beginTask("Time viewer loading...", 2 * inputs.size());
			for (ISounderNcInfo info : inputs) {
				monitor.subTask("Loading: " + info.getPath() + "...");
				monitor.worked(1);
				inputVariableProvider.add(new SounderFileTimedVariableProvider(info));
				if (monitor.isCanceled())
					throw new OperationCanceledException();
				monitor.worked(1);
			}
			return Status.OK_STATUS;
		});

		// import techsas files
		getSelectionAsList(selectionService, TechsasInfo.class).forEach(ttbFileInfo -> //
		{
			try {
				inputVariableProvider.add(new TechsasTimedVariableProvider(ttbFileInfo));
			} catch (Exception e) {
				Messages.openErrorMessage("Time viewer opening",
						"File " + ttbFileInfo.getBaseName() + " can't be open in time viewer.");
				e.printStackTrace();
			}
		});

		// import tide files
		getSelectionAsList(selectionService, TideFileInfo.class).forEach(ttbFileInfo -> //
		inputVariableProvider.add(new TideTimedVariableProvider(ttbFileInfo.getBaseName(), ttbFileInfo.getData())));

		// Open part
		if (!inputVariableProvider.isEmpty()) {
			Display.getDefault().asyncExec(() -> {
				MPart part = partService.createPart(TimeViewer.PART_ID);
				IEclipseContext context = application.getContext();
				context.set("inputFiles", inputVariableProvider);
				context.set("mPart", part);
				PartManager.addEditableBindingContextToPart(part, application);
				PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_STACK_ID);
				PartManager.showPart(partService, part);
			});
		}
	}

}