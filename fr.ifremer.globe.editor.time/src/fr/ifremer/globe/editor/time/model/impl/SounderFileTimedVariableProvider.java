package fr.ifremer.globe.editor.time.model.impl;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.editor.time.model.TimedVariable;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides method to get {@link TimedVariable} from {@link SounderDataContainer}.
 */
public class SounderFileTimedVariableProvider extends DataContainerVariableProvider {

	/** Properties **/
	private final ISounderDataContainerToken sounderDataContainerToken;
	private final int swathCount;
	private final LongLayer1D timeLayer;
	protected final List<Integer> validIndexes = new ArrayList<>();

	/**
	 * Constructor
	 */
	public SounderFileTimedVariableProvider(ISounderNcInfo info) throws GIOException {
		super(info);
		// Get data container
		sounderDataContainerToken = IDataContainerFactory.grab().book(info, this);
		var sounderDataContainer = sounderDataContainerToken.getDataContainer();

		// get variables
		swathCount = sounderDataContainer.getInfo().getCycleCount();
		timeLayer = sounderDataContainer.getLayer(BeamGroup1Layers.PING_TIME);

		// Keep valid swath
		DetectionStatusLayer statusLayer = sounderDataContainer.getStatusLayer();
		for (int i = 0; i < info.getCycleCount(); i++) {
			if (statusLayer.isSwathValid(i))
				validIndexes.add(i);
		}

		initializeTimedVariables();
	}

	/** Close method **/
	public void close() {
		super.close();
		if (sounderDataContainerToken != null)
			sounderDataContainerToken.close();
	}

	@Override
	protected int getSize() {
		return validIndexes.size();
	}

	protected boolean isLayerToDiplay(IBaseLayer layer) {
		return layer.getDimensions().length > 0 && layer.getDimensions()[0] == swathCount;
	}

	@Override
	protected long getTime(int index) {
		return DateUtils.nanoSecondToMilli(timeLayer.get(validIndexes.get(index)));
	}

}
