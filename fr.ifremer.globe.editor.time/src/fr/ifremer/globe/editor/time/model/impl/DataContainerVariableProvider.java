package fr.ifremer.globe.editor.time.model.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.Group;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.layers.AbstractBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.BooleanLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ILoadableLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ShortLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.IAbstractBaseLayerOperation;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerToken;
import fr.ifremer.globe.editor.time.model.ITimedVariableProvider;
import fr.ifremer.globe.editor.time.model.TimedVariable;
import fr.ifremer.globe.editor.time.model.TimedVariable1D;
import fr.ifremer.globe.editor.time.model.TimedVariable2D;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides method to get {@link TimedVariable} from {@link DataContainer}.
 */
public abstract class DataContainerVariableProvider
		implements ITimedVariableProvider, IAbstractBaseLayerOperation, IDataContainerOwner, AutoCloseable {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataContainerVariableProvider.class);

	/** Properties **/
	protected final IDataContainerToken<IDataContainerInfo> dataContainerToken;

	/** {@link TimedVariable} list **/
	private final List<TimedVariable> variables = new ArrayList<>();

	private final String title;

	/**
	 * Constructor
	 */
	protected DataContainerVariableProvider(IDataContainerInfo info) throws GIOException {
		title = info.getBaseName();
		// Get data container
		dataContainerToken = IDataContainerFactory.grab().book(info, this);
	}

	/** Close method **/
	public void close() {
		if (dataContainerToken != null)
			dataContainerToken.close();
	}

	/**
	 * Initializes timed variable for suitable layers.
	 * 
	 * @throws GIOException
	 */
	protected void initializeTimedVariables() throws GIOException {
		for (Group group : dataContainerToken.getDataContainer().getGroups()) {
			for (ILoadableLayer<?> layer : group.getLayers().values()) {
				if (isLayerToDiplay(layer))
					layer.process(this);
			}
		}
	}

	protected boolean isLayerToDiplay(IBaseLayer layer) {
		return true;
	}

	protected abstract int getSize();

	protected int getValidIndex(int index) {
		return index;
	}

	/**
	 * @return time in epoch milliseconds
	 */
	protected abstract long getTime(int index);

	// Getters

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public List<TimedVariable> getVariables() {
		return variables;
	}

	/**
	 * @return an {@link Optional} with the variable matching with the parameter, or an empty optional.
	 */
	@Override
	public Optional<TimedVariable> getVariable(String variableId) {
		for (TimedVariable var : variables) {
			if (var.getTitle().equals(variableId))
				return Optional.of(var);
		}
		return Optional.empty();
	}

	// Private methods

	@FunctionalInterface
	private interface IntToDoubleFunctionWithGIOException {
		double applyAsDouble(int value) throws GIOException;
	}

	/**
	 * Creates a {@link TimedVariable1D} from a 1D Layer.
	 */
	private void addVariable(AbstractBaseLayer<?> layer, IntToDoubleFunctionWithGIOException valueSupplier) {
		String title = layer.getName() + " [" + layer.getUnit() + "]";

		variables.add(new TimedVariable1D(title, layer.getLongName(), getSize(), this::getTime,
				// Value supplier
				index -> {
					try {
						// loads layers if needed
						if (layer instanceof ILoadableLayer && !((ILoadableLayer<?>) layer).isLoaded()) {
							((ILoadableLayer<?>) layer).load();
						}
						return valueSupplier.applyAsDouble(getValidIndex(index));
					} catch (GIOException e) {
						logErrorGettingData(e);
					}
					return 0; // should not happen
				}));
	}

	@FunctionalInterface
	public interface ToDoubleFunctionWithGIOException {
		double applyAsDouble(int firstIndex, int secondIndex) throws GIOException;
	}

	/**
	 * Creates a {@link TimedVariable1D} from a 2D Layer.
	 */
	private void addVariable2D(AbstractBaseLayer<?> layer, ToDoubleFunctionWithGIOException valueSupplier) {
		String title = layer.getName() + " [" + layer.getUnit() + "]";

		variables.add(new TimedVariable2D(title, layer.getLongName(), getSize(), (int) layer.getDimensions()[1],
				this::getTime,
				// Value supplier
				indexes -> {
					try {
						// loads layers if needed
						if (layer instanceof ILoadableLayer && !((ILoadableLayer<?>) layer).isLoaded()) {
							((ILoadableLayer<?>) layer).load();
						}
						return valueSupplier.applyAsDouble(getValidIndex(indexes.getFirst()), indexes.getSecond());
					} catch (GIOException e) {
						logErrorGettingData(e);
					}
					return 0; // should not happen
				}));
	}

	/**
	 * Logs errors
	 */
	protected void logErrorGettingData(GIOException e) {
		LOGGER.error("Error while getting data : " + e.getMessage(), e);
	}

	/*
	 * USE OF VISITOR DESIGN PATTERN TO BUILD TIME VARIABLES
	 */

	@Override
	public void visit(BooleanLayer1D layer) throws GIOException {
		// not managed
	}

	@Override
	public void visit(ByteLayer1D layer) throws GIOException {
		addVariable(layer, layer::get);
	}

	@Override
	public void visit(ShortLayer1D layer) throws GIOException {
		addVariable(layer, layer::get);
	}

	@Override
	public void visit(IntLayer1D layer) throws GIOException {
		addVariable(layer, layer::get);
	}

	@Override
	public void visit(LongLayer1D layer) throws GIOException {
		addVariable(layer, layer::get);
	}

	@Override
	public void visit(FloatLayer1D layer) throws GIOException {
		addVariable(layer, layer::get);
	}

	@Override
	public void visit(DoubleLayer1D layer) throws GIOException {
		addVariable(layer, layer::get);
	}

	@Override
	public void visit(BooleanLayer2D layer) throws GIOException {
		// not managed
	}

	@Override
	public void visit(ByteLayer2D layer) throws GIOException {
		addVariable2D(layer, layer::get);
	}

	@Override
	public void visit(ShortLayer2D layer) throws GIOException {
		addVariable2D(layer, layer::get);
	}

	@Override
	public void visit(IntLayer2D layer) throws GIOException {
		addVariable2D(layer, layer::get);
	}

	@Override
	public void visit(LongLayer2D layer) throws GIOException {
		addVariable2D(layer, layer::get);
	}

	@Override
	public void visit(FloatLayer2D layer) throws GIOException {
		addVariable2D(layer, layer::get);
	}

	@Override
	public void visit(DoubleLayer2D layer) throws GIOException {
		addVariable2D(layer, layer::get);
	}

}
