package fr.ifremer.globe.editor.time.model;

import java.util.function.IntToLongFunction;
import java.util.function.ToDoubleFunction;

import fr.ifremer.globe.core.utils.Pair;

/**
 * Timed variable with a second dimension.
 */
public class TimedVariable2D extends TimedVariable {

	private final ToDoubleFunction<Pair<Integer, Integer>> variableSupplier;
	private final int secondDimensionSize;
	private int secondDimensionIndex;

	public TimedVariable2D(String title, String desc, int size, int secondDimSize, IntToLongFunction timeSupplier,
			ToDoubleFunction<Pair<Integer, Integer>> variableSupplier) {
		super(title, desc, size, timeSupplier);
		this.secondDimensionSize = secondDimSize;
		this.variableSupplier = variableSupplier;
	}

	@Override
	public double getY(int index) {
		return variableSupplier.applyAsDouble(new Pair<>(index, secondDimensionIndex));
	}

	public void setSecondDimensionIndex(int index) {
		secondDimensionIndex = index;
	}

	public int getSecondDimensionSize() {
		return secondDimensionSize;
	}

}
