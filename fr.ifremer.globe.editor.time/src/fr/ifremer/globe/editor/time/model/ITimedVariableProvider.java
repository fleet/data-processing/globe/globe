package fr.ifremer.globe.editor.time.model;

import java.util.List;
import java.util.Optional;

public interface ITimedVariableProvider {
	
	public String getTitle();
	
	public List<TimedVariable> getVariables();

	public Optional<TimedVariable> getVariable(String variableId);

}
