package fr.ifremer.globe.editor.time.model.impl;

import java.util.List;
import java.util.Optional;

import fr.ifremer.globe.core.model.tide.ITideData;
import fr.ifremer.globe.editor.time.model.ITimedVariableProvider;
import fr.ifremer.globe.editor.time.model.TimedVariable;
import fr.ifremer.globe.editor.time.model.TimedVariable1D;

public class TideTimedVariableProvider implements ITimedVariableProvider {

	private final String title;
	private final List<TimedVariable> variables;

	public TideTimedVariableProvider(String title, List<ITideData> tideData) {
		super();
		this.title = title;
		this.variables = List.of(new TimedVariable1D(title, "Tide data", tideData.size(),
				i -> tideData.get(i).getTimestamp().toEpochMilli(), i -> tideData.get(i).getValue()));
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public List<TimedVariable> getVariables() {
		return variables;
	}

	@Override
	public Optional<TimedVariable> getVariable(String variableId) {
		return variables.get(0).getTitle().equals(variableId) ? Optional.of(variables.get(0)) : Optional.empty();
	}

}
