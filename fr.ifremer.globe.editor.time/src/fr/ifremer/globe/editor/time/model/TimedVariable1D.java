package fr.ifremer.globe.editor.time.model;

import java.util.function.IntToDoubleFunction;
import java.util.function.IntToLongFunction;

public class TimedVariable1D extends TimedVariable {

	private final IntToDoubleFunction variableSupplier;

	public TimedVariable1D(String title, String desc, int size, IntToLongFunction timeSupplier,
			IntToDoubleFunction variableSupplier) {
		super(title, desc, size, timeSupplier);
		this.variableSupplier = variableSupplier;
	}

	@Override
	public double getY(int index) {
		return variableSupplier.applyAsDouble(index);
	}

}
