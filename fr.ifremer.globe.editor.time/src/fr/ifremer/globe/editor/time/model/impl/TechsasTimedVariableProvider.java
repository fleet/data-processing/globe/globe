package fr.ifremer.globe.editor.time.model.impl;

import java.util.ArrayList;
import java.util.List;

import fr.ifremer.globe.core.io.techsas.TechsasConstants;
import fr.ifremer.globe.core.io.techsas.TechsasInfo;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IBaseLayer;
import fr.ifremer.globe.editor.time.model.TimedVariable;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class provides method to get {@link TimedVariable} from {@link TechsasInfo}.
 */
public class TechsasTimedVariableProvider extends DataContainerVariableProvider {

	/** Properties **/
	private final int size;
	private final DoubleLoadableLayer1D timeLayer;
	protected final List<Integer> validIndexes = new ArrayList<>();

	/**
	 * Constructor
	 */
	public TechsasTimedVariableProvider(TechsasInfo info) throws GIOException {
		super(info);
		timeLayer = dataContainerToken.getDataContainer().getLayer(TechsasConstants.TIME);
		size = (int) timeLayer.getDimensions()[0];
		// Keep valid times
		for (int i = 0; i < size; i++) {
			if (timeLayer.get(i) > 0)
				validIndexes.add(i);
		}

		initializeTimedVariables();
	}

	protected boolean isLayerToDiplay(IBaseLayer layer) {
		return layer.getDimensions().length > 0 && layer.getDimensions()[0] == size;
	}

	@Override
	protected int getSize() {
		return validIndexes.size();
	}

	@Override
	protected long getTime(int index) {
		return DateUtils.daysFrom1899_12_30ToInstant(timeLayer.get(validIndexes.get(index))).toEpochMilli();
	}

}
