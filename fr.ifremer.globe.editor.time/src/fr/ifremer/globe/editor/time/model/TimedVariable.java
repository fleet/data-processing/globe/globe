package fr.ifremer.globe.editor.time.model;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.function.IntToLongFunction;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesDataSource;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.javafxchart.FXChart;
import fr.ifremer.globe.utils.date.DateUtils;
import javafx.scene.paint.Color;

/**
 * Time stamped variable, implements {@link IChartSeries} to be displayed in an {@link FXChart}.
 */
public abstract class TimedVariable implements IChartSeries<IChartData>, Cloneable {

	private final String title;
	private final String description;
	private final int size;
	private final IntToLongFunction timeSupplier;
	private final DecimalFormat df = new DecimalFormat("0.00");
	private GColor color = new GColor(Color.DARKSLATEBLUE.toString());
	private boolean enabled = true;

	public TimedVariable(String title, String desc, int size, IntToLongFunction timeSupplier) {
		super();
		this.title = title;
		this.description = desc;
		this.size = size;
		this.timeSupplier = timeSupplier;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public IChartSeriesDataSource getSource() {
		return IChartSeriesDataSource.UNKNOWN; // not used here
	}

	public String getDescription() {
		return description;
	}

	@Override
	public GColor getColor() {
		return color;
	}

	public void setColor(GColor color) {
		this.color = color;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Abstract method to implement : defines how Y values are retrieved.
	 */
	public abstract double getY(int index);

	@Override
	public IChartData getData(int index) {

		return new IChartData() {

			@Override
			public IChartSeries<IChartData> getSeries() {
				return TimedVariable.this;
			}

			@Override
			public int getIndex() {
				return index;
			}

			@Override
			public double getX() {
				return timeSupplier.applyAsLong(index);
			}

			@Override
			public double getY() {
				return TimedVariable.this.getY(index);
			}

			@Override
			public String getTooltip() {
				String date = DateUtils.formatDate(new Date(timeSupplier.applyAsLong(index)));
				return TimedVariable.this.getTitle() + "\tindex: " + index + "\tdate: " + date + "\tvalue : "
						+ df.format(getY());
			}

			@Override
			public GColor getColor() {
				return TimedVariable.this.getColor();
			}

		};
	}

	/**
	 * @return a clone of the current object.
	 */
	public TimedVariable copy() {
		try {
			return (TimedVariable) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new AssertionError(); // Can't happen
		}
	}
}
