package fr.ifremer.globe.editor.time;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.Status;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.core.utils.color.ColorProvider;
import fr.ifremer.globe.editor.time.model.ITimedVariableProvider;
import fr.ifremer.globe.editor.time.model.TimedVariable;
import fr.ifremer.globe.editor.time.model.TimedVariable2D;
import fr.ifremer.globe.editor.time.view.ChartComposite;
import fr.ifremer.globe.editor.time.view.FileListComposite;
import fr.ifremer.globe.editor.time.view.VariableComposite;
import fr.ifremer.globe.editor.time.view.VariableGroupComposite;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import javafx.scene.paint.Color;

/**
 * Time viewer
 */
public class TimeViewer {

	private static final Logger LOGGER = LoggerFactory.getLogger(TimeViewer.class);

	public static final String PART_ID = "fr.ifremer.globe.editor.time.part";

	@Inject
	private EPartService partService;
	private MPart mPart;

	// Model
	private ChartComposite chartComposite;
	private final WritableObjectList<ITimedVariableProvider> inputFiles;
	private final ColorProvider colorProvider;
	private final Map<VariableComposite, List<TimedVariable>> variableMap = new HashMap<>();

	/**
	 * Constructor
	 */
	@Inject
	public TimeViewer(EPartService partService, @Named("inputFiles") List<ITimedVariableProvider> inputs,
			@Named("mPart") MPart mPart) {
		this.mPart = mPart;

		colorProvider = new ColorProvider();
		this.partService = partService;
		this.inputFiles = new WritableObjectList<>(inputs);
	}

	/**
	 * Initialization method: creates UI
	 */
	@PostConstruct
	public void createUI(Composite parent) {
		parent.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		parent.setLayout(new GridLayout(2, false));

		// chart
		chartComposite = new ChartComposite(parent, SWT.NONE);
		chartComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 2));
		chartComposite.setLayout(new FillLayout(SWT.HORIZONTAL));

		// variable list
		VariableGroupComposite variableListComposite = new VariableGroupComposite(parent, SWT.FILL, this);
		GridData gd_variableListComposite = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		// gd_variableListComposite.widthHint = 300;
		variableListComposite.setLayoutData(gd_variableListComposite);

		// file list
		FileListComposite fileListComposite = new FileListComposite(parent, SWT.FILL, this);
		fileListComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
	}

	/**
	 * Dispose method
	 */
	@PreDestroy
	public void dispose() {
		IProcessService.grab().runInForeground("Time viewer closing...", (monitor, logger) -> {
			monitor.beginTask("Time viewer closing...", inputFiles.asList().size());
			for (ITimedVariableProvider input : inputFiles.asList()) {
				monitor.subTask("Close: " + input.getTitle() + "...");
				if (input instanceof AutoCloseable)
					try {
						((AutoCloseable) input).close();
					} catch (Exception e) {
						logger.error("Time viewer: close error: " + e.getMessage(), e);
						return Status.error("Time viewer: close error: " + e.getMessage(), e);
					}
				monitor.worked(1);
			}
			return Status.OK_STATUS;
		});

		if (mPart != null && partService != null)
			partService.hidePart(mPart, true);
	}

	public List<ITimedVariableProvider> getVariableProviders() {
		return inputFiles.asList();
	}

	public Color getAvailableColor() {
		return ColorUtils.convertGColorToJavaFXColor(colorProvider.get());
	}

	/**
	 * Refreshes chart
	 */
	public void refreshChart() {
		List<IChartSeries<? extends IChartData>> variableSeriesProxies = new ArrayList<>();
		variableMap.values().forEach(variableSeriesProxies::addAll);
		chartComposite.setChartData(variableSeriesProxies);
	}

	/**
	 * Resets chart zoom
	 */
	public void resetZoom() {
		chartComposite.resetZoom();
	}

	public void onVariableSelectionChanged(VariableComposite variableComposite) {
		// remove previous series
		if (variableMap.remove(variableComposite) != null)
			refreshChart();

		// compute and display new series
		if (!variableComposite.isDisposed() && variableComposite.getSelectedVariable() != null) {
			List<TimedVariable> variableList = new ArrayList<>();
			for (ITimedVariableProvider variableProvider : inputFiles.asList()) {
				Optional<TimedVariable> optVar = variableProvider
						.getVariable(variableComposite.getSelectedVariable().getTitle());
				if (optVar.isPresent()) {
					TimedVariable var = optVar.get();

					// copy the timed variable if it's already displayed (to display 2 series with different 2D index)
					if (isTimedVariableDisplayed(var))
						var = var.copy();

					var.setColor(ColorUtils.convertSWTToGColor(variableComposite.getColor()));
					variableList.add(var);
					if (var instanceof TimedVariable2D) {
						((TimedVariable2D) var).setSecondDimensionIndex(variableComposite.getSecondDimIndex());
					}
				}
			}
			variableMap.put(variableComposite, variableList);
			refreshChart();
		}
	}

	private boolean isTimedVariableDisplayed(TimedVariable timedVariable) {
		for (List<TimedVariable> varList : variableMap.values()) {
			if (varList.contains(timedVariable))
				return true;
		}
		return false;
	}

}