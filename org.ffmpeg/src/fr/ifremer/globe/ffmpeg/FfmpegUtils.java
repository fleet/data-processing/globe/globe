/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ffmpeg;

import java.io.File;
import java.io.FilenameFilter;

import org.apache.commons.io.filefilter.RegexFileFilter;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ffmpeg.model.Codec;
import fr.ifremer.globe.ffmpeg.model.Container;
import fr.ifremer.globe.ffmpeg.model.EncodingParameters;
import fr.ifremer.globe.ffmpeg.model.FrameSize;
import fr.ifremer.globe.ffmpeg.model.Framerate;
import fr.ifremer.globe.ffmpeg.model.H264Preset;

/**
 * FFMPEG utilities
 */
public class FfmpegUtils {

	/**
	 * Constructor.
	 */
	public FfmpegUtils() {
	}

	public static boolean capturedFilesExists(File inputDirectory) {
		return listRawFiles(inputDirectory).length > 0 || listPngFiles(inputDirectory).length > 0;
	}

	public static File[] listRawFiles(File inputDirectory) {
		return inputDirectory.listFiles((FilenameFilter) new RegexFileFilter("img-(\\d+)?\\.raw"));
	}

	public static File getRawFile(File inputDirectory, int fileIndex) {
		return new File(inputDirectory, String.format("img-%d.raw", fileIndex));
	}

	public static File[] listPngFiles(File inputDirectory) {
		return inputDirectory.listFiles((FilenameFilter) new RegexFileFilter("img-(\\d+)?\\.png"));
	}

	public static void encodeVideo(String videoName, File imagePath, Framerate inputFramerate) {
		encodeVideo(null, videoName, imagePath, inputFramerate);
	}

	/**
	 * Perform a encoding of images to one video
	 */
	public static void encodeVideo(Shell shell, String videoName, File imagePath, Framerate inputFramerate) {
		EncodingParameters encodingParameters = new EncodingParameters();
		encodingParameters.setInputDirectory(imagePath);
		encodingParameters.setVideoPath(
				new File(imagePath, (videoName != null && !videoName.isEmpty() ? videoName : "Capture") + ".avi"));
		encodingParameters.setCodec(Codec.H264);
		encodingParameters.setContainer(Container.Avi);
		encodingParameters.setInputFramerate(inputFramerate != null ? inputFramerate : Framerate.T1_2);
		encodingParameters.setOutputFramerate(inputFramerate != null ? inputFramerate : Framerate.T25);
		encodingParameters.setFrameSize(FrameSize.getDefault());
		encodingParameters.setPreset(H264Preset.getDefault());

		Display.getDefault().asyncExec(() -> {
			FfmpegDialog ffmpegDialog = new FfmpegDialog(shell, encodingParameters);
			if (ffmpegDialog.open() == Window.OK) {
				FfmpegEncoder ffmpegEncoder = new FfmpegEncoder();
				ffmpegEncoder.encode(shell, encodingParameters);
			}
		});
	}
}
