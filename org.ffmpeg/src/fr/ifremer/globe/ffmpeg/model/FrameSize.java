/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ffmpeg.model;

/**
 * Frame size.
 */
public enum FrameSize {

	NATIVE("-1:-1", "Native images resolution"),
	DVD("720:480", "480p (720 x 480 pixels)"), //
	DVD_KAR("720:-1", "720 pixels width (keep aspect ratio)"), //
	HD_READY("1280:702", "720p (1280 x 720 pixels)"), //
	HD_READY_KAR("1280:-1", "1280 pixels width (keep aspect ratio)"), //
	FULL_HD("1280:1080", "1080p (1920 x 1080 pixels)"), //
	FULL_HD_KAR("1280:-1", "1910 pixels width (keep aspect ratio)"); //
	//not supported by H264
	//UHD_4K("3840:2160", "4K (3840 x 2160 pixels)"), //
	//UHD_8K("7680:4320", "8K (7680 x 4320 pixels)");

	private FrameSize(String resolution, String description) {
		this.resolution = resolution;
		this.description = description;
	}

	/** Resolution in pixels */
	protected String resolution;
	/** Description */
	protected String description;

	/**
	 * @return the {@link #description}
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the {@link #description} to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the {@link #resolution}
	 */
	public String getResolution() {
		return resolution;
	}

	/**
	 * @param resolution
	 *            the {@link #resolution} to set
	 */
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	/**
	 * Follow the link.
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return description;
	}

	public static FrameSize getDefault() {
		return NATIVE;
	}

}
