/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ffmpeg.model;

/**
 * Available codec.
 */
public enum Codec {

	/**DIVX("mpeg4", "DIVX (MPEG-4 Part 2)"), */H264("libx264", "H264 (MPEG-4 AVC)");

	private Codec(String encoder, String description) {
		this.encoder = encoder;
		this.description = description;
	}

	/** Encoder passed to ffmpeg with -c:v argument */
	protected String encoder;
	/** Description */
	protected String description;

	/**
	 * @return the {@link #encoder}
	 */
	public String getEncoder() {
		return encoder;
	}

	/**
	 * @param encoder
	 *            the {@link #encoder} to set
	 */
	public void setEncoder(String encoder) {
		this.encoder = encoder;
	}

	/**
	 * @return the {@link #description}
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the {@link #description} to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Follow the link.
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return description;
	}
}
