/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ffmpeg.model;

import java.io.File;

/**
 * Parameters used to encode some images in a video.
 */
public class EncodingParameters {

	/** Folder of images in input. */
	protected File inputDirectory;
	/** Video file in output. */
	protected File videoPath;
	/** Frame per second for the input images. */
	protected Framerate inputFramerate;
	/** Frame per second for the ouput video. */
	protected Framerate outputFramerate;
	/** Frame size (WxH) */
	protected FrameSize frameSize;
	/** Video codec (Xvid, H264...) */
	protected Codec codec;
	/** Container of the video (avi, mp4...) */
	protected Container container;
	
	protected H264Preset preset;

//	/** Maximum keyframe interval (frames) */
//	protected int maxKeyframe;
//	/** number of frames to look ahead for when encoding */
//	protected int lagInFrames;
//	/** Encoder speed/quality and cpu use */
//	protected Deadline deadline;
//	/** CPU Used (-16..16) */
//	protected int cpuUsed;
//	/** Bitstream profile number to use */
//	protected int vProfile;
//	/** maximum video quantizer scale */
//	protected int qMax;
//	/** minimum video quantizer scale */
//	protected int qMin;
//	/**
//	 * Directs the encoder to split the coefficient encoding across multiple
//	 * data partitions that can be encoded independently.
//	 */
//	protected int slices;
//	/** bitrate of the output file */
//	protected Bitrate bitrate;

	/** Container of the video (avi, mp4...) */

	/**
	 * Constructor.
	 */
	public EncodingParameters() {
	}

	/**
	 * @return the {@link #codec}
	 */
	public Codec getCodec() {
		return codec;
	}

	/**
	 * @param codec
	 *            the {@link #codec} to set
	 */
	public void setCodec(Codec codec) {
		this.codec = codec;
	}

	/**
	 * @return the {@link #container}
	 */
	public Container getContainer() {
		return container;
	}

	/**
	 * @param container
	 *            the {@link #container} to set
	 */
	public void setContainer(Container container) {
		this.container = container;
	}

	/**
	 * @return the {@link #frameSize}
	 */
	public FrameSize getFrameSize() {
		return frameSize;
	}

	/**
	 * @param frameSize
	 *            the {@link #frameSize} to set
	 */
	public void setFrameSize(FrameSize frameSize) {
		this.frameSize = frameSize;
	}

	/**
	 * @return the {@link #inputDirectory}
	 */
	public File getInputDirectory() {
		return inputDirectory;
	}

	/**
	 * @param imagesDirectory
	 *            the {@link #inputDirectory} to set
	 */
	public void setInputDirectory(File imagesDirectory) {
		this.inputDirectory = imagesDirectory;
	}

	/**
	 * @return the {@link #videoPath}
	 */
	public File getVideoPath() {
		return videoPath;
	}

	/**
	 * @param videoPath
	 *            the {@link #videoPath} to set
	 */
	public void setVideoPath(File videoPath) {
		this.videoPath = videoPath;
	}

	/**
	 * @return the {@link #inputFramerate}
	 */
	public Framerate getInputFramerate() {
		return inputFramerate;
	}

	/**
	 * @param inputFramerate
	 *            the {@link #inputFramerate} to set
	 */
	public void setInputFramerate(Framerate inputFramerate) {
		this.inputFramerate = inputFramerate;
	}

	/**
	 * @return the {@link #outputFramerate}
	 */
	public Framerate getOutputFramerate() {
		return outputFramerate;
	}

	/**
	 * @param outputFramerate
	 *            the {@link #outputFramerate} to set
	 */
	public void setOutputFramerate(Framerate outputFramerate) {
		this.outputFramerate = outputFramerate;
	}

	public H264Preset getPreset() {
		return preset;
	}

	public void setPreset(H264Preset preset) {
		this.preset = preset;
	}

	
}
