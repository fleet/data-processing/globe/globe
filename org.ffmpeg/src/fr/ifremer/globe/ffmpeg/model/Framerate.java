/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ffmpeg.model;

/**
 * Framerate, number of frame per second.
 */
public enum Framerate {

	T1_10("1/10", 10000l, "An image each 10 seconds"), //
	T1_5("1/5", 5000l, "An image each 5 seconds"), //
	T1_4("1/4", 4000l, "An image each 4 seconds"), //
	T1_3("1/3", 3000l, "An image each 3 seconds"), //
	T1_2("1/2", 2000l, "An image each 2 seconds"), //
	T1("1", 1000l, "An image every seconds"), //
	T2("2", 500l, "2 images per second"), //
	T5("5", 200l, "5 images per second"), //
	T10("10", 100l, "10 images per second"), //
	T15("15", 67l, "15 images per second"), //
	T20("20", 50l, "20 images per second"), //
	T24("24", 42l, "24 images per second"), //
	T25("25", 40l, "25 images per second"), //
	T30("30", 34l, "30 images per second"); //

	private Framerate(String value, long fps, String description) {
		this.value = value;
		this.fps = fps;
		this.description = description;
	}

	/** Value passed to ffmpeg with -framerate argument */
	protected String value;
	/** Description */
	protected String description;
	protected long fps;

	/**
	 * @return the {@link #description}
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the {@link #description} to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Follow the link.
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return description;
	}

	/**
	 * @return the {@link #value}
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the {@link #value} to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Getter of fps
	 */
	public long getFps() {
		return fps;
	}

	/**
	 * Setter of fps
	 */
	public void setFps(long fps) {
		this.fps = fps;
	}
}
