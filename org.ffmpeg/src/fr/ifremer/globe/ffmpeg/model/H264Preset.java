/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ffmpeg.model;

/**
 * Encoder speed/quality and cpu use .
 */
public enum H264Preset {
	ultrafast("ultrafast"),
	superfast("superfast"),
	veryfast("veryfast"),
	faster("faster"),
	fast("fast"),
	medium("medium"),
	slow("slow"),
	slower("slower"),
	veryslow("veryslow");
	
	private H264Preset(String value) {
		this.value = value;
	}

	/** value in bit/s */
	protected String value;

	/**
	 * @return the {@link #value}
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the {@link #value} to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	public static H264Preset getDefault() {
		return H264Preset.medium;
	}
}
