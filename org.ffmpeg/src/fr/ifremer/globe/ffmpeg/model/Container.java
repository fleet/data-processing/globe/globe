/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ffmpeg.model;

/**
 * Available container.
 */
public enum Container {

	Avi("avi", "AVI (Audio Video Interleave)"), Mpeg4("mp4", "MP4 (MPEG-4 Part 14)"), Mkv("mkv", "MKV (Matroska)");

	/**
	 * Constructor.
	 */
	private Container(String fileExtension, String description) {
		this.fileExtension = fileExtension;
		this.description = description;
	}

	/** File extension */
	protected String fileExtension;
	/** Description */
	protected String description;

	/**
	 * @return all possible file extension
	 */
	public static String[] getAllExtensions() {
		return new String[] { Avi.fileExtension, Mpeg4.fileExtension, Mkv.fileExtension };
	}

	/**
	 * @return the {@link #fileExtension}
	 */
	public String getFileExtension() {
		return fileExtension;
	}

	/**
	 * @param fileExtension
	 *            the {@link #fileExtension} to set
	 */
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	/**
	 * @return the {@link #description}
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the {@link #description} to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Follow the link.
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return description;
	}

}
