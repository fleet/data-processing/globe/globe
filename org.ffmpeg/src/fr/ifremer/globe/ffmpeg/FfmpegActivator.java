package fr.ifremer.globe.ffmpeg;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.osgi.service.environment.EnvironmentInfo;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Apside
 *
 */
public class FfmpegActivator implements BundleActivator {

	/** Logger. */
	protected static final Logger logger = LoggerFactory.getLogger(FfmpegActivator.class);

	/**
	 * Instance created when plugin was deployed.
	 */
	private static FfmpegActivator INSTANCE;

	/** Bundle's execution context within the Eclipse Framework. */
	private BundleContext bundleContext;

	/**
	 * Constructor.<br>
	 * Invoked twice : when plugin is deployed and when DS inject services.
	 */
	public FfmpegActivator() {
	}

	/**
	 * 
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext
	 *      )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		INSTANCE = this;
		bundleContext = context;
	}

	/**
	 * @return FFMPEG executable path
	 */
	public File getFfmpegExec() throws IOException {
		ServiceReference<?> envInfoSR = bundleContext.getServiceReference(EnvironmentInfo.class.getName());
		if (envInfoSR != null) {
			EnvironmentInfo envInfo = (EnvironmentInfo) bundleContext.getService(envInfoSR);
			String bundleName = "org.ffmpeg." + envInfo.getOS() + "." + envInfo.getOSArch();
			File result = getFFmpegExeAbsolutePath(bundleName);
			logger.debug("Ffmpeg.exe path : " + result.getAbsolutePath());
			logger.debug("Platform architecture supported by FFMPEG");
			return result;
		} else {
			logger.error("Platform architecture not supported by FFMPEG");
		}
		return null;
	}

	/**
	 * Search FFMPEG executable in all bundles
	 */
	protected File getFFmpegExeAbsolutePath(String pluginId) throws IOException {
		Bundle[] bundles = bundleContext.getBundles();
		for (Bundle oneBundle : bundles) {
			if (oneBundle.getSymbolicName().contains(pluginId)) {
				logger.info("FFMPEG is in plugin : " + pluginId);
				File bundleFolder = FileLocator.getBundleFile(oneBundle);
				return new File(bundleFolder, "ffmpeg");
			}
		}
		return null;
	}

	/**
	 * 
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext bundleContext) throws Exception {
	}

	/**
	 * Accesseur de iNSTANCE
	 */
	public static FfmpegActivator getInstance() {
		return INSTANCE;
	}

}