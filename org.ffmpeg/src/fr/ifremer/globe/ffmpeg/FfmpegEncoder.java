package fr.ifremer.globe.ffmpeg;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

import javax.imageio.ImageIO;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.ffmpeg.model.EncodingParameters;
import fr.ifremer.globe.ffmpeg.model.FrameSize;
import fr.ifremer.globe.utils.BufferedImageSerializer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * FFMPEG encoder.
 */
public class FfmpegEncoder {

	protected static final Logger logger = LoggerFactory.getLogger(FfmpegEncoder.class);

	/**
	 * Encode a video with ffmpeg
	 */
	public void encode(Shell shell, EncodingParameters encodingParameters) {

		IProcessService.grab().runInForeground("Video encoding", (monitor, l) -> {
			monitor.subTask("Converting images");
			try {
				File[] rawFiles = FfmpegUtils.listRawFiles(encodingParameters.getInputDirectory());
				convertRawFileToPng(rawFiles, SubMonitor.convert(monitor, rawFiles.length));
				// Make the video
				monitor.subTask("Encoding video");
				if (!doEncode(encodingParameters))
					throw new GIOException("Conversion has failed.");

			} catch (Exception e) {
				throw new GIOException("Error with video encoding : " + e.getMessage(), e);
			}
			monitor.done();
			return Status.OK_STATUS;

		});
	}

	/** Convert Raw files to Png */
	protected void convertRawFileToPng(File[] rawFiles, IProgressMonitor monitor) throws Exception {
		monitor.beginTask("Converts Raw files to Png", rawFiles.length);

		AtomicReference<Exception> exception = new AtomicReference<>();
		Arrays.stream(rawFiles).parallel().forEach((file) -> {
			try {
				// Don't process if any error occured
				if (exception.get() == null && file.length() > 0) {
					BufferedImage bufferedImage = BufferedImageSerializer.read(file);
					// Serialize the BufferedImage to Png
					ImageIO.write(bufferedImage, "png", new File(file.getParentFile(),
							String.format("%s.png", FilenameUtils.getBaseName(file.getName()))));
					monitor.worked(1);
				}
			} catch (Exception e) {
				logger.warn("Unable to parse the raw file " + file.getName(), e);
				exception.set(e);
			}
		});

		if (exception.get() != null) {
			throw exception.get();
		} else {
			Arrays.stream(rawFiles).forEach((file) -> file.delete());
		}
		monitor.done();
	}

	/**
	 * Perform the encoding into a job
	 */
	protected boolean doEncode(EncodingParameters encodingParameters) throws IOException {
		boolean result = false;
		File ffmpeg = FfmpegActivator.getInstance().getFfmpegExec();
		if (ffmpeg != null) {
			String[] args = { //
					// Overwrite output files without asking
					"-y",
					// input frame rate
					"-framerate", encodingParameters.getInputFramerate().getValue(), "-i", "img-%d.png",
					// Video encoder
					"-c:v", encodingParameters.getCodec().getEncoder(),
					// output frame rate and scaling
					"-vf", "fps=" + encodingParameters.getOutputFramerate().getValue() +

							(encodingParameters.getFrameSize() == FrameSize.NATIVE ? ""
									: (",scale=" + encodingParameters.getFrameSize().getResolution())),

					// preset
					"-preset", encodingParameters.getPreset().getValue(),
					// Out file
					encodingParameters.getVideoPath().getAbsolutePath() };

			CommandLine cmdLine = new CommandLine(FilenameUtils.normalize(ffmpeg.getAbsolutePath()));
			cmdLine.addArguments(args);
			DefaultExecutor executor = new DefaultExecutor();
			executor.setWorkingDirectory(encodingParameters.getInputDirectory());
			ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
			executor.setStreamHandler(new PumpStreamHandler(baos));
			logger.info("Starting command line :" + cmdLine.toString());
			int exitValue = executor.execute(cmdLine);
			result = exitValue == 0;
			if (result) {
				logger.warn(baos.toString());
			} else {
				logger.debug(baos.toString());
			}
		}
		return result;
	}
}
