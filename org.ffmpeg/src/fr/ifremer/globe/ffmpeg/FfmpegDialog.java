package fr.ifremer.globe.ffmpeg;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.ffmpeg.model.Codec;
import fr.ifremer.globe.ffmpeg.model.Container;
import fr.ifremer.globe.ffmpeg.model.EncodingParameters;
import fr.ifremer.globe.ffmpeg.model.FrameSize;
import fr.ifremer.globe.ffmpeg.model.Framerate;
import fr.ifremer.globe.ffmpeg.model.H264Preset;
import fr.ifremer.globe.ui.utils.Messages;

public class FfmpegDialog extends Dialog {

	protected Text videoPathText;
	protected Text imagesDirectoryText;
	protected ComboViewer inputFramerateCombo;
	protected ComboViewer outputFramerateCombo;
	protected ComboViewer resolutionCombo;
	protected ComboViewer codecCombo;
	protected ComboViewer containerCombo;

	protected Composite ffmpegComposite;
	protected ComboViewer presetComboViewer;

	/** Managed parameters */
	protected EncodingParameters encodingParameters;

	/**
	 * Constructor.
	 */
	public FfmpegDialog(Shell parentShell, EncodingParameters encodingParameters) {
		super(parentShell);
		setShellStyle(SWT.CLOSE | SWT.RESIZE | SWT.TITLE);

		this.encodingParameters = encodingParameters;
	}

	/**
	 * Follow the link.
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		// if path or file name are invalid
		if (videoPathText.getText().isEmpty()) {
			Messages.openToolTip(videoPathText, "Video path mandatory");
			return;
		}
		if (imagesDirectoryText.getText().isEmpty()) {
			Messages.openToolTip(imagesDirectoryText, "Image directory mandatory");
			return;
		}

		File imagesDirectory = getImagesDirectory();
		if (!imagesDirectory.isDirectory()) {
			Messages.openToolTip(imagesDirectoryText, "This is not a directory");
			return;
		}
		if (FfmpegUtils.listPngFiles(imagesDirectory).length == 0
				&& FfmpegUtils.listRawFiles(imagesDirectory).length == 0) {
			Messages.openToolTip(imagesDirectoryText, "No captured image found");
			return;
		}

		getEncodingParameters().setCodec(getCodec());
		getEncodingParameters().setContainer(getContainer());
		getEncodingParameters().setFrameSize(getFrameSize());
		getEncodingParameters().setInputDirectory(imagesDirectory);
		getEncodingParameters().setInputFramerate(getInputFramerate());
		getEncodingParameters().setOutputFramerate(getOutputFramerate());
		getEncodingParameters().setVideoPath(getVideoPath());
		getEncodingParameters().setPreset(getPreset());

		super.okPressed();
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		parent = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(1, false);
		parent.setLayout(layout);

		// The text fields will grow with the size of the dialog
		GridData gd_ffmpegComposite = new GridData();
		gd_ffmpegComposite.grabExcessHorizontalSpace = true;
		gd_ffmpegComposite.horizontalAlignment = GridData.FILL;

		GridLayout gl_ffmpegComposite = new GridLayout(5, false);
		gl_ffmpegComposite.marginTop = 10;
		gl_ffmpegComposite.marginBottom = 10;
		gl_ffmpegComposite.marginLeft = 10;
		gl_ffmpegComposite.marginRight = 10;
		gl_ffmpegComposite.verticalSpacing = 10;
		gl_ffmpegComposite.horizontalSpacing = 10;

		ffmpegComposite = new Composite(parent, SWT.NONE);
		ffmpegComposite.setLayoutData(gd_ffmpegComposite);
		ffmpegComposite.setLayout(gl_ffmpegComposite);

		createImagesDirectoryComposite(ffmpegComposite);
		createOutVideoComposite(ffmpegComposite);

		// input framerate spinner
		Label inFpsLab = new Label(ffmpegComposite, SWT.NONE);
		inFpsLab.setText("Input framerate :");
		GridData data = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		data.horizontalSpan = 1;
		inputFramerateCombo = new ComboViewer(ffmpegComposite, SWT.READ_ONLY);
		inputFramerateCombo.setContentProvider(ArrayContentProvider.getInstance());
		inputFramerateCombo.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((Framerate) element).getDescription();
			}
		});
		inputFramerateCombo.setInput(Framerate.values());
		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.horizontalSpan = 3;
		inputFramerateCombo.getCombo().setLayoutData(gridData);
		new Label(ffmpegComposite, SWT.NONE);

		// output framerate spinner
		Label outFpsLab = new Label(ffmpegComposite, SWT.NONE);
		outFpsLab.setText("Output framerate :");
		outputFramerateCombo = new ComboViewer(ffmpegComposite, SWT.READ_ONLY);
		outputFramerateCombo.setContentProvider(ArrayContentProvider.getInstance());
		outputFramerateCombo.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((Framerate) element).getDescription();
			}
		});
		outputFramerateCombo.setInput(Framerate.values());
		GridData gridData_1 = new GridData(GridData.FILL_HORIZONTAL);
		gridData_1.horizontalSpan = 3;
		outputFramerateCombo.getCombo().setLayoutData(gridData_1);
		new Label(ffmpegComposite, SWT.NONE);

		Label lblResolution = new Label(ffmpegComposite, SWT.NONE);
		lblResolution.setText("Resolution :");

		// combo for ratio
		resolutionCombo = new ComboViewer(ffmpegComposite, SWT.READ_ONLY);
		Combo combo = resolutionCombo.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		resolutionCombo.setContentProvider(ArrayContentProvider.getInstance());
		resolutionCombo.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((FrameSize) element).getDescription();
			}
		});
		resolutionCombo.setInput(FrameSize.values());
		new Label(ffmpegComposite, SWT.NONE);

		Label lblContainer = new Label(ffmpegComposite, SWT.NONE);
		lblContainer.setText("Container :");
		// combo for container extension
		containerCombo = new ComboViewer(ffmpegComposite, SWT.READ_ONLY);
		containerCombo.setContentProvider(ArrayContentProvider.getInstance());
		containerCombo.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((Container) element).getDescription();
			}
		});
		containerCombo.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		containerCombo.addSelectionChangedListener((event) -> {
			videoPathText.setText(
					FilenameUtils.removeExtension(videoPathText.getText()) + '.' + getContainer().getFileExtension());
		});
		containerCombo.setInput(Container.values());
		new Label(ffmpegComposite, SWT.NONE);

		Label lblCodec = new Label(ffmpegComposite, SWT.NONE);
		lblCodec.setText("Codec :");

		// combo for codec
		codecCombo = new ComboViewer(ffmpegComposite, SWT.READ_ONLY);
		codecCombo.setContentProvider(ArrayContentProvider.getInstance());
		codecCombo.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((Codec) element).getDescription();
			}
		});
		codecCombo.setInput(Codec.values());
		codecCombo.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		new Label(ffmpegComposite, SWT.NONE);

		Label lblPreset = new Label(ffmpegComposite, SWT.NONE);
		lblPreset.setText("Preset :");

		presetComboViewer = new ComboViewer(ffmpegComposite, SWT.READ_ONLY);
		Combo presetCombo = presetComboViewer.getCombo();
		presetCombo.setToolTipText(
				"A preset is a collection of options that will provide a certain encoding speed to compression ratio. A slower preset will provide better compression (compression is quality per filesize). This means that, for example, if you target a certain file size or constant bit rate, you will achieve better quality with a slower preset. Similarly, for constant quality encoding, you will simply save bitrate by choosing a slower preset");
		presetCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
		presetComboViewer.setContentProvider(ArrayContentProvider.getInstance());
		presetComboViewer.setInput(H264Preset.values());
		presetComboViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return ((H264Preset) element).getValue();
			}
		});
		new Label(ffmpegComposite, SWT.NONE);

		Label lblEncodingParameters = new Label(ffmpegComposite, SWT.SEPARATOR | SWT.HORIZONTAL);
		lblEncodingParameters.setText("Encoding parameters");
		lblEncodingParameters.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 5, 1));
		(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1)).minimumWidth = 100;
		(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1)).minimumWidth = 100;
		(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1)).minimumWidth = 100;

		return parent;
	}

	protected void createOutVideoComposite(Composite parent) {

		Label lab = new Label(parent, SWT.NONE);
		lab.setText("Video file :");
		GridData data = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		data.horizontalSpan = 1;
		lab.setLayoutData(data);

		videoPathText = new Text(parent, SWT.BORDER);
		videoPathText.setTextLimit(150);
		GridData gd_videoPathText = new GridData(GridData.FILL_HORIZONTAL);
		gd_videoPathText.horizontalSpan = 3;
		videoPathText.setLayoutData(gd_videoPathText);
		videoPathText.setText("");

		Button browseButton = new Button(parent, SWT.PUSH);
		browseButton.setText("...");
		GridData data_2 = new GridData(GridData.HORIZONTAL_ALIGN_END);
		data_2.horizontalAlignment = SWT.LEFT;
		data_2.horizontalSpan = 1;
		browseButton.setLayoutData(data_2);
		browseButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				FileDialog dlg = new FileDialog(parent.getShell());
				dlg.setFilterExtensions(Container.getAllExtensions());
				dlg.setFilterPath(videoPathText.getText());
				dlg.setText("Select the video output file");
				String path = dlg.open();
				if (path != null) {
					videoPathText.setText(path);
				}
			}
		});
	}

	protected void createImagesDirectoryComposite(Composite parent) {
		// Text field for the name of the video
		Label vidNameLab = new Label(ffmpegComposite, SWT.NONE);
		vidNameLab.setText("Images directory :");
		GridData data = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);
		data.horizontalSpan = 1;
		vidNameLab.setLayoutData(data);

		imagesDirectoryText = new Text(ffmpegComposite, SWT.BORDER);
		GridData gd_imagesDirectoryText = new GridData(GridData.FILL_HORIZONTAL);
		gd_imagesDirectoryText.horizontalSpan = 3;
		imagesDirectoryText.setLayoutData(gd_imagesDirectoryText);

		Button browseButton = new Button(parent, SWT.PUSH);
		browseButton.setText("...");
		GridData data_1 = new GridData(GridData.HORIZONTAL_ALIGN_END);
		data_1.horizontalAlignment = SWT.LEFT;
		data_1.horizontalSpan = 1;
		browseButton.setLayoutData(data_1);
		browseButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				DirectoryDialog dlg = new DirectoryDialog(Display.getDefault().getActiveShell());
				dlg.setFilterPath(imagesDirectoryText.getText());
				dlg.setText("Directory");
				dlg.setMessage("Select the images directory");
				dlg.setFilterPath(getVideoPath().getParent());
				String path = dlg.open();
				if (path != null) {
					imagesDirectoryText.setText(path);
				}
			}
		});
	}

	/**
	 * Follow the link.
	 * 
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Encoding parameters");
	}

	/**
	 * Follow the link.
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#create()
	 */
	@Override
	public void create() {
		super.create();

		setImagesDirectory(getEncodingParameters().getInputDirectory());
		setVideoPath(getEncodingParameters().getVideoPath());
		setCodec(getEncodingParameters().getCodec());
		setContainer(getEncodingParameters().getContainer());
		setFrameSize(getEncodingParameters().getFrameSize());
		setImagesDirectory(getEncodingParameters().getInputDirectory());
		setInputFramerate(getEncodingParameters().getInputFramerate());
		setOutputFramerate(getEncodingParameters().getOutputFramerate());

		setPreset(getEncodingParameters().getPreset());
	}

	private void setPreset(H264Preset preset) {
		presetComboViewer.setSelection(new StructuredSelection(preset));

	}

	/** Set the images Directory */
	public void setImagesDirectory(File imagesDirectory) {
		imagesDirectoryText.setText(imagesDirectory.getAbsolutePath());
	}

	/** get the images Directory */
	public File getImagesDirectory() {
		return new File(imagesDirectoryText.getText());
	}

	/**
	 * @return the {@link #codec}
	 */
	public Codec getCodec() {
		return (Codec) ((StructuredSelection) codecCombo.getSelection()).getFirstElement();
	}

	/**
	 * @param codec the {@link #codec} to set
	 */
	public void setCodec(Codec codec) {
		codecCombo.setSelection(new StructuredSelection(codec));
	}

	/**
	 * @return the {@link #container}
	 */
	public Container getContainer() {
		return (Container) ((StructuredSelection) containerCombo.getSelection()).getFirstElement();
	}

	/**
	 * @param container the {@link #container} to set
	 */
	public void setContainer(Container container) {
		containerCombo.setSelection(new StructuredSelection(container));
	}

	/**
	 * @return the {@link #frameSize}
	 */
	public FrameSize getFrameSize() {
		return (FrameSize) ((StructuredSelection) resolutionCombo.getSelection()).getFirstElement();
	}

	/**
	 * @param frameSize the {@link #frameSize} to set
	 */
	public void setFrameSize(FrameSize frameSize) {
		resolutionCombo.setSelection(new StructuredSelection(frameSize));
	}

	/**
	 * @return the {@link #encodingParameters}
	 */
	public EncodingParameters getEncodingParameters() {
		return encodingParameters;
	}

	/**
	 * @param encodingParameters the {@link #encodingParameters} to set
	 */
	public void setEncodingParameters(EncodingParameters encodingParameters) {
		this.encodingParameters = encodingParameters;
	}

	/**
	 * @return the {@link #videoPathText}
	 */
	public File getVideoPath() {
		return new File(videoPathText.getText());
	}

	/**
	 * @param videoPathText the {@link #videoPathText} to set
	 */
	public void setVideoPath(File videoPathText) {
		this.videoPathText.setText(videoPathText.getAbsolutePath());
	}

	/**
	 * @return the {@link #inputFramerateCombo}
	 */
	public Framerate getInputFramerate() {
		return (Framerate) ((StructuredSelection) inputFramerateCombo.getSelection()).getFirstElement();
	}

	public H264Preset getPreset() {
		return (H264Preset) ((StructuredSelection) presetComboViewer.getSelection()).getFirstElement();
	}

	/**
	 * @param inputFramerateCombo the {@link #inputFramerateCombo} to set
	 */
	public void setInputFramerate(Framerate framerate) {
		inputFramerateCombo.setSelection(new StructuredSelection(framerate));
	}

	/**
	 * @return the {@link #outputFramerateCombo}
	 */
	public Framerate getOutputFramerate() {
		return (Framerate) ((StructuredSelection) outputFramerateCombo.getSelection()).getFirstElement();
	}

	/**
	 * @param outputFramerateCombo the {@link #outputFramerateCombo} to set
	 */
	public void setOutputFramerate(Framerate framerate) {
		outputFramerateCombo.setSelection(new StructuredSelection(framerate));
	}

}
