/**
 *
 */
package fr.ifremer.globe.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.exception.runtime.AssumptionFailedException;

//import fr.ifremer.globe.ui.UIUtils;

//TODO move to globe.utils plugin
/**
 * Utility methods for files.
 *
 * @author G.Bourel, &lt;guillaume.bourel@altran.com&gt;
 */
public class FileUtils {

	protected static Logger logger = LoggerFactory.getLogger(FileUtils.class);

	static {
		// if (OSUtils.isUnix()) {
		// try {
		// System.loadLibrary("fileutils");
		// } catch (UnsatisfiedLinkError e) {
		// logger.error(
		// "Cannot load fileutils native library : no symlinks.",
		// e);
		// }
		// }
	}

	/** Invalid characters for file names. */
	private static final Pattern INVALID_CHARACTERS_PATTERN = Pattern
			.compile(".*(/|`|\\?|\\*|\\\\|<|>|\\||\\\"|:|\\t).*");

	/**
	 * 2<sup>30</sup> : allows conversion bytes => GB.
	 */
	public static final long BYTES_GIGABYTES = 1073741824;

	/**
	 * 2<sup>20</sup> : allows conversion bytes => MB.
	 */
	public static final long BYTES_MEGABYTES = 1048576;

	/**
	 * 2<sup>10</sup> : allows conversion bytes => KB.
	 */
	public static final long BYTES_KILOBYTES = 1024;

	/** Returns user home directory. */
	public static String getHomeDir() {
		String home = System.getProperty("user.home");
		return home;
	}

	/**
	 * Checks file name validity, returns invalid character. Returns <code>null</code> if valid.
	 */
	public static String checkFilename(String filename) {
		if (filename == null) {
			return "null";
		}
		Matcher m = INVALID_CHARACTERS_PATTERN.matcher(filename);
		if (m.matches()) {
			String s = m.group(1);
			return s;
		}
		return null;
	}

	// FIXME replace with java7 : Files.isSymbolicLink
	// (same as apache libs for now)
	public static boolean isSymLink(File file) throws GIOException {
		try {
			if (file == null) {
				throw new NullPointerException("File must not be null");
			}
			File canon;
			if (file.getParent() == null) {
				canon = file;
			} else {
				File canonDir = file.getParentFile().getCanonicalFile();
				canon = new File(canonDir, file.getName());
			}
			return !canon.getCanonicalFile().equals(canon.getAbsoluteFile());
		} catch (IOException e) {
			String message = "Error during symbolic link check " + file.getAbsolutePath() + e.getMessage();
			logger.error(message, e);
			throw new GIOException(message, e);
		}
	}

	public static File getParent(final File file) throws GIOException {
		try {
			if (file == null) {
				return null;
			}
			File parent = file.getParentFile();
			if (parent == null) {
				return null;
			}
			File canonDir = parent.getCanonicalFile();
			File canon = new File(canonDir, file.getName());
			return canon.getCanonicalFile().getParentFile();
		} catch (IOException e) {
			String message = "Error for get parent file\n From: " + file.getAbsolutePath() + e.getMessage();
			logger.error(message, e);
			throw new GIOException(message, e);
		}
	}

	/**
	 * Returns sub-directories of a file.
	 *
	 * @return sub-directories
	 */
	public static List<File> getSubDirectories(File file, boolean recursively) {
		List<File> list = new LinkedList<>();
		if (!recursively) {
			if (!file.exists() || !file.isDirectory()) {
				return list;
			}
			if (file.exists()) {
				File[] filelist = file.listFiles(new FileFilter() {
					@Override
					public boolean accept(File paramFile) {
						return paramFile.isDirectory();
					}
				});
				list.addAll(Arrays.asList(filelist));
			}
			return list;
		} else {

			FileVisitor<Path> simpleFileVisitor = new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					list.add(dir.toFile());
					return FileVisitResult.CONTINUE;
				}

			};
			FileSystem fileSystem = FileSystems.getDefault();
			Path rootPath = fileSystem.getPath(file.getAbsolutePath());
			try {
				Files.walkFileTree(rootPath, simpleFileVisitor);
			} catch (IOException ioe) {
				logger.error("Exception " + ioe.getMessage() + "while parsing directories, continuing", ioe);
			}

			return list;
		}
	}

	/**
	 * Returns file extension for specified file (string following the last period).
	 */
	public static String getFileExtension(final File file) {
		if (file == null) {
			return null;
		}

		String filename = file.getName();
		int dotPos = filename.lastIndexOf(".");
		if (dotPos < 0) {
			return null;
		}

		// +1 use to skip period
		return filename.substring(dotPos + 1);
	}

	/**
	 * Returns file extension for specified file. For nc file, extract the full extension
	 *
	 * @retun the extension (nc, dtm.nc, mbg, xsf...) or "" when none
	 */
	public static String getExtension(File filename) {
		return getExtension(filename.getName());
	}

	/**
	 * Returns file extension for specified file. For nc file, extract the full extension
	 *
	 * @retun the extension (nc, dtm.nc, mbg, xsf...) or "" when none
	 */
	public static String getExtension(String filename) {
		String result = FilenameUtils.getExtension(filename);
		if ("nc".equalsIgnoreCase(result)) {
			filename = filename.substring(0, filename.length() - result.length() - 1);
			String extraExtension = FilenameUtils.getExtension(filename);
			if (!extraExtension.isEmpty()) {
				result = extraExtension + '.' + result;
			}
		}
		return result;
	}

	/**
	 * @retun the name of the file minus the path and the extension
	 */
	public static String getBaseName(File filename) {
		return getBaseName(filename.getName());
	}

	/**
	 * @retun the name of the file minus the path and the extension
	 */
	public static String getBaseName(String filename) {
		String result = FilenameUtils.getName(filename);
		String extension = getExtension(result);
		return extension.isEmpty() ? result : result.substring(0, result.length() - extension.length() - 1);
	}

	/**
	 * Returns a file short name from its filename
	 */
	public static String getFileShortName(String filename) {
		String shortName = filename;
		check(filename);

		int dotPos = shortName.lastIndexOf(".");
		if (dotPos >= 0) // dot not found
		{
			// +1 use to skip period
			shortName = shortName.substring(0, dotPos);
		}
		dotPos = filename.lastIndexOf("/");
		if (dotPos >= 0) // dot not found
		{
			// +1 use to skip period
			shortName = shortName.substring(dotPos + 1);
		}
		dotPos = shortName.lastIndexOf("\\");
		if (dotPos >= 0) // dot not found
		{
			// +1 use to skip period
			shortName = shortName.substring(dotPos + 1);
		}
		return shortName;
	}

	private static void check(String file) {
		if (file == null || file.length() == 0) {
			throw new AssumptionFailedException("Bad filename " + file);
		}

	}

	/**
	 * createNewFile will copy the given file to a new file which name is computed with a suffix
	 *
	 * @param name of file to copy
	 * @param new filename prefix
	 * @return new file name
	 */
	public static String createNewFile(String filename, String prefix) throws GIOException {
		File source = new File(filename);
		String destName = source.getParent() + File.separator + prefix + source.getName();
		File destination = new File(destName);
		destination = FileUtils.copy(source, destination);
		return destination.getAbsolutePath();
	}

	/**
	 * Add suffix to {@link File}.
	 */
	public static File addSuffix(File file, String suffix) {
		var newFilename = getBaseName(file) + suffix + '.' + getExtension(file);
		return new File(file.getParentFile(), newFilename);
	}

	/**
	 * compute a new file name with suffix and index
	 */
	public static String computeSuffixAndIndexedFileName(File inputFileName, String prefix, String suffix) {
		check(inputFileName.getPath());
		String filename = inputFileName.getPath();
		int WindowsIndex = filename.lastIndexOf("\\");
		int unixIndex = filename.lastIndexOf("/");
		String ext = getFileExtension(inputFileName);
		if (prefix == null) {
			prefix = "";
		}
		if (suffix == null) {
			suffix = "";
		}
		return filename.substring(0, Math.max(unixIndex, WindowsIndex) + 1) + prefix
				+ filename.substring(Math.max(unixIndex, WindowsIndex) + 1) + suffix + "." + ext;

	}

	/**
	 * Returns a file short name from its filename
	 */
	public static String getFileShortNameWithExtension(String filename) {
		String shortName = filename;
		check(filename);

		int dotPos = filename.lastIndexOf("/");
		if (dotPos >= 0) // dot not found
		{
			// +1 use to skip period
			shortName = shortName.substring(dotPos + 1);
		}
		dotPos = shortName.lastIndexOf("\\");
		if (dotPos >= 0) // dot not found
		{
			// +1 use to skip period
			shortName = shortName.substring(dotPos + 1);
		}
		return shortName;
	}

	/** Creates a new symbolic link. */
	public native static void createSymLink(String source, String dest);

	/**
	 * File copy from source to destination. If destination file already exists, destination file is suffixed with its
	 * index between parenthesis.
	 *
	 * @param source source file
	 * @param destination destination file
	 * @return <code>true</code> if copy succeed, <code>false</code> otherwise
	 * @throws IOException if file copy fails
	 */
	public static File copy(File source, File destination) throws GIOException {

		FileInputStream fis = null;
		FileOutputStream fos = null;
		FileChannel in = null; // in channel
		FileChannel out = null; // out channel
		try {
			// if destination file exists, and an indexed suffix to its name
			int nb = 1;
			String destName = destination.getName();
			while (destination.exists()) {
				String suffixedName = null;
				File parent = destination.getParentFile();
				int idx = destName.lastIndexOf('.');
				if (idx <= 0) {
					suffixedName = destName + nb;
				} else {
					suffixedName = destName.substring(0, idx) + "(" + nb + ")" + destName.substring(idx);
				}
				destination = new File(parent, suffixedName);
				nb++;
			}

			fis = new FileInputStream(source);
			fos = new FileOutputStream(destination);
			in = fis.getChannel();
			out = fos.getChannel();

			// WARNING : windows copy error
			// http://forums.sun.com/thread.jspa?threadID=439695&messageID=2917510

			// magic number for windows, 64Mb - 32Kb)
			int maxCount = 64 * 1024 * 1024 - 32 * 1024;
			long size = in.size();
			long position = 0;
			while (position < size) {
				position += in.transferTo(position, maxCount, out);
			}
			return destination;

		} catch (IOException e) {
			String message = "Error during file copy\n From: " + source.getAbsolutePath() + "\nTo: "
					+ destination.getAbsolutePath() + " " + e.getMessage();
			logger.error(message, e);
			destination = null;
			throw new GIOException(message, e);

		} finally { // close channels
			try {
				if (fis != null) {
					fis.close();
				}
				if (in != null) {
					in.close();
				}
				if (fos != null) {
					fos.close();
				}
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				String message = "Error during file copy\n From: " + source.getAbsolutePath() + "\nTo: "
						+ destination.getAbsolutePath() + e.getMessage();
				logger.error(message, e);
				destination = null;
				throw new GIOException(message, e);
			}
		}
	}

	/**
	 * File copy from source to destination. If destination file already exists, destination file is suffixed with its
	 * index between parenthesis.
	 *
	 * @param source source file name
	 * @param destination destination file name
	 * @return created file if copy succeed, null otherwise
	 * @throws IOException if file copy fails
	 */
	public static File copy(String source, String destination) throws GIOException {
		return copy(new File(source), new File(destination));

	}

	/**
	 * Directory copy from source to destination.
	 *
	 * @param source source directory
	 * @param destination destination directory
	 * @return <code>true</code> if copy succeed, <code>false</code> otherwise
	 * @throws IOException if any file copy fails
	 */
	public static void copyDirectory(File source, File target) throws GIOException {

		if (source.isDirectory()) {
			String[] children = source.list();
			// warning : if mkdirs is done before source.list, this list may
			// contain the newly created directory and lead to a recursive loop
			if (!target.exists()) {
				target.mkdirs();
			}
			for (String element : children) {
				copyDirectory(new File(source, element), new File(target, element));
			}
		} else {
			copy(source, target);
		}
	}

	/**
	 * Deletes directory and all its content.
	 *
	 * @param dir the directory to delete
	 * @return <code>true</code> if deletion succeed, <code>false</code> otherwise
	 */
	public static boolean deleteDir(File dir) {

		boolean result = true;

		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (String element : children) {
				boolean success = deleteDir(new File(dir, element));
				if (!success) {
					result = false;
				}
			}
		}
		// deletes file or empty directory
		if (dir.exists()) {
			result = dir.delete();
		}

		return result;
	}

	/**
	 * Deletes file.
	 *
	 * @param file source file
	 * @return <code>true</code> if ok, <code>false</code> otherwise
	 */
	public static boolean delete(File file) {
		if (file == null || !file.exists()) {
			// OK : nothing to delete.
			return true;
		}

		if (logger != null) {
			logger.debug("Delete resource " + file.getPath());
		}

		boolean deleted = false;
		if (file.isDirectory()) {
			deleted = FileUtils.deleteDir(file);
		} else {
			deleted = file.delete();
		}

		// Force delete file or dir if not
		if (!deleted) {
			try {
				org.apache.commons.io.FileUtils.forceDelete(file);
			} catch (IOException e) {
				logger.error("Can't delete file", e);
			}
			deleted = !file.exists();
		}

		if (deleted) {
			logger.info(file.getAbsolutePath() + " deleted.");
			return true;
		} else {
			logger.error("Deletion failed for " + file.getAbsolutePath());
			return false;
		}
	}

	/**
	 * Moves source file to destination.
	 *
	 * @param source source file to move
	 * @param destination destination file
	 * @return <code>true</code> if ok, <code>false</code> otherwise
	 */
	public static boolean move(File source, File destination) throws GIOException {
		if (!destination.exists()) {
			// first try with renameTo
			boolean result = source.renameTo(destination);

			if (!result) {
				// if it fails try copy + delete
				result = true;

				result &= copy(source, destination) != null;

				if (result) {
					result &= source.delete();
				}
			}

			return result;
		} else {
			// if destination already exists
			return false;
		}
	}

	/**
	 * Count file lines.
	 *
	 * @param source
	 * @return file's line number
	 * @throws IOException if an error occurs while reading source file
	 */
	public static int countLines(File source) throws IOException {
		FileInputStream fis;
		int nbData = 0;

		fis = new FileInputStream(source);

		LineNumberReader l = new LineNumberReader(new BufferedReader(new InputStreamReader(fis)));
		while (l.readLine() != null) {
			nbData = l.getLineNumber();
		}
		fis.close();

		return nbData;
	}

	/**
	 * Return file content as a String.
	 *
	 * @param filePath the file to read
	 * @return the content as a String
	 * @throws java.io.IOException if an error occurs while reading file
	 */
	public static String readFileAsString(String filePath) throws java.io.IOException {
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			fileData.append(buf, 0, numRead);
		}
		reader.close();
		return fileData.toString();
	}

	/**
	 * Return true if provided file path is an absolute path.
	 *
	 * @param filePath the file path to check
	 * @return true if provided file path is an absolute path, false otherwise.
	 */
	public static boolean isAbsolute(String filePath) {
		if (filePath == null) {
			return false;
		}

		if (filePath.startsWith("/") || filePath.startsWith("\\")) {
			return true;
		}
		// ms windows path
		if (filePath.length() > 1 && filePath.charAt(1) == ':') {
			return true;
		}

		return false;
	}

	/**
	 * Return CRC sum for a file.
	 * <p>
	 * NB : despite this function name CRC isn't a real checksum
	 * </p>
	 *
	 * @param file
	 * @return CRC value
	 * @throws IOException
	 */
	public static long computeCheckSum(File file) throws IOException {
		return org.apache.commons.io.FileUtils.checksumCRC32(file);

	}

	/**
	 * Delete temporary files.
	 *
	 * @param prefix
	 * @param suffix
	 * @throws IOException
	 */
	public static void deleteTempFile(String prefix, String suffix) throws IOException {
		File dir = new File(System.getProperty("java.io.tmpdir"));
		String[] children = dir.list();
		for (String child : children) {
			if (child.startsWith(prefix) && child.endsWith(suffix)) {
				File f = new File(dir.getAbsolutePath() + "/" + child);
				if (!f.delete()) {
					throw new IOException("Erreur suppression fichier temporaire");
				}
			}
		}
	}

	/**
	 * Delete temporary folders starting with prefix.
	 */
	public static void deleteTempDir(String prefix) {
		try {
			Path temporaryFolder = Paths.get(System.getProperty("java.io.tmpdir"));
			Files.walkFileTree(temporaryFolder, new SimpleFileVisitor<Path>() {

				/**
				 * Follow the link.
				 *
				 * @see java.nio.file.SimpleFileVisitor#preVisitDirectory(java.lang.Object,
				 *      java.nio.file.attribute.BasicFileAttributes)
				 */
				@Override
				public FileVisitResult preVisitDirectory(Path path, BasicFileAttributes attrs) {
					FileVisitResult result = FileVisitResult.CONTINUE;
					if (!path.equals(temporaryFolder)) {
						File folder = path.toFile();
						if (folder.getName().startsWith(prefix)) {
							org.apache.commons.io.FileUtils.deleteQuietly(folder);
						}
						result = FileVisitResult.SKIP_SUBTREE;
					}
					return result;
				}
			});
		} catch (IOException e) {
			logger.info("Unable to clean temporary folder of Swath editor : " + e.getMessage());
		}
	}

	/**
	 * Ensure that the file given in argument as the specified extension.
	 *
	 * @param file the input file (this file isn't modified, use returned value)
	 * @param extension the extension to check
	 * @return the file with the specified extension
	 */
	public static File ensureExtension(File file, String extension) {
		if (file == null) {
			return null;
		}

		String path = file.getPath();
		File newFile = file;
		int idx = path.lastIndexOf('.');
		if (idx <= 0) {
			newFile = new File(path + "." + extension);
		} else {
			String ext = path.substring(idx + 1);
			if (!extension.equals(ext)) {
				// replace existing extension
				newFile = new File(path.substring(0, idx) + "." + extension);
			}
		}
		return newFile;
	}

	/**
	 * Compare two lists of files and returns the list of added files.
	 *
	 * @param oldList
	 * @param newList
	 * @return the list of new files
	 */
	public static List<File> compareFileLists(List<File> oldList, List<File> newList) {
		List<File> fileList = new ArrayList<File>();

		for (File newFile : newList) {
			if (!oldList.contains(newFile)) {
				fileList.add(newFile);
			}
		}

		return fileList;
	}

	/**
	 * Return a path relative to workspace of the given file
	 *
	 * @param fullFileName
	 * @return
	 */
	public static IPath getRelativePath(IPath path) {
		if (!path.isAbsolute()) {
			return path;
		}
		IPath location = Platform.getLocation();

		return path.makeRelativeTo(location);
	}

	public static String changeExtension(String fileName, String newExt) {
		File file = new File(fileName);
		File parentFile = file.getParentFile();
		String shortName = getFileShortName(fileName);
		File ensureExtension = new File(parentFile, shortName + newExt);
		return ensureExtension.getAbsolutePath();
	}

	/**
	 * Affecte une taille au fichier donné sans pour autant initialiser son contenu.
	 */
	public static void setLength(File file, long expectedSize) throws IOException {
		RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rwd");
		randomAccessFile.setLength(expectedSize);
		IOUtils.closeQuietly(randomAccessFile);

	}

	public static String supWhiteChars(String fileName) {
		Pattern pattern = Pattern.compile("\\s");
		Matcher match = pattern.matcher(fileName);
		return match.replaceAll("");
	}

	/** Read first lines of a file */
	public static List<String> readLines(String filePath, int lineCount) {
		List<String> lines = Collections.emptyList();
		try {
			// Read in UTF-8
			try (var lineStream = Files.lines(Path.of(filePath), StandardCharsets.UTF_8)) {
				lines = lineStream.limit(lineCount).collect(Collectors.toList());
			} catch (java.io.UncheckedIOException e) {
				try (var lineStream = Files.lines(Path.of(filePath), StandardCharsets.ISO_8859_1)) {
					lines = lineStream.limit(lineCount).collect(Collectors.toList());
				}
			}
		} catch (Exception e) {
			// Not a text file
		}
		return lines;
	}
}
