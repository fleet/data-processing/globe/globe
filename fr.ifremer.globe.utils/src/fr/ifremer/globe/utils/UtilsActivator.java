package fr.ifremer.globe.utils;

import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import fr.ifremer.globe.utils.units.GlobeUnitDB;

public class UtilsActivator implements BundleActivator {

	private static BundleContext context;

	public static BundleContext getContext() {
		return context;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext
	 * )
	 */
	@Override
	public void start(BundleContext bundleContext) throws Exception {
		//initialize unit database, so that our own standard unit are added to the database
		GlobeUnitDB.instance();
		context=bundleContext;
		
		try {
			Properties logProperties = new Properties();
			logProperties.load(getClass().getClassLoader().getResourceAsStream("/log4j.properties"));
			PropertyConfigurator.configure(logProperties);
		} catch (Exception e) {
			System.err.println("Could not initialize logger");
			e.printStackTrace();
		}
		
	}

	@Override
	public void stop(BundleContext context) throws Exception {

	}

	
}
