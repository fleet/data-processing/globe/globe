/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

/**
 * Represents a function that accepts two integers as arguments and produces a double
 */
@FunctionalInterface
public interface BiIntToDoubleFunction {
	double apply(int i1, int i2);
}
