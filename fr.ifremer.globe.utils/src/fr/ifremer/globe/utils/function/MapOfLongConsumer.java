/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

import java.util.Map;
import java.util.function.Consumer;

/**
 * Consumer of a map with a String as key and Long as value
 */
@FunctionalInterface
public interface MapOfLongConsumer extends Consumer<Map<String, Long>> {
}
