/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

/**
 * Represents a consumer of one object and a boolean
 */
@FunctionalInterface
public interface ObjBooleanConsumer<T> {
	void accept(T o, boolean b);
}
