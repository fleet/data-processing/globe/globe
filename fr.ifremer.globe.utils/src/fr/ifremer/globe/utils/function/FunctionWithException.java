package fr.ifremer.globe.utils.function;

/**
 * Represents a function that accepts one argument and produces a result. Can throw {@link Exception}.
 *
 */
@FunctionalInterface
public interface FunctionWithException<T, R, E extends Exception> {

	/**
	 * Applies this function to the given argument.
	 *
	 * @param t the function argument
	 * @return the function result
	 */
	R apply(T t) throws E;
}
