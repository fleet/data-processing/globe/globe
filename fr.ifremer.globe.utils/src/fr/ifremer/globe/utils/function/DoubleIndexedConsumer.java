/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

/**
 * Double consumer for a specific index
 */
@FunctionalInterface
public interface DoubleIndexedConsumer {
	void accept(int index, double value);
}
