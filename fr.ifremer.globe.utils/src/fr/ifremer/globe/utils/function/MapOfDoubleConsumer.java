/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

import java.util.Map;
import java.util.function.Consumer;

/**
 * Consumer of a map with a String as key and Double as value
 */
@FunctionalInterface
public interface MapOfDoubleConsumer extends Consumer<Map<String, Double>> {
}
