/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

/**
 * Two integers consumer
 */
@FunctionalInterface
public interface BiIntConsumer {
	void accept(int i1, int i2);
}
