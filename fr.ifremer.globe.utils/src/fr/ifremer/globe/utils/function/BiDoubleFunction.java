/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

/**
 * Represents a function that accepts two doubles as arguments and produces a result
 */
@FunctionalInterface
public interface BiDoubleFunction<R> {
	R apply(double d1, double d2);
}
