/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

/**
 * Double supplier for a specific index
 */
@FunctionalInterface
public interface DoubleIndexedSupplier {
	double getAsDouble(int index);
}
