/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

/**
 * Int supplier for a specific index
 */
@FunctionalInterface
public interface IntIndexedSupplier {
	int getAsInteger(int index);
}
