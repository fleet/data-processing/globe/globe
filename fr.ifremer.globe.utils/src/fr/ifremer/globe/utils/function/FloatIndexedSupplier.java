/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

/**
 * Float supplier for a specific index
 */
@FunctionalInterface
public interface FloatIndexedSupplier {
	float getAsFloat(int index);
}
