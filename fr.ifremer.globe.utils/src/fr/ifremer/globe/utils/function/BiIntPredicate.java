/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

/**
 * Two integers predicate
 */
@FunctionalInterface
public interface BiIntPredicate {
	boolean test(int i1, int i2);
}
