package fr.ifremer.globe.utils.function;

@FunctionalInterface
public interface IntFunctionWithException<R, E extends Exception> {

	R apply(int i) throws E;

}
