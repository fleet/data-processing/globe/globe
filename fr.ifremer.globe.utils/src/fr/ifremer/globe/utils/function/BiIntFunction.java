/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

/**
 * Represents a function that accepts two integers as arguments and produces a result
 */
@FunctionalInterface
public interface BiIntFunction<R> {
	R apply(int i1, int i2);
}
