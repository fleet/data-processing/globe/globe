/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.function;

/**
 * Represents a function that accepts two integers as arguments and produces a float
 */
@FunctionalInterface
public interface BiIntToFloatFunction {
	float apply(int i1, int i2);
}
