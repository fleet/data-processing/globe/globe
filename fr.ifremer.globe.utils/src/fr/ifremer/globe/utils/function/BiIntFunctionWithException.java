package fr.ifremer.globe.utils.function;

@FunctionalInterface
public interface BiIntFunctionWithException<R, E extends Exception> {

	R apply(int i1, int i2) throws E;

}
