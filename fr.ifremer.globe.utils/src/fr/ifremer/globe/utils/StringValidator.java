package fr.ifremer.globe.utils;

/**
 * Tool used to validate string. Default validation is design to validate file
 * names
 * 
 * @author mguillem
 *
 */
public class StringValidator {

	private char[] excluded = { '/', '\\', ':', '*', '?', '"', '<', '>', '|' };

	/**
	 * build default StringValidator which exclude /\:*?"<>|
	 */
	public StringValidator() {
	}

	/**
	 * build a StringValidator with custom exclusion list
	 */
	public StringValidator(char... exclude) {
		if (exclude != null)
			excluded = exclude;
	}

	/**
	 * return true if parameter string does not contain excluded char
	 * 
	 * @param string
	 * @return
	 */
	public boolean validate(String string) {
		if (string == null || string.length() == 0) {
			return true;
		}
		for (int i = 0; i < excluded.length; i++) {
			if (string.indexOf(excluded[i]) != -1)
				return false;
		}
		return true;
	}

	/**
	 * Return the parameter string without excluded char.
	 * 
	 * @param string
	 * @return
	 */
	public String filterString(String string) {
		StringBuilder result = new StringBuilder();
		char[] array = string.toCharArray();
		boolean add;
		for (int i = 0; i < array.length; i++) {
			add = true;
			for (int j = 0; j < excluded.length; j++) {
				if (array[i] == excluded[j]) {
					add = false;
					break;
				}
			}
			if (add)
				result.append(array[i]);
		}
		return result.toString();
	}

	@Override
	public String toString() {
		return new String(excluded);
	}
}
