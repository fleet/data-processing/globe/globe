/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;

/**
 * Array of primitives.
 * 
 * @author Apside
 *
 */
public interface IArray extends Closeable {

	/**
	 * Close this array clean all used resources. Mapped file is deleted if any.
	 */
	void dispose();

	/**
	 * Consider this array as closed (file is deleted only if it's a temporary
	 * one).
	 */
	@Override
	void close();

	/**
	 * Flush datas.
	 */
	void flush();

	/**
	 * Tells whether or not this array is read-only.
	 */
	boolean isWritable();

	/**
	 * Number of elements in the array.
	 */
	long getElementCount();

	/**
	 * Modifies this buffer's byte order.
	 *
	 * @param byteOrder
	 *            The new byte order, either {@link ByteOrder#BIG_ENDIAN
	 *            BIG_ENDIAN} or {@link ByteOrder#LITTLE_ENDIAN LITTLE_ENDIAN}
	 */
	void setByteOrder(ByteOrder byteOrder);

	/**
	 * Sets this buffer's limit. If the position is larger than the new limit
	 * then it is set to the new limit. If the mark is defined and larger than
	 * the new limit then it is discarded.
	 *
	 * @param newLimit
	 *            The new limit value; must be non-negative and no larger than
	 *            this buffer's capacity
	 *
	 * @throws IllegalArgumentException
	 *             If the preconditions on <tt>newLimit</tt> do not hold
	 */
	void reduceElementCount(long elementCount) throws IOException;

	/**
	 * Dump the content of this array into the specidied file.
	 */
	void dump(File destination) throws IOException;

}
