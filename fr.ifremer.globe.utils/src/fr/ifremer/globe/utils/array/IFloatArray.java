/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array;

import java.io.IOException;
import java.nio.FloatBuffer;

/**
 * Tableau de float mappés sur un fichier.
 * 
 * @author Apside
 *
 */
public interface IFloatArray extends IArray {

	/**
	 * Retrieves a float starting at the specified index.
	 */
	float getFloat(long index);

	/**
	 * Stores a float starting at the specified index.
	 */
	void putFloat(long index, float value);

	/**
	 * Retrieves a float starting at the specified index.
	 */
	float getFloat(int index);

	/**
	 * Stores a float starting at the specified index.
	 */
	void putFloat(int index, float value);

	/**
	 * Retrieves a float stored at the specified coords.
	 */
	float getFloat(int rowIndex, int colIndex);

	/**
	 * Stores a float at the specified coords.
	 */
	void putFloat(int rowIndex, int colIndex, float value);

	/**
	 * Retrieves a float stored at the specified coords.
	 */
	float getFloat(int x, int y, int z);

	/**
	 * Stores a float at the specified coords.
	 */
	void putFloat(int x, int y, int z, float value);

	/** Fill the array with the given value. */
	default void fill(float value) {
		for (long i = 0; i < getElementCount(); i++) {
			putFloat(i, value);
		}
	}

	/**
	 * Creates a view of this array as a float buffer.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will be visible in the new array, and vice versa
	 * 
	 * @param firstIndex
	 *            First element in the view
	 * @param elementCount
	 *            Number of float in the resulting buffer
	 * @return A new float buffer
	 */
	FloatBuffer asFloatBuffer(int firstIndex, int elementCount);

	/**
	 * Create an new instance of IFloatArray reflecting the content of this
	 * IFloatArray.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will NOT be visible in the new array, and vice versa
	 * 
	 * @return
	 */
	IFloatArray cloneFloatArray() throws IOException;

}
