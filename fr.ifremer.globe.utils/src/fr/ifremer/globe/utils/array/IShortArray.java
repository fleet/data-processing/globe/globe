/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array;

import java.io.IOException;
import java.nio.ShortBuffer;

/**
 * Tableau de shorts mappés sur un fichier.
 * 
 * @author Apside
 *
 */
public interface IShortArray extends IArray {

	/**
	 * Retrieves a short starting at the specified index.
	 */
	short getShort(long index);

	/**
	 * Stores a short starting at the specified index.
	 */
	void putShort(long index, short value);

	/**
	 * Retrieves a short starting at the specified index.
	 */
	short getShort(int index);

	/**
	 * Stores a short starting at the specified index.
	 */
	void putShort(int index, short value);

	/**
	 * Retrieves a short stored at the specified coords.
	 */
	short getShort(int rowIndex, int colIndex);

	/**
	 * Stores a short at the specified coords.
	 */
	void putShort(int rowIndex, int colIndex, short value);

	/**
	 * Retrieves a short stored at the specified coords.
	 */
	short getShort(int x, int y, int z);

	/**
	 * Stores a short at the specified coords.
	 */
	void putShort(int x, int y, int z, short value);

	/** Fill the array with the given value. */
	default void fill(short value) {
		for (long i = 0; i < getElementCount(); i++) {
			putShort(i, value);
		}
	}

	/**
	 * Creates a view of this array as a short buffer.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will be visible in the new array, and vice versa
	 * 
	 * @param firstIndex
	 *            First element in the view
	 * @param elementCount
	 *            Number of short in the resulting buffer
	 * @return A new float buffer
	 */
	ShortBuffer asShortBuffer(int firstIndex, int elementCount);

	/**
	 * Create an new instance of IShortArray reflecting the content of this
	 * IShortArray.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will NOT be visible in the new array, and vice versa
	 * 
	 * @return
	 */
	IShortArray cloneShortArray() throws IOException;

}
