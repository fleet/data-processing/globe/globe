/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import fr.ifremer.globe.utils.array.impl.LargeArray;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Factory of {@link IArray}.
 *
 * @author Apside
 *
 */
public interface IArrayFactory {

	/**
	 * Returns the service implementation of IArrayFactory.
	 */
	static IArrayFactory grab() {
		return OsgiUtils.getService(IArrayFactory.class);
	}

	/**
	 * Create a {@link IIntArray} able to contain some integers.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IIntArray}
	 * @throws IOException Trouble.
	 */
	IIntArray makeIntArray(long numberOfElements) throws IOException;

	IIntArray makeIntArray(long numberOfElements, String fileNamePrefix) throws IOException;

	/**
	 * Create a {@link IIntArray} able to contain some integers.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IIntArray}
	 * @throws IOException Trouble.
	 */
	IIntArray makeIntArray(int rowSize, int colSize) throws IOException;

	IIntArray makeIntArray(int rowSize, int colSize, String fileNamePrefix) throws IOException;

	/**
	 * Create a three dimentional {@link IIntArray} able to contain some integers.
	 *
	 * @param xCount Number of elements on x axis
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link IIntArray}
	 * @throws IOException Trouble.
	 */
	IIntArray makeIntArray(int xCount, int yCount, int zCount) throws IOException;

	/**
	 * Create a {@link ILongArray} able to contain some longs.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link ILongArray}
	 * @throws IOException Trouble.
	 */
	ILongArray makeLongArray(long numberOfElements) throws IOException;

	/**
	 * Create a {@link ILongArray} able to contain some longs.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link ILongArray}
	 * @throws IOException Trouble.
	 */
	ILongArray makeLongArray(int rowSize, int colSize) throws IOException;

	ILongArray makeLongArray(int rowSize, int colSize, String fileNamePrefix) throws IOException;

	/**
	 * Create a three dimentional {@link ILongArray} able to contain some longs.
	 *
	 * @param xCount Number of elements on x axis
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link ILongArray}
	 * @throws IOException Trouble.
	 */
	ILongArray makeLongArray(int xCount, int yCount, int zCount) throws IOException;

	/**
	 * Create a {@link IShortArray} able to contain some shorts.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IShortArray}
	 * @throws IOException Trouble.
	 */
	IShortArray makeShortArray(long numberOfElements) throws IOException;

	IShortArray makeShortArray(long numberOfElements, String fileNamePrefix) throws IOException;

	/**
	 * Create a {@link IShortArray} able to contain some shorts.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IShortArray}
	 * @throws IOException Trouble.
	 */
	IShortArray makeShortArray(int rowSize, int colSize) throws IOException;

	/**
	 * Create a three dimentional {@link IShortArray} able to contain some shorts.
	 *
	 * @param xCount Number of elements on x axis
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link IShortArray}
	 * @throws IOException Trouble.
	 */
	IShortArray makeShortArray(int xCount, int yCount, int zCount) throws IOException;

	/**
	 * Create a {@link IDoubleArray} able to contain some doubles.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IDoubleArray}
	 * @throws IOException Trouble.
	 */
	IDoubleArray makeDoubleArray(long numberOfElements) throws IOException;

	/**
	 * Create a two dimentional {@link IDoubleArray} able to contain some doubles.
	 *
	 * @param rowSize Number of rows
	 * @param colSize Number of columns
	 * @return the instance of {@link IDoubleArray}
	 * @throws IOException Trouble.
	 */
	IDoubleArray makeDoubleArray(int rowSize, int colSize) throws IOException;

	/**
	 * Create a three dimentional {@link IDoubleArray} able to contain some doubles.
	 *
	 * @param xCount Number of elements on x axis
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link IDoubleArray}
	 * @throws IOException Trouble.
	 */
	IDoubleArray makeDoubleArray(int xCount, int yCount, int zCount) throws IOException;

	/**
	 * Create a {@link IBooleanArray} able to contain some booleans.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IBooleanArray}
	 * @throws IOException Trouble.
	 */
	IBooleanArray makeBooleanArray(long numberOfElements) throws IOException;

	/**
	 * Create a {@link IBooleanArray} able to contain some booleans.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IBooleanArray}
	 * @throws IOException Trouble.
	 */
	IBooleanArray makeBooleanArray(int rowSize, int colSize) throws IOException;

	/**
	 * Create a three dimentional {@link IBooleanArray} able to contain some booleans.
	 *
	 * @param xCount Number of elements on x axis
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link IBooleanArray}
	 * @throws IOException Trouble.
	 */
	IBooleanArray makeBooleanArray(int xCount, int yCount, int zCount) throws IOException;

	/**
	 * Create a {@link IByteArray} able to contain some bytes.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IByteArray}
	 * @throws IOException Trouble.
	 */
	IByteArray makeByteArray(long numberOfElements) throws IOException;

	IByteArray makeByteArray(long numberOfElements, String fileNamePrefix) throws IOException;

	/**
	 * Create a {@link IByteArray} able to contain some bytes.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IByteArray}
	 * @throws IOException Trouble.
	 */
	IByteArray makeByteArray(int rowSize, int colSize) throws IOException;

	IByteArray makeByteArray(int rowSize, int colSize, String fileNamePrefix) throws IOException;

	/**
	 * Create a three dimentional {@link IByteArray} able to contain some bytes.
	 *
	 * @param xCount Number of elements on x axis
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link IByteArray}
	 * @throws IOException Trouble.
	 */
	IByteArray makeByteArray(int xCount, int yCount, int zCount) throws IOException;

	/**
	 * Create a {@link IFloatArray} able to contain some floats.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IFloatArray}
	 * @throws IOException Trouble.
	 */
	IFloatArray makeFloatArray(long numberOfElements) throws IOException;

	IFloatArray makeFloatArray(long numberOfElements, String fileNamePrefix) throws IOException;

	/**
	 * Create a {@link IFloatArray} able to contain some floats.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IFloatArray}
	 * @throws IOException Trouble.
	 */
	IFloatArray makeFloatArray(int rowSize, int colSize) throws IOException;

	/**
	 * Create a three dimentional {@link IFloatArray} able to contain some floats.
	 *
	 * @param xCount Number of elements on x axis
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link IFloatArray}
	 * @throws IOException Trouble.
	 */
	IFloatArray makeFloatArray(int xCount, int yCount, int zCount) throws IOException;

	/**
	 * Create a {@link ICharArray} able to contain some chars.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link ICharArray}
	 * @throws IOException Trouble.
	 */
	ICharArray makeCharArray(long numberOfElements) throws IOException;

	/**
	 * Create a {@link ICharArray} able to contain some chars.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link ICharArray}
	 * @throws IOException Trouble.
	 */
	ICharArray makeCharArray(int rowSize, int colSize) throws IOException;

	/**
	 * Create a three dimentional {@link ICharArray} able to contain some chars.
	 *
	 * @param xCount Number of elements on x axis
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link ICharArray}
	 * @throws IOException Trouble.
	 */
	ICharArray makeCharArray(int xCount, int yCount, int zCount) throws IOException;

	/**
	 * Creates a {@link IArray} mapped on the specified file.
	 */
	LargeArray openLargeArray(File file, int sizeOfOneElement) throws IOException;

	/**
	 * Create a {@link IBooleanArray} map on the specified file and able to contain some booleans. <br>
	 * Number Of elements is deducted from the file size.
	 *
	 * @return the instance of {@link IBooleanArray}
	 * @throws IOException Trouble.
	 */
	IBooleanArray openBooleanArray(File file, boolean readWrite) throws IOException;

	/**
	 * Create a {@link IBooleanArray} map on the specified file and able to contain some booleans.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IBooleanArray}
	 * @throws IOException Trouble.
	 */
	IBooleanArray openBooleanArray(File file, long numberOfElements, boolean readWrite) throws IOException;

	/**
	 * Create a bidimentionnal {@link IBooleanArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param rowCount number of rows
	 * @param colCount number of cols
	 * @return the build array
	 */
	IBooleanArray openBooleanArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException;

	/**
	 * Create a {@link IByteArray} map on the specified file and able to contain some booleans. <br>
	 * Number Of elements is deducted from the file size.
	 *
	 * @return the instance of {@link IByteArray}
	 * @throws IOException Trouble.
	 */
	IByteArray openByteArray(File file, boolean readWrite) throws IOException;

	/**
	 * Create a {@link IByteArray} map on the specified file and able to contain some booleans.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IByteArray}
	 * @throws IOException Trouble.
	 */
	IByteArray openByteArray(File file, long numberOfElements, boolean readWrite) throws IOException;

	/**
	 * Create a bidimentionnal {@link IByteArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param rowCount number of rows
	 * @param colCount number of cols
	 * @return the build array
	 */
	IByteArray openByteArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException;

	/**
	 * Create a {@link ICharArray} map on the specified file and able to contain some booleans. <br>
	 * Number Of elements is deducted from the file size.
	 *
	 * @return the instance of {@link ICharArray}
	 * @throws IOException Trouble.
	 */
	ICharArray openCharArray(File file, boolean readWrite) throws IOException;

	/**
	 * Create a {@link ICharArray} map on the specified file and able to contain some booleans.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link ICharArray}
	 * @throws IOException Trouble.
	 */
	ICharArray openCharArray(File file, long numberOfElements, boolean readWrite) throws IOException;

	/**
	 * Create a bidimentionnal {@link ICharArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param rowCount number of rows
	 * @param colCount number of cols
	 * @return the build array
	 */
	ICharArray openCharArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException;

	/**
	 * Create a {@link IDoubleArray} map on the specified file and able to contain some booleans. <br>
	 * Number Of elements is deducted from the file size.
	 *
	 * @return the instance of {@link IDoubleArray}
	 * @throws IOException Trouble.
	 */
	IDoubleArray openDoubleArray(File file, boolean readWrite) throws IOException;

	/**
	 * Create a {@link IDoubleArray} map on the specified file and able to contain some booleans.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IDoubleArray}
	 * @throws IOException Trouble.
	 */
	IDoubleArray openDoubleArray(File file, long numberOfElements, boolean readWrite) throws IOException;

	/**
	 * Create a bidimentionnal {@link IDoubleArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param rowCount number of rows
	 * @param colCount number of cols
	 * @return the build array
	 */
	IDoubleArray openDoubleArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException;

	/**
	 * Create a {@link IFloatArray} map on the specified file and able to contain some booleans. <br>
	 * Number Of elements is deducted from the file size.
	 *
	 * @return the instance of {@link IFloatArray}
	 * @throws IOException Trouble.
	 */
	IFloatArray openFloatArray(File file, boolean readWrite) throws IOException;

	/**
	 * Create a {@link IFloatArray} map on the specified file and able to contain some booleans.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IFloatArray}
	 * @throws IOException Trouble.
	 */
	IFloatArray openFloatArray(File file, long numberOfElements, boolean readWrite) throws IOException;

	/**
	 * Create a bidimentionnal {@link IFloatArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param rowCount number of rows
	 * @param colCount number of cols
	 * @return the build array
	 */
	IFloatArray openFloatArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException;

	/**
	 * Create a {@link IIntArray} map on the specified file and able to contain some booleans. <br>
	 * Number Of elements is deducted from the file size.
	 *
	 * @return the instance of {@link IIntArray}
	 * @throws IOException Trouble.
	 */
	IIntArray openIntArray(File file, boolean readWrite) throws IOException;

	/**
	 * Create a {@link IIntArray} map on the specified file and able to contain some booleans.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IIntArray}
	 * @throws IOException Trouble.
	 */
	IIntArray openIntArray(File file, long numberOfElements, boolean readWrite) throws IOException;

	/**
	 * Create a bidimentionnal {@link IIntArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param rowCount number of rows
	 * @param colCount number of cols
	 * @return the build array
	 */
	IIntArray openIntArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException;

	/**
	 * Create a {@link ILongArray} map on the specified file and able to contain some booleans. <br>
	 * Number Of elements is deducted from the file size.
	 *
	 * @return the instance of {@link ILongArray}
	 * @throws IOException Trouble.
	 */
	ILongArray openLongArray(File file, boolean readWrite) throws IOException;

	/**
	 * Create a {@link ILongArray} map on the specified file and able to contain some booleans.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link ILongArray}
	 * @throws IOException Trouble.
	 */
	ILongArray openLongArray(File file, long numberOfElements, boolean readWrite) throws IOException;

	/**
	 * Create a bidimentionnal {@link ILongArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param rowCount number of rows
	 * @param colCount number of cols
	 * @return the build array
	 */
	ILongArray openLongArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException;

	/**
	 * Create a {@link IShortArray} map on the specified file and able to contain some booleans. <br>
	 * Number Of elements is deducted from the file size.
	 *
	 * @return the instance of {@link IShortArray}
	 * @throws IOException Trouble.
	 */
	IShortArray openShortArray(File file, boolean readWrite) throws IOException;

	/**
	 * Create a {@link IShortArray} map on the specified file and able to contain some booleans.
	 *
	 * @param numberOfElements Array capacity
	 * @return the instance of {@link IShortArray}
	 * @throws IOException Trouble.
	 */
	IShortArray openShortArray(File file, long numberOfElements, boolean readWrite) throws IOException;

	/**
	 * Create a bidimentionnal {@link IShortArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param rowCount number of rows
	 * @param colCount number of cols
	 * @return the build array
	 */
	IShortArray openShortArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException;

	/**
	 * Create a three dimentionnal {@link IBooleanArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param xCount number of elements on x axis
	 * @param yCount number of elements on y axis
	 * @param zCount number of elements on z axis
	 * @return the build array
	 */
	IBooleanArray openBooleanArray(File file, int xCount, int yCount, int zCount, boolean readWrite) throws IOException;

	/**
	 * Create a three dimentionnal {@link IByteArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param xCount number of elements on x axis
	 * @param yCount number of elements on y axis
	 * @param zCount number of elements on z axis
	 * @return the build array
	 */
	IByteArray openByteArray(File file, int xCount, int yCount, int zCount, boolean readWrite) throws IOException;

	/**
	 * Create a three dimentionnal {@link ICharArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param xCount number of elements on x axis
	 * @param yCount number of elements on y axis
	 * @param zCount number of elements on z axis
	 * @return the build array
	 */
	ICharArray openCharArray(File file, int xCount, int yCount, int zCount, boolean readWrite) throws IOException;

	/**
	 * Create a three dimentionnal {@link IDoubleArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param xCount number of elements on x axis
	 * @param yCount number of elements on y axis
	 * @param zCount number of elements on z axis
	 * @return the build array
	 */
	IDoubleArray openDoubleArray(File file, int xCount, int yCount, int zCount, boolean readWrite) throws IOException;

	/**
	 * Create a three dimentionnal {@link IFloatArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param xCount number of elements on x axis
	 * @param yCount number of elements on y axis
	 * @param zCount number of elements on z axis
	 * @return the build array
	 */
	IFloatArray openFloatArray(File file, int xCount, int yCount, int zCount, boolean readWrite) throws IOException;

	/**
	 * Create a three dimentionnal {@link IIntArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param xCount number of elements on x axis
	 * @param yCount number of elements on y axis
	 * @param zCount number of elements on z axis
	 * @return the build array
	 */
	IIntArray openIntArray(File file, int xCount, int yCount, int zCount, boolean readWrite) throws IOException;

	/**
	 * Create a three dimentionnal {@link ILongArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param xCount number of elements on x axis
	 * @param yCount number of elements on y axis
	 * @param zCount number of elements on z axis
	 * @return the build array
	 */
	ILongArray openLongArray(File file, int xCount, int yCount, int zCount, boolean readWrite) throws IOException;

	/**
	 * Create a three dimentionnal {@link IShortArray} mapped on a file.
	 *
	 * @param file the mapped file
	 * @param xCount number of elements on x axis
	 * @param yCount number of elements on y axis
	 * @param zCount number of elements on z axis
	 * @return the build array
	 */
	IShortArray openShortArray(File file, int xCount, int yCount, int zCount, boolean readWrite) throws IOException;

	/**
	 * Wrap a {@link ByteBuffer} into a {link IBooleanArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link IBooleanArray}
	 */
	IBooleanArray wrapBooleanArray(ByteBuffer buffer);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IBooleanArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link IBooleanArray}
	 */
	IBooleanArray wrapBooleanArray(ByteBuffer buffer, int colSize);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IBooleanArray}.
	 *
	 * @param buffer buffer to wrap
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link IBooleanArray}
	 */
	IBooleanArray wrapBooleanArray(ByteBuffer buffer, int yCount, int zCount);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IByteArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link IByteArray}
	 */
	IByteArray wrapByteArray(ByteBuffer buffer);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IByteArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link IByteArray}
	 */
	IByteArray wrapByteArray(ByteBuffer buffer, int colSize);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IByteArray}.
	 *
	 * @param buffer buffer to wrap
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link IByteArray}
	 */
	IByteArray wrapByteArray(ByteBuffer buffer, int yCount, int zCount);

	/**
	 * Wrap a {@link ByteBuffer} into a {link ICharArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link ICharArray}
	 */
	ICharArray wrapCharArray(ByteBuffer buffer);

	/**
	 * Wrap a {@link ByteBuffer} into a {link ICharArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link ICharArray}
	 */
	ICharArray wrapCharArray(ByteBuffer buffer, int colSize);

	/**
	 * Wrap a {@link ByteBuffer} into a {link ICharArray}.
	 *
	 * @param buffer buffer to wrap
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link ICharArray}
	 */
	ICharArray wrapCharArray(ByteBuffer buffer, int yCount, int zCount);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IDoubleArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link IDoubleArray}
	 */
	IDoubleArray wrapDoubleArray(ByteBuffer buffer);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IDoubleArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link IDoubleArray}
	 */
	IDoubleArray wrapDoubleArray(ByteBuffer buffer, int colSize);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IDoubleArray}.
	 *
	 * @param buffer buffer to wrap
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link IDoubleArray}
	 */
	IDoubleArray wrapDoubleArray(ByteBuffer buffer, int yCount, int zCount);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IFloatArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link IFloatArray}
	 */
	IFloatArray wrapFloatArray(ByteBuffer buffer);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IFloatArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link IFloatArray}
	 */
	IFloatArray wrapFloatArray(ByteBuffer buffer, int colSize);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IFloatArray}.
	 *
	 * @param buffer buffer to wrap
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link IFloatArray}
	 */
	IFloatArray wrapFloatArray(ByteBuffer buffer, int yCount, int zCount);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IIntArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link IIntArray}
	 */
	IIntArray wrapIntArray(ByteBuffer buffer);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IIntArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link IIntArray}
	 */
	IIntArray wrapIntArray(ByteBuffer buffer, int colSize);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IIntArray}.
	 *
	 * @param buffer buffer to wrap
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link IIntArray}
	 */
	IIntArray wrapIntArray(ByteBuffer buffer, int yCount, int zCount);

	/**
	 * Wrap a {@link ByteBuffer} into a {link ILongArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link ILongArray}
	 */
	ILongArray wrapLongArray(ByteBuffer buffer);

	/**
	 * Wrap a {@link ByteBuffer} into a {link ILongArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link ILongArray}
	 */
	ILongArray wrapLongArray(ByteBuffer buffer, int colSize);

	/**
	 * Wrap a {@link ByteBuffer} into a {link ILongArray}.
	 *
	 * @param buffer buffer to wrap
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link ILongArray}
	 */
	ILongArray wrapLongArray(ByteBuffer buffer, int yCount, int zCount);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IShortArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link IShortArray}
	 */
	IShortArray wrapShortArray(ByteBuffer buffer);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IShortArray}.
	 *
	 * @param buffer buffer to wrap
	 * @return the instance of {@link IShortArray}
	 */
	IShortArray wrapShortArray(ByteBuffer buffer, int colSize);

	/**
	 * Wrap a {@link ByteBuffer} into a {link IShortArray}.
	 *
	 * @param buffer buffer to wrap
	 * @param yCount Number of elements on y axis
	 * @param zCount Number of elements on z axis
	 * @return the instance of {@link IShortArray}
	 */
	IShortArray wrapShortArray(ByteBuffer buffer, int yCount, int zCount);

	/**
	 * Build a LargeArray able to contains the specified number of elements.
	 *
	 * @param numberOfElements contenance du tabelau
	 * @param sizeOfOneElement taille d'un élément en byte
	 * @return l'instance de HugeArray fabriquée
	 * @throws IOException problème de création du fichier temporaire.
	 */
	LargeArray makeLargeArray(long numberOfElements, int sizeOfOneElement, String filePrefix) throws IOException;

	/**
	 * Build a LargeArray able to contains the specified number of elements.
	 *
	 * @param numberOfElements array capacity
	 * @param sizeOfOneElement size of an element
	 * @return the build array
	 * @throws IOException trouble with the file.
	 */
	LargeArray makeLargeArray(int rowCount, int colCount, int sizeOfOneElement, String filePrefix) throws IOException;

	/**
	 * * Build a 3D LargeArray able to contains the specified number of elements.
	 */
	LargeArray makeLargeArray(int xCount, int yCount, int zCount, int sizeOfOneElement, String filePrefix)
			throws IOException;
}
