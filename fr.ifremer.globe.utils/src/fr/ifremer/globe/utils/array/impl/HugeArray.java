/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array.impl;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * {@link IArray} mapped on a file.<br>
 * File size is greater than {@link Integer#MAX_VALUE}.
 * 
 * @author Apside
 *
 */
public class HugeArray extends AbstractByteBufferArray {

	/** Le logger. */
	protected static Logger LOGGER = LoggerFactory.getLogger(HugeArray.class);

	/** Max size of a segement. */
	final static int MAX_SEGMENT_SIZE = 0x7ffffff8; // assures alignment

	/** Mapped file. */
	protected File file;

	/** Ask to delete the file on close. */
	protected boolean deleteOnClose = false;

	/** MappedByteBuffer to store values. */
	protected MappedByteBuffer[] mappedByteBuffers;

	/** Size of one segment in byte. */
	protected int segmentSize = MAX_SEGMENT_SIZE;

	/** Size of one segment in byte. */
	protected int segmentCount = 0;

	private static long previous = -1;

	/**
	 * Constructor. Used by tests
	 * 
	 * @param elementCount
	 *            Number of stored primitive.
	 * @param sizeOfOneElement
	 *            Size of one stored primitive.
	 */
	HugeArray(long elementCount, int sizeOfOneElement) throws IOException {
		super(elementCount, sizeOfOneElement);
	}

	/**
	 * Constructor. Used by tests
	 * 
	 * @param elementCount
	 *            Number of stored primitive.
	 * @param sizeOfOneElement
	 *            Size of one stored primitive.
	 * @param colCount
	 *            number of elements by row.
	 */
	HugeArray(long elementCount, int sizeOfOneElement, int colCount) throws IOException {
		super(elementCount, sizeOfOneElement, colCount);
	}

	/**
	 * Constructor. Used by tests
	 * 
	 * @param elementCount
	 *            Number of stored primitive.
	 * @param sizeOfOneElement
	 *            Size of one stored primitive.
	 * @param yCount
	 *            number of elements int the y axis.
	 * @param zCount
	 *            number of elements int the z axis.
	 */
	HugeArray(long elementCount, int sizeOfOneElement, int yCount, int zCount) throws IOException {
		super(elementCount, sizeOfOneElement, yCount, zCount);
	}

	/**
	 * Opens and memory-maps the specified file, for read-only or read-write access,
	 * with a specified segment size.
	 *
	 * @param file
	 *            The file to open; must be accessible to user.
	 * @param sizeOfOneElement
	 *            Size of one stored primitive.
	 * @param readWrite
	 *            Pass <code>true</code> to open the file with read-write access,
	 *            <code>false</code> to open with read-only access.
	 */
	public HugeArray(File file, int sizeOfOneElement) throws IOException {
		super(file.length() / sizeOfOneElement, sizeOfOneElement);
		this.file = file;
	}

	/**
	 * Opens and memory-maps the specified file, for read-only or read-write access,
	 * for a two-dimensional array.
	 *
	 * @param file
	 *            The file to open; must be accessible to user.
	 * @param sizeOfOneElement
	 *            Size of one stored primitive.
	 * @param colCount
	 *            number of elements by row. Number of elements by column is
	 *            deducted from file size.
	 * @param readWrite
	 *            Pass <code>true</code> to open the file with read-write access,
	 *            <code>false</code> to open with read-only access.
	 */
	public HugeArray(File file, int sizeOfOneElement, int colCount) throws IOException {
		super(file.length() / sizeOfOneElement, sizeOfOneElement, colCount);
		this.file = file;
	}

	/**
	 * Opens and memory-maps the specified file, for read-only or read-write access,
	 * for a three-dimensional array.
	 *
	 * @param file
	 *            The file to open; must be accessible to user.
	 * @param sizeOfOneElement
	 *            Size of one stored primitive.
	 * @param yCount
	 *            number of elements int the y axis.
	 * @param zCount
	 *            number of elements int the z axis.
	 * @param readWrite
	 *            Pass <code>true</code> to open the file with read-write access,
	 *            <code>false</code> to open with read-only access.
	 */
	public HugeArray(File file, int sizeOfOneElement, int yCount, int zCount) throws IOException {
		super(file.length() / sizeOfOneElement, sizeOfOneElement, yCount, zCount);
		this.file = file;
	}

	/**
	 * Suivre le lien.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#close()
	 */
	@Override
	public void close() {
		// Already closed ?
		if (this.mappedByteBuffers != null) {
			try {
				for (MappedByteBuffer mappedFileBuffer : this.mappedByteBuffers) {
					if (!getByteBuffer(0).isReadOnly() && !isDeleteOnClose()) {
						mappedFileBuffer.force();
					}
					ArrayUtils.closeDirectBuffer(mappedFileBuffer);
				}
				if (isDeleteOnClose()) {
					this.file.delete();
				}
				this.mappedByteBuffers = null;
			} catch (Exception ex) {
				LOGGER.warn("Unable to close all MappedByteBuffer : ", ex);
			}
		}
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#flush()
	 */
	@Override
	public void flush() {
		Arrays.stream(this.mappedByteBuffers).forEach(buffer -> buffer.force());
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#getByteBuffer(int)
	 */
	@Override
	public MappedByteBuffer getByteBuffer(int index) {
		return this.mappedByteBuffers[0];
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#getByteBuffer(long)
	 */
	@Override
	public MappedByteBuffer getByteBuffer(long index) {
		long realIndex = index << getShift();
		long bufferIndex = realIndex / getSegmentSize();
		return this.mappedByteBuffers[(int) bufferIndex];
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#getByteBuffer(int,
	 *      int)
	 */
	@Override
	public MappedByteBuffer getByteBuffer(int rowIndex, int colIndex) {
		long offset = colIndex;
		offset += ArrayUtils.multiply(rowIndex, getYCount());
		return getByteBuffer(offset);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#getByteBuffer(int,
	 *      int, int)
	 */
	@Override
	public MappedByteBuffer getByteBuffer(int x, int y, int z) {
		long offset = z;
		offset += ArrayUtils.multiply(y, getZCount());
		offset += ArrayUtils.multiply(x, getYCount(), getZCount());
		return getByteBuffer(offset);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#computeIndexInBuffer(long)
	 */
	@Override
	protected int computeIndexInBuffer(long index) {
		long realIndex = index << getShift();
		long bufferIndex = realIndex / getSegmentSize();
		if (bufferIndex != previous) {
			// System.out.println(" computeIndexInBuffer index " + index + " realIndex " +
			// realIndex + " bufferIndex " + bufferIndex);
			previous = bufferIndex;
		}
		return (int) (realIndex - (bufferIndex * getSegmentSize()));
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#computeIndexInBuffer(int,
	 *      int)
	 */
	@Override
	int computeIndexInBuffer(int rowIndex, int colIndex) {
		long offset = colIndex;
		offset += ArrayUtils.multiply(rowIndex, getYCount());
		return computeIndexInBuffer(offset);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#computeIndexInBuffer(int,
	 *      int, int)
	 */
	@Override
	int computeIndexInBuffer(int x, int y, int z) {
		long offset = z;
		offset += ArrayUtils.multiply(y, getZCount());
		offset += ArrayUtils.multiply(x, getYCount(), getZCount());
		return computeIndexInBuffer(offset);
	}

	/** Open and map the file. */
	protected void openFile(boolean readWrite) throws IOException {
		computeSegmentProperties();

		RandomAccessFile mappedFile = null;
		try {
			String mode = readWrite ? "rw" : "r";
			MapMode mapMode = readWrite ? MapMode.READ_WRITE : MapMode.READ_ONLY;

			mappedFile = new RandomAccessFile(this.file, mode);
			FileChannel channel = mappedFile.getChannel();

			long fileSize = this.file.length();
			this.mappedByteBuffers = new MappedByteBuffer[getSegmentCount()];
			for (int segmentIndex = 0; segmentIndex < this.mappedByteBuffers.length; segmentIndex++) {
				long segmentOffset = ArrayUtils.multiply(segmentIndex, getSegmentSize());
				long thisSegmentSize = Math.min(fileSize - segmentOffset, getSegmentSize());
				this.mappedByteBuffers[segmentIndex] = channel.map(mapMode, segmentOffset, thisSegmentSize);
			}
		} finally {
			IOUtils.closeQuietly(mappedFile);
		}
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#setByteOrder(java.nio.ByteOrder)
	 */
	@Override
	public void setByteOrder(ByteOrder byteOrder) {
		for (MappedByteBuffer mappedByteBuffer : this.mappedByteBuffers) {
			mappedByteBuffer.order(byteOrder);
		}
	}

	/**
	 * @return the {@link #segmentSize}
	 */
	public int getSegmentSize() {
		return this.segmentSize;
	}

	/**
	 * @return the {@link #segmentCount}
	 */
	public int getSegmentCount() {
		return this.segmentCount;
	}

	/**
	 * Compute the size of each segment.
	 */
	protected void computeSegmentProperties() {
		int sizeOfOneRow = (getYCount() * getZCount()) << getShift();
		int nbRowPerSegment = MAX_SEGMENT_SIZE / sizeOfOneRow;
		this.segmentSize = nbRowPerSegment * sizeOfOneRow;
		this.segmentCount = (int) Math.ceil((double) (getElementCount() << getShift()) / this.segmentSize);
	}

	/**
	 * @return the {@link #deleteOnClose}
	 */
	public boolean isDeleteOnClose() {
		return this.deleteOnClose;
	}

	/**
	 * @param deleteOnClose
	 *            the {@link #deleteOnClose} to set
	 */
	public void setDeleteOnClose(boolean deleteOnClose) {
		this.deleteOnClose = deleteOnClose;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#dispose()
	 */
	@Override
	public void dispose() {
		close();
		this.file.delete();
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#reduceElementCount(long)
	 */
	@Override
	public void reduceElementCount(long newLimit) throws IOException {
		setElementCount(newLimit);
		boolean readWrite = !this.mappedByteBuffers[0].isReadOnly();
		flush();
		close();
		FileUtils.setLength(this.file, newLimit << getShift());
		openFile(readWrite);
	}

	/**
	 * @return the {@link #file}
	 */
	public File getFile() {
		return this.file;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#cloneArray()
	 */
	@Override
	protected HugeArray cloneArray() throws IOException {
		File file = TemporaryCache.createTemporaryFile(HUGE_ARRAY_FILE_PREFIX + COPY_PREFIX, ARRAY_FILE_SUFFIX);
		Files.copy(getFile().toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
		HugeArray result = new HugeArray(file, 1 << getShift(), getYCount(), getZCount());
		result.openFile(true);
		result.setByteOrder(this.mappedByteBuffers[0].order());
		result.setDeleteOnClose(true);
		return result;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#dump(java.io.File)
	 */
	@Override
	public void dump(File destination) throws IOException {
		if (!Files.isSameFile(getFile().toPath(), destination.toPath())) {
			Files.copy(getFile().toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
		}
	}

}
