/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array.impl;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * {@link IArray} using a MappedByteBuffer to store primitives.<br>
 * File size is less than {@link Integer#MAX_VALUE}.
 * 
 * @author Apside
 *
 */
public class LargeArray extends AbstractByteBufferArray {

	/** Le logger. */
	protected static Logger LOGGER = LoggerFactory.getLogger(LargeArray.class);

	/** Mapped file. */
	protected final File file;

	/** Ask to delete the file on close. */
	protected boolean deleteOnClose = false;

	/** MappedByteBuffer to store values. */
	private MappedByteBuffer mappedByteBuffer;

	/**
	 * Create a mapped file LargeArray, create a temporary file, open it in r/w and mapp it to a large array
	 * 
	 * @param elementCount The number of element in the file
	 * @param elementSize The size of one element
	 * @param filePrefix A prefix added to the generated file
	 * @throws IOException
	 */
	public static LargeArray create(long elementCount, int elementSize, String filePrefix) throws IOException {
		File file = TemporaryCache.createTemporaryFile(LARGE_ARRAY_FILE_PREFIX + filePrefix, ARRAY_FILE_SUFFIX);
		FileUtils.setLength(file, elementCount * elementSize);
		LargeArray result = new LargeArray(file, elementSize);
		result.openFile(true);
		result.setDeleteOnClose(true);
		return result;
	}

	/**
	 * Opens and memory-maps the specified file, for read-only or read-write access, with a specified segment size.
	 *
	 * @param file The file to open; must be accessible to user.
	 * @param sizeOfOneElement Size of one stored primitive.
	 * @param readWrite Pass <code>true</code> to open the file with read-write access, <code>false</code> to open with
	 *            read-only access.
	 */
	public LargeArray(File file, int sizeOfOneElement) throws IOException {
		super(file.length() / sizeOfOneElement, sizeOfOneElement);
		this.file = file;
	}

	/**
	 * Opens and memory-maps the specified file, for read-only or read-write access, for a two-dimensional array.
	 *
	 * @param file The file to open; must be accessible to user.
	 * @param sizeOfOneElement Size of one stored primitive.
	 * @param colCount number of elements by row. Number of elements by column is deducted from file size.
	 * @param readWrite Pass <code>true</code> to open the file with read-write access, <code>false</code> to open with
	 *            read-only access.
	 */
	public LargeArray(File file, int sizeOfOneElement, int colCount) throws IOException {
		super(file.length() / sizeOfOneElement, sizeOfOneElement, colCount);
		this.file = file;
	}

	/**
	 * Opens and memory-maps the specified file, for read-only or read-write access, for a three-dimensional array.
	 *
	 * @param file The file to open; must be accessible to user.
	 * @param sizeOfOneElement Size of one stored primitive.
	 * @param yCount number of elements int the y axis.
	 * @param zCount number of elements int the z axis.
	 * @param readWrite Pass <code>true</code> to open the file with read-write access, <code>false</code> to open with
	 *            read-only access.
	 */
	public LargeArray(File file, int sizeOfOneElement, int yCount, int zCount) throws IOException {
		super(file.length() / sizeOfOneElement, sizeOfOneElement, yCount, zCount);
		this.file = file;
	}

	/**
	 * Suivre le lien.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#close()
	 */
	@Override
	public void close() {
		try {
			// Already closed ?
			if (this.mappedByteBuffer != null) {
				if (!getByteBuffer(0).isReadOnly() && !isDeleteOnClose()) {
					flush();
				}
				ArrayUtils.closeDirectBuffer(getByteBuffer(0));
				if (isDeleteOnClose()) {
					this.file.delete();
				}
				this.mappedByteBuffer = null;
			}
		} catch (Exception ex) {
			LOGGER.warn("Unable to close all MappedByteBuffer : ", ex);
		}
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#dispose()
	 */
	@Override
	public void dispose() {
		close();
		this.file.delete();
	}

	/**
	 * Accessor.
	 */
	public File getFile() {
		return this.file;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#flush()
	 */
	@Override
	public void flush() {
		this.mappedByteBuffer.force();
	}

	/**
	 * deleteOnClose accessor.
	 */
	public boolean isDeleteOnClose() {
		return this.deleteOnClose;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#isWritable()
	 */
	@Override
	public boolean isWritable() {
		return !this.mappedByteBuffer.isReadOnly();
	}

	/** Map the file. */
	protected void openFile(boolean readWrite) throws IOException {
		RandomAccessFile mappedFile = null;
		try {
			String mode = readWrite ? "rw" : "r";
			MapMode mapMode = readWrite ? MapMode.READ_WRITE : MapMode.READ_ONLY;

			mappedFile = new RandomAccessFile(this.file, mode);
			FileChannel channel = mappedFile.getChannel();

			this.mappedByteBuffer = channel.map(mapMode, 0, this.file.length());

		} finally {
			IOUtils.closeQuietly(mappedFile);
		}
	}

	/**
	 * Ask to delete the file on close.
	 */
	public void setDeleteOnClose(boolean deleteOnClose) {
		this.deleteOnClose = deleteOnClose;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#getByteBuffer(int)
	 */
	@Override
	public MappedByteBuffer getByteBuffer(int index) {
		return this.mappedByteBuffer;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#getByteBuffer(long)
	 */
	@Override
	public MappedByteBuffer getByteBuffer(long index) {
		return this.mappedByteBuffer;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#getByteBuffer(int, int)
	 */
	@Override
	public MappedByteBuffer getByteBuffer(int rowIndex, int colIndex) {
		return this.mappedByteBuffer;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#getByteBuffer(int, int, int)
	 */
	@Override
	public ByteBuffer getByteBuffer(int x, int y, int z) {
		return this.mappedByteBuffer;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#setByteOrder(java.nio.ByteOrder)
	 */
	@Override
	public void setByteOrder(ByteOrder byteOrder) {
		this.mappedByteBuffer.order(byteOrder);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#reduceElementCount(long)
	 */
	@Override
	public void reduceElementCount(long newLimit) throws IOException {
		setElementCount(newLimit);
		boolean readWrite = !this.mappedByteBuffer.isReadOnly();
		flush();
		close();
		FileUtils.setLength(this.file, newLimit << getShift());
		openFile(readWrite);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#fill(double)
	 */
	@Override
	public void fill(double value) {
		if (!this.mappedByteBuffer.isReadOnly()) {
			this.mappedByteBuffer.rewind();
			for (long i = 0; i < getElementCount(); i++) {
				this.mappedByteBuffer.putDouble(value);
			}
		}
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.impl.AbstractByteBufferArray#cloneArray()
	 */
	@Override
	public LargeArray cloneArray() throws IOException {
		String prefix = getFile().getName().startsWith(COPY_PREFIX) ? "" : COPY_PREFIX;
		String fileName = prefix + getFile().getName().replaceAll("[\\d.]*.tmp", "");
		File cloneFile = TemporaryCache.createTemporaryFile(fileName, ARRAY_FILE_SUFFIX);
		Files.copy(getFile().toPath(), cloneFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		return clone(cloneFile);
	}

	/**
	 * @return a clone of this {@link LargeArray} based on a new file.
	 */
	public LargeArray clone(File cloneFile) throws IOException {
		LargeArray result = new LargeArray(cloneFile, 1 << getShift(), getYCount(), getZCount());
		result.openFile(true);
		result.setByteOrder(this.mappedByteBuffer.order());
		result.setDeleteOnClose(true);
		return result;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.array.IArray#dump(java.io.File)
	 */
	@Override
	public void dump(File destination) throws IOException {
		if (!Files.isSameFile(getFile().toPath(), destination.toPath())) {
			Files.copy(getFile().toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
		}
	}
}
