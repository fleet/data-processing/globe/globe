/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array.impl;

/**
 * Constantes du package.
 * 
 * @author Apside
 *
 */
public interface ArrayConstant {

	/** Préfixe des fichiers temporaires. */
	String ARRAY_FILE_SUFFIX = ".tmp";
	/** Suffixe des fichiers temporaires. */
	String HUGE_ARRAY_FILE_PREFIX = "HugeArray_";
	/** Suffixe des fichiers temporaires. */
	String LARGE_ARRAY_FILE_PREFIX = "LargeArray_";

	/** Page size of a mapping buffer. */
	long PAGE_SIZE = 4 * 1024;

	/** Boolean.TRUE sous forme de byte. */
	byte TRUE = (byte) 1; // 1 GB

	/** Boolean.FALSE sous forme de byte. */
	byte FALSE = (byte) 0; // 1 GB

	/** Taille max d'un segment. */
	int MAX_SEGMENT_SIZE = 0x8000000; // 1 GB

	/** Prefix used in temporary file name. */
	String CHAR_PREFIX = "Char_";
	String FLOAT_PREFIX = "Float_";
	String BYTE_PREFIX = "Byte_";
	String BOOLEAN_PREFIX = "Boolean_";
	String DOUBLE_PREFIX = "Double_";
	String SHORT_PREFIX = "Short_";
	String LONG_PREFIX = "Long_";
	String INT_PREFIX = "Int_";
	String COPY_PREFIX = "Copy_";
}
