/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array.impl;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.IBooleanArray;
import fr.ifremer.globe.utils.array.IByteArray;
import fr.ifremer.globe.utils.array.ICharArray;
import fr.ifremer.globe.utils.array.IDoubleArray;
import fr.ifremer.globe.utils.array.IFloatArray;
import fr.ifremer.globe.utils.array.IIntArray;
import fr.ifremer.globe.utils.array.ILongArray;
import fr.ifremer.globe.utils.array.IShortArray;
import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * Factory of {@link IArray}.
 *
 * @author Apside
 *
 */
@Component(name = "globe_utils_array_factory", service = IArrayFactory.class)
public class ArrayFactory implements IArrayFactory, ArrayConstant {

	private static final Logger logger = LoggerFactory.getLogger(ArrayFactory.class);

	/**
	 * Contructor.<br>
	 * Invoked only once because this is an eclipse service.
	 */
	public ArrayFactory() {
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeIntArray(long)
	 */
	@Override
	public IIntArray makeIntArray(long numberOfElements) throws IOException {
		return makeArray(numberOfElements, Integer.BYTES, INT_PREFIX);
	}

	@Override
	public IIntArray makeIntArray(long numberOfElements, String fileNamePrefix) throws IOException {
		return makeArray(numberOfElements, Integer.BYTES, fileNamePrefix);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeIntArray(int, int)
	 */
	@Override
	public IIntArray makeIntArray(int rowCount, int colCount) throws IOException {
		return makeArray(rowCount, colCount, Integer.BYTES, INT_PREFIX);
	}

	@Override
	public IIntArray makeIntArray(int rowCount, int colCount, String fileNamePrefix) throws IOException {
		return makeArray(rowCount, colCount, Integer.BYTES, fileNamePrefix);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeIntArray(int, int, int)
	 */
	@Override
	public IIntArray makeIntArray(int xCount, int yCount, int zCount) throws IOException {
		return makeArray(xCount, yCount, zCount, Integer.BYTES, INT_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeLongArray(long)
	 */
	@Override
	public ILongArray makeLongArray(long numberOfElements) throws IOException {
		return makeArray(numberOfElements, Long.BYTES, LONG_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeLongArray(int, int)
	 */
	@Override
	public ILongArray makeLongArray(int rowCount, int colCount) throws IOException {
		return makeArray(rowCount, colCount, Long.BYTES, LONG_PREFIX);
	}

	@Override
	public ILongArray makeLongArray(int xCount, int yCount, String filePrefix) throws IOException {
		return makeArray(xCount, yCount, Long.BYTES, filePrefix);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeLongArray(int, int, int)
	 */
	@Override
	public ILongArray makeLongArray(int xCount, int yCount, int zCount) throws IOException {
		return makeArray(xCount, yCount, zCount, Long.BYTES, LONG_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeShortArray(long)
	 */
	@Override
	public IShortArray makeShortArray(long numberOfElements) throws IOException {
		return makeArray(numberOfElements, Short.BYTES, SHORT_PREFIX);
	}

	@Override
	public IShortArray makeShortArray(long numberOfElements, String fileNamePrefix) throws IOException {
		return makeArray(numberOfElements, Short.BYTES, fileNamePrefix);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeShortArray(int, int)
	 */
	@Override
	public IShortArray makeShortArray(int rowCount, int colCount) throws IOException {
		return makeArray(rowCount, colCount, Short.BYTES, SHORT_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeShortArray(int, int, int)
	 */
	@Override
	public IShortArray makeShortArray(int xCount, int yCount, int zCount) throws IOException {
		return makeArray(xCount, yCount, zCount, Short.BYTES, SHORT_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeDoubleArray(long)
	 */
	@Override
	public IDoubleArray makeDoubleArray(long numberOfElements) throws IOException {
		return makeArray(numberOfElements, Double.BYTES, DOUBLE_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeDoubleArray(int, int)
	 */
	@Override
	public IDoubleArray makeDoubleArray(int rowCount, int colCount) throws IOException {
		return makeArray(rowCount, colCount, Double.BYTES, DOUBLE_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeDoubleArray(int, int, int)
	 */
	@Override
	public IDoubleArray makeDoubleArray(int xCount, int yCount, int zCount) throws IOException {
		return makeArray(xCount, yCount, zCount, Double.BYTES, DOUBLE_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeBooleanArray(long)
	 */
	@Override
	public IBooleanArray makeBooleanArray(long numberOfElements) throws IOException {
		return makeArray(numberOfElements, Byte.BYTES, BOOLEAN_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeBooleanArray(int, int)
	 */
	@Override
	public IBooleanArray makeBooleanArray(int rowCount, int colCount) throws IOException {
		return makeArray(rowCount, colCount, Byte.BYTES, BOOLEAN_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeBooleanArray(int, int, int)
	 */
	@Override
	public IBooleanArray makeBooleanArray(int xCount, int yCount, int zCount) throws IOException {
		return makeArray(xCount, yCount, zCount, Byte.BYTES, BOOLEAN_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeByteArray(long)
	 */
	@Override
	public IByteArray makeByteArray(long numberOfElements) throws IOException {
		return makeArray(numberOfElements, Byte.BYTES, BYTE_PREFIX);
	}

	@Override
	public IByteArray makeByteArray(long numberOfElements, String fileNamePrefix) throws IOException {
		return makeArray(numberOfElements, Byte.BYTES, fileNamePrefix);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeByteArray(int, int)
	 */
	@Override
	public IByteArray makeByteArray(int rowCount, int colCount) throws IOException {
		return makeArray(rowCount, colCount, Byte.BYTES, BYTE_PREFIX);
	}

	@Override
	public IByteArray makeByteArray(int rowCount, int colCount, String fileNamePrefix) throws IOException {
		return makeArray(rowCount, colCount, Byte.BYTES, fileNamePrefix);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeByteArray(int, int, int)
	 */
	@Override
	public IByteArray makeByteArray(int xCount, int yCount, int zCount) throws IOException {
		return makeArray(xCount, yCount, zCount, Byte.BYTES, BYTE_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeFloatArray(long)
	 */
	@Override
	public IFloatArray makeFloatArray(long numberOfElements) throws IOException {
		return makeArray(numberOfElements, Float.BYTES, FLOAT_PREFIX);
	}

	@Override
	public IFloatArray makeFloatArray(long numberOfElements, String fileNamePrefix) throws IOException {
		return makeArray(numberOfElements, Float.BYTES, fileNamePrefix);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeFloatArray(int, int)
	 */
	@Override
	public IFloatArray makeFloatArray(int rowCount, int colCount) throws IOException {
		return makeArray(rowCount, colCount, Float.BYTES, FLOAT_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeFloatArray(int, int, int)
	 */
	@Override
	public IFloatArray makeFloatArray(int xCount, int yCount, int zCount) throws IOException {
		return makeArray(xCount, yCount, zCount, Float.BYTES, FLOAT_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeCharArray(long)
	 */
	@Override
	public ICharArray makeCharArray(long numberOfElements) throws IOException {
		return makeArray(numberOfElements, Character.BYTES, CHAR_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeCharArray(int, int)
	 */
	@Override
	public ICharArray makeCharArray(int rowCount, int colCount) throws IOException {
		return makeArray(rowCount, colCount, Character.BYTES, CHAR_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#makeCharArray(int, int, int)
	 */
	@Override
	public ICharArray makeCharArray(int xCount, int yCount, int zCount) throws IOException {
		return makeArray(xCount, yCount, zCount, Character.BYTES, CHAR_PREFIX);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openDoubleArray(java.io.File, int, int)
	 */
	@Override
	public IDoubleArray openDoubleArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException {
		return openArray(file, rowCount, colCount, Double.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IBooleanArray#openBooleanArray(java.io.File, boolean)
	 */
	@Override
	public IBooleanArray openBooleanArray(File file, boolean readWrite) throws IOException {
		return openArray(file, file.length() / Byte.BYTES, Byte.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openBooleanArray(java.io.File, long, boolean)
	 */
	@Override
	public IBooleanArray openBooleanArray(File file, long numberOfElements, boolean readWrite) throws IOException {
		return openArray(file, numberOfElements, Byte.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openBooleanArray(java.io.File, int, int)
	 */
	@Override
	public IBooleanArray openBooleanArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException {
		return openArray(file, rowCount, colCount, Byte.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openByteArray(java.io.File, int, int)
	 */
	@Override
	public IByteArray openByteArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException {
		return openArray(file, rowCount, colCount, Byte.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openCharArray(java.io.File, int, int)
	 */
	@Override
	public ICharArray openCharArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException {
		return openArray(file, rowCount, colCount, Character.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openFloatArray(java.io.File, int, int)
	 */
	@Override
	public IFloatArray openFloatArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException {
		return openArray(file, rowCount, colCount, Float.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openIntArray(java.io.File, int, int)
	 */
	@Override
	public IIntArray openIntArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException {
		return openArray(file, rowCount, colCount, Integer.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openLongArray(java.io.File, int, int)
	 */
	@Override
	public ILongArray openLongArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException {
		return openArray(file, rowCount, colCount, Long.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openShortArray(java.io.File, int, int)
	 */
	@Override
	public IShortArray openShortArray(File file, int rowCount, int colCount, boolean readWrite) throws IOException {
		return openArray(file, rowCount, colCount, Short.BYTES, readWrite);
	}

	/**
	 * @return true when size of the buffer is very small.
	 */
	protected boolean isTiny(int... values) {
		return ArrayUtils.multiply(values) < PAGE_SIZE;
	}

	/**
	 * @return true when size of the buffer is very small.
	 */
	protected boolean isTiny(long numberOfElements, int sizeOfOneElement) {
		long totalSize = numberOfElements;
		totalSize *= sizeOfOneElement;
		return totalSize < PAGE_SIZE;
	}

	/**
	 * @return true when size of the buffer is Large.
	 */
	protected boolean isLarge(int... values) {
		return ArrayUtils.multiply(values) < HugeArray.MAX_SEGMENT_SIZE;
	}

	/**
	 * @return true when size of the buffer is Large.
	 */
	protected boolean isLarge(long numberOfElements, int sizeOfOneElement) {
		long totalSize = numberOfElements;
		totalSize *= sizeOfOneElement;
		return totalSize < HugeArray.MAX_SEGMENT_SIZE;
	}

	/**
	 * Make an array depending on the number of elements.
	 */
	// check is not necessary : all Array implements all inhnerited interfaces of IArray
	@SuppressWarnings("unchecked")
	protected <T extends IArray> T makeArray(long numberOfElements, int sizeOfOneElement, String filePrefix)
			throws IOException {
		if (isTiny(numberOfElements, sizeOfOneElement)) {
			return (T) new TinyArray((int) numberOfElements, sizeOfOneElement);
		} else if (isLarge(numberOfElements, sizeOfOneElement)) {
			return (T) makeLargeArray(numberOfElements, sizeOfOneElement, filePrefix);
		}
		return (T) makeHugeArray(numberOfElements, sizeOfOneElement, filePrefix);
	}

	/**
	 * Make an array two-dimentional depending on the number of elements.
	 */
	// check id not necessary : all Array implements all inhnerited interfaces of IArray
	@SuppressWarnings("unchecked")
	protected <T extends IArray> T makeArray(int rowCount, int colCount, int sizeOfOneElement, String filePrefix)
			throws IOException {
		if (isTiny(rowCount, colCount, sizeOfOneElement)) {
			return (T) new TinyArray(rowCount * colCount, sizeOfOneElement, colCount);
		} else if (isLarge(rowCount, colCount, sizeOfOneElement)) {
			return (T) makeLargeArray(rowCount, colCount, sizeOfOneElement, filePrefix);
		}
		return (T) makeHugeArray(rowCount, colCount, sizeOfOneElement, filePrefix);
	}

	/**
	 * Make an three-dimentional array depending on the number of elements.
	 */
	// check id not necessary : all Array implements all inhnerited interfaces
	// of IArray
	@SuppressWarnings("unchecked")
	protected <T extends IArray> T makeArray(int xCount, int yCount, int zCount, int sizeOfOneElement,
			String filePrefix) throws IOException {
		if (isTiny(xCount, yCount, zCount, sizeOfOneElement)) {
			return (T) new TinyArray(xCount * yCount * zCount, sizeOfOneElement, yCount, zCount);
		} else if (isLarge(xCount, yCount, zCount, sizeOfOneElement)) {
			return (T) makeLargeArray(xCount, yCount, zCount, sizeOfOneElement, filePrefix);
		}
		return (T) makeHugeArray(xCount, yCount, zCount, sizeOfOneElement, filePrefix);
	}

	@Override
	public LargeArray openLargeArray(File file, int sizeOfOneElement) throws IOException {
		LargeArray result = new LargeArray(file, sizeOfOneElement);
		result.openFile(true);
		result.setByteOrder(ByteOrder.nativeOrder());
		result.setDeleteOnClose(true);
		return result;
	}

	/**
	 * Create a {@link HugeArray} mapped on a file.
	 */
	@SuppressWarnings("unchecked")
	// check is not necessary : all Array implements all inhnerited interfaces of IArray
	public <T extends IArray> T openArray(File file, long numberOfElements, int sizeOfOneElement, boolean readWrite)
			throws IOException {
		org.apache.commons.io.FileUtils.touch(file);
		FileUtils.setLength(file, numberOfElements * sizeOfOneElement);
		if (isLarge(numberOfElements, sizeOfOneElement)) {
			logger.info("LargeArray: " + file.getName());
			LargeArray result = new LargeArray(file, sizeOfOneElement);
			result.openFile(readWrite);
			return (T) result;
		}
		HugeArray result = new HugeArray(file, sizeOfOneElement);
		logger.info("HugeArray: " + file.getName());

		result.openFile(readWrite);
		return (T) result;
	}

	/**
	 * Create a bidimentionnal {@link HugeArray} mapped on a file.
	 */
	@SuppressWarnings("unchecked")
	// check is not necessary : all Array implements all inhnerited interfaces
	// of IArray
	protected <T extends IArray> T openArray(File file, int rowCount, int colCount, int sizeOfOneElement,
			boolean readWrite) throws IOException {
		org.apache.commons.io.FileUtils.touch(file);
		FileUtils.setLength(file, ArrayUtils.multiply(rowCount, colCount, sizeOfOneElement));
		if (isLarge(rowCount, colCount, sizeOfOneElement)) {
			LargeArray result = new LargeArray(file, sizeOfOneElement, colCount);
			System.out.println(" Large " + file.getName());

			result.openFile(readWrite);
			return (T) result;
		}
		HugeArray result = new HugeArray(file, sizeOfOneElement, colCount);
		System.out.println(" HugeArray " + file.getName());

		result.openFile(readWrite);
		return (T) result;
	}

	/**
	 * Create a bidimentionnal {@link HugeArray} mapped on a file.
	 */
	@SuppressWarnings("unchecked")
	// check is not necessary : all Array implements all inhnerited interfaces of IArray
	protected <T extends IArray> T openArray(File file, int xCount, int yCount, int zCount, int sizeOfOneElement,
			boolean readWrite) throws IOException {
		org.apache.commons.io.FileUtils.touch(file);
		FileUtils.setLength(file, ArrayUtils.multiply(xCount, yCount, zCount, sizeOfOneElement));
		if (isLarge(xCount, yCount, zCount, sizeOfOneElement)) {
			LargeArray result = new LargeArray(file, sizeOfOneElement, yCount, zCount);
			System.out.println(" Large " + file.getName());

			result.openFile(readWrite);
			return (T) result;
		}
		HugeArray result = new HugeArray(file, sizeOfOneElement, yCount, zCount);
		System.out.println(" HugeArray " + file.getName());

		result.openFile(readWrite);
		return (T) result;
	}

	/**
	 * Build a HugeArray able to contains the specified number of elements.
	 *
	 * @param numberOfElements array capacity
	 * @param sizeOfOneElement size of an element
	 * @return the build array
	 * @throws IOException trouble with the file.
	 */
	protected HugeArray makeHugeArray(int rowCount, int colCount, int sizeOfOneElement, String filePrefix)
			throws IOException {
		File file = TemporaryCache.createTemporaryFile(HUGE_ARRAY_FILE_PREFIX + filePrefix, ARRAY_FILE_SUFFIX);
		FileUtils.setLength(file, ArrayUtils.multiply(rowCount, colCount, sizeOfOneElement));
		HugeArray result = new HugeArray(file, sizeOfOneElement, colCount);
		result.openFile(true);
		result.setDeleteOnClose(true);
		return result;
	}

	/**
	 * Build a HugeArray able to contains the specified number of elements.
	 *
	 * @param numberOfElements array capacity
	 * @param sizeOfOneElement size of an element
	 * @return the build array
	 * @throws IOException trouble with the file.
	 */
	protected HugeArray makeHugeArray(int rowCount, int xCount, int zCount, int sizeOfOneElement, String filePrefix)
			throws IOException {
		File file = TemporaryCache.createTemporaryFile(HUGE_ARRAY_FILE_PREFIX + filePrefix, ARRAY_FILE_SUFFIX);
		FileUtils.setLength(file, ArrayUtils.multiply(rowCount, xCount, zCount, sizeOfOneElement));
		HugeArray result = new HugeArray(file, sizeOfOneElement, xCount, zCount);
		result.openFile(true);
		result.setDeleteOnClose(true);
		return result;
	}

	/**
	 * Build a HugeArray able to contains the specified number of elements.
	 *
	 * @param numberOfElements contenance du tabelau
	 * @param sizeOfOneElement taille d'un élément en byte
	 * @return l'instance de HugeArray fabriquée
	 * @throws IOException problème de création du fichier temporaire.
	 */
	HugeArray makeHugeArray(long numberOfElements, int sizeOfOneElement, String filePrefix) throws IOException {
		File file = TemporaryCache.createTemporaryFile(HUGE_ARRAY_FILE_PREFIX + filePrefix, ARRAY_FILE_SUFFIX);
		FileUtils.setLength(file, numberOfElements * sizeOfOneElement);
		HugeArray result = new HugeArray(file, sizeOfOneElement);
		result.openFile(true);
		result.setDeleteOnClose(true);
		return result;
	}

	/**
	 * Build a LargeArray able to contains the specified number of elements.
	 *
	 * @param numberOfElements array capacity
	 * @param sizeOfOneElement size of an element
	 * @return the build array
	 * @throws IOException trouble with the file.
	 */
	@Override
	public LargeArray makeLargeArray(int rowCount, int colCount, int sizeOfOneElement, String filePrefix)
			throws IOException {
		File file = TemporaryCache.createTemporaryFile(LARGE_ARRAY_FILE_PREFIX + filePrefix, ARRAY_FILE_SUFFIX);
		FileUtils.setLength(file, rowCount * colCount * sizeOfOneElement);
		LargeArray result = new LargeArray(file, sizeOfOneElement, colCount);
		result.openFile(true);
		result.setDeleteOnClose(true);
		return result;
	}

	/**
	 * Build a LargeArray able to contains the specified number of elements.
	 *
	 * @param numberOfElements array capacity
	 * @param sizeOfOneElement size of an element
	 * @return the build array
	 * @throws IOException trouble with the file.
	 */
	@Override
	public LargeArray makeLargeArray(int xCount, int yCount, int zCount, int sizeOfOneElement, String filePrefix)
			throws IOException {
		File file = TemporaryCache.createTemporaryFile(LARGE_ARRAY_FILE_PREFIX + filePrefix, ARRAY_FILE_SUFFIX);
		FileUtils.setLength(file, ArrayUtils.multiply(xCount, yCount, zCount, sizeOfOneElement));
		LargeArray result = new LargeArray(file, sizeOfOneElement, yCount, zCount);
		result.openFile(true);
		result.setDeleteOnClose(true);
		return result;
	}

	/**
	 * Build a LargeArray able to contains the specified number of elements.
	 *
	 * @param numberOfElements contenance du tabelau
	 * @param sizeOfOneElement taille d'un élément en byte
	 * @return l'instance de HugeArray fabriquée
	 * @throws IOException problème de création du fichier temporaire.
	 */
	@Override
	public LargeArray makeLargeArray(long numberOfElements, int sizeOfOneElement, String filePrefix)
			throws IOException {
		File file = TemporaryCache.createTemporaryFile(LARGE_ARRAY_FILE_PREFIX + filePrefix, ARRAY_FILE_SUFFIX);
		FileUtils.setLength(file, numberOfElements * sizeOfOneElement);
		LargeArray result = new LargeArray(file, sizeOfOneElement);
		result.openFile(true);
		result.setDeleteOnClose(true);
		return result;
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openBooleanArray(java.io.File, int, int, int, boolean)
	 */
	@Override
	public IBooleanArray openBooleanArray(File file, int xCount, int yCount, int zCount, boolean readWrite)
			throws IOException {
		return openArray(file, xCount, yCount, zCount, Byte.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IByteArray#openByteArray(java.io.File, boolean)
	 */
	@Override
	public IByteArray openByteArray(File file, boolean readWrite) throws IOException {
		return openArray(file, file.length() / Byte.BYTES, Byte.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openByteArray(java.io.File, long, boolean)
	 */
	@Override
	public IByteArray openByteArray(File file, long numberOfElements, boolean readWrite) throws IOException {
		return openArray(file, numberOfElements, Byte.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openByteArray(java.io.File, int, int, int, boolean)
	 */
	@Override
	public IByteArray openByteArray(File file, int xCount, int yCount, int zCount, boolean readWrite)
			throws IOException {
		return openArray(file, xCount, yCount, zCount, Byte.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openCharArray(java.io.File, boolean)
	 */
	@Override
	public ICharArray openCharArray(File file, boolean readWrite) throws IOException {
		return openArray(file, file.length() / Character.BYTES, Character.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openCharArray(java.io.File, long, boolean)
	 */
	@Override
	public ICharArray openCharArray(File file, long numberOfElements, boolean readWrite) throws IOException {
		return openArray(file, numberOfElements, Character.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openCharArray(java.io.File, int, int, int, boolean)
	 */
	@Override
	public ICharArray openCharArray(File file, int xCount, int yCount, int zCount, boolean readWrite)
			throws IOException {
		return openArray(file, xCount, yCount, zCount, Character.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#openDoubleArray(java.io.File, boolean)
	 */
	@Override
	public IDoubleArray openDoubleArray(File file, boolean readWrite) throws IOException {
		return openArray(file, file.length() / Double.BYTES, Double.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openDoubleArray(java.io.File, long, boolean)
	 */
	@Override
	public IDoubleArray openDoubleArray(File file, long numberOfElements, boolean readWrite) throws IOException {
		return openArray(file, numberOfElements, Double.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openDoubleArray(java.io.File, int, int, int, boolean)
	 */
	@Override
	public IDoubleArray openDoubleArray(File file, int xCount, int yCount, int zCount, boolean readWrite)
			throws IOException {
		return openArray(file, xCount, yCount, zCount, Double.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IFloatArray#openFloatArray(java.io.File, boolean)
	 */
	@Override
	public IFloatArray openFloatArray(File file, boolean readWrite) throws IOException {
		return openArray(file, file.length() / Float.BYTES, Float.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openFloatArray(java.io.File, long, boolean)
	 */
	@Override
	public IFloatArray openFloatArray(File file, long numberOfElements, boolean readWrite) throws IOException {
		return openArray(file, numberOfElements, Float.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openFloatArray(java.io.File, int, int, int, boolean)
	 */
	@Override
	public IFloatArray openFloatArray(File file, int xCount, int yCount, int zCount, boolean readWrite)
			throws IOException {
		return openArray(file, xCount, yCount, zCount, Float.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IIntArray#openIntArray(java.io.File, boolean)
	 */
	@Override
	public IIntArray openIntArray(File file, boolean readWrite) throws IOException {
		return openArray(file, file.length() / Integer.BYTES, Integer.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openIntArray(java.io.File, long, boolean)
	 */
	@Override
	public IIntArray openIntArray(File file, long numberOfElements, boolean readWrite) throws IOException {
		return openArray(file, numberOfElements, Integer.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openIntArray(java.io.File, int, int, int, boolean)
	 */
	@Override
	public IIntArray openIntArray(File file, int xCount, int yCount, int zCount, boolean readWrite) throws IOException {
		return openArray(file, xCount, yCount, zCount, Integer.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ILongArray#openLongArray(java.io.File, boolean)
	 */
	@Override
	public ILongArray openLongArray(File file, boolean readWrite) throws IOException {
		return openArray(file, file.length() / Long.BYTES, Long.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openLongArray(java.io.File, long, boolean)
	 */
	@Override
	public ILongArray openLongArray(File file, long numberOfElements, boolean readWrite) throws IOException {
		return openArray(file, numberOfElements, Long.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openLongArray(java.io.File, int, int, int, boolean)
	 */
	@Override
	public ILongArray openLongArray(File file, int xCount, int yCount, int zCount, boolean readWrite)
			throws IOException {
		return openArray(file, xCount, yCount, zCount, Long.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IShortArray#openShortArray(java.io.File, boolean)
	 */
	@Override
	public IShortArray openShortArray(File file, boolean readWrite) throws IOException {
		return openArray(file, file.length() / Short.BYTES, Short.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openShortArray(java.io.File, long, boolean)
	 */
	@Override
	public IShortArray openShortArray(File file, long numberOfElements, boolean readWrite) throws IOException {
		return openArray(file, numberOfElements, Short.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#openShortArray(java.io.File, int, int, int, boolean)
	 */
	@Override
	public IShortArray openShortArray(File file, int xCount, int yCount, int zCount, boolean readWrite)
			throws IOException {
		return openArray(file, xCount, yCount, zCount, Short.BYTES, readWrite);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapBooleanArray(java.nio.ByteBuffer)
	 */
	@Override
	public IBooleanArray wrapBooleanArray(ByteBuffer buffer) {
		return new TinyArray(buffer, Byte.BYTES);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapBooleanArray(java.nio.ByteBuffer, int)
	 */
	@Override
	public IBooleanArray wrapBooleanArray(ByteBuffer buffer, int colSize) {
		return new TinyArray(buffer, Byte.BYTES, colSize);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapBooleanArray(java.nio.ByteBuffer, int, int)
	 */
	@Override
	public IBooleanArray wrapBooleanArray(ByteBuffer buffer, int yCount, int zCount) {
		return new TinyArray(buffer, Byte.BYTES, yCount, zCount);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapByteArray(java.nio.ByteBuffer)
	 */
	@Override
	public IByteArray wrapByteArray(ByteBuffer buffer) {
		return new TinyArray(buffer, Byte.BYTES);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapByteArray(java.nio.ByteBuffer, int)
	 */
	@Override
	public IByteArray wrapByteArray(ByteBuffer buffer, int colSize) {
		return new TinyArray(buffer, Byte.BYTES, colSize);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapByteArray(java.nio.ByteBuffer, int, int)
	 */
	@Override
	public IByteArray wrapByteArray(ByteBuffer buffer, int yCount, int zCount) {
		return new TinyArray(buffer, Byte.BYTES, yCount, zCount);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapCharArray(java.nio.ByteBuffer)
	 */
	@Override
	public ICharArray wrapCharArray(ByteBuffer buffer) {
		return new TinyArray(buffer, Character.BYTES);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapCharArray(java.nio.ByteBuffer, int)
	 */
	@Override
	public ICharArray wrapCharArray(ByteBuffer buffer, int colSize) {
		return new TinyArray(buffer, Character.BYTES, colSize);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapCharArray(java.nio.ByteBuffer, int, int)
	 */
	@Override
	public ICharArray wrapCharArray(ByteBuffer buffer, int yCount, int zCount) {
		return new TinyArray(buffer, Character.BYTES, yCount, zCount);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapDoubleArray(java.nio.ByteBuffer)
	 */
	@Override
	public IDoubleArray wrapDoubleArray(ByteBuffer buffer) {
		return new TinyArray(buffer, Double.BYTES);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapDoubleArray(java.nio.ByteBuffer, int)
	 */
	@Override
	public IDoubleArray wrapDoubleArray(ByteBuffer buffer, int colSize) {
		return new TinyArray(buffer, Double.BYTES, colSize);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapDoubleArray(java.nio.ByteBuffer, int, int)
	 */
	@Override
	public IDoubleArray wrapDoubleArray(ByteBuffer buffer, int yCount, int zCount) {
		return new TinyArray(buffer, Double.BYTES, yCount, zCount);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapFloatArray(java.nio.ByteBuffer)
	 */
	@Override
	public IFloatArray wrapFloatArray(ByteBuffer buffer) {
		return new TinyArray(buffer, Float.BYTES);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapFloatArray(java.nio.ByteBuffer, int)
	 */
	@Override
	public IFloatArray wrapFloatArray(ByteBuffer buffer, int colSize) {
		return new TinyArray(buffer, Float.BYTES, colSize);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapFloatArray(java.nio.ByteBuffer, int, int)
	 */
	@Override
	public IFloatArray wrapFloatArray(ByteBuffer buffer, int yCount, int zCount) {
		return new TinyArray(buffer, Float.BYTES, yCount, zCount);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapIntArray(java.nio.ByteBuffer)
	 */
	@Override
	public IIntArray wrapIntArray(ByteBuffer buffer) {
		return new TinyArray(buffer, Integer.BYTES);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapIntArray(java.nio.ByteBuffer, int)
	 */
	@Override
	public IIntArray wrapIntArray(ByteBuffer buffer, int colSize) {
		return new TinyArray(buffer, Integer.BYTES, colSize);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapIntArray(java.nio.ByteBuffer, int, int)
	 */
	@Override
	public IIntArray wrapIntArray(ByteBuffer buffer, int yCount, int zCount) {
		return new TinyArray(buffer, Integer.BYTES, yCount, zCount);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapLongArray(java.nio.ByteBuffer)
	 */
	@Override
	public ILongArray wrapLongArray(ByteBuffer buffer) {
		return new TinyArray(buffer, Long.BYTES);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapLongArray(java.nio.ByteBuffer, int)
	 */
	@Override
	public ILongArray wrapLongArray(ByteBuffer buffer, int colSize) {
		return new TinyArray(buffer, Long.BYTES, colSize);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapLongArray(java.nio.ByteBuffer, int, int)
	 */
	@Override
	public ILongArray wrapLongArray(ByteBuffer buffer, int yCount, int zCount) {
		return new TinyArray(buffer, Long.BYTES, yCount, zCount);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapShortArray(java.nio.ByteBuffer)
	 */
	@Override
	public IShortArray wrapShortArray(ByteBuffer buffer) {
		return new TinyArray(buffer, Short.BYTES);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapShortArray(java.nio.ByteBuffer, int)
	 */
	@Override
	public IShortArray wrapShortArray(ByteBuffer buffer, int colSize) {
		return new TinyArray(buffer, Short.BYTES, colSize);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArrayFactory#wrapShortArray(java.nio.ByteBuffer, int, int)
	 */
	@Override
	public IShortArray wrapShortArray(ByteBuffer buffer, int yCount, int zCount) {
		return new TinyArray(buffer, Short.BYTES, yCount, zCount);
	}

}
