/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array.impl;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.IntSummaryStatistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * Shared functions.
 * 
 * @author Apside
 *
 */
public class ArrayUtils {

	/** Logger */
	protected static final Logger logger = LoggerFactory.getLogger(ArrayUtils.class);

	/**
	 * Invoke Cleaner.clean with the ByteBuffer as parameter.
	 * 
	 * See :
	 * https://github.com/apache/hadoop/blob/5d084d7eca32cfa647a78ff6ed3c378659f5b186/hadoop-common-project/hadoop-common/src/main/java/org/apache/hadoop/util/CleanerUtil.java
	 * alternative way : http://mail.openjdk.java.net/pipermail/core-libs-dev/2016-February/038669.html
	 */
	public static void closeDirectBuffer(ByteBuffer byteBuffer) {
		final MethodHandles.Lookup lookup = MethodHandles.lookup();
		try {
			// *** sun.misc.Unsafe unmapping (Java 9+) ***
			final Class<?> unsafeClass = Class.forName("sun.misc.Unsafe");
			// first check method
			MethodHandle unmapper = lookup.findVirtual(unsafeClass, "invokeCleaner",
					MethodType.methodType(void.class, ByteBuffer.class));

			// fetch the unsafe instance and bind it to the virtual MH:
			final Field f = unsafeClass.getDeclaredField("theUnsafe");
			f.setAccessible(true);
			final Object theUnsafe = f.get(null);
			MethodHandle cleaner = unmapper.bindTo(theUnsafe);

			// invoke the cleaner method
			cleaner.invokeExact(byteBuffer);

		} catch (Throwable e) {
			// try with Java8 method
			closeDirectBufferForJava8(byteBuffer);
		}

	}

	/**
	 * Invoke Cleaner.clean with the ByteBuffer as parameter.
	 * 
	 * ONLY FOR JAVA 8 : sun.misc.Cleaner has moved in OpenJDK 9 and sun.misc.Unsafe#invokeCleaner(ByteBuffer) is the
	 * replacement
	 */
	private static void closeDirectBufferForJava8(ByteBuffer cb) {
		// we could use this type cast and call functions without reflection code,
		// but static import from sun.* package is risky for non-SUN virtual machine.
		if (cb != null) {
			try {
				Method cleanerMethod = cb.getClass().getMethod("cleaner");
				cleanerMethod.setAccessible(true);
				Method cleanMethod = Class.forName("sun.misc.Cleaner").getMethod("clean");
				cleanMethod.setAccessible(true);
				Object cleaner = cleanerMethod.invoke(cb);
				if (cleaner != null) {
					cleanMethod.invoke(cleaner);
				}
			} catch (Exception e) {
				logger.debug("Cannot close the ByteBuffer " + e.getMessage());
			}
		}
	}

	/**
	 * Multiply all specified ints and return the result as a long.
	 */
	public static long multiply(int... values) {
		long result = 1;
		for (int value : values) {
			result *= value;
		}
		return result;
	}

	/**
	 * Return the minimal and maximal values found in the specified array.
	 * 
	 * @param array The array to parse.
	 * @return A 2-elements array containing min and max values.
	 */
	public int[] getMinMax(int[] array) {
		IntSummaryStatistics stats = Arrays.stream(array).summaryStatistics();
		return new int[] { stats.getMin(), stats.getMax() };
	}

	/**
	 * Return the minimal and maximal values found in the specified matrix.
	 * 
	 * @param array The matrix to parse.
	 * @return A 2-elements array containing min and max values.
	 */
	public int[] getMinMax(int[][] array) {
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		if (array != null) {
			for (int i = 0; i < array.length; i++) {
				// Attention, ce code ne fonctionnerait pas sur des doubles (cf.
				// NaN, Infinity ..etc)
				IntSummaryStatistics stat = Arrays.stream(array[i]).summaryStatistics();
				int statMin = stat.getMin();
				int statMax = stat.getMax();
				min = Math.min(min, statMin);
				max = Math.max(max, statMax);
			}
		}
		return new int[] { min, max };
	}

	/** Clean temporary folder. */
	public static void deleteTemporayFiles() {
		try {
			Files.walkFileTree(new File(TemporaryCache.getRootPath()).toPath(), new SimpleFileVisitor<Path>() {

				/**
				 * Follow the link.
				 * 
				 * @see java.nio.file.SimpleFileVisitor#visitFile(java.lang.Object,
				 *      java.nio.file.attribute.BasicFileAttributes)
				 */
				@Override
				public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
					File file = path.toFile();
					if (file.getName().startsWith(ArrayConstant.LARGE_ARRAY_FILE_PREFIX)
							|| file.getName().startsWith(ArrayConstant.HUGE_ARRAY_FILE_PREFIX)
							|| file.getName().startsWith(ArrayConstant.COPY_PREFIX)) {
						file.delete();
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
					if (exc instanceof AccessDeniedException) {
						return FileVisitResult.CONTINUE;
					}

					return super.visitFileFailed(file, exc);
				}
			});
		} catch (IOException e) {
			logger.warn("Unable to clean temporary folder " + TemporaryCache.getRootPath() + " : "
					+ e.getMessage());
		}
	}

}
