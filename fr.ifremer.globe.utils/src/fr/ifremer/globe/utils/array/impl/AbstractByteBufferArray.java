/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array.impl;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IBooleanArray;
import fr.ifremer.globe.utils.array.IByteArray;
import fr.ifremer.globe.utils.array.ICharArray;
import fr.ifremer.globe.utils.array.IDoubleArray;
import fr.ifremer.globe.utils.array.IFloatArray;
import fr.ifremer.globe.utils.array.IIntArray;
import fr.ifremer.globe.utils.array.ILongArray;
import fr.ifremer.globe.utils.array.IShortArray;

/**
 * Abstraction of {@link IArray} with a default implementation.<br>
 * This abstraction is a "Template method" design pattern.
 *
 * @author Apside
 *
 */
abstract class AbstractByteBufferArray implements ArrayConstant, IIntArray, ICharArray, ILongArray, IShortArray,
		IDoubleArray, IBooleanArray, IByteArray, IFloatArray {

	/** Le logger. */
	protected static Logger LOGGER = LoggerFactory.getLogger(AbstractByteBufferArray.class);

	/**
	 * Number of bit for shifting an index. <br>
	 * To find the real index of a primitive in the buffer, wa have to multiply the given index by the size of one
	 * primitive.<br>
	 * for performance purpose, it is better to shift the given index instead of multiplying.<br>
	 * It is possible because a primitive size is always a power of 2.
	 */
	protected int shift;

	/** Nb of primitives stored in this array. */
	private long elementCount;

	/**
	 * Number of values on the y axis.
	 */
	protected int yCount = 1;

	/**
	 * Number of values on the z axis.
	 */
	protected int zCount = 1;

	/**
	 * Opens and memory-maps the specified file, for read-only or read-write access, with a specified segment size.
	 *
	 * @param elementCount Number of element.
	 * @param sizeOfOneElement Size of one stored primitive.
	 */
	public AbstractByteBufferArray(long elementCount, int sizeOfOneElement) {
		shift = Integer.numberOfTrailingZeros(sizeOfOneElement);
		this.elementCount = elementCount;
	}

	/**
	 * Opens and memory-maps the specified file, for read-only or read-write access, for a two-dimensional array.
	 *
	 * @param elementCount Number of element.
	 * @param sizeOfOneElement Size of one stored primitive.
	 * @param colCount number of elements by row. Number of elements by column is deducted from file size.
	 */
	public AbstractByteBufferArray(long elementCount, int sizeOfOneElement, int colCount) {
		this(elementCount, sizeOfOneElement);
		yCount = colCount;
	}

	/**
	 * Opens and memory-maps the specified file, for read-only or read-write access, for a three-dimensional array.
	 *
	 * @param elementCount Number of element.
	 * @param sizeOfOneElement Size of one stored primitive.
	 * @param yCount number of elements int the y axis.
	 * @param zCount number of elements int the z axis.
	 */
	public AbstractByteBufferArray(long elementCount, int sizeOfOneElement, int yCount, int zCount) {
		this(elementCount, sizeOfOneElement, yCount);
		this.zCount = zCount;
	}

	/**
	 * ByteBuffer accessor.
	 */
	public abstract ByteBuffer getByteBuffer(int index);

	/**
	 * ByteBuffer accessor.
	 */
	public abstract ByteBuffer getByteBuffer(long index);

	/**
	 * ByteBuffer accessor.
	 */
	public abstract ByteBuffer getByteBuffer(int rowIndex, int colIndex);

	/**
	 * ByteBuffer accessor.
	 */
	public abstract ByteBuffer getByteBuffer(int x, int y, int z);

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IBooleanArray#getBoolean(int)
	 */
	@Override
	public boolean getBoolean(int index) {
		return TRUE == getByteBuffer(index).get(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IBooleanArray#getBoolean(long, long)
	 */
	@Override
	public boolean getBoolean(int rowIndex, int colIndex) {
		return TRUE == getByteBuffer(rowIndex, colIndex).get(computeIndexInBuffer(rowIndex, colIndex));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IBooleanArray#getBoolean(int, int, int)
	 */
	@Override
	public boolean getBoolean(int x, int y, int z) {
		return TRUE == getByteBuffer(x, y, z).get(computeIndexInBuffer(x, y, z));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IBooleanArray#getBoolean(long)
	 */
	@Override
	public boolean getBoolean(long index) {
		return TRUE == getByteBuffer(index).get(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IByteArray#getByte(int)
	 */
	@Override
	public byte getByte(int index) {
		return getByteBuffer(index).get(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IByteArray#getByte(int, int)
	 */
	@Override
	public byte getByte(int rowIndex, int colIndex) {
		return getByteBuffer(rowIndex, colIndex).get(computeIndexInBuffer(rowIndex, colIndex));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IByteArray#getByte(int, int, int)
	 */
	@Override
	public byte getByte(int x, int y, int z) {
		return getByteBuffer(x, y, z).get(computeIndexInBuffer(x, y, z));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IByteArray#getByte(long)
	 */
	@Override
	public byte getByte(long index) {
		return getByteBuffer(index).get(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ICharArray#getChar(int)
	 */
	@Override
	public char getChar(int index) {
		return getByteBuffer(index).getChar(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ICharArray#getChar(long, long)
	 */
	@Override
	public char getChar(int rowIndex, int colIndex) {
		return getByteBuffer(rowIndex, colIndex).getChar(computeIndexInBuffer(rowIndex, colIndex));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ICharArray#getChar(int, int, int)
	 */
	@Override
	public char getChar(int x, int y, int z) {
		return getByteBuffer(x, y, z).getChar(computeIndexInBuffer(x, y, z));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ICharArray#getChar(long)
	 */
	@Override
	public char getChar(long index) {
		return getByteBuffer(index).getChar(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#getDouble(int)
	 */
	@Override
	public double getDouble(int index) {
		return getByteBuffer(index).getDouble(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#getDouble(long, long)
	 */
	@Override
	public double getDouble(int rowIndex, int colIndex) {
		return getByteBuffer(rowIndex, colIndex).getDouble(computeIndexInBuffer(rowIndex, colIndex));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#getDouble(int, int, int)
	 */
	@Override
	public double getDouble(int x, int y, int z) {
		return getByteBuffer(x, y, z).getDouble(computeIndexInBuffer(x, y, z));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#getDouble(long)
	 */
	@Override
	public double getDouble(long index) {
		return getByteBuffer(index).getDouble(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#getDouble(long, double[])
	 */
	@Override
	public void getDouble(long index, double[] value) {
		for (int i = 0; i < value.length; i++) {
			value[i] = getByteBuffer(index).getDouble(computeIndexInBuffer(index));
			index++;
		}
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IFloatArray#getFloat(int)
	 */
	@Override
	public float getFloat(int index) {
		return getByteBuffer(index).getFloat(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IFloatArray#getFloat(long, long)
	 */
	@Override
	public float getFloat(int rowIndex, int colIndex) {
		return getByteBuffer(rowIndex, colIndex).getFloat(computeIndexInBuffer(rowIndex, colIndex));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IFloatArray#getFloat(int, int, int)
	 */
	@Override
	public float getFloat(int x, int y, int z) {
		return getByteBuffer(x, y, z).getFloat(computeIndexInBuffer(x, y, z));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IFloatArray#getFloat(long)
	 */
	@Override
	public float getFloat(long index) {
		return getByteBuffer(index).getFloat(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IIntArray#getInt(int)
	 */
	@Override
	public int getInt(int index) {
		return getByteBuffer(index).getInt(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IIntArray#getInt(long, long)
	 */
	@Override
	public int getInt(int rowIndex, int colIndex) {
		return getByteBuffer(rowIndex, colIndex).getInt(computeIndexInBuffer(rowIndex, colIndex));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IIntArray#getInt(int, int, int)
	 */
	@Override
	public int getInt(int x, int y, int z) {
		return getByteBuffer(x, y, z).getInt(computeIndexInBuffer(x, y, z));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IIntArray#getInt(long)
	 */
	@Override
	public int getInt(long index) {
		return getByteBuffer(index).getInt(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ILongArray#getLong(int)
	 */
	@Override
	public long getLong(int index) {
		return getByteBuffer(index).getLong(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ILongArray#getLong(long, long)
	 */
	@Override
	public long getLong(int rowIndex, int colIndex) {
		return getByteBuffer(rowIndex, colIndex).getLong(computeIndexInBuffer(rowIndex, colIndex));
	}

	@Override
	public long getLong(int x, int y, int z) {
		return getByteBuffer(x, y, z).getLong(computeIndexInBuffer(x, y, z));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ILongArray#getLong(long)
	 */
	@Override
	public long getLong(long index) {
		return getByteBuffer(index).getLong(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IShortArray#getShort(int)
	 */
	@Override
	public short getShort(int index) {
		return getByteBuffer(index).getShort(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IShortArray#getShort(long, long)
	 */
	@Override
	public short getShort(int rowIndex, int colIndex) {
		return getByteBuffer(rowIndex, colIndex).getShort(computeIndexInBuffer(rowIndex, colIndex));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IShortArray#getShort(int, int, int)
	 */
	@Override
	public short getShort(int x, int y, int z) {
		return getByteBuffer(x, y, z).getShort(computeIndexInBuffer(x, y, z));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IShortArray#getShort(long)
	 */
	@Override
	public short getShort(long index) {
		return getByteBuffer(index).getShort(computeIndexInBuffer(index));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IBooleanArray#putBoolean(int, boolean)
	 */
	@Override
	public void putBoolean(int index, boolean value) {
		getByteBuffer(index).put(computeIndexInBuffer(index), value ? TRUE : FALSE);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IBooleanArray#putBoolean(long, long, boolean)
	 */
	@Override
	public void putBoolean(int rowIndex, int colIndex, boolean value) {
		getByteBuffer(rowIndex, colIndex).put(computeIndexInBuffer(rowIndex, colIndex), value ? TRUE : FALSE);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IBooleanArray#putBoolean(int, int, int, boolean)
	 */
	@Override
	public void putBoolean(int x, int y, int z, boolean value) {
		getByteBuffer(x, y, z).put(computeIndexInBuffer(x, y, z), value ? TRUE : FALSE);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IBooleanArray#putBoolean(long, boolean)
	 */
	@Override
	public void putBoolean(long index, boolean value) {
		getByteBuffer(index).put(computeIndexInBuffer(index), value ? TRUE : FALSE);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IByteArray#putByte(int, byte)
	 */
	@Override
	public void putByte(int index, byte value) {
		getByteBuffer(index).put(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IByteArray#putByte(int, int, byte)
	 */
	@Override
	public void putByte(int rowIndex, int colIndex, byte value) {
		getByteBuffer(rowIndex, colIndex).put(computeIndexInBuffer(rowIndex, colIndex), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IByteArray#putByte(int, int, int, byte)
	 */
	@Override
	public void putByte(int x, int y, int z, byte value) {
		getByteBuffer(x, y, z).put(computeIndexInBuffer(x, y, z), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IByteArray#putByte(long, byte)
	 */
	@Override
	public void putByte(long index, byte value) {
		getByteBuffer(index).put(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ICharArray#putChar(int, char)
	 */
	@Override
	public void putChar(int index, char value) {
		getByteBuffer(index).putChar(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ICharArray#putChar(long, long, char)
	 */
	@Override
	public void putChar(int rowIndex, int colIndex, char value) {
		getByteBuffer(rowIndex, colIndex).putChar(computeIndexInBuffer(rowIndex, colIndex), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ICharArray#putChar(int, int, int, char)
	 */
	@Override
	public void putChar(int x, int y, int z, char value) {
		getByteBuffer(x, y, z).putChar(computeIndexInBuffer(x, y, z), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ICharArray#putChar(long, char)
	 */
	@Override
	public void putChar(long index, char value) {
		getByteBuffer(index).putChar(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#putDouble(int, double)
	 */
	@Override
	public void putDouble(int index, double value) {
		getByteBuffer(index).putDouble(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#putDouble(long, long, double)
	 */
	@Override
	public void putDouble(int rowIndex, int colIndex, double value) {
		getByteBuffer(rowIndex, colIndex).putDouble(computeIndexInBuffer(rowIndex, colIndex), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#putDouble(int, int, int, double)
	 */
	@Override
	public void putDouble(int x, int y, int z, double value) {
		getByteBuffer(x, y, z).putDouble(computeIndexInBuffer(x, y, z), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#putDouble(long, double)
	 */
	@Override
	public void putDouble(long index, double value) {
		try {
			getByteBuffer(index).putDouble(computeIndexInBuffer(index), value);
		} catch (Exception e) {
			System.out.println(" limites dépassées " + index);
		}
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#putDouble(long, double[])
	 */
	@Override
	public void putDouble(long index, double[] values) {
		for (double value : values) {
			getByteBuffer(index).putDouble(computeIndexInBuffer(index), value);
			index++;
		}
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IFloatArray#putFloat(int, float)
	 */
	@Override
	public void putFloat(int index, float value) {
		getByteBuffer(index).putFloat(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IFloatArray#putFloat(long, long, float)
	 */
	@Override
	public void putFloat(int rowIndex, int colIndex, float value) {
		getByteBuffer(rowIndex, colIndex).putFloat(computeIndexInBuffer(rowIndex, colIndex), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IFloatArray#putFloat(int, int, int, float)
	 */
	@Override
	public void putFloat(int x, int y, int z, float value) {
		getByteBuffer(x, y, z).putFloat(computeIndexInBuffer(x, y, z), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IFloatArray#putFloat(long, float)
	 */
	@Override
	public void putFloat(long index, float value) {
		getByteBuffer(index).putFloat(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IIntArray#putInt(int, int)
	 */
	@Override
	public void putInt(int index, int value) {
		getByteBuffer(index).putInt(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IIntArray#putInt(long, long, int)
	 */
	@Override
	public void putInt(int rowIndex, int colIndex, int value) {
		getByteBuffer(rowIndex, colIndex).putInt(computeIndexInBuffer(rowIndex, colIndex), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IIntArray#putInt(int, int, int, int)
	 */
	@Override
	public void putInt(int x, int y, int z, int value) {
		getByteBuffer(x, y, z).putInt(computeIndexInBuffer(x, y, z), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IIntArray#putInt(long, int)
	 */
	@Override
	public void putInt(long index, int value) {
		getByteBuffer(index).putInt(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ILongArray#putLong(long, long, value)
	 */
	@Override
	public void putLong(int rowIndex, int colIndex, long value) {
		getByteBuffer(rowIndex, colIndex).putLong(computeIndexInBuffer(rowIndex, colIndex), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ILongArray#putLong(int, int, int, long)
	 */
	@Override
	public void putLong(int x, int y, int z, long value) {
		getByteBuffer(x, y, z).putLong(computeIndexInBuffer(x, y, z), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ILongArray#putLong(int, long)
	 */
	@Override
	public void putLong(int index, long value) {
		getByteBuffer(index).putLong(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ILongArray#putLong(long, long)
	 */
	@Override
	public void putLong(long index, long value) {
		getByteBuffer(index).putLong(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IShortArray#putShort(long, long, short)
	 */
	@Override
	public void putShort(int rowIndex, int colIndex, short value) {
		getByteBuffer(rowIndex, colIndex).putShort(computeIndexInBuffer(rowIndex, colIndex), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IShortArray#putShort(int, int, int, short)
	 */
	@Override
	public void putShort(int x, int y, int z, short value) {
		getByteBuffer(x, y, z).putShort(computeIndexInBuffer(x, y, z), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IShortArray#putShort(int, short)
	 */
	@Override
	public void putShort(int index, short value) {
		getByteBuffer(index).putShort(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IShortArray#putShort(long, short)
	 */
	@Override
	public void putShort(long index, short value) {
		getByteBuffer(index).putShort(computeIndexInBuffer(index), value);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IArray#isWritable()
	 */
	@Override
	public boolean isWritable() {
		return !getByteBuffer(0).isReadOnly();
	}

	/**
	 * Compute the real index.
	 */
	int computeIndexInBuffer(int index) {
		return index << getShift();
	}

	/**
	 * Compute the real index.
	 */
	int computeIndexInBuffer(long index) {
		return (int) index << getShift();
	}

	/**
	 * Compute the real index.
	 */
	int computeIndexInBuffer(int rowIndex, int colIndex) {
		return rowIndex * getYCount() + colIndex << getShift();
	}

	/**
	 * Compute the real index.
	 */
	int computeIndexInBuffer(int x, int y, int z) {
		return x * getYCount() * getZCount() + y * getZCount() + z << getShift();
	}

	/**
	 * colCount accessor.
	 */
	public int getYCount() {
		return yCount;
	}

	/**
	 * shift accessor.
	 */
	public int getShift() {
		return shift;
	}

	/**
	 * @return the {@link #zCount}
	 */
	public int getZCount() {
		return zCount;
	}

	/**
	 * @return the {@link #elementCount}
	 */
	@Override
	public long getElementCount() {
		return elementCount;
	}

	/**
	 * @param elementCount the {@link #elementCount} to set
	 */
	public void setElementCount(long elementCount) {
		this.elementCount = elementCount;
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IFloatArray#asFloatBuffer(int, int)
	 */
	@Override
	public FloatBuffer asFloatBuffer(int firstIndex, int elementCount) {
		ByteBuffer buffer = getByteBuffer(firstIndex);
		FloatBuffer result = buffer.asFloatBuffer();
		result.position(firstIndex);
		result.limit(firstIndex + elementCount);
		result.mark();
		return result;
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IByteArray#asByteBuffer(int, int)
	 */
	@Override
	public ByteBuffer asByteBuffer(int firstIndex, int elementCount) {
		ByteBuffer buffer = getByteBuffer(firstIndex);
		ByteBuffer result = buffer.slice();
		result.position(firstIndex);
		result.limit(firstIndex + elementCount);
		result.mark();
		return result;
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IShortArray#asShortBuffer(int, int)
	 */
	@Override
	public ShortBuffer asShortBuffer(int firstIndex, int elementCount) {
		ByteBuffer buffer = getByteBuffer(firstIndex);
		ShortBuffer result = buffer.asShortBuffer();
		result.position(firstIndex);
		result.limit(firstIndex + elementCount);
		result.mark();
		return result;
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#asDoubleBuffer(int, int)
	 */
	@Override
	public DoubleBuffer asDoubleBuffer(int firstIndex, int elementCount) {
		ByteBuffer buffer = getByteBuffer(firstIndex);
		DoubleBuffer result = buffer.asDoubleBuffer();
		result.position(firstIndex);
		result.limit(firstIndex + elementCount);
		result.mark();
		return result;
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ILongArray#asLongBuffer(int, int)
	 */
	@Override
	public LongBuffer asLongBuffer(int firstIndex, int elementCount) {
		ByteBuffer buffer = getByteBuffer(firstIndex);
		LongBuffer result = buffer.asLongBuffer();
		result.position(firstIndex);
		result.limit(firstIndex + elementCount);
		result.mark();
		return result;
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ICharArray#asCharBuffer(int, int)
	 */
	@Override
	public CharBuffer asCharBuffer(int firstIndex, int elementCount) {
		ByteBuffer buffer = getByteBuffer(firstIndex);
		CharBuffer result = buffer.asCharBuffer();
		result.position(firstIndex);
		result.limit(firstIndex + elementCount);
		result.mark();
		return result;
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IIntArray#asIntBuffer(int, int)
	 */
	@Override
	public IntBuffer asIntBuffer(int firstIndex, int elementCount) {
		ByteBuffer buffer = getByteBuffer(firstIndex);
		IntBuffer result = buffer.asIntBuffer();
		result.position(firstIndex);
		result.limit(firstIndex + elementCount);
		result.mark();
		return result;
	}

	/**
	 * @return a clone of this Array as a IByteArray.
	 */
	protected abstract AbstractByteBufferArray cloneArray() throws IOException;

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IByteArray#cloneByteArray()
	 */
	@Override
	public IByteArray cloneByteArray() throws IOException {
		return cloneArray();
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IFloatArray#cloneFloatArray()
	 */
	@Override
	public IFloatArray cloneFloatArray() throws IOException {
		return cloneArray();
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IDoubleArray#cloneDoubleArray()
	 */
	@Override
	public IDoubleArray cloneDoubleArray() throws IOException {
		return cloneArray();
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IShortArray#cloneShortArray()
	 */
	@Override
	public IShortArray cloneShortArray() throws IOException {
		return cloneArray();
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ILongArray#cloneLongArray()
	 */
	@Override
	public ILongArray cloneLongArray() throws IOException {
		return cloneArray();
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.ICharArray#cloneCharArray()
	 */
	@Override
	public ICharArray cloneCharArray() throws IOException {
		return cloneArray();
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.utils.array.IIntArray#cloneIntArray()
	 */
	@Override
	public IIntArray cloneIntArray() throws IOException {
		return cloneArray();
	}

}
