/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array;

import java.io.IOException;
import java.nio.LongBuffer;

/**
 * Tableau de longs mappés sur un fichier.
 * 
 * @author Apside
 *
 */
public interface ILongArray extends IArray {

	/**
	 * Retrieves a long starting at the specified index.
	 */
	long getLong(long index);

	/**
	 * Stores a long starting at the specified index.
	 */
	void putLong(long index, long value);

	/**
	 * Retrieves a long starting at the specified index.
	 */
	long getLong(int index);

	/**
	 * Stores a long starting at the specified index.
	 */
	void putLong(int index, long value);

	/**
	 * Retrieves a long stored at the specified coords.
	 */
	long getLong(int rowIndex, int colIndex);

	/**
	 * Stores a long at the specified coords.
	 */
	void putLong(int rowIndex, int colIndex, long value);

	/**
	 * Retrieves a long stored at the specified coords.
	 */
	long getLong(int x, int y, int z);

	/**
	 * Stores a long at the specified coords.
	 */
	void putLong(int x, int y, int z, long value);

	/** Fill the array with the given value. */
	default void fill(long value) {
		for (long i = 0; i < getElementCount(); i++) {
			putLong(i, value);
		}
	}

	/**
	 * Creates a view of this array as a long buffer.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will be visible in the new array, and vice versa
	 * 
	 * @param firstIndex
	 *            First element in the view
	 * @param elementCount
	 *            Number of long in the resulting buffer
	 * @return A new float buffer
	 */
	LongBuffer asLongBuffer(int firstIndex, int elementCount);

	/**
	 * Create an new instance of ILongArray reflecting the content of this
	 * ILongArray.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will NOT be visible in the new array, and vice versa
	 * 
	 * @return
	 */
	ILongArray cloneLongArray() throws IOException;

}
