/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array;

import java.io.IOException;
import java.nio.CharBuffer;

/**
 * Array of char.
 * 
 * @author Apside
 *
 */
public interface ICharArray extends IArray {

	/**
	 * Retrieves a char starting at the specified index.
	 */
	char getChar(long index);

	/**
	 * Stores a char starting at the specified index.
	 */
	void putChar(long index, char value);

	/**
	 * Retrieves a char starting at the specified index.
	 */
	char getChar(int index);

	/**
	 * Stores a char starting at the specified index.
	 */
	void putChar(int index, char value);

	/**
	 * Retrieves a char stored at the specified coords.
	 */
	char getChar(int rowIndex, int colIndex);

	/**
	 * Stores a char at the specified coords.
	 */
	void putChar(int rowIndex, int colIndex, char value);

	/**
	 * Retrieves a char stored at the specified coords.
	 */
	char getChar(int x, int y, int z);

	/**
	 * Stores a char at the specified coords.
	 */
	void putChar(int x, int y, int z, char value);

	/** Fill the array with the given value. */
	default void fill(char value) {
		for (long i = 0; i < getElementCount(); i++) {
			putChar(i, value);
		}
	}

	/**
	 * Creates a view of this array as a char buffer.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will be visible in the new array, and vice versa
	 * 
	 * @param firstIndex
	 *            First element in the view
	 * @param elementCount
	 *            Number of short in the resulting buffer
	 * @return A new float buffer
	 */
	CharBuffer asCharBuffer(int firstIndex, int elementCount);

	/**
	 * Create an new instance of ICharArray reflecting the content of this
	 * ICharArray.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will NOT be visible in the new array, and vice versa
	 * 
	 * @return
	 */
	ICharArray cloneCharArray() throws IOException;

}
