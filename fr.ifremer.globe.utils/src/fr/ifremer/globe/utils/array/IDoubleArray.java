/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array;

import java.io.IOException;
import java.nio.DoubleBuffer;

/**
 * Tableau de doubles mappés sur un fichier.
 * 
 * @author Apside
 *
 */
public interface IDoubleArray extends IArray {

	/**
	 * Retrieves a double starting at the specified index.
	 */
	double getDouble(long index);

	/**
	 * Stores a double starting at the specified index.
	 */
	void putDouble(long index, double value);

	/**
	 * Retrieves some doubles starting at the specified index.
	 */
	void getDouble(long index, double[] value);

	/**
	 * Stores some doubles starting at the specified index.
	 */
	void putDouble(long index, double[] value);

	/**
	 * Retrieves a double starting at the specified index.
	 */
	double getDouble(int index);

	/**
	 * Stores a double starting at the specified index.
	 */
	void putDouble(int index, double value);

	/**
	 * Retrieves a double stored at the specified coords.
	 */
	double getDouble(int rowIndex, int colIndex);

	/**
	 * Stores a double at the specified coords.
	 */
	void putDouble(int rowIndex, int colIndex, double value);

	/**
	 * Retrieves a double stored at the specified coords.
	 */
	double getDouble(int x, int y, int z);

	/**
	 * Stores a double at the specified coords.
	 */
	void putDouble(int x, int y, int z, double value);

	/** Fill the array with the given value. */
	default void fill(double value) {
		for (long i = 0; i < getElementCount(); i++) {
			putDouble(i, value);
		}
	}

	/**
	 * Creates a view of this array as a double buffer.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will be visible in the new array, and vice versa
	 * 
	 * @param firstIndex
	 *            First element in the view
	 * @param elementCount
	 *            Number of double in the resulting buffer
	 * @return A new float buffer
	 */
	DoubleBuffer asDoubleBuffer(int firstIndex, int elementCount);

	/**
	 * Create an new instance of IDoubleArray reflecting the content of this
	 * IDoubleArray.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will NOT be visible in the new array, and vice versa
	 * 
	 * @return
	 */
	IDoubleArray cloneDoubleArray() throws IOException;

}
