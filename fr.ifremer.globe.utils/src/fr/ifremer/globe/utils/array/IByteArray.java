/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Array of bytes.
 * 
 * @author Apside
 *
 */
public interface IByteArray extends IArray {

	/**
	 * Retrieves a value starting at the specified index.
	 */
	byte getByte(long index);

	/**
	 * Stores a value starting at the specified index.
	 */
	void putByte(long index, byte value);

	/**
	 * Retrieves a value starting at the specified index.
	 */
	byte getByte(int index);

	/**
	 * Stores a value starting at the specified index.
	 */
	void putByte(int index, byte value);

	/**
	 * Retrieves a value stored at the specified coords.
	 */
	byte getByte(int rowIndex, int colIndex);

	/**
	 * Stores a value at the specified coords.
	 */
	void putByte(int rowIndex, int colIndex, byte value);

	/**
	 * Retrieves a value stored at the specified coords.
	 */
	byte getByte(int x, int y, int z);

	/**
	 * Stores a value at the specified coords.
	 */
	void putByte(int x, int y, int z, byte value);

	/** Fill the array with the given value. */
	default void fill(byte value) {
		for (long i = 0; i < getElementCount(); i++) {
			putByte(i, value);
		}
	}

	/**
	 * Creates a view of this array as a byte buffer.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will be visible in the new array, and vice versa
	 * 
	 * @param firstIndex
	 *            First element in the view
	 * @param elementCount
	 *            Number of short in the resulting buffer
	 * @return A new byte buffer
	 */
	ByteBuffer asByteBuffer(int firstIndex, int elementCount);

	/**
	 * Create an new instance of IByteArray reflecting the content of this
	 * IByteArray.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will NOT be visible in the new array, and vice versa
	 * 
	 * @return
	 */
	IByteArray cloneByteArray() throws IOException;

}
