/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array;

import java.io.IOException;
import java.nio.IntBuffer;

/**
 * Tableau d'entiers mappés sur un fichier.
 * 
 * @author Apside
 *
 */
public interface IIntArray extends IArray {

	/**
	 * Retrieves a four-byte integer starting at the specified index.
	 */
	int getInt(long index);

	/**
	 * Stores a four-byte integer starting at the specified index.
	 */
	void putInt(long index, int value);

	/**
	 * Retrieves a four-byte integer starting at the specified index.
	 */
	int getInt(int index);

	/**
	 * Stores a four-byte integer starting at the specified index.
	 */
	void putInt(int index, int value);

	/**
	 * Retrieves a four-byte integer stored at the specified coords.
	 */
	int getInt(int rowIndex, int colIndex);

	/**
	 * Stores a four-byte integer at the specified coords.
	 */
	void putInt(int rowIndex, int colIndex, int value);

	/**
	 * Retrieves a four-byte integer stored at the specified coords.
	 */
	int getInt(int x, int y, int z);

	/**
	 * Stores a four-byte integer at the specified coords.
	 */
	void putInt(int x, int y, int z, int value);

	/** Fill the array with the given value. */
	default void fill(int value) {
		for (long i = 0; i < getElementCount(); i++) {
			putInt(i, value);
		}
	}

	/**
	 * Creates a view of this array as a int buffer.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will be visible in the new array, and vice versa
	 * 
	 * @param firstIndex
	 *            First element in the view
	 * @param elementCount
	 *            Number of int in the resulting buffer
	 * @return A new float buffer
	 */

	IntBuffer asIntBuffer(int firstIndex, int elementCount);

	/**
	 * Create an new instance of IIntArray reflecting the content of this
	 * IIntArray.<br>
	 * The content of the new array will be that of this array. Changes to this
	 * array's content will NOT be visible in the new array, and vice versa
	 * 
	 * @return
	 */
	IIntArray cloneIntArray() throws IOException;

}
