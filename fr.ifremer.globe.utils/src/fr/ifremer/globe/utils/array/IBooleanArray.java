/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array;

/**
 * Array of booleans.
 * 
 * @author Apside
 *
 */
public interface IBooleanArray extends IArray {

	/**
	 * Retrieves a value starting at the specified index.
	 */
	boolean getBoolean(long index);

	/**
	 * Stores a value starting at the specified index.
	 */
	void putBoolean(long index, boolean value);

	/**
	 * Retrieves a value stored at the specified coords.
	 */
	boolean getBoolean(int rowIndex, int colIndex);

	/**
	 * Stores a value at the specified coords.
	 */
	void putBoolean(int rowIndex, int colIndex, boolean value);

	/**
	 * Retrieves a value starting at the specified index.
	 */
	boolean getBoolean(int index);

	/**
	 * Stores a value starting at the specified index.
	 */
	void putBoolean(int index, boolean value);

	/**
	 * Retrieves a value starting at the specified index.
	 */
	boolean getBoolean(int x, int y, int z);

	/**
	 * Stores a value starting at the specified index.
	 */
	void putBoolean(int x, int y, int z, boolean value);

	/** Fill the array with the given value. */
	default void fill(boolean value) {
		for (long i = 0; i < getElementCount(); i++) {
			putBoolean(i, value);
		}
	}

}
