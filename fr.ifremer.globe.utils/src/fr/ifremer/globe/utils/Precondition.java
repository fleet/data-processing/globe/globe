package fr.ifremer.globe.utils;

public class Precondition {
	public static void checkNotNull(Object o)
	{
		if(o==null)
			throw new NullPointerException();
	}
}
