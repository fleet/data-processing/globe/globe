package fr.ifremer.globe.utils;

/**
 * Result (return value) of an operation. An operation could have successed or failed and contains an error message
 */
public class Result {

	enum Status {
		SUCCESS, FAILURE
	}

	Status status;
	String errorMessage = "";

	private Result(Status status, String errorMessage) {
		super();
		this.errorMessage = errorMessage;
		this.status = status;
	}

	/**
	 * build a new {@link Result} with a status {@link Status#SUCCESS}
	 */
	public static Result success() {
		return new Result(Status.SUCCESS, "");
	}

	/**
	 * build a new {@link Result} with a status {@link Status#FAILURE}
	 */
	public static Result failed(String errorMessage) {
		return new Result(Status.FAILURE, errorMessage);
	}

	public boolean isSuccess() {
		return status == Status.SUCCESS;
	}

	public Status getStatus() {
		return status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
