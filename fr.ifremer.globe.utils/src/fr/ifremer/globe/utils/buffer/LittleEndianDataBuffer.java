package fr.ifremer.globe.utils.buffer;

/**
 * @author mgaro, &lt;mickael.garo@ifremer.fr&gt;
 * 
 */
public class LittleEndianDataBuffer extends BinaryDataBuffer {

	/**
	 * @param bufferByte
	 *            table of byte who contains data. this data are converted in
	 *            unsigned data.
	 */
	public LittleEndianDataBuffer(final byte[] bufferByte, int offset) {
		super(bufferByte, offset);
	}

	/**
	 * @return unsigned short
	 */
	@Override
	public final int readUShort() {
		final int b1 = getUByte();
		final int b2 = getUByte();

		return ((b1) + (b2 << 8)) & 0x0000FFFF;
	}

	/**
	 * @return signed short
	 */
	@Override
	public final int readSignedShort() {
		final int b1 = getUByte();
		final int b2 = getUByte();

		return (short) ((b1) + (b2 << 8));
	}

	/**
	 * @return unsigned integer
	 */
	@Override
	public final long readUInt() {
		final int b1 = getUByte();
		final int b2 = getUByte();
		final int b3 = getUByte();
		final int b4 = getUByte();

		return (b1 << 0 | b2 << 8 | b3 << 16 | b4 << 24) & 0xFFFFFFFFL;
	}

	/**
	 * @return signed integer
	 */
	@Override
	public final int readSignedInt() {
		final int b1 = getUByte();
		final int b2 = getUByte();
		final int b3 = getUByte();
		final int b4 = getUByte();

		return (b1 << 0 | b2 << 8 | b3 << 16 | b4 << 24);
	}

	/**
	 * @return signed long
	 */
	@Override
	public final long readSignedLong() {
		final int b1 = getUByte();
		final int b2 = getUByte();
		final int b3 = getUByte();
		final int b4 = getUByte();
		final int b5 = getUByte();
		final int b6 = getUByte();
		final int b7 = getUByte();
		final int b8 = getUByte();

		return ((((long) b8) << 56) | (((long) b7) << 48) | (((long) b6) << 40) | (((long) b5) << 32) | (((long) b4) << 24) | (((long) b3) << 16) | (((long) b2) << 8) | ((b1)));
	}

	/**
	 * @return unsigned long
	 */
	@Override
	public final long readULong() {
		final int b1 = getUByte();
		final int b2 = getUByte();
		final int b3 = getUByte();
		final int b4 = getUByte();
		final int b5 = getUByte();
		final int b6 = getUByte();
		final int b7 = getUByte();
		final int b8 = getUByte();

		return ((((long) b8 & 0xff) << 56) | (((long) b7 & 0xff) << 48) | (((long) b6 & 0xff) << 40) | (((long) b5 & 0xff) << 32) | (((long) b4 & 0xff) << 24) | (((long) b3 & 0xff) << 16)
				| (((long) b2 & 0xff) << 8) | (((long) b1 & 0xff)));

	}
}
