package fr.ifremer.globe.utils.buffer;

/**
 * @author mgaro, &lt;mickael.garo@ifremer.fr&gt;
 * 
 */
public abstract class BinaryDataBuffer {

	/**
	 * 
	 */
	private final int[] buffer;
	/**
	 * 
	 */
	private int index = 0;

	/**
	 * @param bufferByte
	 *            table of byte who contains data. this data are converted in
	 *            unsigned data.
	 */
	public BinaryDataBuffer(final byte[] bufferByte, int offset) {

		buffer = new int[bufferByte.length];

		for (int i = 0; i < bufferByte.length; i++) {
			buffer[i] = bufferByte[i] & 0xFF;
		}

		index = offset;
	}

	/**
	 * @return unsigned short
	 */
	public abstract int readUShort();

	/**
	 * @return signed short
	 */
	public abstract int readSignedShort();

	/**
	 * @return unsigned integer
	 */
	public abstract long readUInt();

	/**
	 * @return signed integer
	 */
	public abstract int readSignedInt();

	/**
	 * @return signed long
	 */
	public abstract long readSignedLong();

	/**
	 * @return unsigned long
	 */
	public abstract long readULong();

	/**
	 * @return unsigned byte
	 */
	public final int readUByte() {
		return getUByte();
	}

	/**
	 * @return signed byte
	 */
	public final int readByte() {
		return (byte) getUByte();
	}

	public final int[] readAllBytes(int lenght)
	{
		int[] result = new int[lenght];
		
		// Get all the byte to read in one shot
		System.arraycopy(this.buffer, index, result, 0, lenght);
		
		// increment index
		this.incrementIndex(lenght);
		
		return result;
	}
	
	/**
	 * @return unsigned byte
	 */
	public final char readChar() {
		return (char) getUByte();
	}

	/**
	 * @param bufferByte
	 *            table of integer
	 * @return number of byte read
	 */
	public final int read(final int[] bufferByte) {
		int byteRead = 0;
		for (int j = 0; j < bufferByte.length; j++) {
			bufferByte[j] = getUByte();
			byteRead++;
		}

		return byteRead;
	}

	/**
	 * @return float
	 */
	public final float readFloat() {
		return Float.intBitsToFloat((int) readUInt());
	}

	/**
	 * @return double
	 */
	public final double readDouble() {
		return Double.longBitsToDouble(readULong());
	}

	/**
	 * @return string
	 */
	public final String readString(int size) {
		String ret = null;

		char[] charTab = new char[size];
		for (int i = 0; i < charTab.length; i++) {
			charTab[i] = readChar();
		}

		ret = new String(charTab);
		return ret;
	}

	/**
	 * @return index
	 */
	public final int getIndex() {
		return index;
	}

	/**
	 * @param increment
	 *            increment
	 */
	public final void incrementIndex(final int increment) {
		index += increment;
	}

	/**
	 * @param pIndex
	 *            index
	 */
	public final void setIndex(final int pIndex) {
		this.index = pIndex;
	}

	/**
	 * @return the value in the buffer
	 */
	protected int getUByte() {
		return buffer[index++];
	}

	/**
	 * @return the size of the buffer
	 */
	public int getBufferSize() {
		return buffer.length;
	}

}
