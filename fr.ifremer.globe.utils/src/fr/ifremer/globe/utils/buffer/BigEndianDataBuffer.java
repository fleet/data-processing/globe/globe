package fr.ifremer.globe.utils.buffer;

/**
 * @author mgaro, &lt;mickael.garo@ifremer.fr&gt;
 * 
 */
public class BigEndianDataBuffer extends BinaryDataBuffer {

	/**
	 * @param bufferByte
	 *            table of byte who contains data. this data are converted in
	 *            unsigned data.
	 */
	public BigEndianDataBuffer(final byte[] bufferByte, int offset) {
		super(bufferByte, offset);
	}

	/**
	 * @return unsigned short
	 */
	@Override
	public final int readUShort() {
		final int b1 = getUByte();
		final int b2 = getUByte();

		return ((b2) + (b1 << 8)) & 0x0000FFFF;
	}

	/**
	 * @return signed short
	 */
	@Override
	public final int readSignedShort() {
		final int b1 = getUByte();
		final int b2 = getUByte();

		return (short) ((b2) + (b1 << 8));
	}

	/**
	 * @return unsigned integer
	 */
	@Override
	public final long readUInt() {
		final int b1 = getUByte();
		final int b2 = getUByte();
		final int b3 = getUByte();
		final int b4 = getUByte();

		return (b4 << 0 | b3 << 8 | b2 << 16 | b1 << 24) & 0xFFFFFFFFL;
	}

	/**
	 * @return signed integer
	 */
	@Override
	public final int readSignedInt() {
		final int b1 = getUByte();
		final int b2 = getUByte();
		final int b3 = getUByte();
		final int b4 = getUByte();

		return (b4 << 0 | b3 << 8 | b2 << 16 | b1 << 24);
	}

	/**
	 * @return signed long
	 */
	@Override
	public final long readSignedLong() {
		final int b1 = getUByte();
		final int b2 = getUByte();
		final int b3 = getUByte();
		final int b4 = getUByte();
		final int b5 = getUByte();
		final int b6 = getUByte();
		final int b7 = getUByte();
		final int b8 = getUByte();

		return (((b8)) | (((long) b7) << 8) | (((long) b6) << 16) | (((long) b5) << 24) | (((long) b4) << 32) | (((long) b3) << 40) | (((long) b2) << 48) | (((long) b1) << 56));
	}

	/**
	 * @return unsigned long
	 */
	@Override
	public final long readULong() {
		final int b1 = getUByte();
		final int b2 = getUByte();
		final int b3 = getUByte();
		final int b4 = getUByte();
		final int b5 = getUByte();
		final int b6 = getUByte();
		final int b7 = getUByte();
		final int b8 = getUByte();

		return ((((long) b8 & 0xff)) | (((long) b7 & 0xff) << 8) | (((long) b6 & 0xff) << 16) | (((long) b5 & 0xff) << 24) | (((long) b4 & 0xff) << 32) | (((long) b3 & 0xff) << 40)
				| (((long) b2 & 0xff) << 48) | (((long) b1 & 0xff) << 56));

	}

}
