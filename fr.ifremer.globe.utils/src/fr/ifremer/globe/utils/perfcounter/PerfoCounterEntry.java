package fr.ifremer.globe.utils.perfcounter;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PerfoCounterEntry extends Observable {
	private Logger logger = LoggerFactory.getLogger(PerfoCounterEntry.class);
	SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");

	static int MeasureCount = 1000;
	String key;

	public class Measure {
		private Date todaysDate;
		public long date;
		public double timeElapsed;
		private String detailedMessage;

		Measure(long date, double time, String detailedMessage) {
			todaysDate = new java.util.Date();
			update(date, time);
			this.detailedMessage = detailedMessage;
		}

		public void update(long date, double time) {
			this.date = date;
			this.timeElapsed = time;
		}

		public void print() {
			logger.debug(this.toString());
		}
		@Override
		public String toString()
		{
			todaysDate.setTime(date);
			String formattedDate = formatter.format(todaysDate);
			String duration = DurationFormatUtils.formatDurationHMS((long) (timeElapsed * 1000));
			if (detailedMessage != null) {
				return formattedDate + ": stop [" + key + ":" + detailedMessage + "] duration (HMS) : [" + duration + "] ";
			} else {
				return formattedDate + ": stop [" + key + "] duration (HMS) : [" + duration + "] ";
			}			
		}
		/**
		 * @return the timeElapsed
		 */
		public double getTimeElapsed() {
			return timeElapsed;
		}

		
	}

	LinkedList<Measure> measures;

	PerfoCounterEntry(String key) {
		this.key = key;
		measures = new LinkedList<PerfoCounterEntry.Measure>();
	}

	public Measure addMeasure(long date, double time,  String detailedMessage) {
		Measure ret = null;
		if (measures.size() > 1000 - 1) {
			// recycle the oldest measure
			ret = measures.removeFirst();
			ret.update(date, time);
			measures.addLast(ret);
		} else {
			ret = new Measure(date, time, detailedMessage);
			measures.addLast(ret);

		}
		this.setChanged();
		this.notifyObservers();
		return ret;
	}

	public List<Measure> getMeasures() {
		return Collections.unmodifiableList(measures);
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	public void clear() {
		measures.clear();
		this.setChanged();
		this.notifyObservers();
	}
}
