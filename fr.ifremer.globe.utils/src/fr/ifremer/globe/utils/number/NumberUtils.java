package fr.ifremer.globe.utils.number;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumberUtils {
	/**
	 * Utility class, hide constructor
	 */
	private NumberUtils() {

	}

	protected static Logger logger = LoggerFactory.getLogger(NumberUtils.class);

	private static final DecimalFormat DEPTH_FORMAT = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.UK));

	private static final DecimalFormat METER_FORMAT = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.UK));
	/** Formatter of angles. */
	private static final DecimalFormat ANGLE_FORMAT = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.UK));

	private static final DecimalFormat ARCSECOND_FORMAT = new DecimalFormat("#.#####",
			new DecimalFormatSymbols(Locale.UK));

	static {
		DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
		decimalFormatSymbols.setDecimalSeparator('\u00B0');
		ANGLE_FORMAT.setDecimalFormatSymbols(decimalFormatSymbols);
		ANGLE_FORMAT.setDecimalSeparatorAlwaysShown(true);
	}

	/**
	 * @param pDouble double
	 * @return the string
	 */
	public static String getStringDouble(double pDouble) {
		return DEPTH_FORMAT.format(pDouble);
	}

	/**
	 * @param pDouble double
	 * @return the string
	 */
	public static String getStringMetersDouble(double pDouble) {
		return METER_FORMAT.format(pDouble);
	}

	/**
	 * @return a formatted angle in String format
	 */
	public static String getStringAngle(double angle) {
		return ANGLE_FORMAT.format(angle);
	}

	/**
	 * @return a formatted arcsecond value in String format
	 */
	public static String getStringArcSecond(double angle) {
		return ARCSECOND_FORMAT.format(angle);
	}

	/**
	 * @param pDouble double
	 * @return the string
	 */
	public static double getDoubleFromString(String sDouble) {
		double result = Double.NaN;
		try {
			Number parse = DEPTH_FORMAT.parse(sDouble);
			if (parse != null) {
				result = parse.doubleValue();
			}
		} catch (ParseException e) {
			logger.warn("cannot parse string", e);
		}
		return result;
	}

	/**
	 * Format a double in a scientific notation with the best precision
	 */
	public static String scientificNotation(double value) {
		String result = String.format(Locale.US, "%.20E", value);
		return result.replaceFirst("0*E", "E");
	}

	/**
	 * Convert a boolean to a byte
	 *
	 * @param a the boolean to convert
	 */
	public static byte bool2byte(boolean a) {
		return (byte) (a ? 1 : 0);
	}

	/**
	 * Convert a byte to a boolean
	 *
	 * @param the byte to convert
	 */
	public static boolean byte2bool(byte a) {
		return a != 0 ? true : false;
	}

	/***
	 * return Min value between two numbers taking into account the NaN case for double
	 *
	 * @see Math.Min
	 */
	public static double Min(double a, double b) {
		if (Double.isNaN(a)) {
			return b;
		}
		if (Double.isNaN(b)) {
			return a;
		}
		return Math.min(a, b);
	}

	/***
	 * return Max value between two numbers taking into account the NaN case for double
	 *
	 * @see Math.Max
	 */
	public static double Max(double a, double b) {
		if (Double.isNaN(a)) {
			return b;
		}
		if (Double.isNaN(b)) {
			return a;
		}
		return Math.max(a, b);
	}

	/***
	 * return Min value between two numbers taking into account the NaN case for float
	 *
	 * @see Math.Min
	 */
	public static float Min(float a, float b) {
		if (Float.isNaN(a)) {
			return b;
		}
		if (Float.isNaN(b)) {
			return a;
		}
		return Math.min(a, b);
	}

	/***
	 * return Max value between two numbers taking into account the NaN case for float
	 *
	 * @see Math.Max
	 */
	public static float Max(float a, float b) {
		if (Float.isNaN(a)) {
			return b;
		}
		if (Float.isNaN(b)) {
			return a;
		}
		return Math.max(a, b);
	}

	/**
	 * Converts a size in bytes into a size in KB, MB, GB ...etc.
	 *
	 * @param bytes Number of bytes.
	 * @param si <code>true</code> for international system notation.
	 * @return The human-readable formatted string.
	 */
	public static String getHumanReadableByteCount(long bytes, boolean si) {
		int unit = si ? 1000 : 1024;
		if (bytes < unit) {
			return bytes + " B";
		}
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
		return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}

	/***
	 * return square value of a double.
	 */
	public static double square(double a) {
		return a * a;
	}

	/** Normalize one latitude between -90 to 90° */
	public static double normalizedDegreesLatitude(double degrees) {
		double lat = degrees % 180;
		return lat > 90 ? 180 - lat : lat < -90 ? -180 - lat : lat;
	}

	/** Normalize one longitude between -180 to 180° */
	public static double normalizedDegreesLongitude(double degrees) {
		double lon = degrees % 360;
		return lon > 180 ? lon - 360 : lon < -180 ? 360 + lon : lon;
	}

}