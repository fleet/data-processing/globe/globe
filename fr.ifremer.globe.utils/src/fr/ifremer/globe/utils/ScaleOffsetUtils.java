package fr.ifremer.globe.utils;

public class ScaleOffsetUtils {

	public static short computeShortFromReal(double value, double scaleFactor, double addOffset) {
		value = (value - addOffset) / scaleFactor;
		return (short)Math.round(value);
	}

	public static int computeIntFromReal(double value, int scaleFactor, int addOffset) {
		value = (value - addOffset) / scaleFactor;
		return (int)Math.round(value);
	}

	public static int computeIntFromReal(double value, double scaleFactor, double addOffset) {
		value = (value - addOffset) / scaleFactor;
		return (int)Math.round(value);
	}

	public static int computeLongFromReal(double value, double scaleFactor, double addOffset) {
		value = (value - addOffset) / scaleFactor;
		return (int)Math.round(value);
	}

	public static int computeReal(int value, int scaleFactor, int addOffset) {
		return value * scaleFactor + addOffset;
	}

	public static double computeReal(int value, double scaleFactor, double addOffset) {
		return value * scaleFactor + addOffset;
	}
}
