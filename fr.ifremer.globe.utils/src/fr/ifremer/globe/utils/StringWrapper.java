package fr.ifremer.globe.utils;

/**
 * A super lightweight string wrapper
 * usefull to pass string parameter and allow to modify them
 * */
public class StringWrapper {
	String string;

	/**
	 * @return the string
	 */
	public String get() {
		return string;
	}

	/**
	 * @param string the string to set
	 */
	public void set(String string) {
		this.string = string;
	}

	public StringWrapper(String string) {
		super();
		this.string = string;
	}
	
}
