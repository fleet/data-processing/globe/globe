/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.bean;

import java.awt.Color;

import org.apache.commons.beanutils.converters.AbstractConverter;

/**
 * Converter of String to any Enum
 */
public class AwtColorTypeConverter extends AbstractConverter {

	/** {@inheritDoc} */
	@Override
	protected <T> T convertToType(Class<T> type, Object value) throws Throwable {
		if (Color.class.equals(type)) {
			return type.cast(Color.decode(value.toString()));
		}
		throw conversionException(type, value);
	}

	/** {@inheritDoc} */
	@Override
	protected Class<?> getDefaultType() {
		return Color.class;
	}

	/** {@inheritDoc} */
	@Override
	protected String convertToString(Object value) throws Throwable {
		return String.valueOf(((Color) value).getRGB());
	}

}