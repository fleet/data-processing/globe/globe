/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.bean;

import java.awt.Color;

import org.apache.commons.beanutils.converters.AbstractConverter;
import org.apache.commons.beanutils.converters.StringConverter;

/**
 * Redefine {@link StringConverter} to add specific conversion of some types (Color...) to String
 */
public class StringTypeConverter extends AbstractConverter {

	/** {@inheritDoc} */
	@Override
	protected <T> T convertToType(final Class<T> type, final Object value) throws Throwable {
		// This the code of org.apache.commons.beanutils.converters.StringConverter
		if (String.class.equals(type) || Object.class.equals(type)) {
			return type.cast(value.toString());
		}
		throw conversionException(type, value);
	}

	/** {@inheritDoc} */
	@Override
	protected Class<?> getDefaultType() {
		return String.class;
	}

	/** {@inheritDoc} */
	@Override
	protected String convertToString(Object value) throws Throwable {
		if (value instanceof Color) {
			return String.valueOf(((Color) value).getRGB());
		}
		return super.convertToString(value);
	}

}