/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.bean;

import org.apache.commons.beanutils.converters.AbstractConverter;

/**
 * Converter of String to any Enum
 */
public class EnumTypeConverter extends AbstractConverter {

	/** One Enum value */
	protected Enum<?> enumValue;

	/**
	 * Constructor
	 */
	public EnumTypeConverter(Enum<?> enumValue) {
		this.enumValue = enumValue;
	}

	/** {@inheritDoc} */
	@Override
	protected <T> T convertToType(Class<T> type, Object value) throws Throwable {
		if (enumValue.getDeclaringClass().equals(type)) {
			return type.cast(Enum.valueOf(enumValue.getDeclaringClass(), value.toString()));
		}
		throw conversionException(type, value);
	}

	/** {@inheritDoc} */
	@Override
	protected Class<?> getDefaultType() {
		return enumValue.getClass();
	}

}