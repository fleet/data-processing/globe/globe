package fr.ifremer.globe.utils.units;


import ucar.units.ConversionException;
import ucar.units.DerivableUnit;
import ucar.units.DerivedUnit;
import ucar.units.DivideException;
import ucar.units.MultiplyException;
import ucar.units.OperationException;
import ucar.units.RaiseException;
import ucar.units.Unit;
import ucar.units.UnitImpl;
import ucar.units.UnitName;

/**
 * Provides support for a unit that is a power of two of a reference
 * unit.
 * 
 * Instances of this class are immutable.
 * 
 */
public class SquaredUnit extends UnitImpl implements DerivableUnit {

	private static final long serialVersionUID = 1L;
	/**
	 * The reference unit.
	 * 
	 * @serial
	 */
	private final Unit        _unit;

	/**
	 * Constructs from a reference unit.
	 * 
	 * @param unit
	 *            The reference unit.
	 */
	public SquaredUnit(final Unit unit) {
		this(unit, null);
	}

	/**
	 * Constructs from a multiplicative factor, a reference unit, and an
	 * identifier.
	 * 
	 * @param scale
	 *            The multiplicative factor.
	 * @param unit
	 *            The reference unit.
	 * @param id
	 *            The identifier for the unit.
	 */
	public SquaredUnit(final Unit unit, final UnitName id) {
		super(id);
		if (!(unit instanceof SquaredUnit)) {
			_unit = unit;
		}
		else {
			_unit = ((SquaredUnit) unit)._unit;
		}
	}
	static Unit getInstance( final Unit unit)
			throws MultiplyException {

		return new SquaredUnit( unit,null);
	}
	/**
	 * Returns the reference unit.
	 * 
	 * @return The reference unit.
	 */
	public Unit getUnit() {
		return _unit;
	}


	@Override
	public Unit clone(UnitName id) {
		return new SquaredUnit(getUnit(), id);
	}

	@Override
	public String getCanonicalString() {
		return "(re " + _unit.toString()+")^2";
	}
	/**
     * Returns the string representation of this unit.
     * 
     * @return The string representation of this unit.
     */
    @Override
    public String toString() {
    	  final String string = super.toString(); // get symbol or name
          return string != null
                  ? string
                  : getCanonicalString();
      }

	/**
	 * Converts a numeric value from this unit to the underlying derived unit.
	 * 
	 * @param amount
	 *            The numeric value in this unit.
	 * @return The equivalent value in the underlying derived unit.
	 * @throws ConversionException
	 *             Can't convert value to the underlying derived unit.
	 */
	@Override
	public float toDerivedUnit(float amount) throws ConversionException {
		if (!(_unit instanceof DerivableUnit)) {
			throw new ConversionException(this, getDerivedUnit());
		}
		return (float) ((DerivableUnit) _unit).toDerivedUnit(Math.sqrt(amount));
	}

	/**
	 * Converts a numeric value from this unit to the underlying derived unit.
	 * 
	 * @param amount
	 *            The numeric value in this unit.
	 * @return The equivalent value in the underlying derived unit.
	 * @throws ConversionException
	 *             Can't convert value to the underlying derived unit.
	 */
	public double toDerivedUnit(final double amount) throws ConversionException {
		if (!(_unit instanceof DerivableUnit)) {
			throw new ConversionException(this, getDerivedUnit());
		}
		return ((DerivableUnit) _unit).toDerivedUnit(Math.sqrt(amount));
	}
	/**
	 * Converts numeric values from this unit to the underlying derived unit.
	 * 
	 * @param input
	 *            The numeric values in this unit.
	 * @param output
	 *            The equivalent values in the underlying derived unit.
	 * @return <code>output</code>.
	 * @throws ConversionException
	 *             Can't convert values to the underlying derived unit.
	 */
	@Override
	public float[] toDerivedUnit(float[] input, float[] output) throws ConversionException {
		for (int i = input.length; --i >= 0;) {
			output[i] = (float) Math.sqrt(input[i]);
		}
		if (!(_unit instanceof DerivableUnit)) {
			throw new ConversionException(this, getDerivedUnit());
		}
		return ((DerivableUnit) getUnit()).toDerivedUnit(output, output);	
	}

	@Override
	public double[] toDerivedUnit(double[] input, double[] output) throws ConversionException {
		for (int i = input.length; --i >= 0;) {
			output[i] = Math.sqrt(input[i]);
		}
		if (!(_unit instanceof DerivableUnit)) {
			throw new ConversionException(this, getDerivedUnit());
		}
		return ((DerivableUnit) getUnit()).toDerivedUnit(output, output);	
	}

	@Override
	public float fromDerivedUnit(float amount) throws ConversionException {
		return (float) fromDerivedUnit((double) amount);
	}

	@Override
	public double fromDerivedUnit(double amount) throws ConversionException {
		if (!(_unit instanceof DerivableUnit)) {
			throw new ConversionException(getDerivedUnit(), this);
		}
		double v=((DerivableUnit) getUnit()).fromDerivedUnit(amount);
		return v*v;
	}

	@Override
	public float[] fromDerivedUnit(float[] input, float[] output) throws ConversionException {
		if (!(_unit instanceof DerivableUnit)) {
			throw new ConversionException(getDerivedUnit(), this);
		}
		((DerivableUnit) getUnit()).fromDerivedUnit(input, output);
		for (int i = input.length; --i >= 0;) {
			output[i] =output[i]*output[i];
		}
		return output;
	}

	@Override
	public double[] fromDerivedUnit(double[] input, double[] output) throws ConversionException {
		if (!(_unit instanceof DerivableUnit)) {
			throw new ConversionException(getDerivedUnit(), this);
		}
		((DerivableUnit) getUnit()).fromDerivedUnit(input, output);
		for (int i = input.length; --i >= 0;) {
			output[i] = output[i]*output[i];
		}
		return output;
	}
	/**
	 * Multiplies this unit by another unit.
	 * 
	 * @param that
	 *            The other unit.
	 * @return The product of this unit and the other unit.
	 * @throws MultiplyException
	 *             Can't multiply these units together.
	 */
	@Override
	protected Unit myMultiplyBy(Unit that) throws MultiplyException {
		return new SquaredUnit(getUnit().multiplyBy(that));
	}

	@Override
	protected Unit myDivideBy(Unit that) throws OperationException {
		return new SquaredUnit(getUnit().divideBy(that));		
	}

	@Override
	protected Unit myDivideInto(Unit unit) throws OperationException {
		throw new DivideException(unit);
	}

	@Override
	protected Unit myRaiseTo(int power) throws RaiseException {
		return new SquaredUnit(getUnit().raiseTo(power));
	}

	  /**
     * Indicates if this unit is semantically identical to an object.
     * 
     * @param object
     *            The object.
     * @return <code>true</code> if an only if this unit is semantically
     *         identical to <code>object
     *				</code>.
     */
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }

        if (!(object instanceof SquaredUnit)) {
            return false;
        }
        SquaredUnit that = (SquaredUnit) object;
        if (!_unit.equals(that._unit)) return false;
        return true;
    }

  /**
     * Returns the hash code of this instance.
     * 
     * @return The hash code of this instance.
     */
    @Override
    public int hashCode() {
        return (Double.valueOf(2).hashCode()) ^ getUnit().hashCode();
    }

	/**
	 * Indicates if this unit is dimensionless. A ScaledUnit is dimensionless if
	 * and only if the reference unit is dimensionless.
	 * 
	 * @return <code>true</code> if and only if this unit is dimensionless.
	 */
	@Override
	public boolean isDimensionless() {
		return getUnit().isDimensionless();
	}
	/**
	 * Gets the derived unit underlying this unit.
	 * 
	 * @return The derived unit which underlies this unit.
	 */
	@Override
	public DerivedUnit getDerivedUnit() {
		return getUnit().getDerivedUnit();
	}
}
