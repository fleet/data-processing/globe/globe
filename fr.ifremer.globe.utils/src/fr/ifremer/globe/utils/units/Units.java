package fr.ifremer.globe.utils.units;

public class Units {

	public static final String DEGREES = "degrees";
	public static final String GAMMA = "gamma";
	public static final String METERS = "m";
	public static final String MILLIGAL = "mgal";

	public static final Double FROM_DEGREES_ARCSECONDS = 3600.0;

	/**
	 * convert arcsec to degrees
	 */
	public static double arcSec2Deg(double resolution) {
		return resolution / FROM_DEGREES_ARCSECONDS;
	}

	/**
	 * convert degrees to arcsec
	 */
	public static double deg2arcSec(double resolution) {
		return resolution * FROM_DEGREES_ARCSECONDS;
	}

	/**
	 * Convert degrees to a {@link String} representation as degrees and decimal minutes (like the Angle class of Nasa
	 * package, but with 3 decimals!)
	 */
	public static String degreestoDMString(double degrees) {
		int sign = (int) Math.signum(degrees);
		degrees *= sign;
		int d = (int) Math.floor(degrees);
		degrees = (degrees - d) * 60d;
		int m = (int) Math.floor(degrees);
		degrees = (degrees - m) * 60d;
		int s = (int) Math.round(degrees);

		if (s == 60) {
			m++;
			s = 0;
		} // Fix rounding errors
		if (m == 60) {
			d++;
			m = 0;
		}

		double mf = s == 0 ? m : m + s / 60.0;

		return (sign == -1 ? "-" : "") + d + '\u00B0' + ' ' + String.format("%5.3f", mf) + '\u2019';
	}

	/**
	 * Converts m/s to Knots
	 */
	public static float metterSecondsToKnots(float msValue) {
		return msValue * 3.6f / 1.852f;
	}

	/**
	 * Converts m/s to Knots
	 */
	public static double metterSecondsToKnots(double msValue) {
		return msValue * 3.6d / 1.852d;
	}
}
