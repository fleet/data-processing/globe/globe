package fr.ifremer.globe.utils.units;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import ucar.units.BaseQuantity;
import ucar.units.BaseUnit;
import ucar.units.LogarithmicUnit;
import ucar.units.NameException;
import ucar.units.Prefix;
import ucar.units.PrefixDBException;
import ucar.units.ScaledUnit;
import ucar.units.StandardPrefixDB;
import ucar.units.StandardUnitDB;
import ucar.units.Unit;
import ucar.units.UnitDBException;
import ucar.units.UnitName;
import ucar.units.UnitSystemException;


/**
 * provide support for declaring special (acoustics) units, and adding them to the default database of standart unit see {@link StandardUnitDB}
 * singleton of this class should be initialized before any use of the database
 * */
public class GlobeUnitDB {
	
	private GlobeUnitDB() throws NameException, UnitSystemException, UnitDBException
	{

		acousticAmplitudeUnit = BaseUnit.getOrCreate(UnitName.newUnitName(amplitudeUnitName, null, amplitudeUnitSymbol),
				BaseQuantity.UNKNOWN);
		acousticEnergy = new SquaredUnit(acousticAmplitudeUnit, UnitName.newUnitName(energyUnitName,null, energyUnitSymbol));
		acousticDB = new ScaledUnit(1/10., new LogarithmicUnit(acousticEnergy, 10),UnitName.newUnitName(acousticDecibelUnitName,null, acousticDecibelUnitSymbol));

		StandardUnitDB.instance().addUnit(acousticAmplitudeUnit);
		StandardUnitDB.instance().addUnit(acousticEnergy);
		StandardUnitDB.instance().addUnit(acousticDB);

	}

	public static final String energyUnitName="Acoustic energy";
	public static final String energyUnitSymbol="Enr";
	public static final String amplitudeUnitName="Acoustic amplitude";
	public static final String amplitudeUnitSymbol="Amp";
	public static final String acousticDecibelUnitName="Acoustic decibel";
	public static final String acousticDecibelUnitSymbol="dB";
	
	
	/**
	 * The singleton instance of this class.
	 */
	private static GlobeUnitDB instance=null;

	/**default unit for acoustic amplitude*/
	private final BaseUnit acousticAmplitudeUnit;
	/**default unit for acoustic energy*/
	private final Unit acousticEnergy;
	/**default unit for acoustics db */
	private final Unit acousticDB;

	/**
	 * Gets an instance of this database.
	 * 
	 * @return An instance of this database.
	 * @throws UnitDBException
	 *             The instance couldn't be created.
	 */
	public static GlobeUnitDB instance() throws UnitDBException {
		synchronized (GlobeUnitDB.class) {
			if (instance == null) {
				try {
					instance = new GlobeUnitDB();
				}
				catch (final Exception e) {
					throw new UnitDBException(
							"Couldn't create standard unit-database", e);
				}
			}
		}
		return instance;
	}
	/**
	 * obtain a list of avalaible unit names and their associated symbol
	 * @throws UnitDBException 
	 * */
	List<Pair<String,String>> getStdUnits() throws UnitDBException
	{
		ArrayList<Pair<String,String>> ret=new ArrayList<>();
		@SuppressWarnings("unchecked")
		Iterator<Unit> it = (Iterator<Unit>) StandardUnitDB.instance().getIterator();
		while (it.hasNext()) {
			Unit unit = it.next();
			if (unit.getSymbol() != null)
			{
				System.err.println("unit " + unit.getName() + " - symbol (" + unit.getSymbol() + ")");
				ret.add(new ImmutablePair<String, String>(unit.getName(),unit.getSymbol()));
			}
		}
		return ret;
	}
	
	/**
	 * return a list of available prefixes, for display usage only
	 * */
	List<Pair<String,String>> getStdPrefixes() throws PrefixDBException
	{
		ArrayList<Pair<String,String>> ret=new ArrayList<>();
		@SuppressWarnings("unchecked")
		Iterator<Prefix> it = (Iterator<Prefix>) StandardPrefixDB.instance().iterator();
		while (it.hasNext()) {
			Prefix preix = it.next();
			if (preix != null)
			{
				ret.add(new ImmutablePair<String, String>(preix.getID(),Double.toString(preix.getValue())));

			}
		}
		return ret;
		
	}
	
	/** return unit for acoustic amplitude*/
	public BaseUnit getAcousticAmplitudeUnit() {
		return acousticAmplitudeUnit;
	}

	/** return unit for acoustic energy*/
	public Unit getAcousticEnergy() {
		return acousticEnergy;
	}

	/** return unit for acoustic db*/
	public Unit getAcousticDB() {
		return acousticDB;
	}

}
