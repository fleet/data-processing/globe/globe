package fr.ifremer.globe.utils;
/**
 * Utilities for text file parser
 * */
public class TextFileParser {

	/**
	 * parse a text line and remove from it comments contained in this line
	 * <p>comments are identify by standart separator line # or // or \
	 * @return the line with no comments, might be empty
	 * */
	public static String removeComments(String line)
	{
		if(line==null || line.isEmpty()) return "";
		String removeComments[] = line.split("[#/\\\\]");
		if (removeComments.length > 0)
			return removeComments[0];
		else
			return "";
	}
}
