/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.csv;

import java.io.IOException;
import java.util.Optional;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;

/**
 * CSV utilities
 */
public class CsvUtils {

	/**
	 * Constructor
	 */
	private CsvUtils() {
	}

	/**
	 * @return the most encountered character (but digit or letter) in the specified String
	 */
	public static Optional<Character> guessDelimiter(String csvLine) {
		int[] occurrences = new int[127];
		for (int i = 0; i < csvLine.length(); i++) {
			char aChar = csvLine.charAt(i);
			if (!Character.isLetterOrDigit(aChar) && csvLine.charAt(i) < occurrences.length) {
				occurrences[aChar] = occurrences[aChar] + 1;
			}
		}

		// Ignore some characters that can not be a separator
		occurrences['"'] = 0;
		occurrences['.'] = 0;
		occurrences['-'] = 0;
		occurrences['+'] = 0;
		occurrences['/'] = 0;
		int nbSpace = occurrences[' '];
		occurrences[' '] = 0;

		// the most encountered character
		char result = 0;
		for (int i = 0; i < occurrences.length; i++) {
			if (occurrences[i] > occurrences[result]) {
				result = (char) i;
			}
		}

		// Ok, may be ' ' if no other caracter occured
		result = result == 0 && nbSpace > 0 ? ' ' : result;

		return Optional.ofNullable(result != 0 ? result : null);
	}

	/**
	 * Parses an incoming String and returns an array of elements.
	 *
	 * @return all elements or an empty array if the String is not parsable
	 */
	public static String[] parseLine(String csvLine, char delimiter) {
		try {
			CSVParser parser = new CSVParserBuilder().withSeparator(delimiter).withIgnoreQuotations(false).build();
			return parser.parseLine(csvLine);
		} catch (IOException e) {
			// Parse error
		}

		return new String[0];
	}

	/**
	 * @return the index of the latitude column. -1 if None
	 */
	public static int guessLatitudeIndex(String[] csvColumns) {
		for (int i = 0; i < csvColumns.length; i++) {
			String column = csvColumns[i].strip().toLowerCase();
			if ("latitude".equals(column) || "lat".equals(column) || "y".equals(column)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * @return the index of the longitude column. -1 if None
	 */
	public static int guessLongitudeIndex(String[] csvColumns) {
		for (int i = 0; i < csvColumns.length; i++) {
			String column = csvColumns[i].strip().toLowerCase();
			if ("longitude".equals(column) || "lon".equals(column) || "x".equals(column)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * @return the index of the elevation column. -1 if None
	 */
	public static int guessElevationIndex(String[] csvColumns) {
		for (int i = 0; i < csvColumns.length; i++) {
			String column = csvColumns[i].strip().toLowerCase();
			if ("elevation".equals(column) || "elev".equals(column) || "depth".equals(column)
					|| "altitude".equals(column) || "immersion".equals(column)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * @return the index of the elevation column. -1 if None
	 */
	public static int guessColumnIndex(String[] csvColumns, String searchColumn) {
		for (int i = 0; i < csvColumns.length; i++) {
			String column = csvColumns[i].strip();
			if (searchColumn.equalsIgnoreCase(column)) {
				return i;
			}
		}
		return -1;
	}

}
