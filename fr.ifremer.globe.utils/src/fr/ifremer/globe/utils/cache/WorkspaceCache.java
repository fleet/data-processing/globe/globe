package fr.ifremer.globe.utils.cache;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.service.datalocation.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Manage files in ".cache" folder of globe workspace.
 */
public class WorkspaceCache {

	private static WorkspaceCache instance = null;

	private static final Logger logger = LoggerFactory.getLogger(WorkspaceCache.class);

	public static String CACHE_DIRECTORY = ".cache" + File.separator;
	/** List of {@link CacheEntity} objects which contains layers and parameters used by Globe. */
	private static List<CacheEntity> cacheEntities;

	private WorkspaceCache() {
		cacheEntities = new ArrayList<CacheEntity>();
	}

	/**
	 * Get WorkspaceCache singleton.
	 * 
	 * @returns WorkspaceCache singleton
	 */
	private static synchronized WorkspaceCache getInstance() {
		if (instance == null) {
			instance = new WorkspaceCache();
		}
		return instance;
	}

	/**
	 * Object where three types of files are gathered. They constitute one entity of a workspace cache with a solid
	 * association guarantee by CacheEntity.
	 */
	public class CacheEntity {

		File guidDirectory;
		List<File> files;
		File parametersFile;

		public CacheEntity(File guidDirectory, List<File> files, File parametersFile) {
			this.guidDirectory = guidDirectory;
			this.files = files;
			this.parametersFile = parametersFile;
		}

		/** @return the guidDirectory */
		public File getGuidDirectory() {
			return this.guidDirectory;
		}

		/** @param guidDirectory the guidDirectory to set */
		public void setGuidDirectory(File guidDirectory) {
			this.guidDirectory = guidDirectory;
		}

		/** @return the files */
		public List<File> getFiles() {
			return this.files;
		}

		/** @param files the files to set */
		public void setFiles(List<File> files) {
			this.files = files;
		}

		/** @return the parametersFile */
		public File getParametersFile() {
			return this.parametersFile;
		}
	}

	/**
	 * Create on workspace cache and save it in {@link WorkspaceCache:CacheEntity}
	 * 
	 * @param guidDirectory : directory where workspaceCache is saved.
	 * @param parameters : XML serialized bytes of a DtmParameters object (for parameters file).
	 * @param fileNames : files content of a IInfoStore object.
	 * @throws GIOException
	 */
	public static void create(File guidDirectory, byte[] parameters, List<File> fileNames) throws GIOException {

		List<CacheEntity> cacheEntities = WorkspaceCache.getCacheEntities();

		if (cacheEntities != null) {
			for (CacheEntity cacheEntity : cacheEntities) {
				if (guidDirectory.getPath().compareTo(cacheEntity.getGuidDirectory().getPath()) == 0
						&& guidDirectory.exists()) {
					if (!FileUtils.deleteDir(guidDirectory)) {
						throw new GIOException(
								"You try to create a workspace cache on another one which can't be deleted ("
										+ guidDirectory + ")");
					}
				}
			}
		}

		guidDirectory.mkdirs();
		// Write parameters file
		File parametersFile = WorkspaceCache.writeParametersFile(guidDirectory, parameters);
		// Write file list file
		for (File fileName : fileNames) {
			new File(guidDirectory + File.separator + "files", fileName.getName());
		}
		// Save this association in cacheEntities
		cacheEntities.add(WorkspaceCache.getInstance().new CacheEntity(guidDirectory, fileNames, parametersFile));
	}

	/**
	 * Write a parameters file.
	 * 
	 * @param guidDirectory : directory where workspaceCache is saved.
	 * @param parameters : XML serialized bytes of a DtmParameters object (for parameters file).
	 */
	private static File writeParametersFile(File guidDirectory, byte[] parameters) {

		File parametersFile = new File(guidDirectory, "parameters.xml");
		FileOutputStream fos = null;

		try {
			fos = new FileOutputStream(parametersFile);
			fos.write(parameters);
		} catch (FileNotFoundException e) {
			logger.warn("cannot write parameters file");
		} catch (IOException e) {
			logger.warn("cannot write parameters file");
		} finally {
			IOUtils.closeQuietly(fos);
		}

		return parametersFile;
	}

	/**
	 * Check if some workspace cache are unused by Globe.
	 * 
	 * @returns a list of unused files (which may be empty)
	 */
	public static List<File> askForUnusedCacheEntity() {

		List<File> unusedCacheFiles = new ArrayList<File>();
		if (getCacheDirectory("").exists()) {
			for (File file : getCacheDirectory("").listFiles()) {
				fillCacheFiles(file, unusedCacheFiles);
			}
		}

		return unusedCacheFiles;
	}

	/**
	 * Fill a list with all cache files which are written in the {@link WorkspaceCache}.
	 * 
	 * @param subdir sub-directory name in {@link WorkspaceCache}
	 * @param unusedFiles list a files in the {@link WorkspaceCache} which are not used by Globe.
	 */
	private static void fillCacheFiles(File subdir, List<File> unusedFiles) {
		if (subdir.exists()) {
			for (File file : subdir.listFiles()) {
				boolean unusedFile = true;
				if (cacheEntities != null) {
					for (CacheEntity cacheEntity : cacheEntities) {
						if (cacheEntity.getGuidDirectory().compareTo(file) == 0) {
							unusedFile = false;
						}
					}
				}

				if (unusedFile) {
					unusedFiles.add(file);
				}
			}
		}
	}

	/**
	 * Computes a unique identifier for a workspace cache file.
	 * 
	 * @param list : name of source file.
	 * @param parameters : serialization of parameters in bytes tab.
	 * @return a unique identifier based on fileNames
	 */
	public static String computeGuid(String fileNames, byte[] parameters) {
		ArrayList<String> list = new ArrayList<>();
		return computeGuid(list, parameters);
	}

	/**
	 * Computes a unique identifier for a workspace cache file.
	 * 
	 * @param fileNames : name of source file.
	 * @param parameters : serialization of parameters in bytes tab.
	 * @return a unique identifier based on fileNames
	 */
	public static String computeGuid(List<String> fileNames, byte[] parameters) {

		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String guid = "";

		/**
		 * find the most recent modified file
		 *
		 */
		long maxDate = 0;

		for (String fileName : fileNames) {
			maxDate = Math.max(maxDate, new File(fileName).lastModified());
		}
		guid = simpleDateFormat.format(maxDate);

		/*
		 * ByteArrayOutputStream out = new ByteArrayOutputStream();
		 * 
		 * try { for (String fileName : fileNames) { out.write(fileName.getBytes()); } guid +=
		 * DigestUtils.md5Hex(out.toByteArray()); } catch (IOException e) { logger.error("error computing GUID", e); }
		 */

		guid += DigestUtils.md5Hex(parameters);

		return guid;
	}

	/**
	 * Remove all unused files in workspace cache directory.
	 */
	public static void cleanUnusedCacheFiles() {

		List<File> unusedCacheFiles = askForUnusedCacheEntity();

		for (File file : unusedCacheFiles) {
			FileUtils.deleteDir(file);
		}
	}

	/**
	 * Remove specific files in workspace cache directory.
	 * 
	 * @return true if deletion succeed
	 */
	public static boolean delete(List<File> files) {

		for (File file : files) {
			if (!delete(file)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Remove specific file in workspace cache directory.
	 * 
	 * @return true if deletion succeed
	 */
	public static boolean delete(File file) {

		boolean result = false;
		List<Integer> indexesToDelete = new ArrayList<Integer>();

		File cache = getCacheDirectory("");
		if (!file.toPath().startsWith(cache.toPath())) {
			// not a cache subdirectory
			return false;
		}

		if (cacheEntities.size() > 0) {
			for (int i = 0; i < cacheEntities.size(); i++) {
				if (cacheEntities.get(i).getGuidDirectory().getPath().compareTo(file.getPath()) == 0) {
					if (FileUtils.deleteDir(cacheEntities.get(i).getGuidDirectory())) {
						indexesToDelete.add(0, i);
						result = true;
					} else {
						return false;
					}
				}
			}
		} else {
			if (FileUtils.deleteDir(file)) {
				result = true;
			} else {
				return false;
			}
		}

		for (int index : indexesToDelete) {
			cacheEntities.remove(index);
		}

		return result;
	}

	/**
	 * Find a {@link CacheEntity} in {@link WorkspaceCache#cacheEntities}by using its directory.
	 * 
	 * @return {@link CacheEntity} or nothing if nothing match.
	 */
	public static CacheEntity findCacheEntityByGUID(File guidDir) {
		if (cacheEntities != null) {
			for (CacheEntity cacheEntity : cacheEntities) {
				if (cacheEntity.getGuidDirectory().getPath().compareTo(guidDir.getPath()) == 0) {
					return cacheEntity;
				}
			}
		}
		return null;
	}

	/**
	 * Returns a sub-path of {@link WorkspaceCache} from a sub-directory name.
	 * 
	 * @param subdir a sub-directory of {@link WorkspaceCache}
	 * @return workspace cache file
	 */
	public static File getCacheDirectory(String subdir) {

		final Location instanceLoc = Platform.getInstanceLocation();
		String currentWorkspacePath = instanceLoc.getURL().getPath();
		File cacheDirectory = new File(currentWorkspacePath, CACHE_DIRECTORY + subdir);
		if (!cacheDirectory.exists()) {
			cacheDirectory.mkdirs();
		}

		return cacheDirectory;
	}

	/**
	 * Returns root path of workspace cache.
	 */
	public static String getRootPath() {
		return getCacheDirectory("").getAbsolutePath();
	}

	/**
	 * Returns path of preferences cache.
	 */
	public static File getPreferencePath() {
		return getCacheDirectory("prefs");
	}

	/**
	 * Returns root path of workspace cache.
	 */
	public static String getFrisePath() {
		return getCacheDirectory("").getAbsolutePath() + File.separator + "placa";
	}

	/** @return {@link WorkspaceCache#cacheEntities} */
	public static List<CacheEntity> getCacheEntities() {
		if (cacheEntities == null) {
			cacheEntities = new ArrayList<CacheEntity>();
		}
		return cacheEntities;
	}
}
