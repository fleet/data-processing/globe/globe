package fr.ifremer.globe.utils.cache;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Globe temporary cache files management. They are written in Workspace .cache/Temp directory. Default system temporary
 * directory, C:\Users\<userName>\AppData\Local\Temp is also check for cleaning.
 */
public class TemporaryCache {

	private static final Logger logger = LoggerFactory.getLogger(TemporaryCache.class);

	public static String TEMP_DIRECTORY = "Temp" + File.separator;

	private static String marker = "GLOBE_";
	private static String[] markers = { marker, "EMODNET" };
	/** List of files used by current instance of Globe. */
	private static List<File> filesInUse = new ArrayList<File>();

	/**
	 * Creates a new temporary cache file.
	 *
	 * @param prefix
	 * @param suffix
	 * @return a new temporary file
	 * @throws IOException
	 */
	public static synchronized File createTemporaryFile(String prefix, String suffix) throws IOException {
		File file = File.createTempFile(prefix, suffix, WorkspaceCache.getCacheDirectory(TEMP_DIRECTORY));
		filesInUse.add(file);
		return file;
	}

	/**
	 * Creates a temporary directory.
	 *
	 * @param prefix
	 * @param suffix
	 * @return a temporary directory
	 * @throws IOException
	 */
	public static File createTemporaryDirectory(String prefix, String suffix) throws IOException {

		File folder = null;

		do {
			File temp = File.createTempFile(prefix, suffix, WorkspaceCache.getCacheDirectory(TEMP_DIRECTORY));
			if (!temp.delete()) {
				throw new IOException("Error on temporary file deletion");
			}
			folder = new File(temp.getAbsolutePath());
		} while (folder == null || !folder.mkdir());

		folder.deleteOnExit();
		filesInUse.add(folder);

		return folder;
	}

	/**
	 * Returns root path of temporary cache.
	 */
	public static String getRootPath() {
		return WorkspaceCache.getCacheDirectory(TEMP_DIRECTORY).getAbsolutePath();
	}

	/**
	 * Delete temporary files with prefix and suffix for input filter.
	 *
	 * @param prefix
	 * @param suffix
	 * @throws GIOException
	 */
	public static void deleteAllFiles(String prefix, String suffix) throws GIOException {

		List<File> files = new ArrayList<File>(filesInUse);
		boolean deletionFailed = false;

		for (File file : files) {
			if (file.getName().startsWith(prefix) && file.getName().endsWith(suffix)) {
				if (!delete(file)) {
					deletionFailed = true;
				}
			}
		}

		if (deletionFailed) {
			throw new GIOException("Cannot suppress one of temporary files.");
		}
	}

	/**
	 * Delete temporary file or directory.
	 *
	 * @param file : file to delete.
	 * @return true is deletion succeed
	 */
	public static boolean delete(File file) {

		if (file.isDirectory()) {
			if (FileUtils.deleteDir(file)) {
				filesInUse.remove(file);
				return true;
			}
		} else {
			filesInUse.remove(file);
			if (FileUtils.delete(file)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Delete temporary files.
	 *
	 * @param files : list of file to delete.
	 * @return true is all deletion succeed, false if one or more file is not deleted
	 */
	public static boolean deleteFiles(List<File> files) {
		boolean result = true;

		for (File file : files) {
			if (!delete(file)) {
				result = false;
			}
		}

		return result;
	}

	/**
	 * Delete all temporary files used by the current instance of Globe
	 *
	 * @return true if all deletion succeed
	 */
	public static boolean cleanFilesInUse() {
		List<File> files = new ArrayList<File>(filesInUse);
		return deleteFiles(files);
	}

	/**
	 * Delete all temporary files (used and unused by Globe)
	 *
	 * @return true if all deletion succeed
	 */
	public static boolean cleanAllFiles() {
		// we keep cleaning of default temp dir for backward compatibility
		File[] defaultCacheDirectoriesAndFiles = new File(System.getProperty("java.io.tmpdir")).listFiles();
		for (String prefix : markers) {
			for (File file : defaultCacheDirectoriesAndFiles) {
				if (file.getName().startsWith(prefix)) {
					FileUtils.deleteDir(file);
				}
			}
		}

		// clean all files in workspace temp dir
		File[] workspaceCacheDirectoriesAndFiles = WorkspaceCache.getCacheDirectory(TEMP_DIRECTORY).listFiles();
		for (File file : workspaceCacheDirectoriesAndFiles) {
			FileUtils.deleteDir(file);
		}

		List<File> files = new ArrayList<File>(filesInUse);
		return deleteFiles(files);
	}

	/**
	 * Check is a file is used by current instance of Globe.
	 *
	 * @param file : file to check.
	 * @return true if the file is used by current instance of Globe
	 */
	public static boolean isUsed(File file) {
		return filesInUse.contains(file);
	}

	/**
	 * @return the cache files in temporary cache directories
	 */
	public static List<File> getTemporaryCacheFiles() {

		List<File> temporaryCacheFiles = new ArrayList<File>();

		File[] defaultCacheDirectoriesAndFiles = new File(System.getProperty("java.io.tmpdir")).listFiles();
		for (String prefix : markers) {
			for (File cacheDirectoryOrFile : defaultCacheDirectoriesAndFiles) {
				if (cacheDirectoryOrFile.getName().startsWith(prefix)) {
					temporaryCacheFiles.add(cacheDirectoryOrFile);
				}
			}
		}

		File[] workspaceCacheDirectoriesAndFiles = WorkspaceCache.getCacheDirectory(TEMP_DIRECTORY).listFiles();
		temporaryCacheFiles.addAll(Arrays.asList(workspaceCacheDirectoriesAndFiles));

		return temporaryCacheFiles;
	}
}