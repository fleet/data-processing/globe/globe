package fr.ifremer.globe.utils;

import java.util.HashMap;
import java.util.Set;
/**
 * An efficient object counter, implemented with an HashMap
 * {@see <a href="http://www.programcreek.com/2013/10/efficient-counter-in-java/"> http://www.programcreek.com/2013/10/efficient-counter-in-java/ </a>
 * */
public class ObjectCounter<T> {

	private HashMap<T, int[]> intCounter = new HashMap<T, int[]>();

	/**
	 * Add an object the Hasmap, increment its count by one
	 * */
	public void add(T object) {
		int[] valueWrapper = intCounter.get(object);

		if (valueWrapper == null) {
			intCounter.put(object, new int[] { 1 });
		} else {
			valueWrapper[0]++;
		}
	}
	/**
	 * retrieve the list of entries
	 * */
	public Set<T> getKeys()
	{
		return intCounter.keySet();
	}
	/**
	 * retrieve the count for one entry
	 * */
	public int getCount(T object)
	{
		int[] valueWrapper = intCounter.get(object);
		if(valueWrapper==null)
			return 0;
		return valueWrapper[0];
	}
	/**
	 * parse the keys and retrieve the one with the highest count
	 * @return the higher count key, null if the map is empty
	 * */
	public T findDominant()
	{
		int max=0;
		T ret=null;
		for(T v:getKeys())
		{
			int count=getCount(v);
			
			if(count>max)
			{
				max=count;
				ret=v;
			}
		}
		return ret;
	}
}
