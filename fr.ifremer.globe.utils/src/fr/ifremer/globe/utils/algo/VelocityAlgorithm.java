package fr.ifremer.globe.utils.algo;

/**
 * CLASS IMPORTED FROM DORIS
 */
public class VelocityAlgorithm {

	protected static final double ABSOLUTE_PRESSURE_IN_DBAR = 10.1325;

	protected static final double ABSOLUTE_SALINITY_FACTOR = 35.16504 / 35;

	/**
	 * Constructor.
	 * 
	 * @param enumValue , Enum value for algorithm.
	 */
	public VelocityAlgorithm() {
	}

	/**
	 * depth in meters from pressure in decibars See the formula here:
	 * http://www.seabird.com/document/an69-conversion-pressure-depth
	 * 
	 * @param pressure pressure in decibars
	 * @return the depth in meters TODO demander r�f�rence formule � Jean Marc
	 * 
	 * @since NSVP 2.1.1
	 */
	public static double calculDepthFromPressure(double pressure) {
		return ((((-1.82e-15 * pressure + 2.279e-10) * pressure - 2.2512e-5) * pressure + 9.72659) * pressure) / 9.8;
	}

	/**
	 * depth in meters from pressure in decibars See the formula here:
	 * http://www.seabird.com/document/an69-conversion-pressure-depth
	 * 
	 * @param pressure pressure in decibars
	 * @param latitude latitude in degrees
	 */
	public static double calculDepthFromPressure(double pressure, double latitude) {
		double x = Math.pow(Math.sin(latitude / 57.29578), 2);
		double gravity = 9.780318 * (1.0 + (5.2788e-3 + 2.36e-5 * x) * x) + 1.092e-6 * pressure;
		return ((((-1.82e-15 * pressure + 2.279e-10) * pressure - 2.2512e-5) * pressure + 9.72659) * pressure)
				/ gravity;
	}

	/**
	 * Compute pressure value.<br/>
	 * <br/>
	 * <i> P(Z,theta) = h(Z,theta) - thyh0Z h(Z,theta) = h(Z,45) x k(Z,theta) h(Z,45) = 1.00818 x 10-2 Z + 2.465 x
	 * 10-8Z2 - 1.25 x 10-13Z3 + 2.8 x 10-19Z4 k(Z,theta) = (g(theta) - 2 x 10-5Z)/(9.80612 - 2 x 10-5Z) g(theta) =
	 * 9.7803(1 + 5.3 x 10-3 sin2theta) thyh0Z = 1.0x10-2 Z/(Z+100) + 6.2x10-6 Z <br/>
	 * Z = depth in metres <br/>
	 * h = pressure in MPa (relative to atmospheric pressure) <br/>
	 * theta = latitude <br/>
	 * <br/>
	 * equation from C. C. Leroy and F Parthiot, Depth-pressure relationship in the oceans and seas (1998) J. Acoust.
	 * Soc. Am. 103(3) pp 1346-1352</i>
	 * 
	 * @param depth , depth in metres
	 * @param latitude , in angle
	 * @return the pressure in Bar
	 */
	public static double calculPressureFromDepth(double depth, double latitude) {

		// Get gravity

		double sin = Math.sin(latitude);

		double g = 9.7803 * (1 + 5.3e-3 * sin * sin);

		double h1 = 1.00818e-2 * depth + 2.465e-8 * Math.pow(depth, 2) - 1.25e-13 * Math.pow(depth, 3)
				+ 2.8E-19 * Math.pow(depth, 4);

		double k = (g - 2e-5 * depth) / (9.80612 - 2e-5 * depth);

		double thyh0Z = 0.01 * depth / (depth + 100.0) + 6.2e-6 * depth;

		double h = h1 * k;
		//
		return (h - thyh0Z) * 10;
	}

	public static double gsw_enthalpy_sso_0_p(double p) {
		double v01 = 9.998420897506056e2;
		double v05 = -6.698001071123802e0;
		double v08 = -3.988822378968490e-2;
		double v12 = -2.233269627352527e-2;
		double v15 = -1.806789763745328e-4;
		double v17 = -3.087032500374211e-7;
		double v20 = 1.550932729220080e-10;
		double v21 = 1.0e0;
		double v26 = -7.521448093615448e-3;
		double v31 = -3.303308871386421e-5;
		double v36 = 5.419326551148740e-6;
		double v37 = -2.742185394906099e-5;
		double v41 = -1.105097577149576e-7;
		double v43 = -1.119011592875110e-10;
		double v47 = -1.200507748551599e-15;
		double db2pa = 1e4;
		double sso = 35.16504e0;
		double sqrtsso = 5.930011804372737e0;

		double a0;
		double a1;
		double a2;
		double a3;
		double b0;
		double b1;
		double b2;
		double b1sq;
		double sqrt_disc;
		double n;
		double m;
		double a;
		double b;
		double part;

		a0 = v21 + sso * (v26 + v36 * sso + v31 * sqrtsso);

		a1 = v37 + v41 * sso;

		a2 = v43;

		a3 = v47;

		b0 = v01 + sso * (v05 + v08 * sqrtsso);

		b1 = 0.5 * (v12 + v15 * sso);

		b2 = v17 + v20 * sso;

		b1sq = b1 * b1;
		sqrt_disc = Math.sqrt(b1sq - b0 * b2);

		n = a0 + (2e0 * a3 * b0 * b1 / b2 - a2 * b0) / b2;

		m = a1 + (4e0 * a3 * b1sq / b2 - a3 * b0 - 2 * a2 * b1) / b2;

		a = b1 - sqrt_disc;
		b = b1 + sqrt_disc;

		part = (n * b2 - m * b1) / (b2 * (b - a));

		return (db2pa * (p * (a2 - 2e0 * a3 * b1 / b2 + 0.5e0 * a3 * p) / b2
				+ (m / (2e0 * b2)) * Math.log(1e0 + p * (2e0 * b1 + b2 * p) / b0)
				+ part * Math.log(1e0 + (b2 * p * (b - a)) / (a * (b + b2 * p)))));
	}

	/**
	 * from salinity.m by: Edward T Peltzer, MBARI revised: 2007 Apr 28.
	 * 
	 * SALINITY CALCULATION (pss) FROM CONDUCTIVITY. Reference: Fofonff, P. and Millard, R.C. Jr. Unesco 1983.
	 * Algorithms for computation of fundamental properties of seawater. Unesco Tech. Pap. in Mar. Sci., No. 44, 53 pp.
	 * 
	 * @param conductivity in situ conductivity (S/m)
	 * @param temperature temperature in degree C
	 * @param pressure pressure in dbars (not SI)
	 * @return the of value of salinity
	 */
	public static double conductivityToSalinity(double conductivity, double temperature, double pressure) {
		// conductivity at 15 deg C MBARI default value = 4.2914
		double c15 = 4.2914;

		double a0 = 0.008;
		double a1 = -0.1692;
		double a2 = 25.3851;
		double a3 = 14.0941;
		double a4 = -7.0261;
		double a5 = 2.7081;

		double b0 = 0.0005;
		double b1 = -0.0056;
		double b2 = -0.0066;
		double b3 = -0.0375;
		double b4 = 0.0636;
		double b5 = -0.0144;

		double c0 = 0.6766097;
		double c1 = 2.00564e-2;
		double c2 = 1.104259e-4;
		double c3 = -6.9698e-7;
		double c4 = 1.0031e-9;

		double d1 = 3.426e-2;
		double d2 = 4.464e-4;
		double d3 = 4.215e-1;
		double d4 = -3.107e-3;

		// The e# coefficients reflect the use of pressure in dbar
		// rather that in Pascals (SI).

		double e1 = 2.07e-5;
		double e2 = -6.37e-10;
		double e3 = 3.989e-15;

		double k = 0.0162;

		// Calculate internal variables

		double r = conductivity / c15;
		double rt = c0 + (c1 + (c2 + (c3 + c4 * temperature) * temperature) * temperature) * temperature;
		double rp = 1.0 + (e1 + (e2 + e3 * pressure) * pressure) * pressure
				/ (1.0 + (d1 + d2 * temperature) * temperature + (d3 + d4 * temperature) * r);
		double rt1 = r / rp / rt;
		double sqrtRt = Math.sqrt(rt1);

		// Calculate salinity

		double salt = a0 + (a1 + (a3 + a5 * rt1) * rt1) * sqrtRt + (a2 + a4 * rt1) * rt1;

		double dS = b0 + (b1 + (b3 + b5 * rt1) * rt1) * sqrtRt + (b2 + b4 * rt1) * rt1;
		dS = dS * (temperature - 15) / (1 + k * (temperature - 15));

		salt = salt + dS;
		return salt;
	}

	public static double calculSalinity(double velocity, double temperature, double depth) {
		return (velocity - 1449.2 - 4.6 * temperature + 0.055 * Math.pow(temperature, 2)
				- 0.00029 * Math.pow(temperature, 3) - 0.016 * depth) / (1.34 - 0.01 * temperature) + 35;
	}

}