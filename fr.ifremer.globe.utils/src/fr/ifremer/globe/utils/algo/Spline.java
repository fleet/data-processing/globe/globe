package fr.ifremer.globe.utils.algo;

import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

public class Spline {

	private Spline() {
		// utility class : no constructor
	}

	public static double[] apply(double[] x, double[] y, double[] xi) {
		var splineInterpolator = new SplineInterpolator(); // or other interpolator
		PolynomialSplineFunction psf = splineInterpolator.interpolate(x, y);

		var yi = new double[xi.length];
		for (var i = 0; i < xi.length; i++) {
			yi[i] = psf.value(xi[i]);
		}
		return yi;
	}

}
