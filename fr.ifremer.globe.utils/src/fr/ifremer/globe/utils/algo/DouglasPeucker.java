package fr.ifremer.globe.utils.algo;

import java.util.ArrayList;
import java.util.List;

/**
 * This class provides methods to apply the Douglas Peucker reduction algorithm.
 */
public class DouglasPeucker {

	/** Constructor **/
	private DouglasPeucker() {
		// utility class
	}

	/**
	 * Applies the "Douglas Peucker" algorithm while the reduced list size is over the target size. Each time, the
	 * reduction factor is grown to get a larger reduction.
	 * 
	 */
	public static List<double[]> performRecursivly(List<double[]> list, double reductionFactor, int targetSize) {
		// Target size must be 2 or more.
		targetSize = Math.max(2, targetSize);
		// Apply "Douglas Peucker" algorithm while it's necessary
		while (list.size() > targetSize) {
			// square of reduction factor because of the last line of "dist()" method : (dx * dx + dy * dy); // instead
			// of "Math.sqrt(dx * dx + dy * dy)"
			list = DouglasPeucker.performClassic(list, reductionFactor * reductionFactor);
			reductionFactor *= 1.5; // grow the reduction factor for the next loop
			if (Double.isInfinite(reductionFactor))
				break;
		}
		return list;
	}

	/**
	 * Classic Douglas Peucker algorithm with double[]
	 * <p>
	 * Be careful with reduction factor (should be squared distance : see dist method) !
	 * <p>
	 */
	public static List<double[]> performClassic(List<double[]> inputList, double reductionFactor) {
		List<double[]> resultList = new ArrayList<>();

		// Get the most distant point
		double dmax = 0;
		int index = 0;
		double[] firstPoint = inputList.get(0);
		double[] lastPoint = inputList.get(inputList.size() - 1);
		for (int i = 1; i < inputList.size() - 1; i++) {
			double d = dist(inputList.get(i), firstPoint, lastPoint);
			if (d >= dmax) {
				index = i;
				dmax = d;
			}
		}

		if (dmax > reductionFactor) {
			// Two new data list to process
			List<double[]> subList1 = performClassic(inputList.subList(0, index + 1), reductionFactor);
			List<double[]> subList2 = performClassic(inputList.subList(index, inputList.size()), reductionFactor);

			resultList.addAll(subList1.subList(0, subList1.size() - 1));
			resultList.addAll(subList2);

		} else {
			resultList.add(firstPoint);
			resultList.add(lastPoint);
		}
		return resultList;
	}

	/**
	 * @return distance between a point and a segment
	 */
	private static double dist(double[] point, double[] startLine, double[] endLine) {
		double x1 = startLine[0];
		double y1 = startLine[1];

		double x2 = endLine[0];
		double y2 = endLine[1];

		double x3 = point[0];
		double y3 = point[1];

		double px = x2 - x1;
		double py = y2 - y1;

		double something = px * px + py * py;

		double u = ((x3 - x1) * px + (y3 - y1) * py) / something;

		if (u > 1) {
			u = 1;
		} else if (u < 0) {
			u = 0;
		}
		double x = x1 + u * px;
		double y = y1 + u * py;

		double dx = x - x3;
		double dy = y - y3;

		// Note: If the actual distance does not matter,
		// if you only want to compare what this function
		// returns to other results of this function, you
		// can just return the squared distance instead
		// (i.e. remove the sqrt) to gain a little performance
		//
		// Avoid use of Math.hypot()... bad performances!

		return (dx * dx + dy * dy); // instead of "Math.sqrt(dx * dx + dy * dy)": faster! (but be careful with the
									// reduction factor)
	}
}
