package fr.ifremer.globe.utils.algo;

import org.apache.commons.math3.analysis.interpolation.LoessInterpolator;
import org.apache.commons.math3.exception.MathIllegalArgumentException;

import fr.ifremer.globe.utils.exception.GIOException;

public class Smoothing {

	public static final double DEFAULT_BANDWIDTH = 0.25; // LoessInterpolator.DEFAULT_BANDWIDTH;
	public static final int DEFAULT_ROBUSTNESS_ITERS = LoessInterpolator.DEFAULT_ROBUSTNESS_ITERS;
	public static final double DEFAULT_ACCURACY = LoessInterpolator.DEFAULT_ACCURACY;

	private Smoothing() {
		// utility class : no constructor
	}

	public static double[] apply(double[] x, double[] y) throws GIOException {
		var interpolator = new LoessInterpolator();
		return run(interpolator, x, y);
	}

	public static double[] apply(double[] x, double[] y, double bandwidth, int robustnessIters, double accuracy)
			throws GIOException {
		var interpolator = new LoessInterpolator(bandwidth, robustnessIters, accuracy);
		return run(interpolator, x, y);
	}

	/**
	 * Applies smooth and catch potential exceptions.
	 */
	private static double[] run(LoessInterpolator interpolator, double[] x, double[] y) throws GIOException {
		try {
			return interpolator.smooth(x, y);
		} catch (MathIllegalArgumentException e) {
			throw new GIOException("Invalid parameter : " + e.getMessage() + " (" + e.getClass().getSimpleName() + ")", e);
		}
	}

}