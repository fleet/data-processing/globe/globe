package fr.ifremer.globe.utils.algo.delaunaytriangulation;

import fr.ifremer.globe.utils.pool.IObjectPool;

/**
 * This class represents a 3D triangle in a Triangulation!
 * 
 */

public class Triangle_dt {
	Point_dt a, b, c;
	Triangle_dt abnext, bcnext, canext;
	Circle_dt circum;
	int _mc = 0; // modcounter for triangulation fast update.

	boolean halfplane = false; // true iff it is an infinite face.
	// public boolean visitflag;
	boolean _mark = false; // tag - for bfs algorithms
	// private static boolean visitValue=false;
	public static int _counter = 0, _c2 = 0;

	/**
	 * Constructor.
	 */
	public Triangle_dt() {
	}

	/**
	 * initialize the a triangle form 3 point - store it in counterclockwised order.
	 */
	public void initialize(Point_dt A, Point_dt B, Point_dt C, boolean changeOrder,IObjectPool<Circle_dt> circlePool, IObjectPool<Point_dt> pointPool) {
		// visitflag=visitValue;
		this.a = A;
		int res = C.pointLineTest(A, B);
		if ((res <= Point_dt.LEFT) || (res == Point_dt.INFRONTOFA) || (res == Point_dt.BEHINDB) || !changeOrder) {
			this.b = B;
			this.c = C;
		} else { // RIGHT
			// System.out.println("Warning, ajTriangle(A,B,C) " +
			// "expects points in counterclockwise order.");
			// System.out.println("" + A + B + C);
			this.b = C;
			this.c = B;
		}
		circumcircle(circlePool, pointPool);
		// _id = _counter++;
		// _counter++;_c2++;
		// if(_counter%10000 ==0) System.out.println("Triangle: "+_counter);
	}

	/**
	 * initialize the triangle as a half plane using the segment (A,B).
	 */
	public void initialize(Point_dt A, Point_dt B) {
		this.a = A;
		this.b = B;
		this.halfplane = true;
	}

	/*
	 * protected void finalize() throws Throwable{ super.finalize(); _counter--; }
	 */

	/**
	 * remove all pointers (for debug)
	 */
	// public void clear() {
	// this.abnext = null; this.bcnext=null; this.canext=null;}

	/**
	 * returns true iff this triangle is actually a half plane.
	 */
	public boolean isHalfplane() {
		return this.halfplane;
	}

	/**
	 * returns the first vertex of this triangle.
	 */
	public Point_dt p1() {
		return this.a;
	}

	/**
	 * returns the second vertex of this triangle.
	 */
	public Point_dt p2() {
		return this.b;
	}

	/**
	 * returns the 3th vertex of this triangle.
	 */
	public Point_dt p3() {
		return this.c;
	}

	/**
	 * returns the consecutive triangle which shares this triangle p1,p2 edge.
	 */
	public Triangle_dt next_12() {
		return this.abnext;
	}

	/**
	 * returns the consecutive triangle which shares this triangle p2,p3 edge.
	 */
	public Triangle_dt next_23() {
		return this.bcnext;
	}

	/**
	 * returns the consecutive triangle which shares this triangle p3,p1 edge.
	 */
	public Triangle_dt next_31() {
		return this.canext;
	}

	/**
	 * @return The bounding rectange between the minimum and maximum coordinates of
	 *         the triangle
	 */
	public BoundingBox getBoundingBox() {
		Point_dt lowerLeft, upperRight;
		lowerLeft = new Point_dt(Math.min(this.a.x(), Math.min(this.b.x(), this.c.x())),
				Math.min(this.a.y(), Math.min(this.b.y(), this.c.y())));
		upperRight = new Point_dt(Math.max(this.a.x(), Math.max(this.b.x(), this.c.x())),
				Math.max(this.a.y(), Math.max(this.b.y(), this.c.y())));
		return new BoundingBox(lowerLeft, upperRight);
	}

	void switchneighbors(Triangle_dt Old, Triangle_dt New) {
		if (this.abnext == Old) {
			this.abnext = New;
		} else if (this.bcnext == Old) {
			this.bcnext = New;
		} else if (this.canext == Old) {
			this.canext = New;
		} else {
			System.out.println("Error, switchneighbors can't find Old.");
		}
	}

	Triangle_dt neighbor(Point_dt p) {
		if (this.a == p) {
			return this.canext;
		}
		if (this.b == p) {
			return this.abnext;
		}
		if (this.c == p) {
			return this.bcnext;
		}
		System.out.println("Error, neighbors can't find p: " + p);
		return null;
	}

	/**
	 * Returns the neighbors that shares the given corner and is not the previous
	 * triangle.
	 * 
	 * @param p            The given corner
	 * @param prevTriangle The previous triangle.
	 * @return The neighbors that shares the given corner and is not the previous
	 *         triangle.
	 * 
	 *         By: Eyal Roth & Doron Ganel.
	 */
	Triangle_dt nextNeighbor(Point_dt p, Triangle_dt prevTriangle) {
		Triangle_dt neighbor = null;

		if (this.a.equals(p)) {
			neighbor = this.canext;
		}
		if (this.b.equals(p)) {
			neighbor = this.abnext;
		}
		if (this.c.equals(p)) {
			neighbor = this.bcnext;
		}

		// Udi Schneider: Added a condition check for isHalfPlane. If the
		// current
		// neighbor is a half plane, we also want to move to the next neighbor
		if (neighbor.equals(prevTriangle) || neighbor.isHalfplane()) {
			if (this.a.equals(p)) {
				neighbor = this.abnext;
			}
			if (this.b.equals(p)) {
				neighbor = this.bcnext;
			}
			if (this.c.equals(p)) {
				neighbor = this.canext;
			}
		}

		return neighbor;
	}

	Circle_dt circumcircle(IObjectPool<Circle_dt> circlePool, IObjectPool<Point_dt> pointPool) {
		if (this.circum != null) {
			pointPool.returnObject(this.circum.Center());
			circlePool.returnObject(this.circum);
		}

		double u = ((this.a.x - this.b.x) * (this.a.x + this.b.x) + (this.a.y - this.b.y) * (this.a.y + this.b.y))
				/ 2.0f;
		double v = ((this.b.x - this.c.x) * (this.b.x + this.c.x) + (this.b.y - this.c.y) * (this.b.y + this.c.y))
				/ 2.0f;
		double den = (this.a.x - this.b.x) * (this.b.y - this.c.y) - (this.b.x - this.c.x) * (this.a.y - this.b.y);

		this.circum = circlePool.borrowObject();
		Point_dt cen = pointPool.borrowObject();
		if (den == 0) {
			cen.initialize(this.a.x, this.a.y, this.a.z, this.a.info);
			this.circum.setCenter(cen);
			this.circum.setRadius(Double.POSITIVE_INFINITY);
		} else {
			cen.initialize((u * (this.b.y - this.c.y) - v * (this.a.y - this.b.y)) / den,
					(v * (this.a.x - this.b.x) - u * (this.b.x - this.c.x)) / den);
			this.circum.setCenter(cen);
			this.circum.setRadius(cen.distance2(this.a));
		}
		return this.circum;
	}

	boolean circumcircle_contains(Point_dt p) {

		return this.circum.Radius() > this.circum.Center().distance2(p);
	}

	@Override
	public String toString() {
		String res = ""; // +_id+") ";
		res += this.a.toString() + this.b.toString();
		if (!this.halfplane) {
			res += this.c.toString();
		}
		// res +=c.toString() +" | "+abnext._id+" "+bcnext._id+" "+canext._id;
		return res;
	}

	/**
	 * determinates if this triangle contains the point p.
	 * 
	 * @param p the query point
	 * @return true iff p is not null and is inside this triangle (Note: on boundary
	 *         is considered inside!!).
	 */
	public boolean contains(Point_dt p) {
		boolean ans = false;
		if (this.halfplane | p == null) {
			return false;
		}

		if (isCorner(p)) {
			return true;
		}

		int a12 = p.pointLineTest(this.a, this.b);
		int a23 = p.pointLineTest(this.b, this.c);
		int a31 = p.pointLineTest(this.c, this.a);

		if ((a12 == Point_dt.LEFT && a23 == Point_dt.LEFT && a31 == Point_dt.LEFT)
				|| (a12 == Point_dt.RIGHT && a23 == Point_dt.RIGHT && a31 == Point_dt.RIGHT)
				|| (a12 == Point_dt.ONSEGMENT || a23 == Point_dt.ONSEGMENT || a31 == Point_dt.ONSEGMENT)) {
			ans = true;
		}

		return ans;
	}

	/**
	 * determinates if this triangle contains the point p.
	 * 
	 * @param p the query point
	 * @return true iff p is not null and is inside this triangle (Note: on boundary
	 *         is considered outside!!).
	 */
	public boolean contains_BoundaryIsOutside(Point_dt p) {
		boolean ans = false;
		if (this.halfplane | p == null) {
			return false;
		}

		if (isCorner(p)) {
			return true;
		}

		int a12 = p.pointLineTest(this.a, this.b);
		int a23 = p.pointLineTest(this.b, this.c);
		int a31 = p.pointLineTest(this.c, this.a);

		if ((a12 == Point_dt.LEFT && a23 == Point_dt.LEFT && a31 == Point_dt.LEFT)
				|| (a12 == Point_dt.RIGHT && a23 == Point_dt.RIGHT && a31 == Point_dt.RIGHT)) {
			ans = true;
		}

		return ans;
	}

	/**
	 * Checks if the given point is a corner of this triangle.
	 * 
	 * @param p The given point.
	 * @return True iff the given point is a corner of this triangle.
	 * 
	 *         By Eyal Roth & Doron Ganel.
	 */
	public boolean isCorner(Point_dt p) {
		return (p.x == this.a.x & p.y == this.a.y) | (p.x == this.b.x & p.y == this.b.y)
				| (p.x == this.c.x & p.y == this.c.y);
	}

	// Doron
	public boolean fallInsideCircumcircle(Point_dt[] arrayPoints) {
		boolean isInside = false;
		Point_dt p1 = this.p1();
		Point_dt p2 = this.p2();
		Point_dt p3 = this.p3();
		int i = 0;
		while (!isInside && i < arrayPoints.length) {
			Point_dt p = arrayPoints[i];
			if (!p.equals(p1) && !p.equals(p2) && !p.equals(p3)) {
				isInside = this.circumcircle_contains(p);
			}
			i++;
		}

		return isInside;
	}

	/**
	 * compute the Z value for the X,Y values of q. <br />
	 * assume this triangle represent a plane --> q does NOT need to be contained in
	 * this triangle.
	 * 
	 * @param q query point (its Z value is ignored).
	 * @return the Z value of this plane implies by this triangle 3 points.
	 */
	public double z_value(Point_dt q) {
		if (q == null || this.halfplane) {
			throw new RuntimeException("*** ERR wrong parameters, can't approximate the z value ..***: " + q);
		}
		/* incase the query point is on one of the points */
		if (q.x == this.a.x & q.y == this.a.y) {
			return this.a.z;
		}
		if (q.x == this.b.x & q.y == this.b.y) {
			return this.b.z;
		}
		if (q.x == this.c.x & q.y == this.c.y) {
			return this.c.z;
		}

		/*
		 * plane: aX + bY + c = Z: 2D line: y= mX + k
		 */
		double X = 0, x0 = q.x, x1 = this.a.x, x2 = this.b.x, x3 = this.c.x;
		double Y = 0, y0 = q.y, y1 = this.a.y, y2 = this.b.y, y3 = this.c.y;
		double Z = 0, m01 = 0, k01 = 0, m23 = 0, k23 = 0;

		// 0 - regular, 1-horisintal , 2-vertical.
		int flag01 = 0;
		if (x0 != x1) {
			m01 = (y0 - y1) / (x0 - x1);
			k01 = y0 - m01 * x0;
			if (m01 == 0) {
				flag01 = 1;
			}
		} else { // 2-vertical.
			flag01 = 2;// x01 = x0
		}
		int flag23 = 0;
		if (x2 != x3) {
			m23 = (y2 - y3) / (x2 - x3);
			k23 = y2 - m23 * x2;
			if (m23 == 0) {
				flag23 = 1;
			}
		} else { // 2-vertical.
			flag23 = 2;// x01 = x0
		}

		if (flag01 == 2) {
			X = x0;
			Y = m23 * X + k23;
		} else {
			if (flag23 == 2) {
				X = x2;
				Y = m01 * X + k01;
			} else { // regular case
				X = (k23 - k01) / (m01 - m23);
				Y = m01 * X + k01;

			}
		}
		double r = 0;
		if (flag23 == 2) {
			r = (y2 - Y) / (y2 - y3);
		} else {
			r = (x2 - X) / (x2 - x3);
		}
		Z = this.b.z + (this.c.z - this.b.z) * r;
		if (flag01 == 2) {
			r = (y1 - y0) / (y1 - Y);
		} else {
			r = (x1 - x0) / (x1 - X);
		}
		double qZ = this.a.z + (Z - this.a.z) * r;
		return qZ;
	}

	/**
	 * compute the Z value for the X,Y values of q. assume this triangle represent a
	 * plane --> q does NOT need to be contained in this triangle.
	 * 
	 * @param x x-coordinate of the query point.
	 * @param y y-coordinate of the query point.
	 * @return z (height) value approximation given by the triangle it falls in.
	 * 
	 */
	public double z(double x, double y) {
		return z_value(new Point_dt(x, y));
	}

	/**
	 * compute the Z value for the X,Y values of q. assume this triangle represent a
	 * plane --> q does NOT need to be contained in this triangle.
	 * 
	 * @param q query point (its Z value is ignored).
	 * @return q with updated Z value.
	 * 
	 */
	public Point_dt z(Point_dt q) {
		double z = z_value(q);
		return new Point_dt(q.x, q.y, z);
	}

	public Vector_dt getNormal(IObjectPool<Vector_dt> vectorPool) {
		Vector_dt u = vectorPool.borrowObject();
		u.initialize(this.p1(), this.p2());
		Vector_dt v = vectorPool.borrowObject();
		v.initialize(this.p1(), this.p3());
		Vector_dt result = u.crossProduct(v, vectorPool);
		vectorPool.returnObject(u);
		vectorPool.returnObject(v);
		return result;
	}

	public void updateNeighbours() {
		this.a.addNeighbour(this.b);
		this.b.addNeighbour(this.a);
		if (!this.halfplane) {
			this.a.addNeighbour(this.c);
			this.c.addNeighbour(this.a);
			this.b.addNeighbour(this.c);
			this.c.addNeighbour(this.b);
		}

	}

	/**
	 * @return the {@link #a}
	 */
	public Point_dt getA() {
		return this.a;
	}

	/**
	 * @param a the {@link #a} to set
	 */
	public void setA(Point_dt a) {
		this.a = a;
	}

	/**
	 * @return the {@link #b}
	 */
	public Point_dt getB() {
		return this.b;
	}

	/**
	 * @param b the {@link #b} to set
	 */
	public void setB(Point_dt b) {
		this.b = b;
	}

	/**
	 * @return the {@link #c}
	 */
	public Point_dt getC() {
		return this.c;
	}

	/**
	 * @param c the {@link #c} to set
	 */
	public void setC(Point_dt c) {
		this.c = c;
	}

	/**
	 * @param halfplane the {@link #halfplane} to set
	 */
	public void setHalfplane(boolean halfplane) {
		this.halfplane = halfplane;
	}

	/**
	 * @return the {@link #abnext}
	 */
	public Triangle_dt getAbnext() {
		return this.abnext;
	}

	/**
	 * @param abnext the {@link #abnext} to set
	 */
	public void setAbnext(Triangle_dt abnext) {
		this.abnext = abnext;
	}

	/**
	 * @return the {@link #bcnext}
	 */
	public Triangle_dt getBcnext() {
		return this.bcnext;
	}

	/**
	 * @param bcnext the {@link #bcnext} to set
	 */
	public void setBcnext(Triangle_dt bcnext) {
		this.bcnext = bcnext;
	}

	/**
	 * @return the {@link #canext}
	 */
	public Triangle_dt getCanext() {
		return this.canext;
	}

	/**
	 * @param canext the {@link #canext} to set
	 */
	public void setCanext(Triangle_dt canext) {
		this.canext = canext;
	}

	/**
	 * @return the {@link #circum}
	 */
	public Circle_dt getCircum() {
		return this.circum;
	}

	/**
	 * @param circum the {@link #circum} to set
	 */
	public void setCircum(Circle_dt circum) {
		this.circum = circum;
	}

	/**
	 * Initialize this instance as if it was just created.
	 */
	public void clean() {
		this.a = null;
		this.b = null;
		this.c = null;
		this.abnext = null;
		this.bcnext = null;
		this.canext = null;
		this.circum = null;
		this._mc = 0;
		this.halfplane = false;
		this._mark = false;
	}

	/**
	 * @return true if clean() ha been called.
	 */
	public boolean isCleaned() {
		return this.a == null;
	}

}
