package fr.ifremer.globe.utils.algo.delaunaytriangulation;

import java.awt.Point;
import java.util.Iterator;

import fr.ifremer.globe.utils.algo.delaunaytriangulation.service.IAlgoPoolService;
import fr.ifremer.globe.utils.pool.IObjectPool;

/**
 * Created by IntelliJ IDEA. User: Aviad Segev Date: 22/11/2009 Time: 20:10:04
 * 
 * Grid Index is a simple spatial index for fast point/triangle location. The idea is to divide a predefined geographic
 * extent into equal sized cell matrix (tiles). Every cell will be associated with a triangle which lies inside.
 * Therfore, one can easily locate a triangle in close proximity of the required point by searching from the point's
 * cell triangle. If the triangulation is more or less uniform and bound in space, this index is very effective, roughly
 * recuing the searched triangles by square(xCellCount * yCellCount), as only the triangles inside the cell are
 * searched.
 * 
 * The index takes xCellCount * yCellCount capacity. While more cells allow faster searches, even a small grid is
 * helpfull.
 * 
 * This implementation holds the cells in a memory matrix, but such a grid can be easily mapped to a DB table or file
 * where it is usually used for it's fullest.
 * 
 * Note that the index is geographically bound - only the region given in the c'tor is indexed. Added Triangles outside
 * the indexed region will cause rebuilding of the whole index. Since triangulation is mostly always used for static
 * raster data, and usually is never updated outside the initial zone (only refininf existing triangles) this is never
 * an issue in real life.
 */
public class GridIndex {
	/**
	 * The triangulation of the index
	 */
	private DelaunayTriangulation indexDelaunay;

	/**
	 * Horizontal geographic size of a cell index
	 */
	private double x_size;

	/**
	 * Vertical geographic size of a cell inedx
	 */
	private double y_size;

	/**
	 * Horizontal number of cells in x
	 */
	private int x_cellCount;

	/**
	 * Vertical geographic size of a cell inedx
	 */
	private int y_cellCount;
	/**
	 * The indexed geographic size
	 */
	private BoundingBox indexRegion;

	/**
	 * A division of indexRegion to a cell matrix, where each cell holds a triangle which lies in it
	 */
	private Triangle_dt[][] grid;

	/** Object pool. */
	IObjectPool<Point_dt> pointPool = IAlgoPoolService.grab().getPointPool();

	/**
	 * Constructs a grid index holding the triangles of a delaunay triangulation. This version uses the bounding box of
	 * the triangulation as the region to index.
	 * 
	 * @param delaunay delaunay triangulation to index
	 * @param xCellCount number of grid cells in a row
	 * @param yCellCount number of grid cells in a column
	 */
	public GridIndex(DelaunayTriangulation delaunay, int xCellCount, int yCellCount) {
		this(delaunay, xCellCount, yCellCount, delaunay.getBoundingBox());
	}

	/**
	 * Constructs a grid index holding the triangles of a delaunay triangulation. The grid will be made of (xCellCount *
	 * yCellCount) cells. The smaller the cells the less triangles that fall in them, whuch means better indexing, but
	 * also more cells in the index, which mean more storage. The smaller the indexed region is, the smaller the cells
	 * can be and still maintain the same capacity, but adding geometries outside the initial region will invalidate the
	 * index !
	 * 
	 * @param delaunay delaunay triangulation to index
	 * @param xCellCount number of grid cells in a row
	 * @param yCellCount number of grid cells in a column
	 * @param region geographic region to index
	 */
	public GridIndex(DelaunayTriangulation delaunay, int xCellCount, int yCellCount, BoundingBox region) {
		init(delaunay, xCellCount, yCellCount, region);
	}

	/**
	 * Initialize the grid index
	 * 
	 * @param delaunay delaunay triangulation to index
	 * @param xCellCount number of grid cells in a row
	 * @param yCellCount number of grid cells in a column
	 * @param region geographic region to index
	 */
	private void init(DelaunayTriangulation delaunay, int xCellCount, int yCellCount, BoundingBox region) {
		this.indexDelaunay = delaunay;
		this.indexRegion = region;
		this.x_size = region.getWidth() / xCellCount;
		this.y_size = region.getHeight() / yCellCount;
		this.x_cellCount = xCellCount;
		this.y_cellCount = yCellCount;

		// The grid will hold a trinagle for each cell, so a point (x,y) will
		// lie
		// in the cell representing the grid partition of region to a
		// xCellCount on yCellCount grid
		this.grid = new Triangle_dt[xCellCount][yCellCount];

		Point_dt middle = middleOfCell(0, 0);
		Triangle_dt colStartTriangle = this.indexDelaunay.find(middle, null);
		this.pointPool.returnObject(middle);
		updateCellValues(0, 0, xCellCount - 1, yCellCount - 1, colStartTriangle);
	}

	/**
	 * Finds a triangle near the given point
	 * 
	 * @param point a query point
	 * @return a triangle at the same cell of the point
	 */
	public Triangle_dt findCellTriangleOf(Point_dt point) {
		int x_index = (int) ((point.x() - this.indexRegion.minX()) / this.x_size);
		if (x_index >= this.x_cellCount) {
			x_index = this.x_cellCount - 1;
		}
		int y_index = (int) ((point.y() - this.indexRegion.minY()) / this.y_size);
		if (y_index >= this.y_cellCount) {
			y_index = this.y_cellCount - 1;
		}
		return this.grid[x_index][y_index];
	}

	/**
	 * Updates the grid index to reflect changes to the triangulation. Note that added triangles outside the indexed
	 * region will force to recompute the whole index with the enlarged region.
	 */
	/**
	 * Updates the grid index to reflect changes to the triangulation. Note that added triangles outside the indexed
	 * region will force to recompute the whole index with the enlarged region.
	 * 
	 * @param updatedTriangles changed triangles of the triangulation. This may be added triangles, removed triangles or
	 *            both. All that matter is that they cover the changed area.
	 */
	public void updateIndex(Iterator<Triangle_dt> updatedTriangles) {

		// Gather the bounding box of the updated area
		BoundingBox updatedRegion = new BoundingBox();

		while (updatedTriangles.hasNext()) {
			updatedRegion = updatedRegion.unionWith(updatedTriangles.next().getBoundingBox());
		}

		if (updatedRegion.isNull()) {
			return;
		}

		// Bad news - the updated region lies outside the indexed region.
		// The whole index must be recalculated
		if (!this.indexRegion.contains(updatedRegion)) {
			init(this.indexDelaunay, (int) (this.indexRegion.getWidth() / this.x_size),
					(int) (this.indexRegion.getHeight() / this.y_size), this.indexRegion.unionWith(updatedRegion));
		} else {
			// Find the cell region to be updated
			Point minInvalidCell = getCellOf(updatedRegion.getMinPoint());
			Point maxInvalidCell = getCellOf(updatedRegion.getMaxPoint());

			// And update it with fresh triangles
			Triangle_dt adjacentValidTriangle = findValidTriangle(minInvalidCell);
			updateCellValues(minInvalidCell.x, minInvalidCell.y, maxInvalidCell.x, maxInvalidCell.y,
					adjacentValidTriangle);
		}
	}

	private void updateCellValues(int startXCell, int startYCell, int lastXCell, int lastYCell,
			Triangle_dt startTriangle) {
		// Go over each grid cell and locate a triangle in it to be the cell's
		// starting search triangle. Since we only pass between adjacent cells
		// we can search from the last triangle found and not from the start.

		// Add triangles for each column cells
		for (int i = startXCell; i <= lastXCell; i++) {
			// Find a triangle at the begining of the current column
			Point_dt middle = middleOfCell(i, startYCell);
			startTriangle = this.indexDelaunay.find(middle, startTriangle);
			this.pointPool.returnObject(middle);

			this.grid[i][startYCell] = startTriangle;
			Triangle_dt prevRowTriangle = startTriangle;

			// Add triangles for the next row cells
			for (int j = startYCell + 1; j <= lastYCell; j++) {
				middle = middleOfCell(i, j);
				this.grid[i][j] = this.indexDelaunay.find(middle, prevRowTriangle);
				this.pointPool.returnObject(middle);
				prevRowTriangle = this.grid[i][j];
			}
		}
	}

	/**
	 * Finds a valid (existing) trinagle adjacent to a given invalid cell
	 * 
	 * @param minInvalidCell minimum bounding box invalid cell
	 * @return a valid triangle adjacent to the invalid cell
	 */
	private Triangle_dt findValidTriangle(Point minInvalidCell) {
		// If the invalid cell is the minimal one in the grid we are forced to
		// search the
		// triangulation for a trinagle at that location
		if (minInvalidCell.x == 0 && minInvalidCell.y == 0) {
			Point_dt middle = middleOfCell(minInvalidCell.x, minInvalidCell.y);
			Triangle_dt result = this.indexDelaunay.find(middle, null);
			this.pointPool.returnObject(middle);
			return result;
		} else {
			// Otherwise we can take an adjacent cell triangle that is still
			// valid
			return this.grid[Math.min(0, minInvalidCell.x)][Math.min(0, minInvalidCell.y)];
		}
	}

	/**
	 * Locates the grid cell point covering the given coordinate
	 * 
	 * @param coordinate world coordinate to locate
	 * @return cell covering the coordinate
	 */
	private Point getCellOf(Point_dt coordinate) {
		int xCell = (int) ((coordinate.x() - this.indexRegion.minX()) / this.x_size);
		int yCell = (int) ((coordinate.y() - this.indexRegion.minY()) / this.y_size);
		return new Point(xCell, yCell);
	}

	/**
	 * Create a point at the center of a cell
	 * 
	 * @param x_index horizontal cell index
	 * @param y_index vertical cell index
	 * @return Point at the center of the cell at (x_index, y_index). Instance should be released to the pool.
	 */
	private Point_dt middleOfCell(int x_index, int y_index) {
		double middleXCell = this.indexRegion.minX() + x_index * this.x_size + this.x_size / 2;
		double middleYCell = this.indexRegion.minY() + y_index * this.y_size + this.y_size / 2;
		Point_dt result = this.pointPool.borrowObject();
		result.initialize(middleXCell, middleYCell);
		return result;

	}
}
