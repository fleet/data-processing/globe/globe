/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.algo.delaunaytriangulation.service.impl;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.utils.algo.delaunaytriangulation.Circle_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Edge_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Point_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Triangle_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Vector_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.service.IAlgoPoolService;
import fr.ifremer.globe.utils.pool.IObjectPool;
import fr.ifremer.globe.utils.pool.IObjectPoolFactory;

/**
 * Implementation of {@link IAlgoPoolService}.
 */
@Component(name = "globe_utils_algo_pool_service", service = IAlgoPoolService.class)
public class AlgoPoolService implements IAlgoPoolService {

	/** Pool of Point_dt. */
	protected IObjectPool<Point_dt> pointPool = null;

	/** Pool of Circle_dt. */
	protected IObjectPool<Circle_dt> circlePool = null;

	/** Pool of Edge_dt. */
	protected IObjectPool<Edge_dt> edgePool = null;

	/** Pool of Triangle_dt. */
	protected IObjectPool<Triangle_dt> trianglePool = null;

	/** Pool of Vector_dt. */
	protected IObjectPool<Vector_dt> vectorPool = null;

	/**
	 * Constructor.
	 */
	public AlgoPoolService() {
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.algo.delaunaytriangulation.service.IAlgoPoolService#getPointPool()
	 */
	@Override
	public IObjectPool<Point_dt> getPointPool() {
		if (this.pointPool == null) {
			this.pointPool = IObjectPoolFactory.grab().makeObjectPool(1024, () -> new Point_dt(),
					(point) -> point.clean());
		}
		return this.pointPool;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.algo.delaunaytriangulation.service.IAlgoPoolService#getCirclePool()
	 */
	@Override
	public IObjectPool<Circle_dt> getCirclePool() {
		if (this.circlePool == null) {
			this.circlePool = IObjectPoolFactory.grab().makeObjectPool(1024, () -> new Circle_dt(),
					(circle) -> circle.clean());
		}
		return this.circlePool;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.algo.delaunaytriangulation.service.IAlgoPoolService#getEdgePool()
	 */
	@Override
	public IObjectPool<Edge_dt> getEdgePool() {
		if (this.edgePool == null) {
			this.edgePool = IObjectPoolFactory.grab().makeObjectPool(1024, () -> new Edge_dt(), (edge) -> edge.clean());
		}
		return this.edgePool;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.algo.delaunaytriangulation.service.IAlgoPoolService#getTrianglePool()
	 */
	@Override
	public IObjectPool<Triangle_dt> getTrianglePool() {
		if (this.trianglePool == null) {
			this.trianglePool = IObjectPoolFactory.grab().makeObjectPool(1024, () -> new Triangle_dt(),
					(triangle) -> triangle.clean());
		}
		return this.trianglePool;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.algo.delaunaytriangulation.service.IAlgoPoolService#getVectorPool()
	 */
	@Override
	public IObjectPool<Vector_dt> getVectorPool() {
		if (this.vectorPool == null) {
			this.vectorPool = IObjectPoolFactory.grab().makeObjectPool(1024, () -> new Vector_dt(),
					(vector) -> vector.clean());
		}
		return this.vectorPool;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.algo.delaunaytriangulation.service.IAlgoPoolService#resetAll()
	 */
	@Override
	public void resetAll() {
		if (this.circlePool != null) {
			this.circlePool.reset();
		}
		if (this.edgePool != null) {
			this.edgePool.reset();
		}
		if (this.pointPool != null) {
			this.pointPool.reset();
		}
		if (this.trianglePool != null) {
			this.trianglePool.reset();
		}
		if (this.vectorPool != null) {
			this.vectorPool.reset();
		}
	}

}
