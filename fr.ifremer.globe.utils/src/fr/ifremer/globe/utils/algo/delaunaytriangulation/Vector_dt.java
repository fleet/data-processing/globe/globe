package fr.ifremer.globe.utils.algo.delaunaytriangulation;

import fr.ifremer.globe.utils.pool.IObjectPool;

/**
 * This class represents a 3D vector, with some simple geometric methods
 */
public class Vector_dt {

	double x, y, z;

	/**
	 * Default Constructor. <br />
	 * constructs a 3D vector at (0,0,0).
	 */
	public Vector_dt() {
	}

	/**
	 * Initialize a 3D vector
	 */
	public void initialize(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/** simple copy constructor */
	public Vector_dt(Vector_dt v) {
		this.x = v.x;
		this.y = v.y;
		this.z = v.z;
	}

	/** Initialize with two points */
	public void initialize(Point_dt a, Point_dt b) {
		this.x = b.x() - a.x();
		this.y = b.y() - a.y();
		this.z = b.z() - a.z();
	}

	/** returns the x-coordinate of this vector. */
	public double x() {
		return this.x;
	}

	/**
	 * Sets the x coordinate.
	 * 
	 * @param x The new x coordinate.
	 */
	public void setX(double x) {
		this.x = x;
	}

	/** returns the y-coordinate of this vector. */
	public double y() {
		return this.y;
	}

	/**
	 * Sets the y coordinate.
	 * 
	 * @param y The new y coordinate.
	 */
	public void setY(double y) {
		this.y = y;
	}

	/** returns the z-coordinate of this vector. */
	public double z() {
		return this.z;
	}

	/**
	 * Sets the z coordinate.
	 * 
	 * @param z The new z coordinate.
	 */
	public void setZ(double z) {
		this.z = z;
	}

	/**
	 * Sets the client info.
	 * 
	 * @param x The new x coordinate.
	 * @param y The new y coordinate.
	 * @param z The new z coordinate.
	 */
	public void set(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * return true if this vector [x,y,z] coordinates are the same as v [x,y,z]
	 * coordinates.
	 */
	public boolean equals(Vector_dt v) {
		return (this.x == v.x) && (this.y == v.y) && (this.z == v.z);
	}

	/** return a String in the [x,y,z] format */
	@Override
	public String toString() {
		return (new String(" Vec[" + this.x + "," + this.y + "," + this.z + "]"));
	}

	public double distance(Vector_dt v) {
		double temp = Math.pow(v.x() - this.x, 2) + Math.pow(v.y() - this.y, 2) + Math.pow(v.z() - this.z, 2);
		return Math.sqrt(temp);
	}

	/** return a String: x y z (used by the save to file - write_tsin method). */
	public String toFile() {
		return ("" + this.x + " " + this.y + " " + this.z);
	}

	@Override
	public boolean equals(Object ob) {
		return false;
	}

	public Vector_dt opposite() {
		this.x = -this.x;
		this.y = -this.y;
		this.z = -this.z;
		return this;
	}

	public double product(Vector_dt w) {
		return (this.x() * w.x() + this.y() * w.y() + this.z() * w.z());
	}

	public Vector_dt crossProduct(Vector_dt w, IObjectPool<Vector_dt> vectorPool) {
		Vector_dt result = vectorPool.borrowObject();
		result.initialize(this.y() * w.z() - this.z() * w.y(), this.z() * w.x() - this.x() * w.z(),
				this.x() * w.y() - this.y() * w.x());
		return result;
	}

	public double norm() {
		return Math.sqrt(this.x() * this.x() + this.y() * this.y() + this.z() * this.z());
	}

	public double getAngle(Vector_dt w) {
		double normU = this.norm();
		double normW = w.norm();
		return Math.acos(this.product(w) / (normU * normW));
	}

	/**
	 * Initialize this instance as if it was just created.
	 */
	public void clean() {
		this.x = 0d;
		this.y = 0d;
		this.z = 0d;
	}

	/**
	 * @return true if clean() ha been called.
	 */
	public boolean isCleaned() {
		return this.x == 0d && this.y == 0d && this.z == 0d;
	}

}