/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.algo.delaunaytriangulation.service;

import fr.ifremer.globe.utils.algo.delaunaytriangulation.Circle_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Edge_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Point_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Triangle_dt;
import fr.ifremer.globe.utils.algo.delaunaytriangulation.Vector_dt;
import fr.ifremer.globe.utils.osgi.OsgiUtils;
import fr.ifremer.globe.utils.pool.IObjectPool;

/**
 * Object pool service
 */
public interface IAlgoPoolService {

	/** @return the service implementation */
	static IAlgoPoolService grab() {
		return OsgiUtils.getService(IAlgoPoolService.class);
	}

	/** @return the unique pool of Point_dt. */
	IObjectPool<Point_dt> getPointPool();

	/** @return the unique pool of Circle_dt. */
	IObjectPool<Circle_dt> getCirclePool();

	/** @return the unique pool of Edge_dt. */
	IObjectPool<Edge_dt> getEdgePool();

	/** @return the unique pool of Triangle_dt. */
	IObjectPool<Triangle_dt> getTrianglePool();

	/** @return the unique pool of Vector_dt. */
	IObjectPool<Vector_dt> getVectorPool();

	/** Close all pools. */
	void resetAll();

}
