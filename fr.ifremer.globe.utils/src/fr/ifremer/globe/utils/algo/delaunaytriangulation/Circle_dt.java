package fr.ifremer.globe.utils.algo.delaunaytriangulation;

/**
 * this class represents a simple circle. <br />
 * it is used by the Delaunay Triangulation class. <br />
 * <br />
 * note that this class is immutable.
 * 
 * @see DelaunayTriangulation
 */
public class Circle_dt {

	private Point_dt c;
	private double r;

	/**
	 * Constructor.
	 */
	public Circle_dt() {
	}

	@Override
	public String toString() {
		return (new String(
				" Circle[" + this.c.toString() + "|" + this.r + "|" + (int) Math.round(Math.sqrt(this.r)) + "]"));
	}

	/**
	 * Gets the center of the circle.
	 * 
	 * @return the center of the circle.
	 */
	public Point_dt Center() {
		return this.c;
	}

	/**
	 * Gets the radius of the circle.
	 * 
	 * @return the radius of the circle.
	 */
	public double Radius() {
		return this.r;
	}

	/**
	 * @param c the {@link #c} to set
	 */
	public void setCenter(Point_dt c) {
		this.c = c;
	}

	/**
	 * @param r the {@link #r} to set
	 */
	public void setRadius(double r) {
		this.r = r;
	}

	/**
	 * Initialize this instance as if it was just created.
	 */
	public void clean() {
		this.c = null;
	}

	/**
	 * @return true if clean() ha been called.
	 */
	public boolean isCleaned() {
		return this.c == null;
	}

}
