package fr.ifremer.globe.utils.algo.delaunaytriangulation;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

/**
 * This class represents a 3D point, with some simple geometric methods (pointLineTest).
 */
public class Point_dt {
	double x, y, z;
	Set<Point_dt> neighbours = new HashSet<>();
	int info;

	/**
	 * Default Constructor. <br />
	 * constructs a 3D point at (0,0,0).
	 */
	public Point_dt() {
	}

	/** constructs a 3D point with a z value of 0. */
	public Point_dt(double x, double y) {
		this(x, y, 0);
	}

	/** Initialize the 3D point with a z value of 0. */
	public void initialize(double x, double y) {
		this.x = x;
		this.y = y;
		this.z = 0;
		this.info = -1;
	}

	/**
	 * Initialize a 3D point
	 */
	public void initialize(double x, double y, double z, int info) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.info = info;
	}

	/**
	 * constructs a 3D point without info
	 */
	public Point_dt(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.info = -1;
	}

	/** returns the x-coordinate of this point. */
	public double x() {
		return this.x;
	}

	/** returns the y-coordinate of this point. */
	public double y() {
		return this.y;
	}

	/** returns the z-coordinate of this point. */
	public double z() {
		return this.z;
	}

	/** returns the client info of this point. */
	public int info() {
		return this.info;
	}

	/** returns the neighbours of this point. */
	public Collection<Point_dt> neighbours() {
		return this.neighbours;
	}

	/**
	 * Add a neighbour
	 * 
	 * @param neighbour My new neighbour.
	 */
	public void addNeighbour(Point_dt p) {
		neighbours().add(p);
	}

	double distance2(Point_dt p) {
		return (p.x - this.x) * (p.x - this.x) + (p.y - this.y) * (p.y - this.y);
	}

	double distance2(double px, double py) {
		return (px - this.x) * (px - this.x) + (py - this.y) * (py - this.y);
	}

	boolean isLess(Point_dt p) {
		return (this.x < p.x) || ((this.x == p.x) && (this.y < p.y));
	}

	boolean isGreater(Point_dt p) {
		return (this.x > p.x) || ((this.x == p.x) && (this.y > p.y));
	}

	/**
	 * return true iff this point [x,y] coordinates are the same as p [x,y] coordinates. (the z value is ignored).
	 */
	public boolean equals(Point_dt p) {
		return (this.x == p.x) && (this.y == p.y);
	}

	/** return a String in the [x,y,z] format */
	@Override
	public String toString() {
		return (new String(" Pt[" + this.x + "," + this.y + "," + this.z + "]"));
	}

	/** @return the L2 distanse NOTE: 2D only!!! */
	public double distance(Point_dt p) {
		double temp = Math.pow(p.x() - this.x, 2) + Math.pow(p.y() - this.y, 2);
		return Math.sqrt(temp);
	}

	/** @return the L2 distanse NOTE: 2D only!!! */
	public double distance3D(Point_dt p) {
		double temp = Math.pow(p.x() - this.x, 2) + Math.pow(p.y() - this.y, 2) + Math.pow(p.z() - this.z, 2);
		return Math.sqrt(temp);
	}

	/** return a String: x y z (used by the save to file - write_tsin method). */
	public String toFile() {
		return ("" + this.x + " " + this.y + " " + this.z);
	}

	String toFileXY() {
		return ("" + this.x + " " + this.y);
	}

	// pointLineTest
	// ===============
	// simple geometry to make things easy!
	/** �����a----+----b������ */
	public final static int ONSEGMENT = 0;

	/**
	 * + <br>
	 * �����a---------b������
	 */
	public final static int LEFT = 1;

	/**
	 * �����a---------b������ <br>
	 * +
	 */
	public final static int RIGHT = 2;
	/** ��+��a---------b������ */
	public final static int INFRONTOFA = 3;
	/** ������a---------b���+��� */
	public final static int BEHINDB = 4;
	public final static int ERROR = 5;

	/**
	 * tests the relation between this point (as a 2D [x,y] point) and a 2D segment a,b (the Z values are ignored),
	 * returns one of the following: LEFT, RIGHT, INFRONTOFA, BEHINDB, ONSEGMENT
	 * 
	 * @param a the first point of the segment.
	 * @param b the second point of the segment.
	 * @return the value (flag) of the relation between this point and the a,b line-segment.
	 */
	public int pointLineTest(Point_dt a, Point_dt b) {

		double dx = b.x - a.x;
		double dy = b.y - a.y;
		double res = dy * (this.x - a.x) - dx * (this.y - a.y);

		if (res < 0) {
			return LEFT;
		}
		if (res > 0) {
			return RIGHT;
		}

		if (dx > 0) {
			if (this.x < a.x) {
				return INFRONTOFA;
			}
			if (b.x < this.x) {
				return BEHINDB;
			}
			return ONSEGMENT;
		}
		if (dx < 0) {
			if (this.x > a.x) {
				return INFRONTOFA;
			}
			if (b.x > this.x) {
				return BEHINDB;
			}
			return ONSEGMENT;
		}
		if (dy > 0) {
			if (this.y < a.y) {
				return INFRONTOFA;
			}
			if (b.y < this.y) {
				return BEHINDB;
			}
			return ONSEGMENT;
		}
		if (dy < 0) {
			if (this.y > a.y) {
				return INFRONTOFA;
			}
			if (b.y > this.y) {
				return BEHINDB;
			}
			return ONSEGMENT;
		}
		System.out.println("Error, pointLineTest with a=b");
		return ERROR;
	}

	boolean areCollinear(Point_dt a, Point_dt b) {
		double dx = b.x - a.x;
		double dy = b.y - a.y;
		double res = dy * (this.x - a.x) - dx * (this.y - a.y);
		return res == 0;
	}

	/*
	 * public ajSegment Bisector( ajPoint b) { double sx = (x+b.x)/2; double sy = (y+b.y)/2; double dx = b.x-x; double
	 * dy = b.y-y; ajPoint p1 = new ajPoint(sx-dy,sy+dx); ajPoint p2 = new ajPoint(sx+dy,sy-dx); return new ajSegment(
	 * p1,p2 ); }
	 */

	Point_dt circumcenter(Point_dt a, Point_dt b) {

		double u = ((a.x - b.x) * (a.x + b.x) + (a.y - b.y) * (a.y + b.y)) / 2.0f;
		double v = ((b.x - this.x) * (b.x + this.x) + (b.y - this.y) * (b.y + this.y)) / 2.0f;
		double den = (a.x - b.x) * (b.y - this.y) - (b.x - this.x) * (a.y - b.y);
		if (den == 0) {
			System.out.println("circumcenter, degenerate case");
		}
		return new Point_dt((u * (b.y - this.y) - v * (a.y - b.y)) / den, (v * (a.x - b.x) - u * (b.x - this.x)) / den);
	}

	public static Comparator<Point_dt> getComparator(int flag) {
		return new Compare(flag);
	}

	public static Comparator<Point_dt> getComparator() {
		return new Compare(0);
	}

	/**
	 * Initialize this instance as if it was just created.
	 */
	public void clean() {
		this.x = 0d;
		this.y = 0d;
		this.z = 0d;
		this.neighbours.clear();
		this.info = -1;
	}
}

class Compare implements Comparator<Point_dt> {
	private int _flag;

	public Compare(int i) {
		this._flag = i;
	}

	/** compare between two points. */
	@Override
	public int compare(Point_dt d1, Point_dt d2) {
		int ans = 0;
		if (d1 != null && d2 != null) {
			if (this._flag == 0) {
				if (d1.x > d2.x) {
					return 1;
				}
				if (d1.x < d2.x) {
					return -1;
				}
				// x1 == x2
				if (d1.y > d2.y) {
					return 1;
				}
				if (d1.y < d2.y) {
					return -1;
				}
			} else if (this._flag == 1) {
				if (d1.x > d2.x) {
					return -1;
				}
				if (d1.x < d2.x) {
					return 1;
				}
				// x1 == x2
				if (d1.y > d2.y) {
					return -1;
				}
				if (d1.y < d2.y) {
					return 1;
				}
			} else if (this._flag == 2) {
				if (d1.y > d2.y) {
					return 1;
				}
				if (d1.y < d2.y) {
					return -1;
				}
				// y1 == y2
				if (d1.x > d2.x) {
					return 1;
				}
				if (d1.x < d2.x) {
					return -1;
				}

			} else if (this._flag == 3) {
				if (d1.y > d2.y) {
					return -1;
				}
				if (d1.y < d2.y) {
					return 1;
				}
				// y1 == y2
				if (d1.x > d2.x) {
					return -1;
				}
				if (d1.x < d2.x) {
					return 1;
				}
			}
		} else {
			if (d1 == null && d2 == null) {
				return 0;
			}
			if (d1 == null && d2 != null) {
				return 1;
			}
			if (d1 != null && d2 == null) {
				return -1;
			}
		}
		return ans;
	}

	@Override
	public boolean equals(Object ob) {
		return false;
	}

}