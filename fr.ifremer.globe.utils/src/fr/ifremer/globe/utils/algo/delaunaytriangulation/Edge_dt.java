package fr.ifremer.globe.utils.algo.delaunaytriangulation;

public class Edge_dt {
	Point_dt source, target;
	double length = 0d;

	public Edge_dt() {
		this.source = null;
		this.target = null;
	}

	public void initialize(Point_dt source, Point_dt target) {
		this.source = source;
		this.target = target;
		this.length = Math.sqrt(squared_value(this.target.x() - this.source.x())
				+ squared_value(this.target.y() - this.source.y()) + squared_value(this.target.z() - this.source.z()));
	}

	public Point_dt source() {
		return this.source;
	}

	public Point_dt target() {
		return this.target;
	}

	public double length() {
		return this.length;
	}

	private double squared_value(double val) {
		return val * val;
	}

	/**
	 * Initialize this instance as if it was just created.
	 */
	public void clean() {
		this.source = null;
		this.target = null;
	}

	/**
	 * @return true if clean() ha been called.
	 */
	public boolean isCleaned() {
		return this.source == null;
	}

}
