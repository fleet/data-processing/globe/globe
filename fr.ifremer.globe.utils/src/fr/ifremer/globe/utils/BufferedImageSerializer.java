/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferDouble;
import java.awt.image.DataBufferFloat;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.MappedByteBuffer;
import java.nio.ShortBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.FileChannel;

import fr.ifremer.globe.utils.array.impl.ArrayUtils;
import fr.ifremer.globe.utils.exception.runtime.NotImplementedException;

/**
 * Utility class to serialize efficiently a BufferedImage into a file
 */
public class BufferedImageSerializer {

	/**
	 * Deserialize a BufferedImage from a file
	 */
	public static BufferedImage read(File file) throws IOException {
		MappedByteBuffer byteBuffer = null;
		BufferedImage bufferedImage = null;
		try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r")) {
			FileChannel fileChannel = randomAccessFile.getChannel();
			byteBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, file.length());
			int type = byteBuffer.getInt();
			int height = byteBuffer.getInt();
			int width = byteBuffer.getInt();
			int dataType = byteBuffer.getInt();
			bufferedImage = new BufferedImage(width, height, type);
			DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();
			switch (dataType) {
			case DataBuffer.TYPE_BYTE:
				DataBufferByte dataBufferByte = (DataBufferByte) dataBuffer;
				byteBuffer.get(dataBufferByte.getData());
				break;
			case DataBuffer.TYPE_INT:
				DataBufferInt dataBufferInt = (DataBufferInt) dataBuffer;
				byteBuffer.asIntBuffer().get(dataBufferInt.getData());
				break;
			case DataBuffer.TYPE_DOUBLE:
				DataBufferDouble dataBufferDouble = (DataBufferDouble) dataBuffer;
				byteBuffer.asDoubleBuffer().get(dataBufferDouble.getData());
				break;
			case DataBuffer.TYPE_FLOAT:
				DataBufferFloat dataBufferFloat = (DataBufferFloat) dataBuffer;
				byteBuffer.asFloatBuffer().get(dataBufferFloat.getData());
				break;
			case DataBuffer.TYPE_SHORT:
				DataBufferShort dataBufferShort = (DataBufferShort) dataBuffer;
				byteBuffer.asShortBuffer().get(dataBufferShort.getData());
				break;
			default:
				throw new NotImplementedException();
			}
		} catch (ClosedChannelException e) {
			// May append when Thread is interrupt for example
		} finally {
			ArrayUtils.closeDirectBuffer(byteBuffer);
		}

		return bufferedImage;
	}

	/**
	 * Serialize a BufferedImage into a file
	 */
	public static void write(BufferedImage bufferedImage, File file) throws IOException {
		MappedByteBuffer byteBuffer = null;
		try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw")) {
			FileChannel fileChannel = randomAccessFile.getChannel();
			DataBuffer dataBuffer = bufferedImage.getRaster().getDataBuffer();
			byteBuffer = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0,
					dataBuffer.getSize() * DataBuffer.getDataTypeSize(dataBuffer.getDataType()));
			byteBuffer.putInt(bufferedImage.getType());
			byteBuffer.putInt(bufferedImage.getHeight());
			byteBuffer.putInt(bufferedImage.getWidth());
			byteBuffer.putInt(dataBuffer.getDataType());

			switch (dataBuffer.getDataType()) {
			case DataBuffer.TYPE_BYTE:
				DataBufferByte dataBufferByte = (DataBufferByte) dataBuffer;
				byte[] bytes = dataBufferByte.getData();
				byteBuffer.put(bytes);
				break;
			case DataBuffer.TYPE_INT:
				DataBufferInt dataBufferInt = (DataBufferInt) dataBuffer;
				int[] ints = dataBufferInt.getData();
				IntBuffer intBuffer = byteBuffer.asIntBuffer();
				intBuffer.put(ints);
				break;
			case DataBuffer.TYPE_DOUBLE:
				DataBufferDouble dataBufferDouble = (DataBufferDouble) dataBuffer;
				double[] doubles = dataBufferDouble.getData();
				DoubleBuffer doubleBuffer = byteBuffer.asDoubleBuffer();
				doubleBuffer.put(doubles);
				break;
			case DataBuffer.TYPE_FLOAT:
				DataBufferFloat dataBufferFloat = (DataBufferFloat) dataBuffer;
				float[] floats = dataBufferFloat.getData();
				FloatBuffer floatsBuffer = byteBuffer.asFloatBuffer();
				floatsBuffer.put(floats);
				break;
			case DataBuffer.TYPE_SHORT:
				DataBufferShort dataBufferShort = (DataBufferShort) dataBuffer;
				short[] shorts = dataBufferShort.getData();
				ShortBuffer shortsBuffer = byteBuffer.asShortBuffer();
				shortsBuffer.put(shorts);
				break;
			default:
				throw new NotImplementedException();
			}
		} catch (ClosedChannelException e) {
			// May append when Thread is interrupt for example
		} finally {
			ArrayUtils.closeDirectBuffer(byteBuffer);
		}
	}

}