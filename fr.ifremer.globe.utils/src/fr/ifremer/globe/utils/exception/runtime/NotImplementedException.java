package fr.ifremer.globe.utils.exception.runtime;

/**
 * Exception for unintelligible interfaces methods.
 */
public class NotImplementedException extends GRuntimeException {
	public NotImplementedException() {
		super();
	}
	public NotImplementedException(String string) {
		super(string);
	}
	public NotImplementedException(String string, Throwable cause) {
		super(string,cause);
	}

	private static final long serialVersionUID = 1L;

}
