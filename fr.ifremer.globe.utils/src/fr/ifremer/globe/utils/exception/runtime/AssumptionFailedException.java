package fr.ifremer.globe.utils.exception.runtime;

/**
 * Exception thrown when the software detect that a component does not respect
 * its general contract or assumption which is mainly du to an implementation
 * error
 * */
public class AssumptionFailedException extends GRuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AssumptionFailedException(String paramString) {
		super (paramString);
	}

	public AssumptionFailedException(String paramString, Throwable paramThrowable) {
		super(paramString, paramThrowable);
	}

	public AssumptionFailedException(Throwable paramThrowable) {
		super(paramThrowable);
	}

}
