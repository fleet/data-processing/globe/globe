package fr.ifremer.globe.utils.exception.runtime;

/**
 * Exception thrown when the software detect that a component need an embedded ressource that is not found
 * */
public class RessourceNotFoundException extends GRuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RessourceNotFoundException(String paramString) {
		super (paramString);
	}

	public RessourceNotFoundException(String paramString, Throwable paramThrowable) {
		super(paramString, paramThrowable);
	}

	public RessourceNotFoundException(Throwable paramThrowable) {
		super(paramThrowable);
	}

}
