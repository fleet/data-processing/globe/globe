package fr.ifremer.globe.utils.exception.runtime;

@SuppressWarnings("serial")
public class OutOfBoundException extends AssumptionFailedException {

	public OutOfBoundException(int index, int expected) {
		super("OutOfBoundException, index "+index+" is higher than expected value ("+expected+")");
	}
	public OutOfBoundException(String message) {
		super(message);
	}
}
