package fr.ifremer.globe.utils.exception.runtime;

import org.apache.log4j.Logger;


/**
 * Base Class for Globe Runtime exception
 * */
public abstract class GRuntimeException extends RuntimeException {

	private static final Logger logger = Logger.getLogger(GRuntimeException.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 4815156263897370473L;

	/**
	 * @see RuntimeException#RuntimeException()
	 * */
	public GRuntimeException() {
		super();
		logger.error(this.getClass().getName()+this.getMessage());
	}

	/**
	 * @see RuntimeException#RuntimeException(String)
	 * */
	public GRuntimeException(String message) {
		super(message);
		logger.error(this.getClass().getName() + ":"+message);
	}
	/**
	 * @see RuntimeException#RuntimeException(String, Throwable)
	 * */
	public GRuntimeException(String message, Throwable cause) {
		super(message, cause);
		logger.error(this.getClass().getName() + ":"+message+":" +cause.getMessage());
	}
	/**
	 * @see RuntimeException#RuntimeException(Throwable)
	 * */
	public GRuntimeException(Throwable cause) {
		super(cause);
		logger.error(this.getClass().getName() + ":" +cause.getMessage());
	}

}
