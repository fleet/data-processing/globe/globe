package fr.ifremer.globe.utils.exception;

public class GThreadException extends GException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1748569506930586784L;
	public GThreadException(String paramString) {
		super(paramString);
	}

	public GThreadException(String string, Throwable e) {
		super(string,e);
	}
}
