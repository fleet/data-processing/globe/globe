package fr.ifremer.globe.utils.exception;

public class BadParameterException extends GException {

	/**
	 * @see GException#GException(String)
	 * */
	public BadParameterException(String message) {
		super(message);
	}
	/**
	 * @see GException#GException(String, Throwable)
	 * */
	public BadParameterException(String message, Throwable cause) {
		super(message, cause);
	}
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 6781653397550106908L;
}
