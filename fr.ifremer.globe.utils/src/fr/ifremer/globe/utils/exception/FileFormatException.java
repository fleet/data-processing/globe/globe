package fr.ifremer.globe.utils.exception;

public class FileFormatException extends GIOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5294535773979573406L;

	public FileFormatException(String paramString) {
		super(paramString);
	}
}
