package fr.ifremer.globe.utils.exception;

/**
 * Base class for Globe Exception
 * */
public class GUnknownException extends GException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7585831997933879812L;

	/**
	 * @see Exception#Exception()
	 * */
	public GUnknownException() {
		super();
	}

	/**
	 * @see Exception#Exception(String , Throwable )
	 * */
	public GUnknownException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @see Exception#Exception(String)
	 * */
	public GUnknownException(String message) {
		super(message);
	}
}
