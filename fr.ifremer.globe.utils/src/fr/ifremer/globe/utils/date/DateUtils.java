package fr.ifremer.globe.utils.date;

import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MILLI_OF_SECOND;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;
import static java.time.temporal.ChronoField.SECOND_OF_MINUTE;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.JulianFields;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;
import java.util.function.IntFunction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtils {

	private static final double MILLISECONDS_PER_DAY = 24.0 * 3600.0 * 1000.0;

	/** Date format pattern */
	public static final String DATE_PATTERN = "dd/MM/yyyy"; //$NON-NLS-1$

	/** Time format pattern */
	public static final String TIME_PATTERN = "HH:mm:ss.SSS"; //$NON-NLS-1$

	/** HH:mm:ss.SSS Time formatter */
	public static final DateTimeFormatter LOCAL_TIME = new DateTimeFormatterBuilder()//
			.appendValue(HOUR_OF_DAY, 2)//
			.appendLiteral(':')//
			.appendValue(MINUTE_OF_HOUR, 2)//
			.appendLiteral(':')//
			.appendValue(SECOND_OF_MINUTE, 2)//
			.toFormatter(Locale.getDefault(Locale.Category.FORMAT));
	/** HH:mm:ss.SSS Time formatter */
	public static final DateTimeFormatter LOCAL_TIME_MILLIS = new DateTimeFormatterBuilder()//
			.append(LOCAL_TIME)//
			.appendFraction(MILLI_OF_SECOND, 0, 3, true)//
			.toFormatter(Locale.getDefault(Locale.Category.FORMAT));
	/** HH:mm:ss.SSSZ Time formatter */
	public static final DateTimeFormatter LOCAL_TIME_MILLIS_Z = new DateTimeFormatterBuilder()//
			.parseCaseInsensitive()//
			.append(LOCAL_TIME_MILLIS)//
			.appendOffsetId()//
			.toFormatter(Locale.getDefault(Locale.Category.FORMAT));
	/** yyyy-MM-dd HH:mm:ss.SSS Z */
	public static final DateTimeFormatter LOCAL_DATE_TIME_MILLIS = new DateTimeFormatterBuilder()//
			.parseCaseInsensitive()//
			.append(DateTimeFormatter.ISO_LOCAL_DATE)//
			.appendLiteral('T')//
			.append(LOCAL_TIME_MILLIS)//
			.toFormatter(Locale.getDefault(Locale.Category.FORMAT));
	/** yyyy-MM-dd HH:mm:ss.SSS Z */
	public static final DateTimeFormatter LOCAL_DATE_TIME_MILLIS_Z = new DateTimeFormatterBuilder()//
			.parseCaseInsensitive()//
			.append(LOCAL_DATE_TIME_MILLIS)//
			.appendOffsetId()//
			.toFormatter(Locale.getDefault(Locale.Category.FORMAT));

	public static final String FR_DATE_FORMAT = "[dd/MM/yyyy][yyyy/MM/dd]";
	public static final String US_DATE_FORMAT = "[yyyy-MM-dd][dd-MM-yyyy]";
	public static final String TIME_FORMAT = "[HH][H]:mm:ss";

	public static final DateTimeFormatter TIME = new DateTimeFormatterBuilder().appendPattern(TIME_FORMAT)
			.optionalStart().appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true).optionalEnd().toFormatter();

	public static final DateTimeFormatter DATE_TIME_FR = new DateTimeFormatterBuilder()
			.appendPattern(FR_DATE_FORMAT + " " + TIME_FORMAT).optionalStart()
			.appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true).optionalEnd().toFormatter();
	
	public static final DateTimeFormatter DATE_TIME_US = new DateTimeFormatterBuilder()
			.appendPattern(US_DATE_FORMAT + " " + TIME_FORMAT).optionalStart()
			.appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true).optionalEnd().toFormatter();

	/** Date format pattern */
	public static final String DATE_TIME_PATTERN = DATE_PATTERN + " " + TIME_PATTERN; //$NON-NLS-1$

	/** TimeZone GMT */
	public static final TimeZone TIME_ZONE_GMT = TimeZone.getTimeZone("GMT"); //$NON-NLS-1$

	protected static Logger logger = LoggerFactory.getLogger(DateUtils.class);

	private static final Instant INSTANT_1899_12_30 = Instant.parse("1899-12-30T00:00:00Z");

	private static final SimpleDateFormat SONARSCOPE_DATE_FORMAT;
	static {
		SONARSCOPE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		SONARSCOPE_DATE_FORMAT.setTimeZone(TIME_ZONE_GMT);
	}

	/** How to format a date time of TemporalAccessor */
	public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern(TIME_PATTERN, Locale.US)
			.withZone(ZoneId.from(ZoneOffset.UTC));
	/** How to format a time of TemporalAccessor */
	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter
			.ofPattern(DATE_TIME_PATTERN, Locale.US).withZone(ZoneId.from(ZoneOffset.UTC));

	/**
	 * Parse from ISO format date (see DateTimeFormatter.ISO_INSTANT : "The ISO instant formatter that formats or parses
	 * an instant in UTC, such as '2011-12-03T10:15:30Z'.")
	 **/
	public static Instant parseIsoDate(String stringDate) {
		return Instant.from(DateTimeFormatter.ISO_INSTANT.parse(stringDate));
	}

	/**
	 * @param days since 1899-12-30 00:00:00 UTC
	 *
	 * @return {@link Instant}.
	 */
	public static Instant daysFrom1899_12_30ToInstant(double days) {
		long daysInMilliseconds = (long) (days * 24 * 60 * 60 * 1000L);
		return INSTANT_1899_12_30.plusMillis(daysInMilliseconds);
	}

	public static final Instant EPOCH_1601 = Instant.parse("1601-01-01T00:00:00Z");

	public static Date getDateWithNanosecondsFrom1601(final long timestamp) {
		// Converts to milliseconds (nanoseconds values are > Long.MAX_VALUE)
		var duration = Duration.ofMillis(Long.divideUnsigned(timestamp, 1000_000L));
		return new Date(EPOCH_1601.plus(duration).toEpochMilli());
	}

	/**
	 * Format a date
	 *
	 * @param date the date in local time zone
	 * @param pattern the format pattern
	 * @return the formatted text
	 */
	public static String formatDate(Date date, String pattern) {
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		format.setTimeZone(TIME_ZONE_GMT);
		return format.format(date);
	}

	/**
	 * @param pDate the date
	 * @return the string
	 */
	public static String getStringDate(Date pDate) {
		if (pDate != null) {
			final DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
			formatter.setTimeZone(TIME_ZONE_GMT);
			return formatter.format(pDate) + " GMT";
		} else
			return "Unknown";
	}

	/**
	 * @param pStrDate the string date
	 * @param pAcceptedFormats the array of accepted date formats
	 * @param pPosition the parse position of decoded date
	 * @return the date
	 */
	public static Date parse(String pStrDate, String[] pAcceptedFormats, ParsePosition pPosition) {
		Date date = null;
		for (int i = 0; i < pAcceptedFormats.length && date == null; i++) {
			final DateFormat formatter = new SimpleDateFormat(pAcceptedFormats[i]);
			formatter.setTimeZone(TIME_ZONE_GMT);
			formatter.setLenient(false);
			date = formatter.parse(pStrDate, pPosition);
		}
		return date;
	}

	/**
	 * @param pStrDate the string date
	 * @param pAcceptedFormats the array of accepted date formats
	 * @return the date
	 */
	public static String getDateFormat(String pStrDate, String[] pAcceptedFormats, ParsePosition pPosition) {
		Date date = null;
		for (int i = 0; i < pAcceptedFormats.length && date == null; i++) {
			final DateFormat formatter = new SimpleDateFormat(pAcceptedFormats[i]);
			formatter.setTimeZone(TIME_ZONE_GMT);
			formatter.setLenient(false);
			date = formatter.parse(pStrDate, pPosition);
			if (date != null)
				return pAcceptedFormats[i];
		}
		return null;
	}

	/**
	 * return Simrad date.
	 *
	 * @param pDate date (yyyyMMdd)
	 * @param pTime time since midnight in milliseconds
	 * @return the date
	 */
	public static Date getSimradDate(final long pDate, final long pTime, final long factorToMilliseconds) {
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		formatter.setTimeZone(TIME_ZONE_GMT);
		Date date = null;
		try {
			if (pDate == 0)
				date = new Date(0);
			else
				date = formatter.parse(String.valueOf(pDate));
			date = new Date(date.getTime() + pTime * factorToMilliseconds);
		} catch (ParseException ex) {
			logger.error(ex.getMessage(), ex);
		}
		return date;
	}

	/**
	 * return S7k date.
	 *
	 * @param year year
	 * @param day day
	 * @param hour hour
	 * @param minute minute
	 * @param seconds seconds
	 * @return the date
	 */
	public static Date getS7kDate(int year, int day, int hour, int minute, float seconds) {

		Calendar calendar = Calendar.getInstance();

		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.DAY_OF_YEAR, day);
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, (int) seconds);
		// calendar.set(Calendar.MILLISECOND, (int) ((seconds - Math.floor(seconds)) * 1000.0));
		calendar.set(Calendar.MILLISECOND, (int) ((seconds + 0.0005 - (int) seconds) * 1000.0));
		calendar.setTimeZone(TIME_ZONE_GMT);

		return calendar.getTime();
	}

	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
	public static final SimpleDateFormat DATE_FORMAT_1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss:SSS");
	public static final SimpleDateFormat DATE_FORMAT_2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	public static final SimpleDateFormat DATE_FORMAT_3 = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	public static final List<SimpleDateFormat> DATE_FORMATS = List.of(DATE_FORMAT, DATE_FORMAT_1, DATE_FORMAT_2,
			DATE_FORMAT_3);

	static {
		for (SimpleDateFormat format : DATE_FORMATS)
			format.setTimeZone(TIME_ZONE_GMT);
	}

	public static String formatDate(Date date) {
		if (date == null)
			return "";
		return DATE_FORMAT.format(date);
	}

	public static Date parseDate(String dateString) {
		Date date = null;
		for (SimpleDateFormat format : DATE_FORMATS)
			try {
				format.setLenient(false);
				date = format.parse(dateString.trim());
				break;
			} catch (ParseException e) {
				logger.warn(String.format("date '%s' don't match format '%s'", dateString, format.toPattern()));
			}
		return date;
	}

	/** Parse the specified String to an Instant */
	public static Optional<Instant> parseInstant(String stringDate) {
		if (stringDate != null && !stringDate.isEmpty())
			try {
				return Optional.of(Instant.parse(stringDate));
			} catch (DateTimeParseException e) {
				try {
					// old time format from SonarScope files
					return Optional.of(SONARSCOPE_DATE_FORMAT.parse(stringDate).toInstant());
				} catch (ParseException e2) {
					//  Bad format
				}
			}
		return Optional.empty();
	}

	/** Julian date for 1970/01/01 = 2 440 588 **/
	public static final long JULIAN_REFERENCE_DATE = LocalDate.ofEpochDay(0).getLong(JulianFields.JULIAN_DAY);

	// EPOCH => JULIAN

	/**
	 * @return milliseconds from the epoch of 1970-01-01T00:00:00Z.
	 */
	public static long getEpochMilliFromJulian(int julianDate, int time) {
		return (julianDate - JULIAN_REFERENCE_DATE) * 24 * 3600 * 1000 + time;
	}

	/**
	 * @return {@link Date} from Julian date/time.
	 */
	public static Date getDateFromJulian(int julianDate, int time) {
		return new Date(getEpochMilliFromJulian(julianDate, time));
	}

	/**
	 * @return {@link Instant} from Julian date/time.
	 */
	public static Instant getInstantFromJulian(int julianDate, int time) {
		return Instant.ofEpochMilli(getEpochMilliFromJulian(julianDate, time));
	}

	// JULIAN => EPOCH

	public static int[] getJulianDateTime(Instant instant) {
		return getJulianDateTime(instant.toEpochMilli());
	}

	public static int[] getJulianDateTime(Date date) {
		return getJulianDateTime(date.getTime());
	}

	public static int[] getJulianDateTime(long time) {
		int[] dateTime = getDateTime(time);
		dateTime[0] += JULIAN_REFERENCE_DATE;
		return dateTime;
	}

	/**
	 * @return time in an long array [number of days from 1/1/1970, number of milliseconds in the day]
	 */
	public static int[] getDateTime(Instant instant) {
		return getDateTime(instant.toEpochMilli());
	}

	/**
	 * @param time : milliseconds from the epoch of 1970-01-01T00:00:00Z
	 * @return time in an long array [number of days from 1/1/1970, number of milliseconds in the day]
	 */
	public static int[] getDateTime(long time) {
		int[] currentDateTime = new int[2];
		currentDateTime[0] = (int) (time / MILLISECONDS_PER_DAY);
		currentDateTime[1] = (int) (time - currentDateTime[0] * MILLISECONDS_PER_DAY);
		return currentDateTime;
	}

	/**
	 * Return time in nanosecond from a time in seconds
	 */
	public static long secondToNano(double value) {
		return (long) (value * 1_000_000_000);
	}

	/**
	 * Return time in nanosecond from a time in seconds
	 */

	public static long secondToNano(long value) {
		return value * 1_000_000_000;
	}

	/**
	 * Return time in milliseconds from a time in seconds
	 */
	public static long secondToMilli(double value) {
		return (long) (value * 1_000);
	}

	/**
	 * Return time in nanosecond from a time in milliseconds
	 */
	public static long milliSecondToNano(long value) {
		return value * 1_000_000;
	}

	/**
	 * Return time in millisecond from a time in seconds
	 */
	public static double milliSecondToSeconds(long value) {
		return (double) value / 1_000;
	}

	/**
	 * Return time in millisecond from a time in nano seconds
	 */
	public static long nanoSecondToMilli(long value) {
		return Long.divideUnsigned(value, 1_000_000);
	}

	/**
	 * @return the UTC date & time from an {@link Instant}.
	 */
	public static String getDateTimeString(Instant instant) {
		if (!Instant.MIN.equals(instant) && !Instant.MAX.equals(instant))
			return DateUtils.getDateString(instant) + " " + DateUtils.getTimeString(instant);
		return "";
	}

	/**
	 * @return the UTC time from an {@link Instant}.
	 */
	public static String getTimeString(Instant instant) {
		return LocalTime.ofInstant(instant, ZoneOffset.UTC).toString();
	}

	/**
	 * @return the UTC date from an {@link Instant}.
	 */
	public static String getDateString(Instant instant) {
		return LocalDate.ofInstant(instant, ZoneOffset.UTC).toString();
	}

	/**
	 * @return the epoch time in nanos from an {@link Instant}.
	 */
	public static long instantToEpochNano(Instant instant) {
		return Instant.EPOCH.until(instant, ChronoUnit.NANOS);
	}

	/**
	 * @return the instant from a epoch time in nanos.
	 */
	public static Instant epochNanoToInstant(long value) {
		return Instant.ofEpochSecond(0, value);
	}

	/**
	 * @return true when from <= instant <= to
	 */
	public static boolean between(Instant instant, Instant from, Instant to) {
		return after(instant, from) && before(instant, to);
	}

	/**
	 * @return true when instant <= limit
	 */
	public static boolean before(Instant instant, Instant limit) {
		return instant.equals(limit) || instant.isBefore(limit);
	}

	/**
	 * @return true when instant >= limit
	 */
	public static boolean after(Instant instant, Instant limit) {
		return instant.equals(limit) || instant.isAfter(limit);
	}

	/**
	 * Searches the index of the closest Instant in the list of supplier for the specified time using the binary search
	 * algorithm (dicotomic search). <br>
	 * This algorithm is inspired by Collections.binarySearch <br>
	 * 
	 * @param expectedTime intant to search
	 * @param instantCount nb of Instant supplier is able to give
	 * @param instantSupplier
	 * @return the index of expectedTime in the list of supplier Instant. returns negative index in case of error
	 */
	public static int binarySearch(Instant expectedTime, int instantCount, IntFunction<Instant> instantSupplier) {
		int low = 0;
		int high = instantCount - 1;

		if (before(expectedTime, instantSupplier.apply(low)))
			return low;
		if (after(expectedTime, instantSupplier.apply(high)))
			return high;

		while (low <= high) {
			int mid = (low + high) >>> 1;
			Instant midTime = instantSupplier.apply(mid);
			if (midTime != null) {
				if (expectedTime.equals(midTime))
					return mid; // Found !
				if (expectedTime.isAfter(midTime))
					low = mid + 1; // Later
				else
					high = mid - 1; // Early
			} else
				return Integer.MIN_VALUE;
		}

		// Not found exactly. Return closest
		Instant lowTime = instantSupplier.apply(low);
		Instant highTime = instantSupplier.apply(high);
		return Math.abs(Duration.between(expectedTime, lowTime).toMillis()) < Math
				.abs(Duration.between(expectedTime, highTime).toMillis()) ? low : high;
	}
}
