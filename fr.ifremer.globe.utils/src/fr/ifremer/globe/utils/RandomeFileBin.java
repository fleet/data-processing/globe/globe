package fr.ifremer.globe.utils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.utils.exception.GIOException;


/**
 * A class opening, closing and deleting a set of RandomFiles files
 * */
public class RandomeFileBin {

	/** Logger. */
	private static final Logger logger = LoggerFactory.getLogger(RandomeFileBin.class);

	List<RandomAccessFile> files=new LinkedList<>();
	public RandomeFileBin( ) {
		super();

	}

	/**
	 * open the given file  
	 * @return 
	 * @throws IOException 
	 * */
	public RandomAccessFile openFile(File file,String mode) throws IOException
	{
		RandomAccessFile io=new RandomAccessFile(file, mode);
		this.files.add(io);
		return  io;
	}

	/**
	 * close all opened files, if an error occurs try to continue and after that throw an exception
	 * */
	public void cleanup() throws  GIOException
	{		
		boolean error=false;
		String message="";
		for(RandomAccessFile f:this.files)
		{
			try{
				f.close();
			} catch (IOException e)
			{
				error=true;
				message += '\n' + e.getMessage();
			}
		}
		this.files.clear();
		if(error)
		{
			throw new GIOException(message);
		}
	}

	/**
	 * close all opened files, if an error occurs try to continue and after that
	 * log errors
	 * */
	public void cleanupQuietly() {
		try {
			cleanup();
		} catch (GIOException e) {
			logger.warn("Error while closing files", e);
		}
	}
}
