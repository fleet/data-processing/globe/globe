/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.osgi;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

/**
 * Osgi and Eclipse Utility class.
 */
public class OsgiUtils {

	/**
	 * Constructor
	 */
	private OsgiUtils() {
	}

	/**
	 * @return A {@code Bundle} for the specified bundle class or {@code null} if the specified class was not defined by
	 *         a bundle class loader.
	 */
	public static Bundle getBundle(final Class<?> classFromBundle) {
		return FrameworkUtil.getBundle(classFromBundle);
	}

	/**
	 * @return The symbolic name of the bundle for the specified bundle class or empty String if the specified class was
	 *         not defined by a bundle class loader.
	 */
	public static String getBundleName(final Class<?> classFromBundle) {
		Bundle bundle = getBundle(classFromBundle);
		return bundle != null ? bundle.getSymbolicName() : "";
	}

	/**
	 * @return A {@code Bundle} for the specified bundle object or {@code null} if the specified object was not defined
	 *         by a bundle class loader.
	 */
	public static Bundle getBundle(final Object object) {
		return object != null ? FrameworkUtil.getBundle(object.getClass()) : null;
	}

	/**
	 * @return The symbolic name of the bundle for the specified bundle object or empty String if the specified object
	 *         was not defined by a bundle class loader.
	 */
	public static String getBundleName(final Object object) {
		Bundle bundle = getBundle(object);
		return bundle != null ? bundle.getSymbolicName() : "";
	}

	/**
	 * @return A {@code BundleContext} for the specified bundle class or {@code null} if the specified class was not
	 *         defined by a bundle class loader.
	 */
	public static BundleContext getBundleContext(final Class<?> classFromBundle) {
		return getBundle(classFromBundle).getBundleContext();
	}

	/**
	 * @return A {@code BundleContext} for the specified bundle class or {@code null} if the specified class was not
	 *         defined by a bundle class loader.
	 */
	public static BundleContext getBundleContext(final Object object) {
		return object != null ? getBundleContext(object.getClass()) : null;
	}

	/**
	 * @return the Osgi Service
	 */
	public static <S> S getService(Class<S> clazz) {
		return getService(getBundleContext(OsgiUtils.class), clazz);
	}

	/**
	 * @return the Osgi Service
	 */
	public static <S> S getService(BundleContext bundleContext, Class<S> clazz) {
		ServiceReference<S> reference = bundleContext.getServiceReference(clazz);
		return reference == null //
				? EclipseContextFactory.getServiceContext(bundleContext).get(clazz) // Eclipse service
				: bundleContext.getService(reference); // Osgi Service
	}

	/**
	 * Obtain an instance of the specified class and set the result in the context
	 */
	public static <S> S make(Class<S> clazz, IEclipseContext context) {
		S result = ContextInjectionFactory.make(clazz, context);
		context.set(clazz, result);
		return result;
	}

	/**
	 * Obtain an instance of the specified class and set the result in the context for one of its facets
	 */
	public static <F, S extends F> F make(Class<S> clazz, Class<F> facet, IEclipseContext context) {
		F result = ContextInjectionFactory.make(clazz, context);
		context.set(facet, result);
		return result;
	}

}
