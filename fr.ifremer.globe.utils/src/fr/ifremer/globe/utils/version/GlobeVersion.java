package fr.ifremer.globe.utils.version;

public class GlobeVersion {

	// keep version here to be accessible from converter
	public static final String version = "2.5.15";

}
