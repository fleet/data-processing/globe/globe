/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.map;

/**
 * An entry that binds a typed key and a value. These entries can be placed into a {@link TypedMap} or any other type of
 * object with typed values.
 *
 * @param <A> The type of the key and value in this entry.
 */
public final class TypedEntry<A> {
	private final TypedKey<A> key;
	private final A value;

	/**
	 * Constructor
	 */
	public TypedEntry(TypedKey<A> key, A value) {
		this.key = key;
		this.value = value;
	}

	/** @return the key part of this entry. */
	public TypedKey<A> key() {
		return key;
	}

	/** @return the value part of this entry. */
	public A value() {
		return value;
	}

	/**
	 * Bind this key to a value.
	 *
	 * @param value The value to bind this key to.
	 * @return A bound value.
	 * @throws ClassCastException - if the object is not assignable to the type A.
	 */
	public TypedEntry<A> bindObject(Object object) {
		return key.bindValue(valueClass().cast(object));
	}

	/** @return the value part of this entry. */
	@SuppressWarnings("unchecked")
	public Class<A> valueClass() {
		return (Class<A>) value.getClass();
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return key.hashCode();
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		var other = (TypedEntry<?>) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}
}
