/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.map;

/**
 * A TypedKey is a key that can be used to get and put values in a {@link TypedMap} or any object with typed keys. This
 * class uses reference equality for comparisons, so each new instance is different key.
 */
public final class TypedKey<A> {

	/** Name of the key */
	private final String name;

	/**
	 * Constructor
	 */
	public TypedKey(String name) {
		this.name = name;
	}

	/**
	 * Bind this key to a value.
	 *
	 * @param value The value to bind this key to.
	 * @return A bound value.
	 */
	public TypedEntry<A> bindValue(A value) {
		return new TypedEntry<>(this, value);
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return "TypedKey [name=" + name + "]";
	}

	/**
	 * @return the {@link #name}
	 */
	public String getName() {
		return name;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return name.hashCode();
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		var other = (TypedKey<?>) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
