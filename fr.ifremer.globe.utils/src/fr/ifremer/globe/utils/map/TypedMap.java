/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.map;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A TypedMap is a map containing typed values. Each entry is associated with a {@link TypedKey} that can be used to
 * look up the value. A <code>TypedKey</code> also defines the type of the value, e.g. a
 * <code>TypedKey&lt;String&gt;</code> would be associated with a <code>String
 * </code> value.
 */
public final class TypedMap {

	private final Set<TypedEntry<?>> entries;

	/**
	 * Constructor
	 */
	public TypedMap() {
		entries = new HashSet<>();
	}

	/**
	 * Get a value from the map, returning an empty {@link Optional} if it is not present.
	 *
	 * @param key The key for the value to retrieve.
	 * @param <A> The type of value to retrieve.
	 * @return An <code>Optional</code>, with the value present if it is in the map.
	 */
	@SuppressWarnings("unchecked") // Cast error is not possible
	public <A> Optional<A> get(TypedKey<A> key) {
		for (var entry : entries) {
			if (entry.key().equals(key))
				return Optional.of((A) entry.value());
		}
		return Optional.empty();
	}

	/**
	 * Check if the map contains a value with the given key.
	 *
	 * @param key The key to check for.
	 * @return True if the value is present, false otherwise.
	 */
	public boolean containsKey(TypedKey<?> key) {
		for (var entry : entries) {
			if (entry.key().equals(key))
				return true;
		}
		return false;
	}

	/**
	 * Update the map with the given key and value.
	 *
	 * @param entryToPut The entry to put.
	 * @param <A> The type of value.
	 */
	public <A> void put(TypedEntry<A> entryToPut) {
		remove(entryToPut.key());
		entries.add(entryToPut);
	}

	/**
	 * Removes a key from the map.
	 *
	 * @param keyToRemove The key to remove.
	 */
	public void remove(TypedKey<?> keyToRemove) {
		for (var entry : entries) {
			if (entry.key().equals(keyToRemove)) {
				entries.remove(entry);
				return;
			}
		}
	}

	/**
	 * Returns a {@link Set} view of the mappings contained in this map.
	 */
	public Map<String, Object> toMap() {
		return entries.stream().collect(Collectors.toMap(entry -> entry.key().getName(), TypedEntry::value));
	}

	/**
	 * Returns a {@link Set} view of the mappings contained in this map.
	 */
	public Set<TypedEntry<?>> entrySet() {
		return Set.copyOf(entries); // Avoid Concurrent modification
	}

	/**
	 * Returns a {@link Set} view of the mappings contained in this map.
	 */
	public Set<String> keySet() {
		return entries.stream().map(TypedEntry::key).map(TypedKey::getName).collect(Collectors.toSet());
	}

	/**
	 * @return {@code true} if this map contains no elements
	 */
	public boolean isEmpty() {
		return entries.isEmpty();
	}

	/**
	 * Returns the number of elements in this set (its cardinality).
	 */
	public int size() {
		return entries.size();
	}

	/**
	 * @return a TypedMap filled with specified entries
	 */
	public static TypedMap of(TypedEntry<?>... entries) {
		TypedMap result = new TypedMap();
		Arrays.stream(entries).forEach(result::put);
		return result;
	}
}
