/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.pool;

import java.util.List;

/**
 * Pool of objects.
 * 
 * @author Apside
 *
 */
public interface IObjectPool<T> {

	/**
	 * Obtains an instance from this pool.
	 */
	T borrowObject();

	/**
	 * Return an instance to the pool.
	 */
	void returnObject(T object);

	/**
	 * Return an instance to the pool.
	 */
	void returnObject(List<T> objects);

	/**
	 * Return the number of instances currently idle in this pool.
	 */
	int getNumIdle();

	/**
	 * Return the number of instances currently borrowed from this pool.
	 */
	int getNumActive();

	/**
	 * Clears any objects sitting idle in the pool
	 */
	void reset();


}
