/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.pool;

import java.util.function.Consumer;
import java.util.function.Supplier;

import fr.ifremer.globe.utils.osgi.OsgiUtils;

/**
 * Factory of {@link IObjectPool}.
 * 
 * @author Apside
 *
 */
public interface IObjectPoolFactory {

	/**
	 * Returns the service implementation
	 */
	static IObjectPoolFactory grab() {
		return OsgiUtils.getService(IObjectPoolFactory.class);
	}

	/**
	 * Create a {@link IObjectPool} able to contain some object of <T>. <br>
	 * CAUTION : the resulting pool is not thread safe.
	 * 
	 * @param capacity      Pool capacity
	 * @param objectFactory Factory of object T
	 * @param objectCleaner cleaner of object T
	 * @return the instance of {@link IObjectPool}
	 */
	<T> IObjectPool<T> makeObjectPool(int capacity, Supplier<T> objectFactory, Consumer<T> objectCleaner);
}
