/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.pool.impl;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import fr.ifremer.globe.utils.pool.IObjectPool;

/**
 * Simple pool od objects.<br>
 * CAUTION : Not thread safe.
 */
public class SimpleObjectPool<T> implements IObjectPool<T> {

	/**
	 * Real pool of objects.
	 */
	protected Deque<T> pool;
	/**
	 * Factory of object.
	 */
	protected Supplier<T> objectFactory;
	/**
	 * Cleaner of object.
	 */
	protected Consumer<T> objectCleaner;
	/**
	 * Number of instances currently borrowed from this pool.
	 */
	protected int numActive = 0;
	/**
	 * Number of instances currently borrowed from this pool.
	 */
	protected int capacity;

	/**
	 * Constructor.
	 */
	public SimpleObjectPool() {
	}

	/**
	 * Initialization after instanciation.
	 */
	public void initialize(int capacity, Supplier<T> objectFactory, Consumer<T> objectCleaner) {
		this.capacity = capacity;
		this.objectFactory = objectFactory;
		this.objectCleaner = objectCleaner;
		this.pool = new ArrayDeque<T>(capacity);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.pool.IObjectPool#borrowObject()
	 */
	@Override
	public T borrowObject() {
		T result = this.pool.pollLast();
		if (result == null) {
			result = this.objectFactory.get();
		}
		this.numActive++;
		return result;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.pool.IObjectPool#returnObject(java.lang.Object)
	 */
	@Override
	public void returnObject(T object) {
		if (object != null) {
			this.objectCleaner.accept(object);
			this.pool.offer(object);
			this.numActive--;
		}
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.pool.IObjectPool#returnObject(java.util.List)
	 */
	@Override
	public void returnObject(List<T> objects) {
		if (objects != null) {
			for (T object : objects) {
				returnObject(object);
			}
		}

	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.pool.IObjectPool#getNumIdle()
	 */
	@Override
	public int getNumIdle() {
		return this.pool.size();
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.pool.IObjectPool#getNumActive()
	 */
	@Override
	public int getNumActive() {
		return this.numActive;
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.pool.IObjectPool#reset()
	 */
	@Override
	public void reset() {
		this.pool = new ArrayDeque<T>(this.capacity);
	}

}
