/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.pool.impl;

import java.util.function.Consumer;
import java.util.function.Supplier;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.utils.pool.IObjectPool;
import fr.ifremer.globe.utils.pool.IObjectPoolFactory;

/**
 * Implementation of {@link IObjectPoolFactory}
 */
@Component(name = "globe_utils_object_pool_factory", service = IObjectPoolFactory.class)
public class ObjectPoolFactory implements IObjectPoolFactory {

	/**
	 * Constructor.
	 */
	public ObjectPoolFactory() {
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.utils.pool.IObjectPoolFactory#makeObjectPool(int,
	 *      java.util.function.Supplier, java.util.function.Consumer)
	 */
	@Override
	public <T> IObjectPool<T> makeObjectPool(int capacity, Supplier<T> objectFactory, Consumer<T> objectCleaner) {
		SimpleObjectPool<T> result = new SimpleObjectPool<T>();
		result.initialize(capacity, objectFactory, objectCleaner);
		return result;
	}
}
