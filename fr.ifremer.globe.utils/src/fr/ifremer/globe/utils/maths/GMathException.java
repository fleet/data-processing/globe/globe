package fr.ifremer.globe.utils.maths;

import fr.ifremer.globe.utils.exception.GException;

/**
 * Exception occured during math Calculation
 * */
public class GMathException extends GException{
	/**
	 * default serial 
	 */
	private static final long serialVersionUID = 1L;
	
	GMathException()
	{
		super();
	}
	GMathException(String message)
	{
		super(message);
		
	}
	GMathException(String message,Throwable cause)
	{
		super(message, cause);
	}
}
