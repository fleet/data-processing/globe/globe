package fr.ifremer.globe.utils.maths;

import org.ejml.data.FixedMatrix3x3_64F;
/**
 * A simple Matrix 3x3
 * data are stored in a row column manner
 * aXY is the value of row = X and column = Y.
 *
 * */
public abstract class AbstractMatrix3x3D {
	// we use ejml to implements computation 
	FixedMatrix3x3_64F imp;
	public AbstractMatrix3x3D() {
		imp=new FixedMatrix3x3_64F(1,0,0,0,1,0,0,0,1);
	}
	
	AbstractMatrix3x3D(FixedMatrix3x3_64F imp)
	{
		this.imp=imp.copy();
	}
	
	/**
	 * Update matrix content with the set of parameters
	 * <p> aXY is the value of row = X and column = Y.
	 * this method is protected, in case o {@link RotationMatrix3x3D} only the {@link MatrixBuilder} is allowed to set the parameters
	 * */
	protected void set(double a11,double a12,double a13,
            double a21,double a22,double a23,
            double a31,double a32,double a33)
	{
		imp.a11=a11;
		imp.a12= a12;
		imp.a13= a13;
		imp.a21= a21;
		imp.a22= a22;
		imp.a23= a23;
		imp.a31 =a31;
		imp.a32= a32;
		imp.a33= a33;
	}
}
