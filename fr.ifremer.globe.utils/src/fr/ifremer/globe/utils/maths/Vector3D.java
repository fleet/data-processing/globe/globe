package fr.ifremer.globe.utils.maths;

import org.ejml.data.FixedMatrix3_64F;

/**
 * Vector 3 implementation
 * */
public class Vector3D {
	//the matrix implementation, only accessible in package
	protected FixedMatrix3_64F imp;
	/**
	 * create an empty vector3 filled with zero values
	 * */
	public Vector3D() {
		imp=new FixedMatrix3_64F();
	}
	
	/**
	 * create a vector with x,y,z values
	 * */
	public Vector3D(double x, double y, double z) {
		imp=new FixedMatrix3_64F(x,y,z);
	}

	

	protected Vector3D(FixedMatrix3_64F value)
	{
		imp=value.copy();
	}

	/**
	 * set all vector component with the given value
	 * @param value the value 
	 * */
	public void fill(double value)
	{
		imp.a1=imp.a2=imp.a3=0;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((imp == null) ? 0 : imp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector3D other = (Vector3D) obj;
		if (imp == null) {
			if (other.imp != null)
				return false;
		} else if (imp.a1!=other.imp.a1 || imp.a2!=other.imp.a2 || imp.a3!=other.imp.a3 )
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Vector3D ["+imp.a1+","+imp.a2+","+imp.a3+"]";
	}
	
	/** Getters **/
	
	public double getX() {
		return imp.a1;
	}
	
	public double getY() {
		return imp.a2;
	}
	
	public double getZ() {
		return imp.a3;
	}
}
