package fr.ifremer.globe.utils.maths;

import org.ejml.alg.fixed.FixedOps3;

/**
 * Class implementing available operations on {@link AbstractMatrix3x3D} and {@link Vector3D}
 * <p> to save memory footprint, the general rule is to pass the result matrix or vector as argument and leave the memory allocation to consumers
 * 
 * */
public class Ops {
	private Ops() {
		throw new IllegalStateException("Utility class");
	}
	/**
	 * <p>Performs the following operation:<br>
	 * <br>
	 * c = a * b <br>
	 * <br>
	 * c<sub>ij</sub> = &sum;<sub>k=1:n</sub> { a<sub>ik</sub> * b<sub>kj</sub>}
	 * </p>
	 *
	 * The product of two rotation matrix is a rotation matrix
	 * @param a The left matrix in the multiplication operation. Not modified.
	 * @param b The right matrix in the multiplication operation. Not modified.
	 * @param c Where the results of the operation are stored. Modified.
	 */
	public static void mult(RotationMatrix3x3D a,RotationMatrix3x3D b, RotationMatrix3x3D out)
	{
		//we rely on ejml implementation
		FixedOps3.mult(a.imp,b.imp,out.imp);
	}


	/**
	 * <p> Retrieve the associated Euler angles from the given rotation matrix</p>
	 * see https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions#Quaternion_%E2%86%92_Euler_angles_(z-y%E2%80%B2-x%E2%80%B3_intrinsic)
	 * <p>Note that by convention, angle for the pitch angle cover
	 * [0,pi] (see https://en.wikipedia.org/wiki/Euler_angles#Tait.E2.80.93Bryan_angles)
	 * @return a vector3D filled with rotation angles around with X,Y,Z axes, i.e vector3D(roll,pitch,heading or yaw)
	 * 
	 * */
	public static Vector3D toEulerAngle(RotationMatrix3x3D matrix)
	{
		double qr=0.5*Math.sqrt(1+matrix.imp.a11+matrix.imp.a22+matrix.imp.a33);
		double qi=(matrix.imp.a32-matrix.imp.a23)/(4*qr);
		double qj=(matrix.imp.a13-matrix.imp.a31)/(4*qr);
		double qk=(matrix.imp.a21-matrix.imp.a12)/(4*qr);
		
		
		double roll=Math.atan2(2*(qr*qi+qj*qk),1-2*(qi*qi+qj*qj));
		double pitch=Math.asin(2*(qr*qj-qk*qi));
		double heading=Math.atan2(2*(qr*qk+qi*qj),1-2*(qj*qj+qk*qk));
		return new Vector3D(roll,pitch,heading);
	}


    /**
     * <p>Performs matrix to vector multiplication:<br>
     * <br>
     * c = a * b <br>
     * <br>
     * c<sub>i</sub> = &sum;<sub>k=1:n</sub> { a<sub>ik</sub> * b<sub>k</sub>}
     * </p>
     *
     * @param a The left matrix in the multiplication operation. Not modified.
     * @param b The right vector in the multiplication operation. Not modified.
     * @param c Where the results of the operation are stored. Modified.
     */
    public static void mult( AbstractMatrix3x3D a , Vector3D b , Vector3D out) {
    	FixedOps3.mult(a.imp,b.imp,out.imp);
    }

    /**
     * Inverts matrix 'a' using minor matrices and stores the results in 'inv'.  Scaling is applied to improve
     * stability against overflow and underflow.
     *
     * @param a Input matrix. Not modified.
     * @param out Inverted output matrix.  Modified.
     * @throws GMathException if it failed
     */
    public static void inverse(Matrix3x3D a, Matrix3x3D out) throws GMathException
    {
    	if(!FixedOps3.invert(a.imp, out.imp))
    	{
    		throw new GMathException();
    	}
    }
    /**
     * Inverts a rotation matrix 'a' stores the results in 'inv'.  
     * A rotation matrix is a special cases of matrix since A<sup>-1</sup> = A<sup>T</sup>
     * 
     * @param a Input matrix. Not modified.
     * @param out Inverted output matrix.  Modified.
     */
    public static void inverse(RotationMatrix3x3D a, RotationMatrix3x3D out) 
    {
    	FixedOps3.transpose(a.imp, out.imp);
    }
    
    /**
     * <p>Performs the vector dot product:<br>
     * <br>
     * c = a * b <br>
     * <br>
     * c = &sum;<sub>k=1:n</sub> { b<sub>k</sub> * a<sub>k</sub> }
     * </p>
     *
     * @param a The left vector in the  operation. Not modified.
     * @param b The right vector in the  operation. Not modified.
     * @return The dot product
     */
    public static double dot(Vector3D a, Vector3D b)
    {
    	return FixedOps3.dot(a.imp, b.imp);
    }
    
    /**
     * <p>Performs the vector cross product : <p>
     * <b> c = a x b  </b>  ( or  <b>c= a^b</b> in french notation )
     * </p>
     *
     * @param a The left vector in the operation. Not modified.
     * @param b The right vector in the operation. Not modified.
     * @param out the result
     */
    public static void cross(Vector3D a, Vector3D b, Vector3D out)
    {
    	out.imp.a1=a.imp.a2*b.imp.a3-a.imp.a3*b.imp.a2;
    	out.imp.a2=a.imp.a3*b.imp.a1-a.imp.a1*b.imp.a3;
    	out.imp.a3=a.imp.a1*b.imp.a2-a.imp.a2*b.imp.a1;
    }
    
    /**
     * <p>Performs a vector addition  : <p>
     * <b> c = a + b  </b>
     * </p>
     *
     * @param a The left vector in the multiplication operation. Not modified.
     * @param b The right vector in the multiplication operation. Not modified.
     * @param out the result
     */
    public static void add(Vector3D a, Vector3D b, Vector3D out)
    {
    	out.imp.a1=a.imp.a1+b.imp.a1;
    	out.imp.a2=a.imp.a2+b.imp.a2;
    	out.imp.a3=a.imp.a3+b.imp.a3;
    }
   
    /**
     * <p>Performs a vector substraction : <p>
     * <b> c = a - b  </b>
     * </p>
     *
     * @param a The left vector in the multiplication operation. Not modified.
     * @param b The right vector in the multiplication operation. Not modified.
     * @param out the result
     */
    public static void substract(Vector3D a, Vector3D b, Vector3D out)
    {
    	out.imp.a1=a.imp.a1-b.imp.a1;
    	out.imp.a2=a.imp.a2-b.imp.a2;
    	out.imp.a3=a.imp.a3-b.imp.a3;
    }
    
    /**
     * <p>Performs a vector multiplication by a scalar: <p>
     * <b> c = a * b  </b>
     * </p>
     *
     * @param a The left vector in the multiplication operation. Not modified.
     * @param b The scalar in the multiplication operation. Not modified.
     * @param out the result
     */
    public static void mul(Vector3D a, double b, Vector3D out)
    {
    	out.imp.a1=a.imp.a1*b;
    	out.imp.a2=a.imp.a2*b;
    	out.imp.a3=a.imp.a2*b;
    }

	/***
	 * Normalize vector
	 * 
	 * @param a the input vector
	 * 
	 */
	public static void normalize(Vector3D a, Vector3D out) {
		double scale = dot(a, a);
		if (scale == 0) {
			out.fill(0);
		} else {
			mul(a, 1 / scale, out);
		}
	}

}
