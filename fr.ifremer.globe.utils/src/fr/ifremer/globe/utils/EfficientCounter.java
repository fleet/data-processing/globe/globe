package fr.ifremer.globe.utils;

import java.util.HashMap;
import java.util.Set;
import java.util.function.BiConsumer;

public class EfficientCounter<K> {
	HashMap<K, MutableInteger> values=new HashMap<>();
	
	/**
     * Returns a {@link Set} view of the keys contained in this map.
     * The set is backed by the map, so changes to the map are
     * reflected in the set, and vice-versa.  
     * see {@link HashMap#keySet()}
     * */
	public Set<K> getKeys()
	{
		return values.keySet();
	}
	/**
	 * see {@link HashMap#forEach(BiConsumer)}
	 * */
	public void forEach(BiConsumer<K, MutableInteger> action)
	{
		values.forEach(action);
	}
	
	/**
     * add an entry in the value set, increment the counter associated with the key value
     * @param key the key whose associated value is to be incremented
	 * */
	public void put(K key)
	{
		MutableInteger initValue = new MutableInteger(1);
		MutableInteger oldValue = values.put(key, initValue);
		if(oldValue != null){
			initValue.set(oldValue.get() + 1);
		}
	}

	/**
	 * return the count of values associated with the given key
	 * @param key the key whose associated counter value is to be returned
	 * */
	public int getCount(K key)
	{
		MutableInteger count=values.get(key);
		if(count==null)
			return 0;
		else return count.get();
	}
}
