package fr.ifremer.globe.utils.sounders;

/**
 * The description of a given sounder
 * */
public class SounderDescription
{
	public enum Constructor{
		Kongsberg,
		Reson, 
		Unknown,
	}
	private Constructor company=Constructor.Unknown;
	
	public Constructor getCompany() {
		return company;
	}
	static final int Invalid=-1;
	final public boolean isIdValid(int id)
	{
		return id!=Invalid;
	}
	
	public SounderDescription(Constructor constructor, int constructorSounderID, short genericId, String name) {
		super();
		this.company=constructor;
		this.constructorModelId = constructorSounderID;
		this.genericId = genericId;
		this.name = name;
	}
	/**
	 * id of the sounder given by the constructor (modelID)
	 * */
	public int getConstructorModelID() {
		return constructorModelId;
	}
	/**
	 * id given by CARAIBES and mbg, constructor blind
	 * */
	public short getGenericId() {
		return genericId;
	}
	/**
	 * descriptive name of the sounder
	 * */
	public String getName() {
		return name;
	}
	int constructorModelId; // id of the sounder given by the constructor (modelID)
	short genericId; //id given by CARAIBES and mbg
	String name; // name of the sounder displayed to the user
}
