import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.ifremer.globe.utils.array.impl.HugeArrayTests;
import fr.ifremer.globe.utils.array.impl.LargeArrayTests;
import fr.ifremer.globe.utils.array.impl.TinyArrayTests;

/**
 * Test suite for maven , declared test will be launched while compiling with
 * command mvn integration-test
 * 
 * */
@RunWith(Suite.class)
@SuiteClasses({ TinyArrayTests.class, LargeArrayTests.class, HugeArrayTests.class })
public class MvnTestSuite {

}
