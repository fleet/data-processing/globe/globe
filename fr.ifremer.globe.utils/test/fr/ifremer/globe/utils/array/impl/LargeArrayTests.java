/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array.impl;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.array.IBooleanArray;
import fr.ifremer.globe.utils.array.IByteArray;
import fr.ifremer.globe.utils.array.ICharArray;
import fr.ifremer.globe.utils.array.IDoubleArray;
import fr.ifremer.globe.utils.array.IFloatArray;
import fr.ifremer.globe.utils.array.ILongArray;
import fr.ifremer.globe.utils.array.IShortArray;

/**
 * Test of LargeArray.
 * 
 * @author Apside
 *
 */
public class LargeArrayTests {

	/** Ressource à libérer après chaque test. */
	protected Closeable closeable;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		IOUtils.closeQuietly(this.closeable);
	}

	/**
	 * Test du Boolean LargeArray.
	 */
	@Test
	public void testBooleanLargeArray() throws IOException {
		int elementCount = 10000;
		ArrayFactory arrayFactory = new ArrayFactory();
		IBooleanArray array = arrayFactory.makeBooleanArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof LargeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		Boolean value = true;
		// first Boolean of segment
		int firstElementIndexInSegment = 0;
		array.putBoolean(firstElementIndexInSegment, value);
		value = !value;
		int lastElementIndexInSegment = elementCount - 1; // last Boolean of
															// segment
		array.putBoolean(lastElementIndexInSegment, value);

		value = true;
		// first Boolean of segment
		Assert.assertEquals(value, array.getBoolean(firstElementIndexInSegment));
		value = !value;
		Assert.assertEquals(value, array.getBoolean(lastElementIndexInSegment));
	}

	/**
	 * Test du Byte LargeArray.
	 */
	@Test
	public void testByteLargeArray() throws IOException {
		int elementCount = 10000;
		ArrayFactory arrayFactory = new ArrayFactory();
		IByteArray array = arrayFactory.makeByteArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof LargeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		byte value = 3;
		// first Byte of segment
		int firstElementIndexInSegment = 0;
		array.putByte(firstElementIndexInSegment, value);
		value += 3;
		int lastElementIndexInSegment = elementCount - 1; // last Byte of
															// segment
		array.putByte(lastElementIndexInSegment, value);

		value = 3;
		// first Byte of segment
		Assert.assertEquals(value, array.getByte(firstElementIndexInSegment));
		value += 3;
		Assert.assertEquals(value, array.getByte(lastElementIndexInSegment));
	}

	/**
	 * Test du Char LargeArray.
	 */
	@Test
	public void testCharLargeArray() throws IOException {
		int elementCount = 10000;
		ArrayFactory arrayFactory = new ArrayFactory();
		ICharArray array = arrayFactory.makeCharArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof LargeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		char value = 3;
		// first Char of segment
		int firstElementIndexInSegment = 0;
		array.putChar(firstElementIndexInSegment, value);
		value += 3;
		int lastElementIndexInSegment = elementCount - 1; // last Char of
															// segment
		array.putChar(lastElementIndexInSegment, value);

		value = 3;
		// first Char of segment
		Assert.assertEquals(value, array.getChar(firstElementIndexInSegment));
		value += 3;
		Assert.assertEquals(value, array.getChar(lastElementIndexInSegment));
	}

	/**
	 * Test du Double LargeArray.
	 */
	@Test
	public void testDoubleLargeArray() throws IOException {
		int elementCount = 10000;
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof LargeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		double value = Math.PI;
		// first double of segment
		int firstElementIndexInSegment = 0;
		array.putDouble(firstElementIndexInSegment, value);
		value += Math.PI;
		int lastElementIndexInSegment = elementCount - 1; // last double of
															// segment
		array.putDouble(lastElementIndexInSegment, value);

		value = Math.PI;
		// first double of segment
		Assert.assertEquals(value, array.getDouble(firstElementIndexInSegment), 0d);
		value += Math.PI;
		Assert.assertEquals(value, array.getDouble(lastElementIndexInSegment), 0d);
	}

	/**
	 * Test du Double two dimentional TinyArray.
	 */
	@Test
	public void testDoubleBiDimensionalTinyArray() throws IOException {
		int xCount = 100;
		int yCount = 100;
		int elementCount = xCount * yCount;
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(xCount, yCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof LargeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		double value = Math.PI;
		// first double of segment
		array.putDouble(0, 0, value);
		value += Math.PI;
		// last double of segment
		array.putDouble(xCount - 1, yCount - 1, value);

		value = Math.PI;
		// first double of segment
		Assert.assertEquals(value, array.getDouble(0, 0), 0d);
		value += Math.PI;
		Assert.assertEquals(value, array.getDouble(xCount - 1, yCount - 1), 0d);
		Assert.assertEquals(value, array.getDouble(elementCount - 1), 0d);
	}

	/**
	 * Test du Double tri dimentional TinyArray.
	 */
	@Test
	public void testDoubleTriDimensionalTinyArray() throws IOException {
		int xCount = 100;
		int yCount = 10;
		int zCount = 9;
		int elementCount = xCount * yCount * zCount;
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(xCount, yCount, zCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof LargeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		double value = Math.PI;
		// first double of segment
		array.putDouble(0, 0, 0, value);
		value += Math.PI;
		// last double of segment
		array.putDouble(xCount - 1, yCount - 1, zCount - 1, value);

		value = Math.PI;
		// first double of segment
		Assert.assertEquals(value, array.getDouble(0, 0, 0), 0d);
		Assert.assertEquals(value, array.getDouble(0), 0d);
		value += Math.PI;
		Assert.assertEquals(value, array.getDouble(xCount - 1, yCount - 1, zCount - 1), 0d);
		Assert.assertEquals(value, array.getDouble(elementCount - 1), 0d);
	}

	/**
	 * Test du Float LargeArray.
	 */
	@Test
	public void testFloatLargeArray() throws IOException {
		int elementCount = 10000;
		ArrayFactory arrayFactory = new ArrayFactory();
		IFloatArray array = arrayFactory.makeFloatArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof LargeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		float value = (float) Math.PI;
		// first double of segment
		int firstElementIndexInSegment = 0;
		array.putFloat(firstElementIndexInSegment, value);
		value += Math.PI;
		int lastElementIndexInSegment = elementCount - 1; // last double of
															// segment
		array.putFloat(lastElementIndexInSegment, value);

		value = (float) Math.PI;
		// first double of segment
		Assert.assertEquals(value, array.getFloat(firstElementIndexInSegment), 0d);
		value += Math.PI;
		Assert.assertEquals(value, array.getFloat(lastElementIndexInSegment), 0d);
	}

	/**
	 * Test du Long LargeArray.
	 */
	@Test
	public void testLongLargeArray() throws IOException {
		int elementCount = 10000;
		ArrayFactory arrayFactory = new ArrayFactory();
		ILongArray array = arrayFactory.makeLongArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof LargeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		long value = 1234l;
		// first Long of segment
		int firstElementIndexInSegment = 0;
		array.putLong(firstElementIndexInSegment, value);
		value *= 3;
		int lastElementIndexInSegment = elementCount - 1; // last Long of
															// segment
		array.putLong(lastElementIndexInSegment, value);

		value = 1234l;
		Assert.assertEquals(value, array.getLong(firstElementIndexInSegment));
		value *= 3;
		Assert.assertEquals(value, array.getLong(lastElementIndexInSegment));
	}

	/**
	 * Test du Short LargeArray.
	 */
	@Test
	public void testShortLargeArray() throws IOException {
		int elementCount = 10000;
		ArrayFactory arrayFactory = new ArrayFactory();
		IShortArray array = arrayFactory.makeShortArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof LargeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		short value = 123;
		// first Short of segment
		int firstElementIndexInSegment = 0;
		array.putShort(firstElementIndexInSegment, value);
		value *= 3;
		int lastElementIndexInSegment = elementCount - 1; // last Short of
															// segment
		array.putShort(lastElementIndexInSegment, value);

		value = 123;
		Assert.assertEquals(value, array.getShort(firstElementIndexInSegment));
		value *= 3;
		Assert.assertEquals(value, array.getShort(lastElementIndexInSegment));
	}

	/**
	 * Test du Short reduceElementCount.
	 */
	@Test
	public void testReduceElementCount() throws IOException {
		int elementCount = 10000;
		ArrayFactory arrayFactory = new ArrayFactory();
		IShortArray array = arrayFactory.makeShortArray(elementCount);
		this.closeable = array;

		Assert.assertEquals(elementCount, array.getElementCount());
		array.reduceElementCount(elementCount / 2);

		LargeArray shortLargeArray = (LargeArray) array;
		Assert.assertEquals((elementCount / 2) << shortLargeArray.getShift(), shortLargeArray.getFile().length());
	}

	/**
	 * Test cloneByteArray.
	 */
	@Test
	public void cloneByteArray() throws IOException {
		int elementCount = 10000;
		ArrayFactory arrayFactory = new ArrayFactory();
		IByteArray array = arrayFactory.makeByteArray(elementCount);
		array.setByteOrder(ByteOrder.nativeOrder());
		this.closeable = array;

		Assert.assertTrue(array instanceof LargeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		// Filling
		for (int i = 0; i < elementCount; i += 100) {
			array.putByte(i, (byte) i);
		}

		// Comparing
		try (IByteArray cloneArray = array.cloneByteArray()) {
			for (int i = 0; i < elementCount; i += 100) {
				Assert.assertEquals("Bad value at " + i, cloneArray.getByte(i), (byte) i);
			}
		}
	}

	/**
	 * Test cloneDoubleArray.
	 */
	@Test
	public void cloneDoubleArray() throws IOException {
		int elementCount = 10000;
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(elementCount);
		array.setByteOrder(ByteOrder.nativeOrder());
		this.closeable = array;

		Assert.assertTrue(array instanceof LargeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		// Filling
		double value = Math.PI;
		for (int i = 0; i < elementCount; i += 100) {
			array.putDouble(i, value);
			value += Math.PI;
		}

		// Comparing
		try (IDoubleArray cloneArray = array.cloneDoubleArray()) {
			value = Math.PI;
			for (int i = 0; i < elementCount; i += 100) {
				Assert.assertEquals("Bad value at " + i, cloneArray.getDouble(i), value, 0d);
				value += Math.PI;
			}
		}
	}

	/**
	 * Test dump.
	 */
	@Test
	public void dumpDoubleArray() throws IOException {
		int elementCount = 10000;
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(elementCount);
		array.setByteOrder(ByteOrder.nativeOrder());
		this.closeable = array;

		Assert.assertTrue(array instanceof LargeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		// Filling
		double value = Math.PI;
		for (int i = 0; i < elementCount; i += 100) {
			array.putDouble(i, value);
			value += Math.PI;
		}

		File file = File.createTempFile(getClass().getSimpleName() + "_dump_", ".tmp");
		array.dump(file);
		try (IDoubleArray copyArray = arrayFactory.openDoubleArray(file, false);) {
			copyArray.setByteOrder(ByteOrder.nativeOrder());
			value = Math.PI;
			for (int i = 0; i < elementCount; i += 100) {
				Assert.assertEquals("Bad value at " + i, copyArray.getDouble(i), value, 0d);
				value += Math.PI;
			}
		}
		file.delete();
	}

}
