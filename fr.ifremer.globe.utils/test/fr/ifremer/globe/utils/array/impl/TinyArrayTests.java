/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array.impl;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.array.IBooleanArray;
import fr.ifremer.globe.utils.array.IByteArray;
import fr.ifremer.globe.utils.array.ICharArray;
import fr.ifremer.globe.utils.array.IDoubleArray;
import fr.ifremer.globe.utils.array.IFloatArray;
import fr.ifremer.globe.utils.array.IIntArray;
import fr.ifremer.globe.utils.array.ILongArray;
import fr.ifremer.globe.utils.array.IShortArray;

/**
 * Test of TinyArray.
 * 
 * @author Apside
 *
 */
public class TinyArrayTests {

	/** Ressource à libérer après chaque test. */
	protected Closeable closeable;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		IOUtils.closeQuietly(this.closeable);
	}

	/**
	 * Test du Boolean TinyArray.
	 */
	@Test
	public void testBooleanTinyArray() throws IOException {
		int elementCount = 500;
		ArrayFactory arrayFactory = new ArrayFactory();
		IBooleanArray array = arrayFactory.makeBooleanArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof TinyArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		Boolean value = true;
		// first Boolean of segment
		int firstElementIndexInSegment = 0;
		array.putBoolean(firstElementIndexInSegment, value);
		value = !value;
		int lastElementIndexInSegment = elementCount - 1; // last Boolean of
															// segment
		array.putBoolean(lastElementIndexInSegment, value);

		value = true;
		// first Boolean of segment
		Assert.assertEquals(value, array.getBoolean(firstElementIndexInSegment));
		value = !value;
		Assert.assertEquals(value, array.getBoolean(lastElementIndexInSegment));
	}

	/**
	 * Test du Byte TinyArray.
	 */
	@Test
	public void testByteTinyArray() throws IOException {
		int elementCount = 500;
		ArrayFactory arrayFactory = new ArrayFactory();
		IByteArray array = arrayFactory.makeByteArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof TinyArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		byte value = 3;
		// first Byte of segment
		int firstElementIndexInSegment = 0;
		array.putByte(firstElementIndexInSegment, value);
		value += 3;
		int lastElementIndexInSegment = elementCount - 1; // last Byte of
															// segment
		array.putByte(lastElementIndexInSegment, value);

		value = 3;
		// first Byte of segment
		Assert.assertEquals(value, array.getByte(firstElementIndexInSegment));
		value += 3;
		Assert.assertEquals(value, array.getByte(lastElementIndexInSegment));
	}

	/**
	 * Test du Char TinyArray.
	 */
	@Test
	public void testCharTinyArray() throws IOException {
		int elementCount = 500;
		ArrayFactory arrayFactory = new ArrayFactory();
		ICharArray array = arrayFactory.makeCharArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof TinyArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		char value = 3;
		// first Char of segment
		int firstElementIndexInSegment = 0;
		array.putChar(firstElementIndexInSegment, value);
		value += 3;
		int lastElementIndexInSegment = elementCount - 1; // last Char of
															// segment
		array.putChar(lastElementIndexInSegment, value);

		value = 3;
		// first Char of segment
		Assert.assertEquals(value, array.getChar(firstElementIndexInSegment));
		value += 3;
		Assert.assertEquals(value, array.getChar(lastElementIndexInSegment));
	}

	/**
	 * Test du Double TinyArray.
	 */
	@Test
	public void testDoubleTinyArray() throws IOException {
		int elementCount = 500;
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof TinyArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		double value = Math.PI;
		// first double of segment
		int firstElementIndexInSegment = 0;
		array.putDouble(firstElementIndexInSegment, value);
		value += Math.PI;
		int lastElementIndexInSegment = elementCount - 1; // last double of
															// segment
		array.putDouble(lastElementIndexInSegment, value);

		value = Math.PI;
		// first double of segment
		Assert.assertEquals(value, array.getDouble(firstElementIndexInSegment), 0d);
		value += Math.PI;
		Assert.assertEquals(value, array.getDouble(lastElementIndexInSegment), 0d);
	}

	/**
	 * Test du Double two dimentional TinyArray.
	 */
	@Test
	public void testDoubleBiDimensionalTinyArray() throws IOException {
		int xCount = 50;
		int yCount = 10;
		int elementCount = xCount * yCount;
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(xCount, yCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof TinyArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		double value = Math.PI;
		// first double of segment
		array.putDouble(0, 0, value);
		value += Math.PI;
		// last double of segment
		array.putDouble(xCount - 1, yCount - 1, value);

		value = Math.PI;
		// first double of segment
		Assert.assertEquals(value, array.getDouble(0, 0), 0d);
		value += Math.PI;
		Assert.assertEquals(value, array.getDouble(xCount - 1, yCount - 1), 0d);
		Assert.assertEquals(value, array.getDouble(elementCount - 1), 0d);
	}

	/**
	 * Test du Double tri dimentional TinyArray.
	 */
	@Test
	public void testDoubleTriDimensionalTinyArray() throws IOException {
		int xCount = 7;
		int yCount = 10;
		int zCount = 6;
		int elementCount = xCount * yCount * zCount;
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(xCount, yCount, zCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof TinyArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		double value = Math.PI;
		// first double of segment
		array.putDouble(0, 0, 0, value);
		value += Math.PI;
		// last double of segment
		array.putDouble(xCount - 1, yCount - 1, zCount - 1, value);

		value = Math.PI;
		// first double of segment
		Assert.assertEquals(value, array.getDouble(0, 0, 0), 0d);
		Assert.assertEquals(value, array.getDouble(0), 0d);
		value += Math.PI;
		Assert.assertEquals(value, array.getDouble(xCount - 1, yCount - 1, zCount - 1), 0d);
		Assert.assertEquals(value, array.getDouble(elementCount - 1), 0d);
	}

	/**
	 * Test du Float TinyArray.
	 */
	@Test
	public void testFloatTinyArray() throws IOException {
		int elementCount = 500;
		ArrayFactory arrayFactory = new ArrayFactory();
		IFloatArray array = arrayFactory.makeFloatArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof TinyArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		float value = (float) Math.PI;
		// first double of segment
		int firstElementIndexInSegment = 0;
		array.putFloat(firstElementIndexInSegment, value);
		value += Math.PI;
		int lastElementIndexInSegment = elementCount - 1; // last double of
															// segment
		array.putFloat(lastElementIndexInSegment, value);

		value = (float) Math.PI;
		// first double of segment
		Assert.assertEquals(value, array.getFloat(firstElementIndexInSegment), 0d);
		value += Math.PI;
		Assert.assertEquals(value, array.getFloat(lastElementIndexInSegment), 0d);
	}

	/**
	 * Test du Long TinyArray.
	 */
	@Test
	public void testLongTinyArray() throws IOException {
		int elementCount = 500;
		ArrayFactory arrayFactory = new ArrayFactory();
		ILongArray array = arrayFactory.makeLongArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof TinyArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		long value = 1234l;
		// first Long of segment
		int firstElementIndexInSegment = 0;
		array.putLong(firstElementIndexInSegment, value);
		value *= 3;
		int lastElementIndexInSegment = elementCount - 1; // last Long of
															// segment
		array.putLong(lastElementIndexInSegment, value);

		value = 1234l;
		Assert.assertEquals(value, array.getLong(firstElementIndexInSegment));
		value *= 3;
		Assert.assertEquals(value, array.getLong(lastElementIndexInSegment));
	}

	/**
	 * Test du Short TinyArray.
	 */
	@Test
	public void testShortTinyArray() throws IOException {
		int elementCount = 500;
		ArrayFactory arrayFactory = new ArrayFactory();
		IShortArray array = arrayFactory.makeShortArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof TinyArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		short value = 123;
		// first Short of segment
		int firstElementIndexInSegment = 0;
		array.putShort(firstElementIndexInSegment, value);
		value *= 3;
		int lastElementIndexInSegment = elementCount - 1; // last Short of
															// segment
		array.putShort(lastElementIndexInSegment, value);

		value = 123;
		Assert.assertEquals(value, array.getShort(firstElementIndexInSegment));
		value *= 3;
		Assert.assertEquals(value, array.getShort(lastElementIndexInSegment));
	}

	/**
	 * Test reduceElementCount.
	 */
	@Test
	public void testReduceElementCount() throws IOException {
		int elementCount = 500;
		ArrayFactory arrayFactory = new ArrayFactory();
		IShortArray array = arrayFactory.makeShortArray(elementCount);
		this.closeable = array;

		Assert.assertEquals(elementCount, array.getElementCount());
		array.reduceElementCount(elementCount / 2);
		Assert.assertEquals(elementCount / 2, array.getElementCount());
	}

	/**
	 * Test asFloatBuffer.
	 */
	@Test
	public void testAsFloatBuffer() throws IOException {
		int elementCount = 100;
		ArrayFactory arrayFactory = new ArrayFactory();
		IFloatArray array = arrayFactory.makeFloatArray(elementCount);
		this.closeable = array;

		Assert.assertEquals(elementCount, array.getElementCount());
		// Filling
		for (int i = 0; i < elementCount; i++) {
			array.putFloat(i, i);
		}

		int firstExpectedFloatIndex = 10;
		int expectedFloatCount = 15;
		FloatBuffer floatBuffer = array.asFloatBuffer(firstExpectedFloatIndex, expectedFloatCount);

		for (int i = 0; i < expectedFloatCount; i++) {
			Assert.assertEquals("Bad value at " + i, floatBuffer.get(), 10 + i, 0f);
		}

		// Should raise an exception
		try {
			floatBuffer.get();
			Assert.fail("Should raise a BufferUnderflowException");
		} catch (BufferUnderflowException e) {
			// That's good !
		}

		// Really shared mémory ?
		array.putFloat(firstExpectedFloatIndex, 1234.05f);
		Assert.assertEquals(1234.05f, floatBuffer.get(firstExpectedFloatIndex), 0f);
	}

	/**
	 * Test asDoubleBuffer.
	 */
	@Test
	public void testAsDoubleBuffer() throws IOException {
		int elementCount = 100;
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(elementCount);
		this.closeable = array;

		Assert.assertEquals(elementCount, array.getElementCount());
		// Filling
		for (int i = 0; i < elementCount; i++) {
			array.putDouble(i, i);
		}

		int firstExpectedDoubleIndex = 10;
		int expectedDoubleCount = 15;
		DoubleBuffer doubleBuffer = array.asDoubleBuffer(firstExpectedDoubleIndex, expectedDoubleCount);

		for (int i = 0; i < expectedDoubleCount; i++) {
			Assert.assertEquals("Bad value at " + i, doubleBuffer.get(), 10 + i, 0f);
		}

		// Should raise an exception
		try {
			doubleBuffer.get();
			Assert.fail("Should raise a BufferUnderflowException");
		} catch (BufferUnderflowException e) {
			// That's good !
		}

		// Really shared mémory ?
		array.putDouble(firstExpectedDoubleIndex, 1234.05f);
		Assert.assertEquals(1234.05f, doubleBuffer.get(firstExpectedDoubleIndex), 0f);
	}

	/**
	 * Test asLongBuffer.
	 */
	@Test
	public void testAsLongBuffer() throws IOException {
		int elementCount = 100;
		ArrayFactory arrayFactory = new ArrayFactory();
		ILongArray array = arrayFactory.makeLongArray(elementCount);
		this.closeable = array;

		Assert.assertEquals(elementCount, array.getElementCount());
		// Filling
		for (int i = 0; i < elementCount; i++) {
			array.putLong(i, i);
		}

		int firstExpectedLongIndex = 10;
		int expectedLongCount = 15;
		LongBuffer longBuffer = array.asLongBuffer(firstExpectedLongIndex, expectedLongCount);

		for (int i = 0; i < expectedLongCount; i++) {
			Assert.assertEquals("Bad value at " + i, longBuffer.get(), 10 + i);
		}

		// Should raise an exception
		try {
			longBuffer.get();
			Assert.fail("Should raise a BufferUnderflowException");
		} catch (BufferUnderflowException e) {
			// That's good !
		}

		// Really shared mémory ?
		array.putLong(firstExpectedLongIndex, 1234l);
		Assert.assertEquals(1234l, longBuffer.get(firstExpectedLongIndex));
	}

	/**
	 * Test asShortBuffer.
	 */
	@Test
	public void testAsShortBuffer() throws IOException {
		int elementCount = 100;
		ArrayFactory arrayFactory = new ArrayFactory();
		IShortArray array = arrayFactory.makeShortArray(elementCount);
		this.closeable = array;

		Assert.assertEquals(elementCount, array.getElementCount());
		// Filling
		for (int i = 0; i < elementCount; i++) {
			array.putShort(i, (short) i);
		}

		int firstExpectedShortIndex = 10;
		int expectedShortCount = 15;
		ShortBuffer shortBuffer = array.asShortBuffer(firstExpectedShortIndex, expectedShortCount);

		for (int i = 0; i < expectedShortCount; i++) {
			Assert.assertEquals("Bad value at " + i, shortBuffer.get(), 10 + i);
		}

		// Should raise an exception
		try {
			shortBuffer.get();
			Assert.fail("Should raise a BufferUnderflowException");
		} catch (BufferUnderflowException e) {
			// That's good !
		}

		// Really shared mémory ?
		array.putShort(firstExpectedShortIndex, (short) 123);
		Assert.assertEquals(123, shortBuffer.get(firstExpectedShortIndex));
	}

	/**
	 * Test asCharBuffer.
	 */
	@Test
	public void testAsCharBuffer() throws IOException {
		int elementCount = 100;
		ArrayFactory arrayFactory = new ArrayFactory();
		ICharArray array = arrayFactory.makeCharArray(elementCount);
		this.closeable = array;

		Assert.assertEquals(elementCount, array.getElementCount());
		// Filling
		for (int i = 0; i < elementCount; i++) {
			array.putChar(i, (char) i);
		}

		int firstExpectedCharIndex = 10;
		int expectedCharCount = 15;
		CharBuffer charBuffer = array.asCharBuffer(firstExpectedCharIndex, expectedCharCount);

		for (int i = 0; i < expectedCharCount; i++) {
			Assert.assertEquals("Bad value at " + i, charBuffer.get(), 10 + i);
		}

		// Should raise an exception
		try {
			charBuffer.get();
			Assert.fail("Should raise a BufferUnderflowException");
		} catch (BufferUnderflowException e) {
			// That's good !
		}

		// Really shared mémory ?
		array.putChar(firstExpectedCharIndex, (char) 123);
		Assert.assertEquals(123, charBuffer.get(firstExpectedCharIndex));
	}

	/**
	 * Test asByteBuffer.
	 */
	@Test
	public void testAsByteBuffer() throws IOException {
		int elementCount = 100;
		ArrayFactory arrayFactory = new ArrayFactory();
		IByteArray array = arrayFactory.makeByteArray(elementCount);
		this.closeable = array;

		Assert.assertEquals(elementCount, array.getElementCount());
		// Filling
		for (int i = 0; i < elementCount; i++) {
			array.putByte(i, (byte) i);
		}

		int firstExpectedByteIndex = 10;
		int expectedByteCount = 15;
		ByteBuffer byteBuffer = array.asByteBuffer(firstExpectedByteIndex, expectedByteCount);

		for (int i = 0; i < expectedByteCount; i++) {
			Assert.assertEquals("Bad value at " + i, byteBuffer.get(), 10 + i);
		}

		// Should raise an exception
		try {
			byteBuffer.get();
			Assert.fail("Should raise a BufferUnderflowException");
		} catch (BufferUnderflowException e) {
			// That's good !
		}

		// Really shared mémory ?
		array.putByte(firstExpectedByteIndex, (byte) 123);
		Assert.assertEquals(123, byteBuffer.get(firstExpectedByteIndex));
	}

	/**
	 * Test asIntBuffer.
	 */
	@Test
	public void testAsIntBuffer() throws IOException {
		int elementCount = 100;
		ArrayFactory arrayFactory = new ArrayFactory();
		IIntArray array = arrayFactory.makeIntArray(elementCount);
		array.setByteOrder(ByteOrder.nativeOrder());
		this.closeable = array;

		Assert.assertEquals(elementCount, array.getElementCount());
		// Filling
		for (int i = 0; i < elementCount; i++) {
			array.putInt(i, i);
		}

		int firstExpectedIntIndex = 10;
		int expectedIntCount = 15;
		IntBuffer intBuffer = array.asIntBuffer(firstExpectedIntIndex, expectedIntCount);

		for (int i = 0; i < expectedIntCount; i++) {
			Assert.assertEquals("Bad value at " + i, intBuffer.get(), 10 + i);
		}

		// Should raise an exception
		try {
			intBuffer.get();
			Assert.fail("Should raise a BufferUnderflowException");
		} catch (BufferUnderflowException e) {
			// That's good !
		}

		// Really shared mémory ?
		array.putInt(firstExpectedIntIndex, 1234);
		Assert.assertEquals(1234, intBuffer.get(firstExpectedIntIndex));
	}

	/**
	 * Test cloneByteArray.
	 */
	@Test
	public void cloneByteArray() throws IOException {
		int elementCount = 100;
		ArrayFactory arrayFactory = new ArrayFactory();
		IByteArray array = arrayFactory.makeByteArray(elementCount);
		array.setByteOrder(ByteOrder.nativeOrder());
		this.closeable = array;

		Assert.assertTrue(array instanceof TinyArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		// Filling
		for (int i = 0; i < elementCount; i++) {
			array.putByte(i, (byte) i);
		}

		// Comparing
		IByteArray cloneArray = array.cloneByteArray();
		for (int i = 0; i < elementCount; i++) {
			Assert.assertEquals("Bad value at " + i, cloneArray.getByte(i), (byte) i);
		}
	}

	/**
	 * Test cloneDoubleArray.
	 */
	@Test
	public void cloneDoubleArray() throws IOException {
		int elementCount = 100;
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(elementCount);
		array.setByteOrder(ByteOrder.nativeOrder());
		this.closeable = array;

		Assert.assertTrue(array instanceof TinyArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		// Filling
		double value = Math.PI;
		for (int i = 0; i < elementCount; i++) {
			array.putDouble(i, value);
			value += Math.PI;
		}

		// Comparing
		IDoubleArray cloneArray = array.cloneDoubleArray();
		value = Math.PI;
		for (int i = 0; i < elementCount; i++) {
			Assert.assertEquals("Bad value at " + i, cloneArray.getDouble(i), value, 0d);
			value += Math.PI;
		}
	}

	/**
	 * Test dump.
	 */
	@Test
	public void dumpDoubleArray() throws IOException {
		int elementCount = 100;
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(elementCount);
		array.setByteOrder(ByteOrder.nativeOrder());
		this.closeable = array;

		Assert.assertTrue(array instanceof TinyArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		// Filling
		double value = Math.PI;
		for (int i = 0; i < elementCount; i++) {
			array.putDouble(i, value);
			value += Math.PI;
		}

		// Comparing
		File file = File.createTempFile(getClass().getSimpleName() + "_dump_", ".tmp");
		array.dump(file);
		try (IDoubleArray copyArray = arrayFactory.openDoubleArray(file, false);) {
			copyArray.setByteOrder(ByteOrder.nativeOrder());
			value = Math.PI;
			for (int i = 0; i < elementCount; i++) {
				Assert.assertEquals("Bad value at " + i, copyArray.getDouble(i), value, 0d);
				value += Math.PI;
			}
		}
		file.delete();
	}

}
