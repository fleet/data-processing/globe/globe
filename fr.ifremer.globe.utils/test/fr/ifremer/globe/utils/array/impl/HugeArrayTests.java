/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.array.impl;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteOrder;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.array.IBooleanArray;
import fr.ifremer.globe.utils.array.IByteArray;
import fr.ifremer.globe.utils.array.ICharArray;
import fr.ifremer.globe.utils.array.IDoubleArray;
import fr.ifremer.globe.utils.array.IFloatArray;
import fr.ifremer.globe.utils.array.ILongArray;
import fr.ifremer.globe.utils.array.IShortArray;

/**
 * Test of HugeArray.
 * 
 * @author Apside
 *
 */
public class HugeArrayTests {

	/** Ressource à libérer après chaque test. */
	protected Closeable closeable;

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		IOUtils.closeQuietly(this.closeable);
	}

	/**
	 * Test of computeIndexInBuffer().
	 */
	@Test
	public void testComputeIndexInBuffer() throws IOException {
		int segmentCount = 4;
		int sizeOfOneElement = Double.BYTES;
		int elementBySegment = HugeArray.MAX_SEGMENT_SIZE / sizeOfOneElement;
		long elementCount = ((long) elementBySegment) * segmentCount;

		HugeArray array = new HugeArray(elementCount, sizeOfOneElement);

		// For each segment of doubles
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first double of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			Assert.assertEquals("Bad first index for segment " + segmentIndex, 0, array.computeIndexInBuffer(firstElementIndexInSegment));
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last double of segment
			Assert.assertEquals("Bad last index for segment " + segmentIndex, HugeArray.MAX_SEGMENT_SIZE - sizeOfOneElement, array.computeIndexInBuffer(lastElementIndexInSegment));
		}

		// Second segment

		array.close();
	}

	/**
	 * Test of computeSegmentSize().
	 */
	@Test
	public void testComputeSegmentProperties() throws IOException {
		// One dimentional Array
		{
			long elementCount = HugeArray.MAX_SEGMENT_SIZE + 5;
			HugeArray array = new HugeArray(elementCount, Double.BYTES);
			array.computeSegmentProperties();
			Assert.assertEquals("Bad segment count", 9, array.getSegmentCount());
			Assert.assertEquals("Bad segment size", HugeArray.MAX_SEGMENT_SIZE, array.getSegmentSize());
			array.close();
		}

		// bi dimentional Array
		{
			int rowCount = 100000;
			int colCount = 200000;
			long elementCount = (long) rowCount * (long) colCount;
			HugeArray array = new HugeArray(elementCount, Double.BYTES, colCount);
			array.computeSegmentProperties();
			Assert.assertEquals("Bad segment count", 75, array.getSegmentCount());
			// 1342 rows per segement
			Assert.assertEquals("Bad segment size", ArrayUtils.multiply(1342, colCount, Double.BYTES), array.getSegmentSize());
			array.close();
		}

		// three dimentional Array
		{
			int xCount = 100000;
			int yCount = 20000;
			int zCount = 10;
			long elementCount = (long) xCount * (long) yCount * zCount;
			HugeArray array = new HugeArray(elementCount, Double.BYTES, yCount, zCount);
			array.computeSegmentProperties();
			Assert.assertEquals("Bad segment count", 75, array.getSegmentCount());
			// 1342 rows per segement
			Assert.assertEquals("Bad segment size", ArrayUtils.multiply(1342, yCount, zCount, Double.BYTES), array.getSegmentSize());
			array.close();
		}

	}

	/**
	 * Test of computeIndexInBuffer().
	 */
	@Test
	public void testComputeIndexInBuffer2Dimensions() throws IOException {
		int segmentCount = 4;
		int sizeOfOneElement = Double.BYTES;
		int elementBySegment = HugeArray.MAX_SEGMENT_SIZE / sizeOfOneElement;
		long elementCount = ((long) elementBySegment) * segmentCount;

		HugeArray array = new HugeArray(elementCount, sizeOfOneElement);

		// For each segment of doubles
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first double of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			Assert.assertEquals("Bad first index for segment " + segmentIndex, 0, array.computeIndexInBuffer(firstElementIndexInSegment));
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last double of segment
			Assert.assertEquals("Bad last index for segment " + segmentIndex, HugeArray.MAX_SEGMENT_SIZE - sizeOfOneElement, array.computeIndexInBuffer(lastElementIndexInSegment));
		}

		// Second segment

		array.close();
	}

	/**
	 * Test du Boolean HugeArray.
	 */
	@Test
	public void testBooleanHugeArray() throws IOException {
		int segmentCount = 4;
		int sizeOfOneElement = 1;
		int elementBySegment = HugeArray.MAX_SEGMENT_SIZE / sizeOfOneElement;
		long elementCount = ((long) elementBySegment) * segmentCount;

		ArrayFactory arrayFactory = new ArrayFactory();
		IBooleanArray array = arrayFactory.makeBooleanArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		Boolean value = true;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first Boolean of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			array.putBoolean(firstElementIndexInSegment, value);
			value = !value;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last Boolean of segment
			array.putBoolean(lastElementIndexInSegment, value);
			value = !value;
		}
		value = true;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first Boolean of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			Assert.assertEquals(value, array.getBoolean(firstElementIndexInSegment));
			value = !value;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last Boolean of segment
			Assert.assertEquals(value, array.getBoolean(lastElementIndexInSegment));
			value = !value;
		}
	}

	/**
	 * Test du Byte HugeArray.
	 */
	@Test
	public void testByteHugeArray() throws IOException {
		int segmentCount = 4;
		int sizeOfOneElement = Byte.BYTES;
		int elementBySegment = HugeArray.MAX_SEGMENT_SIZE / sizeOfOneElement;
		long elementCount = ((long) elementBySegment) * segmentCount;

		ArrayFactory arrayFactory = new ArrayFactory();
		IByteArray array = arrayFactory.makeByteArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		byte value = 3;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first Byte of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			array.putByte(firstElementIndexInSegment, value);
			value += 3;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last Byte of segment
			array.putByte(lastElementIndexInSegment, value);
			value += 3;
		}
		value = 3;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first Byte of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			Assert.assertEquals(value, array.getByte(firstElementIndexInSegment));
			value += 3;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last Byte of segment
			Assert.assertEquals(value, array.getByte(lastElementIndexInSegment));
			value += 3;
		}
	}

	/**
	 * Test du Char HugeArray.
	 */
	@Test
	public void testCharHugeArray() throws IOException {
		int segmentCount = 4;
		int sizeOfOneElement = Character.BYTES;
		int elementBySegment = HugeArray.MAX_SEGMENT_SIZE / sizeOfOneElement;
		long elementCount = ((long) elementBySegment) * segmentCount;

		ArrayFactory arrayFactory = new ArrayFactory();
		ICharArray array = arrayFactory.makeCharArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		char value = 3;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first Char of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			array.putChar(firstElementIndexInSegment, value);
			value += 3;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last Char of segment
			array.putChar(lastElementIndexInSegment, value);
			value += 3;
		}
		value = 3;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first Char of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			Assert.assertEquals(value, array.getChar(firstElementIndexInSegment));
			value += 3;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last Char of segment
			Assert.assertEquals(value, array.getChar(lastElementIndexInSegment));
			value += 3;
		}
	}

	/**
	 * Test du Double HugeArray.
	 */
	@Test
	public void testDoubleHugeArray() throws IOException {
		int segmentCount = 4;
		int sizeOfOneElement = Double.BYTES;
		int elementBySegment = HugeArray.MAX_SEGMENT_SIZE / sizeOfOneElement;
		long elementCount = ((long) elementBySegment) * segmentCount;

		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		double value = Math.PI;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first double of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			array.putDouble(firstElementIndexInSegment, value);
			value += Math.PI;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last double of segment
			array.putDouble(lastElementIndexInSegment, value);
			value += Math.PI;
		}
		value = Math.PI;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first double of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			Assert.assertEquals(value, array.getDouble(firstElementIndexInSegment), 0d);
			value += Math.PI;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last double of segment
			Assert.assertEquals(value, array.getDouble(lastElementIndexInSegment), 0d);
			value += Math.PI;
		}
	}

	/**
	 * Test du Double two dimentional TinyArray.
	 */
	@Test
	public void testDoubleBiDimensionalTinyArray() throws IOException {
		int xCount = 100000;
		int yCount = 100000;
		long elementCount = ArrayUtils.multiply(xCount, yCount);
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(xCount, yCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		// Set values
		double value = Math.PI;
		for (int x = 0; x < xCount; x += 10000) {
			for (int y = 0; y < yCount; y += 10000) {
				array.putDouble(x, y, value);
				value += Math.PI;
			}
		}

		// Last value
		array.putDouble(xCount - 1, yCount - 1, value);

		// check values
		value = Math.PI;
		for (int x = 0; x < xCount; x += 10000) {
			for (int y = 0; y < yCount; y += 10000) {
				Assert.assertEquals(value, array.getDouble(x, y), 0d);
				value += Math.PI;
			}
		}

		// Last value
		Assert.assertEquals(value, array.getDouble(xCount - 1, yCount - 1), 0d);
		Assert.assertEquals(value, array.getDouble(elementCount - 1), 0d);
	}

	/**
	 * Test du Double two dimentional TinyArray.
	 */
	@Test
	public void testDoubleTriDimensionalTinyArray() throws IOException {
		int xCount = 100000;
		int yCount = 1000;
		int zCount = 100;
		long elementCount = ArrayUtils.multiply(xCount, yCount, zCount);
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(xCount, yCount, zCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		// Set values
		double value = Math.PI;
		for (int x = 0; x < xCount; x += 10000) {
			for (int y = 0; y < yCount; y += 100) {
				for (int z = 0; z < zCount; z += 10) {
					array.putDouble(x, y, z, value);
					value += Math.PI;
				}
			}
		}

		// Last value
		array.putDouble(xCount - 1, yCount - 1, zCount - 1, value);

		// check values
		value = Math.PI;
		for (int x = 0; x < xCount; x += 10000) {
			for (int y = 0; y < yCount; y += 100) {
				for (int z = 0; z < zCount; z += 10) {
					Assert.assertEquals(value, array.getDouble(x, y, z), 0d);
					value += Math.PI;
				}
			}
		}

		// Last value
		Assert.assertEquals(value, array.getDouble(xCount - 1, yCount - 1, zCount - 1), 0d);
		Assert.assertEquals(value, array.getDouble(elementCount - 1), 0d);
	}

	/**
	 * Test du Double three dimentional TinyArray.
	 */
	@Test
	public void testDoubleThreeDimensionalTinyArray() throws IOException {
		int xCount = 50000;
		int yCount = 50000;
		int zCount = 2;
		long elementCount = ArrayUtils.multiply(xCount, yCount, zCount);
		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(xCount, yCount, zCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		// Set values
		double value = Math.PI;
		for (int x = 0; x < xCount; x += 5000) {
			for (int y = 0; y < yCount; y += 5000) {
				array.putDouble(x, y, 0, value);
				value += Math.PI;
				array.putDouble(x, y, 1, value);
				value += Math.PI;
			}
		}

		// Last value
		array.putDouble(xCount - 1, yCount - 1, value);

		// check values
		value = Math.PI;
		for (int x = 0; x < xCount; x += 5000) {
			for (int y = 0; y < yCount; y += 5000) {
				Assert.assertEquals(value, array.getDouble(x, y, 0), 0d);
				value += Math.PI;
				Assert.assertEquals(value, array.getDouble(x, y, 1), 0d);
				value += Math.PI;
			}
		}

		// Last value
		Assert.assertEquals(value, array.getDouble(xCount - 1, yCount - 1), 0d);
	}

	/**
	 * Test du Float HugeArray.
	 */
	@Test
	public void testFloatHugeArray() throws IOException {
		int segmentCount = 4;
		int sizeOfOneElement = Float.BYTES;
		int elementBySegment = HugeArray.MAX_SEGMENT_SIZE / sizeOfOneElement;
		long elementCount = ((long) elementBySegment) * segmentCount;

		ArrayFactory arrayFactory = new ArrayFactory();
		IFloatArray array = arrayFactory.makeFloatArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		float value = (float) Math.PI;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first double of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			array.putFloat(firstElementIndexInSegment, value);
			value += Math.PI;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last double of segment
			array.putFloat(lastElementIndexInSegment, value);
			value += Math.PI;
		}
		value = (float) Math.PI;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first double of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			Assert.assertEquals(value, array.getFloat(firstElementIndexInSegment), 0d);
			value += Math.PI;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last double of segment
			Assert.assertEquals(value, array.getFloat(lastElementIndexInSegment), 0d);
			value += Math.PI;
		}
	}

	/**
	 * Test du Long HugeArray.
	 */
	@Test
	public void testLongHugeArray() throws IOException {
		int segmentCount = 4;
		int sizeOfOneElement = Long.BYTES;
		int elementBySegment = HugeArray.MAX_SEGMENT_SIZE / sizeOfOneElement;
		long elementCount = ((long) elementBySegment) * segmentCount;

		ArrayFactory arrayFactory = new ArrayFactory();
		ILongArray array = arrayFactory.makeLongArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		long value = 1234l;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first Long of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			array.putLong(firstElementIndexInSegment, value);
			value *= 3;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last Long of segment
			array.putLong(lastElementIndexInSegment, value);
			value *= 3;
		}
		value = 1234l;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first Long of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			Assert.assertEquals(value, array.getLong(firstElementIndexInSegment));
			value *= 3;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last Long of segment
			Assert.assertEquals(value, array.getLong(lastElementIndexInSegment));
			value *= 3;
		}
	}

	/**
	 * Test du Short HugeArray.
	 */
	@Test
	public void testShortHugeArray() throws IOException {
		int segmentCount = 4;
		int sizeOfOneElement = Short.BYTES;
		int elementBySegment = HugeArray.MAX_SEGMENT_SIZE / sizeOfOneElement;
		long elementCount = ((long) elementBySegment) * segmentCount;

		ArrayFactory arrayFactory = new ArrayFactory();
		IShortArray array = arrayFactory.makeShortArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		short value = 123;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first Short of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			array.putShort(firstElementIndexInSegment, value);
			value *= 3;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last Short of segment
			array.putShort(lastElementIndexInSegment, value);
			value *= 3;
		}
		value = 123;
		for (int segmentIndex = 0; segmentIndex < segmentCount; segmentIndex++) {
			// first Short of segment
			long firstElementIndexInSegment = ((long) elementBySegment) * segmentIndex;
			Assert.assertEquals(value, array.getShort(firstElementIndexInSegment));
			value *= 3;
			long lastElementIndexInSegment = firstElementIndexInSegment + elementBySegment - 1;
			// last Short of segment
			Assert.assertEquals(value, array.getShort(lastElementIndexInSegment));
			value *= 3;
		}
	}

	/**
	 * Test du Short reduceElementCount.
	 */
	@Test
	public void testReduceElementCount() throws IOException {
		int segmentCount = 4;
		int sizeOfOneElement = Short.BYTES;
		int elementBySegment = HugeArray.MAX_SEGMENT_SIZE / sizeOfOneElement;
		long elementCount = ((long) elementBySegment) * segmentCount;

		ArrayFactory arrayFactory = new ArrayFactory();
		IShortArray array = arrayFactory.makeShortArray(elementCount);
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		HugeArray shortHugeArray = (HugeArray) array;
		int previousCapacity = shortHugeArray.mappedByteBuffers[shortHugeArray.mappedByteBuffers.length - 1].capacity();

		shortHugeArray.reduceElementCount(elementCount - 2);
		Assert.assertEquals((elementCount - 2) << shortHugeArray.getShift(), shortHugeArray.getFile().length());

		int newCapacity = shortHugeArray.mappedByteBuffers[shortHugeArray.mappedByteBuffers.length - 1].capacity();
		Assert.assertEquals(previousCapacity - (Short.BYTES * 2), newCapacity);

	}

	/**
	 * Test cloneByteArray.
	 */
	@Test
	public void cloneByteArray() throws IOException {
		int segmentCount = 2;
		int sizeOfOneElement = Byte.BYTES;
		int elementBySegment = HugeArray.MAX_SEGMENT_SIZE / sizeOfOneElement;
		long elementCount = ((long) elementBySegment) * segmentCount;

		ArrayFactory arrayFactory = new ArrayFactory();
		IByteArray array = arrayFactory.makeByteArray(elementCount);
		array.setByteOrder(ByteOrder.nativeOrder());
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		// Filling only 100 bytes
		long step = elementCount / 100;
		for (long i = 0; i < elementCount; i += step) {
			array.putByte(i, (byte) i);
		}

		// Comparing
		try (IByteArray cloneArray = array.cloneByteArray()) {
			for (long i = 0; i < elementCount; i += step) {
				Assert.assertEquals("Bad value at " + i, cloneArray.getByte(i), (byte) i);
			}
		}
	}

	/**
	 * Test cloneDoubleArray.
	 */
	@Test
	public void cloneDoubleArray() throws IOException {
		int segmentCount = 2;
		int sizeOfOneElement = Double.BYTES;
		int elementBySegment = HugeArray.MAX_SEGMENT_SIZE / sizeOfOneElement;
		long elementCount = ((long) elementBySegment) * segmentCount;

		ArrayFactory arrayFactory = new ArrayFactory();
		IDoubleArray array = arrayFactory.makeDoubleArray(elementCount);
		array.setByteOrder(ByteOrder.nativeOrder());
		this.closeable = array;

		Assert.assertTrue(array instanceof HugeArray);
		Assert.assertEquals(elementCount, array.getElementCount());

		// Filling only 100 bytes
		long step = elementCount / 100;
		double value = Math.PI;
		for (long i = 0; i < elementCount; i += step) {
			array.putDouble(i, value);
			value += Math.PI;
		}

		// Comparing
		try (IDoubleArray cloneArray = array.cloneDoubleArray()) {
			value = Math.PI;
			for (long i = 0; i < elementCount; i += step) {
				Assert.assertEquals("Bad value at " + i, cloneArray.getDouble(i), value, 0d);
				value += Math.PI;
			}
		}
	}

}
