/**
 * GLOBE - Ifremer
 */
package fr.ifremer.sonarnative;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.sonarnative.jni.LogProxy;

/**
 * Log proxy of Sonar Native to redirect the message to Globe logger
 */
public class SonarNativeLogger extends LogProxy {

	private static final Logger logger = LoggerFactory.getLogger(SonarNativeLogger.class);

	/** {@inheritDoc} */
	@Override
	public void debug(String message) {
		logger.debug(message);
	}

	/** {@inheritDoc} */
	@Override
	public void error(String message) {
		logger.error(message);
	}

	/** {@inheritDoc} */
	@Override
	public void fatal(String message) {
		logger.error(message);
	}

	/** {@inheritDoc} */
	@Override
	public void info(String message) {
		logger.info(message);
	}

	/** {@inheritDoc} */
	@Override
	public void warn(String message) {
		logger.warn(message);
	}
}
