/**
 * GLOBE - Ifremer
 */
package fr.ifremer.sonarnative;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.gdal.GdalLibLoader;

/**
 * Activation of Sonar native
 */
public class Activator implements BundleActivator {

	/** Keep a static reference on logger used by Sonar native (may crash if released) */
	private static SonarNativeLogger SONAR_NATIVE_LOGGER;

	/** {@inheritDoc} */
	@Override
	public void start(BundleContext context) throws Exception {
		Logger logger = LoggerFactory.getLogger(Activator.class);
		// Ensure Netcdf library is loaded (Bring with Gdal)
		GdalLibLoader.loadNativeLibrary();
		SonarNativeLibLoader.loadNativeLibrary();
		if (SonarNativeLibLoader.isAvailable()) {
			logger.info("Sonarnative library loaded");
			SONAR_NATIVE_LOGGER = new SonarNativeLogger();
			fr.ifremer.sonarnative.jni.Log.initializeLog(SONAR_NATIVE_LOGGER);
		} else {
			logger.error("Unable to load Sonarnative library");
		}

	}

	/** {@inheritDoc} */
	@Override
	public void stop(BundleContext context) throws Exception {
		// Nothing to do
	}

}
