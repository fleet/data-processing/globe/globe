#!/bin/bash
DIR=`pwd`

export CONDA_DIR="/Users/cponcele/.conda/envs/pyat_dev/"
export NETCDF4_DIR=${CONDA_DIR}
export ZLIB_DIR=${CONDA_DIR}
export GEOS_DIR=${CONDA_DIR}
export HDF5_DIR=${CONDA_DIR}
export GEOTIFF_DIR=${CONDA_DIR}

# Install SWIG

# Install GDAL
v=2.4.4
GDAL_DIR=${DIR}/install/gdal-${v}
cd gdal-${v}
export MACOSX_DEPLOYMENT_TARGET=10.9
./configure --with-threads --without-grass \
--with-static-proj4=${PROJ_DIR} \
--with-java=${JAVA_HOME} \
--with-netcdf=${NETCDF4_DIR} \
--with-libz=${ZLIB_DIR} \
--with-curl \
--with-geotiff=${GEOTIFF_DIR} \
--with-libjson-c=${CONDA_DIR} \
--with-geos=${GEOS_DIR}/bin/geos-config \
--enable-static \
--with-jvm-lib=${JAVA_HOME}/jre/lib/server/libjvm.dylib --prefix=${GDAL_DIR} \
CFLAGS="-Os -arch x86_64 -stdlib=libc++" CXXFLAGS="-Os -arch x86_64 -stdlib=libc++" LDFLAGS="-arch x86_64 -stdlib=libc++"

#--with-hdf5=${HDF5_DIR}  \


make clean
make install
cd swig/java
make
cd ../..


