/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.h5;

import org.eclipse.core.runtime.Platform;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class Activator implements BundleActivator {

	/** Logger. */
	protected static final Logger logger = LoggerFactory.getLogger(Activator.class);

	@Override
	public void start(BundleContext context) throws Exception {
		logger.info("Starting HDF5 bundle, OS=" + Platform.getOS());
		// On Windows, we must load dependent library of jhdf5.dll
		if ("win32".equalsIgnoreCase(Platform.getOS())) {
			logger.info("Loading msvcr100");
			System.loadLibrary("msvcr100");
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// Nothing to do

	}

}
