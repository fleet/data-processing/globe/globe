package fr.ifremer.globe.utils.h5;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ncsa.hdf.hdf5lib.H5;
import ncsa.hdf.hdf5lib.HDF5Constants;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;
import ncsa.hdf.object.Attribute;
import ncsa.hdf.object.Dataset;
import ncsa.hdf.object.HObject;
import ncsa.hdf.object.h5.H5Group;

/**
 * Gives an acces on a H5Group members and metadata by their names instead of their index.
 * <p>
 * Note : only Attribute metadata are indexed.
 */
public class H5GroupWithMembers {

	/** Underlying group to be indexed. */
	protected H5Group group;

	/** Group's members. */
	protected Map<String, HObject> members;

	/** Group's metadata (only Attributes are indexed). */
	protected Map<String, Attribute> attributes;

	/**
	 * Constructor.
	 * 
	 * @param group
	 *            The H5Group to be indexed for rapid access to its members and attributes.
	 */
	public H5GroupWithMembers(H5Group group) {
		this.group = group;
		this.members = new HashMap<>(10);
		this.attributes = new HashMap<>(10);

		try {
			List<?> metadata = group.getMetadata();
			for (Object m : metadata) {
				if (m instanceof Attribute) {
					((Attribute) m).getName();
					this.attributes.put(((Attribute) m).getName(), (Attribute) m);
				}
			}
		} catch (HDF5Exception e) {
			e.printStackTrace();
		}

		List<HObject> memberList = group.getMemberList();
		for (HObject hObject : memberList) {
			members.put(hObject.getName(), hObject);
		}
	}

	public List<H5Group> getGroups() {
		List<H5Group> groups = new ArrayList<H5Group>();
		for (Entry<String, HObject> entry : members.entrySet()) {
			if (entry.getValue() instanceof H5Group) {
				groups.add((H5Group) entry.getValue());
			}
		}

		return groups;
	}

	/**
	 * @return The underlying group.
	 */
	public H5Group getH5Group() {
		return this.group;
	}

	/**
	 * @return The group's full name.
	 */
	public String getFullName() {
		return this.group.getFullName();
	}

	/**
	 * Returns <code>true</code> if the underlying group has an attribute named <code>$attributeName</code>.
	 * 
	 * @param attributeName
	 *            The attribute name.
	 * @return <code>true</code> if the underlying group has an attribute named <code>$attributeName</code>, <code>false</code> otherwise.
	 */
	public boolean containsAttribute(String attributeName) {
		return this.attributes.containsKey(attributeName);
	}

	/**
	 * Returns <code>true</code> if the underlying group has a dataset named <code>$datasetName</code>.
	 * 
	 * @param datasetName
	 *            The dataset name.
	 * @return <code>true</code> if the underlying group has a dataset named <code>$datasetName</code>, <code>false</code> otherwise.
	 */
	public boolean containsDataset(String datasetName) {
		HObject hObject = this.members.get(datasetName);
		return hObject != null && (hObject instanceof Dataset);
	}

	/**
	 * Returns <code>true</code> if the underlying group has a subgroup named <code>$groupName</code>.
	 * 
	 * @param groupName
	 *            The dataset name.
	 * @return <code>true</code> if the underlying group has a subgroup named <code>$groupName</code>, <code>false</code> otherwise.
	 */
	public boolean containsGroup(String groupName) {
		HObject hObject = this.members.get(groupName);
		return hObject != null && (hObject instanceof H5Group);
	}

	/**
	 * Returns the attribute named $attributeName or <code>null</code> if it doesn't exists.
	 * 
	 * @param attributeName
	 *            The attribute name.
	 * @return the attribute named $attributeName or <code>null</code> if it doesn't exists.
	 */
	public Attribute getAttribute(String attributeName) {
		return this.attributes.get(attributeName);
	}

	/**
	 * Returns the attribute named $attributeName or <code>null</code> if it doesn't exists.
	 * 
	 * @param attributeName
	 *            The attribute name.
	 * @return the attribute named $attributeName or <code>null</code> if it doesn't exists.
	 */
	public Dataset getDataset(String datasetName) {
		return (Dataset) this.members.get(datasetName);
	}

	/**
	 * Returns the attribute named $attributeName or <code>null</code> if it doesn't exists.
	 * 
	 * @param attributeName
	 *            The attribute name.
	 * @return the attribute named $attributeName or <code>null</code> if it doesn't exists.
	 */
	public H5GroupWithMembers getGroup(String groupName) {
		HObject g = this.members.get(groupName);
		if (g != null) {
			return new H5GroupWithMembers((H5Group) g);
		}
		return null;
	}

	/**
	 * Generic access to any member of the group.
	 * 
	 * @param memberName
	 *            Name of the researched member.
	 * @return The member named <code>$memberName</code> or <code>null</code> if it doesn't exist.
	 */
	public HObject getMember(String memberName) {
		return this.members.get(memberName);
	}

	/**
	 * Get all members.
	 * 
	 * @return All members of the group, ordered by names.
	 */
	public List<HObject> getMembers() {

		Collection<HObject> values = this.members.values();
		List<HObject> result = null;

		if (!(values instanceof ArrayList)) {

			result = new ArrayList<>(values);

			Collections.sort(result, (o1, o2) -> {
				return o1.getName().compareTo(o2.getName());
			});
		}
		return result;
	}

	// =============================================================================================
	// Single attributes
	// =============================================================================================

	public Double getDoubleAttributeValue(String attributeName) {
		Double result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = ((double[]) this.attributes.get(attributeName).getValue())[0];
		}

		return result;
	}

	public Integer getIntegerAttributeValue(String attributeName) {
		Integer result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = ((int[]) this.attributes.get(attributeName).getValue())[0];
		}

		return result;
	}

	public Float getFloatAttributeValue(String attributeName) {
		Float result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = ((float[]) this.attributes.get(attributeName).getValue())[0];
		}

		return result;
	}

	public Long getLongAttributeValue(String attributeName) {
		Long result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = ((long[]) this.attributes.get(attributeName).getValue())[0];
		}

		return result;
	}

	public String getStringAttributeValue(String attributeName) {
		String result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = ((String[]) this.attributes.get(attributeName).getValue())[0];
		}

		return result;
	}

	// =============================================================================================
	// 1D array attributes
	// =============================================================================================

	public double[] getDoubleArrayAttributeValue(String attributeName) {
		double[] result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = (double[]) this.attributes.get(attributeName).getValue();
		}

		return result;
	}

	public int[] getIntegerArrayAttributeValue(String attributeName) {
		int[] result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = (int[]) this.attributes.get(attributeName).getValue();
		}

		return result;
	}

	public float[] getFloatArrayAttributeValue(String attributeName) {
		float[] result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = (float[]) this.attributes.get(attributeName).getValue();
		}

		return result;
	}

	public long[] getLongArrayAttributeValue(String attributeName) {
		long[] result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = (long[]) this.attributes.get(attributeName).getValue();
		}

		return result;
	}

	public String[] getStringArrayAttributeValue(String attributeName) {
		String[] result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = (String[]) this.attributes.get(attributeName).getValue();
		}

		return result;
	}

	// =============================================================================================
	// 2D array attributes
	// =============================================================================================

	public double[][] getDouble2DAttributeValue(String attributeName) {
		double[][] result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = (double[][]) this.attributes.get(attributeName).getValue();
		}

		return result;
	}

	public int[][] getInteger2DAttributeValue(String attributeName) {
		int[][] result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = (int[][]) this.attributes.get(attributeName).getValue();
		}

		return result;
	}

	public float[][] getFloat2DAttributeValue(String attributeName) {
		float[][] result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = (float[][]) this.attributes.get(attributeName).getValue();
		}

		return result;
	}

	public long[][] getLong2DAttributeValue(String attributeName) {
		long[][] result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = (long[][]) this.attributes.get(attributeName).getValue();
		}

		return result;
	}

	public String[][] getString2DAttributeValue(String attributeName) {
		String[][] result = null;

		if (this.attributes.containsKey(attributeName)) {
			result = (String[][]) this.attributes.get(attributeName).getValue();
		}

		return result;
	}

	// =============================================================================================
	// Datasets
	// =============================================================================================
	public double[] getDoubleArrayDatasetValues(String datasetName) throws OutOfMemoryError, Exception {
		Dataset dataset = getDataset(datasetName);
		if (dataset != null) {
			return double[].class.cast(dataset.read());
		}
		return null;
	}

	public int[] getIntegerArrayDatasetValues(String datasetName) throws OutOfMemoryError, Exception {
		Dataset dataset = getDataset(datasetName);
		if (dataset != null) {
			return int[].class.cast(dataset.read());
		}
		return null;
	}

	public String[] getStringArrayDatasetValue(String datasetName) throws OutOfMemoryError, Exception {
		Dataset dataset = getDataset(datasetName);
		if (dataset != null) {
			return String[].class.cast(dataset.read());
		}
		return null;
	}

	public double[][] getDouble2DDatasetValue(String name, int lengthX, int lengthY) throws HDF5Exception {
		return get2DDatasetValue(name, lengthX, lengthY, HDF5Constants.H5T_NATIVE_DOUBLE, double.class, double[][].class);
	}

	public int[][] getInteger2DDatasetValues(String name, int lengthX, int lengthY) throws HDF5Exception {
		return get2DDatasetValue(name, lengthX, lengthY, HDF5Constants.H5T_NATIVE_INT, int.class, int[][].class);
	}

	public float[][] getFloat2DDatasetValues(String name, int lengthX, int lengthY) throws HDF5Exception {
		return get2DDatasetValue(name, lengthX, lengthY, HDF5Constants.H5T_NATIVE_FLOAT, float.class, float[][].class);
	}

	public long[][] getLong2DDatasetValues(String name, int lengthX, int lengthY) throws HDF5Exception {
		return get2DDatasetValue(name, lengthX, lengthY, HDF5Constants.H5T_NATIVE_FLOAT, long.class, long[][].class);
	}

	private <T> T get2DDatasetValue(String name, int lengthX, int lengthY, int h5Type, Class<?> contentType, Class<T> arrayType) throws HDF5Exception {

		long dimsm[] = { lengthX, lengthY };
		long count[] = { 1, 1 };
		long offset[] = { 0, 0 };
		long block[] = { lengthX, lengthY };

		int dataset_id = H5.H5Dopen(group.getFID(), this.group.getFullName() + "/" + name, HDF5Constants.H5P_DEFAULT);
		int memspace_id = H5.H5Screate_simple(2, dimsm, null);
		int dataspace_id = H5.H5Dget_space(dataset_id);

		@SuppressWarnings("unchecked")
		// Cast is safe here
		T dataOut = (T) Array.newInstance(contentType, lengthY, lengthX);

		H5.H5Sselect_hyperslab(dataspace_id, HDF5Constants.H5S_SELECT_SET, offset, null, count, block);

		// IMPORTANT : ne pas passer un tableau à 2 dimensions à la méthode H5.H5Dread(...)
		// car la lecture dans un tableau à 1 dimension est BEAUCOUP PLUS RAPIDE. Et ensuite
		// il faut recopier les éléments lus dans le tableau à 2 dimensions.
		Object tempArray = Array.newInstance(contentType, lengthX * lengthY);
		H5.H5Dread(dataset_id, h5Type, memspace_id, dataspace_id, HDF5Constants.H5P_DEFAULT, tempArray);
		for (int i = 0; i < lengthY; i++) {
			System.arraycopy(tempArray, i * lengthX, Array.get(dataOut, i), 0, lengthX);
		}

		H5.H5Dclose(dataset_id);
		H5.H5Sclose(memspace_id);
		H5.H5Sclose(dataspace_id);

		return dataOut;
	}

	@Override
	public String toString() {
		return getFullName();
	}
}
