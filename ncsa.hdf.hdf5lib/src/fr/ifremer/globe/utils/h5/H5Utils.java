package fr.ifremer.globe.utils.h5;

import ncsa.hdf.hdf5lib.H5;
import ncsa.hdf.hdf5lib.HDF5Constants;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;
import ncsa.hdf.hdf5lib.exceptions.HDF5LibraryException;
import ncsa.hdf.object.Attribute;
import ncsa.hdf.object.Dataset;
import ncsa.hdf.object.Datatype;
import ncsa.hdf.object.h5.H5Datatype;
import ncsa.hdf.object.h5.H5File;
import ncsa.hdf.object.h5.H5Group;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class H5Utils {

	public static final Logger logger = LoggerFactory.getLogger(H5Utils.class);

	public static H5Group createGroup(H5File file, String groupName) {
		return createGroup(file, null, groupName);
	}

	public static H5Group createGroup(H5File file, H5Group parent, String groupName) {
		String groupFullName = groupName;

		if (parent != null) {
			groupFullName = parent.getFullName() + "/" + groupName;
		}

		H5Group node = getGroup(file, groupFullName);
		try {
			if (node == null) {
				int group_id = -1;
				// Create a group in the file.
				node = (H5Group) file.createGroup(groupName, parent);

				group_id = node.open();
				node.close(group_id);
			}
		} catch (Throwable t) {
			logger.error("Error while creating group : " + groupName, t);
		}

		return node;
	}

	public static H5Group getGroup(H5File file, String groupName) {
		H5Group group = null;
		try {
			if (H5.H5Lexists(file.getFID(), groupName, HDF5Constants.H5P_DEFAULT)) {
				group = (H5Group) file.get(groupName);
			}
		} catch (Throwable t) {
			logger.error("Error while getting group : " + groupName, t);
		}
		return group;
	}

	public static H5GroupWithMembers createGroupWithMembers(H5File file, String groupName) throws Exception {
		H5GroupWithMembers result = null;
		H5Group group = createGroup(file, groupName);

		if (group != null) {
			result = new H5GroupWithMembers(group);
		}

		return result;
	}

	public static H5GroupWithMembers getGroupWithMembers(H5File file, String groupName) throws Exception {
		H5GroupWithMembers groupWithMembers = null;
		H5Group groupe = getGroup(file, groupName);
		if (groupe != null) {
			groupWithMembers = new H5GroupWithMembers(groupe);
		}
		return groupWithMembers;
	}

	public static void writeIntDataSet(H5File file, H5Group parentGroup, String datasetName, int[] data) throws HDF5LibraryException, HDF5Exception {
		int dataspace_id = -1;
		int dataset_id = -1;
		int rank = 1;
		long[] dims = { data.length };

		// Create dataspace. Setting maximum size to NULL sets the maximum
		// size to be the current size.
		dataspace_id = H5.H5Screate_simple(rank, dims, null);

		// Create the dataset and write the floating point data to it. In
		// this example we will save the data as 64 bit little endian IEEE
		// floating point numbers, regardless of the native type. The HDF5
		// library automatically converts between different floating point
		// types.
		if ((file.getFID() >= 0) && (dataspace_id >= 0)) {
			dataset_id = H5.H5Dcreate(file.getFID(), parentGroup.getFullName() + "/" + datasetName, HDF5Constants.H5T_STD_I32LE, dataspace_id, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT,
					HDF5Constants.H5P_DEFAULT);
		}

		// Write the data to the dataset.
		H5.H5Dwrite(dataset_id, HDF5Constants.H5T_NATIVE_INT, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL, HDF5Constants.H5P_DEFAULT, data);

		// End access to the dataset and release resources used by it.
		H5.H5Dclose(dataset_id);

		// Terminate access to the data space.
		H5.H5Sclose(dataspace_id);
	}

	public static void writeIntDataSet(H5File sourceFile, H5Group parentGroup, String datasetName, int[][] buf) throws NullPointerException, HDF5Exception {

		long[] dims = new long[2];
		if (buf != null) {
			dims[0] = buf.length;
			if (buf.length > 0) {
				dims[1] = buf[0].length;
			}
		}
		int filespace_id = H5.H5Screate_simple(2, dims, null);
		int dataset_id = H5.H5Dcreate(sourceFile.getFID(), parentGroup.getFullName() + "/" + datasetName, HDF5Constants.H5T_STD_I32LE, filespace_id, HDF5Constants.H5P_DEFAULT,
				HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);

		// Write the data to the dataset.
		H5.H5Dwrite(dataset_id, HDF5Constants.H5T_NATIVE_INT, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL, HDF5Constants.H5P_DEFAULT, buf);

		// End access to the dataset and release resources used by it.
		H5.H5Dclose(dataset_id);
	}

	public static void writeShortDataSet(H5File sourceFile, H5Group parentGroup, String datasetName, short[][] buf) throws NullPointerException, HDF5Exception {

		long[] dims = new long[2];
		if (buf != null) {
			dims[0] = buf.length;
			if (buf.length > 0) {
				dims[1] = buf[0].length;
			}
		}
		int filespace_id = H5.H5Screate_simple(2, dims, null);
		int dataset_id = H5.H5Dcreate(sourceFile.getFID(), parentGroup.getFullName() + "/" + datasetName, HDF5Constants.H5T_STD_I16LE, filespace_id, HDF5Constants.H5P_DEFAULT,
				HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);

		// Write the data to the dataset.
		H5.H5Dwrite(dataset_id, HDF5Constants.H5T_NATIVE_SHORT, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL, HDF5Constants.H5P_DEFAULT, buf);

		// End access to the dataset and release resources used by it.
		H5.H5Dclose(dataset_id);
	}

	public static void writeCompressedShortDataSet(H5File sourceFile, H5Group parentGroup, String datasetName, short[][] buf) throws NullPointerException, HDF5Exception {

		long[] dims = new long[2];
		long[] chunksDims = new long[2];
		int compressionLevel = 1;

		if (buf != null) {
			dims[0] = buf.length;
			chunksDims[0] = buf.length;
			if (buf.length > 0) {
				dims[compressionLevel] = buf[0].length;
				chunksDims[compressionLevel] = buf[0].length;
			}
		}
		// Chunks are mandatory to allow compression
		int dcpl_id = H5.H5Pcreate(HDF5Constants.H5P_DATASET_CREATE);
		int dapl_id = H5.H5Pcreate(HDF5Constants.H5P_DATASET_ACCESS);
		H5.H5Pset_chunk_cache(dapl_id, 0, 0, compressionLevel);

		if (dcpl_id >= 0)
			H5.H5Pset_chunk(dcpl_id, 2, chunksDims);

		H5.H5Pset_deflate(dcpl_id, compressionLevel);

		int filespace_id = H5.H5Screate_simple(2, dims, null);
		int dataset_id = H5.H5Dcreate(sourceFile.getFID(), parentGroup.getFullName() + "/" + datasetName, HDF5Constants.H5T_STD_I16LE, filespace_id, HDF5Constants.H5P_DEFAULT, dcpl_id, dapl_id);

		// Write the data to the dataset.
		H5.H5Dwrite(dataset_id, HDF5Constants.H5T_NATIVE_SHORT, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL, HDF5Constants.H5P_DEFAULT, buf);

		// End access to the dataset and release resources used by it.
		H5.H5Dclose(dataset_id);
		H5.H5Pclose(dapl_id);
	}

	public static void writeDoubleDataSet(H5File file, H5Group parentGroup,
			String datasetName, double[] data) throws HDF5LibraryException,
			HDF5Exception {
		int dataspace_id = -1;
		int dataset_id = -1;
		int rank = 1;
		long[] dims = { data.length };

		// Create dataspace. Setting maximum size to NULL sets the maximum
		// size to be the current size.
		dataspace_id = H5.H5Screate_simple(rank, dims, null);

		// Create the dataset and write the floating point data to it. In
		// this example we will save the data as 64 bit little endian IEEE
		// floating point numbers, regardless of the native type. The HDF5
		// library automatically converts between different floating point
		// types.
		if ((file.getFID() >= 0) && (dataspace_id >= 0)) {
			dataset_id = H5.H5Dcreate(file.getFID(), parentGroup.getFullName()
					+ "/" + datasetName, HDF5Constants.H5T_NATIVE_DOUBLE,
					dataspace_id, HDF5Constants.H5P_DEFAULT,
					HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
		}

		// Write the data to the dataset.
		H5.H5Dwrite(dataset_id, HDF5Constants.H5T_NATIVE_DOUBLE,
				HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL,
				HDF5Constants.H5P_DEFAULT, data);

		// End access to the dataset and release resources used by it.
		H5.H5Dclose(dataset_id);

		// Terminate access to the data space.
		H5.H5Sclose(dataspace_id);
	}

	/**
	 * Utilitarian function which writes a HDF5 file data set.
	 * @param sourceFile HDF5 source file where the data set need to be write
	 * @param parentGroup HDF5 group where the data set will be writen
	 * @param datasetName name of the HDF5 data set
	 * @param buf data buffer
	 * @param dimensionZeroIsColumn true if the first dimension of buf argument correspond to the column numbers of the data set
	 * @throws NullPointerException
	 * @throws HDF5Exception
	 */
	public static void writeDoubleDataSet(H5File sourceFile, H5Group parentGroup, String datasetName, double[][] buf, boolean dimensionZeroIsColumn) throws NullPointerException, HDF5Exception {

		long[] dims = new long[2];

		if (buf != null) {
			if(dimensionZeroIsColumn) {
				dims[1] = buf.length; // ==> height / nbRows
				if (buf.length > 0) {
					dims[0] = buf[0].length; // ==> width / nbColumns
				}
			} else {
				dims[0] = buf.length; // ==> width / nbRows
				if (buf.length > 0) {
					dims[1] = buf[0].length; // ==> height / nbColumns
				}
			}
		}

		int filespace_id = H5.H5Screate_simple(2, dims, null);
		int dataset_id = H5.H5Dcreate(sourceFile.getFID(), parentGroup.getFullName() + "/" + datasetName, HDF5Constants.H5T_NATIVE_DOUBLE, filespace_id, HDF5Constants.H5P_DEFAULT,
				HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);

		// Write the data to the dataset.
		H5.H5Dwrite(dataset_id, HDF5Constants.H5T_NATIVE_DOUBLE, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL, HDF5Constants.H5P_DEFAULT, buf);

		// End access to the dataset and release resources used by it.
		H5.H5Dclose(dataset_id);
	}

	public static void writeFloatDataSet(H5File file, H5Group parentGroup, String datasetName, float[] data) throws NullPointerException, HDF5Exception {
		int dataspace_id = -1;
		int dataset_id = -1;
		int rank = 1;
		long[] dims = { data.length };

		// Create dataspace. Setting maximum size to NULL sets the maximum
		// size to be the current size.
		dataspace_id = H5.H5Screate_simple(rank, dims, null);

		// Create the dataset and write the floating point data to it. In
		// this example we will save the data as 64 bit little endian IEEE
		// floating point numbers, regardless of the native type. The HDF5
		// library automatically converts between different floating point
		// types.
		if ((file.getFID() >= 0) && (dataspace_id >= 0)) {
			dataset_id = H5.H5Dcreate(file.getFID(), parentGroup.getFullName() + "/" + datasetName, HDF5Constants.H5T_IEEE_F64LE, dataspace_id, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT,
					HDF5Constants.H5P_DEFAULT);
		}

		// Write the data to the dataset.
		H5.H5Dwrite(dataset_id, HDF5Constants.H5T_NATIVE_FLOAT, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL, HDF5Constants.H5P_DEFAULT, data);

		// End access to the dataset and release resources used by it.
		H5.H5Dclose(dataset_id);

		// Terminate access to the data space.
		H5.H5Sclose(dataspace_id);
	}

	public static void writeFloatDataSet(H5File sourceFile, H5Group parentGroup, String datasetName, float[][] buf) throws NullPointerException, HDF5Exception {

		long[] dims = new long[2];
		if (buf != null) {
			dims[0] = buf.length;
			if (buf.length > 0) {
				dims[1] = buf[0].length;
			}
		}
		int filespace_id = H5.H5Screate_simple(2, dims, null);
		int dataset_id = H5.H5Dcreate(sourceFile.getFID(), parentGroup.getFullName() + "/" + datasetName, HDF5Constants.H5T_STD_I32LE, filespace_id, HDF5Constants.H5P_DEFAULT,
				HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);

		// Write the data to the dataset.
		H5.H5Dwrite(dataset_id, HDF5Constants.H5T_NATIVE_FLOAT, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL, HDF5Constants.H5P_DEFAULT, buf);

		// End access to the dataset and release resources used by it.
		H5.H5Dclose(dataset_id);
	}

	public static void writeStringDataSet(H5File file, H5Group parentGroup, String datasetName, String[] str_data) throws NullPointerException, HDF5Exception {
		int type_id = -1;
		int dataspace_id = -1;
		int dataset_id = -1;
		int rank = 1;

		long[] dims = { str_data.length };

		type_id = H5.H5Tcopy(HDF5Constants.H5T_C_S1);
		H5.H5Tset_size(type_id, HDF5Constants.H5T_VARIABLE);

		// Create dataspace. Setting maximum size to NULL sets the maximum
		// size to be the current size.
		dataspace_id = H5.H5Screate_simple(rank, dims, null);

		// Create the dataset and write the string data to it.
		if ((file.getFID() >= 0) && (type_id >= 0) && (dataspace_id >= 0)) {
			dataset_id = H5.H5Dcreate(file.getFID(), parentGroup.getFullName() + "/" + datasetName, type_id, dataspace_id, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT,
					HDF5Constants.H5P_DEFAULT);
		}

		// Write the data to the dataset.
		H5.H5DwriteString(dataset_id, type_id, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL, HDF5Constants.H5P_DEFAULT, str_data);

		H5.H5Sclose(dataspace_id);
		H5.H5Tclose(type_id);
		H5.H5Dclose(dataset_id);
	}

	//
	// public static String[] readStringDataSet(H5File file, String parentGroupName, String datasetName, int
	// numberOfStrings) throws NullPointerException, HDF5Exception {
	//
	// int did = H5.H5Dopen(file.getFID(), parentGroupName + "/" + datasetName, HDF5Constants.H5P_DEFAULT);
	// int tid = H5.H5Dget_type(did);
	// int sid = H5.H5Dget_space(did);
	//
	// int stringLength = (int) H5.H5Tget_size(tid);
	// int bufferSize = numberOfStrings * stringLength;
	// byte[] byteBuff = new byte[bufferSize];
	//
	// // read the string data into byte buff
	// int mspace = HDF5Constants.H5S_ALL;
	// int fspace = HDF5Constants.H5S_ALL;
	// int plist = HDF5Constants.H5P_DEFAULT;
	// H5.H5Dread(did, tid, mspace, fspace, plist, byteBuff);
	//
	// // convert byte array into string array
	// String[] strOut = new String[numberOfStrings];
	// for (int i = 0; i < numberOfStrings; i++) {
	// strOut[i] = new String(byteBuff, i * stringLength, stringLength).trim();
	// }
	//
	// H5.H5Sclose(sid);
	// H5.H5Sclose(did);
	// H5.H5Tclose(tid);
	//
	// return strOut;
	// }

	public static void writeBooleanDataSet(H5File file, String parentGroupName, String datasetName, boolean[] data) throws HDF5LibraryException, NullPointerException, HDF5Exception {
		int dataspace_id = -1;
		int dataset_id = -1;
		int rank = 1;
		long[] dims = { data.length };

		int dataint[] = new int[data.length];
		for (int i = 0; i < data.length; i++) {
			dataint[i] = data[i] ? 1 : 0;
		}

		// Create dataspace. Setting maximum size to NULL sets the maximum
		// size to be the current size.
		dataspace_id = H5.H5Screate_simple(rank, dims, null);

		// Create the dataset and write the integer data to it. In this
		// example we will save the data as 64 bit little endian integers,
		// regardless of the native integer type. The HDF5 library
		// automatically converts between different integer types.
		if ((file.getFID() >= 0) && (dataspace_id >= 0)) {
			dataset_id = H5.H5Dcreate(file.getFID(), parentGroupName + "/" + datasetName, HDF5Constants.H5T_STD_I64LE, dataspace_id, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT,
					HDF5Constants.H5P_DEFAULT);
		}

		// Write the data to the dataset.
		H5.H5Dwrite(dataset_id, HDF5Constants.H5T_NATIVE_INT8, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL, HDF5Constants.H5P_DEFAULT, dataint);

		// End access to the dataset and release resources used by it.
		H5.H5Dclose(dataset_id);

		// Terminate access to the data space.
		H5.H5Sclose(dataspace_id);
	}

	public static void writeByteDataSet(H5File file, String parentGroupName, String datasetName, byte[] data) throws HDF5Exception {
		int dataspace_id = -1;
		int dataset_id = -1;
		int rank = 1;
		long[] dims = { data.length };

		int[] dataInt = new int[data.length];
		for (int i = 0; i < data.length; i++) {
			dataInt[i] = data[i];
		}

		// Create dataspace. Setting maximum size to NULL sets the maximum
		// size to be the current size.
		dataspace_id = H5.H5Screate_simple(rank, dims, null);

		// Create the dataset and write the integer data to it. In this
		// example we will save the data as 64 bit little endian integers,
		// regardless of the native integer type. The HDF5 library
		// automatically converts between different integer types.
		if ((file.getFID() >= 0) && (dataspace_id >= 0)) {
			dataset_id = H5.H5Dcreate(file.getFID(), parentGroupName + "/" + datasetName, HDF5Constants.H5T_STD_I64LE, dataspace_id, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT,
					HDF5Constants.H5P_DEFAULT);
		}

		// Write the data to the dataset.
		int[] values = new int[data.length];
		for (int i = 0; i < values.length; i++) {
			values[i] = data[i];
		}
		H5.H5Dwrite(dataset_id, HDF5Constants.H5T_NATIVE_INT, HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL, HDF5Constants.H5P_DEFAULT, values);

		// End access to the dataset and release resources used by it.
		H5.H5Dclose(dataset_id);

		// Terminate access to the data space.
		H5.H5Sclose(dataspace_id);
	}

	public static void addStringAttributeToGroup(H5File file, H5Group group, String name, String value) throws Exception {

		// Create a String array of size one to hold the value
		String[] values = new String[] { value };

		// Create a byte array from values using the stringToByte method
		byte[] bvalue = Dataset.stringToByte(values, value.length());

		// Create a custom String data type for the value
		H5Datatype datatype = (H5Datatype) file.createDatatype(Datatype.CLASS_STRING, bvalue.length, Datatype.NATIVE, Datatype.NATIVE);

		// 1D of size 1
		long[] dims = { 1 };

		// Create an attribute object
		Attribute attribute = new Attribute(name, datatype, dims);

		// Set the value of the attribute to bvalue
		attribute.setValue(bvalue);

		// Write the attribute to the group's metadata
		group.writeMetadata(attribute);
	}

	public static void addDoubleAttributeToGroup(H5File file, H5Group group, String name, double value) throws Exception {

		addDoubleAttributeToGroup(file, group, name, new double[] { value });
	}

	public static void addDoubleAttributeToGroup(H5File file, H5Group group, String name, double[] values) throws Exception {

		// Create a custom String data type for the value
		H5Datatype datatype = (H5Datatype) file.createDatatype(Datatype.CLASS_FLOAT, 8, Datatype.NATIVE, Datatype.NATIVE);

		// 1D of values size
		long[] dims = { values.length };

		// Create an attribute object
		Attribute attribute = new Attribute(name, datatype, dims);

		// Set the value of the attribute to bvalue
		attribute.setValue(values);

		// Write the attribute to the group's metadata
		group.writeMetadata(attribute);
	}

	public static void addLongAttributeToGroup(H5File file, H5Group group, String name, long value) throws Exception {
		addLongAttributeToGroup(file, group, name, new long[] { value });
	}

	public static void addLongAttributeToGroup(H5File file, H5Group group, String name, long[] values) throws Exception {
		// Create a custom String data type for the value
		H5Datatype datatype = (H5Datatype) file.createDatatype(Datatype.CLASS_INTEGER, 8, Datatype.NATIVE, Datatype.NATIVE);

		// 1D of values size
		long[] dims = { values.length };

		// Create an attribute object
		Attribute attribute = new Attribute(name, datatype, dims);

		// Set the value of the attribute to bvalue
		attribute.setValue(values);

		// Write the attribute to the group's metadata
		group.writeMetadata(attribute);

	}

	public static void addIntegerAttributeToGroup(H5File file, H5Group group, String name, int value) throws Exception {
		addIntegerAttributeToGroup(file, group, name, new int[] { value });
	}

	public static void addIntegerAttributeToGroup(H5File file, H5Group group, String name, int[] values) throws Exception {

		// Create a custom String data type for the value
		H5Datatype datatype = (H5Datatype) file.createDatatype(Datatype.CLASS_INTEGER, 4, Datatype.NATIVE, Datatype.NATIVE);

		// 1D of values size
		long[] dims = { values.length };

		// Create an attribute object
		Attribute attribute = new Attribute(name, datatype, dims);

		// Set the value of the attribute to bvalue
		attribute.setValue(values);

		// Write the attribute to the group's metadata
		group.writeMetadata(attribute);
	}

}
