package fr.ifremer.globe.consolelog;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.EclipseContextFactory;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import fr.ifremer.globe.consolelog.model.EventLogData;
import fr.ifremer.globe.consolelog.preference.EventLogSettings;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "fr.ifremer.globe.consolelog"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;

	// The console model
	private EventLogData logs;

	// The preference node
	private static EventLogSettings param;

	public static EventLogSettings getPluginSettings() {
		return param;
	}

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext )
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		// declare ConsolelogPreference node on root node
		PreferenceComposite root = PreferenceRegistry.getInstance().getViewsSettingsNode();
		param = new EventLogSettings(root, "Console log");
		// WARNING : log must be created after preferences:
		logs = ContextInjectionFactory.make(EventLogData.class, EclipseContextFactory.getServiceContext(context));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext )
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	public EventLogData getLogs() {
		return logs;
	}

}
