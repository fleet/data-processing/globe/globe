package fr.ifremer.globe.consolelog;

import org.apache.log4j.spi.LoggingEvent;

/**
 * {@link IObserver} interface. Provides methods for a class that needs to
 * observe.
 */
public interface IObserver {

	/**
	 * Define a behavior when an {@link LoggingEvent} occurs.
	 * 
	 * @param evt
	 *            , the event to handle.
	 */
	public void update(LoggingEvent evt);
}
