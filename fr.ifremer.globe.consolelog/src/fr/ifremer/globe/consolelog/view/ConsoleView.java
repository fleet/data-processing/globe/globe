package fr.ifremer.globe.consolelog.view;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;

import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBar;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import fr.ifremer.globe.consolelog.Activator;
import fr.ifremer.globe.consolelog.model.EventLogData;
import fr.ifremer.globe.consolelog.preference.EventLogSettings;

/**
 * ConsoleView Class. ConsoleView displays logs (from an {@link EventLogData} object) in a table.
 */

public class ConsoleView {

	/** The model where logs are stored */
	private EventLogData data;
	/** The parent composite where the console should be attached */
	private Composite parent;
	/** The table object where the logs will be displayed */
	private Table table;
	/** A boolean to lock/unlock the console auto-scroll */
	private boolean locked;
	private Observer observer;

	/**
	 * Flag indicating if packing was already done
	 */
	private boolean packed = false;

	/**
	 * Top index used when console is locked
	 */
	private int topIndex = 0;
	private EventLogSettings settings;
	private MToolBar toolbar;

	/**
	 * ConsoleView constructor.
	 * 
	 * @param parent is the parent composite where the console should be attached.
	 */
	@Inject
	public ConsoleView(Composite parent) {

		// Get the model from the plugin activator.
		// Logs are saved since the plugin has been initialized.
		if (Activator.getDefault() != null) {
			this.data = Activator.getDefault().getLogs();
			this.settings = data.getSettings();
		} else {
			this.data = new EventLogData();
			settings = data.getSettings();
		}
		this.observer = new Observer() {

			@Override
			public void update(Observable arg0, Object arg1) {
				// Check if the table has been disposed to avoid adding new
				// elements if the receiver's resources have been released.
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						if (!table.isDisposed()) {
							table.clearAll();
							table.setItemCount(data.getLogs().size());
							if (isLocked()) {
								table.setTopIndex(topIndex);
							} else {
								int datasize = table.getItemCount();
								if (datasize > 0) {
									table.showItem(table.getItem(datasize - 1));
								}
							}
						}
					}
				});

			}

		};
		this.data.addObserver(observer);
		this.parent = parent;
		this.locked = false;
	}

	@PreDestroy
	public void dispose() {
		this.data.deleteObserver(observer);
		System.err.println("Remove Observer");
	}

	/**
	 * Initialize the view before rendering the console.
	 */
	@PostConstruct
	public void createView() {

		parent.setLayout(new FillLayout());

		// Initialize a new virtual table.
		// Virtual tables allow to quickly create tables with large amounts of
		// data and populate them efficiently.
		table = new Table(parent, SWT.BORDER | SWT.VIRTUAL);

		// Add 4 columns in order to sort the different part of each log.
		TableColumn dateColumn = new TableColumn(table, SWT.NONE);
		dateColumn.setText("Date");
		TableColumn levelColumn = new TableColumn(table, SWT.NONE);
		levelColumn.setText("Level");
		// TableColumn originColumn = new TableColumn(table, SWT.NONE);
		// originColumn.setText("Origin");
		TableColumn detailsColumn = new TableColumn(table, SWT.NONE);
		detailsColumn.setText("Details");

		// Pack columns to display column headers properly.
		for (int i = 0; i < table.getColumnCount(); i++) {
			table.getColumn(i).pack();
		}
		// Then, display headers.
		table.setHeaderVisible(true);

		// Add a new listener to the table so we can quickly add new elements in
		// the table
		table.addListener(SWT.SetData, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (toolbar != null) {
					checkFiltersState(toolbar);
				}
				TableItem item = (TableItem) event.item;
				LoggingEvent log = data.getLogs().get(event.index);

				// Red foreground for error logs.
				if (log.getLevel() == Level.ERROR) {
					item.setForeground(new Color(Display.getCurrent(), 255, 0, 0));
				}
				// Red foreground for error logs.
				if (log.getLevel() == Level.WARN) {
					item.setForeground(new Color(Display.getCurrent(), 0, 0, 255));
				}
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

				// Fill cells.
				item.setText(0, sdf.format(new Date(log.getTimeStamp())));
				item.setText(1, log.getLevel().toString());
				// item.setText(2, log.getLoggerName());
				String displayedMsg = "No message";
				if (log.getMessage() != null) {
					displayedMsg = log.getMessage().toString();
				}
				// check if an exception was passed
				ThrowableInformation eInfo = log.getThrowableInformation();
				if (eInfo != null) {
					displayedMsg += " " + eInfo.getThrowable().getMessage();
				}
				item.setText(2, displayedMsg);

				if (!packed) {
					packed = true;

					table.getColumn(0).pack();
					table.getColumn(1).pack();
					table.getColumn(2).pack();
				}

			}
		});
		table.setItemCount(data.getLogs().size());
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		data.heightHint = 200;
		table.setLayoutData(data);
	}

	/**
	 * Tell if the console scroll is locked or not.
	 * 
	 * @return a boolean that indicates if the console scroll is locked or not.
	 */
	public boolean isLocked() {
		return locked;
	}

	/**
	 * Lock or unlock the console auto-scroll. When the console is unlocked, the top index (focused element) is set to
	 * the latest element each time a new element is added. Otherwise, the console scroll is never moved automatically.
	 * 
	 * @param locked
	 */
	public void setLocked(boolean locked) {
		this.locked = locked;
		this.topIndex = table.getTopIndex();
	}

	/**
	 * Allows to check the state of filter buttons.
	 * 
	 * @param toolbar , the console log toolbar.
	 * @param level , the last level selected.
	 */
	public void checkFiltersState(MToolBar toolbar) {

		// Buttons position in the toolbar.
		// lock button = 0
		// clear button = 1
		// separator = 2
		int infoButtonPosition = 3;
		int debugButtonPosition = 6;
		int warningButtonPosition = 4;
		int errorButtonPosition = 5;

		((MToolItem) toolbar.getChildren().get(infoButtonPosition)).setSelected(settings.isLogInfo());
		((MToolItem) toolbar.getChildren().get(debugButtonPosition)).setSelected(settings.isLogDebug());
		((MToolItem) toolbar.getChildren().get(errorButtonPosition)).setSelected(settings.isLogError());
		((MToolItem) toolbar.getChildren().get(warningButtonPosition)).setSelected(settings.isLogWarning());

	}

	public void setToolBar(MToolBar toolbar) {
		this.toolbar = toolbar;
	}
}
