package fr.ifremer.globe.consolelog.model;

import java.util.ArrayList;
import java.util.Observable;

import jakarta.inject.Inject;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.eclipse.e4.core.services.events.IEventBroker;

import fr.ifremer.globe.consolelog.Activator;
import fr.ifremer.globe.consolelog.IObserver;
import fr.ifremer.globe.consolelog.appender.GlobeConsoleAppender;
import fr.ifremer.globe.consolelog.preference.EventLogSettings;
import fr.ifremer.globe.ui.events.UpdateStatusBarErrorIndicator;

/**
 * EventLogData Class. EventLogData allows to store logs collected by {@link GlobeConsoleAppender}. Moreover, a
 * displaying level allows to filter logs by level.
 */
public class EventLogData extends Observable {

	@Inject
	private IEventBroker eventBroker;

	/** The initial maximum number of logs stored */
	private final int MAX_LOGS = 10000;
	/** The list where the logs are stored */
	private ArrayList<LoggingEvent> logs;
	/** The sublist which represent what is displayed */
	private ArrayList<LoggingEvent> logsToDisplay;
	/** The appender to observe in order to fill logs data */
	private GlobeConsoleAppender appender;
	/** The object where log settings are stored */
	private EventLogSettings settings;

	public EventLogSettings getSettings() {
		return settings;
	}

	public void settingsChanged() {
		updateLogsToDisplay(true);
	}

	/**
	 * EventLogData constructor.
	 * 
	 * @param level , the maximum log level allowed.
	 */
	public EventLogData() {
		this.settings = Activator.getPluginSettings();

		this.logs = new ArrayList<LoggingEvent>();
		this.logs.ensureCapacity(MAX_LOGS);
		this.logsToDisplay = new ArrayList<LoggingEvent>();
		this.logsToDisplay.ensureCapacity(MAX_LOGS);

		// Create a new appender.
		this.appender = new GlobeConsoleAppender(Level.ALL);
		this.appender.addObserver(new IObserver() {
			@Override
			public void update(LoggingEvent evt) {
				// Add the new LoggingEvent to the main list.
				logs.add(evt);
				if (evt.getLevel() == Level.ERROR)
					eventBroker.post(UpdateStatusBarErrorIndicator.EVENT, new UpdateStatusBarErrorIndicator(true));

				// Update the display list without changing the log level
				updateLogsToDisplay(false);

				// Notify observers.
				setChanged();
				notifyObservers();
			}
		});

		// Register the appender to the main logger.
		Logger.getLogger("fr.ifremer").addAppender(appender);
	}

	/**
	 * This methods allows to update the log display list.
	 * 
	 * @param levelHasChanged is a boolean that indicates if the log level has changed or not.
	 */
	private void updateLogsToDisplay(boolean levelHasChanged) {

		// If the log level has changed, we need to rebuilt the display list.
		if (levelHasChanged) {
			// Clear it.
			logsToDisplay.clear();

			for (LoggingEvent evt : logs) {
				addLog(evt);
			}
		}
		// Otherwise, simply update the display list.
		else {
			// Get the last added log.
			LoggingEvent lastEvt = logs.get(logs.size() - 1);

			addLog(lastEvt);

		}

		// Notify observers that the displaying list has changed.
		this.setChanged();
		this.notifyObservers();
	}

	private void addLog(LoggingEvent evt) {
		if (evt.getLevel() == Level.ERROR && settings.isLogError()) {
			logsToDisplay.add(evt);
		} else if (evt.getLevel() == Level.DEBUG && settings.isLogDebug()) {
			logsToDisplay.add(evt);
		} else if (evt.getLevel() == Level.WARN && settings.isLogWarning()) {
			logsToDisplay.add(evt);

		} else if (settings.isLogInfo() /*
										 * add all log whatever their level except debug and error
										 */) {
			if (evt.getLevel() != Level.DEBUG && evt.getLevel() != Level.ERROR && evt.getLevel() != Level.WARN) {
				logsToDisplay.add(evt);
			}
		}
	}

	/**
	 * Clear all logs and notify observers
	 */
	public void clear() {
		// Clear logs.
		logs.clear();
		logsToDisplay.clear();
		eventBroker.post(UpdateStatusBarErrorIndicator.EVENT, new UpdateStatusBarErrorIndicator(false));
		// Notify observers.
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * This method allows to get the logs to display, according to the displayingLevel.
	 * 
	 * @return the logs to display.
	 */
	public ArrayList<LoggingEvent> getLogs() {
		return logsToDisplay;
	}

}
