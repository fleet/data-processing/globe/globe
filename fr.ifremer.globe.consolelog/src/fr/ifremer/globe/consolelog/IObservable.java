package fr.ifremer.globe.consolelog;

import org.apache.log4j.spi.LoggingEvent;

/**
 * {@link IObservable} interface. Provides methods for a class that needs to be
 * observed.
 */
public interface IObservable {

	/**
	 * Add a new {@link IObserver} to this {@link IObservable}.
	 * 
	 * @param obs
	 *            , the new observer.
	 */
	public void addObserver(IObserver obs);

	/**
	 * Update all registered {@link IObserver} when a {@link LoggingEvent}
	 * occurs.
	 * 
	 * @param evt
	 *            , the event to transmit.
	 */
	public void updateObserver(LoggingEvent evt);

	/**
	 * Remove all observers from this {@link IObservable}.
	 */
	public void delObservers();
}
