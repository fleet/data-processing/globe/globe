package fr.ifremer.globe.consolelog.preference;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;

public class EventLogSettings extends PreferenceComposite {

	private BooleanPreferenceAttribute error;
	private BooleanPreferenceAttribute warning;
	private BooleanPreferenceAttribute info;
	private BooleanPreferenceAttribute debug;

	public EventLogSettings(PreferenceComposite superNode, String nodeName) {
		super(superNode, nodeName);

		// defaultLogMask = MASK_INFO | MASK_ERROR | MASK_WARNING;
		error = new BooleanPreferenceAttribute("error", "Log ERROR", true);
		warning = new BooleanPreferenceAttribute("warning", "Log WARNING", true);
		info = new BooleanPreferenceAttribute("info", "Log INFO", true);
		debug = new BooleanPreferenceAttribute("debug", "Log DEBUG", false);

		this.declareAttribute(error);
		this.declareAttribute(warning);
		this.declareAttribute(info);
		this.declareAttribute(debug);
		// The consolelog plugin is used before the root node loading call
		// we load local node
		this.load();
	}

	public boolean isLogError() {
		return error.getValue();
	}

	public boolean isLogWarning() {
		return warning.getValue();
	}

	public boolean isLogInfo() {
		return info.getValue();
	}

	public boolean isLogDebug() {
		return debug.getValue();
	}

	public void setLogError(boolean set) {
		error.setValue(set, true);
		this.save();
	}

	public void setLogWarning(boolean set) {
		warning.setValue(set, true);
		this.save();

	}

	public void setLogInfo(boolean set) {
		info.setValue(set, true);
		this.save();

	}

	public void setLogDebug(boolean set) {
		debug.setValue(set, true);
		this.save();

	}
}
