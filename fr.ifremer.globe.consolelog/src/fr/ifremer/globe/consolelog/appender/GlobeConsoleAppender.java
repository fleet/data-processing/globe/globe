package fr.ifremer.globe.consolelog.appender;

import java.util.ArrayList;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;

import fr.ifremer.globe.consolelog.IObservable;
import fr.ifremer.globe.consolelog.IObserver;

/**
 * GlobeConsoleAppender Class. GlobeConsoleAppender gathers log message from
 * Log4j and is observable. Observers register to this class and display the
 * logs.
 */
public class GlobeConsoleAppender extends AppenderSkeleton implements IObservable {

	/** The list of registered observers */
	protected ArrayList<IObserver> observers = new ArrayList<IObserver>();

	/**
	 * GlobeConsoleAppender constructor. Default constructor.
	 */
	public GlobeConsoleAppender() {
		this(Level.ALL);
	}

	/**
	 * GlobeConsoleAppender constructor.
	 * 
	 * @param level
	 *            , all log events with lower level than this one are ignored by
	 *            the appender.
	 */
	public GlobeConsoleAppender(Level level) {
		this.setThreshold(level);
	}

	/**
	 * 
	 */
	@Override
	public void close() {
		super.closed = false;
	}

	/**
	 * Configurators call this method to determine if the appender requires a
	 * layout.
	 * 
	 * @return false , because this appender don't need any layout
	 */
	@Override
	public boolean requiresLayout() {
		return false;
	}

	/**
	 * 
	 */
	@Override
	protected void append(LoggingEvent event) {
		this.updateObserver(event);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addObserver(IObserver obs) {
		observers.add(obs);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateObserver(LoggingEvent event) {
		for (IObserver ob : observers) {
			ob.update(event);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delObservers() {
		observers.clear();
	}

}
