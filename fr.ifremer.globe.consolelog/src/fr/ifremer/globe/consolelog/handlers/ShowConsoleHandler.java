package fr.ifremer.globe.consolelog.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.consolelog.view.ConsoleView;
import fr.ifremer.globe.ui.handler.PartManager;

public class ShowConsoleHandler {
	static String partId = "fr.ifremer.globe.consolelog";

	@Execute
	public void execute(ESelectionService selectionService, EPartService partService, MApplication application, EModelService modelService) {
		
		MPart part = PartManager.createPart(partService, partId);
		PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_STACK_ID);
		PartManager.showPart(partService, part);

		if (part != null) {
			

			partService.showPart(part, PartState.VISIBLE);
			// Set selection

			ConsoleView view = (ConsoleView) part.getObject();
			if (view != null) {
				view.setToolBar(part.getToolbar());
				view.checkFiltersState(part.getToolbar());
			}

		}

	}
}
