package fr.ifremer.globe.consolelog.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.consolelog.Activator;
import fr.ifremer.globe.consolelog.model.EventLogData;
import fr.ifremer.globe.consolelog.preference.EventLogSettings;
import fr.ifremer.globe.consolelog.view.ConsoleView;

public class DebugLogHandler {

	@Execute
	public void execute(EPartService partService) {

		MPart part = partService.findPart("fr.ifremer.globe.consolelog");
		ConsoleView console = (ConsoleView) part.getObject();

		Activator activator = Activator.getDefault();
		if (activator != null) {
			EventLogData data = activator.getLogs();
			if (data != null) {
				EventLogSettings settings = data.getSettings();
				settings.setLogDebug(!settings.isLogDebug());
				data.settingsChanged();
				console.checkFiltersState(part.getToolbar());
			}
		}
	}
}
