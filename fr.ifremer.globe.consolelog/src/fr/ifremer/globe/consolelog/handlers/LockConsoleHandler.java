package fr.ifremer.globe.consolelog.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import fr.ifremer.globe.consolelog.view.ConsoleView;

public class LockConsoleHandler {

	@Execute
	public void execute(EPartService partService) {

		MPart part = partService.findPart("fr.ifremer.globe.consolelog");

		if (part != null) {
			ConsoleView console = (ConsoleView) part.getObject();
			if (console != null) {
				console.setLocked(!console.isLocked());
			}
		}
	}
}
