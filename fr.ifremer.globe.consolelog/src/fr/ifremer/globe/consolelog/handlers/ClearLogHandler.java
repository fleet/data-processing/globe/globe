package fr.ifremer.globe.consolelog.handlers;

import org.eclipse.e4.core.di.annotations.Execute;

import fr.ifremer.globe.consolelog.Activator;
import fr.ifremer.globe.consolelog.model.EventLogData;

public class ClearLogHandler {

	@Execute
	public void execute() {

		Activator activator = Activator.getDefault();
		if (activator != null) {
			EventLogData data = activator.getLogs();
			if (data != null) {
				data.clear();
			}
		}
	}
}
