//package fr.ifremer.globe.benchmark;
//
//import java.io.File;
//
//import org.eclipse.core.runtime.NullProgressMonitor;
//import org.junit.Assert;
//import org.junit.Test;
//
//import fr.ifremer.globe.cibmbes.MBGDriver;
//import fr.ifremer.globe.genericdriver.sounder.driver.SounderDriverInfo;
//import fr.ifremer.globe.model.datastore.loading.ILoadingKey;
//import fr.ifremer.globe.model.datastore.loading.LoadingKeyFactory;
//import fr.ifremer.globe.model.mbes.CycleContainer;
//import fr.ifremer.globe.editor.spatial.IndexFileUtils;
//import fr.ifremer.globe.editor.spatial.grid.ConsumerKey;
//import fr.ifremer.globe.editor.spatial.grid.GridIndexer;
//import fr.ifremer.globe.utils.exception.GException;
//import fr.ifremer.globe.utils.perfcounter.PerfoCounter;
//
//public class BenchIndexCreate {
//	public static final String TEST_FILE = "C:/Globe/donnees/TEST_GLOBE_PERFORMANCE/upd_bobgosmf24_0054.mbg";
//
//	/**
//	 * Check that all soundings can be retrieved from index
//	 * */
//	@Test
//	public void testGridNodeManager() throws GException {
//		
//		ConsumerKey.getKey("GridTest.testGridNodeManager");
//		File srcFile = new File(TEST_FILE);
//		Assert.assertTrue("MBG file not found : " + srcFile.getPath(), srcFile.isFile());
//		MBGDriver driver = new MBGDriver();
//
//		final SounderDriverInfo info = driver.initData(srcFile.getAbsolutePath());
//		GridIndexer indexer = new GridIndexer(info);
//		File index=IndexFileUtils.getIndexFile(info);
//		if(index.exists())
//		{
//			Assert.assertTrue("Could not delete file :"+index.getAbsolutePath(),index.delete());
//		}
//		ILoadingKey loadingKey = LoadingKeyFactory.BATHY_LOADING_KEY;
//		CycleContainer dataStore = (CycleContainer) driver.loadDataStore(loadingKey, info);
//
//		// Create index
//		PerfoCounter perf = new PerfoCounter("create index");
//		perf.start();
//		indexer.indexStore(dataStore, new NullProgressMonitor());
//		perf.stop().print();;
//
//		driver.release(loadingKey, info);
//		driver.dispose(info);
//
//		return;
//	}
//	
//}