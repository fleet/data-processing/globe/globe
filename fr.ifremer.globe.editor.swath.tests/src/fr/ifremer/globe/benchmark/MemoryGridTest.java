//package fr.ifremer.globe.benchmark;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.eclipse.core.runtime.NullProgressMonitor;
//import org.junit.Assert;
//import org.junit.Test;
//
//import fr.ifremer.globe.cibmbes.MBGDriver;
//import fr.ifremer.globe.genericdriver.sounder.driver.SounderDriverInfo;
//import fr.ifremer.globe.model.GeoPoint;
//import fr.ifremer.globe.model.IDataStore;
//import fr.ifremer.globe.model.datastore.loading.ILoadingKey;
//import fr.ifremer.globe.model.datastore.loading.LoadingKeyFactory;
//import fr.ifremer.globe.model.mbes.CycleContainer;
//import fr.ifremer.globe.editor.spatial.IndexFileUtils;
//import fr.ifremer.globe.editor.spatial.grid.ConsumerKey;
//import fr.ifremer.globe.editor.spatial.grid.GridIndexer;
//import fr.ifremer.globe.editor.spatial.grid.GridNode;
//import fr.ifremer.globe.editor.spatial.grid.SpatialIndex;
//import fr.ifremer.globe.utils.FileUtils;
//import fr.ifremer.globe.utils.exception.GException;
//import fr.ifremer.globe.utils.exception.GIOException;
///**
// * Memory grid test
// * */
//public class MemoryGridTest {
//
//	
//	
//	
//	public static final String TEST_FILE = "C:/Globe/donnees/TEST_GLOBE_PERFORMANCE/upd_bobgosmf24_0056.mbg";
//	
//	/**
//	 * Loop on file creation, needed to be checked in regard with windows taskmanager to see if memory is increasing (due to a memory leak in hdf lib)
//	 * */
//	@Test
//	public void testIndexMemoryConsumptionOnFileCreate() throws GIOException {
//
//
//		File srcFile = new File(TEST_FILE);
//		Assert.assertTrue("MBG file not found : " + srcFile.getPath(), srcFile.isFile());
//
//		MBGDriver driver = new MBGDriver();
//
//		final SounderDriverInfo info = driver.initData(srcFile.getAbsolutePath());
//		ILoadingKey loadingKey = LoadingKeyFactory.BATHY_LOADING_KEY;
//		IDataStore dataStore = driver.loadDataStore(loadingKey, info);
//		final CycleContainer beamStore = (CycleContainer) dataStore;
//
//		for(int i=0;i<100;i++)
//		{
//			GridIndexer indexer = new GridIndexer(info);
//
//			// Create index
//			indexer.indexStore(beamStore, new NullProgressMonitor());
//			indexer.dispose();
//			FileUtils.delete(IndexFileUtils.getIndexFile(info));
//
//			Assert.assertFalse(IndexFileUtils.getIndexFile(info).exists());
//			dumMemory();
//		}
//
//		driver.release(loadingKey, info);
//	}
//	
//	/**
//	 * Loop on file query, needed to be checked in regard with windows taskmanager to see if memory is increasing (due to a memory leak in hdf lib)
//	 * */
//	@Test
//	public void testIndexMemoryConsumption() throws GException {
//
//
//		// Test ok if non hdf file is rejected
//
//		File srcFile = new File(TEST_FILE);
//		Assert.assertTrue("MBG file not found : " + srcFile.getPath(), srcFile.isFile());
//
//		MBGDriver driver = new MBGDriver();
//
//		dumMemory();
//		final SounderDriverInfo info = driver.initData(srcFile.getAbsolutePath());
//		ILoadingKey loadingKey = LoadingKeyFactory.BATHY_LOADING_KEY;
//		IDataStore dataStore = driver.loadDataStore(loadingKey, info);
//
//		final CycleContainer beamStore = (CycleContainer) dataStore;
//
//		GridIndexer indexer = new GridIndexer(info);
//
//		// Create index
//		indexer.indexStore(beamStore, new NullProgressMonitor());
//		ConsumerKey key=ConsumerKey.getKey("MemoryGridTest.testIndexMemoryConsumption");
//
//		GeoPoint point = new GeoPoint(0, 0);
//
//
//		// loop on soundings
//		for (int i = 0; i < info.getCycleCount(); i++) {
//			for (int j = 0; j < info.getTotalRxBeamCount(); j++) {
//
//				double lon = beamStore.getRxAntennas().getBeam(j).getLongitude(i);
//				double lat = beamStore.getRxAntennas().getBeam(j).getLatitude(i);
//				point.setLat(lat);
//				point.setLong(lon);
//				if (!point.isInside(info.getGeoBox())) {
//					continue;
//				}
//
//				SpatialIndex v = new SpatialIndex();
//				indexer.computeIndex(lat, lon, v);
//				List<SpatialIndex> vlist = new ArrayList<SpatialIndex>();
//				vlist.add(v);
//				List<GridNode> nodeList=indexer.retrieveNodes(vlist,key);
//				indexer.release(nodeList,key);
//			}
//			dumMemory();
//			
//		}	
//		driver.release(loadingKey, info);
//		indexer.dispose();
//	}
//
//	private void dumMemory()  
//	{
//		System.err.println("Runtime  Memory Used   (Ko)"+Runtime.getRuntime().totalMemory()/1024.d );
//
//	}
//}
