package fr.ifremer.globe.editor.spatial.tests;
//package fr.ifremer.globe.editor.spatial.tests;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
//import org.eclipse.core.runtime.NullProgressMonitor;
//import org.junit.Assert;
//import org.junit.Test;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import fr.ifremer.globe.cibmbes.MBGDriver;
//import fr.ifremer.globe.genericdriver.sounder.driver.SounderDriverInfo;
//import fr.ifremer.globe.model.GeoBox;
//import fr.ifremer.globe.model.GeoPoint;
//import fr.ifremer.globe.model.GeoZone;
//import fr.ifremer.globe.model.IDataStore;
//import fr.ifremer.globe.model.datastore.loading.ILoadingKey;
//import fr.ifremer.globe.model.datastore.loading.LoadingKeyFactory;
//import fr.ifremer.globe.model.mbes.CycleContainer;
//import fr.ifremer.globe.editor.spatial.IndexFileUtils;
//import fr.ifremer.globe.editor.spatial.commons.CheckStatus;
//import fr.ifremer.globe.editor.spatial.grid.ConsumerKey;
//import fr.ifremer.globe.editor.spatial.grid.GridIndexer;
//import fr.ifremer.globe.editor.spatial.grid.GridNode;
//import fr.ifremer.globe.editor.spatial.grid.GridNodeManager;
//import fr.ifremer.globe.editor.spatial.grid.GridNodes;
//import fr.ifremer.globe.editor.spatial.grid.SpatialIndex;
//import fr.ifremer.globe.utils.FileUtils;
//import fr.ifremer.globe.utils.exception.GException;
//import fr.ifremer.globe.utils.exception.GIOException;
//import fr.ifremer.globe.utils.perfcounter.PerfoCounter;
//import fr.ifremer.globe.utils.perfcounter.PerfoCounterEntry.Measure;
///**
// * Test for indexer
// * */
//public class GridTest {
//
//	public static final String TEST_FILE = "data/Tm7111.mbg";
//	public static final String TEST_INDEX_OK = "data/Tm7111.mbg.index";
//	public static final String TEST_INDEX_NOK = "data/Tm7111.nok.index";
//	public static final String TEST_DIRECTORY = "data";
//	private static final Logger logger = LoggerFactory.getLogger(GridTest.class);
//
//	/**
//	 * Validate the {@link GridNodeManager#checkFile()} function
//	 * */
//	@Test
//	public void testIndexChecker() throws GException {
//		
//		File srcFile = new File(TEST_FILE);
//		Assert.assertTrue("MBG file not found : " + srcFile.getPath(), srcFile.isFile());
//		MBGDriver driver = new MBGDriver();
//
//		final SounderDriverInfo info = driver.initData(srcFile.getAbsolutePath());
//
//		File indexFile = IndexFileUtils.getIndexFile(info);
//		
//		GridNodeManager gridNodeManager=new GridNodeManager(info);
//		if(indexFile.isDirectory())
//			FileUtils.deleteDir(indexFile);
//		else
//			indexFile.delete();
//		FileUtils.copyDirectory(new File(TEST_DIRECTORY), indexFile);
//		try{
//			// Test ok if directory is rejected
//			gridNodeManager.checkFile(-1);
//			Assert.assertTrue("Directory should be rejected",false );
//		} catch (GIOException e){
//			//standart behaviour we should come here
//		}
//		FileUtils.deleteDir(indexFile);
//
//		FileUtils.copy(new File(TEST_FILE), indexFile);
//		try{
//			// Test ok if directory is rejected
//			// Test ok if non hdf file is rejected
//			gridNodeManager.checkFile(-1);
//			//an exception shall occur
//			Assert.assertFalse(true);
//		} catch (GIOException e){
//			//standart behaviour we should come here
//		}
//
//		FileUtils.delete(indexFile);
//		FileUtils.copy(new File(TEST_INDEX_NOK), indexFile);
//		// Test ok if hdf file with a wrong version is rejected
//		Assert.assertTrue(gridNodeManager.checkFile(-1)!=CheckStatus.FILE_OK);
//
//		FileUtils.delete(indexFile);
//		FileUtils.copy(new File(TEST_INDEX_OK), indexFile);
//		// Test ok if hdf file with a good version is accepted
//		Assert.assertTrue(gridNodeManager.checkFile(502211487)==CheckStatus.FILE_OK);
//
//		FileUtils.delete(indexFile);
//
//	}
//	/**
//	 * Check that all soundings can be retrieved from index
//	 * */
//	@Test
//	public void testGridNodeManager() throws GException {
//		ConsumerKey key=ConsumerKey.getKey("GridTest.testGridNodeManager");
//		File srcFile = new File(TEST_FILE);
//		Assert.assertTrue("MBG file not found : " + srcFile.getPath(), srcFile.isFile());
//
//		MBGDriver driver = new MBGDriver();
//
//		final SounderDriverInfo info = driver.initData(srcFile.getAbsolutePath());
//		GridIndexer indexer = new GridIndexer(info);
//		ILoadingKey loadingKey = LoadingKeyFactory.BATHY_LOADING_KEY;
//		IDataStore dataStore = driver.loadDataStore(loadingKey, info);
//		final CycleContainer beamStore = (CycleContainer) dataStore;
//
//		// Create index
//		PerfoCounter perf = new PerfoCounter("create index");
//		perf.start();
//		indexer.indexStore(beamStore, new NullProgressMonitor());
//		perf.stop();
//
//		// now parse data and check that we find all ping beam
//		int maxNodeCount = 50;
//		int nbSoundings = 0;
//		int nbRetrievedSoundings = 0;
//		GeoBox boundingbox = info.getGeoBox();
//		double latitudeStep = info.getGeoBox().getHeight() / maxNodeCount;
//		double longitudeStep = info.getGeoBox().getWidth() / maxNodeCount;
//		DescriptiveStatistics stats = new DescriptiveStatistics();
//		double startLongitude = boundingbox.getWestLongitude();
//
//		// count soundings in geo-frame
//		GeoPoint point = new GeoPoint(0, 0);
//		for (int i = 0; i < info.getCycleCount(); i++) {
//			for (int j = 0; j < info.getTotalRxBeamCount(); j++) {
//
//				double lon = beamStore.getRxAntennas().getBeam(j).getLongitude(i);
//				double lat = beamStore.getRxAntennas().getBeam(j).getLatitude(i);
//				point.setLat(lat);
//				point.setLong(lon);
//				if (point.isInside(info.getGeoBox())) {
//					nbSoundings++;
//				}
//			}
//		}
//		indexer.getNodeManager();
//		for (short x = 0; x < GridNodes.MaxNodeCount; x++) {
//			indexer.getNodeManager();
//			for (short z = 0; z < GridNodes.MaxNodeCount; z++) {
//					
//				GridNode node=indexer.getNodeManager().loadNode(x, z,key);
//				nbRetrievedSoundings+=node.size();
//				
//			}
//		}
//		indexer.releaseAll(key);
//
//		// Check that all soundings were retrieved
//		Assert.assertEquals(nbSoundings,nbRetrievedSoundings );
//		nbRetrievedSoundings=0;
//		
//		// compute and fill subgrid grid
//		List<GridNode> retrievedNodes = new ArrayList<>();
//		for (int x = 0; x < maxNodeCount; x++) {
//
//			double westLongitude = startLongitude;
//			double eastLongitude = startLongitude + longitudeStep;
//			// ensure rounding issues
//			if (x == maxNodeCount - 1) {
//				eastLongitude = boundingbox.getEastLongitude();
//			}
//			double startLatitude = boundingbox.getSouthLatitude();
//			for (int y = 0; y < maxNodeCount; y++) {
//
//				double southLatitude = startLatitude;
//				double northLatitude = southLatitude + latitudeStep;
//				//
//				if (y == maxNodeCount - 1) {
//					northLatitude = boundingbox.getNorthLatitude();
//				}
//				GeoBox box = new GeoBox(northLatitude, southLatitude, eastLongitude, westLongitude);
//				PerfoCounter perfSearch = new PerfoCounter("create index");
//				perfSearch.start();
//				List<GridNode> nodes = indexer.retrieveNodes(new GeoZone(box),key,true);
//				Measure m = perfSearch.stop();
//				stats.addValue(m.getTimeElapsed());
//
//				// Count soundings from new retrieved nodes
//				for (GridNode node : nodes) {
//					if (!retrievedNodes.contains(node)) {
//						nbRetrievedSoundings += node.size();
//						retrievedNodes.add(node);
//					}
//				}
//				indexer.release(nodes,key);
//				startLatitude = northLatitude;
//			}
//			startLongitude = eastLongitude;
//		}
//		logger.info("Search Time Mean:" + stats.getMean() + " Max:" + stats.getMax() + " Std:" + stats.getStandardDeviation());
//
//		info.getDriver().release(loadingKey, info);
//
//		// Check that all soundings were retrieved
//		Assert.assertEquals(nbSoundings,nbRetrievedSoundings );
//
//		return;
//	}
//
//	/**
//	 * check that each sounding can be retrieve in its matching index cell
//	 * */
//	@Test
//	public void testRetrieveNodesSpatialIndex() throws GException {
//		ConsumerKey key=ConsumerKey.getKey("GridTest.testRetrieveNodesSpatialIndex");
//
//		File srcFile = new File(TEST_FILE);
//		Assert.assertTrue("MBG file not found : " + srcFile.getPath(), srcFile.isFile());
//
//		MBGDriver driver = new MBGDriver();
//
//		final SounderDriverInfo info = driver.initData(srcFile.getAbsolutePath());
//		GridIndexer indexer = new GridIndexer(info);
//		ILoadingKey loadingKey = LoadingKeyFactory.BATHY_LOADING_KEY;
//		IDataStore dataStore = driver.loadDataStore(loadingKey, info);
//		final CycleContainer beamStore = (CycleContainer) dataStore;
//
//		// Create index
//		PerfoCounter perf = new PerfoCounter("create index");
//		perf.start();
//		indexer.indexStore(beamStore, new NullProgressMonitor());
//		perf.stop();
//
//		GeoPoint point = new GeoPoint(0, 0);
//
//		// loop on soundings
//		for (int i = 0; i < info.getCycleCount(); i++) {
//			for (int j = 0; j < info.getTotalRxBeamCount(); j++) {
//
//					double lon = beamStore.getRxAntennas().getBeam(j).getLongitude(i);
//					double lat = beamStore.getRxAntennas().getBeam(j).getLatitude(i);
//					point.setLat(lat);
//					point.setLong(lon);
//					if (!point.isInside(info.getGeoBox())) {
//						continue;
//					}
//
//					SpatialIndex v = new SpatialIndex();
//					indexer.computeIndex(lat, lon, v);
//					List<SpatialIndex> vlist = new ArrayList<SpatialIndex>();
//					vlist.add(v);
//					List<GridNode> nodeList = indexer.retrieveNodes(vlist,key);
//					// now check that we really find our sounding in the cell
//					Assert.assertEquals(1, nodeList.size());
//					GridNode node = nodeList.get(0);
//					boolean found = false;
//					for (int k = 0; k < node.size(); k++) {
//						// find matchin ping beam
//						if (node.getBeam().get(k) == (short) j && node.getCycle().get(k) == i) {
//							found = true;
//						}
//					}
//					indexer.release(nodeList,key);
//					Assert.assertTrue(found);
//			}
//		}
//		info.getDriver().release(loadingKey, info);
//		indexer.dispose();
//		return;
//	}
//}
