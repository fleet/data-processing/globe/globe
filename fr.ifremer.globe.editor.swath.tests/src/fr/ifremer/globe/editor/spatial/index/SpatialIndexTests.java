package fr.ifremer.globe.editor.spatial.index;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.EnumSet;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.e4.ui.internal.workbench.E4Workbench;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runners.MethodSorters;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.geo.GeoPoint;
import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.CompositeCSLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.ByteLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatus;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.SonarDetectionStatusDetail;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.editor.swath.application.context.ContextInitializer;
import fr.ifremer.globe.editor.swath.application.context.ContextNames;
import fr.ifremer.globe.editor.swath.model.spatialindex.Coordinates;
import fr.ifremer.globe.editor.swath.model.spatialindex.CoordinatesUtils;
import fr.ifremer.globe.editor.swath.model.spatialindex.SpatialIndex;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.MappingValidity;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.Mappings;
import fr.ifremer.globe.editor.swath.model.spatialindex.mapping.ValidityMapper;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParameters;
import fr.ifremer.globe.editor.swath.ui.wizard.SwathEditorParametersFactory;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Testing cases for class SpatialIndex.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SpatialIndexTests implements IDataContainerOwner {
	/** File to test. */
	public static final String MBG_FILE1 = GlobeTestUtil.getTestDataPath() + "/file/upd_bobgosmf24_0054.mbg";
	/** File to test. */
	public static final String MBG_FILE2 = GlobeTestUtil.getTestDataPath()
			+ "/file/Tide_20130322_204233_PP-7150-24kHz-CIB.mbg";

	protected ISounderNcInfo info;
	protected SpatialIndex spatialIndex;
	protected SounderDataContainer dataContainer;
	ISounderDataContainerToken token;

	@BeforeClass
	public static void setUp() {
		ContextInitializer.startup(E4Workbench.getServiceContext());
		ContextInitializer.prepareNewEdition();
		TemporaryCache.cleanAllFiles();
	}

	@Rule
	public TemporaryFolder tmpFolder = TemporaryFolder.builder().assureDeletion().build();

	/**
	 * Build a Mappings according to MBG_FILE2 file. <br>
	 * Check that all attributes are properly mapped.
	 */
	@Test
	public void checkMappingGeneration() throws Exception {
		initialize(MBG_FILE2);
		SwathEditorParameters parameters = ContextInitializer.getInContext(ContextNames.SWATH_EDITOR_PARAMETERS);

		// Get sounding positions with the right coordinate system
		CompositeCSLayers cSLayers = dataContainer.getCsLayers(CoordinateSystem.FCS);
		DoubleLayer2D latLayer = cSLayers.getProjectedX();
		DoubleLayer2D lonLayer = cSLayers.getProjectedY();
		DoubleLayer2D depthLayer = cSLayers.getProjectedZ();
		DetectionStatusLayer statusLayer = dataContainer.getStatusLayer();
		ByteLoadableLayer2D ampPhaseDetectionLayer = dataContainer.hasLayer(BathymetryLayers.DETECTION_TYPE)
				? dataContainer.getLayer(BathymetryLayers.DETECTION_TYPE)
				: null;
		FloatLoadableLayer2D reflectivityLayer = dataContainer.getLayer(BathymetryLayers.DETECTION_BACKSCATTER_R);
		FloatLoadableLayer2D qualityFactorLayer = dataContainer.getLayer(BathymetryLayers.DETECTION_QUALITY_FACTOR);

		spatialIndex.indexStore(new NullProgressMonitor());
		Mappings mappings = spatialIndex.getMappings();
		int totalRxBeamCount = info.getTotalRxBeamCount();
		for (int cycleIndex = 0; cycleIndex < info.getCycleCount(); cycleIndex++) {
			for (int beamIndex = 0; beamIndex < totalRxBeamCount; beamIndex++) {
				if (statusLayer.isDetectionEditable(cycleIndex, beamIndex)) {
					double[] xy = parameters.projection.project(lonLayer.get(cycleIndex, beamIndex),
							latLayer.get(cycleIndex, beamIndex));
					GeoPoint geoPoint = new GeoPoint(xy[0], xy[1]);
					Coordinates coord = new Coordinates();
					CoordinatesUtils.project(coord, geoPoint, parameters);

					// Beam belonging to the grid ?
					if (CoordinatesUtils.contains(coord, parameters)) {
						// How many beams in the cell ?
						int cellBeamCount = mappings.getCellBeamCount(coord.line, coord.col);
						Assert.assertTrue(cellBeamCount > 0);

						// Search the index of the beam in the cell
						int cellBeamIndex = 0;
						do {
							int mappedBeamIndex = mappings.getBeamIndex(coord.line, coord.col, cellBeamIndex);
							long mappedCycleIndex = mappings.getCycleIndex(coord.line, coord.col, cellBeamIndex);
							if (mappedBeamIndex == beamIndex && mappedCycleIndex == cycleIndex) {
								break;
							}
							cellBeamIndex++;
						} while (cellBeamIndex < cellBeamCount);
						Assert.assertTrue("Beam not found in the mapping. CycleIndex = " + cycleIndex + ". beamIndex = "
								+ beamIndex, cellBeamIndex < cellBeamCount);

						// Compare each values
						Assert.assertEquals((float) -depthLayer.get(cycleIndex, beamIndex),
								mappings.getDepth(coord.line, coord.col, cellBeamIndex), 0f);
						Assert.assertEquals(reflectivityLayer.get(cycleIndex, beamIndex),
								mappings.getReflectivity(coord.line, coord.col, cellBeamIndex), 0f);
						Assert.assertEquals(ValidityMapper.get(statusLayer, cycleIndex, beamIndex),
								mappings.getValidity(coord.line, coord.col, cellBeamIndex));
						Assert.assertEquals(ampPhaseDetectionLayer.get(cycleIndex, beamIndex),
								mappings.getDetection(coord.line, coord.col, cellBeamIndex), 0f);
						Assert.assertEquals(qualityFactorLayer.get(cycleIndex, beamIndex),
								mappings.getQualityFactor(coord.line, coord.col, cellBeamIndex), 0f);
						Assert.assertEquals((float) xy[0], mappings.getX(coord.line, coord.col, cellBeamIndex), 0f);
						Assert.assertEquals((float) xy[1], mappings.getY(coord.line, coord.col, cellBeamIndex), 0f);
					}
				}
			}
		}
	}

	@After
	public void clean() {
		if (dataContainer != null) {
			dataContainer.dispose();
		}
		if (spatialIndex != null) {
			spatialIndex.dispose();
		}
		if (token != null) {
			token.close();
		}
	}

	/**
	 * Build a Mappings according to MBG_FILE1 file. <br>
	 * Check the content
	 */
	@Test
	public void generateMapping() throws Exception {
		initialize(MBG_FILE1);
		spatialIndex.indexStore(new NullProgressMonitor());
		Mappings mappings = spatialIndex.getMappings();
		checkMapping(mappings);
	}

	/**
	 * Open a Mappings . <br>
	 * Modify some validities, save the result and check.
	 */
	@Test
	public void verifyUpdateStore() throws Exception {

		File original = new File(MBG_FILE1);
		File workingCopy = File.createTempFile(getClass().getName() + "_" + System.currentTimeMillis(),
				original.getName(), tmpFolder.getRoot());
		workingCopy.deleteOnExit();
		Files.copy(original.toPath(), workingCopy.toPath(), StandardCopyOption.REPLACE_EXISTING);

		initialize(workingCopy.getAbsolutePath());

		// Load the mapping
		spatialIndex.indexStore(new NullProgressMonitor());

		// Change all beam's validity
		Mappings mappings = spatialIndex.getMappings();

		int firstCycle = 1000;
		int lastCycle = firstCycle + 500;
		for (long index = 0; index < mappings.getSoundingCount(); index++) {
			int cycle = mappings.getCycleIndex(index);
			if (cycle >= firstCycle && cycle <= lastCycle) {
				mappings.setValidity(index, MappingValidity.SND_UNVALID_AUTO);
			}
		}

		// Save
		spatialIndex.updateStore(new NullProgressMonitor());

		// Reload
		clean();
		initialize(workingCopy.getAbsolutePath());

		// check
		int beamCount = dataContainer.getInfo().getTotalRxBeamCount();
		DetectionStatusLayer statusLayer = dataContainer.getStatusLayer();

		for (int iSwath = firstCycle; iSwath <= lastCycle; iSwath++) {
			for (int iBeam = 0; iBeam < beamCount; iBeam++) {
				if (statusLayer.isDetectionEditable(iSwath, iBeam)) {

					EnumSet<SonarDetectionStatus> status = statusLayer.getStatus(iSwath, iBeam);
					SonarDetectionStatusDetail details = statusLayer.getDetails(iSwath, iBeam);
					Assert.assertTrue(
							String.format("Bad validity (%s,%s) for cycle %d and beam %d ", status.toString(),
									details.toString(), iSwath, iBeam),
							// See ValidityMapper.updateStatusLayer
							// Nominal case
							status.contains(SonarDetectionStatus.REJECTED) && details == SonarDetectionStatusDetail.AUTO
									// Unchanged status
									|| details == SonarDetectionStatusDetail.UNKNOWN);
				}
			}
		}
	}

	/**
	 * Check the data.
	 */
	protected void checkMapping(Mappings mapping) {
		float minX = Float.POSITIVE_INFINITY;
		float maxX = Float.NEGATIVE_INFINITY;
		double sumX = 0d;
		float minY = Float.POSITIVE_INFINITY;
		float maxY = Float.NEGATIVE_INFINITY;
		double sumY = 0d;
		float minDepth = Float.POSITIVE_INFINITY;
		float maxDepth = Float.NEGATIVE_INFINITY;
		double sumDepth = 0d;
		float minReflectivity = Float.POSITIVE_INFINITY;
		float maxReflectivity = Float.NEGATIVE_INFINITY;
		double sumReflectivity = 0d;
		int validBathymetrie = 0;
		int invalidBathymetrie = 0;
		int trueAmplitudePhaseDetection = 0;
		int falseAmplitudePhaseDetection = 0;
		int minCycleIndex = Integer.MAX_VALUE;
		int maxCycleIndex = Integer.MIN_VALUE;
		long sumCycleIndex = 0L;

		SwathEditorParameters parameters = ContextInitializer.getInContext(ContextNames.SWATH_EDITOR_PARAMETERS);

		long totalBeamCount = 0;
		for (int line = 0; line < parameters.rowCount; line++) {
			for (int col = 0; col < parameters.columnCount; col++) {
				totalBeamCount += mapping.getCellBeamCount(line, col);
			}
		}
		for (int line = 0; line < parameters.rowCount; line++) {
			for (int col = 0; col < parameters.columnCount; col++) {
				if (mapping.getCellBeamCount(line, col) > 0) {
					long offset = mapping.getCellOffset(line, col);
					Assert.assertTrue("neg offset " + offset, offset >= 0);
					Assert.assertTrue("high offset " + offset + " vs " + totalBeamCount, offset <= totalBeamCount);
				}
			}
		}
		Assert.assertEquals(mapping.getSoundingCount(), totalBeamCount);

		for (int soundingIndex = 0; soundingIndex < mapping.getSoundingCount(); soundingIndex++) {

			float x = mapping.getX(soundingIndex);
			float y = mapping.getY(soundingIndex);
			float depth = mapping.getDepth(soundingIndex);
			float reflectivity = mapping.getReflectivity(soundingIndex);
			MappingValidity valid = mapping.getValidity(soundingIndex);
			boolean detection = mapping.isAmplitudeDetection(soundingIndex);
			int ci = mapping.getCycleIndex(soundingIndex);

			minX = Math.min(minX, x);
			maxX = Math.max(maxX, x);
			sumX += x;
			minY = Math.min(minY, y);
			maxY = Math.max(maxY, y);
			sumY += y;
			minDepth = Math.min(minDepth, depth);
			maxDepth = Math.max(maxDepth, depth);
			sumDepth += depth;
			minReflectivity = Math.min(minReflectivity, reflectivity);
			maxReflectivity = Math.max(maxReflectivity, reflectivity);
			sumReflectivity += reflectivity;
			validBathymetrie += MappingValidity.isValid(valid) ? 1 : 0;
			invalidBathymetrie += !MappingValidity.isValid(valid) ? 1 : 0;
			trueAmplitudePhaseDetection += detection ? 1 : 0;
			falseAmplitudePhaseDetection += !detection ? 1 : 0;
			minCycleIndex = Math.min(minCycleIndex, ci);
			maxCycleIndex = Math.max(maxCycleIndex, ci);
			sumCycleIndex += ci;
		}

		Assert.assertEquals(-45492.17f, minX, 0.1f);
		Assert.assertEquals(46032.02f, maxX, 0.1f);
		Assert.assertEquals(7.431806606948886E10, sumX, 0.1d);

		Assert.assertEquals(-39217.55f, minY, 0.1f);
		Assert.assertEquals(39582.8f, maxY, 0.1f);
		Assert.assertEquals(-6.041235744851282E10, sumY, 0.1d);

		Assert.assertEquals(-2565.0f, minDepth, 0f);
		Assert.assertEquals(0.0f, maxDepth, 0f);
		Assert.assertEquals(-7.881747256768269E9, sumDepth, 0d);

		Assert.assertEquals(0.0f, minReflectivity, 0f);
		Assert.assertEquals(11.5f, maxReflectivity, 0f);
		Assert.assertEquals(7.66322605E7, sumReflectivity, 0d);

		Assert.assertEquals(1318736, invalidBathymetrie);
		Assert.assertEquals(10051951, validBathymetrie);

		Assert.assertEquals(4154279, trueAmplitudePhaseDetection);
		Assert.assertEquals(7216408, falseAmplitudePhaseDetection);

		Assert.assertEquals(0, minCycleIndex);
		Assert.assertEquals(31399, maxCycleIndex);
		Assert.assertEquals(163027906543L, sumCycleIndex);
	}

	protected void initialize(String resource) throws GException {
		info = (ISounderNcInfo) IFileService.grab().getFileInfo(resource).orElse(null);
		Assert.assertNotNull(info);

		token = IDataContainerFactory.grab().book(info, this);

		SwathEditorParameters swathEditorParameters = SwathEditorParametersFactory
				.create(Collections.singletonList(info));
		ContextInitializer.setInContext(ContextNames.SWATH_EDITOR_PARAMETERS, swathEditorParameters);

		dataContainer = token.getDataContainer();
		spatialIndex = ContextInitializer.make(SpatialIndex.class);
		spatialIndex.initialize(dataContainer);
	}
}
