import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.ifremer.globe.utils.cache.TemporaryCache;


/**
 * Test suite for maven , declared test will be launched while compiling with
 * command mvn integration-test
 * 
 * */
@RunWith(Suite.class)
@SuiteClasses({ AllTests.class })
public class MvnTestSuite {
	@AfterClass
	public static void cleanUp() {
		TemporaryCache.cleanAllFiles();
	}
}
