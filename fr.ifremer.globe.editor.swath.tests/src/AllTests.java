
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.ifremer.globe.editor.spatial.index.SpatialIndexTests;

@RunWith(Suite.class)
@SuiteClasses({ SpatialIndexTests.class })
public class AllTests {

}
