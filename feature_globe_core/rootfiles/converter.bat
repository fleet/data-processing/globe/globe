%echo off
REM run globe.exe with no splash screen and add a --convert option
%echo on
%~dp0/globec.exe -nosplash --convert %*
