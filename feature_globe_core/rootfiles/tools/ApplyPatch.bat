@echo off

echo Please wait jshell launcher, this could take a minute...
REM find jshell
set jshell_exe=jshell
FOR /f "tokens=*" %%G IN ('WHERE /R "..\plugins" "jshell.exe"') do (set jshell_exe=%%G) 

%jshell_exe% ./patch/patchApplier.java
