import java.io.File;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*

public class JFilePicker extends JPanel {
	private String textFieldLabel;
	private String buttonLabel;
	
	private JLabel label;
	private JTextField textField;
	private JButton button;
	
	private JFileChooser fileChooser;
	
	private int mode;
	public static final int MODE_OPEN = 1;
	public static final int MODE_SAVE = 2;
	
	public JFilePicker(String textFieldLabel, String buttonLabel) {
		this.textFieldLabel = textFieldLabel;
		this.buttonLabel = buttonLabel;
		
		fileChooser = new JFileChooser();
		
		SpringLayout layout = new SpringLayout();
        setLayout(layout);
		
		// creates the GUI
		label = new JLabel(textFieldLabel);
		
		textField = new JTextField();
		button = new JButton(buttonLabel);
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				buttonActionPerformed(evt);				
			}
		});
		
		add(label);
		add(textField);
		add(button);
		//horizontal alignments
		layout.putConstraint(SpringLayout.WEST, label, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.WEST, textField, 5, SpringLayout.EAST, label);
		layout.putConstraint(SpringLayout.EAST, textField, -5, SpringLayout.WEST, button);
		layout.putConstraint(SpringLayout.EAST, button, -5, SpringLayout.EAST, this);
		//vertical alignments
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, label, 0, SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, textField, 0, SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, button, 0, SpringLayout.VERTICAL_CENTER, this);
	}
	
	private void buttonActionPerformed(ActionEvent evt) {
		if (mode == MODE_OPEN) {
			if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				textField.setText(fileChooser.getSelectedFile().getAbsolutePath());
			}
		} else if (mode == MODE_SAVE) {
			if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
				textField.setText(fileChooser.getSelectedFile().getAbsolutePath());
			}
		}
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public void setFile(File file) {
		this.textField.setText(file.getAbsolutePath());
	}
	
	public String getSelectedFilePath() {
		return textField.getText();
	}
	
	public JFileChooser getFileChooser() {
		return this.fileChooser;
	}
}
new JFrame() // Possibly the first attempt fails, so
var frame = new JFrame()
UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())
frame.setTitle("Patch applier for Globe")
frame.setSize(600, 200)
frame.setLayout(new GridLayout(0,1))
frame.setLocationRelativeTo(null)
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
Path globeDir = Path.of("..").toAbsolutePath().normalize()
Path globeParentDir = Path.of("../..").toAbsolutePath().normalize()

//Creates the GUI
JFilePicker patchPicker = new JFilePicker("Select a patch file (P) :", "Browse...")
patchPicker.setMode(JFilePicker.MODE_OPEN)
patchPicker.getFileChooser().setCurrentDirectory(globeParentDir.toFile())
frame.add(patchPicker)

JFilePicker sourcePicker = new JFilePicker("Source Globe directory (S) :", "Browse...")
sourcePicker.setMode(JFilePicker.MODE_OPEN)
sourcePicker.setFile(globeDir.toFile())
sourcePicker.getFileChooser().setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY)
sourcePicker.getFileChooser().setCurrentDirectory(globeParentDir.toFile())
frame.add(sourcePicker)

JFilePicker destinationPicker = new JFilePicker("Parent Globe directory (R) :", "Browse...")
destinationPicker.setMode(JFilePicker.MODE_OPEN)
destinationPicker.setFile(globeParentDir.toFile())
destinationPicker.getFileChooser().setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY)
destinationPicker.getFileChooser().setCurrentDirectory(globeParentDir.getParent().toFile())
frame.add(destinationPicker)

JPanel newVersionPanel = new JPanel()
newVersionPanel.setLayout(new FlowLayout())
JLabel newVersionLabel = new JLabel("New Globe version (V) :")
JTextField newVersionTextField = new JTextField(30)
newVersionTextField.setText("X.X.X-Globe-windows")

newVersionPanel.add(newVersionLabel)
newVersionPanel.add(newVersionTextField)
frame.add(newVersionPanel)

//SUMMARY
var button = new JButton("Apply patch (P) from (S) to (R/V)")
button.addActionListener(x -> {
	Path hpatchPath = Path.of("patch/hdiffpatch_v3.0.8/windows64/hpatchz");
	Path sourceDirPath = Path.of(sourcePicker.getSelectedFilePath());
	Path patchPath = Path.of(patchPicker.getSelectedFilePath());
	Path destDirPath = Path.of(destinationPicker.getSelectedFilePath()+ "/" + newVersionTextField.getText());
	System.out.println("");
	System.out.println("\tApply patch : " + patchPath);
	System.out.println("\tFrom : " + sourceDirPath);
	System.out.println("\tTo : " + destDirPath);
	System.out.println("");
		
	String commandLine = new String(hpatchPath + " " + sourceDirPath + " " + patchPath + " " + destDirPath);
	ProcessBuilder pb = new ProcessBuilder(commandLine.split(" "));
	pb.directory(new File("."));
	pb.redirectErrorStream(true);
	try {
		Process process = pb.start();
		BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));
        String line;
		while ((line = reader.readLine()) != null) {
			System.out.println(line);
		}
		reader.close();
		int exitVal = process.waitFor();
		if (exitVal == 0) {
			System.out.println("");
			System.out.println("Patch successfully applied. You can close the shell.");
			System.out.println("");
			System.exit(0);
		}
    } catch (IOException  | InterruptedException e) {
		e.printStackTrace();
    }
	System.out.println("Apply patch finished with error. Check parameters.");
	})
frame.add(button)
frame.setVisible(true)
