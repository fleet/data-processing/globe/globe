#!/bin/bash

echo Globe Launcher script to be called from Globe directory\n

#uncomment to globally activate miniconda
#source $PWD/bin/activate base

# setup proj lib for java use
export PROJ_DATA=$PWD/plugins/share/proj
# set env to link with gdal deps (solve issue with lidstdc++.so.6)
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/plugins/org.gdal.linux.x86_64_2.4.4
# uncomment to use emulated opengl
#export LIBGL_ALWAYS_SOFTWARE=1

#run Globe
./Globe


