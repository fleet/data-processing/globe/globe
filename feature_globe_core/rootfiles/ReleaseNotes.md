# Version 2.5.15
26/02/2025
* Update to converter version 1.2.13
* Backscatter corrections : Add support for Kongsberg EM1002 + robustify processes


# Version 2.5.14
04/02/2025
* 'Apply navigation' on TECHSAS file : new attributes for navigation variables, and new option to filter NVI to keep only valid data. 
* ChartEditor : Support bsar.nc file evolution for calibration.
* Geographic view: improvement of mercator DTM display (fix grid alignment).
* USBL: new Python tool to convert USBL file data to CSV (before display in GLOBE).
* NavShift Editor: fixed and improved automatic shift vector calculation (now handles multi-part isobaths and overlapping survey lines).

# Version 2.5.13
20/12/2024
* Sonar-NetCDF converter : fix error with the conversion from s7k (with datagram 7027).
* Measure tool : Use right click to remove point

# Version 2.5.12
09/12/2024
* USBL layer : USBL : keep only valid data to display landing & refresh display after edition in ChartEditor.
* TECHSAS apply navigation : bug in version 2.5.10 fixed.

# Version 2.5.11
06/12/2024
* WC : fix echo spatialisation in case of sonar head mounted with 180 heading.
* Upgrade XSF : bug fix for upgrading XSF from s7k without WC.
* Fix regression importing data from CSV in 2.5.10

# Version 2.5.10
22/11/2024
* DTM "Reset Cell" process : Add missing value filter for DTM ResetCells process
* Logbook : improve loading of report from previous version of GLOBE.
* Fix detection filter process to allow filtering of infinite values
* Update to converter version 1.2.13

# Version 2.5.9
08/11/2024
* New feature to display and analyze USBL positions : loading of USBL position file (.csv), display of target and landing positions, export info to JSON report file...

## Xsf Converter 
* Update to converter version 1.2.12
* S7K->XSF : Fix computation of txTransducerDepth layer.
* Fix kmall->XSF : potentially wrong MRU/Position ids.

# Version 2.5.8
17/09/2024

## Viewers
* NavShift Editor : new feature to compute shift vectors based on isobath registration.
* Navigation layer : Add acquisition mode in variable color choices (loaded on demand).
* 2D Viewer : display current value under cursor (texture, WC, ...).
* Direct load of DelphiNS CSV file.
* NVI : accept NVI file with uncomplete optional layers.
* Geographic view : tooltips stay open until mouse move.
* IsobathParameters : default ratio main/secondary to 5.

## Toolbox / Processes
* Cut / Merge (XSF/Techsas/Nvi files) : new option to apply cut from a geographic mask file (.shp, .geojson, .kml...).
* Export point cloud CSV to DTM (batch mode) : new specific page for "spatial resolution" option. 
* New process "Automatic navigation shift" : shifts sounder file navigation using reference DTM isobaths as ground thruth.
* New process "Compute isobath" : computes isobaths from DTM, and produce shapefiles.

## Water Column
* WC Filters : add filtering by range, filtering by multiping id.
* WC display : normalization by range has now a fix output value (can be changed).

## Xsf Converter 
* Update to v1.2.10
* Allow conversion of uncomplete .all

# Version 2.5.7
29/08/2024
* Geographic View : tooltip feature for DTM.
* DTM display : shading enabled by default.
* XSF : min/max depth in Properties view.
* GoTo feature : refactoring to handle properly underwater target.
* SEGY : new display layer and opening in 2D Viewer.
* Better management of files located around the antimeridian.
* NetCDF Viewer : keep filters when selected variable changed.
* Geographic Box wizard page : save selected projection in preferences. 
* Export DTM to XYZ : new options to select separator, decimal character and column order.
* Export point cloud CSV to DTM (batch mode) : new option "spatial resolution". 

# Version 2.5.6
23/07/2024

## Toolbox
* Bug fixed with toolbox when GLOBE directory has been moved after product unzip (path to miniconda and environment are now relative).
* Bug fixed with a GLOBE install in a directory whose path contains space or special characters.
* New button to describe Toolbox state (on/off/loading...) and allows to relaunch if off. 
* Process launching : display a warning message if input parameters contains non-ASCII characters. 

## Swath Editor
* New selection feature "lasso".
* Update of keyboard shortcuts (space == left click; enter = right click).
* New color option "keep invalid red".
* Tools windows (filtri, detection filter...) can now be moved and attached in part views.
* Improvement of "auto-scale" after update (bug fixed with horizontal bounds, and improvement of bounding box computing).

## Others
* Optimization of "Upgrade NVI" process (.nvi to .nvi.nc).
* Fix Xsf Upgrade : the cut process no more adds one leading cell.
* WC filters : Fix loading of angleCoefficient from JSON.
* Export to XSF : keep user parameters in memory (new "reset" button).
* TECHSAS : handle of '.fluo' extension.
* Geobox page : bug fixed with "Evaluate from input file" when input file coordinate system differs from output geobox.


# Version 2.5.5
26/06/2024
## Xsf Converter 
* Update to v1.2.8
* Bug fixed with merge/cut tool : Use IndividualSensor datagram epoch time as an ID and sort criteria
## PyAT
* Optimize launch time for pyat processes
* Fix compute of multiple files geobox
* DTM > Reset Cell & Gap Filling : new option to reverse the geographic mask (apply reset outside polygons).

# Version 2.5.4
21/06/2024
* Bug fixed with GLOBE installed in a directory path containing spaces.
* Point Cloud (CSV) new features : variable size, labels, scale widget.
* New preferences "Color palettes" (define default color palette for different topics : bathymetry, watercolumn...). 

# Version 2.5.3
28/05/2024
## Water Column
* Bottom detection filtering uses now interpolation to complete missing/invalid data.
* Improve spatialization accuracy in case of tilted WC.
* Add polar echograms export (G3D).
## Backscatter 
* Reduce memory load for BS sliding process.
## DTM
* GEBCO WMS background : update URL and fix request issue .
* Export Point Cloud CSV to DTM : allow to select several input CSV files to create 1 DTM.
* Merge Fill : check undefined CDI : improvement of error message.
## Others
* "Output file" wizard page : new button to select a file to overwrite.
* Click on "No network" allows to switch to "online" mode.  

# Version 2.5.2
30/04/2024
* Geographic View : raster layer shading is now based on elevation layer for all layers.

# Version 2.5.1
11/04/2024
* Embedded Globe Web Service (GWS): server to execute all processes of the "Toolbox" (which replaces the previous "PyToolbox").
* New "Logbook" feature : historic view of all processes. Allows to analyze, load/save, relaunch processes...
* New "Display EMODnet CVE annotation" feature : allows to display a layer in background which contains EMODnet CVE annotations.
* Project Explorer : new option to group DTM by provider ID 
* XSF Cut / Merge : 
- changed cut interval management (previous version retained immediately preceding cut value, whatever the variable), and account for last value only slice
- introduced state variable, which value needs to be kept before the cut occurs (such as SSP)

[Bug fixes]
* Wizard output file selection : bug fixed with the group selection when 'load after' option is enabled.
* Swath Editor : bug fixed with layer refresh (some layers, like "elevation min" were not correctly refreshed).

[PyAT]
* Interpolation Tools (Coronis) : integration of the version 1.0.3 (with GPU options...) & update of CDI interpolation (use "nearest" method which is faster).
* Reset Cell : new options to apply specific filter by layer, to define a "AND" or "OR" logic between filters, and to define filter from several CDI.
* Export CSV to DTM : new process to execute the conversion in "batch mode" with an automatic compute of geographic bounds.
* Merge Fill : new option to keep an empty border between DTMs (for a custom interpolation in a second time…).
* Backscatter : rename rolling process in sliding and fix memory overload.

[Xsf Converter]
* XSF Converter is now embedded from an external project.
* All/Kmall Converter : in case of dual head sounders, swath with incomplete data are totally skipped.
* S7K -> XSF Converter : enable the parsing of datagram 1015.

# Version 2.4.5
22/02/2024
* Apply validity flag : try to retrieve swath from date (if pair index-datetime of validity flag does not match file content)
* S7K -> XSF Converter : enable the parsing of datagram 1015
* SwathEditor : fix reflectivity bounds computation causing flat grey colormap

# Version 2.4.4
01/02/2024
* Improved XSF split/merge process.
* WC Flatten layer : avoid texture pikes due to sample_interval changes.
* Update XSF Version (0.6) : move snippets for XSF from Reson (s7k)
* S7k2Xsf : Fix converter to have proper file without WC if asked 

# Version 2.4.3
24/01/2024
* all2Xsf : Fix multiping variable computation
* s7k2Xsf : Fix heading and depth datagram reading (7009)
* DTM : Use gray colormap as default for backscatter layer
* [PyAT] Backscatter : Optimization of renormalization process. Improve discontinuities management during rolling process.
* [PyAT] New tool to create cutfile from a list of XSF files
* [PyAT] Fix Upgrade/Merge XSF process to handle dimension changes between and inside files (ex: txBeam)

# Version 2.4.2
15/12/2023
* DTM : add spatial antialising option based on linear interpolation of cells centroid
* Backscatter : Bug fixed dealing with unordered timed data + Add reference angle parameters on rolling renormalisation process 

# Version 2.4.1
08/12/2023
* KML : accept KML file with description field not in GLOBE format (json)
* Line Editor > Turn filter : bug fixed with Python methods (the 'mean' method was always used, even if other selected)
* PyTechsas 0.4 : compute theorical mag with more precise date & bug fixed to handle NetCDF4 files (output of cut/merge) 
* PyNvi 0.7 : export navigation to CSV with milliseconds
* DTM : Fix STDDEV layer computation to avoid numerical issues using double precision bufers. Cells with only one value have now a zero value.
* Cut/Merge (XSF) : Fix chunksize greater than dimension bug.
* Backscatter : introduction of rolling window normalization process.
* Terrain shading : Add 20% ambient component
* Kmall2Xsf : Fix case of empty WC Datagram
* Gridded/beam texture layer : improvement of synchronization (for each file, do not disable current layer if there is no other layer to select)

# Version 2.4.0
31/10/2023
* [PyAT] New tool to export DTM to TIFF (with 'no data value' and 'compression' options)
* [PyAT] 'Apply navigation' on TECHSAS files : improvement to handle properly several files (inputs and navigations...)
* Bug fixed with 'GeoBox page' (in some case, it was possible to specify a unprojected geobox with a projected CRS reference) 
* Bug fixed at application startup : with large session, an error raised if a menu was selected during the session loading 

# Version 2.3.10
12/10/2023
* [PyAT] Gridded CSV To DTM : correct handle of CPRD and Value Count columns
* [PyTechsas] Improvement of gravi procressing, turn filter and smooth tool
* Chart Editor : enable zoom from Y axis only

# Version 2.3.9
22/09/2023
* [PyAT] GapFilling : interpolate backscatter layer
* Help : new section for EMODnet 2023

# Version 2.3.8
13/09/2023
* Export to DTM : new option in Geobox page to set spatial resolution with a meter estimation (in lat/lon CRS)
* Convert to XSF : bug fixed with s7k (due to previous optimization)  

# Version 2.3.7
06/09/2023
Viewer 2D (Water Column) : new option to display sea floor detection
[PyAT] Gridded CSV to DTM : fix interpolation flag read with missing data

# Version 2.3.5
21/07/2023
Bug fixed with session loading
All2Xsf conversion : fix Tvg_law_cross_over_angle reading & Kmall2Xsf converter : Fix MRZ parsing

# Version 2.3.3
23/06/2023
* Bug fixed in "Merge Simple"

# Version 2.3.2
21/06/2023
* Compute Tide Wizard : new button to show properties about the selected Tide Gauge
* XSF conversion : bug fixed with compression 

# Version 2.3.1
15/06/2023
* EMODnet CDI : link to Seadatanet updated
* Handle ME70 roll compensation for WC display
* dtm_quality_indicator : check density only on valid detections 

# Version 2.3.0
09/06/2023
* New navigation file format (.nvi.nc) (beta) 
* Navigation layer : bug fixed with variable color

# Version 2.2.3
07/06/2023
* Activate NetCDF compression for XSF, DTM (.dtm.nc)...
* TECHSAS files : to improve the memory consumption : navigation line decimation is enabled by default, and lazy loading used in ChartEditor

# Version 2.2.1
16/05/2023
* ChartEditor : minor improvements with Legends 
* All to Xsf Converter : fix heading group conversion (misreading of heading data causing conversion failure)

# Version 2.2.0
12/05/2023
* Parameters View : new "histogram" widget to set contrast 
* S7k to XSF converter: better management of incomplete pings (ignore datagrams pointing to ping without bathymetry or watercolumn)
* Loading and display of ADCP Oceansite file format (new 'current' layer) 
* Navigation Layer : "decimation" is now synchronizable 
* Flatten WC : Fix display in case of use of a beam subset & improve mean computation of samples using linear amplitude instead of dB

# Version 2.1.3
03/05/2023
* Bug fixed in S7k to XSF converter (error while parsing xml file with default antenna conf)

# Version 2.1.2
* Bug fixed with PyAT menu (disabled by error) 

# Version 2.1.1
05/04/2023
* Parameters View : new widget to select color palette
* PyAT : new tool "TECHSAS Sanity Check" (finds anomalies in TECHSAS files (duplicates, leaps back, invalid position...))

# Version 2.1.0
## EK80
* New opening and parameters to handle Sonar-NetCDF from EK80. 
## Geographic View
* Re-enable of dynamic contrast computing (shortcut "c")
* GEBKO layer as default background
* New button and shortcut ("g") to show/hide latitude-longitude graticules
## Chart Editor
* New edition tools (linear transform, smoothing from PyAT, filtering from navigation...) 
* Display : improvement and bug fixed
## Bias Correction
* Add Time delay correctors
* Add MRU heading bias correction (an equivalent of ME70Roll corrector of Coratt (Caraibes))
## WaterColumn
* New functionalities to compute slices from the seefloor
* Display compensated backscatter (normalization by range)
## Others
* DTM Statisctis and Quality Indicators : new help and minor improvements 
* Wizards, output selection : new option to select the node where result files have to be load
* Fixed bug that prevented GLOBE from closing

# Version 2.0.4
* Navigation Line & Point Cloud : new 'Point' display mode (to display large amount of data efficiently) 
* Bug fixed with command line parser
* Sonarnative 1.6.1 : fix SonarNc Wc display for files without Attitude.
* PyAT : fix "MBG to Shape File" export when only 1 antenna (EM122 for example)
* PyAT : 'Cut / Merge' available for all Sonar Netcdf files

# Version 2.0.3
* PyToolbox: new menu/files organization 
* Linear transform (DTM) : upgrade to apply parameters as float
* All to Xsf Converter : add non centered Sensor Systems
* Draugth correction : allow files with one value

# Version 2.0.2
* Geographic View : enable rectangular selection of navigations with Ctrl+LeftMouseClick
* Converters : fix kmall installation parameters parsing in case of multiple MRUs
## DTM 
* prevent creation of gdal xml cache files (can cause display issues)
* Add capacities to compute statistics / quality indicators
## WaterColumn
* Update sonarnative to 1.5.0 for better spatialisation (mainly with high depth WC)
* Check marker layer visibility for WC 2D viewer
* Fix reading of xml PolarEchograms (xsd template update) 
## Swath Editor
* Selection by Swaths : enable multi-swath selection
* Selection shift according the orientation
* Toolbox refactoring
* Manage file color / edition / display
## PyAT
* Check accessibility and unload output files before process
* Bug fix in Orpheus function read_across_angles
* Process menu reorganisation to have hierarchical display

# Version 2.0.0
* Update version of language, framework and dependencies (Java, Worldwind, GDAL...)
* New NetCDF Viewer : generic view to explore NetCDF file
* New Polygon Editor & functionalities to create and edit polygons. (Saved in kml, geojson or shp)
* WaterColumn : new functionalities to compute slices

# Version 1.21.0
* MBG/XSF bathymetry detection to CSV : enable elevation sign option & bug fixed with detection over MSL
* New tooltip on navigation : display information about position and linked data, synchonization with Chart Editor
* ChartViewer : reset zoom without disabled points if they are hidden
* Navigation Editor : update file statistics and reload file after save
## Converters
* new "Export 1D variables to CSV" 
* new "Convert TIFF to CSV"
* "EMO to Dtm" : pre-fill the parsing for EMO files
* XSF Converter : fix bug in Kmall MRZ; fix All->XSF heading convertion; fill navigation for points without
## WaterColumn 
* XSF : swath player : new option to "skip" missing swath
* Markers : new 'elastic slider' to set marker size 
* Sonarnative v1.3.2 : Fix display of Xsf WC without bathymetry group
## DTM
* Csv to Dtm, pre-fill the parsing for EMO files
* Geobox page : new option to select round arcminute values for spatial resolution
* Handling of "Undefined CDI" (cells with data and no CDI if CDI layer exists)

---

# Version 1.20.11
* XSF WC : add a default fill sample time offset variable (missing for kmall files)
* XSF Fix a bug when two MRU where declared but only the second one had data

# Version 1.20.10
* NavShift: minor correction to correctly use value set in open wizard
* Line Editor : memory leak fixed, improvement of robustness
* SanityCheck : add cdi_interpolation option to get nearest CDI on interpolated cells

# Version 1.20.9
* XSF WC : native water column (c++) by default 
* TECHSAS files : navigation line is now colorable with any variable 

# Version 1.20.8
* Point Cloud from CSV : new functionalities to load several fields from a CSV, and syncrhonization
* DTM fix :  bug fixed with interpolation_flag layer display, unknown values displayed as valid
* EMODnet : new set_interpolated process to mark DTM cells as interpolated
* EMODnet : SanityCheck process no longer erases min/max elevation layer 
* Terrain layer : cache files could stay in temporary dir 

# Version 1.20.7
* Apply navigation :  bug fixed with 'Apply immersion' option
* Bug fixed with DTM : unwanted resampling was applied during terrain tilling
* Longitudinal slicer (PyAT) : new filtering support 

# Version 1.20.6
* Optimization of session loading
* Geographic View : new "Preference Nasa" option to set texture memory cache capacity
* Geographic View layers : new 'Move layers to front/background' from group nodes
* Navigation from CSV : bug fixed with elevation & heading, ignore lines with wrong date/time
* GeoBox window : new tool to align geobox on integer values
* Marker Layers : new display parameters

# Version 1.20.5
* New XSF version 0.3 (previous XSF must be regenerated) aligned on sonar-netcdf 2.0 model.
* Tide : Add option to compute platform elevation wrt ellipsoid + offset
* Terrain layer : improve shading dynamic effect based on slope

# Version 1.20.3
* Apply validity flags : bug fixed. Importing validity flags from MBG to XSF was failing.
* EMODnet : add a tool for norway data automatic processing.

# Version 1.20.2
* Improve soundvelocity correction for MBG
* Bias correction : new option to specify antenna
* Optimization of XSF converter 
* WaterColumn display : improve openGL memory management to avoid leaks

# Version 1.20.1
* PyAT Merge Simple : bug fixed with interpolation flag

# Version 1.20.0
* New XSF version 0.2 (previous XSF must be regenerated)
* Tide : "tide estimation" replaced by "platform elevation estimation" (handles draught)
* NavShift : bug fixed with old DTM NetCDF3 (.dtm)
* New GeoTIFF export from DTM NetCDF4 (.dtm.nc) (Arcgis9/mimosa compliant)
* WaterColumn (XSF) : new experimental 3D display based on C++ 

# Version 1.19.3
* Bias Correction in swath editor : use min date of selection per file when correction point are not common
* PyAT : add interpolation layer in Coronis processes

# Version 1.19.2
* Parameters view : new reset contrast option computed from the set of synchronized layers
* GriddedData : display date in tooltip & new navigation layer
* NavShift : 2 bug fixed (pref isobath step as double, ouput dir...)
* PyAT : parsing CSV with whitespaces

# Version 1.19.1
* Bug fixed with option to retrieve old shading functionnality

# Version 1.19.0
* New terrain layer to display DTM/TiFF... :
	- new shading functionnality based on GDAL slope/aspect
	- fixed interpolation artefacts (blue lines)
* Line Editor : new  tools to compute cut lines (from heading, speed or sensor type)
* User preferences saved in local repository (will be kept between versions of Globe)
* "Check" enabled from multi-selection
* Enable parameters syncrhonization between TiFF without elevation
* Orpheux : accross angle error fixed
* gridded_csv_to_dtm and csv_to_dtm : new parameter to compute one geobox per input

# Version 1.18.19
* DTM : new feature to extend the elevation model
* Merge DTM : bug fixed with GeoBox wizard page
* Fix WC depth in flatten WC Texture

# Version 1.18.18
* Apply validity flag : new batch mode
* New option "vertical offset" for water column layers
* Fix bug with tide computation

# Version 1.18.17
* Handle of big texture (g3d,xsf...) (with size over OpenGL limits)
* Converter all2xsf : manage passif mode
* Swath Editor : new Bias Correction window (with slider, and compute...)
* Apply validity flags to MBG/XSF : new "batch" mode to process several files

# Version 1.18.16
* Update PyAT : new DTM interpolation algorithms (coronis)

# Version 1.18.15
* Compute tide : 
	- new tide estimation module using gps data.
	- Preferences : "FES2014 model directory" renamed "Tide models directory". 
* Swath Editor : bugs fixed with DTM display & refresh
* Geographic view : new default elevation model server

# Version 1.18.13
* New navigation layer : synchonization by group/all, size option, display points, color from variable...
* Swath Editor : new selection mode per swath

# Version 1.18.12
* Fix a bug in .all files when missing a water column datagram

# Version 1.18.11
* Improvement of performance of export MBG/XSF to DTM.nc (PyAT)

# Version 1.18.10
* Converter of S7K : correct handling of transducer installation parameters
* "Show navigation path" (option to display the projection of navigation line) set as preference
* New supported format : MGD77 (gravity, magnetism & depth)

# Version 1.18.9
* Read of ttb file from CARAIBES
* Bug fixed with S7K installation parameter reading (X/Y inverted & Z sign) 
* MBG/XSF to DTM (NetCDF4) : fix compute of STDEV

# Version 1.18.8
* Bug fixed in "Cut / Merge Tool" wizard
* Export MBG/XSF to DTM : bug with CDI fixed

# Version 1.18.7
* XSF converter adapted to handle EM2040D kmall files

# Version 1.18.6
* Nav shift editor can use dtm.nc or geotiff as reference DTM
* Export MBG/XSF to DTM (.nc NetCDF4) : 2 bugs fixed :  drop of CDI file, mask size of "gap filling" interpolation was not taken into account 
* SwathEditor : disable camera update after a selection in sounding viewer
* SwathEditor : bug fixed : texture contrast is no more reset after an action.
* New option to cut MBG from an area defined in shp or kml file.
* Optimization of G3D display

# Version 1.18.5
* Export CSV to DTM : bug fixed with CDI
* Update laloxy and fix a few (mainly reverse X/Y axis)

# Version 1.18.4
* Bug fixed in GeoBox page when input is MBG or XSF

# Version 1.18.3
* Heighmap interpolation : new algorithm to compute CDI/CPRD
* Minor bug fixed in GeoBox page (when projection selected)

# Version 1.18.2
* Improvement of EMODnet tools
* New "Help" page

# Version 1.18.1
* Update CDI status in two pass to auto refresh when time out occurs
* Fix UI issue in merge, all files were marked as Reference
* Fix a bug in geotiff -> dtm.nc
* Fix fill gap with borders
* Add kml filter on diff mnt

# Version 1.18.0
* Fix Reson WC display, allow to display data in db
* Add export dtm,dtm.nc to Golden Surfer v7
* Integrate a tool to apply patch
* Fix missing backscatter layer when available in dtm
* Prevent from loading data with the same names
* Globe freeze on modify CDI
* prevent loading file with the same name
* NVI export only valid points
* Open techsas files in time viewer

# Version 1.17.12
* Fix elevation inversion in tiff -> dtm (old format) import
* Apply Navigation : new option to apply immersion
* Improvement of generic CSV loading

# Version 1.17.10
* G3D : display only the selected layer 
* Bug fixed with converter MBG/XSF to DTM (NetCDF4)

# Version 1.17.9
* Remove marnav and harmar from Caraibes Toolbox
* Marker Editor : new "enable" property for markers and filter tool 
* Update Background layers : new GEBCO layer + update server for : BlueMarble, i-cubed landsat & OpenStreetMap
* Bug fixed with DTM (NetCDF 3 .dtm) processes (projection error)
* New converter CSV to DTM NetCDF 4

# Version 1.17.8
* New tool to clean MBG correction flag
* Fixed bug with SoundingFileConverter on linux (native library loading)
* Fixed bug with Kmall to XSF converter (course over ground, seabed image variables)  

# Version 1.17.6
* compute per default backscatter for ifremer reson files (as mean value of samples in amplitude)

# Version 1.17.5
* New "Compute tide" tool (Work in progress)
* New "Chart Viewer" (Work in progress)
* New cache for G3D 

# Version 1.17.4
* Generic import of CSV

# Version 1.17.2
* New console view 
* Several bugs fixed (Time Viewer opening, area menu, apply nav to XSF...) 

# Version 1.17.1
* Bug with console view fixed (several processes at the same time locked the first console)

# Version 1.17.0
* New syncrhonization system for display parameters (XSF, G3D...)
* Save of display parameters in session file
* WaterColumn : improvement of G3D functionalities (load, display...) & new preference options 

# Version 1.16.18
* Load of MultiTexture data (.xml files) enabled

# Version 1.16.17
* New export of velocity profile (.vel) from s7k

# Version 1.16.16
* New convert mbg/xsf to DTM NetCDF 4 (.nc)
* Projected DTM handled by PyAT EMODnet processes
* all2mbg/xsf : bug fixed with .all without position datagram with 'active' position system  
* New tool to compute tide (work in progress)

# Version 1.16.15
* Add a techsas file export to csv (with georeferencing)
* Fix a bug in XSF WC subpsampling by default
* Add a increment capacity in WC player to allow to skip some ping (like for multiswath)
* New menu to load file from a file list (.txt)
* MBG : improve flag setting, create 
* Merge : output file in NetCDF4 (& bug fixed)
* SwathEditor : optimization of temporary file management, bug fixed with STDEV layer & validate/invalidate tool...

# Version 1.16.14
* Fix a bug in XSF converter when XYZ datagram is missing
* Fix a regression in dtm processings making them shift to zero (with linear Transform for example)
* Fix missing impnav in Caraibes Toolbox
* Fix bug in mbg to shape file export


# Version 1.16.13
* Update of tooltips in Geographic view
* Improvement of 2DViewer (picking, load of XSF Flatten...)
* Fix conversion of S7k to XSF (when installation datagram missing)
* Fix bug in NavShiftEditor 
* Fix missing ImpNav in Caraibes Toolbox
* Fix a bug in merge simple for big projected dtm.

# Version 1.16.12
* Lat/lon graticule formatted with preferences
* Bug fixed in Measure Tool (new 'clear' button to remove current selection)
* Optimization of start & session loading

# Version 1.16.11
* Improvement of water column display for XSF (S7k source)
* New parameter view for .dtm files

# Version 1.16.10
* New "Marker Editor" for water column / terrains markers
* New marker files for terrain
* Water column display : bug fixed with XSF (from s7k) 

# Version 1.16.9
* Bug fixed in S7K -> MBG converter (error with incomplete ping) 
* Memory leak fixed in Bias Corrector (Swath Editor) (with temporary files)

# Version 1.16.8
* Caraibes toolbox: updating Harmar: not working with csteCeltique.har

# Version 1.16.7
* Caraïbes help/toolbox updated
* Wizard to import CSV files as PointCloud
* Bug fixed: SoundvelocityCorrection was disabled

# Version 1.16.6
* AllToMbg converter : improvement of convert from .all with dual antenna 
* NavShift: multiple selection, reload of input file after shift apply...
* MbgToShp : filter on cycle validity flag

# Version 1.16.5
* Update ImpTide parameters in Caraibes toolbox
* Caraibes toolbox: bug fixed in file selection (extension)
* New : loading of navigation from CSV files
* New : loading and display CSV files as "Point Cloud"
* Export to GeoTiff (mimosa) fixed (GeoTools lib update)
* DTM shading fixed

# Version 1.16.4
* New loading strategy for 3D NetCDF files (.g3d.nc)
* Marker enabled for 3D NetCDF files in Viewer2D
* Transparency fix for 3D NetCDF files (.g3d.nc)
* New CSV file accepted & displayed as "Point cloud"

# Version 1.16.3
* Enable loading of old session files (.xml)
* Viewer 2D for .nc warter columns files
* New marker editor (and picking for new textures)
* Memory leak fixed with textures

# Version 1.16.2
* bug fixed in converter of .all to .mbg with Depth68 datagrams
* markers for water column improved (work in progress...)

# Version 1.16.1
* bug fixed with .dtm processes access (execute with -> ...)
* minor bugs fixed with DTMs layer (shading on several files...)

# Version 1.16.0
* Update of JAVA (8 -> 11) & JavaFX
* Update of Eclipse framework
* Global refactoring (file management, background architecture...)
* New format: 3D display from NetCDF file
* New detection filtor
* CARAIBES tools: adding ImToMbg : import .IM, .im in MBG
* DepthCorrection: ignore empty lines in the correction file and correct the handling of allowed extensions
* Biascorrection in Swath Editor: after shifting selection the current correction  parameters were not active

# Version 1.15.23
* Improvement of converter EMO to DTM(.nc) : manage CDI/CPRD coming from the 2 EMO file columns: CDIID & data source.
* 2 bugs fixed in converter DTM(.nc) to EMO : a delimiter was missing if no smoothed depth & 
 the missing value of interpolation flag is no longer exported.

# Version 1.15.22
* Correct a bug occurring when converting emo files already having CPRD id field to dtm.nc files. 
In such cases, the id of CPRD were corrupted and modified with an extra « : » character. 

# Version 1.15.21
* Allow Globe to handle correctly CDI with spaces

# Version 1.15.20
* Fix a bug in modify cdi, working only the very first time for a given workspace
* After modify cdi, update cdi info displayed in globe correctly

# Version 1.15.19
* Fix a bug in gebco geotiff to dtm import

# Version 1.15.18
* Update emo to dtm import, correct a bug in grid step estimation, now read 10e6 lines to estimate it

# Version 1.15.17
* Add a kernel smoothing for dtm in python toolbox
* Add a diffmnt tool in python toolbox
* Export Dtm: Fixed error: can reload DTM after creating
* Shading : fix dependency to dtm min/max values
* Fix bug in flatten view : take into account for sample_interval changes

# Version 1.15.16
* Filtri from Process: update parameters projection
* XSF Flatten water column: parameters saved as preferences
* Bug fixed with convertToDtm from a file not loaded in Globe
* Bug fixed with export mbg to shape when only 1 point

# Version 1.15.15
* Fix a bug when converting from xyz to dtm, maximum value could not be displayed correctly in some cases
* Add an export file list in project explorer
* Allow to load xsf flat view in batch
* Fixed bug with journey point of view selection

# Version 1.15.14
* EMODnet : update of interpolation process
* Fixed regression in version 1.15.13 (wizard output page)

# Version 1.15.13
* New NetCDF format to display texture. 
* Bug fixed in cut/merge process
* Bug fixed with color of shape markers

# Version 1.15.12
* Add a warning when using not UTF8 path
* Fix a bug preventing kml usage
* Python Plug Experimental peak detector algorithm
* Python Bug correction in interpolation process
* Python Correct a bug preventing merge with file having a CDI
* Python Add more explicit errors when processing files

# Version 1.15.11
* Add a geographic preview of bounding box definition in python processes
* Correct a bug in merges processed preventing further operation on cdi layers

# Version 1.15.10
* Correct a bug preventing interpolation to start
* Correct a bug preventing emo and xyz to dtm conversion
* Correct a bug for shape file and kml display
* Bug fixed in converter mbg -> .dtm (sometimes, old Gdal error message locked the process)

# Version 1.15.9
* Add a warning when bounding boxes coordinates are not exactly a arcmin integer
* Improve dtm.nc properties displayed (add layer list, exact resolution)
* Improve merge when requested layers are not in input file layers
* Fix a but in setCDI
* Create a new process update geobox allowing to reproject data when changing geobox
* Raise of texture cache memory size

# Version 1.15.7
* Ihm Globe/caraibes toolbox: correction and improvment of file and directory selection

# Version 1.15.6
* PyToolbox: errors in red
* EMODnet processes bugs fixed

# Version 1.15.5
* EMODnet: improvement of CDI management during merge

# Version 1.15.4
* EMODnet processes bugs fixed
* XSF : new functionnality to display a flatten water column longitudinal slice
* Update TGeoS in Caraibes toolbox: memory leak correction

# Version 1.14.16
* Update path in pyAt
* New wizard page to define GeoBox

# Version 1.14.15
* Gdal : new version 2.4.2
* XSF : bugs fixed with conversion & watercolumn display 
* Video: minor improvements (setting of point of view position is now easier)
* Management of Tiff with wrong "NoDataValue" attributs

# Version 1.14.14
* MBG : improved management of dual head sounder
* Convert NVI to Shape : filter added to keep only valid positions
* EMODnet Python processes : new interpolation process

# Version 1.14.13
* Improvement of EMODnet Python processes
* "Cut/Merge for NVI" fixed
* Other bugs fixed (some tools were not available in version 1.14.XX : DepthCorrection, ApplyNav...)

# Version 1.14.12
* Improvement of EMODnet Python processes
* Caraibes toolbox: error fixed if Globe installed in a directory with spaces; log deplaced in workspace
* Caraibes tollbox: ImpNav: adding NAEN2 navigation type

# Version 1.14.10
* New wizards to launch PyAT processes
* Bug fixed with GeoTIFF display
* Export new DTM_V4 to GeoTIFF

# Version 1.14.9
* Bug fixed whit export .all to .nvi

# Version 1.14.8
* Bug fixed with the coordinate format "Degree-Minute-Second" and the "expands to integer" method provided in Geographic Bounds definition page (DTM creation wizard...) 

# Version 1.14.7
* Navigation Editor : minor bug fixed (zoom with right click instead of left click, which is used for pan) 
* Bug fixed with PyToolBox path

# Version 1.14.6
* Bug fixed with PyToolBox path

# Version 1.14.5
* Toolbox araibes: problem to acces CIB_Temp
* PyAt & miniconda are now embedded into Globe

# Version 1.14.4
* Emodnet CDI URLs updated

# Version 1.14.3
* Geographic viewer : fixed bug : navigation layer were not removed after unload

# Version 1.14.2
* Swath Editor : refactoring of WW layers to display the generated DTM
* Export DTM : fixed bug with lat/lon used to define the grid

# Version 1.14.1
* Swath Editor : refactoring of temporary file management (+fixed save bug)
* One global parameter to display projected navigation path (in "background" node)
* NavShift : improvement of the export wizards 

# Version 1.14.0
* NavShift Editor : bug fixed: file correctly released (and now, can be renamed after close)
* NavShift Editor : improvement of the selection of the first or last shit period point

# Version 1.13.6
* fixed bug with session loading (bug from version 1.13.5)
* new Isobaths layers in Swath Editor

# Version 1.13.5
* Refactoring of isobath display in 3DViewer
* Filtri : bug fixed (issue when too many invalid soundings)
* Toolbox Caraibes: windows are now modal
* Swath editor & BiasCorrection: in the combo box only files involved in selection data
* Toolbox Caraibes: closing properly Caraibes jobs when GLOBE stops
* Workspace: checking if worskspace name contains spaces

# Version 1.13.4
* Swath Editor : DTM correctly projected in 3DViewer
* Process wizard output page: full filename edition if "merge" options checked
* DTM creation : fixed bug of the "expand to int..." functionality in geographic bounds page
* NavShift Editor : bug fixed: error with context file, lat/long was potentially saved in wrong format
* NavShift Editor : new functionality to import vectors from an XML context file
* NavShift Editor : apply correction/export NVI only for profile within period
* Toolbox Caraibes : update default .val for TGeoS with actual specific parameters
* Swath Editor: legend view: files names in color

# Version 1.13.3
* NavShift Editor : bugs fixed (save after closing, vector table display...)
* DTM generation : latitude and longitude input depends on coordinates display preferences

# Version 1.13.2
* Line Editor : cut line table edition
* Wizard output page : if only one file, the name can be edited
* Layers in DTM converter are saved in preferences 
* Swath Editor: 
*              bug fixed: names files, destroy index files only if directory exists
*              suppress of question about save index files
* BiasCorrection in Swath Editor: bug fixed: export individual (directory choice)
* New wizard for apply navigation process

# Version 1.13.1
* NavShift2D: refactoring projection of reference DTM / fix display bug 
* MBG to SHP feature (via PyToolbox)

# Version 1.13.0
* NavShift Editor: new "2D version"
* Line Editor: minor bugs fixed (pref, undo/redo...), and minor evolutions (color by cut line...) 
* Navigation Editor and Time viewer: corrected dates
* Refactoring of  Caraibes tool box about "VAL" (parameters directory)  and" CIB_Temp" (temporay directory) directories: they are created in user workspace
* Caraibes tool box: update.val by default
* Pytoolbox and NVI to SHP convert

# Version 1.12.1
* Xml Water Column: memory leak bug fixed
* Animation / video : disable automatic switch in fly mode

# Version 1.12.0
* XSF : update, water column is recorded as vlen variables, old XSF files are no longuer supported

# Version 1.11.6
* 3DViewer : multi selection of layer with CTRL key down
* Water Column 3D (xsf) : correct blinking effect 
* WaterColumn markers: update of marker files (lat/lon column) and ping index for xml
* Line Editor : review of cut line creation algorithm, and others minors evolutions
* NavShift : bugs fixed and add of "multi color" option for isobaths

# Version 1.11.5
* New Nasa Cache path

# Version 1.11.4
* Fixed error: Swath Editor and Bias Correction: impossible to add a correction with " Common is selected"
* Add a line and a polygon edition mode in the Navigation Editor
* New organization for preferences
* Merge and cut for NVI files
* Fix filtering threshold for wall view
* Water Column 2D viewer for XSF

# Version 1.11.3
* Correct a bug preventing to use and load csv markers

# Version 1.11.2
* Line editor: new option to specify the first line index

# Version 1.11.1
* Workaround for angle inversion in XSF Water Column
* Fix ttb file parser throwing lots of errors on other txt files
* Fix Multiple import after reduction process
* Correct bug in Reprojection leading to slightly translate rerojected DTM

# Version 1.11.0
* XSF Conversion : bug in EM302 conversion
* Correct XSF WC inverted 
* Export nvi from s7K: immersion from 1015 datagram was bad in .nvi ( inverted sign)
* Merge / Cut Tool: new mbg merge tool (beta version) 
* Export MBG to CSV (x,y,z)
* Swath Editor : new information in Beam Summary (geographic coordinates, across/along distances, filenames...) 
* Shader compilation bug fixed (compilation error with HP Dock) 
* Updating help on line about the navigation shift editor .

# Version 1.10.9
* Fixed error: bad output files names with option "one  .mbg file per time interval"
* Add Mimosa2 (arcgis9) compliant geotiff export 
* Fixed error: import .txt file with empty lines
* Fixed error: export to DTM from .txt
* Export to DTM: check if a layer is not too big (2 giga) before to create dtm file
* Update ImpNav in Toolbox: Import Techsas file from Marion Dufresne
* Time Viewer : new tool to display time stamped data (beta version)
* Export raw file to nvi : bug fixed (navigation indexed by position datagram)

# Version 1.10.8
* Fix tooltips display in 2D WC 


# Version 1.10.7
* Depth (tide/dynamic draught) correction : chart preview for correction file
* 3DViewer: display navigation line with immersion

# Version 1.10.6
* Tide/DynamicDraught correction: fixed error if correction file has txt extension
* Add new display mode for lat/long
* Increase Swath editor precision in color scale
* Bug fixed in export Edmonet DTM from xyz
* Improved process management (closing the console causes the process to stop)
* Bug in SSC navigation when clicking on it in 3DV
* XSF : Add tolerance for default date for SSP 


# Version 1.10.5
* Line Editor : optimization and new functionalities.

# Version 1.10.4
* Update Bias correction : replace immersion and heave correction with a Vertical_offset correction

# Version 1.10.3
* Add WC for XSF in beta version (sound velocity is considered constant)
* Improved process management of the toolbox.

# Version 1.10.2
* Bug in WC indexing 
* New wizard for depth correction (tide/dynamic draught corrections), available in "Tools"->"Process" menu.
* Optimization of navigation line display in 3DViewer (new decimation algorithm)

# Version 1.10.1
* PLACA correct a bug in grd display, raster values were multiplied by -1*Math.abs(value) 

# Version 1.10.0
* Add ; separator to import tide file from the tide gauge observations of SHOM (REFMAR)
* Add in toolbox: NavExp (Export ascii navigation file) and Marnav( create a tide correction file from harmonic constants and ascii navigation file)
* Add tooltip in Navigation layer
* Improve precision while displaying lat lon in 3DViewer and connect to
preferences
* Online help updated
* Line editor full rewrite (beta version)


# Version 1.9.10
* Bug fixed in swath editor: wrongly positioned dtm since using the transverse mercator projection
* Bug fixed in MbgUpd (toolbox) : bad scale depth converted from old mbg files
* Bug fixed WC 2Dview menu was hidden

# Version 1.9.9
* Bug fixed in export DTM: following the removal of automatic step computation

# Version 1.9.8
* Add workaround for geotiff export from dtm in case of projection set to Mercator_2SP, choice of the framework to use for export is done through preferences in General\geotiff export framework
* keep only navigation from active system or take all navigation positions if none navigation system is active
* Improvement loading navigation layers
* Disable batch dtm step resolution computation 

# Version 1.9.7
* Bug fixed in swath editor : error of bounding box computation when changing display mode 
* Bug fixed in MergeTool : error with accross/along values, distance scale was not correctly managed
* Loading of "standard" mbg 
* Enable axis step defined by user
* Close Swath editor: close bias correction view
* Bias correction in swath editor: fix error about applying immersion bias
* Update bias correction in swath editor: add minimum date for each file and minimum of all files for common option
* import s7k to XSF converter as beta function
* Mbg to Dtm converter : bug fixed (layer selection was disabled)
* Tool box Caraibes: update MbgUpd to create extended MBG format always
* Tool box Caraibes: Add ImpTide
* Swath Editor : fix error about date in status bar
* Improvment for all parts: modify "save part" by an explicit message


# Version 1.9.6
* Correct bug in line editor
* Swath editor : time was not correctly set in bias correction
* reconnect display of journey lines
* remove FusMbg from toolbox

# Version 1.9.5
* New navigation editor
* SwathEditorSoundings: add close and save in the top banner of the Soundings View

# Version 1.9.4
* Bug to convert . all to vel ( profile date , start and end date)

# Version 1.9.3
* Bug when opening line editor
* bug Sometimes impossible to create a DTM from a mbg


# Version 1.9.2
* New look of wizards conversion tools (all, s7k to mbg, nvi and s7k)
* Disable the position datagram active state check
* update export nvi to csv handler to allow multiselection

# Version 1.9.1
* "Filtering by triangulation" available for xsf
* bugs fixed in Swath Editor: DTM might be not correctly refreshed & "Invalid" soundings were displayed as "Invalid acquisition"
* bug fixed: "Export to DTM..." for mbg was not available
* bug fixed: MergeTool: bad cartographic limits and mbBFlags empty
* improvment: Mergetool : adding  background process and several messages, 

# Version 1.9.0
* Merge with Globe v1.6.13 (emodnet version) 

# Version 1.8.3
* add emodnet check process for emodnet command line tool

# Version 1.8.3
* fix flyview limit to [-12km,40km]


# Version 1.8.2
* Fix bug export geotiff
* Update treatment tags in MBG (depthCorrection, BiasCorrection, SwathEditor)
* Moving "Batch Dtm from external files" in the Process menu
* Swath Editor: adding the sounding view window under the Geographic view window

# Version 1.8.1
* Add Point of Interest for WC

# Version 1.8.0
* WC first release


# Version 1.7.19
* fix bug in setCDI and modifyCDI functionalities
* add Kongsberg 304 with code 94 (mbSounder in MBG)
* improve  loading nvi files in "project Explorer" after convert from .all

# Version 1.7.18
* Batch DTM from external files: errors fixed


# Version 1.7.17
* Emdonet processes: new "average merge" command

# Version 1.7.16
* correct import .all from EM1002 (Alis)

# Version 1.7.15
* XSF water column first release
* fix bug: immersion = missing value in nvi file
* Add export navigation (nvi) in Process/Convert S7K/ALL..To MBG + improve export to MBG/NVI from Project Explorer (output directory, loading nvi, etc...)
* Tide/Draught correction : immersion not updated


# Version 1.7.14
* fix bug: water column 2D and 3D not synchronized from xml: problem with 2D visualization
* fix bug: bad DTM from MBG (bad read of mbHeading)
* fix bug: wath editor didn't save files
* Fix bug: missing point in csv "Methane" file converter

# Version 1.7.13
* Correct a bug in journey file creation
* Fix error:GLOBE_864: waterColumn 2D and 3D; stack overflow if 3D is select in parameters view

# Version 1.7.12
* fix bug in "export DTM" process to handle mbg without reflectivity layer

# Version 1.7.11
* Add transparency and allow constrast over min/max limits for Vertical Data
* Emodnet : change parameters for fill merge in emodnet online process

# Version 1.7.10
* PLACA export xyz on some files failed + correct performance in xyz file
writing

# Version 1.7.9
* PLACA incorrect export in xyz file (sometimes du to different resolutions between age grid and elevation grid, a few points where exported)

# Version 1.7.8
* Add Emodnet fill merge process
* Correct opacity & filtering bug for xml images
* add fusnvi in toolbox

# Version 1.7.7
* Correct refresh pb in project explorer
* bug fixes when changing dtm layer
* improve performance for check/uncheck of layers
* add new layer in dtm (min across distance and max across distance
* fix bug in geotiff positionning in export DTM to geotiff


# Version 1.7.6
* add refresh button in project explorer

# Version 1.7.5
* correct GDAL macOS libraries


# Version 1.7.4
* correct a display performance issue when loading a lot of EK60 files
* few modification in interpolation and color handling by default

# Version 1.7.3
* correct bugs with XML plot and image display (contrast, transparency...)
** add contrast / offset in the parameters view for XML plot and image layers

# Version 1.7.2
* FlyView set maximum elevation limit to 40km instead of 4

# Version 1.7.1
* Robustify morphing in PLACA

# Version 1.7.0
* correct bug in PLACA when area does not have the same bounding box
* integrate CARNOT project developpement
** PLACA Morphing, xyz export
** WC integration and 3D display


# Version 1.6_13
* reconnect CDI Tool to new sextant URL and parse

# Version 1.6_12
* Add a non interactive conversion tool from emo to dtm (called Fast export)


# Version 1.6_11
* correct import .all from EM1002 (Alis)
* * JIRA 871:fix bug about convert to mbg: special .all file: some datagram at the end of the file with datagramType =0

# Version 1.6_10
* correct bug in modify CDI, file needed to be reloaded to take into account the modified CDI
* improve Fill gap performances


# Version 1.6_9
* correct bug of Line Editor
* correct bug on decimal point if decimal is ","
* add space handling in CPRD when computing sextant url
* change link to sextant database


# Version 1.6_8
* add shortcuts in 3DView to display (s) or hide (h) all dtm intersecting the bounding view
* New menu items to display/hide all data belonging to a group
* New process to split a DTM by CDI
* Add sanity check mode to :
** create a CDI layer when metadata is set but CDI is erased
** compress CDI metadata
** recompute min max values for layers
* correct a bug in merge dtm, interpolation layer was not updated correctly
* add a tool to create EMODnet defaults layers
* add a tool to marke a file as completely interpolated
* correct bug when converting dtm to ascii files
* allow to define decimal separator when importing xyz or txt files
* retain txt import settings in preferences
* split merge menu in two : one for simple merge, one for fill missing values
* correct a bug, the exact min and max values for a dtm where displayed with the wrong color

# Version 1.6_7
* improve performance for emo -> dtm file converter

# Version 1.6_5
* Improvment Bias Correction dialog
* Correct cat files of Caraibes toolbox
* TimeEditor (Legacy and future) about .cor file

# Version 1.6_5
* add case insensitity for field names when retrieving CDI and CPRD from sextant and seadatanet databases


# Version 1.6_4
* correct a bug in File-CDI association for batchDTM
* Switch CPRD to new sextant URL

# Version 1.6_3
* Add capacity to organize dtm data given their QI 
* Bias correction : bugs correction and improvements
* Take into account new file format for mbg
* EMODNET correct a bug: adding to many CDI information in DTM when parsing CDI Layer files
* Add a DTM cleaner that remove unused informations

# Version 1.6_2
* Correct a few bug in fileSoundingConverter (bad interpolation in motion and position + robustness,transcoding error in s7k when no installation parameter datagram is recorded in file)
* Add explicit message when trying to convert to mbg a file with not bathymetry



# Version 1.6_1
* Modify water column display
* Some menus had disappeared (export to DTM for emo files)


# Version 1.6_0
* Correct a bug in Bias Correction process: bad read of correction type
* Add a new sounder: 2040C (Id 2045)
* Correct a bug about UTM projection (bad zone displayed by default)
* Correct a bug in batch dtm from mbg files
* Correct a bug in placa, cache was permanently disabled
* EMODNET : add import capacity for lat long and long lat
* EMODNET : add all in one process : (batchdtm + reprojection + set cdi)
* EDMONET : correct reduction, some cells having number of soudings invalid where discarded
* EDMONET : latlon and UTM grid where not positionned in arcgis at the same place

KNOWN BUG : dtm files in UTM projection are slightly shifted (one or 2 pixels)


# Version 1.5_27
* desactivate terrain sort if layer added is not terrain
* change ping index vs ping number displayed in 2D view
* remove a filter for less than 20 db values for reson
* sort xml navigation sublayer according to their names
* display bounding box layer for watercolumn files

# Version 1.5.26
* remove a lock preventing to change constrasts and threshold values when out of the textures values

# Version 1.5.25
* Swath editor : take into account validity flags for pings and beams, and do not use missing soundings

# Version 1.5.24
* Emodnet fixed CDI tooling URL
* Bug in tile computation, maximum values where missed
* clear RAM cache when reloading a file
* add EMODNET help

# Version 1.5.23
* correct widget illumination sync when apply all is active
* remember illumination settings when changing in 3DV

# Version 1..22
* correct sync when changing color bar in 3DV
* a few emodnet modification around dtm export
* bug bias correction erasing some metadata
* bug vertical image badly displayed
* bug in batch dtm with several files, sometimes delete mbg sources files when an error occured


# Version 1.5.21
* correct a bug in widget constrast layer
* add quality factor in swath editor
* correct a few bugs in swath editor

# Version 1.5.20
* Correct a bug in loading water column file
* In case of load session crash backup the old file 
* Bug in delete of layer explorer elements.
* Bug on reflectivity files
* rename worldwind cache data (caches between 1.5.17 and 1.5.18 are not compatibles)
* export multilayer geotiff
* increase precision for constrast layer

# Version 1.5.19
* Major rewrite of swath editor
* Add converter from csv to xml binary files
* PLACA : change geotiff driver to gdal.


# Version 1.5.18
* switch nasa tiles to 512x512
* Add CARNOT features
** Water Column new ncs file pointing and display capacities 
** PLACA subduction features and mtec files support
*Bias and celerity correction


# Version 1.5.17
* allow display of some old dtm formats
* Version used for FLOWS training

# Version 1.5.16
* correct a bug when converting from emo files to dtm, smooth layer had its signe reversed


# Version 1.5.15
* add .s7k import
* Add a goto to water column csv file + a few bug correction on WC processing

# Version 1.5.14
* improve data loading to use less memory

# Version 1.5.13
* improve Asciidtm driver to use less memory

# Version 1.5.12
* improve performance in MBGDriver and correct a few bugs
* refactor and correct cross plots.
* Correct a bug in dtm computer crashing on big areas

# Version 1.5.11
* Add metadata to velocity file format

# Version 1.5.10
* Correct bug, dtm converted from .emo badly positionned in argis/qgis
* Modify Ascii navigation export format
* Add a tool for CDI validation before its use in dtm generation and CDI modification
* correct a bug in swath editor, navigation was not updated when changing vertical exaggeration
* correct a bug in filtri
* performance in swath editor
* add gamepad support in globe
* integrate bias and celerity correction

# Version 1.5.9
* open console when clicking on status bar
* correct bug preventing conversion from xyz to dtm
* clear nasa cache when reloading a node
* add an driver to load grd files. Note that grd must respond to the following constraints : be in lat/long projection, and have and x,y dimension and a z data variable.



# Version 1.5.8
* Display a progress bar at startup when loading session.
* Change icon and status for unloaded nodes in layer explorer
* GLOBE 629 : sometime layer where doubled at startup
* In parameters view contrast where incorrectly set
* EMODNET | Reduction menu entry disappeared in previous version.

# Version 1.5.7
* Change in pyramid generation for NasaWorldWind. Resolution was reduced in some cases, now full resolution is used for dtm files.
* Change in constrast composite, when changing layer data displayed can become erroneous
* GLOBE-661 GEBCO data missing along edges of regional DTMs
* GLOBE-659 DTM reprojection bug (where destination or source projection differs from lat long)
* Correct a bug in Swath Editor preventing the save of invalidation in mbg.
* MAC: correct a bug prevent keyboard event to be sent to 3DView


# Version 1.5.6
* Correct a bug in CPRD id computation when importing from emo to dtm
* New management of imported lines in line editor.
* Correction of performance issues in globe filtTri.

# Version 1.5.5
* ** Swith to java 8, user on mac platform must update their java version in order for globe to work**

* CDI : for CPRD, globe was pointing the wrong url with cditool
* DTM : add a menu in execute with to correct old dtm being misplaced in arcgis. This will shift the data from the right corner so that they are well placed in arcgis/qgis.
* DTM LINES and COLUMNS shift from half spatial resolution position of cell.

* bug: Reset Cell work now correctly with multiple geographic zones.
* add preferences for swath editor.
* bug: WC point deletion work now correctly and cursor is properly changed in pointing mode.
* WC: improvement of selection display.
* improvement of typology colour.
* correct a bug on mac when switching to mercator.


# Version 1.5.4
* bug: allow selection in swath editor when navigation is display

* bug: false holes in dtm when edited with swath editor

# Version 1.5.3

* add preferences for swath editor (right view)

* improve swath editor mouse handling


# Version 1.5.2

 * Correction in line editor for handling small files


# Version 1.5.1

##What's new ?

* EMODNET add generic filters to resetCell

* Swath editor improvements

* Line editor (ex Split and Merge editor) improvements

* Add filtri in caraibes Toolbox


##Close
* Water column
	- correct a bug, loading big csv files takes too long
	- correct a bug preventing from load preferences at startup

* Globe-644 Threshold mal pris en compte sur les fichiers DTM.

# Version 1.5.0
* EMODNET correct DTM interpolation
	- now the interpolation is made by the "Fill Gap" menu and no longer overflow of DTM limits
	- modification in EMODNET export format, attribute 8 is now used as "dtm processing methods" and is set to one when the cell was filled by "Fill Gap", its CDI is the most dominant one


* EDMONET Grid reprojection
	- Add a grid reprojection algorithm based on Catmull-Rom surface/OverHauser interpolation
	- This allow to change projection of gridded data

* Layer explorer
	- Add button and drag&drop to manage data order

* EMODNET wizards
	- Add buttons to manage data orders
	- Add warning when grid exceed the maximum netcdf allowed size
	- Add a page to sum up the amount of data to be processed
	- Refactor geoboxes page to be more consistent
	- Fix 60 minutes display
	- remove geographical area in setCdi since it was not taken into account.

* EMODNET process
	- Action failed shall not occur anymore
	- work on exception in Globe so that the user will be informed when something crash
	- Erase temporary files when not needed anymore
	- Work on cancellation
	- Globe can read a file list to start batch dtm (menu entry : process, the expected file shall contains the list of file to process on file per line).

* Parameter view
	- Add a widget to move depth min and max values at the same time

* clear Cache
	- User interface should be more responsive.

* Swath Editor
	- The swath editor is currently being rewritten, you should not use it in this version of globe to edit soundings
	- Pb on geowarn11.mbg file of stdev not displayed (all blue) seems corrected by side effects of the works done on dtm.
* DTM
	- Add a layer node on all DTM to diplay scale in 3D view
	- Add a global statistical view for CDI in a DTM

# Version 1.4.9
##What's new ?
* Improve Swath editor
* Integrate PLACA V2 late 2014

# Version 1.4.8
##What's new?
* update display of soundings in swath editor
* add new 2D and tabular views for water column


# Version 1.4.7
##What's new?
* Add a convertor from .emo -> .dtm
* Modify import for .txt sounder files in order to set parameters only once per import of multiple files.
* memory release for batch dtm
* Placa 2014 developpement features

---------------------------------------------------------------------------

# Version 1.4.6
##What's new?
* remove clean dtm
* correct export in emodnet ascii format
* fix a memory leak in swath editor


---------------------------------------------------------------------------

# Version 1.4.5
##What's new?
* Correct a bug impacting performance on texture loading
* Correct a few bugs around water  column pointing
* 2310 clean context menu
* Add an activity bar to globe
* Add linux library for netcdf
* 2322 bug on display DTM
* 1677  Move "preferences", "clear cache" and "add WMS Layer buttons"
* Add a spy to check for loading node for spatial indexing.Spy is started with ctrl + T key on swath editor view (ensure to get focus on the view before)

----------------------------------------------------------------------------



# Version 1.4.4
##What's new?
* 2303 3D Viewer - navigation select and display. Add selection behaviour around navigation in 3DView
* Correction for MATOPO Module

----------------------------------------------------------------------------

# Version 1.4.3
##What's new?
* MAPOTO toolbox for EMODNET grid

----------------------------------------------------------------------------
# Version 1.4.2

##What's new?

* 2320 Fix tide correction for mbg files
* Most of techsas file can be loaded and displayed in TimeEditor
* Add gravimetry and magnetometrie

##Known issues
* 2230 Geoseas portal evolved and is no globe no longuer points to the good url
* 2229 CDI : when there is no network, the tooltip displays "No CDI data in this area"
* 2228 Subbottom file coloration is badly handle (as in 3DViewer)
* 2227 Marker opacity not handled
* 2226 CERSAT import file KO
* 2225 Previmer import file KO
* 2224 SEGY file import KO
* 2223 Old MNT file format KO
* 2221 Memory management for Big DTM
* 1656 Navigation editor no longer work

----------------------------------------------------------------------------

# Version 1.4.1

##What's new?

* 2304 Layer Explorer node expansion is now related to the selected node
* bug correction for PLACA
* Imagery : add sample and sample statistics view


##Known issues
* 2230 Geoseas portal evolved and is no globe no longuer points to the good url
* 2229 CDI : when there is no network, the tooltip displays "No CDI data in this area"
* 2228 Subbottom file coloration is badly handle (as in 3DViewer)
* 2227 Marker opacity not handled
* 2226 CERSAT import file KO
* 2225 Previmer import file KO
* 2224 SEGY file import KO
* 2223 Old MNT file format KO
* 2221 Memory management for Big DTM
* 1656 Navigation editor no longer work

----------------------------------------------------------------------------
# Version 1.4.0

* When merging 2 DTM with fill algorithm, process selected layers, but keep others.
* Recode geographic selection algo in smooth and reset DTM process

----------------------------------------------------------------------------

# Version 1.3.9

##What's new?
* Reconnect PLACA and merge last known bug corrections into trunk
* Standard deviation wrong calculation in DTM
* DTM cliping
* DTM reset geographic zones
* DTM compute smooth depth


##Close
* Correct a bug when importing CSV files
* Add keyboard control on Water Column player.

##Known issues
* 2230 Geoseas portal evolved and is no globe no longuer points to the good url
* 2229 CDI : when there is no network, the tooltip displays "No CDI data in this area"
* 2228 Subbottom file coloration is badly handle (as in 3DViewer)
* 2227 Marker opacity not handled
* 2226 CERSAT import file KO
* 2225 Previmer import file KO
* 2224 SEGY file import KO
* 2223 Old MNT file format KO
* 2221 Memory management for Big DTM
* 1656 Navigation editor no longer work

----------------------------------------------------------------------------

# Version 1.3.8
##What's new?
* session.xml file format changed (added PLACA session in this file)
* Add a behaviour to load/unload folder and keep them loaded or unloaded on startup (usefull for big data management)
* possibility to export or not missing values for a DTM
* refactor of DI fields in DTM export
* add globe version and module in DTM history netcdf attribute
* when merging DTMs, defaut output file name is "merged.dtm"
* possibility t modify a CDI in a DTM
* export DTM is possible in format Lon-Lat-Z
* number of CDI values in a DTM set to 32768
* merge DTM : possibility to use a simple average algorithm to compute the merge, does not take in account VSOUNDINGS


##Close :
* 2234 Ghost lines on DTM when zooming out. Focus on any side effect and report it to the team, none was seen during our tests
* 2217,2218 Layer explorer need to be reworked
* 2231 Session management must be refactor. Some parameters are not restored, file format of session.xml is to be reviewed for a long term support
* 2258 : in a DTM LINES and COLUMS variables computation after a DTM reduction
* 2259 : stupid VSOUNDINGS values equal to zero in a DTM
* 2262 : DTM merge : one cell shift to south west, corrected

##Known issues
* 2230 Geoseas portal evolved and is no globe no longuer points to the good url
* 2229 CDI : when there is no network, the tooltip displays "No CDI data in this area"
* 2228 Subbottom file coloration is badly handle (as in 3DViewer)
* 2227 Marker opacity not handled
* 2226 CERSAT import file KO
* 2225 Previmer import file KO
* 2224 SEGY file import KO
* 2223 Old MNT file format KO
* 2221 Memory management for Big DTM
* 1656 Navigation editor no longer work

----------------------------------------------------------------------------

# Version 1.3.7
##What's new?
* Layer Explorer was refactored, session.xml file format changed
* Dtm interpolation from context menu
* Emodnet ASCII export bug correction (CDI field)
* Change spinner values in multilayer parameter explorer
* Number of CDI values in a DTM set to 512
* Debug export from Simrad/Reson to mbg
* manage corrupted lines in CDI files
* Emodnet export : export geographic cell center points
* change CDI links to geoseas version 3





##Close :
* 2246 3DV Selection outil pointage
* 2231 Session management must be refactor. Some parameters are not restored, file format of session.xml is to be reviewed for a long term support
* 2217 Layer explorer : champs de recherche


##Known issues
* 2231 Session management must be refactor. Some parameters are not restored, file format of session.xml is to be reviewed for a long term support
* 2230 Geoseas portal evolved and is no globe no longuer points to the good url
* 2229 CDI : when there is no network, the tooltip displays "No CDI data in this area"
* 2228 Subbottom file coloration is badly handle (as in 3DViewer)
* 2227 Marker opacity not handled
* 2226 CERSAT import file KO
* 2225 Previmer import file KO
* 2224 SEGY file import KO
* 2223 Old MNT file format KO
* 2221 Memory management for Big DTM
* 2217,2218 Layer explorer need to be reworked

* 1656 Navigation editor no longer work
* 2234 Ghost lines on DTM when zooming out

----------------------------------------------------------------------------
# Version 1.3.6

##What's new?
* Imagery processing now delivered as a beta version (only works on windows)
* Generic filter editor : allow to filter and modify any attribute from sounder files
* Move globe data cache from tmp to globe_data

##Close :
* 2015 Handle marker size
* 2016 black circle on markers
* 2183 Delete entry in NasaWorldWind cache when file is deleted from session.
* 2154 Bag file cannot be read
* 2204 Water column
* 2222 Batch DTM - high number of soundings per cell
* 2195 pb with esri projection on caraibes
* 2118 better shading on DTMs
* 2182 Fix Emodnet ASCII export format
* 2192 merge mercator and any projection map
* 2193 configuration of Mercator projection parameters (especially latitude of true scale)
* 2189 display projected DTM in 3D viewer



##Known issues


* 2231 Session management must be refactor. Some parameters are not restored, file format of session.xml is to be reviewed for a long term support
* 2230 Geoseas portal evolved and is no globe no longuer points to the good url
* 2229 CDI : when there is no network, the tooltip displays "No CDI data in this area"
* 2228 Subbottom file coloration is badly handle (as in 3DViewer)
* 2227 Marker opacity not handled
* 2226 CERSAT import file KO
* 2225 Previmer import file KO
* 2224 SEGY file import KO
* 2223 Old MNT file format KO
* 2221 Memory management for Big DTM
* 2217,2218 Layer explorer need to be reworked
* 1656 Navigation editor no longer work
* 2234 Ghost lines on DTM when zooming out


----------------------------------------------------------------------------
# Version 1.3.5

##What’s new ?

* ew Java version (1.7). MacOS users must install JDK 7.
* New OpenGL version (2.1)
* New NASA World Wind version (2.0)
* New option to automatically stick DTM geoboxes to integer number of minutes.
* Fixed bug when merging big DTMs (wrong position of data, big geographic errors)
* Export ASCII format reviewed
* CDI picking when two DTM overlap reviewed
* Correction of minor geographic shifts in DTM display (one cell shift)
* Taking in account depth sign in text sounder files
* Deleting Nasa World Wind Cache Entries when deleting an item from
* Layer Explorer
* Deleting Nasa World Wind Entries when deleting a group of items from


##Known Issues
=======