package fr.ifremer.globe.netcdf.test;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.ifremer.globe.netcdf.test.netcdf.TestAttributesManipulation;
import fr.ifremer.globe.netcdf.test.netcdf.TestGroupAPI;
import fr.ifremer.globe.netcdf.test.netcdf.TestVariables;
import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * Test suite for maven , declared test will be launched while compiling with
 * command mvn integration-test
 * 
 */
@RunWith(Suite.class)
@SuiteClasses({ TestAttributesManipulation.class, TestGroupAPI.class, TestVariables.class })
public class MvnTestSuite {
	@AfterClass
	public static void cleanUp() {
		TemporaryCache.cleanAllFiles();
	}
}
