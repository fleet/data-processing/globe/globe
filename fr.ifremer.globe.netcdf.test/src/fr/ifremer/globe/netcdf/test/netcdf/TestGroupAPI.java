package fr.ifremer.globe.netcdf.test.netcdf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import fr.ifremer.globe.netcdf.jna.Nc4prototypes;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCGroup;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.netcdf.ucar.NCFile.Version;

public class TestGroupAPI {

    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    /** test creation of groups */
    @Test
    public void testCreateGroups() throws NCException, IOException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {

            NCGroup grp = writer.addGroup("A");
            // re create an existing group returns the same one
            assertSame(grp, writer.addGroup("A"));
            
            writer.addGroup("B");
            // groups can be nested
            writer.addGroup("C").addGroup("D");            
        }

        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            assertEquals(3, reader.getGroups().size());
            assertEquals("B", reader.getGroup("B").getName());
            assertEquals(1, reader.getGroup("C").getGroups().size());
            
        }
    }

    /** test creation of dimensions */
    @Test
    public void testCreateDimensions() throws NCException, IOException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            writer.addDimension("simple_dim", 5);
        }

        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            assertEquals(5, reader.getDimension("simple_dim").getLength());
        }
    }
    
    /** test redefinition of dimensions */
    @Test
    public void testRedefineDimension() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("simple_dim", 5);
            
            // recreate the dim with the same name / size return the sume object
            assertSame(dim, writer.addDimension("simple_dim", 5));
            
            try {
                writer.addDimension("simple_dim", 6);
                fail("should not be possible to redefine a dim with different width");
            } catch (NCException e) {
                assertTrue(true);
            }
        }
    }
    
    /**
     * test that a subgroup can define a dimension with the same name as a dimension of a parent group
     */
    @Test
    public void testRedefineDimensionsSubGroups() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            writer.addDimension("simple_dim", 5);
            writer.addGroup("subgroup").addDimension("simple_dim", 6);
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            assertEquals(5, reader.getDimension("simple_dim").getLength());
            assertEquals(6, reader.getGroup("subgroup").getDimension("simple_dim").getLength());
        }
    }
    
    @Test
    public void testSimpleAddVariable() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            final NCDimension simpleVectorDim = writer.addDimension("vectorDim", 2);
            final List<NCDimension> varShape = Arrays.asList(simpleVectorDim);
            
            writer.addVariable("simpleVector", DataType.INT, varShape);
        }

        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable vector = reader.getVariable("simpleVector");

            assertEquals("simpleVector", vector.getName());
            assertEquals(Nc4prototypes.NC_INT, vector.getType());
            assertEquals(1, vector.getShape().size());
            assertEquals("vectorDim", vector.getShape().get(0).getName());
            assertEquals(2, vector.getShape().get(0).getLength());
        }
    }
    
    @Test
    public void testRecreateVariable() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            final NCDimension simple_dim = writer.addDimension("simple_dim", 2);
            final List<NCDimension> varShape = Arrays.asList(simple_dim);
            
            NCVariable var = writer.addVariable("var", DataType.INT, varShape);
            assertSame(var, writer.addVariable("var", DataType.INT, varShape));
        }
    }
    
    @Test
    public void testRecreateVariableDifferentType() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            final NCDimension simple_dim = writer.addDimension("simple_dim", 2);
            final List<NCDimension> varShape = Arrays.asList(simple_dim);
            
            writer.addVariable("var", DataType.INT, varShape);
            try {
                writer.addVariable("var", DataType.LONG, varShape);
                fail();
            } catch (NCException ex) {
                assertTrue(true);
            }
        }
    }
    
    @Test
    public void testRecreateVariableDifferentOrder() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            final NCDimension simple_dim = writer.addDimension("simple_dim", 2);
            final List<NCDimension> varShape = Arrays.asList(simple_dim);
            final List<NCDimension> varShape2 = Arrays.asList(simple_dim, simple_dim);
            
            writer.addVariable("var", DataType.INT, varShape);
            try {
                writer.addVariable("var", DataType.INT, varShape2);
                fail();
            } catch (NCException ex) {
                assertTrue(true);
            }
        }
    }
    
    @Test
    public void testRecreateVariableDifferentShape() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            final NCDimension simple_dim = writer.addDimension("simple_dim", 2);
            final NCDimension simple_dim_2 = writer.addDimension("simple_dim_2", 2);
            final List<NCDimension> varShape = Arrays.asList(simple_dim);
            final List<NCDimension> varShape2 = Arrays.asList(simple_dim_2);
            
            writer.addVariable("var", DataType.INT, varShape);
            try {
                writer.addVariable("var", DataType.INT, varShape2);
                fail("should not be possible to recreate a var with a different shape");
            } catch (NCException ex) {
                assertTrue(true);
            }
        }
    }
}
