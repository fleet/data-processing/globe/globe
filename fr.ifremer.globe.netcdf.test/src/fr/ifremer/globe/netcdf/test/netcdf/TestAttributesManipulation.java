package fr.ifremer.globe.netcdf.test.netcdf;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import fr.ifremer.globe.netcdf.jna.Nc4prototypes;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCGroup;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.netcdf.ucar.NCFile.Version;

public class TestAttributesManipulation {
    
    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();

    @Test
    public void testCountNoAttInFile() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCGroup grp = writer.addGroup("group1");
            grp.addVariable("someVar", DataType.BYTE, Arrays.asList(grp.addDimension("someDim", 1)));
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            assertEquals(0, reader.getAttributeCount());
            assertEquals(0, reader.getGroup("group1").getAttributeCount());
            assertEquals(0, reader.getGroup("group1").getVariable("someVar").getAttributeCount());
        }
    }

    @Test
    public void testCountSomeAtts() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            writer.addAttribute("firstAtt", "someVal");
            NCGroup grp = writer.addGroup("some_group");
            grp.addAttribute("one_att", 1.0);
            grp.addAttribute("otherAtt", 2);
            NCVariable var = grp.addVariable("var1", DataType.INT, Arrays.asList(writer.addDimension("dim", 1)));
            var.addAttribute("name", "var");
            var.addAttribute("add_offset", 0.0);
            var.addAttribute("scare_factor", 10.0);
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            assertEquals(1, reader.getAttributeCount());
            assertEquals(2, reader.getGroup("some_group").getAttributeCount());
            assertEquals(3, reader.getGroup("some_group").getVariable("var1").getAttributeCount());
        }
    }
    
    @Test
    public void testAttributeNameList() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            writer.addAttribute("firstAtt", "someVal");
            NCGroup grp = writer.addGroup("some_group");
            grp.addAttribute("one_att", 1.0);
            grp.addAttribute("otherAtt", 2);
            NCVariable var = grp.addVariable("var1", DataType.INT, Arrays.asList(writer.addDimension("dim", 1)));
            var.addAttribute("name", "var");
            var.addAttribute("add_offset", 0.0);
            var.addAttribute("scare_factor", 10.0);
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            assertEquals(1, reader.getAttributeCount());
            assertEquals(2, reader.getGroup("some_group").getAttributeCount());
            assertEquals(3, reader.getGroup("some_group").getVariable("var1").getAttributeCount());
            
            assertArrayEquals(new String[] {"firstAtt"}, reader.getAttributeNames());
            assertArrayEquals(new String[] {"one_att", "otherAtt"}, reader.getGroup("some_group").getAttributeNames());
            assertArrayEquals(new String[] {"name", "add_offset", "scare_factor" }, reader.getGroup("some_group").getVariable("var1").getAttributeNames());
        }
    }
    
    @Test
    public void testAttributeType() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            writer.addAttribute("firstAtt", "someVal");
            writer.addAttribute("someFloatAtt", 1.0f);
            writer.addAttribute("someDoubleAtt", 1.0d);
            writer.addAttribute("someIntAtt", (int)1.0);
            writer.addAttribute("someLongAtt", (long)1.0);
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            // we have NC_CHAR for single string. NC_STRING denodet an array of strings. strange.
            assertEquals(Nc4prototypes.NC_CHAR, reader.getAttributeType("firstAtt"));
            assertEquals(Nc4prototypes.NC_FLOAT, reader.getAttributeType("someFloatAtt"));
            assertEquals(Nc4prototypes.NC_DOUBLE, reader.getAttributeType("someDoubleAtt"));
            assertEquals(Nc4prototypes.NC_INT, reader.getAttributeType("someIntAtt"));
            assertEquals(Nc4prototypes.NC_INT64, reader.getAttributeType("someLongAtt"));
        }
    }
    
    @Test
    public void testReadAttribute() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            writer.addAttribute("firstAtt", "someVal");
            writer.addAttribute("someFloatAtt", 1.1f);
            writer.addAttribute("someDoubleAtt", 1.1d);
            writer.addAttribute("someIntAtt", (int)1);
            writer.addAttribute("someLongAtt", (long)1);
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            // we have NC_CHAR for single string. NC_STRING denodet an array of strings. strange.
            assertEquals("someVal", reader.getAttributeText("firstAtt"));
            assertEquals(1.1, reader.getAttributeFloat("someFloatAtt"), 0.00001);
            assertEquals(1.1, reader.getAttributeDouble("someDoubleAtt"), 0.00001);
            assertEquals(1, reader.getAttributeInt("someIntAtt"));
            assertEquals(1, reader.getAttributeLong("someLongAtt"));
            
            // check the API allows to read int attrs into floats (conversions are possible
            assertEquals(1.0, reader.getAttributeFloat("someIntAtt"), 0.0001);
            // there might be implicit conversions that decrease precision
            assertEquals(1, reader.getAttributeInt("someFloatAtt"));
        }
    }
}
