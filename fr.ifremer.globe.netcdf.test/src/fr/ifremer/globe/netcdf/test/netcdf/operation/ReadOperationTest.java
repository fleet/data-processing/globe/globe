package fr.ifremer.globe.netcdf.test.netcdf.operation;

import java.nio.ByteBuffer;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.netcdf.api.buffer.ByteBufferWrapper;
import fr.ifremer.globe.netcdf.api.buffer.DoubleBufferWrapper;
import fr.ifremer.globe.netcdf.api.buffer.FloatBufferWrapper;
import fr.ifremer.globe.netcdf.api.buffer.IntBufferWrapper;
import fr.ifremer.globe.netcdf.api.buffer.LongBufferWrapper;
import fr.ifremer.globe.netcdf.api.buffer.ShortBufferWrapper;
import fr.ifremer.globe.netcdf.api.buffer.UByteBufferWrapper;


public class ReadOperationTest {

	@Test
	public void testSimpleValues()
	{
		ByteBuffer buff=ByteBuffer.allocateDirect(Double.BYTES);
		buff.put(Byte.MAX_VALUE);
		buff.rewind();
		Assert.assertEquals(Byte.MAX_VALUE,new ByteBufferWrapper(buff).getAsByte(0));
		buff.rewind();
		buff.asDoubleBuffer().put(Double.MAX_VALUE);
		Assert.assertEquals(Double.MAX_VALUE,new DoubleBufferWrapper(buff).getAsDouble(0),0);
		
		buff.rewind();
		buff.asFloatBuffer().put(Float.MAX_VALUE);
		Assert.assertEquals(Float.MAX_VALUE,new FloatBufferWrapper(buff).getAsFloat(0),0);

		buff.rewind();
		buff.asLongBuffer().put(Long.MAX_VALUE);
		Assert.assertEquals(Long.MAX_VALUE,new LongBufferWrapper(buff).getAsLong(0),0);

		buff.rewind();
		buff.asIntBuffer().put(Integer.MAX_VALUE);
		Assert.assertEquals(Integer.MAX_VALUE,new IntBufferWrapper(buff).getAsInt(0),0);
		
		buff.rewind();
		buff.asShortBuffer().put(Short.MAX_VALUE);
		Assert.assertEquals(Short.MAX_VALUE,new ShortBufferWrapper(buff).getAsShort(0),0);
	}
	
	@Test
	public void testSignedValues()
	{
		//allocate the biggest buffer
		ByteBuffer buff=ByteBuffer.allocateDirect(Double.BYTES); 
		//we write something with all byte set to 1
		buff.putLong(0xFFFFFFFF);

		UByteBufferWrapper sourceBuffer = new UByteBufferWrapper(buff);
		// test for ubyte reading and upcasting to short
		short expectedValue= 0xFF;
		buff.rewind();
		Assert.assertEquals(expectedValue, sourceBuffer.getAsShort(0));
		
		
		// test for write conversion to a higher buffer
		ByteBuffer buffWrite=ByteBuffer.allocateDirect(Double.BYTES); 
		buffWrite.rewind();
		buffWrite.putLong(0);
		UByteBufferWrapper destinationBuffer = new UByteBufferWrapper(buffWrite);

		destinationBuffer.putFrom(sourceBuffer, 0);
		
		buffWrite.rewind();

		Assert.assertEquals(expectedValue, destinationBuffer.getAsShort(0));
	}
}
