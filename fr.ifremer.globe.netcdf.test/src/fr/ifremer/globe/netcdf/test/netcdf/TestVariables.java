package fr.ifremer.globe.netcdf.test.netcdf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.netcdf.ucar.NCFile.Version;

public class TestVariables {

    @Rule
    public TemporaryFolder tmp = new TemporaryFolder();
    
    @Test
    public void testWriteRead1S() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.BYTE, Arrays.asList(dim));
            var.put(new long[] { 0 }, new long[] { 3 }, new byte[] {0, 5, -1});
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable var = reader.getVariable("dat");
            
            // read data in the native type
            byte[] dat = var.get_byte(new long[] { 0 }, new long[] { 3 });
            assertEquals(0, dat[0]);
            assertEquals(5, dat[1]);
            assertEquals(-1, dat[2]);

        }
    }
    
    @Test
    public void testWriteRead1U() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.UBYTE, Arrays.asList(dim));
            var.putu(new long[] { 0 }, new long[] { 3 }, new byte[] {0, 5, (byte) 255});
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable var = reader.getVariable("dat");
            
            // read data in the native type
            byte[] dat = var.get_ubyte(new long[] { 0 }, new long[] { 3 });
            assertEquals(0, Byte.toUnsignedInt(dat[0]));
            assertEquals(5, Byte.toUnsignedInt(dat[1]));
            assertEquals(255, Byte.toUnsignedInt(dat[2]));

            // read the data into another type, conversion handeled by the netCDF api
            short[] short_dat = var.get_short(new long[] { 0 }, new long[] { 3 });
            assertEquals(0, short_dat[0]);
            assertEquals(5, short_dat[1]);
            assertEquals(255, short_dat[2]);
        }
    }
        
    @Test
    public void testWriteRead2S() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.SHORT, Arrays.asList(dim));
            var.put(new long[] { 0 }, new long[] { 3 }, new short[] {0, 5, -1});
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable var = reader.getVariable("dat");
            
            // read data in the native type
            short[] dat = var.get_short(new long[] { 0 }, new long[] { 3 });
            assertEquals(0, dat[0]);
            assertEquals(5, dat[1]);
            assertEquals(-1, dat[2]);

        }
    }
    
    
    @Test
    public void testWriteRead2U() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.USHORT, Arrays.asList(dim));
            var.putu(new long[] { 0 }, new long[] { 3 }, new short[] {0, 5, (byte) 65535});
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable var = reader.getVariable("dat");
            
            // read data in the native type
            short[] dat = var.get_ushort(new long[] { 0 }, new long[] { 3 });
            assertEquals(0, Short.toUnsignedInt(dat[0]));
            assertEquals(5, Short.toUnsignedInt(dat[1]));
            assertEquals(65535, Short.toUnsignedInt(dat[2]));

            // read the data into another type, conversion handeled by the netCDF api
            int[] short_dat = var.get_int(new long[] { 0 }, new long[] { 3 });
            assertEquals(0, short_dat[0]);
            assertEquals(5, short_dat[1]);
            assertEquals(65535, short_dat[2]);
        }
    }
    
    @Test
    public void testWriteRead4S() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.INT, Arrays.asList(dim));
            var.put(new long[] { 0 }, new long[] { 3 }, new int[] {0, 5, -1});
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable var = reader.getVariable("dat");
            
            // read data in the native type
            int[] dat = var.get_int(new long[] { 0 }, new long[] { 3 });
            assertEquals(0, dat[0]);
            assertEquals(5, dat[1]);
            assertEquals(-1, dat[2]);

        }
    }
    
    
    @Test
    public void testWriteRead4U() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.UINT, Arrays.asList(dim));
            var.putu(new long[] { 0 }, new long[] { 3 }, new int[] {0, 5, (int) 4294967295l});
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable var = reader.getVariable("dat");
            
            // read data in the native type
            int[] dat = var.get_uint(new long[] { 0 }, new long[] { 3 });
            assertEquals(0, Integer.toUnsignedLong(dat[0]));
            assertEquals(5, Integer.toUnsignedLong(dat[1]));
            assertEquals(4294967295l, Integer.toUnsignedLong(dat[2]));

            // read the data into another type, conversion handeled by the netCDF api
            long[] short_dat = var.get_long(new long[] { 0 }, new long[] { 3 });
            assertEquals(0, short_dat[0]);
            assertEquals(5, short_dat[1]);
            assertEquals(4294967295l, short_dat[2]);
        }
    }
    
    @Test
    public void testWriteRead8S() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.LONG, Arrays.asList(dim));
            var.put(new long[] { 0 }, new long[] { 3 }, new long[] {0, 5, -1});
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable var = reader.getVariable("dat");
            
            // read data in the native type
            long[] dat = var.get_long(new long[] { 0 }, new long[] { 3 });
            assertEquals(0, dat[0]);
            assertEquals(5, dat[1]);
            assertEquals(-1, dat[2]);

        }
    }
    
    
    @Test
    public void testWriteRead8U() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.ULONG, Arrays.asList(dim));
            var.putu(new long[] { 0 }, new long[] { 3 }, new long[] {0, 5, -1});
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable var = reader.getVariable("dat");

            // read the data as unsigned long
            long[] dat = var.get_ulong(new long[] { 0 }, new long[] { 3 });
            assertEquals(0, dat[0]);
            assertEquals(5, dat[1]);
            // the value is properly read, but miss-understoud by java types
            assertEquals(-1, dat[2]);
            
            // read the data as signed long. only the 2 first varues are readable
            long[] sdat = var.get_ulong(new long[] { 0 }, new long[] { 2 });
            assertEquals(0, sdat[0]);
            assertEquals(5, sdat[1]);
            // the 3rd one causes an error since the netCDF api tells it cannot convert
            // to signed long because of overflow
            try {
                var.get_long(new long[] { 3 }, new long[] { 1 });
                fail("cannot read unsigned long data if it is bigger than a long");
            } catch (NCException ex) {
                assertTrue(true);
            }
        }
    }
    
    @Test
    public void testWriteReadF() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.FLOAT, Arrays.asList(dim));
            var.put(new long[] { 0 }, new long[] { 3 }, new float[] {0.0f, 5.5f, -1.0f});
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable var = reader.getVariable("dat");
            
            // read data in the native type
            float[] dat = var.get_float(new long[] { 0 }, new long[] { 3 });
            assertEquals(0.0f, dat[0], 1e-5);
            assertEquals(5.5f, dat[1], 1e-5);
            assertEquals(-1.0f, dat[2], 1e-5);

        }
    }
    
    @Test
    public void testWriteReadD() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.DOUBLE, Arrays.asList(dim));
            var.put(new long[] { 0 }, new long[] { 3 }, new double[] {0.0f, 5.5f, -1.0f});
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable var = reader.getVariable("dat");
            
            // read data in the native type
            double[] dat = var.get_double(new long[] { 0 }, new long[] { 3 });
            assertEquals(0.0f, dat[0], 1e-5);
            assertEquals(5.5f, dat[1], 1e-5);
            assertEquals(-1.0f, dat[2], 1e-5);

        }
    }
    
    @Test
    public void testWriteReadStr() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.STRING, Arrays.asList(dim));
            var.put(new long[] { 0 }, new long[] { 3 }, new String[] {"one", "two", "three"});
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable var = reader.getVariable("dat");
            
            // read data in the native type
            String[] dat = var.get_string(new long[] { 0 }, new long[] { 3 });
            assertEquals("one", dat[0]);
            assertEquals("two", dat[1]);
            assertEquals("three", dat[2]);

        }
    }
    
    @Test
    public void testWriteSetFillValue() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.USHORT, Arrays.asList(dim));
            var.setShortFillValue((short) 0);
        }
        
        try (NCFile reader = NCFile.open(filename, Mode.readonly)) {
            NCVariable var = reader.getVariable("dat");
            
            // read data in the native type
            int[] dat = var.get_int(new long[] { 0 }, new long[] { 3 });
            assertEquals(0, dat[0]);
            assertEquals(0, dat[1]);
            assertEquals(0, dat[2]);
        }
    }
    
    @Test
    public void testResetSameFillValue() throws IOException, NCException {
        final String filename = tmp.newFile("test.nc").getAbsolutePath();
        try (NCFile writer = NCFile.createNew(Version.netcdf4, filename)) {
            NCDimension dim = writer.addDimension("siple", 3);
            NCVariable var = writer.addVariable("dat", DataType.USHORT, Arrays.asList(dim));
            var.setShortFillValue((short) 0);
            var.setShortFillValue((short) 0);
        }
    }
    
    
}
