package fr.ifremer.globe.netcdf.test.netcdf.operation;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import fr.ifremer.globe.netcdf.api.NetcdfFile;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.api.convention.CFStandardNames;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.netcdf.ucar.NCFile.Mode;
import fr.ifremer.globe.netcdf.ucar.NCUnitError;
import fr.ifremer.globe.netcdf.ucar.NCVariable;
import ucar.units.StandardUnitFormat;
import ucar.units.Unit;

public class UnitOperationTest {
	@Rule
	public TemporaryFolder tmp = new TemporaryFolder();

	private static final String DIM_X_NAME = "TEST_DIM_X";
	private static final String TEST_VAR_NAME = "TEST_VAR";

	private int expectedDimX = 100;

	private void MakeFile(String filename, Optional<Double> scaleFactor, Optional<Double> addOffset,
			Optional<String> unit) throws Exception {
		// generate the file
		NCFile file = NCFile.createNew(NCFile.Version.netcdf4, filename);
		List<NCDimension> dimList = new ArrayList<>();

		file.addAttribute("description", "file generated for test at " + LocalDateTime.now().toString());
		// Add dimensions
		dimList.add(file.addDimension(DIM_X_NAME, expectedDimX));

		// Create variable (= layer)
		NCVariable var = file.addVariable(TEST_VAR_NAME, DataType.INT, dimList);
		var.setIntFillValue(0); // fill value with 0
		if (scaleFactor.isPresent()) {
			double scale_factor = scaleFactor.get();
			var.addAttribute(CFStandardNames.CF_SCALE_FACTOR, scale_factor);
		}
		if (addOffset.isPresent()) {
			double add_offset = addOffset.get();
			var.addAttribute(CFStandardNames.CF_ADD_OFFSET, add_offset);
		}
		if (unit.isPresent()) {
			String unitName = unit.get();
			var.addAttribute(CFStandardNames.CF_UNITS, unitName);
		}
		file.synchronize();
		int[] inputData = new int[expectedDimX];
		for (int i = 0; i < inputData.length; i++) {
			inputData[i] = getValue(i);
		}

		var.put(new long[] { 0 }, new long[] { expectedDimX }, inputData);
		file.close();

	}

	class Tuple {
		public Tuple(MappedByteBuffer buffer, RandomAccessFile file) {
			super();
			this.buffer = buffer;
			this.file = file;
		}

		MappedByteBuffer buffer;
		RandomAccessFile file;

		public void close() throws IOException {
			file.close();
		}
	}

	private Tuple read(String filename, DataType askedType, Optional<Unit> unit) throws NCException, IOException {
		// read the file
		try (NetcdfFile file = NetcdfFile.open(filename, Mode.readwrite)) {
			long dimX = file.getDimension(DIM_X_NAME).getLength();
			NetcdfVariable var = file.getVariable(TEST_VAR_NAME);
			MappedByteBuffer mappedFile = null;

			File tmpFile = tmp.newFile("GLOBE_TEST_READ_XSF");
			tmpFile.deleteOnExit();
			RandomAccessFile raf = new RandomAccessFile(tmpFile, "rw");
			FileChannel channel = raf.getChannel();
			mappedFile = channel.map(MapMode.READ_WRITE, 0, Double.BYTES * dimX);
			mappedFile.order(ByteOrder.nativeOrder());
			// Read XSF variable
			var.read(new long[] { dimX }, mappedFile, askedType, unit);
			mappedFile.rewind();

			return new Tuple(mappedFile, raf);
		}
	}

	@Test
	public void testScaleFactorAndOffset() throws Exception {
		double add_offset = 10.0;
		double scale_factor = 5.0;
		final String filename = tmp.newFile("test.nc").getAbsolutePath();
		MakeFile(filename, Optional.of(scale_factor), Optional.of(add_offset), Optional.empty());
		Tuple tup = read(filename, DataType.DOUBLE, Optional.empty());

		DoubleBuffer buff = tup.buffer.asDoubleBuffer();
		for (int i = 0; i < expectedDimX; i++) {
			double value = buff.get(i);
			Assert.assertEquals("index :" + i, getValue(i) * scale_factor + add_offset, value, 10e-6);
		}
	}

	@Test
	public void testNone() throws Exception {
		final String filename = tmp.newFile("test.nc").getAbsolutePath();
		MakeFile(filename, Optional.empty(), Optional.empty(), Optional.empty());
		Tuple tup = read(filename, DataType.INT, Optional.empty());
		tup.buffer.rewind();
		IntBuffer buff = tup.buffer.asIntBuffer();
		for (int i = 0; i < expectedDimX; i++) {
			int value = buff.get(i);
			Assert.assertEquals("index :" + i, getValue(i), value);
		}
		tup.close();
	}

	@Test
	public void testUnitEmpty() throws Exception {
		final String filename = tmp.newFile("test.nc").getAbsolutePath();
		MakeFile(filename, Optional.empty(), Optional.empty(), Optional.of("cm"));

		Tuple tup = read(filename, DataType.INT, Optional.empty());
		tup.buffer.rewind();

		IntBuffer buff = tup.buffer.asIntBuffer();
		for (int i = 0; i < expectedDimX; i++) {
			int value = buff.get(i);
			Assert.assertEquals("index :" + i, getValue(i), value);
		}
		tup.close();
	}

	@Test
	public void testUnitMeters() throws Exception {
		final String filename = tmp.newFile("test.nc").getAbsolutePath();
		MakeFile(filename, Optional.empty(), Optional.empty(), Optional.of("cm"));

		Unit unitDst = StandardUnitFormat.instance().parse("m");
		Tuple tup = read(filename, DataType.DOUBLE, Optional.of(unitDst));
		tup.buffer.rewind();

		DoubleBuffer buff = tup.buffer.asDoubleBuffer();
		for (int i = 0; i < expectedDimX; i++) {
			double value = buff.get(i);
			// convert from cm to m
			Assert.assertEquals("index :" + i, getValue(i) * 0.01, value, 10e-6);
		}
		tup.close();
	}

	@Test(expected = NCUnitError.class)
	public void testUnitIncompatible() throws Exception {
		final String filename = tmp.newFile("test.nc").getAbsolutePath();
		MakeFile(filename, Optional.empty(), Optional.empty(), Optional.of("Hz"));

		Unit unitDst = StandardUnitFormat.instance().parse("m");
		Tuple tup = read(filename, DataType.DOUBLE, Optional.of(unitDst));
		tup.buffer.rewind();

		DoubleBuffer buff = tup.buffer.asDoubleBuffer();
		for (int i = 0; i < expectedDimX; i++) {
			double value = buff.get(i);
			// convert from cm to m
			Assert.assertEquals("index :" + i, getValue(i) * 0.01, value, 10e-6);
		}
		tup.close();
	}

	/**
	 * compute expected value for index x
	 */
	int getValue(int x) {
		return x;
	}
}
