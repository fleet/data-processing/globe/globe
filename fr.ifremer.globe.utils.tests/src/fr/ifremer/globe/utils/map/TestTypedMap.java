/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.utils.map;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests of class TypedMap
 */
public class TestTypedMap {

	@Test
	public void testTypedMap() {

		// Empy map
		TypedMap map = TypedMap.of();
		Assert.assertEquals(0, map.size());

		//
		TypedKey<Boolean> falseBoolKey = new TypedKey<>("falseBool");
		TypedKey<Integer> intKey = new TypedKey<>("int");

		// Known keys
		map = TypedMap.of(falseBoolKey.bindValue(false), intKey.bindValue(12));
		Assert.assertEquals(2, map.size());
		Assert.assertEquals(true, map.containsKey(falseBoolKey));
		Assert.assertEquals(Boolean.FALSE, map.get(falseBoolKey).orElse(null));
		Assert.assertEquals(true, map.containsKey(intKey));
		Assert.assertEquals(Integer.valueOf(12), map.get(intKey).orElse(null));

		// Unknown key
		TypedKey<Boolean> trueBoolKey = new TypedKey<>("trueBool");
		Assert.assertEquals(false, map.containsKey(trueBoolKey));

		// New key
		map.put(trueBoolKey.bindValue(true));
		Assert.assertEquals(3, map.size());
		Assert.assertEquals(true, map.containsKey(trueBoolKey));

		// Replace entry
		map.put(intKey.bindValue(44));
		Assert.assertEquals(true, map.containsKey(intKey));
		Assert.assertEquals(Integer.valueOf(44), map.get(intKey).orElse(Integer.MAX_VALUE));

		// Remove entry
		map.remove(intKey);
		Assert.assertEquals(2, map.size());

	}

}
