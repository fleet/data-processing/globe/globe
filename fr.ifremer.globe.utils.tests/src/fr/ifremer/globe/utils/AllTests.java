package fr.ifremer.globe.utils;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.ifremer.globe.utils.map.TestTypedMap;
import fr.ifremer.globe.utils.maths.TestMatrixBuilder;
import fr.ifremer.globe.utils.maths.TestVectorOps;
import fr.ifremer.globe.utils.units.GlobeUnitsTest;
import fr.ifremer.globe.utils.units.UDUnitTests;

@RunWith(Suite.class)
@SuiteClasses({ GlobeUnitsTest.class, UDUnitTests.class, TestVectorOps.class, TestMatrixBuilder.class,
		TestTypedMap.class })
public class AllTests {

}
