package fr.ifremer.globe.utils;

import org.junit.Assert;
import org.junit.Test;

public class Temp {
	@Test
	public void test()
	{
		for(int i=0;i<Integer.MAX_VALUE;i++)
		{
			Number n=i*1.0f+0f;
			Assert.assertEquals(i,n.intValue());
		}
	}
}
