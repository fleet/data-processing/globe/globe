package fr.ifremer.globe.utils.units;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.utils.units.GlobeUnitDB;
import ucar.units.BaseUnit;
import ucar.units.ConversionException;
import ucar.units.NameException;
import ucar.units.NoSuchUnitException;
import ucar.units.PrefixDBException;
import ucar.units.SI;
import ucar.units.SpecificationException;
import ucar.units.StandardUnitFormat;
import ucar.units.Unit;
import ucar.units.UnitClassException;
import ucar.units.UnitDBException;
import ucar.units.UnitParseException;
import ucar.units.UnitSystemException;

public class GlobeUnitsTest {
	static final double TEST_PRECISION = 10e-10;

	/**
	 * sample checking for conversion
	 */
	@Test
	public void testBaseUnitsConverter() throws ConversionException {
		Unit deg = SI.ARC_DEGREE;
		double value = deg.convertTo(10., SI.RADIAN);
		Assert.assertEquals("Invalid result ", value, 10 * (Math.PI / 180), 0);
		double[] p = { 10., 10. };
		double[] ret = deg.convertTo(p, SI.RADIAN);
		double[] expected = { 10 * Math.PI / 180, 10 * Math.PI / 180 };
		Assert.assertArrayEquals(expected, ret, 0);
	}

	/**
	 * print default known units 
	 * */ 
	@Test
	public void printStandard() throws UnitDBException, PrefixDBException
	{
		// init our own units 
	//	GlobeUnitDB.instance();

		System.err.println("Standard Unit");
		List<Pair<String,String>> values=GlobeUnitDB.instance().getStdUnits();
		for(Pair<String,String> value:values)
		{
			System.err.println("unit " + value.getKey() + " - symbol (" + value.getValue() + ")");

		}
		
		values=GlobeUnitDB.instance().getStdPrefixes();

		for(Pair<String,String> prefix:values)
		{
			System.err.println("unit " + prefix.getKey() + " - symbol (" + prefix.getValue() + ")");

		}
		System.err.println("end");
	}
	@Test
	public void testMetricFormat() throws ConversionException, NoSuchUnitException, UnitParseException,
	SpecificationException, UnitDBException, PrefixDBException, UnitSystemException {
		Unit meter = StandardUnitFormat.instance().parse("m");
		Unit centimeter = StandardUnitFormat.instance().parse("cm");
		Unit decimeter = StandardUnitFormat.instance().parse("dm");
		Unit kilometer = StandardUnitFormat.instance().parse("km");
		Unit millimeter = StandardUnitFormat.instance().parse("mm");

		// check conversion
		Assert.assertEquals("Invalid result ", 1., centimeter.convertTo(100., meter), 0);
		Assert.assertEquals("Invalid result ", 1., decimeter.convertTo(10., meter), 0);
		Assert.assertEquals("Invalid result ", 1000., kilometer.convertTo(1., meter), 0);
		Assert.assertEquals("Invalid result ", 1., millimeter.convertTo(1000., meter), 0);

	}

	@Test
	public void testSpeedFormat() throws ConversionException, NoSuchUnitException, UnitParseException,
	SpecificationException, UnitDBException, PrefixDBException, UnitSystemException {
		Unit speedms = StandardUnitFormat.instance().parse("m/s");
		Unit speedkmh = StandardUnitFormat.instance().parse("km/h");
		Assert.assertEquals("Invalid result ", 1000. / (3600), speedkmh.convertTo(1., speedms), 0);
	}

	@Test
	public void testFnFormat() throws ConversionException, NoSuchUnitException, UnitParseException,
	SpecificationException, UnitDBException, PrefixDBException, UnitSystemException {
		Unit herz = StandardUnitFormat.instance().parse("Hz");
		Unit kherz = StandardUnitFormat.instance().parse("kHz");
		Assert.assertEquals("Invalid result ", 1, herz.convertTo(1000., kherz), 0);
	}

	@Test
	public void testTimeFormat() throws ConversionException, NoSuchUnitException, UnitParseException,
	SpecificationException, UnitDBException, PrefixDBException, UnitSystemException {
		Unit second = StandardUnitFormat.instance().parse("s");
		Unit millisecond = StandardUnitFormat.instance().parse("ms");
		Unit nanosecond = StandardUnitFormat.instance().parse("ns");
		Unit microsecond = StandardUnitFormat.instance().parse("us");
		Assert.assertEquals("Invalid result ", 1, millisecond.convertTo(1000., second), 0);
		Assert.assertEquals("Invalid result ", 1, nanosecond.convertTo(1e9, second), 0);
		Assert.assertEquals("Invalid result ", 1, microsecond.convertTo(1e6, second), 0);
	}

	@Test
	public void testEnergyFormat() throws ConversionException, NoSuchUnitException, UnitParseException,
	SpecificationException, UnitDBException, PrefixDBException, UnitSystemException, NameException {
		final BaseUnit amplitude =GlobeUnitDB.instance().getAcousticAmplitudeUnit();
		Unit energy = GlobeUnitDB.instance().getAcousticEnergy();
		Unit dbEner= GlobeUnitDB.instance().getAcousticDB();

		double x = 3;



		// check conversion from Amp to Energy, ie check if a value is x in amp,
		// the value in x*x in energy
		Assert.assertEquals(x*x, amplitude.convertTo(x, energy), TEST_PRECISION);

		// check conversion from energy to amplitude
		Assert.assertEquals(Math.sqrt(x),energy.convertTo(x, amplitude),TEST_PRECISION);


		// check conversion from energy to dbEnergy 
		Assert.assertEquals(10* Math.log10(x), energy.convertTo(x, dbEner),TEST_PRECISION);

		// check conversion from dbEnergy to energy
		Assert.assertEquals( Math.pow(10,x/10.), dbEner.convertTo(x, energy),TEST_PRECISION);

		// check conversion from db to amplitude
		Assert.assertEquals( Math.sqrt(Math.pow(10,x/10.)), dbEner.convertTo(x, amplitude),TEST_PRECISION);

		// check conversion from amplitude to db
		Assert.assertEquals( 20* Math.log10(x), amplitude.convertTo(x, dbEner),TEST_PRECISION);
	}

	@Test
	public void testAcousticUnitParsing() throws NameException, UnitSystemException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, UnitClassException, ConversionException
	{
		// check if we find our units
		Unit energy = GlobeUnitDB.instance().getAcousticEnergy();
		Unit ener= StandardUnitFormat.instance().parse(GlobeUnitDB.energyUnitSymbol);
		Assert.assertNotNull(ener);
		Assert.assertEquals(ener, energy);

		Unit amplitude = GlobeUnitDB.instance().getAcousticAmplitudeUnit();
		Unit amp= StandardUnitFormat.instance().parse(GlobeUnitDB.amplitudeUnitSymbol);
		Assert.assertNotNull(amp);
		Assert.assertEquals(amplitude, amp);


		Unit dbUnit= GlobeUnitDB.instance().getAcousticDB();
		Unit db= StandardUnitFormat.instance().parse(GlobeUnitDB.acousticDecibelUnitSymbol);
		Assert.assertNotNull(db);
		Assert.assertEquals(dbUnit, db);

		// check if we could convert centi decibel to decibel
		Unit centidb= StandardUnitFormat.instance().parse("c"+GlobeUnitDB.acousticDecibelUnitSymbol);
		Assert.assertNotNull(centidb);
		Assert.assertEquals("Invalid result ", 1., centidb.convertTo(100., dbUnit), TEST_PRECISION);

	}
	
	@Test 
	public void testAngleFormat() throws NoSuchUnitException, UnitParseException, SpecificationException, UnitDBException, PrefixDBException, UnitSystemException, ConversionException
	{
		Unit arc_degree= StandardUnitFormat.instance().parse("arc_degree");
		Unit deg = SI.ARC_DEGREE;
		double input=10;
		double value = deg.convertTo(input, arc_degree);
		Assert.assertEquals("Invalid result ", value, input, 0);
		
	}
	@Test
	/**
	 * test units definition from mbg
	 * */
	public void testMbgUnits() throws Exception
	{
		String mbgUnit[]= {"Julian_date","ms","degree","Khz","m","m/s","Hz","dB","s","%"};
		for(String unit: mbgUnit)
		{
			Unit amp= StandardUnitFormat.instance().parse(unit);
			Assert.assertNotNull(amp);
		}
	}
}
