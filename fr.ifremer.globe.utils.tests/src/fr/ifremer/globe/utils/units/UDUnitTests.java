package fr.ifremer.globe.utils.units;

import java.util.Calendar;
import java.util.TimeZone;

import org.junit.Assert;
import org.junit.Test;

import ucar.units.BaseQuantity;
import ucar.units.BaseUnit;
import ucar.units.DivideException;
import ucar.units.LogarithmicUnit;
import ucar.units.MultiplyException;
import ucar.units.OffsetUnit;
import ucar.units.RaiseException;
import ucar.units.ScaledUnit;
import ucar.units.Unit;
import ucar.units.UnitName;
import ucar.units.UnknownUnit;

public class UDUnitTests {
	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void testOffsetUnit() throws Exception {
		final BaseUnit kelvin = BaseUnit.getOrCreate(UnitName.newUnitName("kelvin", null, "K"), BaseQuantity.THERMODYNAMIC_TEMPERATURE);
		final OffsetUnit celsius = new OffsetUnit(kelvin, 273.15);
		System.out.println("celsius.equals(kelvin)=" + celsius.equals(kelvin));
		System.out.println("celsius.getUnit().equals(kelvin)=" + celsius.getUnit().equals(kelvin));
		Assert.assertTrue( !celsius.equals(kelvin));
		Assert.assertTrue( celsius.getUnit().equals(kelvin));

		final Unit celsiusKelvin = celsius.multiplyBy(kelvin);
		System.out.println("celsiusKelvin.divideBy(celsius)=" + celsiusKelvin.divideBy(celsius));
		System.out.println("celsius.divideBy(kelvin)=" + celsius.divideBy(kelvin));
		System.out.println("kelvin.divideBy(celsius)=" + kelvin.divideBy(celsius));
		System.out.println("celsius.raiseTo(2)=" + celsius.raiseTo(2));
		System.out.println("celsius.toDerivedUnit(1.)=" + celsius.toDerivedUnit(1.));
		System.out.println("celsius.toDerivedUnit(new float[]{1,2,3}, new float[3])[1]=" + celsius.toDerivedUnit(new float[]{1, 2, 3}, new float[3])[1]);
		System.out.println("celsius.fromDerivedUnit(274.15)=" + celsius.fromDerivedUnit(274.15));
		System.out.println("celsius.fromDerivedUnit(new float[]{274.15f},new float[1])[0]=" + celsius.fromDerivedUnit(new float[]{274.15f}, new float[1])[0]);
		System.out.println("celsius.equals(celsius)=" + celsius.equals(celsius));
		Assert.assertTrue( celsius.equals(celsius));

		final OffsetUnit celsius100 = new OffsetUnit(celsius, 100.);
		System.out.println("celsius.equals(celsius100)=" + celsius.equals(celsius100));
		System.out.println("celsius.isDimensionless()=" + celsius.isDimensionless());
		Assert.assertTrue( !celsius.equals(celsius100));
		Assert.assertTrue( !celsius.isDimensionless());

		final BaseUnit radian = BaseUnit.getOrCreate(UnitName.newUnitName("radian", null, "rad"), BaseQuantity.PLANE_ANGLE);
		final OffsetUnit offRadian = new OffsetUnit(radian, 3.14159 / 2);
		System.out.println("offRadian.isDimensionless()=" + offRadian.isDimensionless());
		Assert.assertTrue( offRadian.isDimensionless());
		
	}

	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void testLogarithmicUnit() throws Exception {
		final BaseUnit meter = BaseUnit.getOrCreate(UnitName.newUnitName("meter", null, "m"), BaseQuantity.LENGTH);
		final ScaledUnit micron = new ScaledUnit(1e-6, meter);
		final Unit cubicMicron = micron.raiseTo(3);
		final LogarithmicUnit Bz = new LogarithmicUnit(cubicMicron, 10.0);
		Assert.assertTrue( Bz.isDimensionless());
		Assert.assertTrue( Bz.equals(Bz));
		Assert.assertTrue( Bz.getReference().equals(cubicMicron));
		Assert.assertTrue( Bz.getBase() == 10.0);
		Assert.assertTrue( !Bz.equals(cubicMicron));
		Assert.assertTrue( !Bz.equals(micron));
		Assert.assertTrue( !Bz.equals(meter));
		try {
			Bz.multiplyBy(meter);
			Assert.assertTrue( false);
		} catch (final MultiplyException e) {
		}
		try {
			Bz.divideBy(meter);
			Assert.assertTrue( false);
		} catch (final DivideException e) {
		}
		try {
			Bz.raiseTo(2);
			Assert.assertTrue( false);
		} catch (final RaiseException e) {
		}
		double value = Bz.toDerivedUnit(0);
		Assert.assertTrue( 0.9e-18 < value && value < 1.1e-18 );
		value = Bz.toDerivedUnit(1);
		Assert.assertTrue( 0.9e-17 < value && value < 1.1e-17 );
		value = Bz.fromDerivedUnit(1e-18);
		Assert.assertTrue( -0.1 < value && value < 0.1 );
		value = Bz.fromDerivedUnit(1e-17);
		Assert.assertTrue( 0.9 < value && value < 1.1 );
		final String string = Bz.toString();
		Assert.assertTrue( string.equals("lg(re 9.999999999999999E-19 m3)"));
	}

	@Test
	public void testTimeScaleUnit() throws Exception {
		final TimeZone tz = TimeZone.getTimeZone("UTC");
		final Calendar calendar = Calendar.getInstance(tz);
		calendar.clear();
		calendar.set(1970, 0, 1);
		//   TimeScaleUnit tunit = new TimeScaleUnit(TimeScaleUnit.SECOND, calendar.getTime());
		//	    System.out.printf("%s%n",tunit );
	}

	@Test
	public void testUnknownUnit() throws Exception  {
		final UnknownUnit unit1 = UnknownUnit.create("a");
		Assert.assertTrue("unit1.equals(unit1)=" + unit1.equals(unit1), unit1.equals(unit1)  );
		Assert.assertTrue(  "UnknownUnit.isDimensionless()=" + unit1.isDimensionless(),!unit1.isDimensionless() );
		UnknownUnit unit2 = UnknownUnit.create("b");
		Assert.assertTrue( "unit1.equals(unit2)="+unit1.equals(unit2),!unit1.equals(unit2));
		unit2 = UnknownUnit.create("A");
		Assert.assertTrue(  "unit_a.equals(unit_A))="+unit1.equals(unit2),unit1.equals(unit2));
	}
}
