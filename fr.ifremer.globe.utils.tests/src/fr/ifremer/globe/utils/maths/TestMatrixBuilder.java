package fr.ifremer.globe.utils.maths;

import org.ejml.alg.fixed.FixedOps3;
import org.ejml.data.FixedMatrix3x3_64F;
import org.junit.Assert;
import org.junit.Test;

public class TestMatrixBuilder {
	private static final double DELTA = 10e-6;

	/**
	 * Ensure that Matrix created by matrix builder are really rotation matrix
	 * */
	@Test
	public void testMatrixBuilder()
	{
		RotationMatrix3x3D rot=new RotationMatrix3x3D();
		MatrixBuilder.setHeadingRotationMatrix(Math.random(), rot);
		checkRotationMatrix(rot);

		MatrixBuilder.setRollRotationMatrix(Math.random(), rot);
		checkRotationMatrix(rot);

		MatrixBuilder.setPithRotationMatrix(Math.random(), rot);
		checkRotationMatrix(rot);
		
		MatrixBuilder.setRotationMatrix(Math.random(), Math.random(), Math.random(), rot);
		checkRotationMatrix(rot);
	}
	@Test 
	public void testRotationMatrix()
	{
		double heading=Math.random();
		double pitch=Math.random();
		double roll=Math.random();
		RotationMatrix3x3D expected=new RotationMatrix3x3D();
		RotationMatrix3x3D pitchM=new RotationMatrix3x3D();
		RotationMatrix3x3D rollM=new RotationMatrix3x3D();
		RotationMatrix3x3D headingM=new RotationMatrix3x3D();
		RotationMatrix3x3D tmp=new RotationMatrix3x3D();
		MatrixBuilder.setRollRotationMatrix(roll, rollM);
		MatrixBuilder.setPithRotationMatrix(pitch, pitchM);
		MatrixBuilder.setHeadingRotationMatrix(heading, headingM);
		Ops.mult(pitchM, rollM, tmp);
		Ops.mult(headingM, tmp, expected);
		
		RotationMatrix3x3D created=new RotationMatrix3x3D();
		MatrixBuilder.setRotationMatrix(heading, pitch, roll, created);
		checkMatrix(expected.imp, created.imp, DELTA);
	}
	private void checkRotationMatrix(RotationMatrix3x3D rot)
	{
		double det=FixedOps3.det(rot.imp);
		Assert.assertEquals(1.0, det,DELTA);
		FixedMatrix3x3_64F identity=new FixedMatrix3x3_64F();
		FixedOps3.setIdentity(identity);

		/*
		 * check that transpose(m)m=Identity
		 */
		FixedMatrix3x3_64F m=new FixedMatrix3x3_64F();
		FixedMatrix3x3_64F res=new FixedMatrix3x3_64F();
		FixedOps3.transpose(rot.imp,m);
		FixedOps3.mult(rot.imp, m, res);
		checkMatrix(identity, res, DELTA);
		FixedOps3.mult(m,rot.imp, res);
		checkMatrix(identity, res, DELTA);
	
	}
	@Test
	public void checkRotationMatrixCreation()
	{
		RotationMatrix3x3D rot=new  RotationMatrix3x3D();
		FixedMatrix3x3_64F identity=new FixedMatrix3x3_64F();
		FixedOps3.setIdentity(identity);
		checkMatrix(identity, rot.imp, DELTA);
	
	}
	public static void checkMatrix(FixedMatrix3x3_64F expected, FixedMatrix3x3_64F actual, double delta)
	{
		Assert.assertEquals(expected.a11, expected.a11,delta);
		Assert.assertEquals(expected.a12, expected.a12,delta);
		Assert.assertEquals(expected.a13, expected.a13,delta);

		Assert.assertEquals(expected.a21, expected.a21,delta);
		Assert.assertEquals(expected.a22, expected.a22,delta);
		Assert.assertEquals(expected.a23, expected.a23,delta);

		Assert.assertEquals(expected.a31, expected.a31,delta);
		Assert.assertEquals(expected.a32, expected.a32,delta);
		Assert.assertEquals(expected.a33, expected.a33,delta);

	}
}
