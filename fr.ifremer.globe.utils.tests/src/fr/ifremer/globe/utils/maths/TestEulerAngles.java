package fr.ifremer.globe.utils.maths;

import org.junit.Assert;
import org.junit.Test;

public class TestEulerAngles {
	double angle = 0.15 * Math.PI;

	@Test
	public void testHeading() {
		// test heading
		testAngles(-angle, 0, 0);
		testAngles(angle, 0, 0);

	}

	@Test
	public void testRoll() {
		testAngles(0, 0, angle);
		testAngles(0, 0, -angle);

	}

	@Test
	public void testPitch() {
		testAngles(0, angle, 0);
		testAngles(0, -angle, 0);

	}

	/**
	 * We test with random vector if angle are well retrieved. Note that by convention, angle for the pitch angle cover
	 * [0,pi] (see https://en.wikipedia.org/wiki/Euler_angles#Tait.E2.80.93Bryan_angles)
	 * 
	 */
	@Test
	public void testRandom() {
		int batch = 1000;
		for (int i = 0; i < batch; i++) {

			double v1 = (2 * Math.random() - 1) * Math.PI;
			double v2 = (2 * Math.random() - 1) * Math.PI / 2;
			double v3 = (2 * Math.random() - 1) * Math.PI;
			testAngles(v1, v2, v3);
		}

	}

	private void testAngles(double heading, double pitch, double roll) {
		RotationMatrix3x3D rot = new RotationMatrix3x3D();
		Vector3D rotation = new Vector3D(roll, pitch, heading);
		MatrixBuilder.setRotationMatrix(rotation.getZ(), rotation.getY(), rotation.getX(), rot);
		Vector3D inverseAngle = Ops.toEulerAngle(rot);
		chekResult(inverseAngle, rotation);

	}

	private void chekResult(Vector3D result, Vector3D expected) {

		Assert.assertEquals(result + " vs " + expected, result.getZ(), expected.getZ(), 10e-6);
		Assert.assertEquals(result + " vs " + expected, result.getX(), expected.getX(), 10e-6);
		Assert.assertEquals(result + " vs " + expected, result.getY(), expected.getY(), 10e-6);
	}

}
