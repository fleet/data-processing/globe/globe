package fr.ifremer.globe.utils.maths;

import java.util.function.DoubleBinaryOperator;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test math operation, <p>
 * operation backed up and made by ejml implementation are supposed to be already tested
 * */
public class TestVectorOps {
	@Test
	public void testVectorOpCross()
	{
		Vector3D x=new Vector3D(1,0,0);
		Vector3D y=new Vector3D(0,1,0);
		Vector3D z=new Vector3D(0, 0, 1);
		Vector3D res=new Vector3D();
		Ops.cross(x, y, res);
		Assert.assertEquals(z, res);
		Ops.cross(y, z, res);
		Assert.assertEquals(x, res);
		Ops.cross(new Vector3D(0.5, 0,0), new Vector3D(0, 0.4, 0), res);
		Assert.assertEquals(res, new Vector3D(0, 0, 0.2));

		Ops.cross(new Vector3D(0.5, 0,0), new Vector3D(0, 0.4, 0), res);
		Assert.assertEquals(res, new Vector3D(0, 0, 0.2));

		Ops.cross(new Vector3D(0.5, 0.5,0), new Vector3D(-1, 0.4, 0), res);
		Assert.assertEquals(new Vector3D(0, 0, 0.7),res); // compare with pynum code

		for(int i=0;i<5;i++)
		{
			double a1=Math.random();
			double a2=Math.random();
			double a3=Math.random();
			double b1=Math.random();
			double b2=Math.random();
			double b3=Math.random();
			Ops.cross(new Vector3D(a1, a2,a3), new Vector3D(b1, b2, b3), res);

			//code extracted from stack overflow with no modification
			double res1=a2*b3-a3*b2;
			double res2=a3*b1-a1*b3;
			double res3 = a1*b2-a2*b1;
			Assert.assertEquals(res.imp.a1, res1,0);
			Assert.assertEquals(res.imp.a2, res2,0);
			Assert.assertEquals(res.imp.a3, res3,0);
		}
	}

	@Test
	public void testVectorOpAddSub()
	{
		testVectorOp((a,b)->a+b, Ops::add);
	}
	@Test
	public void testVectorOpSub()
	{
		testVectorOp((a,b)->a-b, Ops::substract);
	}
	public void testVectorOpScalarMul()
	{
		for(int i=0;i<5;i++)
		{
			double a1=Math.random();
			double a2=Math.random();
			double a3=Math.random();
			double scalar=Math.random();
			new Vector3D(a1, a2,a3);
			Vector3D out=new Vector3D();
			Ops.mul(new Vector3D(a1, a2,a3), scalar, out);
			Assert.assertEquals(out.imp.a1, a1*scalar,0);
			Assert.assertEquals(out.imp.a2, a2*scalar,0);
			Assert.assertEquals(out.imp.a3, a3*scalar,0);
		}
	}
	
	interface VectorOps{
		void apply(Vector3D a,Vector3D b, Vector3D res);
	}
	
	private void testVectorOp(DoubleBinaryOperator op, VectorOps ops)
	{
		for(int i=0;i<5;i++)
		{
			double a1=Math.random();
			double a2=Math.random();
			double a3=Math.random();
			double b1=Math.random();
			double b2=Math.random();
			double b3=Math.random();
			Vector3D res=new Vector3D();
			ops.apply(new Vector3D(a1, a2,a3), new Vector3D(b1, b2, b3), res);

			//code extracted from stack overflow with no modification
			double res1=op.applyAsDouble(a1, b1);
			double res2=op.applyAsDouble(a2,b2);
			double res3 =op.applyAsDouble(a3,b3);
			Assert.assertEquals(res.imp.a1, res1,0);
			Assert.assertEquals(res.imp.a2, res2,0);
			Assert.assertEquals(res.imp.a3, res3,0);
		}
	}

}
