package fr.ifremer.globe.utils.maths;

import org.ejml.data.FixedMatrix3x3_64F;
import org.junit.Assert;

public class TestUtils {
	private TestUtils() {};
	
	public static void checkMatrix(FixedMatrix3x3_64F expected, FixedMatrix3x3_64F actual, double delta)
	{
		Assert.assertEquals(expected.a11, expected.a11,delta);
		Assert.assertEquals(expected.a12, expected.a12,delta);
		Assert.assertEquals(expected.a13, expected.a13,delta);

		Assert.assertEquals(expected.a21, expected.a21,delta);
		Assert.assertEquals(expected.a22, expected.a22,delta);
		Assert.assertEquals(expected.a23, expected.a23,delta);

		Assert.assertEquals(expected.a31, expected.a31,delta);
		Assert.assertEquals(expected.a32, expected.a32,delta);
		Assert.assertEquals(expected.a33, expected.a33,delta);

	}
}
