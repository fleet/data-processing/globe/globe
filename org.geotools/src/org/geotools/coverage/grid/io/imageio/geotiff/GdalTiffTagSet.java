package org.geotools.coverage.grid.io.imageio.geotiff;

import it.geosolutions.imageio.plugins.tiff.TIFFTag;
import it.geosolutions.imageio.plugins.tiff.TIFFTagSet;

import java.util.ArrayList;
import java.util.List;

/**
 * GDAL Tags definition pour GeoTiff files.
 *
 */
public class GdalTiffTagSet extends TIFFTagSet {

	private static GdalTiffTagSet theInstance = null;

	/**
	 * Constant specifying the "GDAL_METADATA" tag.
	 */
	public static final int TAG_GDAL_METADATA = 42112;

	/**
	 * Constant specifying the "GDAL_METADATA" tag.
	 */
	public static final int TAG_GDAL_NODATA = 42113;

	static class GdalMetaData extends TIFFTag {

		public GdalMetaData() {
			super("GdalMetaData", TAG_GDAL_METADATA, 1 << TIFF_ASCII);
		}
	}

	static class GdalNoData extends TIFFTag {

		public GdalNoData() {
			super("GdalNoData", TAG_GDAL_NODATA, 1 << TIFF_DOUBLE);
		}
	}

	private static List<TIFFTag> tags;

	private static void initTags() {
		tags = new ArrayList<TIFFTag>(2);

		tags.add(new GdalTiffTagSet.GdalMetaData());
		tags.add(new GdalTiffTagSet.GdalNoData());
	}

	private GdalTiffTagSet() {
		super(tags);
	}

	/**
	 * Returns a shared instance of a <code>GdalTiffTagSet</code>.
	 *
	 * @return a <code>GdalTiffTagSet</code> instance.
	 */
	public synchronized static GdalTiffTagSet getInstance() {
		if (theInstance == null) {
			initTags();
			theInstance = new GdalTiffTagSet();
			tags = null;
		}
		return theInstance;
	}
}
