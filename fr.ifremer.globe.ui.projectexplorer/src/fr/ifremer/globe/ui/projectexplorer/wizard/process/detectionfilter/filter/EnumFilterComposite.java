/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.filter;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters.Comparator;

/**
 * Composite used to edit a numeric filtering criteria.
 */
public class EnumFilterComposite extends ADataComposite implements IConstants {
	private Combo value;

	protected String[] values;

	/**
	 * Constructor.
	 */
	public EnumFilterComposite(Composite parent, int style, String[] labels, String[] values) {
		super(parent, style);
		this.values = values;

		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = gridLayout.marginWidth = 0;
		setLayout(gridLayout);
		value = new Combo(this, SWT.READ_ONLY);
		value.setItems(labels);
		GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		value.setLayoutData(gridData);
		value.select(0);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.filter.ADataComposite#updateParameters(fr.ifremer.globe.imagery.script.parameter.SingleFilterParameters)
	 */
	@Override
	public void updateParameters(SingleFilterParameters parameters) {
		parameters.setValueA(values[value.getSelectionIndex()]);
	}

	/**
	 * Follow the link.
	 * 
	 * @see fr.ifremer.globe.editor.script.ui.filter.ADataComposite#getComparatorFilter()
	 */
	@Override
	public ViewerFilter getComparatorFilter() {
		ViewerFilter filter = new ViewerFilter() {

			@Override
			public boolean select(Viewer viewer, Object parentElement, Object element) {
				return element == Comparator.equal;
			}
		};
		return filter;
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.filter.ADataComposite#getDefaultComparator()
	 */
	@Override
	public Comparator getDefaultComparator() {
		return Comparator.equal;
	}
}
