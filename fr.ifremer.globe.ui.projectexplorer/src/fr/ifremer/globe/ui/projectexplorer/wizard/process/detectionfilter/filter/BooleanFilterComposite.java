/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.filter;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.IConstants;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters.Comparator;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * Composite used to edit a boolean filtering criteria.
 */
public class BooleanFilterComposite extends ADataComposite implements IConstants {
	private Button trueButton;
	private Button falseButton;

	/**
	 * Constructor.
	 */
	public BooleanFilterComposite(Composite parent, int style) {
		super(parent, style);

		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.marginHeight = gridLayout.marginWidth = 0;
		setLayout(gridLayout);

		trueButton = new Button(this, SWT.RADIO);
		trueButton.setText(Boolean.TRUE.toString());
		trueButton.setLayoutData(new GridData(GridData.GRAB_VERTICAL | GridData.VERTICAL_ALIGN_CENTER));
		trueButton.setSelection(true);

		falseButton = new Button(this, SWT.RADIO);
		falseButton.setText(Boolean.FALSE.toString());
		falseButton.setLayoutData(new GridData(GridData.GRAB_VERTICAL | GridData.VERTICAL_ALIGN_CENTER));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.filter.ADataComposite#updateParameters(fr.ifremer.globe.imagery.script.parameter.SingleFilterParameters)
	 */
	@Override
	public void updateParameters(SingleFilterParameters parameters) {
		parameters.setValueA(String.valueOf(NumberUtils.bool2byte(trueButton.getSelection())));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.filter.ADataComposite#getComparatorFilter()
	 */
	@Override
	public ViewerFilter getComparatorFilter() {
		ViewerFilter filter = new ViewerFilter() {
			@Override
			public boolean select(Viewer viewer, Object parentElement, Object element) {
				return element == Comparator.equal;
			}
		};
		return filter;
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.filter.ADataComposite#getDefaultComparator()
	 */
	@Override
	public Comparator getDefaultComparator() {
		return Comparator.equal;
	}
}
