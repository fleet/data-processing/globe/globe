package fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection;

import java.io.File;
import java.util.Optional;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.processes.TimeIntervalParameters;
import fr.ifremer.globe.core.processes.biascorrection.model.BiasCorrectionParameters;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;

/**
 * Model of the wizard page
 */
public class BiasCorrectionWizardModel extends ConvertWizardDefaultModel {

	private BiasCorrectionParameters biasCorrectionParameters = new BiasCorrectionParameters();
	private TimeIntervalParameters timeIntervalParameters = new TimeIntervalParameters(
			"fr.ifremer.globe.process.biascorrection");
	private static final String DEFAULT_BIAS_SUFFIX = "bias";

	/** Max number of antenna encountered in all files */
	private int antennaCount = 0;

	public BiasCorrectionWizardModel() {
		IFileService fileService = IFileService.grab();
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.getSounderNcContentType()));

		inputFiles.addChangeListener(event -> {
			// Update
			long minDate = Long.MAX_VALUE;
			long maxDate = -Long.MAX_VALUE;
			for (File file : inputFiles) {
				Optional<IFileInfo> infoStore = fileService.getFileInfo(file.getAbsolutePath());
				if (infoStore.isPresent() && infoStore.get().getContentType().isSounderNcInfo()) {
					ISounderNcInfo infoFile = (ISounderNcInfo) infoStore.get();
					minDate = Math.min(minDate, infoFile.getFirstPingDate().getTime());
					maxDate = Math.max(maxDate, infoFile.getLastPingDate().getTime());
					antennaCount = Math.max(antennaCount, infoFile.getAntennaParameters().size());
				}
			}
			if (minDate != Long.MAX_VALUE) {
				timeIntervalParameters.setLongProperty(TimeIntervalParameters.PROPERTY_STARTDATETIME_VALUE, minDate);
			}
			if (maxDate != -Long.MAX_VALUE) {
				timeIntervalParameters.setLongProperty(TimeIntervalParameters.PROPERTY_ENDDATETIME_VALUE, maxDate);
			}
		});

		suffix.set(DEFAULT_BIAS_SUFFIX);

	}

	// GETTERS

	public BiasCorrectionParameters getBiasCorrectionParameters() {
		return biasCorrectionParameters;
	}

	public TimeIntervalParameters getTimeIntervalParameters() {
		return timeIntervalParameters;
	}

	/**
	 * @return the {@link #antennaCount}
	 */
	public int getAntennaCount() {
		return antennaCount;
	}
}
