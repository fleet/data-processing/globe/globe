package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.model.projection.CoordinateSystem;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.CompositeCSLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.DetectionStatusExporter;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.layers.AbstractBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.AbstractBuffer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.LayersWalker;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.LayersWalkerParameters;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.DetectionStatusLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.exporter.NviToCsvExporter;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.exporter.SounderNcToCsvExporter;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * "Convert to CSV" process with wizard
 */
public class ConvertToCsvProcessWithWizard extends DefaultProcessWithWizard<ConvertToCsvWizardModel> {

	/** Factory of the sounder data container */
	private final LayersWalker layersWalker;

	/**
	 * Constructor
	 */
	public ConvertToCsvProcessWithWizard(Shell shell, LayersWalker layersWalker, Set<ContentType> inputContentTypes,
			String fileExtension) {
		super(shell, "Export to CSV", () -> new ConvertToCsvWizardModel(fileExtension), ConvertToCsvWizard::new);
		IFileService fileService = IFileService.grab();
		model.getInputFilesFilterExtensions().addAll(fileService.getFileFilters(inputContentTypes));
		this.layersWalker = layersWalker;
	}

	/** {@inheritDoc} */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		List<File> inputFiles = model.getInputFiles().asList();
		logger.info("Beginning conversion of {} files", inputFiles.size());
		SubMonitor submonitor = SubMonitor.convert(monitor, "Export to CSV in progress", inputFiles.size() * 100);

		for (File inputFile : model.getInputFiles().asList()) {
			File outputFile = outputFileNameComputer.compute(inputFile, model, model.getFileExtension());
			logger.info("Conversion of {} to {}", inputFile.getAbsolutePath(), outputFile.getAbsolutePath());
			if (shouldBeProcessed(outputFile, logger)) {
				try {
					if (ConvertToCsvWizardModel.VALIDITY_EXTENSION.equals(model.getFileExtension())) {
						exportValidityFlagsToCsv(inputFile, outputFile, submonitor.split(100));
					} else {
						exportDataContainerToCsv(inputFile, outputFile, submonitor.split(100));
					}
				} catch (OperationCanceledException e) {
					// Cancel required
					logger.info("Conversion of {} canceled", inputFile.getName());
					FileUtils.deleteQuietly(outputFile);
					return Status.CANCEL_STATUS;
				} catch (GIOException e) {
					logger.error("Error while converting {} to {} ({})", inputFile.getName(), outputFile.getName(),
							e.getMessage(), e);
					FileUtils.deleteQuietly(outputFile);
					throw e;
				}
			} else {
				logger.warn("{} exists. Conversion skipped", outputFile.getName());
			}
		}
		logger.info("Conversion of {} files done", inputFiles.size());
		return Status.OK_STATUS;
	}

	/**
	 * Execute the conversion of a NVI, MBG or XSF file to CSV
	 */
	protected void exportDataContainerToCsv(File inputFile, File outputFile, SubMonitor submonitor)
			throws GIOException {
		IFileInfo fileInfo = IFileService.grab().getFileInfo(inputFile.getAbsolutePath()).orElse(null);
		if (fileInfo != null) {
			IDataContainerInfoService dataContainerInfoService = IDataContainerInfoService.grab();
			if (fileInfo.getContentType() == ContentType.NVI_NETCDF_4) {
				Optional<IDataContainerInfo> dataContainerInfo = dataContainerInfoService
						.getDataContainerInfo(fileInfo);
				if (dataContainerInfo.isPresent()) {
					exportNviToCsv(dataContainerInfo.get(), outputFile, submonitor.split(100));
				}
			} else {
				Optional<ISounderNcInfo> sounderNcInfo = dataContainerInfoService.getSounderNcInfo(fileInfo);
				if (sounderNcInfo.isPresent()) {
					exportSounderNcToCsv(sounderNcInfo.get(), outputFile, submonitor.split(100));
				}
			}
		}
	}

	/**
	 * Execute the conversion of a NVI file to CSV
	 */
	protected void exportNviToCsv(IDataContainerInfo nviInfo, File outputFile, IProgressMonitor monitor)
			throws GIOException {
		Optional<INavigationDataSupplier> supplier = INavigationService.grab().getNavigationDataSupplier(nviInfo);
		if (supplier.isPresent())
			new NviToCsvExporter(model).export(supplier.get(), outputFile, monitor);
	}

	/**
	 * Execute the conversion to CSV files
	 */
	protected void exportSounderNcToCsv(ISounderNcInfo info, File outputFile, IProgressMonitor monitor)
			throws GIOException {
		try (SounderNcToCsvExporter csvExporter = new SounderNcToCsvExporter(model, outputFile)) {
			LayersWalkerParameters<ISounderNcInfo, SounderDataContainer> walkerParameters = new LayersWalkerParameters<>(
					info, this::grabSounderNcLayers, csvExporter);
			walkerParameters.setIndexesPredicate(this::testBeamValidity);
			layersWalker.walk(walkerParameters, monitor);
		}
	}

	/**
	 * Execute the conversion to CSV files
	 */
	protected void exportValidityFlagsToCsv(File inputFile, File outputFile, IProgressMonitor monitor)
			throws GIOException {
		Optional<ISounderNcInfo> option = IFileService.grab().getFileInfo(inputFile.getAbsolutePath())
				.filter(ISounderNcInfo.class::isInstance).map(ISounderNcInfo.class::cast);

		if (option.isPresent()) {
			ISounderNcInfo info = option.get();
			try (var detectionStatusExporter = new DetectionStatusExporter(layersWalker, info, outputFile,
					model.withHeader.isTrue(), model.columnSeparator.get())) {
				detectionStatusExporter.apply(monitor, logger);
			}
		}
	}

	/** @return the list of layers to export */
	protected List<AbstractBaseLayer<? extends AbstractBuffer>> grabValidityFlagsLayers(
			SounderDataContainer sounderDataContainer) throws GIOException {
		List<AbstractBaseLayer<? extends AbstractBuffer>> result = new ArrayList<>();

		result.add(sounderDataContainer.getLayer(BathymetryLayers.STATUS));
		result.add(sounderDataContainer.getLayer(BathymetryLayers.STATUS_DETAIL));

		return result;
	}

	/** @return the list of layers to export */
	protected List<AbstractBaseLayer<? extends AbstractBuffer>> grabSounderNcLayers(
			SounderDataContainer sounderDataContainer) throws GIOException {
		List<AbstractBaseLayer<? extends AbstractBuffer>> result = new ArrayList<>();
		CompositeCSLayers cSLayers = sounderDataContainer.getCsLayers(CoordinateSystem.FCS);
		result.add(cSLayers.getProjectedX());
		result.add(cSLayers.getProjectedY());
		result.add(cSLayers.getProjectedZ());
		return result;
	}

	/**
	 * @return true to allow the valid beam to be export
	 */
	protected boolean testBeamValidity(SounderDataContainer sounderDataContainer, int[] indexes) throws GIOException {
		DetectionStatusLayer statusLayer = sounderDataContainer.getStatusLayer();
		return statusLayer.isValid(indexes[0], indexes[1]);
	}

}
