package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.exporter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;

import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.ConvertToCsvWizardModel;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * CsvLayerExporter customizing the depth value and the header for sounder file
 */
public class NviToCsvExporter {

	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
			.withZone(ZoneOffset.UTC);

	public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss")
			.withZone(ZoneOffset.UTC);

	/** Model */
	protected ConvertToCsvWizardModel model;

	/**
	 * Constructor
	 */
	public NviToCsvExporter(ConvertToCsvWizardModel model) {
		this.model = model;
	}

	public void export(INavigationDataSupplier navigationDataSupplier, File outputFile, IProgressMonitor monitor)
			throws GIOException {
		char sep = model.getColumnSeparator().get();

		try (var navigationData = navigationDataSupplier.getNavigationData(true);
				BufferedWriter writer = Files.newBufferedWriter(outputFile.toPath(), StandardCharsets.UTF_8,
						StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {

			if (model.getWithHeader().isTrue()) {
				writer.append("Date" + sep + "Time" + sep + "Latitude (degrees)" + sep + "Longitude (degrees)" + sep
						+ "Immersion (m)" + sep + "Vessel speed (knots)" + sep + "Ship heading (degrees)");
				writer.newLine();
			}

			SubMonitor submonitor = SubMonitor.convert(monitor, "Export to CSV in progress", navigationData.size());
			for (int i = 0; i < navigationData.size(); i++) {
				if (navigationData.isValid(i)) {
					var optInstant = navigationData.getMetadataValue(NavigationMetadataType.TIME, i)
							.map(Instant::ofEpochMilli);

					writer.append(optInstant.map(DATE_FORMATTER::format).orElse("NOT_DEFINED"));
					writer.append(sep);

					writer.append(optInstant.map(TIME_FORMATTER::format).orElse("NOT_DEFINED"));
					writer.append(sep);

					writer.append(String.valueOf(navigationData.getLatitude(i)));
					writer.append(sep);

					writer.append(String.valueOf(navigationData.getLongitude(i)));
					writer.append(sep);

					var elevation = navigationData.getMetadataValue(NavigationMetadataType.ELEVATION, i)
							.map(value -> model.getElevationSign() * value).orElse(Double.NaN);
					writer.append(String.valueOf(elevation));
					writer.append(sep);

					writer.append(String.valueOf(
							navigationData.getMetadataValue(NavigationMetadataType.SPEED, i).orElse(Float.NaN)));
					writer.append(sep);

					writer.append(String.valueOf(
							navigationData.getMetadataValue(NavigationMetadataType.HEADING, i).orElse(Float.NaN)));
					writer.newLine();

					submonitor.worked(1);
				}
			}
		} catch (IOException e) {
			throw GIOException.wrap("Error on CSV Generation", e);
		}
	}

}
