package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tottb;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;

/**
 * Model of the wizard page
 */
public class ConvertToTtbWizardModel extends ConvertWizardDefaultModel {
	public static final String TTB_EXTENSION = "ttb";

	/**
	 * Constructor
	 */
	public ConvertToTtbWizardModel() {
		IFileService fileService = IFileService.grab();
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.TIDE_CSV));

		// output file extensions
		outputFormatExtensions.add(TTB_EXTENSION);

		loadFilesAfter.set(Boolean.TRUE);
	}
}