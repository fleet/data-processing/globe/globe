package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tonvi;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.SelectObservableValue;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import fr.ifremer.globe.core.io.nvi.service.INviWriter.SamplingMode;
import fr.ifremer.globe.ui.databinding.conversion.EqualsObjectToBooleanConverter;
import fr.ifremer.globe.ui.databinding.observable.WritableNumber;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;

/**
 * Wizard page used to define the output format in a sounder files convertion.
 */
public class SelectNviExportOptionPage extends WizardPage {
	/** Model in MVC */
	protected SelectNviExportOptionPageModel selectNviExportOptionPageModel;

	/** Widgets */
	protected Spinner soundingsSpinner;
	protected Spinner timeSpinner;
	protected Button noSamplingButton;
	protected Button timeSamplingButton;
	protected Button soundingsSamplingButton;

	/** View/model bindings */
	protected DataBindingContext bindingContext;
	protected Binding soundingsSpinnerBinding;
	protected Binding timeSpinnerBinding;

	/**
	 * Constructor
	 */
	public SelectNviExportOptionPage(SelectNviExportOptionPageModel model) {
		super(SelectNviExportOptionPage.class.getName(), "Select export option", null);
		this.selectNviExportOptionPageModel = model;
		updatePageComplete();
	}

	/**
	 * Creates the design of the input files selection page
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container_1 = new Composite(parent, SWT.NONE);
		GridLayout gl_container_1 = new GridLayout(1, false);
		gl_container_1.marginHeight = 0;
		container_1.setLayout(gl_container_1);
		setControl(container_1);

		Group nviGroup = new Group(container_1, SWT.NONE);
		nviGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		nviGroup.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_NVI_SAMPLING_GROUP")); //$NON-NLS-1$
		GridLayout gl_nviGroup = new GridLayout(3, false);
		gl_nviGroup.marginHeight = 10;
		nviGroup.setLayout(gl_nviGroup);

		noSamplingButton = new Button(nviGroup, SWT.RADIO);
		noSamplingButton.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_NO_SAMPLING_BUTTON")); //$NON-NLS-1$
		GridData gd_noSamplingButton = new GridData(SWT.LEFT, SWT.CENTER, true, false, 3, 1);
		gd_noSamplingButton.horizontalIndent = 15;
		noSamplingButton.setLayoutData(gd_noSamplingButton);

		timeSamplingButton = new Button(nviGroup, SWT.RADIO);
		GridData gd_soundingsSamplingButton;
		GridData gd_timeSamplingButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_timeSamplingButton.horizontalIndent = 15;
		timeSamplingButton.setLayoutData(gd_timeSamplingButton);
		timeSamplingButton.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_TIME_SAMPLING_BUTTON"));

		timeSpinner = new Spinner(nviGroup, SWT.BORDER);
		timeSpinner.setEnabled(false);
		timeSpinner.setMinimum(1);
		timeSpinner.setMaximum(100000);

		Label lblNewLabel_1 = new Label(nviGroup, SWT.NONE);
		lblNewLabel_1.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_TIME_SAMPLING_LABEL")); //$NON-NLS-1$

		soundingsSamplingButton = new Button(nviGroup, SWT.RADIO);
		gd_soundingsSamplingButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_soundingsSamplingButton.horizontalIndent = 15;
		soundingsSamplingButton.setLayoutData(gd_soundingsSamplingButton);
		soundingsSamplingButton.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_SOUNDINGS_SAMPLING_BUTTON"));

		soundingsSpinner = new Spinner(nviGroup, SWT.BORDER);
		soundingsSpinner.setEnabled(false);
		soundingsSpinner.setMinimum(1);
		soundingsSpinner.setMaximum(100000);

		Label lblNewLabel_2 = new Label(nviGroup, SWT.NONE);
		lblNewLabel_2.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_SOUNDINGS_SAMPLING_LABEL")); //$NON-NLS-1$

		bindingContext = initDataBindings();
		initCheckBoxGroupBindings();

		// force view updating
		timeSpinnerBinding.updateModelToTarget();
		soundingsSpinnerBinding.updateModelToTarget();

	}

	/** Sets whether this page is complete */
	protected void updatePageComplete() {
		setPageComplete(true);
	}

	/**
	 * Model of the OutputFormat wizard page
	 */
	public static interface SelectNviExportOptionPageModel {

		/**
		 * Getter of nviSamplingMode
		 */
		public WritableObject<SamplingMode> getNviSamplingMode();

		/**
		 * Getter of nviTimeSamplingValue
		 */
		public WritableNumber getNviTimeSamplingValue();

		/**
		 * Getter of SoundingsSamplingValue
		 */
		public WritableNumber getNviSoundingsSamplingValue();
	}

	/** Initialize a specific binding on CheckBoxGroup (not managed by Windows Builder */
	protected void initCheckBoxGroupBindings() {

		// Bind SamplingMode on radio buttons
		var observeSelectionNoSamplingButton = WidgetProperties.buttonSelection().observe(noSamplingButton);
		var observeSelectionTimeSamplingButton = WidgetProperties.buttonSelection().observe(timeSamplingButton);
		var observeSelectionSoundingsSamplingButton = WidgetProperties.buttonSelection()
				.observe(soundingsSamplingButton);
		var featureRepoPolicyObservable = new SelectObservableValue<SamplingMode>();
		featureRepoPolicyObservable.addOption(SamplingMode.NONE, observeSelectionNoSamplingButton);
		featureRepoPolicyObservable.addOption(SamplingMode.TIME, observeSelectionTimeSamplingButton);
		featureRepoPolicyObservable.addOption(SamplingMode.SOUNDINGS, observeSelectionSoundingsSamplingButton);
		bindingContext.bindValue(featureRepoPolicyObservable, selectNviExportOptionPageModel.getNviSamplingMode());
	}

	/**
	 * @see org.eclipse.jface.dialogs.DialogPage#setVisible(boolean)
	 */
	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			updatePageComplete();
		}
		super.setVisible(visible);
	}

	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();

		//
		var observeSelectionTimeSpinnerObserveWidget = WidgetProperties.spinnerSelection().observe(timeSpinner);
		bindingContext.bindValue(observeSelectionTimeSpinnerObserveWidget,
				selectNviExportOptionPageModel.getNviTimeSamplingValue(), null, null);
		//
		var observeSelectionSoundingsSpinnerObserveWidget = WidgetProperties.spinnerSelection()
				.observe(soundingsSpinner);
		bindingContext.bindValue(observeSelectionSoundingsSpinnerObserveWidget,
				selectNviExportOptionPageModel.getNviSoundingsSamplingValue(), null, null);
		//
		var observeEnabledTimeSpinnerObserveWidget = WidgetProperties.enabled().observe(timeSpinner);
		var strategy = new UpdateValueStrategy<SamplingMode, Boolean>();
		strategy.setConverter(new EqualsObjectToBooleanConverter<>(SamplingMode.TIME));
		timeSpinnerBinding = bindingContext.bindValue(observeEnabledTimeSpinnerObserveWidget,
				selectNviExportOptionPageModel.getNviSamplingMode(),
				new UpdateValueStrategy<>(UpdateValueStrategy.POLICY_NEVER), strategy);
		//
		var observeEnabledSoundingsSpinnerObserveWidget = WidgetProperties.enabled().observe(soundingsSpinner);
		var strategy_1 = new UpdateValueStrategy<SamplingMode, Boolean>();
		strategy_1.setConverter(new EqualsObjectToBooleanConverter<>(SamplingMode.SOUNDINGS));
		soundingsSpinnerBinding = bindingContext.bindValue(observeEnabledSoundingsSpinnerObserveWidget,
				selectNviExportOptionPageModel.getNviSamplingMode(),
				new UpdateValueStrategy<>(UpdateValueStrategy.POLICY_NEVER), strategy_1);
		//
		return bindingContext;
	}
}
