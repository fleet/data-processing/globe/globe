package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.exporter;

import java.io.File;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.CompositeCSLayers;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.AbstractBaseLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.exporter.CsvLayerExporter;
import fr.ifremer.globe.core.runtime.datacontainer.layers.operation.ToStringOperation;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.ConvertToCsvWizardModel;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * CsvLayerExporter customizing the depth value and the header for sounder file
 */
public class SounderNcToCsvExporter extends CsvLayerExporter<ISounderNcInfo, SounderDataContainer> {

	/** Constructor */
	public SounderNcToCsvExporter(ConvertToCsvWizardModel convertToCsvWizardModel, File outputFile) {
		super(outputFile, new DepthToStringOperation(convertToCsvWizardModel));
		setWithHeader(convertToCsvWizardModel.getWithHeader().isTrue());
		setColumnSeparator(convertToCsvWizardModel.getColumnSeparator().get());
	}

	/** {@inheritDoc} */
	@Override
	public <T extends AbstractBaseLayer<?>> String getHeader(T layer) {
		String result = layer.getName();
		if (CompositeCSLayers.PROJ_DETECTION_X.equals(result)) {
			result = "latitude";
		} else if (CompositeCSLayers.PROJ_DETECTION_Y.equals(result)) {
			result = "longitude";
		} else if (CompositeCSLayers.PROJ_DETECTION_Z.equals(result)) {
			result = "elevation";
		}
		return result;
	}

	/**
	 * ToStringOperation customizing the depth format
	 */
	static class DepthToStringOperation extends ToStringOperation {

		private final ConvertToCsvWizardModel model;

		/** Constructor */
		public DepthToStringOperation(ConvertToCsvWizardModel convertToCsvWizardModel) {
			this.model = convertToCsvWizardModel;
		}

		/** {@inheritDoc} */
		@Override
		public void visit(DoubleLayer2D layer) throws GIOException {
			if (CompositeCSLayers.PROJ_DETECTION_Z.equals(layer.getName())) {
				double depth = model.getElevationSign() * layer.get(indexes[0], indexes[1]);
				outputStringBuilder.append(depth);
			} else {
				super.visit(layer);
			}
		}
	}
}
