package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tovel;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;

/**
 * Model of the wizard page
 */
public class ConvertToVelWizardModel extends ConvertWizardDefaultModel {
	public static final String VEL_EXTENSION = "vel";

	/**
	 * Constructor
	 */
	public ConvertToVelWizardModel() {
		IFileService fileService = IFileService.grab();
		ContentType.getSounderRawFileContentTypes()
				.forEach(c -> inputFilesFilterExtensions.addAll(fileService.getFileFilters(c)));

		// output file extensions
		outputFormatExtensions.add(VEL_EXTENSION);

		loadFilesAfter.set(Boolean.TRUE);
	}
}