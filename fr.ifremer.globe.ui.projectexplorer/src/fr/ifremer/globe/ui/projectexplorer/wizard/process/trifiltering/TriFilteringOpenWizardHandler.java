package fr.ifremer.globe.ui.projectexplorer.wizard.process.trifiltering;

import java.util.Set;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

public class TriFilteringOpenWizardHandler extends AbstractOpenProcessWizardHandler<TriFilteringProcessWithWizard> {
	@Override
	protected TriFilteringProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new TriFilteringProcessWithWizard(shell);
	}

	/** {@inheritDoc} */
	@Override
	protected Set<ContentType> getAcceptedContentType() {
		// by default MBG & XSF are accepted
		return ContentType.getSounderNcContentType();
	}

}