package fr.ifremer.globe.ui.projectexplorer.wizard.process.cleanmbgflags;

import java.util.Set;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

/**
 * Handler class to open the process wizard (referenced in fragment.e4xmi).
 */
public class CleanMbgFlagsOpenWizardHandler extends AbstractOpenProcessWizardHandler<CleanMbgFlagsProcessWithWizard> {

	@Override
	protected Set<ContentType> getAcceptedContentType() {
		return Set.of(ContentType.MBG_NETCDF_3);
	}

	@Override
	protected CleanMbgFlagsProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new CleanMbgFlagsProcessWithWizard(shell);
	}

}