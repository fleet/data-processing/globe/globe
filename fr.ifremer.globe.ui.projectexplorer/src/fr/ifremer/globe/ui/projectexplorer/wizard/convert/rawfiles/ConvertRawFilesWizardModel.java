package fr.ifremer.globe.ui.projectexplorer.wizard.convert.rawfiles;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.core.io.nvi.service.INviWriter.SamplingMode;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.tonvi.ConvertToNviWizardModel;

/**
 * Model of the "Convert sounder files" wizard
 */
public class ConvertRawFilesWizardModel extends ConvertToNviWizardModel {
	/** Logger. */
	private static final Logger logger = LoggerFactory.getLogger(ConvertRawFilesWizardModel.class);

	/** .mbg */
	public static final String MBG_EXTENSION = IFileService.grab().getExtensions(ContentType.MBG_NETCDF_3).get(0);
	/** .nvi */
	public static final String NVI_EXTENSION = IFileService.grab().getExtensions(ContentType.NVI_V2_NETCDF_4).get(0);
	/** .xsf */
	public static final String XSF_EXTENSION = "xsf.nc";

	/** Convert to XSF required */
	private WritableBoolean xsfFormat = new WritableBoolean();
	/** Convert to mbg required */
	private WritableBoolean mbgFormat = new WritableBoolean();
	/** Convert to nvi required */
	private WritableBoolean nviFormat = new WritableBoolean();

	/** XSF parameters */
	private WritableString xsfTitle = new WritableString(XsfConverterParameters.TITLE_DEFAULT);
	private WritableString xsfSummary = new WritableString();
	private WritableString xsfKeywords = new WritableString(XsfConverterParameters.KEYWORDS_DEFAULT);
	private WritableString xsfLicense = new WritableString();
	private WritableString xsfRights = new WritableString();
	private WritableBoolean xsfIgnoreWC = new WritableBoolean();
	private WritableFile xsfReferenceFolder = new WritableFile();

	/** Mbg parameter */
	private WritableString mbgShipName = new WritableString();
	/** Mbg parameter */
	private WritableString mbgSurveyName = new WritableString();
	/** Mbg parameter */
	private WritableString mbgReferencePoint = new WritableString();
	/** Mbg parameter */
	private WritableString mbgCdi = new WritableString();

	/**
	 * Constructor
	 */
	public ConvertRawFilesWizardModel() {
		// input file extensions
		inputFilesFilterExtensions.clear(); // remove mbg/xsf extensions (from the mother class ConvertToNviWizardModel)
		inputFilesFilterExtensions.addAll(IFileService.grab().getFileFilters(ContentType.SOUNDER_ALL));
		inputFilesFilterExtensions.add(new Pair<>("Kongsberg (*.kmall)", "*.kmall"));
		inputFilesFilterExtensions.addAll(IFileService.grab().getFileFilters(ContentType.SOUNDER_S7K));

		// output file extensions
		outputFormatExtensions.clear();
		xsfFormat.addChangeListener(e -> {
			if (xsfFormat.isTrue()) {
				outputFormatExtensions.add(XSF_EXTENSION);
			} else {
				outputFormatExtensions.remove(XSF_EXTENSION);
			}
		});
		mbgFormat.addChangeListener(e -> {
			if (mbgFormat.isTrue()) {
				outputFormatExtensions.add(MBG_EXTENSION);
			} else {
				outputFormatExtensions.remove(MBG_EXTENSION);
			}
		});
		nviFormat.addChangeListener(e -> {
			if (nviFormat.isTrue()) {
				outputFormatExtensions.add(NVI_EXTENSION);
			} else {
				outputFormatExtensions.remove(NVI_EXTENSION);
			}
		});
		outputFormatExtensions.addChangeListener(e -> singleOutputFilename.set(null));
	}

	/** Serialize the values in the specified file in a json format */
	public void save(File outJsonFile) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try (JsonWriter writer = gson.newJsonWriter(new FileWriter(outJsonFile))) {
			writer.beginObject();
			// XSF
			writer.name("xsfFormat").value(xsfFormat.isTrue());
			writer.name("xsfTitle").value(xsfTitle.get());
			writer.name("xsfSummary").value(xsfSummary.get());
			writer.name("xsfKeywords").value(xsfKeywords.get());
			writer.name("xsfLicense").value(xsfLicense.get());
			writer.name("xsfRights").value(xsfRights.get());
			writer.name("xsfIgnoreWC").value(xsfIgnoreWC.isTrue());
			writer.name("xsfReferenceFolder")
					.value(xsfReferenceFolder.isNotNull() ? xsfReferenceFolder.get().getPath() : null);
			// MBG
			writer.name("mbgFormat").value(mbgFormat.isTrue());
			writer.name("mbgShipName").value(mbgShipName.get());
			writer.name("mbgSurveyName").value(mbgSurveyName.get());
			writer.name("mbgReferencePoint").value(mbgReferencePoint.get());
			writer.name("mbgCdi").value(mbgCdi.get());
			// NVI
			writer.name("nviFormat").value(nviFormat.isTrue());
			writer.name("nviSamplingMode").value(nviSamplingMode.get().name());
			writer.name("nviSoundingsSamplingValue").value(nviSoundingsSamplingValue.get());
			writer.name("nviTimeSamplingValue").value(nviTimeSamplingValue.get());

			writer.endObject();

		} catch (Exception e) {
			logger.warn("Unable to save the model in the json file", e);
		}
	}

	/** Load the values from the specified json file */
	public void load(File outJsonFile) {
		if (!outJsonFile.exists())
			return;

		Gson gson = new GsonBuilder().create();
		try (JsonReader reader = gson.newJsonReader(new FileReader(outJsonFile))) {
			reader.beginObject();
			while (reader.hasNext()) {
				switch (reader.nextName()) {
				// XSF
				case "xsfFormat" -> xsfFormat.set(reader.nextBoolean());
				case "xsfTitle" -> xsfTitle.set(reader.nextString());
				case "xsfSummary" -> xsfSummary.set(reader.nextString());
				case "xsfKeywords" -> xsfKeywords.set(reader.nextString());
				case "xsfLicense" -> xsfLicense.set(reader.nextString());
				case "xsfRights" -> xsfRights.set(reader.nextString());
				case "xsfIgnoreWC" -> xsfIgnoreWC.set(reader.nextBoolean());
				case "xsfReferenceFolder" -> {
					String folder = reader.nextString();
					xsfReferenceFolder.set(folder != null ? new File(folder) : null);
				}
				// MBG
				case "mbgFormat" -> mbgFormat.set(reader.nextBoolean());
				case "mbgShipName" -> mbgShipName.set(reader.nextString());
				case "mbgSurveyName" -> mbgSurveyName.set(reader.nextString());
				case "mbgReferencePoint" -> mbgReferencePoint.set(reader.nextString());
				case "mbgCdi" -> mbgCdi.set(reader.nextString());
				// NVI
				case "nviFormat" -> nviFormat.set(reader.nextBoolean());
				case "nviSamplingMode" -> nviSamplingMode.set(SamplingMode.valueOf(reader.nextString()));
				case "nviSoundingsSamplingValue" -> nviSoundingsSamplingValue.set(reader.nextInt());
				case "nviTimeSamplingValue" -> nviTimeSamplingValue.set(reader.nextInt());

				default -> logger.warn("Unable to retore the model from '{}' : bad format", outJsonFile.getPath());
				}

			}

			reader.endObject();
		} catch (Exception e) {
			logger.warn("Unable to load the model in the json file", e);
		}
	}

	/** Reset the output format parameters to default values */
	public void reset() {
		// XSF
		xsfFormat.set(false);
		xsfTitle.set(XsfConverterParameters.TITLE_DEFAULT);
		xsfSummary.set("");
		xsfKeywords.set(XsfConverterParameters.KEYWORDS_DEFAULT);
		xsfLicense.set("");
		xsfRights.set("");
		xsfIgnoreWC.set(false);
		xsfReferenceFolder.set(null);

		// MBG
		mbgFormat.set(false);
		mbgShipName.set("");
		mbgSurveyName.set("");
		mbgReferencePoint.set("");
		mbgCdi.set("");

		// NVI
		nviFormat.set(false);
		nviSamplingMode.set(SamplingMode.NONE);
		nviSoundingsSamplingValue.set(1);
		nviTimeSamplingValue.set(1);
	}

	@Override
	public Optional<WritableBoolean> getMergeOutputFiles() {
		return Optional.empty();
	}

	/**
	 * Getter of xsfFormat
	 */
	public WritableBoolean getXsfFormat() {
		return xsfFormat;
	}

	/**
	 * Getter of mbgFormat
	 */
	public WritableBoolean getMbgFormat() {
		return mbgFormat;
	}

	/**
	 * Getter of nviFormat
	 */
	public WritableBoolean getNviFormat() {
		return nviFormat;
	}

	// XSF parameters

	public WritableString getXsfTitle() {
		return xsfTitle;
	}

	public WritableString getXsfSummary() {
		return xsfSummary;
	}

	public WritableString getXsfKeywords() {
		return xsfKeywords;
	}

	public WritableString getXsfLicense() {
		return xsfLicense;
	}

	public WritableString getXsfRights() {
		return xsfRights;
	}

	/**
	 * Getter of mbgShipName
	 */
	public WritableString getMbgShipName() {
		return mbgShipName;
	}

	/**
	 * Getter of mbgSurveyName
	 */
	public WritableString getMbgSurveyName() {
		return mbgSurveyName;
	}

	/**
	 * Getter of mbgReferencePoint
	 */
	public WritableString getMbgReferencePoint() {
		return mbgReferencePoint;
	}

	/**
	 * Getter of mbgCdi
	 */
	public WritableString getMbgCdi() {
		return mbgCdi;
	}

	public WritableBoolean getXSFIgnoreWC() {
		return xsfIgnoreWC;
	}

	/** {@inheritDoc} */
	public WritableFile getXsfReferenceFolder() {
		return xsfReferenceFolder;
	}

}