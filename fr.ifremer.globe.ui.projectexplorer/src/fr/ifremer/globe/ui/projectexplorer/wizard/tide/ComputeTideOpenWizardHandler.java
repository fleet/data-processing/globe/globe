package fr.ifremer.globe.ui.projectexplorer.wizard.tide;

import java.util.List;
import java.util.Set;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

public class ComputeTideOpenWizardHandler extends AbstractOpenProcessWizardHandler<ComputeTideProcessWithWizard> {

	@Override
	protected ComputeTideProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new ComputeTideProcessWithWizard(shell);
	}

	/** {@inheritDoc} */
	@Override
	protected Set<ContentType> getAcceptedContentType() {
		// tide can be computed from navigation data
		return ContentType.getNavigationContentTypes();
	}

	/**
	 * Opens the wizard and launches the process in foreground.
	 */
	@Override
	protected void openWizard(ComputeTideProcessWithWizard processWithWizard) {
		if (processWithWizard.getWizardDialog().open() == Window.OK)
			processWithWizard.runInForeground();
		processWithWizard.getModel().dispose();
	}

	/**
	 * Checking if files are not booked is not useful here because this process don't modify them. Files are accessed in
	 * read-only mode.
	 */
	@Override
	protected boolean isBooked(List<IDataContainerInfo> dataContainerInfos) {
		return false;
	}

}