package fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

public class BiasCorrectionOpenWizardHandler extends AbstractOpenProcessWizardHandler<BiasCorrectionProcessWithWizard> {

	@Override
	protected BiasCorrectionProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new BiasCorrectionProcessWithWizard(shell);
	}

}