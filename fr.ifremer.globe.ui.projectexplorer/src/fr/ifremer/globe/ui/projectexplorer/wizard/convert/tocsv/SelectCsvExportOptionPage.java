package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv;

import java.util.Optional;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.value.SelectObservableValue;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;

/**
 * Wizard page used to define the CSV export options.
 */
public class SelectCsvExportOptionPage extends WizardPage {

	/** Model in MVC */
	protected SelectCsvExportOptionPageModel pageModel;

	/** View/model bindings */
	protected DataBindingContext dateBindingContext;

	private Button btnHeaders;
	private Text textOtherSeparator;
	private Button btnPositive;
	private Button btnNegative;

	/**
	 * Constructor
	 */
	public SelectCsvExportOptionPage(SelectCsvExportOptionPageModel selectOutputFormatPageModel) {
		super(SelectCsvExportOptionPage.class.getName(), "Select export option", null);
		pageModel = selectOutputFormatPageModel;
	}

	/**
	 * Creates the design of the input files selection page
	 */
	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout glComposite = new GridLayout(3, false);
		glComposite.verticalSpacing = 20;
		glComposite.horizontalSpacing = 10;
		glComposite.marginLeft = 10;
		glComposite.marginHeight = 10;
		composite.setLayout(glComposite);
		setControl(composite);

		Label lblHeaders = new Label(composite, SWT.NONE);
		lblHeaders.setText("Header : ");

		btnHeaders = new Button(composite, SWT.CHECK);
		new Label(composite, SWT.NONE);

		Label label = new Label(composite, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		label.setText("Column separator : ");

		Composite compositeSeparator = new Composite(composite, SWT.NONE);
		GridLayout glCompositeSeparator = new GridLayout(1, false);
		glCompositeSeparator.marginWidth = 0;
		glCompositeSeparator.marginHeight = 0;
		compositeSeparator.setLayout(glCompositeSeparator);

		Button btnSemicolon = new Button(compositeSeparator, SWT.RADIO);
		btnSemicolon.setText(DicoBundle.getString("WORD_SEMICOLON"));//$NON-NLS-1$
		btnSemicolon.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> setColumnSeparator(';')));

		Button btnComma = new Button(compositeSeparator, SWT.RADIO);
		btnComma.setText(DicoBundle.getString("WORD_COMMA") + "");//$NON-NLS-1$
		btnComma.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> setColumnSeparator(',')));

		Button btnSpace = new Button(compositeSeparator, SWT.RADIO);
		btnSpace.setText(DicoBundle.getString("WORD_SPACE"));//$NON-NLS-1$
		btnSpace.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> setColumnSeparator(' ')));

		Button btnTabulation = new Button(compositeSeparator, SWT.RADIO);
		btnTabulation.setText(DicoBundle.getString("WORD_TABULATION"));//$NON-NLS-1$
		btnTabulation.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> setColumnSeparator('\t')));

		Button btnOtherSeparator = new Button(compositeSeparator, SWT.RADIO);
		btnOtherSeparator.setText("Other");
		btnOtherSeparator.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> {
			textOtherSeparator.setEnabled(btnOtherSeparator.getSelection());
			if (btnOtherSeparator.getSelection()) {
				setColumnSeparator('|');
				textOtherSeparator.setFocus();
				textOtherSeparator.selectAll();
			}
		}));

		textOtherSeparator = new Text(composite, SWT.BORDER);
		textOtherSeparator.setEnabled(false);
		textOtherSeparator.setBounds(0, 0, 10, 16);
		GridData gdTextOtherSeparator = new GridData(SWT.LEFT, SWT.BOTTOM, true, false, 1, 1);
		gdTextOtherSeparator.widthHint = 10;
		textOtherSeparator.setLayoutData(gdTextOtherSeparator);

		Label lblElevation = new Label(composite, SWT.NONE);
		lblElevation.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		lblElevation.setText("Height below sea surface : ");

		Composite compositeElevation = new Composite(composite, SWT.NONE);
		GridLayout glCompositeElevation = new GridLayout(1, false);
		glCompositeElevation.marginWidth = 0;
		glCompositeElevation.marginHeight = 0;
		compositeElevation.setLayout(glCompositeElevation);

		btnPositive = new Button(compositeElevation, SWT.RADIO);
		btnPositive.setText("Positive");
		btnNegative = new Button(compositeElevation, SWT.RADIO);
		btnNegative.setText("Negative");
		new Label(composite, SWT.NONE);

		dateBindingContext = initDataBindings();
		if (pageModel.getPositiveElevation().isPresent()) {
			initElevationBindings();
		} else {
			lblElevation.setVisible(false);
			btnPositive.setVisible(false);
			btnNegative.setVisible(false);
		}
		btnSemicolon.setSelection(true);
	}

	protected void setColumnSeparator(char car) {
		pageModel.getColumnSeparator().set(car);
	}

	/**
	 * Model of the option page
	 */
	public static interface SelectCsvExportOptionPageModel {
		/**
		 * Getter of header flag
		 */
		WritableBoolean getWithHeader();

		/**
		 * Getter of ColumnSeparator
		 */
		WritableObject<Character> getColumnSeparator();

		/**
		 * Getter of the elevation style
		 */
		default Optional<WritableBoolean> getPositiveElevation() {
			return Optional.empty();
		}
	}

	/** Initialize a specific binding on CheckBoxGroup (not managed by Windows Builder */
	protected void initElevationBindings() {
		SelectObservableValue<Boolean> separatorObservable = new SelectObservableValue<>(Character.class);
		separatorObservable.addOption(Boolean.TRUE, WidgetProperties.buttonSelection().observe(btnPositive));
		separatorObservable.addOption(Boolean.FALSE, WidgetProperties.buttonSelection().observe(btnNegative));
		dateBindingContext.bindValue(separatorObservable, pageModel.getPositiveElevation().get());
	}

	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		var observeSelectionBtnHeadersObserveWidget = WidgetProperties.buttonSelection().observe(btnHeaders);
		bindingContext.bindValue(observeSelectionBtnHeadersObserveWidget, pageModel.getWithHeader(), null, null);
		//
		var observeTextTextOtherSeparatorObserveWidget = WidgetProperties.text(SWT.Modify).observe(textOtherSeparator);
		bindingContext.bindValue(observeTextTextOtherSeparatorObserveWidget, pageModel.getColumnSeparator(), null,
				null);
		//
		return bindingContext;
	}

}
