package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.composite;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.core.processes.detectionfilter.IAttributeProvider;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.DataParameters;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.data.AllDataComposite;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.widget.AttributeCombo;

public class DataComposite extends Composite {
	private AllDataComposite valueComposite;
	private AttributeCombo avalaibleData;
	private Label lblIs;

	public DataParameters getParameters() {
		return parameters;
	}

	DataParameters parameters;
	private Label lblSet;

	public DataComposite(Composite parent, int style, IAttributeProvider attributeProvider) {

		super(parent, style);
		parameters = new DataParameters(attributeProvider);
		setLayout(new GridLayout(4, false));

		lblSet = new Label(this, SWT.NONE);
		lblSet.setText("set");
		lblSet.setAlignment(SWT.CENTER);
		lblSet.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));

		avalaibleData = new AttributeCombo(this, SWT.READ_ONLY);
		avalaibleData.getCombo().setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));

		lblIs = new Label(this, SWT.NONE);
		lblIs.setAlignment(SWT.CENTER);
		lblIs.setText("to");

		valueComposite = new AllDataComposite(attributeProvider, this, SWT.NONE);
		valueComposite.setLayoutData(new GridData(
				GridData.VERTICAL_ALIGN_CENTER | GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL));
		addListener();

		if (attributeProvider != null) {
			fill();
		}
	}

	public void addListener() {
		avalaibleData.addSelectionChangedListener(event -> {
			PredefinedLayers<?> selection = (PredefinedLayers<?>) ((IStructuredSelection) event.getSelection())
					.getFirstElement();
			parameters.setSelectedAttributeIndex(avalaibleData.getCombo().getSelectionIndex());
			valueComposite.show(selection);
		});
	}

	public void fill() {
		avalaibleData.setInput(parameters.getAvailableAttributes());
		avalaibleData.setSelection(new StructuredSelection(parameters.getAvailableAttributes().get(0)));
	}

	public void updateControlButton(boolean addEnabled, boolean minusEnabled) {

	}

	/**
	 * retrieve values and update parameters
	 */
	public void updateParameters() {
		parameters.setSelectedAttributeIndex(avalaibleData.getCombo().getSelectionIndex());
		valueComposite.getTopDataComposite().updateParameters(parameters);
	}

	/**
	 * @param attributeProvider the {@link #attributeProvider} to set
	 */
	public void setAttributeProvider(IAttributeProvider attributeProvider) {
		parameters.setAttributeProvider(attributeProvider);
		valueComposite.setAttributeProvider(attributeProvider);
		fill();
	}

}
