/**
 * Globe - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.convert.upgradexsf;

import java.util.Set;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

/**
 * Handler class to upgrade a XSF files
 */
public class UpgradeXsfHandler extends AbstractOpenProcessWizardHandler<UpgradeXsfProcessWithWizard> {

	@Override
	protected Set<ContentType> getAcceptedContentType() {
		return Set.of(ContentType.XSF_NETCDF_4);
	}

	@Override
	protected UpgradeXsfProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		UpgradeXsfProcessWithWizard process = new UpgradeXsfProcessWithWizard(shell);
		return process;
	}

}