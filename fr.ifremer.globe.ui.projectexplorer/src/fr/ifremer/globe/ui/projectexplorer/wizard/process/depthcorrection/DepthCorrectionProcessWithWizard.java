package fr.ifremer.globe.ui.projectexplorer.wizard.process.depthcorrection;

import java.util.List;
import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.processes.depthcorrection.DepthCorrection;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.ui.wizard.FilesToProcessEntry;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Depth correction process with wizard
 */
public class DepthCorrectionProcessWithWizard extends DefaultProcessWithWizard<DepthCorrectionWizardModel> {

	/**
	 * Constructor
	 */
	public DepthCorrectionProcessWithWizard(Shell shell) {
		super(shell, "Tide/Draught correction", DepthCorrectionWizardModel::new, DepthCorrectionWizard::new);
	}

	/**
	 * Process method executed after a click on finish.
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		// Get file list (input/output/temp file tuples)
		List<FilesToProcessEntry> filesToProcess = getFilesToProcessWithTemp(logger);

		IDataContainerInfoService infoService = IDataContainerInfoService.grab();

		// Apply depth correction
		String s = filesToProcess.size() > 1 ? "s" : "";
		logger.info("Start processing depth correction of {} file{}...", filesToProcess.size(), s);
		DepthCorrection depthCorrection = new DepthCorrection(model.getDepthCorrectionParameters(),
				model.getTimeIntervalParameters(), logger);
		depthCorrection.setTideType(model.getTideType());

		monitor.setTaskName("Depth correction");
		monitor.beginTask("Depth correction running ...", filesToProcess.size());

		int processedFileCount = 0;
		int errorFileCount = 0;
		for (FilesToProcessEntry entry : filesToProcess) {
			monitor.setTaskName("Processing: " + entry.getInputFile().getPath());
			logger.info("Processing:  {}", entry.getInputFile().getPath());
			Optional<ISounderNcInfo> info = infoService.getSounderNcInfo(entry.getTempFile().getAbsolutePath());
			if (info.isPresent()) {
				try {
					// Process file
					depthCorrection.processFile(info.get(), logger);
					// write output file
					writeOutputFileFromTemp(entry);
					processedFileCount++;
				} catch (Exception e) {
					logger.error("Error while processing file {}: {}", entry.getInputFile().getPath(), e.getMessage(),
							e);
					errorFileCount++;
				}
			}
			if (monitor.isCanceled())
				return Status.CANCEL_STATUS;
			monitor.worked(1);
		}
		// load output files if asked
		loadOutputFiles(filesToProcess, monitor);
		logger.info("End of process: Depth correction applied on {} file{}.", processedFileCount, s);
		if (errorFileCount > 0) {
			String sError = errorFileCount > 1 ? "s" : "";
			logger.error("Error while processing {} file{}", errorFileCount, sError);
		}
		return monitor.isCanceled() ? Status.CANCEL_STATUS : Status.OK_STATUS;

	}

}
