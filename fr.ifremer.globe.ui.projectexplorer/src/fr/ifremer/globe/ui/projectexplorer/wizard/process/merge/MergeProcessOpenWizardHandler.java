package fr.ifremer.globe.ui.projectexplorer.wizard.process.merge;

import java.util.List;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

/**
 * Handler class to open the process wizard (referenced in fragment.e4xmi).
 */
public class MergeProcessOpenWizardHandler extends AbstractOpenProcessWizardHandler<MergeProcessWithWizard> {

	/**
	 * The accepted selected is EITHER a list of MBG or a list of NVI.
	 */
	@Override
	protected List<FileInfoNode> getSuitableFileInfoNode(Object[] selection) {
		// Get only MBG first
		List<FileInfoNode> result = getSelectionAsList(selection, ContentType.MBG_NETCDF_3::equals);
		// Return all MBG if present or all NVI if any
		return !result.isEmpty() ? result : getSelectionAsList(selection, ContentType.NVI_NETCDF_4::equals);
	}

	@Override
	protected MergeProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new MergeProcessWithWizard(shell);
	}

}
