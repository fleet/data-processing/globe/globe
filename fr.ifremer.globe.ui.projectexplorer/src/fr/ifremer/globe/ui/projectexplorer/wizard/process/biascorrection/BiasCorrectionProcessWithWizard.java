package fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection;

import java.util.List;
import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.processes.biascorrection.BiasCorrection;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.ui.wizard.FilesToProcessEntry;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Process and wizard for bias correction
 */
public class BiasCorrectionProcessWithWizard extends DefaultProcessWithWizard<BiasCorrectionWizardModel> {

	public BiasCorrectionProcessWithWizard(Shell shell) {
		super(shell, "Bias Correction", BiasCorrectionWizardModel::new, BiasCorrectionWizard::new);
	}

	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {

		// Get file list (input/output/temp file tuples)
		List<FilesToProcessEntry> filesToProcess = getFilesToProcessWithTemp(logger);
		IDataContainerInfoService infoService = IDataContainerInfoService.grab();

		// Apply bias correction
		String s = filesToProcess.size() > 1 ? "s" : "";
		logger.info("Start processing of {} file{}...", filesToProcess.size(), s);
		BiasCorrection biasCorrection = new BiasCorrection(model.getBiasCorrectionParameters(),
				model.getTimeIntervalParameters());

		// load all needed configuration files
		CorrectionList correctionPoints = biasCorrection.loadConfiguration(monitor, logger, filesToProcess.size());
		if (correctionPoints == null)
			throw new GIOException("No correction point to apply.");
		logger.info("Correction type : {}", correctionPoints.getType());
		logger.info("Correction points : {}", correctionPoints.getSize());

		int processedFileCount = 0;
		int errorFileCount = 0;
		for (FilesToProcessEntry entry : filesToProcess) {
			monitor.setTaskName("Processing: " + entry.getInputFile().getPath());
			logger.info("Processing:  {}", entry.getInputFile().getPath());
			Optional<ISounderNcInfo> info = infoService.getSounderNcInfo(entry.getTempFile().getAbsolutePath());
			if (info.isPresent()) {
				try {
					// Process file
					biasCorrection.processOnFile(monitor, info.get(), correctionPoints,
							model.getBiasCorrectionParameters().getAntennaIndex());
					// write and load output files if asked
					writeOutputFileFromTemp(entry);
					processedFileCount++;
				} catch (Exception e) {
					logger.error("Error while processing file {}: {}", entry.getInputFile().getPath(), e.getMessage(),
							e);
					errorFileCount++;
				}
			}
			if (monitor.isCanceled())
				return Status.CANCEL_STATUS;
		}

		// load output files if asked
		loadOutputFiles(filesToProcess, monitor);

		logger.info("End of process: on {} file{}.", processedFileCount, s);
		if (errorFileCount > 0) {
			String sError = errorFileCount > 1 ? "s" : "";
			logger.error("Error while processing {} file{}", errorFileCount, sError);
		}
		return monitor.isCanceled() ? Status.CANCEL_STATUS : Status.OK_STATUS;
	}

}
