package fr.ifremer.globe.ui.projectexplorer.wizard.tide;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.core.databinding.observable.list.WritableList;

import fr.ifremer.globe.core.io.pointcloud.PointCloudFileInfo;
import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.impl.SimpleChartSeriesBuilder;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.file.pointcloud.IPoint;
import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;
import fr.ifremer.globe.core.model.navigation.CompositeNavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.NavigationPoint;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.model.navigation.utils.NavigationInterpolator;
import fr.ifremer.globe.core.model.tide.ITideData;
import fr.ifremer.globe.core.model.tide.ITideService;
import fr.ifremer.globe.core.model.tide.TideGauge;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableDouble;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.IWWPointCloudLayer;
import fr.ifremer.globe.ui.wizard.DefaultSelectOutputParametersPageModel;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage.SelectInputFilesPageModel;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.PointPlacemark;

/**
 * Default model for convert wizard : contains "input files" and "output parameters" page models.
 */
public class ComputeTideWizardModel extends DefaultSelectOutputParametersPageModel
		implements SelectInputFilesPageModel {

	/** Services **/
	private IGeographicViewService geographicViewService = IGeographicViewService.grab();
	private ITideService tideService = ITideService.grab();
	private IWWLayerFactory wWLayerFactory = IWWLayerFactory.grab();

	/** Properties */
	protected final WritableString tideDataTitle = new WritableString("");
	protected final WritableBoolean isLocalisationFromInputFiles = new WritableBoolean();
	protected final WritableDouble longitude = new WritableDouble(null);
	protected final WritableDouble latitude = new WritableDouble(null);
	private Instant end = Instant.now();
	private Instant start = end.minusSeconds(3600);
	private Optional<Integer> intervalInMinutes = Optional.empty();
	private final WritableDouble zRef = new WritableDouble(0.0);

	/** Tide data **/
	private List<ITideData> tideData = Collections.emptyList();
	private final WritableList<TideGauge> tideGauges = new WritableList<>();

	private List<IChartSeries<? extends IChartData>> otherSeries = new ArrayList<>();

	/** Object displayed in geographic view **/
	private IWWPointCloudLayer pointCloudLayer;
	private final List<PointPlacemark> tideGaugePlacemarks = new ArrayList<>();

	/**
	 * Constructor
	 */
	public ComputeTideWizardModel() {
		getOutputFormatExtensions().add(".ttb");
		// init localisation with current geographic view position
		var viewPosition = IGeographicViewService.grab().getWwd().getView().getEyePosition();
		longitude.set(viewPosition.longitude.degrees);
		latitude.set(viewPosition.latitude.degrees);
		ContextInitializer.setInContext("ComputeTideWizardModel", this);
		displayTideGauges();
	}

	public void dispose() {
		removeTideDataPointCloudLayer();
		tideGaugePlacemarks.forEach(geographicViewService::removePlacemark);
	}

	@Override
	public List<Pair<String, String>> getInputFilesFilterExtensions() {
		return IFileService.grab().getFileFilters(ContentType.getNavigationContentTypes());
	}

	/**
	 * Input files is optional
	 */
	@Override
	public boolean isMandatory() {
		return false;
	}

	/**
	 * Forces 1 output file.
	 */
	@Override
	public Optional<WritableBoolean> getMergeOutputFiles() {
		return Optional.of(new WritableBoolean(true));
	}

	public WritableString getTideDataTitle() {
		return tideDataTitle;
	}

	public List<ITideData> getTideData() {
		return tideData;
	}

	public void setTideData(List<ITideData> tideData) {
		this.tideData = tideData;
		displayTideDataAsPointCloud();
	}

	public List<IChartSeries<? extends IChartData>> getOtherDataToDisplay() {
		return otherSeries;
	}

	public void addDataToDisplay(String title, List<ITideData> data, GColor color) {
		List<double[]> seriesData = data.stream()
				.map(obs -> new double[] { obs.getTimestamp().toEpochMilli(), obs.getValue() }) //
				.toList();
		otherSeries.add(new SimpleChartSeriesBuilder(title).withColor(color).withData(seriesData)
				.withTooltipFormatter(ComputeTideWizardPage.TOOLTIP_FORMATTER).build());
	}

	public void setSingleOutputFilename(String singleOutputFilename) {
		getSingleOutputFilename().set(singleOutputFilename);
	}

	public WritableDouble getLongitude() {
		return longitude;
	}

	public WritableDouble getLatitude() {
		return latitude;
	}

	public WritableDouble getZRef() {
		return zRef;
	}

	public Instant getStart() {
		return start;
	}

	public void setStart(Instant start) {
		this.start = start;
	}

	public Instant getEnd() {
		return end;
	}

	public void setEnd(Instant end) {
		this.end = end;
	}

	/**
	 * @return an interval in seconds (with a minimum of 1 minute).
	 */
	public int getIntervalInSecond() {
		return Math.max(intervalInMinutes.orElse(1), 1) * 60;
	}

	public Optional<Integer> getIntervalInMinutes() {
		return intervalInMinutes;
	}

	public void setIntervalInMinutes(Optional<Integer> intervalInMinutes) {
		this.intervalInMinutes = intervalInMinutes;
	}

	public List<NavigationPoint> getLocalisation() throws GIOException, IOException {
		List<NavigationPoint> navPoints = new ArrayList<>();
		if (isLocalisationFromInputFiles.get())
			// compute navigation points from input file
			navPoints = getInputNavigationPoints();
		else {
			// compute navigation points with custom longitude / latitude
			Instant instant = Instant.ofEpochMilli(start.toEpochMilli());
			while (instant.isBefore(end)) {
				navPoints.add(new NavigationPoint(longitude.get(), latitude.get(), instant));
				instant = instant.plusSeconds(getIntervalInSecond());
			}
			// add one point after
			navPoints.add(new NavigationPoint(longitude.get(), latitude.get(), instant));
		}
		return navPoints;
	}

	/**
	 * Computes navigation points into the time interval. (interpolation for input navigation file positions)
	 */
	public List<NavigationPoint> getInputNavigationPoints() throws GIOException, IOException {
		// get navigation from input files
		List<INavigationDataSupplier> navigationDataSuppliers = getInputFiles().stream()//
				.map(file -> INavigationService.grab().getNavigationDataProvider(file.getAbsolutePath()))
				.filter(Optional::isPresent).map(Optional::get)//
				.collect(Collectors.toList());

		List<NavigationPoint> navPoints = new ArrayList<>();
		try (INavigationData navData = CompositeNavigationData.build(navigationDataSuppliers, true)) {
			// compute navigation in time interval by interpolation
			var navInterpolator = new NavigationInterpolator(navData);
			var instant = Instant.ofEpochMilli(start.toEpochMilli());
			while (instant.isBefore(end)) {
				var longitude = navInterpolator.getLongitude(instant);
				var latitude = navInterpolator.getLatitude(instant);
				navPoints.add(new NavigationPoint(longitude, latitude, instant));
				instant = instant.plusSeconds(getIntervalInSecond());
			}
			// add one point after
			var longitude = navInterpolator.getLongitude(end.toEpochMilli());
			var latitude = navInterpolator.getLatitude(end.toEpochMilli());
			navPoints.add(new NavigationPoint(longitude, latitude, end));
		}

		return navPoints;
	}

	/**
	 * @return center position from input files.
	 */
	public LatLon getCenterPosition() {
		if (!isLocalisationFromInputFiles.get())
			return LatLon.fromDegrees(latitude.get(), longitude.get());

		List<IFileInfo> fileInfos = IFileService.grab()
				.getFileInfos(getInputFiles().stream().map(File::getAbsolutePath).collect(Collectors.toList()));

		// sort tide gauge from distance to input files center
		List<LatLon> positionList = fileInfos.stream().map(fileInfo -> Optional.ofNullable(fileInfo.getGeoBox()))
				.filter(Optional::isPresent).map(Optional::get)//
				.map(gb -> LatLon.fromDegrees(gb.getCenter().getLat(), gb.getCenter().getLong()))
				.collect(Collectors.toList());
		return LatLon.getCenter(positionList);
	}

	/**
	 * Removes {@link IWWPointCloudLayer} used to display {@link ITideData} if exists.
	 */
	private void removeTideDataPointCloudLayer() {
		if (pointCloudLayer != null) {
			geographicViewService.removeLayer(pointCloudLayer);
			pointCloudLayer = null;
		}
	}

	/**
	 * Displays {@link ITideData} with a {@link IWWPointCloudLayer}.
	 */
	private void displayTideDataAsPointCloud() {
		removeTideDataPointCloudLayer();

		var points = tideData.stream().filter(data -> data.getLatitude().isPresent() && data.getLongitude().isPresent())
				.map(data -> new IPoint() {

					@Override
					public int getValueCount() {
						return 1;
					}

					@Override
					public double getValue(int index) {
						return data.getValue();
					}

					@Override
					public Optional<Temporal> getTime() {
						return Optional.of(data.getTimestamp());
					}

					@Override
					public double getLongitude() {
						return data.getLongitude().get();
					}

					@Override
					public double getLatitude() {
						return data.getLatitude().get();
					}

					@Override
					public double getElevation() {
						return data.getValue();
					}

					@Override
					public Optional<String> getLabel() {
						return Optional.empty();
					}
				}).collect(Collectors.toList());

		if (!points.isEmpty()) {
			var fieldInfos = PointCloudFieldInfo.gatherFieldInfos(points, i -> "Value");
			// Only one PointCloudFieldInfo is expected in this use case
			if (!fieldInfos.isEmpty()) {
				var fileInfo = new PointCloudFileInfo("", points, fieldInfos);
				pointCloudLayer = wWLayerFactory.createPointCloudLayer(fileInfo, fieldInfos.get(0));
				geographicViewService.addLayer(pointCloudLayer);
			}
		}
	}

	/**
	 * Displays tide gauges in geographic view.
	 */
	private void displayTideGauges() {
		try {
			tideService.getTideGauges()
					.forEach(tideGauge -> tideGaugePlacemarks.add(geographicViewService.addPlacemark(
							Position.fromDegrees(tideGauge.getLatitude(), tideGauge.getLongitude()),
							tideGauge.getName())));
		} catch (IOException e) {
			// do nothing
		}
	}

	/**
	 * @return the {@link #tideGauges}
	 */
	public WritableList<TideGauge> getTideGauges() {
		return tideGauges;
	}

}