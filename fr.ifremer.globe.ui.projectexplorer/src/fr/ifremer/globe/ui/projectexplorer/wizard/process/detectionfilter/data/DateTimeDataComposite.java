/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.data;

import java.util.Date;

import org.eclipse.nebula.widgets.cdatetime.CDT;
import org.eclipse.nebula.widgets.cdatetime.CDateTime;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.processes.detectionfilter.parameters.DataParameters;
import fr.ifremer.globe.utils.date.DateUtils;

/**
 * Composite used to edit a DateTime value to set.
 */
public class DateTimeDataComposite extends ADataComposite {
	protected CDateTime valueA;

	/**
	 * Constructor.
	 */
	public DateTimeDataComposite(Composite parent, int style) {
		super(parent, style);

		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = gridLayout.marginWidth = 0;
		setLayout(gridLayout);

		valueA = new CDateTime(this, CDT.BORDER | CDT.BORDER | CDT.COMPACT | CDT.DROP_DOWN | CDT.DATE_LONG
				| CDT.TIME_SHORT | CDT.CLOCK_12_HOUR);
		valueA.setTimeZone(DateUtils.TIME_ZONE_GMT);
		valueA.setPattern(DateUtils.DATE_TIME_PATTERN);
		valueA.setSelection(new Date());
		GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gridData.widthHint = valueA.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
		valueA.setLayoutData(gridData);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.data.ADataComposite#updateParameters(fr.ifremer.globe.imagery.script.parameter.DataParameters)
	 */
	@Override
	public void updateParameters(DataParameters parameters) {
		Date value = valueA.getSelection();
		parameters.setValue(String.valueOf(value.getTime()));
	}
}
