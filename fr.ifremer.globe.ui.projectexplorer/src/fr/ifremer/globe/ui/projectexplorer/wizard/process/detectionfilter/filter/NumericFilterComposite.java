/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.filter;

import org.eclipse.nebula.widgets.formattedtext.FormattedText;
import org.eclipse.nebula.widgets.formattedtext.NumberFormatter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters.Comparator;

/**
 * Composite used to edit a numeric filtering criteria.
 */
public class NumericFilterComposite extends ADataComposite {
	private FormattedText ValueA;
	private FormattedText ValueB;
	private Label labelB;

	/**
	 * Constructor.
	 */
	public NumericFilterComposite(Composite parent, int style, String editPattern) {
		super(parent, style);

		GridLayout gridLayout = new GridLayout(3, false);
		gridLayout.marginHeight = gridLayout.marginWidth = 0;
		setLayout(gridLayout);
		ValueA = new FormattedText(this, SWT.BORDER);
		ValueA.setFormatter(new NumberFormatter(editPattern));
		GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gridData.widthHint = editPattern.length() * 6;
		ValueA.getControl().setLayoutData(gridData);

		labelB = new Label(this, SWT.NONE);
		labelB.setText("and");
		labelB.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));

		ValueB = new FormattedText(this, SWT.BORDER);
		ValueB.setFormatter(new NumberFormatter(editPattern));
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gridData.widthHint = editPattern.length() * 6;
		ValueB.getControl().setLayoutData(gridData);

	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.filter.ADataComposite#updateParameters(fr.ifremer.globe.imagery.script.parameter.SingleFilterParameters)
	 */
	@Override
	public void updateParameters(SingleFilterParameters parameters) {
		if (ValueA.getControl().isVisible()) {
			parameters.setValueA(ValueA.getFormatter().getValue().toString());
		}
		if (ValueB.getControl().isVisible()) {
			parameters.setValueB(ValueB.getFormatter().getValue().toString());
		}

	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.filter.ADataComposite#setComparator(fr.ifremer.globe.imagery.script.parameter.SingleFilterParameters.Comparator)
	 */
	@Override
	public void setComparator(Comparator comparator) {
		if (comparator != SingleFilterParameters.Comparator.between) {
			ValueB.getControl().setVisible(false);
			labelB.setVisible(false);
		} else {
			ValueB.getControl().setVisible(true);
			labelB.setVisible(true);

		}
	}
}
