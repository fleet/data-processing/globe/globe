package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.handlers;

import jakarta.inject.Inject;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.LayersWalker;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.ConvertToCsvProcessWithWizard;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.ConvertToCsvWizardModel;

/**
 * Handler class to open the depth correction wizard (tide / dynamic draught corrections)
 */
public class ConvertMbgXsfToValidityFlagHandler
		extends AbstractOpenProcessWizardHandler<ConvertToCsvProcessWithWizard> {

	@Inject
	protected LayersWalker layersWalker;

	@Override
	protected ConvertToCsvProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new ConvertToCsvProcessWithWizard(shell, layersWalker, getAcceptedContentType(),
				ConvertToCsvWizardModel.VALIDITY_EXTENSION);
	}

}