package fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog;

import fr.ifremer.globe.core.processes.biascorrection.model.CommonCorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.FileCorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.ICorrectionPointVisitor;

public class PointLabelProvider implements ICorrectionPointVisitor {
	private String label;

	public void visit(FileCorrectionPoint p) {
		setLabel(p.getFileName());
	}

	public void visit(CommonCorrectionPoint p) {
		setLabel("COMMON");
	}

	public String getLabel() {
		return label;
	}

	private void setLabel(String label) {
		this.label = label;
	}
}
