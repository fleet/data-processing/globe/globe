package fr.ifremer.globe.ui.projectexplorer.wizard.tide;

import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.tide.ttb.TtbFileInfoSupplier;
import fr.ifremer.globe.core.model.tide.ITideData;
import fr.ifremer.globe.core.model.tide.ITideService;
import fr.ifremer.globe.core.model.tide.TideGauge;
import fr.ifremer.globe.core.model.tide.TideGaugeSource;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.globe.utils.mbes.Ellipsoid;
import gov.nasa.worldwind.geom.LatLon;

public class TideGaugeComposite extends Composite {

	private static Logger logger = LoggerFactory.getLogger(TideGaugeComposite.class);

	/** Properties **/
	private final ComputeTideWizardModel model;

	/** Inner widgets **/
	private ComboViewer tideGaugeComboViewer;
	private ComboViewer sourceComboViewer;
	private Group grpTideGauge;
	private Group grpSources;
	private Label lblTideGaugeInfo;

	/**
	 * Constructor.
	 */
	public TideGaugeComposite(Composite parent, int style, ComputeTideWizardModel wizardModel) {
		super(parent, style);
		createUI();
		model = wizardModel;
		initialize();
	}

	/**
	 * Builds the UI.
	 */
	public void createUI() {
		setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.marginWidth = 0;
		gridLayout.verticalSpacing = 0;
		setLayout(gridLayout);

		grpTideGauge = new Group(this, SWT.NONE);
		grpTideGauge.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpTideGauge.setLayout(new GridLayout(2, false));
		GridData gdGrpTideGauge = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gdGrpTideGauge.widthHint = 500;
		grpTideGauge.setLayoutData(gdGrpTideGauge);
		grpTideGauge.setText("Tide gauge");

		tideGaugeComboViewer = new ComboViewer(grpTideGauge, SWT.READ_ONLY);
		Combo combo = tideGaugeComboViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		tideGaugeComboViewer.setContentProvider(ArrayContentProvider.getInstance());
		tideGaugeComboViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				@SuppressWarnings("unchecked")
				Pair<TideGauge, Double> selection = (Pair<TideGauge, Double>) element;
				return String.format("%s (%.2f km)", selection.getFirst().getName(), selection.getSecond());
			}
		});
		tideGaugeComboViewer.addSelectionChangedListener(evt -> updateTideGaugeInfo());

		var btnInfo = new Button(grpTideGauge, SWT.NONE);
		btnInfo.setToolTipText("Tide gauge properties");
		btnInfo.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.ui", "icons/16/filefind.png"));
		btnInfo.setText("Info");

		lblTideGaugeInfo = new Label(grpTideGauge, SWT.NONE);
		lblTideGaugeInfo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		lblTideGaugeInfo.setForeground(ColorUtils.convertGColorToSWT(GColor.RED));

		btnInfo.addListener(SWT.Selection, evt -> displayTideGagueInfo());

		grpSources = new Group(this, SWT.NONE);
		grpSources.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpSources.setLayout(new GridLayout(1, false));
		grpSources.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpSources.setText("Sources");

		sourceComboViewer = new ComboViewer(grpSources, SWT.READ_ONLY);
		sourceComboViewer.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		sourceComboViewer.setContentProvider(ArrayContentProvider.getInstance());
		sourceComboViewer.setInput(TideGaugeSource.values());
		sourceComboViewer.setSelection(new StructuredSelection(TideGaugeSource.RAW_HIGHT_FREQUENCY));
	}

	private void updateTideGaugeInfo() {
		lblTideGaugeInfo.setText("");
		getSelectedTideGauge().ifPresent(tideGauge -> {
			try {
				var tideGaugeInfo = ITideService.grab().getTideGaugeInfo(tideGauge.getId());
				var verticalRef = tideGaugeInfo.verticalRef();
				if (verticalRef.name() == null || verticalRef.zeroHydro() == null || verticalRef.zhRef() == null)
					lblTideGaugeInfo.setText("Warning : doubtfull vertical reference (see info for details).");
			} catch (IOException e) {
				Messages.openErrorMessage("Error while getting info about tide gauge : " + tideGauge.getName(), e);
			}
		});
	}

	/**
	 * Initializes parameter with input files.
	 */
	private void initialize() {
		// Listens the TideGauges list in model and waits for its loading to finish
		model.getTideGauges().addChangeListener(changeEvent -> {
			if (!tideGaugeComboViewer.getControl().isDisposed()) {
				sortTideGauges();
				model.isLocalisationFromInputFiles.addChangeListener(e -> sortTideGauges());
				model.latitude.addChangeListener(e -> sortTideGauges());
				model.getInputFiles().addChangeListener(e -> sortTideGauges());
			}
		});
	}

	/**
	 * Computes tide observation with selected parameters.
	 */
	public List<ITideData> compute() throws IOException {
		// get parameters
		Optional<TideGauge> selectedTideGauge = getSelectedTideGauge();
		TideGaugeSource source = (TideGaugeSource) ((StructuredSelection) sourceComboViewer.getSelection())
				.getFirstElement();
		if (selectedTideGauge.isPresent())
			return compute(selectedTideGauge.get(), source);

		return Collections.emptyList();
	}

	/**
	 * Computes tide observation with selected parameters.
	 */
	public List<ITideData> compute(TideGauge tideGauge, TideGaugeSource source) throws IOException {

		// request tide data from service
		var result = ITideService.grab().getObservations(tideGauge.getId(), source, model.getStart(), model.getEnd(),
				model.getIntervalInMinutes()).stream().map(ITideData.class::cast).collect(Collectors.toList());

		// add the tide gauge position to the last data (enables the display of this
		// position in Geographic view)
		if (!result.isEmpty()) {
			var lastData = result.remove(result.size() - 1);
			result.add(ITideData.build(lastData.getTimestamp(), lastData.getValue(), tideGauge.getLongitude(),
					tideGauge.getLatitude()));
		}

		return result;
	}

	/**
	 * Creates a process to compute tide observation with selected parameters.
	 */
	public Optional<ComputeTideProcess> prepareCompute() {
		// get parameters
		Optional<TideGauge> selectedTideGauge = getSelectedTideGauge();
		TideGaugeSource source = (TideGaugeSource) ((StructuredSelection) sourceComboViewer.getSelection())
				.getFirstElement();

		return Optional.of(new ComputeTideProcess() {
			@Override
			List<ITideData> computesTide(IProgressMonitor monitor) throws IOException {
				if (selectedTideGauge.isPresent())
					return compute(selectedTideGauge.get(), source);
				return Collections.emptyList();
			}

			@Override
			void onFinished() {
				selectedTideGauge.ifPresent(tideGauge -> {
					model.setTideData(result);
					var tideGaugeName = tideGauge.getName();
					model.getTideDataTitle()
							.set(String.format("%s from %s to %s %s", tideGaugeName, model.getStart(), model.getEnd(),
									model.getIntervalInMinutes().map(i -> "(interval : " + i + ")").orElse("")));

					// update output filename
					Function<Instant, String> formatDate = instant -> instant.toString().split("\\.")[0].replace(":",
							"-");
					model.setSingleOutputFilename(String.format("%s_%s_%s.%s", tideGaugeName.split("/")[0],
							formatDate.apply(model.getStart()), formatDate.apply(model.getEnd()),
							TtbFileInfoSupplier.EXTENSION_TTB));
				});
			}
		});
	}

	@SuppressWarnings("unchecked")
	public Optional<TideGauge> getSelectedTideGauge() {
		return Optional.ofNullable(
				(Pair<TideGauge, Double>) ((StructuredSelection) tideGaugeComboViewer.getSelection()).getFirstElement())
				.map(Pair::getFirst);
	}

	public TideGaugeSource getSelectedTideGaugeSource() {
		return (TideGaugeSource) ((StructuredSelection) sourceComboViewer.getSelection()).getFirstElement();
	}

	/**
	 * Sorts tide gauges by distance from current localisation.
	 */
	private void sortTideGauges() {
		logger.debug("Sorting the list of tide gauge");
		LatLon center = model.getCenterPosition();
		List<Pair<TideGauge, Double>> tideGaugeAndDistances = model.getTideGauges().stream().sorted((tg1, tg2) -> {
			LatLon tideGaugePos1 = LatLon.fromDegrees(tg1.getLatitude(), tg1.getLongitude());
			LatLon tideGaugePos2 = LatLon.fromDegrees(tg2.getLatitude(), tg2.getLongitude());
			return LatLon.linearDistance(tideGaugePos1, center).getDegrees() > LatLon
					.linearDistance(tideGaugePos2, center).getDegrees() ? 1 : -1;
		}).map(tideGauge -> {
			LatLon tideGaugePos = LatLon.fromDegrees(tideGauge.getLatitude(), tideGauge.getLongitude());
			return new Pair<TideGauge, Double>(tideGauge, LatLon.ellipsoidalDistance(tideGaugePos, center,
					Ellipsoid.WGS84.getHalf_A(), Ellipsoid.WGS84.getHalf_B()) / 1000d);
		}).toList();
		tideGaugeComboViewer.setInput(tideGaugeAndDistances);
		tideGaugeComboViewer.setSelection(new StructuredSelection(tideGaugeAndDistances.get(0)));
	}

	private void displayTideGagueInfo() {
		getSelectedTideGauge().ifPresent(tideGauge -> {
			try {
				new TideGaugeInfoDialog(getShell(), ITideService.grab().getTideGaugeInfo(tideGauge.getId())).open();
			} catch (IOException e) {
				Messages.openErrorMessage("Error while getting info about tide gauge : " + tideGauge.getName(), e);
			}
		});
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if (enabled) {
			grpTideGauge.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
			grpSources.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		} else {
			grpTideGauge.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NONE));
			grpSources.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NONE));
		}
	}

}