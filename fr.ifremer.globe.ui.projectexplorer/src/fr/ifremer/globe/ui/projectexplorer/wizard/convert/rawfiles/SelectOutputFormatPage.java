package fr.ifremer.globe.ui.projectexplorer.wizard.convert.rawfiles;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.SelectObservableValue;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.api.xsf.converter.XsfConverterParameters;
import fr.ifremer.globe.core.io.nvi.service.INviWriter.SamplingMode;
import fr.ifremer.globe.ui.databinding.conversion.EqualsObjectToBooleanConverter;
import fr.ifremer.globe.ui.databinding.property.CheckBoxGroupActivateProperty;
import fr.ifremer.globe.ui.databinding.validation.FileValidator;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.ui.widget.CheckBoxGroup;
import fr.ifremer.globe.ui.widget.FileComposite;

/**
 * Wizard page used to define the output format in a sounder files convertion.
 */
public class SelectOutputFormatPage extends WizardPage {
	/** Model in MVC */
	protected ConvertRawFilesWizardModel selectOutputFormatPageModel;

	/** Widgets */
	protected CheckBoxGroup xsfComposite;
	protected Text xsfTitleText;
	protected Text xsfSummaryText;
	protected Text xsfKeywordsText;
	protected Text xsfLicenseText;

	protected CheckBoxGroup mbgComposite;
	protected Text mbgShipNameText;
	protected Text mbgSurveyNameText;
	protected Text mbgReferencePointText;
	protected Text mbgCdiText;

	protected CheckBoxGroup nviComposite;
	protected Spinner soundingsSpinner;
	protected Spinner timeSpinner;
	protected Button noSamplingButton;
	protected Button timeSamplingButton;
	protected Button soundingsSamplingButton;

	/** View/model bindings */
	protected DataBindingContext bindingContext = new DataBindingContext();;
	protected Binding soundingsSpinnerBinding;
	protected Binding timeSpinnerBinding;
	private Text xsfRightsText;
	private FileComposite xsfReferenceFolderComposite;
	private Label lblsondertypeIsUsed;
	private Button xsfIgnoreWC;

	/**
	 * Constructor
	 */
	public SelectOutputFormatPage(ConvertRawFilesWizardModel selectOutputFormatPageModel) {
		super(SelectOutputFormatPage.class.getName(), DicoBundle.getString("SELECTOUTPUTFORMATPAGE_TITLE"), null);
		this.selectOutputFormatPageModel = selectOutputFormatPageModel;
		updatePageComplete();
		setDescription("Note: geodesic system must be WGS-84.");
	}

	/**
	 * Creates the design of the input files selection page
	 */
	@Override
	public void createControl(Composite parent) {
		Composite container_1 = new Composite(parent, SWT.NONE);
		GridLayout gl_container_1 = new GridLayout(1, false);
		gl_container_1.marginHeight = 0;
		container_1.setLayout(gl_container_1);
		setControl(container_1);

		xsfComposite = new CheckBoxGroup(container_1, SWT.NONE);
		xsfComposite.setText("Xsf");
		GridData gd_xsfComposite = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_xsfComposite.horizontalIndent = 10;
		xsfComposite.setLayoutData(gd_xsfComposite);
		xsfComposite.setLayout(new GridLayout(2, false));
		Composite container = xsfComposite.getContent();

		// XSF title
		Label xsfTitleLabel = new Label(container, SWT.NONE);
		xsfTitleLabel.setText("Title");
		xsfTitleLabel.setToolTipText(XsfConverterParameters.TITLE_DESC);
		xsfTitleText = new Text(container, SWT.BORDER);
		xsfTitleText.setText("");
		xsfTitleText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		xsfTitleText.setToolTipText(XsfConverterParameters.TITLE_DESC);

		// XSF summary
		Label xsfSummaryLabel = new Label(container, SWT.NONE);
		xsfSummaryLabel.setText("Summary");
		xsfSummaryLabel.setToolTipText(XsfConverterParameters.SUMMARY_DESC);
		xsfSummaryText = new Text(container, SWT.BORDER);
		xsfSummaryText.setText("");
		xsfSummaryText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		xsfSummaryText.setToolTipText(XsfConverterParameters.SUMMARY_DESC);

		// XSF keywords
		Label xsfKeywordsLabel = new Label(container, SWT.NONE);
		xsfKeywordsLabel.setText("Keywords");
		xsfKeywordsLabel.setToolTipText(XsfConverterParameters.KEYWORDS_DESC);
		xsfKeywordsText = new Text(container, SWT.BORDER);
		xsfKeywordsText.setText("");
		xsfKeywordsText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		xsfKeywordsText.setToolTipText(XsfConverterParameters.KEYWORDS_DESC);

		// XSF license
		Label xsfLicenseLabel = new Label(container, SWT.NONE);
		xsfLicenseLabel.setText("License");
		xsfLicenseLabel.setToolTipText(XsfConverterParameters.LICENSE_DESC);
		xsfLicenseText = new Text(container, SWT.BORDER);
		xsfLicenseText.setText("");
		xsfLicenseText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		xsfLicenseText.setToolTipText(XsfConverterParameters.LICENSE_DESC);

		// XSF rights
		Label lblRights = new Label(xsfComposite.getContent(), SWT.NONE);
		lblRights.setText("Rights");
		lblRights.setToolTipText(XsfConverterParameters.RIGHTS_DESC);
		xsfRightsText = new Text(xsfComposite.getContent(), SWT.BORDER);
		xsfRightsText.setText("");
		xsfRightsText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		xsfRightsText.setToolTipText(XsfConverterParameters.RIGHTS_DESC);

		// reference folder
		Label lblRefFolder = new Label(xsfComposite.getContent(), SWT.NONE);
		lblRefFolder.setText("Reference");
		lblRefFolder.setToolTipText(XsfConverterParameters.REFERENCE_FOLDER_DESC);
		xsfReferenceFolderComposite = new FileComposite(xsfComposite.getContent(), SWT.NONE,
				selectOutputFormatPageModel.getXsfReferenceFolder(),
				FileValidator.FOLDER | FileValidator.NULL | FileValidator.EXISTS, bindingContext);
		xsfReferenceFolderComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		xsfReferenceFolderComposite.setToolTipText(XsfConverterParameters.REFERENCE_FOLDER_DESC);

		lblsondertypeIsUsed = new Label(xsfComposite.getContent(), SWT.NONE);
		lblsondertypeIsUsed.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lblsondertypeIsUsed.setLocation(0, 0);
		lblsondertypeIsUsed.setText("%SONDER_TYPE% expression will be replaced by value retrieved from input file(s).");
		lblsondertypeIsUsed.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

		xsfIgnoreWC = new Button(xsfComposite.getContent(), SWT.CHECK);
		xsfIgnoreWC.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		xsfIgnoreWC.setText("Ignore WaterColumn datagrams and files"); //$NON-NLS-1$

		mbgComposite = new CheckBoxGroup(container_1, SWT.NONE);
		mbgComposite.setText("Mbg");
		GridData gd_mbgComposite = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_mbgComposite.horizontalIndent = 10;
		mbgComposite.setLayoutData(gd_mbgComposite);
		mbgComposite.setLayout(new GridLayout(2, false));
		container = mbgComposite.getContent();

		Label mbgShipNameLabel = new Label(container, SWT.NONE);
		mbgShipNameLabel.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_SHIP_NAME"));

		mbgShipNameText = new Text(container, SWT.BORDER);
		mbgShipNameText.setText("");
		mbgShipNameText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		Label mbgSurveyNameLabel = new Label(container, SWT.NONE);
		mbgSurveyNameLabel.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_SURVEY_NAME"));

		mbgSurveyNameText = new Text(container, SWT.BORDER);
		mbgSurveyNameText.setText("");
		mbgSurveyNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label mbgReferencePointLabel = new Label(container, SWT.NONE);
		mbgReferencePointLabel.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_REFERENCE_POINT_DESCRIPTION"));

		mbgReferencePointText = new Text(container, SWT.BORDER);
		mbgReferencePointText.setText("");
		mbgReferencePointText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label mbgCdiILabel = new Label(container, SWT.NONE);
		mbgCdiILabel.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_CDI"));

		mbgCdiText = new Text(container, SWT.BORDER);
		mbgCdiText.setText("");
		mbgCdiText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		nviComposite = new CheckBoxGroup(container_1, SWT.NONE);
		nviComposite.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_NVI_NAVIGATION_FILE"));
		GridData gd_nviComposite = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_nviComposite.horizontalIndent = 10;
		nviComposite.setLayoutData(gd_nviComposite);
		nviComposite.setLayout(new FillLayout());

		Group nviGroup = new Group(nviComposite.getContent(), SWT.NONE);
		nviGroup.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_NVI_SAMPLING_GROUP")); //$NON-NLS-1$
		GridLayout gl_nviGroup = new GridLayout(3, false);
		nviGroup.setLayout(gl_nviGroup);

		noSamplingButton = new Button(nviGroup, SWT.RADIO);
		noSamplingButton.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_NO_SAMPLING_BUTTON")); //$NON-NLS-1$
		GridData gd_noSamplingButton = new GridData(SWT.LEFT, SWT.CENTER, true, false, 3, 1);
		gd_noSamplingButton.horizontalIndent = 15;
		noSamplingButton.setLayoutData(gd_noSamplingButton);

		timeSamplingButton = new Button(nviGroup, SWT.RADIO);
		GridData gd_soundingsSamplingButton;
		GridData gd_timeSamplingButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_timeSamplingButton.horizontalIndent = 15;
		timeSamplingButton.setLayoutData(gd_timeSamplingButton);
		timeSamplingButton.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_TIME_SAMPLING_BUTTON"));

		timeSpinner = new Spinner(nviGroup, SWT.BORDER);
		timeSpinner.setEnabled(false);
		timeSpinner.setMinimum(1);
		timeSpinner.setMaximum(100000);

		Label lblNewLabel_1 = new Label(nviGroup, SWT.NONE);
		lblNewLabel_1.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_TIME_SAMPLING_LABEL")); //$NON-NLS-1$

		soundingsSamplingButton = new Button(nviGroup, SWT.RADIO);
		gd_soundingsSamplingButton = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_soundingsSamplingButton.horizontalIndent = 15;
		soundingsSamplingButton.setLayoutData(gd_soundingsSamplingButton);
		soundingsSamplingButton.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_SOUNDINGS_SAMPLING_BUTTON"));

		soundingsSpinner = new Spinner(nviGroup, SWT.BORDER);
		soundingsSpinner.setEnabled(false);
		soundingsSpinner.setMinimum(1);
		soundingsSpinner.setMaximum(100000);

		Label lblNewLabel_2 = new Label(nviGroup, SWT.NONE);
		lblNewLabel_2.setText(DicoBundle.getString("SELECTOUTPUTFORMATPAGE_SOUNDINGS_SAMPLING_LABEL")); //$NON-NLS-1$

		initDataBindings();
		initCheckBoxGroupBindings();

		// force view updating
		timeSpinnerBinding.updateModelToTarget();
		soundingsSpinnerBinding.updateModelToTarget();

	}

	/** Sets whether this page is complete */
	public void updatePageComplete() {
		setPageComplete(selectOutputFormatPageModel.getXsfFormat().isTrue()
				|| selectOutputFormatPageModel.getMbgFormat().isTrue()
				|| selectOutputFormatPageModel.getNviFormat().isTrue());
	}

	/**
	 * UpdateValueStrategy to manage the state PageComplete when something change in the viow
	 */
	public static class UpdateStrategy extends UpdateValueStrategy<Boolean, Boolean> {

		protected SelectOutputFormatPage selectOutputFormatPage;

		/**
		 * Constructor
		 */
		public UpdateStrategy(SelectOutputFormatPage selectOutputFormatPage) {
			this.selectOutputFormatPage = selectOutputFormatPage;
		}

		@Override
		protected IStatus doSet(IObservableValue<? super Boolean> observableValue, Boolean value) {
			IStatus result = super.doSet(observableValue, value);
			selectOutputFormatPage.updatePageComplete();
			return result;
		}

		/**
		 * @see org.eclipse.core.databinding.UpdateValueStrategy#doSet(org.eclipse.core.databinding.observable.value.IObservableValue,
		 *      java.lang.Object)
		 */
	}

	/** Initialize a specific binding on CheckBoxGroup (not managed by Windows Builder */
	protected void initCheckBoxGroupBindings() {
		// CheckBox XSF
		var observeEnabledXsfCompositeObserveWidget = new CheckBoxGroupActivateProperty().observe(xsfComposite);
		bindingContext.bindValue(observeEnabledXsfCompositeObserveWidget, selectOutputFormatPageModel.getXsfFormat(),
				new UpdateStrategy(this), null);

		var observeSelectionBtnOverwriteExistingFilesObserveWidget = WidgetProperties.buttonSelection()
				.observe(xsfIgnoreWC);
		bindingContext.bindValue(observeSelectionBtnOverwriteExistingFilesObserveWidget,
				selectOutputFormatPageModel.getXSFIgnoreWC(), null, null);

		// CheckBox MBG
		var observeEnabledMbgCompositeObserveWidget = new CheckBoxGroupActivateProperty().observe(mbgComposite);
		bindingContext.bindValue(observeEnabledMbgCompositeObserveWidget, selectOutputFormatPageModel.getMbgFormat(),
				new UpdateStrategy(this), null);

		// CheckBox NVI
		var observeEnabledNviCompositeObserveWidget = new CheckBoxGroupActivateProperty().observe(nviComposite);
		bindingContext.bindValue(observeEnabledNviCompositeObserveWidget, selectOutputFormatPageModel.getNviFormat(),
				new UpdateStrategy(this), null);

		// Bind SamplingMode on radio buttons
		var observeSelectionNoSamplingButton = WidgetProperties.buttonSelection().observe(noSamplingButton);
		var observeSelectionTimeSamplingButton = WidgetProperties.buttonSelection().observe(timeSamplingButton);
		var observeSelectionSoundingsSamplingButton = WidgetProperties.buttonSelection()
				.observe(soundingsSamplingButton);
		var featureRepoPolicyObservable = new SelectObservableValue<SamplingMode>();
		featureRepoPolicyObservable.addOption(SamplingMode.NONE, observeSelectionNoSamplingButton);
		featureRepoPolicyObservable.addOption(SamplingMode.TIME, observeSelectionTimeSamplingButton);
		featureRepoPolicyObservable.addOption(SamplingMode.SOUNDINGS, observeSelectionSoundingsSamplingButton);
		bindingContext.bindValue(featureRepoPolicyObservable, selectOutputFormatPageModel.getNviSamplingMode());
	}

	/**
	 * @see org.eclipse.jface.dialogs.DialogPage#setVisible(boolean)
	 */
	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			updatePageComplete();
		}
		super.setVisible(visible);
	}

	protected void initDataBindings() {
		//
		var observeTextShipNameTextObserveWidget = WidgetProperties.text(SWT.Modify).observe(mbgShipNameText);
		bindingContext.bindValue(observeTextShipNameTextObserveWidget, selectOutputFormatPageModel.getMbgShipName(),
				null, null);
		//
		var observeTextSurveyNameTextObserveWidget = WidgetProperties.text(SWT.Modify).observe(mbgSurveyNameText);
		bindingContext.bindValue(observeTextSurveyNameTextObserveWidget, selectOutputFormatPageModel.getMbgSurveyName(),
				null, null);
		//
		var observeTextReferencePointTextObserveWidget = WidgetProperties.text(SWT.Modify)
				.observe(mbgReferencePointText);
		bindingContext.bindValue(observeTextReferencePointTextObserveWidget,
				selectOutputFormatPageModel.getMbgReferencePoint(), null, null);
		//
		var observeTextCDITextObserveWidget = WidgetProperties.text(SWT.Modify).observe(mbgCdiText);
		bindingContext.bindValue(observeTextCDITextObserveWidget, selectOutputFormatPageModel.getMbgCdi(), null, null);
		//
		var observeSelectionTimeSpinnerObserveWidget = WidgetProperties.spinnerSelection().observe(timeSpinner);
		bindingContext.bindValue(observeSelectionTimeSpinnerObserveWidget,
				selectOutputFormatPageModel.getNviTimeSamplingValue(), null, null);
		//
		var observeSelectionSoundingsSpinnerObserveWidget = WidgetProperties.spinnerSelection()
				.observe(soundingsSpinner);
		bindingContext.bindValue(observeSelectionSoundingsSpinnerObserveWidget,
				selectOutputFormatPageModel.getNviSoundingsSamplingValue(), null, null);
		//
		var observeEnabledTimeSpinnerObserveWidget = WidgetProperties.enabled().observe(timeSpinner);
		var strategy = new UpdateValueStrategy<SamplingMode, Boolean>();
		strategy.setConverter(new EqualsObjectToBooleanConverter<>(SamplingMode.TIME));
		timeSpinnerBinding = bindingContext.bindValue(observeEnabledTimeSpinnerObserveWidget,
				selectOutputFormatPageModel.getNviSamplingMode(),
				new UpdateValueStrategy<>(UpdateValueStrategy.POLICY_NEVER), strategy);
		//
		var observeEnabledSoundingsSpinnerObserveWidget = WidgetProperties.enabled().observe(soundingsSpinner);
		var strategy_1 = new UpdateValueStrategy<SamplingMode, Boolean>();
		strategy_1.setConverter(new EqualsObjectToBooleanConverter<>(SamplingMode.SOUNDINGS));
		soundingsSpinnerBinding = bindingContext.bindValue(observeEnabledSoundingsSpinnerObserveWidget,
				selectOutputFormatPageModel.getNviSamplingMode(),
				new UpdateValueStrategy<>(UpdateValueStrategy.POLICY_NEVER), strategy_1);
		//
		var observeTextXsfTitleTextObserveWidget = WidgetProperties.text(SWT.Modify).observe(xsfTitleText);
		bindingContext.bindValue(observeTextXsfTitleTextObserveWidget, selectOutputFormatPageModel.getXsfTitle(), null,
				null);
		//
		var observeTextXsfSummaryTextObserveWidget = WidgetProperties.text(SWT.Modify).observe(xsfSummaryText);
		bindingContext.bindValue(observeTextXsfSummaryTextObserveWidget, selectOutputFormatPageModel.getXsfSummary(),
				null, null);
		//
		var observeTextXsfKeywordsTextObserveWidget = WidgetProperties.text(SWT.Modify).observe(xsfKeywordsText);
		bindingContext.bindValue(observeTextXsfKeywordsTextObserveWidget, selectOutputFormatPageModel.getXsfKeywords(),
				null, null);
		//
		var observeTextXsfLicenseTextObserveWidget = WidgetProperties.text(SWT.Modify).observe(xsfLicenseText);
		bindingContext.bindValue(observeTextXsfLicenseTextObserveWidget, selectOutputFormatPageModel.getXsfLicense(),
				null, null);
		//
		var observeTextXsfRightsTextObserveWidget = WidgetProperties.text(SWT.Modify).observe(xsfRightsText);
		bindingContext.bindValue(observeTextXsfRightsTextObserveWidget, selectOutputFormatPageModel.getXsfRights(),
				null, null);
	}
}
