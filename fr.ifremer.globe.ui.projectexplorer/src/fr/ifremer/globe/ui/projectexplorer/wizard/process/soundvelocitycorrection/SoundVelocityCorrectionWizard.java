package fr.ifremer.globe.ui.projectexplorer.wizard.process.soundvelocitycorrection;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.core.processes.soundvelocitycorrection.process.SoundVelocityCorrectionType;
import fr.ifremer.globe.ui.projectexplorer.wizard.TimeIntervalParamPage;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

public class SoundVelocityCorrectionWizard extends Wizard {

	/** Model */
	protected SoundVelocityCorrectionWizardModel correctionWizardModel;

	/** Pages */
	protected SelectInputFilesPage selectInputFilesPage;
	protected SoundVelocityCorrectionParamMain mainPage;
	protected TimeIntervalParamPage timeIntervalParamPage;
	protected SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public SoundVelocityCorrectionWizard(SoundVelocityCorrectionWizardModel correctionWizardModel) {
		this.correctionWizardModel = correctionWizardModel;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(correctionWizardModel);
		addPage(selectInputFilesPage);
		mainPage = new SoundVelocityCorrectionParamMain(correctionWizardModel.getSoundVelocityCorrectionParameters());
		addPage(mainPage);
		if (correctionWizardModel.getCorrectionType() == SoundVelocityCorrectionType.CORRECTION_FROM_MBG) {
			timeIntervalParamPage = new TimeIntervalParamPage(correctionWizardModel.getTimeIntervalParameters());
			addPage(timeIntervalParamPage);
			selectOutputParametersPage = new SelectOutputParametersPage(correctionWizardModel);
			addPage(selectOutputParametersPage);
		}
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		mainPage.saveData();
		correctionWizardModel.getSoundVelocityCorrectionParameters().save();

		if (correctionWizardModel.getCorrectionType() == SoundVelocityCorrectionType.CORRECTION_FROM_MBG) {
			timeIntervalParamPage.saveData();
			correctionWizardModel.getTimeIntervalParameters().save();
		}
		return true;
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		return correctionWizardModel.getInputFiles().isEmpty() ? selectInputFilesPage : mainPage;
	}

}
