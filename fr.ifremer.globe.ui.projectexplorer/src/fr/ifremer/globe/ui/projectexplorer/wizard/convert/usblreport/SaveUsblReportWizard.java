package fr.ifremer.globe.ui.projectexplorer.wizard.convert.usblreport;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Convert wizard
 */
public class SaveUsblReportWizard extends Wizard {

	/** Model */
	protected SaveUsblReportWizardModel wizardModel;

	/** Pages */
	protected SelectInputFilesPage selectInputFilesPage;
	protected SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public SaveUsblReportWizard(SaveUsblReportWizardModel wizardModel) {
		this.wizardModel = wizardModel;
		setNeedsProgressMonitor(true);
	}

	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(wizardModel);
		addPage(selectInputFilesPage);
		selectOutputParametersPage = new SelectOutputParametersPage(wizardModel);
		selectOutputParametersPage.enableLoadFilesAfter(false);
		addPage(selectOutputParametersPage);
	}

	@Override
	public boolean performFinish() {
		return selectInputFilesPage.isPageComplete() && selectOutputParametersPage.isPageComplete();
	}

	@Override
	public IWizardPage getStartingPage() {
		return wizardModel.getInputFiles().isEmpty() ? selectInputFilesPage : selectOutputParametersPage;
	}
}
