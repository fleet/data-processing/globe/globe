/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.widget;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;


/**
 * ComboViewer used to display Attributes.
 */
public class AttributeCombo extends ComboViewer {

	/**
	 * Constructor.
	 */
	public AttributeCombo(Composite parent, int style) {
		super(parent, style);
		setLabelProvider(new LabelProvider() {

			/**
			 * Follow the link.
			 *
			 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
			 */
			@Override
			public String getText(Object element) {
				PredefinedLayers<?> att = (PredefinedLayers<?>) element;
				String label = att.getName();
				if (!att.getUnit().isBlank())
					label += " [" + att.getUnit() + "]";
				return label;
			}
		});
		setContentProvider(new ArrayContentProvider());

		// Get Long name in tooltip
		addSelectionChangedListener(event -> {
			PredefinedLayers<?> att = (PredefinedLayers<?>) event.getStructuredSelection().getFirstElement();
			getCombo().setToolTipText(att.getDisplayedName());
		});
	}

}
