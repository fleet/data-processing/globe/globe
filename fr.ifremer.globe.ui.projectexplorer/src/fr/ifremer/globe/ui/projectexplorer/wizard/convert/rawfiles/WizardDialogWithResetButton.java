/**
 * 
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.convert.rawfiles;

import java.util.Optional;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.PageChangedEvent;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

/**
 * A WizardDialog with a reset button to clear the output format parameters
 */
public class WizardDialogWithResetButton extends WizardDialog {

	/** Managed model */
	private final ConvertRawFilesWizardModel model;

	/** Output page initialized on wizard */
	Optional<SelectOutputFormatPage> selectOutputFormatPage = Optional.empty();

	private Button resetButton;

	/**
	 * Constructor
	 */
	public WizardDialogWithResetButton(Shell shell, IWizard wizard, ConvertRawFilesWizardModel model) {
		super(shell, wizard);
		addPageChangedListener(this::onPageChanged);
		this.model = model;
	}

	/** Add the Reset button */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		resetButton = createButton(parent, IDialogConstants.CLIENT_ID, "Reset", false);
		resetButton.setToolTipText("Reset parameters of output format page");
		resetButton.setEnabled(false);
		super.createButtonsForButtonBar(parent);
	}

	private void onPageChanged(PageChangedEvent event) {
		if (event.getSelectedPage() instanceof SelectOutputFormatPage page) {
			this.selectOutputFormatPage = Optional.of(page);
			resetButton.setEnabled(true);
		} else
			resetButton.setEnabled(false);
	}

	/** Reacts when a button is pressed */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.CLIENT_ID) {
			model.reset();
			selectOutputFormatPage.ifPresent(SelectOutputFormatPage::updatePageComplete);
			updateButtons();
		} else
			super.buttonPressed(buttonId);
	}
}
