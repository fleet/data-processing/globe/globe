package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tonvi;

import java.util.Set;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

/**
 * Handler class to open the process wizard (referenced in fragment.e4xmi).
 */
public class ConvertToNviOpenWizardHandler extends AbstractOpenProcessWizardHandler<ConvertToNviProcessWithWizard> {

	@Override
	protected ConvertToNviProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new ConvertToNviProcessWithWizard(shell);
	}

	@Override
	protected Set<ContentType> getAcceptedContentType() {
		return ContentType.getNavigationContentTypes();
	}

}