/**
 * Globe - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.convert.rawfiles;

import java.io.File;

import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.ui.wizard.OutputFileNameComputer;
import fr.ifremer.globe.utils.FileUtils;

/** Redefines OutputFileNameComputer to propose a specific name for the XSF file to migrate */
class XsfFileNameComputer extends OutputFileNameComputer {

	/** Model */
	protected ConvertRawFilesWizardModel convertRawFilesWizardModel;

	/**
	 * Constructor
	 */
	public XsfFileNameComputer(ConvertRawFilesWizardModel convertRawFilesWizardModel) {
		this.convertRawFilesWizardModel = convertRawFilesWizardModel;
	}

	/** {@inheritDoc} */
	@Override
	protected File computeOutputFilename(File file, OutputFileNameComputerModel model, String extension) {
		// Default behavior when the user decides to take over
		if (XsfBase.SUFFIX_XSF.equals(extension)) {
			File referenceFolder = convertRawFilesWizardModel.getXsfReferenceFolder().doGetValue();
			// When migration is required, search the reference xsf file and set the same name to the output file
			if (referenceFolder != null && referenceFolder.isDirectory()) {
				String basename = FileUtils.getBaseName(file);
				String[] machingFiles = referenceFolder
						.list((d, n) -> n.startsWith(basename) && n.endsWith(XsfBase.EXTENSION_XSF));
				if (machingFiles.length > 0) {
					file = new File(file.getParent(), machingFiles[0]);
				}
			}
		}
		return super.computeOutputFilename(file, model, extension);
	}
}
