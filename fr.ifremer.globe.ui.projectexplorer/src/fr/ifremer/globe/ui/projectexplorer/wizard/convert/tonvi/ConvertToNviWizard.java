package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tonvi;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Wizard to convert to NVI files
 */
public class ConvertToNviWizard extends Wizard {

	/** Model */
	protected ConvertToNviWizardModel convertToNviWizardModel;

	/** Pages */
	protected SelectInputFilesPage selectInputFilesPage;
	protected SelectNviExportOptionPage selectOutputFormatPage;
	protected SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public ConvertToNviWizard(ConvertToNviWizardModel convertToNviWizardModel) {
		this.convertToNviWizardModel = convertToNviWizardModel;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(convertToNviWizardModel);
		addPage(selectInputFilesPage);
		selectOutputFormatPage = new SelectNviExportOptionPage(convertToNviWizardModel);
		addPage(selectOutputFormatPage);
		selectOutputParametersPage = new SelectOutputParametersPage(convertToNviWizardModel);
		selectOutputParametersPage.enableMergeOutputFiles(true);
		addPage(selectOutputParametersPage);
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		return selectInputFilesPage.isPageComplete() && selectOutputFormatPage.isPageComplete()
				&& selectOutputParametersPage.isPageComplete();
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		return convertToNviWizardModel.getInputFiles().isEmpty() ? selectInputFilesPage : selectOutputFormatPage;
	}

}
