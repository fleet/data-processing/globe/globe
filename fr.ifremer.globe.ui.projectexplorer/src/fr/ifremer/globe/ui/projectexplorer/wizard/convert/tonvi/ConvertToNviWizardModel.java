package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tonvi;

import java.util.Optional;

import fr.ifremer.globe.core.io.nvi.service.INviWriter.SamplingMode;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableNumber;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.tonvi.SelectNviExportOptionPage.SelectNviExportOptionPageModel;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;

/**
 * Model of the wizard page
 */
public class ConvertToNviWizardModel extends ConvertWizardDefaultModel implements SelectNviExportOptionPageModel {
	/** .nvi */
	public static final String NVI_EXTENSION = IFileService.grab().getExtensions(ContentType.NVI_NETCDF_4).get(0);

	/** NVI export option */
	protected WritableObject<SamplingMode> nviSamplingMode = new WritableObject<>(SamplingMode.NONE);
	protected WritableNumber nviTimeSamplingValue = new WritableNumber(1);
	protected WritableNumber nviSoundingsSamplingValue = new WritableNumber(1);
	protected WritableBoolean mergeOutputFiles = new WritableBoolean(false);

	/**
	 * Constructor
	 */
	public ConvertToNviWizardModel() {
		IFileService fileService = IFileService.grab();
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.getNavigationContentTypes()));
		outputFormatExtensions.add(NVI_EXTENSION);
	}

	/**
	 * Getter of nviSamplingMode
	 */
	@Override
	public WritableObject<SamplingMode> getNviSamplingMode() {
		return nviSamplingMode;
	}

	/**
	 * Getter of nviTimeSamplingValue
	 */
	@Override
	public WritableNumber getNviTimeSamplingValue() {
		return nviTimeSamplingValue;
	}

	/**
	 * Getter of nviSoundingsSamplingValue
	 */
	@Override
	public WritableNumber getNviSoundingsSamplingValue() {
		return nviSoundingsSamplingValue;
	}

	@Override
	public Optional<WritableBoolean> getMergeOutputFiles() {
		return Optional.of(mergeOutputFiles);
	}
}