package fr.ifremer.globe.ui.projectexplorer.wizard.convert.usblreport;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import fr.ifremer.globe.core.io.usbl.UsblFileInfo;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.globes.Earth;

/**
 * Tide conversion process with wizard
 */
public class SaveUsblReportProcessWithWizard extends DefaultProcessWithWizard<SaveUsblReportWizardModel> {

	private Logger logger;

	/**
	 * Constructor
	 */
	public SaveUsblReportProcessWithWizard(Shell shell) {
		super(shell, "Save USBL report", SaveUsblReportWizardModel::new, SaveUsblReportWizard::new);
	}

	/**
	 * Performs processing.
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		this.logger = logger;
		List<File> inputFiles = model.getInputFiles().asList();
		SubMonitor subMonitor = SubMonitor.convert(monitor, inputFiles.size());
		for (File inputFile : inputFiles) {
			processConversion(inputFile, subMonitor.split(1));
		}
		return Status.OK_STATUS;
	}

	/**
	 * Processes the conversion for one file.
	 */
	protected void processConversion(File inputFile, IProgressMonitor monitor) {
		String inputFilePath = inputFile.getPath();
		File outputFile = outputFileNameComputer.compute(inputFile, model,
				SaveUsblReportWizardModel.USBL_REPORT_EXTENSION);

		if (shouldBeProcessed(outputFile, logger)) {
			try {
				// read selected file
				Optional<IFileInfo> infoStore = IFileService.grab().getFileInfo(inputFilePath);
				if (infoStore.isPresent() && infoStore.get() instanceof UsblFileInfo usblFileInfo) {
					write(usblFileInfo, outputFile.getAbsolutePath());
				} else {
					throw new GIOException("Input file not correct: " + inputFile.getName());
				}
			} catch (Exception e) {
				logger.error("Error while converting from {} to {} ({})", inputFile.getName(), outputFile.getName(),
						e.getMessage());
				FileUtils.deleteQuietly(outputFile);
			}
		}
	}

	/**
	 * Writes a CSV tide file.
	 */
	public void write(UsblFileInfo usblFileInfo, String filePath) throws GIOException {
		logger.info("Write USBL report file (.json) : {} ", filePath);
		if (!filePath.endsWith("json"))
			throw new GIOException("Invalid file path : .json extension required");

		// Convert the data object to JSON format and write to file
		try (FileWriter writer = new FileWriter(filePath)) {

			// Get data
			var settings = usblFileInfo.getSettings();
			var usblLat = usblFileInfo.getAverageLatitudeBeforeLanding();
			var usblLon = usblFileInfo.getAverageLongitudeBeforeLanding();
			var targetLatlon = LatLon.fromDegrees(settings.targetLatitude(), settings.targetLongitude());
			var usblLatlon = LatLon.fromDegrees(usblLat, usblLon);
			double deltaDistance = LatLon.ellipsoidalDistance(targetLatlon, usblLatlon, Earth.WGS84_EQUATORIAL_RADIUS,
					Earth.WGS84_POLAR_RADIUS);
			Angle deltaHeading = LatLon.ellipsoidalForwardAzimuth(targetLatlon, usblLatlon,
					Earth.WGS84_EQUATORIAL_RADIUS, Earth.WGS84_POLAR_RADIUS);
			double detlaX = deltaDistance * Math.sin(deltaHeading.radians);
			double detlaY = deltaDistance * Math.cos(deltaHeading.radians);

			// Build JSON object
			var json = new JsonObject();

			// Name
			json.addProperty("name", usblFileInfo.getBaseName());
			json.addProperty("survey", settings.survey());
			json.addProperty("description", settings.description());

			// Target
			var jsonTarget = new JsonObject();
			jsonTarget.addProperty("latitude", LatLongFormater.latitudeToString(settings.targetLatitude()));
			jsonTarget.addProperty("longitude", LatLongFormater.longitudeToString(settings.targetLongitude()));
			jsonTarget.addProperty("radius (m)", settings.targetRadius());
			json.add("target", jsonTarget);

			// USBL location
			var jsonLanding = new JsonObject();
			jsonLanding.addProperty("datetime", settings.landingDateTime().toString());
			jsonLanding.addProperty("average latitude", LatLongFormater.latitudeToString(usblLat));
			jsonLanding.addProperty("average longitude", LatLongFormater.longitudeToString(usblLon));
			jsonLanding.addProperty("seconds before landing to compute average",
					settings.durationBeforeLandingInSeconds());
			jsonLanding.addProperty("data count to compute average", usblFileInfo.getDataBeforeLanding().size());
			json.add("landing", jsonLanding);

			// Delta information
			var jsonDelta = new JsonObject();
			jsonDelta.addProperty("distance (m)", round(deltaDistance));
			jsonDelta.addProperty("heading (degrees)", round(deltaHeading.degrees));
			jsonDelta.addProperty("delta X (m)", round(detlaX));
			jsonDelta.addProperty("delta Y (m)", round(detlaY));
			json.add("delta", jsonDelta);

			// Write JSON
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			logger.info("Report :\n" + gson.toJson(json));
			gson.toJson(json, writer);
			logger.info("USBL report file created successfully.");

		} catch (IOException e) {
			throw new GIOException("Error while writing USBL report file : " + e.getMessage(), e);
		}
	}

	private double round(double value) {
		return new BigDecimal(value).setScale(2, RoundingMode.HALF_UP).doubleValue();
	}

}
