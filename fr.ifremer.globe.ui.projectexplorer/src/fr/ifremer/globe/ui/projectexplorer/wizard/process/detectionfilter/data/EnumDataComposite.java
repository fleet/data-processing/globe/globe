/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.data;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.processes.detectionfilter.parameters.DataParameters;

/**
 * Composite used to edit a enum value to set.
 */
public class EnumDataComposite extends ADataComposite {
	protected Combo value;
	protected String[] values;

	/**
	 * Constructor.
	 */
	public EnumDataComposite(Composite parent, int style, String[] labels, String[] values) {
		super(parent, style);
		this.values = values;

		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = gridLayout.marginWidth = 0;
		setLayout(gridLayout);
		value = new Combo(this, SWT.READ_ONLY);
		value.setItems(labels);
		GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		value.setLayoutData(gridData);
		value.select(0);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.data.ADataComposite#updateParameters(fr.ifremer.globe.imagery.script.parameter.DataParameters)
	 */
	@Override
	public void updateParameters(DataParameters parameters) {
		parameters.setValue(values[value.getSelectionIndex()]);
	}
}
