package fr.ifremer.globe.ui.projectexplorer.wizard.convert.usblreport;

import java.util.EnumSet;
import java.util.Set;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

/**
 * Handler class to open the process wizard (referenced in fragment.e4xmi).
 */
public class SaveUsblReportWizardHandler extends AbstractOpenProcessWizardHandler<SaveUsblReportProcessWithWizard> {

	@Override
	protected Set<ContentType> getAcceptedContentType() {
		return EnumSet.of(ContentType.USBL);
	}

	@Override
	protected SaveUsblReportProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new SaveUsblReportProcessWithWizard(shell);
	}

}
