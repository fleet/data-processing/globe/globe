/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.data;

import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.processes.detectionfilter.parameters.DataParameters;

/**
 * Swt Composite to edit the value of the attribute to set.
 */
public abstract class ADataComposite extends Composite {

	/**
	 * Constructor.
	 */
	public ADataComposite(Composite parent, int style) {
		super(parent, style);
	}

	/** Ask to update the parameters with the edition. */
	abstract public void updateParameters(DataParameters parameters);

}
