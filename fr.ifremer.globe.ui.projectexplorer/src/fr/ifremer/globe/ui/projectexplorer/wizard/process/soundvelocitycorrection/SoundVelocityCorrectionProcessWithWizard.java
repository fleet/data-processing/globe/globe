package fr.ifremer.globe.ui.projectexplorer.wizard.process.soundvelocitycorrection;

import java.util.List;
import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.processes.soundvelocitycorrection.process.SoundVelocityCorrection;
import fr.ifremer.globe.core.processes.soundvelocitycorrection.process.SoundVelocityCorrectionType;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.ui.wizard.FilesToProcessEntry;
import fr.ifremer.globe.utils.exception.GException;

/**
 * Depth correction process with wizard
 */
public class SoundVelocityCorrectionProcessWithWizard
		extends DefaultProcessWithWizard<SoundVelocityCorrectionWizardModel> {

	/**
	 * Constructor
	 */
	public SoundVelocityCorrectionProcessWithWizard(Shell shell, SoundVelocityCorrectionType type) {
		super(shell, SoundVelocityCorrection.NAME, () -> new SoundVelocityCorrectionWizardModel(type),
				SoundVelocityCorrectionWizard::new);
	}

	/**
	 * Process method executed after a click on finish.
	 * 
	 * @throws GException
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		SoundVelocityCorrectionType correctionType = model.getCorrectionType();
		// Get file list (input/output/temp file tuples)
		List<FilesToProcessEntry> filesToProcess = getFilesToProcessWithTemp(logger);
		IDataContainerInfoService infoService = IDataContainerInfoService.grab();

		// Apply sound velocity correction
		String s = filesToProcess.size() > 1 ? "s" : "";
		logger.info("Start processing {} of {} file{}...", correctionType, filesToProcess.size(), s);
		SoundVelocityCorrection soundVelocityCorrection = new SoundVelocityCorrection(
				model.getSoundVelocityCorrectionParameters(), model.getTimeIntervalParameters(), logger);
		soundVelocityCorrection.loadConfiguration(monitor, filesToProcess.size());
		// Loop on input files
		int processedFileCount = 0;
		int errorFileCount = 0;
		for (FilesToProcessEntry entry : filesToProcess) {
			monitor.setTaskName("Processing : " + entry.getInputFile().getAbsolutePath());
			logger.info("Processing : {}", entry.getInputFile().getAbsolutePath());
			Optional<ISounderNcInfo> info = infoService.getSounderNcInfo(entry.getTempFile().getAbsolutePath());
			if (info.isPresent()) {
				try {
					// Process file
					soundVelocityCorrection.processFile(info.get());
					// write output file
					writeOutputFileFromTemp(entry);
					processedFileCount++;
				} catch (Exception e) {
					logger.error("Error while processing file {}: {}", entry.getInputFile().getPath(), e.getMessage(),
							e);
					errorFileCount++;
				}
			}
			if (monitor.isCanceled())
				return Status.CANCEL_STATUS;
		}

		// load output files if asked
		loadOutputFiles(filesToProcess, monitor);

		logger.info("End of process: {} applied on {} file{}.", correctionType, processedFileCount, s);
		if (errorFileCount > 0) {
			String sError = errorFileCount > 1 ? "s" : "";
			logger.error("Error while processing {} file{}", errorFileCount, sError);
		}

		return monitor.isCanceled() ? Status.CANCEL_STATUS : Status.OK_STATUS;
	}
}
