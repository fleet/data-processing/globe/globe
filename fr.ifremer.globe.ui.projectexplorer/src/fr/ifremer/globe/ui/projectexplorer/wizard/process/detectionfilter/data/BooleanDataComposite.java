/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.data;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.processes.detectionfilter.parameters.DataParameters;
import fr.ifremer.globe.utils.number.NumberUtils;

/**
 * Composite used to edit a boolean value to set.
 */
public class BooleanDataComposite extends ADataComposite {
	private Button trueButton;
	private Button falseButton;

	/**
	 * Constructor.
	 */
	public BooleanDataComposite(Composite parent, int style) {
		super(parent, style);

		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.marginHeight = gridLayout.marginWidth = 0;
		setLayout(gridLayout);

		trueButton = new Button(this, SWT.RADIO);
		trueButton.setText(Boolean.TRUE.toString());
		trueButton.setLayoutData(new GridData(GridData.GRAB_VERTICAL | GridData.VERTICAL_ALIGN_CENTER));
		trueButton.setSelection(true);

		falseButton = new Button(this, SWT.RADIO);
		falseButton.setText(Boolean.FALSE.toString());
		falseButton.setLayoutData(new GridData(GridData.GRAB_VERTICAL | GridData.VERTICAL_ALIGN_CENTER));
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.data.ADataComposite#updateParameters(fr.ifremer.globe.imagery.script.parameter.DataParameters)
	 */
	@Override
	public void updateParameters(DataParameters parameters) {
		parameters.setValue(String.valueOf(NumberUtils.bool2byte(trueButton.getSelection())));
	}
}
