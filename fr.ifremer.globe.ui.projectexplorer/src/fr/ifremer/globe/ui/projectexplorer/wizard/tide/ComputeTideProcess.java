/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.tide;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.tide.ITideData;
import fr.ifremer.globe.core.runtime.job.GProcess;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * A simple process to perform a tide computation and produce a list of ITideData.
 */
public abstract class ComputeTideProcess extends GProcess {

	protected final List<ITideData> result = new ArrayList<>();

	/**
	 * Constructor
	 */
	protected ComputeTideProcess() {
		super("Conputing Tide");
	}

	/** Specific tide computations */
	abstract List<ITideData> computesTide(IProgressMonitor monitor) throws IOException, GException;

	/** Additional operations to perform in UI after a correct execution */
	abstract void onFinished();

	/** {@inheritDoc} */
	@Override
	protected IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		try {
			result.addAll(computesTide(monitor));
		} catch (IOException e) {
			throw GIOException.wrap(e.getMessage(), e);
		}
		return Status.OK_STATUS;
	}

}
