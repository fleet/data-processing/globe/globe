package fr.ifremer.globe.ui.projectexplorer.wizard.tide;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Stream;

import jakarta.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.tide.TideFileInfo;
import fr.ifremer.globe.core.io.tide.csv.TideCsvFileInfoSupplier;
import fr.ifremer.globe.core.io.tide.ttb.TtbFileInfoSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.tide.ITideData;
import fr.ifremer.globe.core.runtime.gws.GwsServiceAgent;
import fr.ifremer.globe.core.runtime.gws.param.EstimateTideParams;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.projectexplorer.Activator;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import swing2swt.layout.FlowLayout;

public class TideEstimationComposite extends Composite {

	private static final String NAV_PYAT_PROCESS = "Navigation tide estimator";
	private static final String XSFNMEA_PYAT_PROCESS = "XSF NMEA tide estimator";

	private static final Logger LOGGER = LoggerFactory.getLogger(TideEstimationComposite.class);

	@Inject
	GwsServiceAgent gwsServiceAgent;
	@Inject
	TideCsvFileInfoSupplier tideCsvFileInfoSupplier;

	/** Properties **/
	private File tideModelsDirFile;
	private final ComputeTideWizardModel model;
	private boolean isReferenceSurfaceAvailable;
	private boolean isFesModelAvailable;
	private boolean isBathyElliAvailable;

	/** Inner widgets **/
	private Button checkBoxRTKFiltering;
	private Button checkBoxPredictiveMode;
	private Button radioBtnReferenceSurfaceLAT;
	private Button radioBtnReferenceSurfaceBathyElli;
	private Button radioBtnReferenceSurfaceEllipsoid;
	private Spinner zRefSpinner;

	/**
	 * Constructor
	 */
	public TideEstimationComposite(Composite parent, int style, ComputeTideWizardModel wizardModel) {
		super(parent, style);
		model = wizardModel;
		ContextInitializer.inject(this);
		ComputeTidePreferences preferences = Activator.getComputeTidePreferences();
		try {
			tideModelsDirFile = preferences.getTideModelsDir().getValue().toFile();

			// if (xsfTideReferenceAltitude.getValue() != null)
			// model.getZRef().set(Double.valueOf(xsfTideReferenceAltitude.getValue()));

			// if (sensorNavTideReferenceAltitude.getValue() != null)
			// model.getZRef().set(Double.valueOf(sensorNavTideReferenceAltitude.getValue()));

			// setup estimation process
			isReferenceSurfaceAvailable = tideModelsDirFile.exists() && tideModelsDirFile.isDirectory() && Stream
					.of(tideModelsDirFile.listFiles()).anyMatch(file -> file.getName().endsWith("reference_surfaces"));
			isFesModelAvailable = tideModelsDirFile.exists() && tideModelsDirFile.isDirectory()
					&& Stream.of(tideModelsDirFile.listFiles()).anyMatch(file -> file.getName().endsWith("fes2014"));
			isBathyElliAvailable = tideModelsDirFile.exists() && tideModelsDirFile.isDirectory()
					&& Stream.of(tideModelsDirFile.listFiles())
							.anyMatch(file -> (file.getName().endsWith("BATHYELLI_ZH_ell_V1_1")
									|| file.getName().endsWith("BATHYELLI_ZH_ell_V2")));
		} catch (NoSuchElementException e) {
			// error getting pyAt process : just display error message
			setLayout(new GridLayout(1, false));
			Label lblError = new Label(this, SWT.NONE);
			lblError.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
			lblError.setText("Compute plateform elevation process not available (PyAT project not compatible).");
			return;
		}

		createUI();
		intialize();
	}

	/**
	 * Builds the UI.
	 */
	public void createUI() {
		setLayout(new GridLayout(1, false));

		Label lbl = new Label(this, SWT.WRAP);
		lbl.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lbl.setText("Estimate plateform elevation relative to reference surface.\n"
				+ "The resulting estimation doesn't contain the actual tide.\n"
				+ "Substract dynamic draught and nominal water level to have comparable values.");

		// estimation model options
		Group grpEstimationModel = new Group(this, SWT.NONE);
		grpEstimationModel.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		GridData gd_grpEstimationModel = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_grpEstimationModel.widthHint = 400;
		grpEstimationModel.setLayoutData(gd_grpEstimationModel);
		grpEstimationModel.setLayout(new GridLayout(2, false));
		grpEstimationModel.setText("Estimation model");

		Composite composite = new Composite(grpEstimationModel, SWT.NONE);
		GridData gd_composite = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_composite.widthHint = 460;
		composite.setLayoutData(gd_composite);
		composite.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		var buttonHelp = new Button(composite, SWT.CENTER);
		buttonHelp.setToolTipText("Show tide estimation help");
		buttonHelp.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.ui.projectexplorer", "icons/16/help.png"));
		buttonHelp.addListener(SWT.Selection, e -> opendHelp());

		Label lblTips = new Label(composite, SWT.WRAP);
		lblTips.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lblTips.setText(" Only (*.xsf.nc,*.nvi.nc,*.nvi,*.nav;*.gps) files are supported.\n"
				+ " Model files must be defined in preferences to get reference surface available.");

		checkBoxRTKFiltering = new Button(grpEstimationModel, SWT.CHECK);
		checkBoxRTKFiltering.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		checkBoxRTKFiltering.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		checkBoxRTKFiltering.setText("Remove non RTK values");

		checkBoxPredictiveMode = new Button(grpEstimationModel, SWT.CHECK);
		checkBoxPredictiveMode.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		checkBoxPredictiveMode.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		checkBoxPredictiveMode
				.setText("Predictive mode : Use FES2014 predicted tide to improve data filtering/interpolation");

		Label lblReferenceSurface = new Label(grpEstimationModel, SWT.NONE);
		lblReferenceSurface.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblReferenceSurface.setText("Reference surface :");

		radioBtnReferenceSurfaceLAT = new Button(grpEstimationModel, SWT.RADIO);
		radioBtnReferenceSurfaceLAT.setSelection(true);
		GridData gd_btnLat = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_btnLat.horizontalIndent = 10;
		radioBtnReferenceSurfaceLAT.setLayoutData(gd_btnLat);
		radioBtnReferenceSurfaceLAT.setText("LAT (FES2014)");

		radioBtnReferenceSurfaceBathyElli = new Button(grpEstimationModel, SWT.RADIO);
		GridData gd_btnBathyElli = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		gd_btnBathyElli.horizontalIndent = 10;
		radioBtnReferenceSurfaceBathyElli.setLayoutData(gd_btnBathyElli);
		radioBtnReferenceSurfaceBathyElli.setText("ZeroHydro/BathyElli (SHOM)");

		radioBtnReferenceSurfaceEllipsoid = new Button(grpEstimationModel, SWT.RADIO);
		GridData gd_btnEllipsoid = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btnEllipsoid.horizontalIndent = 10;
		radioBtnReferenceSurfaceEllipsoid.setLayoutData(gd_btnEllipsoid);
		radioBtnReferenceSurfaceEllipsoid.setText("Ellipsoid");

		Composite compositeZref = new Composite(grpEstimationModel, SWT.NONE);
		compositeZref.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		compositeZref.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));

		Label lblZref = new Label(compositeZref, SWT.WRAP);
		lblZref.setText("     Z ref :");

		zRefSpinner = new Spinner(compositeZref, SWT.BORDER);
		zRefSpinner.setToolTipText("\"Reference altitude wrt ellipsoid\"");
		zRefSpinner.setMinimum(-100000);
		zRefSpinner.setMaximum(100000);
		zRefSpinner.setDigits(2);
		zRefSpinner.setSelection((int) Math.round(model.getZRef().get() * Math.pow(10, zRefSpinner.getDigits())));
		zRefSpinner.addListener(SWT.Modify, e -> onZRefChanged());

	}

	private void onZRefChanged() {
		double zRef = zRefSpinner.getSelection() / Math.pow(10, zRefSpinner.getDigits());
		model.getZRef().set(zRef);
	}

	private void intialize() {
		checkBoxRTKFiltering.setEnabled(true);
		checkBoxRTKFiltering.setSelection(true);
		checkBoxPredictiveMode.setEnabled(isFesModelAvailable);
		checkBoxPredictiveMode.setSelection(isFesModelAvailable);
		radioBtnReferenceSurfaceLAT.setEnabled(isReferenceSurfaceAvailable);
		radioBtnReferenceSurfaceLAT.setSelection(isReferenceSurfaceAvailable);
		radioBtnReferenceSurfaceBathyElli.setEnabled(isBathyElliAvailable);
		radioBtnReferenceSurfaceBathyElli.setSelection(isBathyElliAvailable && !isReferenceSurfaceAvailable);
		radioBtnReferenceSurfaceEllipsoid.setEnabled(true);
		radioBtnReferenceSurfaceEllipsoid.setSelection(!isBathyElliAvailable && !isReferenceSurfaceAvailable);
	}

	@Override
	public Point computeSize(int wHint, int hHint, boolean changed) {
		if (isVisible()) {
			return super.computeSize(wHint, hHint, changed);
		}
		return new Point(0, 0);
	}

	/**
	 * Creates a process to compute tide estimation with selected parameters.
	 */
	public Optional<ComputeTideProcess> prepareCompute() {
		// Check model availability
		if (!isReferenceSurfaceAvailable && !isBathyElliAvailable) {
			Messages.openErrorMessage(NAV_PYAT_PROCESS, "No reference surface available.");
			return Optional.empty();
		}

		// find input files type (check first file)
		if (model.getInputFiles().isEmpty()) {
			Messages.openErrorMessage(NAV_PYAT_PROCESS, "No input file to compute.");
			return Optional.empty();
		}

		IFileService fileService = IFileService.grab();
		ContentType contentType = fileService.getContentType(model.getInputFiles().get(0).getAbsolutePath());
		if (!contentType.isOneOf(ContentType.XSF_NETCDF_4, ContentType.NVI_NETCDF_4, ContentType.NVI_V2_NETCDF_4, ContentType.TECHSAS_NETCDF)) {
			Messages.openErrorMessage(NAV_PYAT_PROCESS, "Only (*.xsf.nc,*.nvi.nc,*.nvi,*.nav;*.gps) files are supported.");
			return Optional.empty();
		}

		if (model.getInputFiles().stream().anyMatch(e -> {
			return fileService.getContentType(e.getAbsolutePath()) != contentType;
		})) {
			Messages.openErrorMessage(NAV_PYAT_PROCESS, "All input files must have the same type.");
			return Optional.empty();
		}

		String process = ContentType.XSF_NETCDF_4 == contentType ? XSFNMEA_PYAT_PROCESS : NAV_PYAT_PROCESS;

		return Optional.of(new ComputeTideProcess() {
			@Override
			List<ITideData> computesTide(IProgressMonitor monitor) throws IOException, GException {
				return computeTideEstimation(process, monitor);
			}

			@Override
			void onFinished() {
				String estimationName = "plateform_elevation";
				model.setTideData(result);
				model.getTideDataTitle().set(String.format("%s from %s to %s %s", estimationName, model.getStart(),
						model.getEnd(), model.getIntervalInMinutes().map(i -> "(interval : " + i + ")").orElse("")));

				// update output filename
				Function<Instant, String> formatDate = instant -> instant.toString().split("\\.")[0].replace(":", "-");
				model.setSingleOutputFilename(
						String.format("%s_%s_%s.%s", estimationName.toUpperCase(), formatDate.apply(model.getStart()),
								formatDate.apply(model.getEnd()), TtbFileInfoSupplier.EXTENSION_TTB));
			}
		});
	}

	private List<ITideData> computeTideEstimation(String processName, IProgressMonitor monitor)
			throws IOException, GException {
		File outputFile = null;
		try {
			// launch prediction process
			outputFile = TemporaryCache.createTemporaryFile("PLATEFORM_ELEVATION_OUTPUT", ".csv");
			if (XSFNMEA_PYAT_PROCESS.equals(processName))
				gwsServiceAgent.estimateXsfNmeaTide(makeEstimateTideParams(outputFile), monitor, LOGGER);
			else
				gwsServiceAgent.estimateNavigationTide(makeEstimateTideParams(outputFile), monitor, LOGGER);
			// get result
			TideFileInfo ttbFile = tideCsvFileInfoSupplier.getFileInfo(outputFile.getPath())
					.orElseThrow(() -> new GIOException("Plateform elevation file not found."));
			return ttbFile.getData();
		} finally {
			// delete tmp files
			FileUtils.deleteQuietly(outputFile);
		}
	}

	private EstimateTideParams makeEstimateTideParams(File outputFile) {
		AtomicReference<EstimateTideParams> result = new AtomicReference<>();
		getDisplay().syncExec(() -> {
			String referenceSurface = "LAT";
			double referenceAltitude = 0.0;
			if (radioBtnReferenceSurfaceBathyElli.getSelection()) {
				referenceSurface = "ZH";
			} else if (radioBtnReferenceSurfaceEllipsoid.getSelection()) {
				referenceSurface = "ellipsoid";
				referenceAltitude = model.getZRef().get();
			}
			result.set(new EstimateTideParams(//
					model.getInputFiles().asList(), // inputFiles,
					outputFile, //
					isReferenceSurfaceAvailable ? tideModelsDirFile : null, // modelDir
					checkBoxRTKFiltering.getSelection(), // enableFiltering
					false, // displayMatplot
					checkBoxPredictiveMode.getSelection(), // predictiveMode
					referenceSurface, //
					referenceAltitude//
			));

		});
		return result.get();
	}

	/**
	 * Opens help page of the current process.
	 */
	public void opendHelp() {
		ContentType contentType = ContentType.XSF_NETCDF_4;
		if (!model.getInputFiles().isEmpty())
			// find input files type (check first file)
			contentType = IFileService.grab().getContentType(model.getInputFiles().get(0).getAbsolutePath());
		if (ContentType.XSF_NETCDF_4 == contentType)
			gwsServiceAgent.openHelp(XSFNMEA_PYAT_PROCESS);
		else
			gwsServiceAgent.openHelp(NAV_PYAT_PROCESS);
	}

}
