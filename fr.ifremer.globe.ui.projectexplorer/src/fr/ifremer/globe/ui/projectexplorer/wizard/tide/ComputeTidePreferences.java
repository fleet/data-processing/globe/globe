package fr.ifremer.globe.ui.projectexplorer.wizard.tide;

import java.nio.file.Path;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.attributes.DirectoryPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.FilePreferenceAttribute;

public class ComputeTidePreferences extends PreferenceComposite {

	/** Preferences attributes **/
	private final DirectoryPreferenceAttribute tideModelsDir;
	private final FilePreferenceAttribute shomHarmonicFile;

	/**
	 * Constructor
	 */
	public ComputeTidePreferences(PreferenceComposite father) {
		super(father, "Compute tide");

		tideModelsDir = new DirectoryPreferenceAttribute("tideModelsDir",
				"Tide models directory (containing ./reference_surfaces, ./fes2014, ...)", Path.of(""));
		declareAttribute(tideModelsDir);

		shomHarmonicFile = new FilePreferenceAttribute("shomHarmonicFile", "SHOM harmonic file", Path.of(""));
		declareAttribute(shomHarmonicFile);

		load();
	}

	public DirectoryPreferenceAttribute getTideModelsDir() {
		return tideModelsDir;
	}

	public FilePreferenceAttribute getShomHarmonicFile() {
		return shomHarmonicFile;
	}

}
