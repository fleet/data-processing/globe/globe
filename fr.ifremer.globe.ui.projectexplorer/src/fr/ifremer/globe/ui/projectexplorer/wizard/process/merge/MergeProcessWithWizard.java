package fr.ifremer.globe.ui.projectexplorer.wizard.process.merge;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.LongRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.processes.merge.IMergeProcess;
import fr.ifremer.globe.core.processes.merge.MbgMergeProcess;
import fr.ifremer.globe.core.processes.merge.NviMergeProcess;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.BadParameterException;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Merge process with wizard
 */
public class MergeProcessWithWizard extends DefaultProcessWithWizard<MergeWizardModel> {

	/**
	 * Constructor
	 */
	public MergeProcessWithWizard(Shell shell) {
		super(shell, "Cut / Merge tool", MergeWizardModel::new, MergeWizard::new);
	}

	/**
	 * Process method executed after a click on finish.
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		// Get input files
		List<File> inputFiles = model.getInputFiles().asList();

		IDataContainerInfoService infoService = IDataContainerInfoService.grab();

		// Choose merge process
		IMergeProcess mergeProcess = null;
		if (model.getMergeType() == ContentType.NVI_NETCDF_4) {
			List<IDataContainerInfo> infos = inputFiles.stream().//
					map(File::getAbsolutePath).//
					map(infoService::getDataContainerInfo).//
					filter(Optional::isPresent).//
					map(Optional::get).//
					collect(Collectors.toList());
			if (!infos.isEmpty()) {
				mergeProcess = new NviMergeProcess(infos);
			}
		} else if (model.getMergeType() == ContentType.MBG_NETCDF_3) {
			List<ISounderNcInfo> infos = inputFiles.stream().//
					map(File::getAbsolutePath).//
					map(infoService::getSounderNcInfo).//
					filter(Optional::isPresent).//
					map(Optional::get).//
					collect(Collectors.toList());
			if (!infos.isEmpty()) {
				mergeProcess = new MbgMergeProcess(infos);
			}
		}
		if (mergeProcess == null) {
			throw new GIOException("Error with input files, type not managed.");
		}

		// Launch a merge for each output file
		Map<File, Optional<LongRange>> outputFiles = model.getOutputFilesWithTimeIntervals();
		for (Entry<File, Optional<LongRange>> entry : outputFiles.entrySet()) {
			File outputFile = entry.getKey();
			if (!shouldBeProcessed(outputFile, logger))
				continue;

			Optional<LongRange> optTimeInterval = entry.getValue();
			String withS = inputFiles.size() < 2 ? "" : "s";
			logger.info("Cut / merge {} file{} to {}...", inputFiles.size(), withS, outputFile.getName());
			if (optTimeInterval.isPresent()) {
				LongRange timeInterval = optTimeInterval.get();
				String startDate = DateUtils.formatDate(new Date(timeInterval.getMinimumLong()));
				String endDate = DateUtils.formatDate(new Date(timeInterval.getMaximumLong()));
				logger.info("Time interval : from {} to {}...", startDate, endDate);
			} else {
				logger.info("No time interval.");
			}

			try {
				mergeProcess.setLogger(logger);
				mergeProcess.setTimeInterval(optTimeInterval);
				mergeProcess.setOutputFilePath(outputFile.getAbsolutePath());
				mergeProcess.run(monitor);

				loadOutputFile(outputFile, monitor);
			} catch (NCException | IOException | BadParameterException e) {
				outputFiles.keySet().forEach(FileUtils::deleteQuietly);
				throw new GIOException("Merge process error : " + e.getMessage(), e);
			} catch (OperationCanceledException e) {
				// Cancel required
				logger.info("Cut / merge process canceled");
				outputFiles.keySet().forEach(FileUtils::deleteQuietly);
				return Status.CANCEL_STATUS;
			}
		}
		return Status.OK_STATUS;
	}

}
