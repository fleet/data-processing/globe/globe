
/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.applynavigation;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.e4.core.di.annotations.Creatable;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.validation.FileValidator;
import fr.ifremer.globe.ui.widget.FileComposite.SelectFileCompositeModel;

/**
 * Model used by the {@link RestoreSessionContextWizard}
 */

@Creatable
public class SelectNviFileCompositeModel implements SelectFileCompositeModel {

	/**
	 * file to be set
	 */
	private WritableFile inputFile;

	public SelectNviFileCompositeModel(WritableFile inputFile) {
		this.inputFile = inputFile;
	}

	@Override
	public WritableFile getSelectedFile() {
		return inputFile;
	}

	@Override
	public String getSelectedFileFilterExtension() {
		IFileService fileService = IFileService.grab();
		return fileService.getExtensions(ContentType.NVI_V2_NETCDF_4).get(0) + ";"
				+ fileService.getExtensions(ContentType.NVI_NETCDF_4).get(0);
	}

	@Override
	public int getFileRequirements() {
		return FileValidator.FILE;
	}

	@Override
	public List<Pair<String, String>> getSelectedFileFilterExtensions() {
		List<Pair<String, String>> result = new LinkedList<>();
		IFileService fileService = IFileService.grab();
		result.addAll(fileService.getFileFilters(ContentType.NVI_V2_NETCDF_4));
		result.addAll(fileService.getFileFilters(ContentType.NVI_NETCDF_4));
		return result;
	}

}