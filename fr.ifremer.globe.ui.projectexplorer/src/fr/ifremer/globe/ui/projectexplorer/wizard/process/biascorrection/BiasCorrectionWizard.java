package fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.projectexplorer.wizard.TimeIntervalParamPage;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

public class BiasCorrectionWizard extends Wizard {
	/** Model */
	protected BiasCorrectionWizardModel model;

	/** Pages */
	protected SelectInputFilesPage selectInputFilesPage;
	protected BiasCorrectionParamMain mainPage;
	protected TimeIntervalParamPage timeIntervalParamPage;
	protected SelectOutputParametersPage selectOutputParametersPage;

	public BiasCorrectionWizard(BiasCorrectionWizardModel model) {
		super();
		this.model = model;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(model);
		addPage(selectInputFilesPage);
		mainPage = new BiasCorrectionParamMain(model);
		addPage(mainPage);
		timeIntervalParamPage = new TimeIntervalParamPage(model.getTimeIntervalParameters());
		addPage(timeIntervalParamPage);
		selectOutputParametersPage = new SelectOutputParametersPage(model);
		addPage(selectOutputParametersPage);
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		mainPage.saveData();
		model.getBiasCorrectionParameters().save();
		timeIntervalParamPage.saveData();
		model.getTimeIntervalParameters().save();

		return selectInputFilesPage.isPageComplete() && selectOutputParametersPage.isPageComplete();
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		return model.getInputFiles().isEmpty() ? selectInputFilesPage : mainPage;
	}

}
