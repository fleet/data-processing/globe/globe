package fr.ifremer.globe.ui.projectexplorer.wizard.process.depthcorrection;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.TreeMap;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.ChartSeriesUtils;
import fr.ifremer.globe.core.model.chart.impl.SimpleChartSeries;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.tide.TideType;
import fr.ifremer.globe.core.processes.depthcorrection.CorrectionFileUtils;
import fr.ifremer.globe.core.processes.depthcorrection.DepthCorrectionParameters;
import fr.ifremer.globe.core.processes.depthcorrection.DepthCorrectionSource;
import fr.ifremer.globe.core.processes.depthcorrection.DepthCorrectionType;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.LongLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.environment.TideLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.platform.DynamicDraughtLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.javafxchart.FXChart;
import fr.ifremer.globe.ui.javafxchart.utils.FXChartException;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.widget.CheckBoxGroup;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;
import javafx.scene.paint.Color;
import swing2swt.layout.FlowLayout;

/**
 * Page to select correction file.
 */
public class DepthCorrectionSelectCorrFilePage extends WizardPage {
	protected Logger logger = LoggerFactory.getLogger(DepthCorrectionSelectCorrFilePage.class);

	/** Properties **/
	private static GColor TIDE_COLOR = new GColor(Color.DODGERBLUE.toString());
	private static GColor DRAUGHT_COLOR = GColor.ORANGE;
	private static GColor PLATFORM_COLOR = GColor.RED;

	private static String TIDE_LEGEND = "Tide";
	private static String DRAUGHT_LEGEND = "Delta draught";
	private static String PLATFORM_LEGEND = "Platform elevation";

	private DepthCorrectionWizardModel wizardModel;

	private FileFieldEditor tideFileFieldEditor;
	private FileFieldEditor elevationFileFieldEditor;
	private FileFieldEditor draughtFileFieldEditor;
	DepthCorrectionSource selectedTideCorrectionSource = DepthCorrectionSource.NONE;
	DepthCorrectionSource selectedDraughtCorrectionSource = DepthCorrectionSource.NONE;

	private FXChart chart;
	LegendComposite tideLegend;
	LegendComposite draughtLegend;
	LegendComposite platformElevationLegend;

	private Spinner nominalWaterLineSpinner;
	private Label nominalWaterLineLabel;
	private Composite compositeTideFileSelection;
	private Composite compositeElevationFileSelection;
	private Composite compositeDraughtFileSelection;

	private int updatingControlCounter = 0;
	private boolean isCorrectionValid = false;

	/**
	 * Chart model
	 */
	List<SimpleChartSeries> inputTideSeries;
	List<SimpleChartSeries> zerosTideSeries;
	List<SimpleChartSeries> inputDraughtSeries;
	List<SimpleChartSeries> zerosDraughtSeries;

	/**
	 * Constructor
	 */
	public DepthCorrectionSelectCorrFilePage(DepthCorrectionWizardModel depthCorrectionWizardModel) {
		super("");

		this.wizardModel = depthCorrectionWizardModel;
		setTitle("Configure depth corrections");
		setDescription("Select the correction files (.ttb .txt .dat) to apply.");
		wizardModel.getInputFiles().addChangeListener(e -> onInputFilesChanged());
	}

	/**
	 * Hook all listeners before displaying this composite.
	 */
	public void postInitialization() {
		lockChartUpdate();
		loadPreferences();
		onTideSourceChanged(DepthCorrectionSource.NONE);
		onDraughtSourceChanged(DepthCorrectionSource.NONE);
		onInputFilesChanged();
		unlockChartUpdate();
		updateChart();
	}

	@Override
	public boolean isPageComplete() {
		boolean isTypeValid = (wizardModel.getTideCorrectionSource() == DepthCorrectionSource.NONE)
				|| (wizardModel.getTideCorrectionSource() == DepthCorrectionSource.RESET)
				|| (wizardModel.getTideType() != TideType.UNKNWON);
		boolean hasCorrection = (wizardModel.getTideCorrectionSource() != DepthCorrectionSource.NONE)
				|| (wizardModel.getDraughtCorrectionSource() != DepthCorrectionSource.NONE);
		return hasCorrection && isTypeValid && isCorrectionValid;
	}

	private void setCorrectionValid(boolean isValid) {
		isCorrectionValid = isValid;
		setPageComplete(isPageComplete());
	}

	private void setTideType(TideType type) {
		wizardModel.setTideType(type);
		setPageComplete(isPageComplete());
	}

	/**
	 * Creates the graphical interface.
	 */
	@Override
	public void createControl(Composite parent) {
		Composite mainComposite = new Composite(parent, SWT.NONE);

		// set main composite layout
		mainComposite.setLayout(new GridLayout(1, false));

		////////////////////////////////////////
		// add tide corrections group
		CheckBoxGroup grpTideCorrections = new CheckBoxGroup(mainComposite, SWT.NONE);
		grpTideCorrections.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpTideCorrections.setText("Tide correction");
		grpTideCorrections.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				javafx.application.Platform.runLater(() -> {
					onTideCorrectionChanged(grpTideCorrections.getSelection());
				});
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		// set tide correction composite layout
		Composite tideContainer = grpTideCorrections.getContent();
		tideContainer.setLayout(new GridLayout(1, false));

		// tide type
		Group grpTideType = new Group(tideContainer, SWT.NONE);
		grpTideType.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpTideType.setText("Type");
		grpTideType.setLayout(new GridLayout(3, false));

		Button measuredTideRadioButton = new Button(grpTideType, SWT.RADIO);
		measuredTideRadioButton.setText("Measured");
		measuredTideRadioButton.addListener(SWT.Selection, e -> setTideType(TideType.MEASURED));
		Button predictedTideRadioButton = new Button(grpTideType, SWT.RADIO);
		predictedTideRadioButton.setText("Predicted");
		predictedTideRadioButton.addListener(SWT.Selection, e -> setTideType(TideType.PREDICTED));
		Button gpsTideRadioButton = new Button(grpTideType, SWT.RADIO);
		gpsTideRadioButton.setText("GPS");
		gpsTideRadioButton.addListener(SWT.Selection, e -> setTideType(TideType.GPS));

		Group grpTideSource = new Group(tideContainer, SWT.NONE);
		grpTideSource.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpTideSource.setText("Source");
		grpTideSource.setLayout(new GridLayout(1, false));

		// tide file selection
		Button tideFileRadioButton = new Button(grpTideSource, SWT.RADIO);
		tideFileRadioButton.setText("Tide file (Gauge observations / Prediction)");
		tideFileRadioButton.addListener(SWT.Selection, e -> onTideSourceChanged(DepthCorrectionSource.CORRECTION_FILE));

		compositeTideFileSelection = new Composite(grpTideSource, SWT.NONE);
		GridData gd_compositeTideFileSelection = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_compositeTideFileSelection.horizontalIndent = 20;
		compositeTideFileSelection.setLayoutData(gd_compositeTideFileSelection);
		tideFileFieldEditor = new FileFieldEditor(DepthCorrectionParameters.PROPERTY_TIDE_FILENAME, "",
				compositeTideFileSelection);
		tideFileFieldEditor.setFileExtensions(CorrectionFileUtils.getFilterExtensions());
		Text tideFileText = tideFileFieldEditor.getTextControl(compositeTideFileSelection);
		tideFileText.addListener(SWT.Modify, e -> onFileChanged());

		// elevation file selection
		Button elevationFileRadioButton = new Button(grpTideSource, SWT.RADIO);
		elevationFileRadioButton.setText("Plateform Elevation file (Computed from GPS data)");
		elevationFileRadioButton.addListener(SWT.Selection,
				e -> onTideSourceChanged(DepthCorrectionSource.PLATFORM_ELEVATION_FILE));

		Composite elevationModelGrp = new Composite(grpTideSource, SWT.NONE);
		GridData gd_elevationModelGrp = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_elevationModelGrp.horizontalIndent = 20;
		elevationModelGrp.setLayoutData(gd_elevationModelGrp);
		// set elevation group layout
		GridLayout gl_elevationModelGrp = new GridLayout(2, false);
		gl_elevationModelGrp.marginWidth = 0;
		gl_elevationModelGrp.marginHeight = 0;
		elevationModelGrp.setLayout(gl_elevationModelGrp);

		compositeElevationFileSelection = new Composite(elevationModelGrp, SWT.NONE);
		compositeElevationFileSelection.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		elevationFileFieldEditor = new FileFieldEditor(DepthCorrectionParameters.PROPERTY_PLATFORM_ELEVATION_FILENAME,
				"", compositeElevationFileSelection);
		elevationFileFieldEditor.setFileExtensions(CorrectionFileUtils.getFilterExtensions());
		Text elevationFileText = elevationFileFieldEditor.getTextControl(compositeElevationFileSelection);
		elevationFileText.addListener(SWT.Modify, e -> onFileChanged());

		// nominal water line
		nominalWaterLineSpinner = new Spinner(elevationModelGrp, SWT.BORDER);
		GridData gd_nominalWaterLineSpinner = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_nominalWaterLineSpinner.horizontalIndent = 8;
		nominalWaterLineSpinner.setLayoutData(gd_nominalWaterLineSpinner);
		nominalWaterLineSpinner.setDigits(2);
		nominalWaterLineSpinner.setMaximum(10000);
		nominalWaterLineSpinner.setMinimum(-10000);
		nominalWaterLineSpinner.addListener(SWT.Modify, e -> onNominalWaterLineChanged());

		nominalWaterLineLabel = new Label(elevationModelGrp, SWT.NONE);
		nominalWaterLineLabel.setText(
				"Sounder installation (nominal) waterline coordinate (positive when waterline is below reference point)");

		// reset tide
		Button resetTideRadioButton = new Button(grpTideSource, SWT.RADIO);
		resetTideRadioButton.setText("Reset tide correction");
		resetTideRadioButton.addListener(SWT.Selection, e -> onTideSourceChanged(DepthCorrectionSource.RESET));

		// deactivate tide group
		grpTideCorrections.setSelection(false);

		///////////////////////////////////
		// add draught corrections group
		CheckBoxGroup grpDraughtCorrections = new CheckBoxGroup(mainComposite, SWT.NONE);
		grpDraughtCorrections.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpDraughtCorrections.setText("Draught correction");
		grpDraughtCorrections.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				javafx.application.Platform.runLater(() -> {
					onDraughtCorrectionChanged(grpDraughtCorrections.getSelection());
				});
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		// set draught correction composite layout
		Composite draughtContainer = grpDraughtCorrections.getContent();
		draughtContainer.setLayout(new GridLayout(1, false));

		// draught file selection
		Button draughtFileRadioButton = new Button(draughtContainer, SWT.RADIO);
		draughtFileRadioButton.setText("Draught correction file");
		draughtFileRadioButton.addListener(SWT.Selection,
				e -> onDraughtSourceChanged(DepthCorrectionSource.CORRECTION_FILE));

		compositeDraughtFileSelection = new Composite(draughtContainer, SWT.NONE);
		GridData gd_compositeDraughtFileSelection = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_compositeDraughtFileSelection.horizontalIndent = 20;
		compositeDraughtFileSelection.setLayoutData(gd_compositeDraughtFileSelection);
		draughtFileFieldEditor = new FileFieldEditor(DepthCorrectionParameters.PROPERTY_DYNAMIC_DRAUGHT_FILENAME, "",
				compositeDraughtFileSelection);
		draughtFileFieldEditor.setFileExtensions(CorrectionFileUtils.getFilterExtensions());
		Text draughtFileText = draughtFileFieldEditor.getTextControl(compositeDraughtFileSelection);
		draughtFileText.addListener(SWT.Modify, e -> onFileChanged());

		/// reset draught
		Button resetDraughtRadioButton = new Button(draughtContainer, SWT.RADIO);
		resetDraughtRadioButton.setText("Reset draught correction");
		resetDraughtRadioButton.addListener(SWT.Selection, e -> onDraughtSourceChanged(DepthCorrectionSource.RESET));

		// deactivate draught group
		grpDraughtCorrections.setSelection(false);

		/////////////////////
		// chart
		Composite compositeChart = new Composite(mainComposite, SWT.NONE);
		compositeChart.setLayout(new FillLayout(SWT.VERTICAL));
		compositeChart.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		try {
			chart = new FXChart(compositeChart, "Correction data");
			DateFormat format = new SimpleDateFormat("dd/MM/yy HH:mm");
			format.setTimeZone(TimeZone.getTimeZone("GMT"));
			chart.setXAxisLabelFormatter(d -> format.format(new Date(d.longValue())));
			chart.setVerticalZoomEnabled(false);
		} catch (FXChartException | IOException e) {
			logger.error(e.getMessage(), e);
		}

		Composite compositeLegend = new Composite(mainComposite, SWT.NONE);
		compositeLegend.setLayout(new FlowLayout());
		compositeLegend.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		tideLegend = new LegendComposite(compositeLegend, TIDE_LEGEND, TIDE_COLOR, chart);
		draughtLegend = new LegendComposite(compositeLegend, DRAUGHT_LEGEND, DRAUGHT_COLOR, chart);
		platformElevationLegend = new LegendComposite(compositeLegend, PLATFORM_LEGEND, PLATFORM_COLOR, chart);

		setControl(mainComposite);
		setCorrectionValid(false);
		postInitialization();
	}

	private void lockChartUpdate() {
		updatingControlCounter++;
	}

	private void unlockChartUpdate() {
		updatingControlCounter--;
	}

	private boolean isChartUpdateLocked() {
		return updatingControlCounter > 0;
	}

	private void onTideCorrectionChanged(boolean selected) {
		DepthCorrectionSource correctionSource = DepthCorrectionSource.NONE;
		if (selected) {
			correctionSource = selectedTideCorrectionSource;
		}
		onTideSourceChanged(correctionSource);
	}

	private void onTideSourceChanged(DepthCorrectionSource correctionSource) {
		if (correctionSource != DepthCorrectionSource.NONE)
			selectedTideCorrectionSource = correctionSource;
		DepthCorrectionSource previousSource = wizardModel.getTideCorrectionSource();
		wizardModel.setTideCorrectionSource(correctionSource);
		// Correction file
		tideFileFieldEditor.setEnabled(correctionSource == DepthCorrectionSource.CORRECTION_FILE,
				compositeTideFileSelection);
		// Platform elevation
		nominalWaterLineSpinner.setEnabled(correctionSource == DepthCorrectionSource.PLATFORM_ELEVATION_FILE);
		nominalWaterLineLabel.setEnabled(correctionSource == DepthCorrectionSource.PLATFORM_ELEVATION_FILE);

		elevationFileFieldEditor.setEnabled(correctionSource == DepthCorrectionSource.PLATFORM_ELEVATION_FILE,
				compositeElevationFileSelection);
		platformElevationLegend.setVisible(correctionSource == DepthCorrectionSource.PLATFORM_ELEVATION_FILE);
		platformElevationLegend.getParent().layout();
		if (previousSource != correctionSource)
			updateChart();
	}

	private void onDraughtCorrectionChanged(boolean selected) {
		DepthCorrectionSource correctionSource = DepthCorrectionSource.NONE;
		if (selected) {
			correctionSource = selectedDraughtCorrectionSource;
		}
		onDraughtSourceChanged(correctionSource);
	}

	private void onDraughtSourceChanged(DepthCorrectionSource correctionSource) {
		if (correctionSource != DepthCorrectionSource.NONE)
			selectedDraughtCorrectionSource = correctionSource;
		DepthCorrectionSource previousSource = wizardModel.getDraughtCorrectionSource();
		wizardModel.setDraughtCorrectionSource(correctionSource);
		// correction file
		draughtFileFieldEditor.setEnabled(correctionSource == DepthCorrectionSource.CORRECTION_FILE,
				compositeDraughtFileSelection);
		if (previousSource != correctionSource)
			updateChart();

	}

	private void onNominalWaterLineChanged() {
		double nominalWaterLine = nominalWaterLineSpinner.getSelection()
				/ Math.pow(10, nominalWaterLineSpinner.getDigits());
		wizardModel.getNominalWaterLine().set((float) nominalWaterLine);
		updateChart();
	}

	private void onInputFilesChanged() {
		loadInputSeries();
		// update controls
		lockChartUpdate();
		long nominalWaterLineSelection = Math
				.round(wizardModel.getNominalWaterLine().get() * Math.pow(10, nominalWaterLineSpinner.getDigits()));
		nominalWaterLineSpinner.setSelection((int) nominalWaterLineSelection);
		unlockChartUpdate();
		updateChart();
	}

	public void loadInputSeries() {
		inputTideSeries = new ArrayList<>();
		zerosTideSeries = new ArrayList<>();
		inputDraughtSeries = new ArrayList<>();
		zerosDraughtSeries = new ArrayList<>();
		for (File file : wizardModel.getInputFiles()) {
			IFileService fileService = IFileService.grab();
			IDataContainerFactory dataContainerFactory = IDataContainerFactory.grab();
			IDataContainerOwner owner = IDataContainerOwner.generate("DetectionCorrectionSelectCorrFilePage", true);

			Optional<IFileInfo> infoStore = fileService.getFileInfo(file.getAbsolutePath());
			if (infoStore.isPresent() && infoStore.get().getContentType().isSounderNcInfo()) {
				ISounderNcInfo infoFile = (ISounderNcInfo) infoStore.get();
				try (ISounderDataContainerToken token = dataContainerFactory.book(infoFile, owner)) {
					SounderDataContainer container = token.getDataContainer();
					LongLoadableLayer1D tideTimeLayer = container.hasLayer(TideLayers.TIME)
							? container.getLayer(TideLayers.TIME)
							: container.getLayer(BeamGroup1Layers.PING_TIME); // mbg doesn't have specific tide time
					FloatLoadableLayer1D tide = container.getLayer(TideLayers.TIDE_INDICATIVE);
					LongLoadableLayer1D draughtTimeLayer = container.hasLayer(DynamicDraughtLayers.TIME)
							? container.getLayer(DynamicDraughtLayers.TIME)
							: container.getLayer(BeamGroup1Layers.PING_TIME); // mbg doesn't have specific draught time
					FloatLoadableLayer1D draught = container.getLayer(DynamicDraughtLayers.DELTA_DRAUGHT);

					// retrieve input file tide data
					List<double[]> tideData = new ArrayList<>();
					List<double[]> zeroTideData = new ArrayList<>();
					long tideLength = tideTimeLayer.getDimensions()[0];
					for (int i = 0; i < tideLength; i++) {
						tideData.add(new double[] { DateUtils.nanoSecondToMilli(tideTimeLayer.get(i)), tide.get(i) });
						zeroTideData.add(new double[] { DateUtils.nanoSecondToMilli(tideTimeLayer.get(i)), 0 });
					}
					inputTideSeries.add(new SimpleChartSeries(TIDE_LEGEND, TIDE_COLOR, tideData));
					zerosTideSeries.add(new SimpleChartSeries(TIDE_LEGEND, TIDE_COLOR, zeroTideData));

					// retrieve input file draught data
					List<double[]> draughtData = new ArrayList<>();
					List<double[]> zeroDraughtData = new ArrayList<>();
					long draughtLength = draughtTimeLayer.getDimensions()[0];
					for (int i = 0; i < draughtLength; i++) {
						draughtData.add(
								new double[] { DateUtils.nanoSecondToMilli(draughtTimeLayer.get(i)), draught.get(i) });
						zeroDraughtData.add(new double[] { DateUtils.nanoSecondToMilli(draughtTimeLayer.get(i)), 0 });
					}
					inputDraughtSeries.add(new SimpleChartSeries(DRAUGHT_LEGEND, DRAUGHT_COLOR, draughtData));
					zerosDraughtSeries.add(new SimpleChartSeries(DRAUGHT_LEGEND, DRAUGHT_COLOR, zeroDraughtData));

				} catch (GException e) {
					Messages.openErrorMessage("Error : file not available. ", e);
				}
			}
		}
	}

	public void updateChart() {
		if (isChartUpdateLocked())
			return;

		setErrorMessage(null);
		setCorrectionValid(true);

		List<SimpleChartSeries> tideSeries = new ArrayList<>();
		List<SimpleChartSeries> draughtSeries = new ArrayList<>();
		List<SimpleChartSeries> plateformSeries = new ArrayList<>();

		// load draught correction
		switch (wizardModel.getDraughtCorrectionSource()) {
		case NONE:
			draughtSeries.addAll(inputDraughtSeries);
			break;
		case CORRECTION_FILE:
			try {
				Optional<SimpleChartSeries> correction = getCorrectionFileChartSeries(
						draughtFileFieldEditor.getStringValue(), DepthCorrectionType.DRAUGHT);
				if (correction.isPresent()) {
					for (SimpleChartSeries inputZeros : zerosDraughtSeries) {
						SimpleChartSeries draught = new SimpleChartSeries(inputZeros, DRAUGHT_LEGEND, DRAUGHT_COLOR);
						ChartSeriesUtils.add(draught, correction.get());
						draughtSeries.add(draught);
					}
				} else {
					draughtSeries.addAll(zerosDraughtSeries);
				}
			} catch (GIOException e) {
				setErrorMessage(e.getMessage());
				setCorrectionValid(false);
				logger.error("Error reading ttb file : " + draughtFileFieldEditor.getStringValue(), e);
				draughtSeries.addAll(zerosDraughtSeries);
			}
			break;
		case RESET:
		default:
			draughtSeries.addAll(zerosDraughtSeries);
			break;
		}

		// load tide correction
		switch (wizardModel.getTideCorrectionSource()) {
		case NONE:
			tideSeries.addAll(inputTideSeries);
			break;
		case CORRECTION_FILE:
			try {
				Optional<SimpleChartSeries> correction = getCorrectionFileChartSeries(
						tideFileFieldEditor.getStringValue(), DepthCorrectionType.TIDE);
				if (correction.isPresent()) {
					for (SimpleChartSeries inputZeros : zerosTideSeries) {
						SimpleChartSeries tide = new SimpleChartSeries(inputZeros, TIDE_LEGEND, TIDE_COLOR);
						ChartSeriesUtils.add(tide, correction.get());
						tideSeries.add(tide);
					}
				} else {
					tideSeries.addAll(zerosTideSeries);
				}
			} catch (GIOException e) {
				setErrorMessage(e.getMessage());
				setCorrectionValid(false);
				logger.error("Error reading ttb file : " + tideFileFieldEditor.getStringValue(), e);
				tideSeries.addAll(zerosTideSeries);
			}
			break;
		case PLATFORM_ELEVATION_FILE:
			try {
				Optional<SimpleChartSeries> elevation = getCorrectionFileChartSeries(
						elevationFileFieldEditor.getStringValue(), DepthCorrectionType.NOMINAL_WATERLINE);
				if (elevation.isPresent()) {
					for (int i = 0; i < zerosTideSeries.size(); i++) {
						SimpleChartSeries tide = new SimpleChartSeries(zerosTideSeries.get(i), TIDE_LEGEND, TIDE_COLOR);
						// apply elevation
						ChartSeriesUtils.add(tide, elevation.get());
						plateformSeries.add(new SimpleChartSeries(tide, PLATFORM_LEGEND, PLATFORM_COLOR));
						// remove nominal water line
						ChartSeriesUtils.applyOffset(tide, -wizardModel.getNominalWaterLine().get());
						// remove draught correction
						ChartSeriesUtils.minus(tide, draughtSeries.get(i));
						tideSeries.add(tide);
					}
				} else {
					tideSeries.addAll(zerosTideSeries);
				}
			} catch (GIOException e) {
				setErrorMessage(e.getMessage());
				setCorrectionValid(false);
				logger.error("Error reading ttb file : " + elevationFileFieldEditor.getStringValue(), e);
				tideSeries.addAll(zerosTideSeries);
			}
			break;
		case RESET:
			tideSeries.addAll(zerosTideSeries);
			break;
		}

		// Add series data
		List<SimpleChartSeries> allSeries = new ArrayList<>();
		allSeries.addAll(plateformSeries);
		allSeries.addAll(draughtSeries);
		allSeries.addAll(tideSeries);

		chart.setData(allSeries);

		tideLegend.setSeries(tideSeries);
		draughtLegend.setSeries(draughtSeries);
		platformElevationLegend.setSeries(plateformSeries);
	}

	public Optional<SimpleChartSeries> getCorrectionFileChartSeries(String filename, DepthCorrectionType type)
			throws GIOException {
		if (filename.isEmpty()) {
			// empty series
			setErrorMessage("Select the correction file (.ttb .txt .dat) to apply.");
			setCorrectionValid(false);
			return Optional.empty();
		}
		// read correction file
		TreeMap<Instant, Double> correctionValuesByDate = CorrectionFileUtils.read(filename, type);

		// check if correction dates match with input files
		Instant minDateFile = Instant.MAX;
		Instant maxDateFile = Instant.MIN;
		for (File file : wizardModel.getInputFiles()) {
			IFileService fileService = IFileService.grab();
			Optional<IFileInfo> infoStore = fileService.getFileInfo(file.getAbsolutePath());
			if (infoStore.isPresent() && infoStore.get().getContentType().isSounderNcInfo()) {
				ISounderNcInfo info = (ISounderNcInfo) infoStore.get();
				minDateFile = minDateFile.isBefore(info.getFirstPingDate().toInstant()) ? minDateFile :  info.getFirstPingDate().toInstant();
				maxDateFile = maxDateFile.isAfter(info.getLastPingDate().toInstant()) ? maxDateFile : info.getLastPingDate().toInstant() ;
			}
		}
		if (type == DepthCorrectionType.DRAUGHT) {
			// extend draught values
			if (minDateFile.isBefore(correctionValuesByDate.firstKey()))
				correctionValuesByDate.put(minDateFile, correctionValuesByDate.firstEntry().getValue());
			if (maxDateFile.isAfter(correctionValuesByDate.lastKey()))
				correctionValuesByDate.put(maxDateFile, correctionValuesByDate.lastEntry().getValue());
		} else if (maxDateFile.isBefore(correctionValuesByDate.firstKey()) || minDateFile.isAfter(correctionValuesByDate.lastKey())) {
			setErrorMessage("Correction file dates don't match with input files.");
			setCorrectionValid(false);
			return Optional.empty();
		}
		// fill series
		List<double[]> data = correctionValuesByDate.entrySet().stream()
				.map(entry -> new double[] { entry.getKey().toEpochMilli(), entry.getValue() }).toList();
		SimpleChartSeries series = new SimpleChartSeries(TIDE_LEGEND, TIDE_COLOR, data);
		return Optional.of(series);
	}

	/**
	 * Method called when the selected file changed.
	 */
	public void onFileChanged() {
		updateChart();
	}

	private void loadFileFieldEditor(FileFieldEditor editor, String fileName) {
		File path = null;
		if (fileName != null && !fileName.isEmpty()) {
			path = new File(fileName).getParentFile();
		} else {
			try {
				URL locationUrl = Platform.getInstanceLocation().getURL();
				URL fileUrl = FileLocator.toFileURL(locationUrl);
				path = new File(fileUrl.getPath());
			} catch (IOException e) {
				logger.error("Error with correction file : {}", e.getMessage(), e);
			}
		}
		editor.setStringValue(fileName);
		editor.setFilterPath(path);
	}

	/**
	 * Loads correction preferences.
	 */
	public void loadPreferences() {
		loadFileFieldEditor(tideFileFieldEditor, wizardModel.getDepthCorrectionParameters()
				.getProperty(DepthCorrectionParameters.PROPERTY_TIDE_FILENAME));
		loadFileFieldEditor(elevationFileFieldEditor, wizardModel.getDepthCorrectionParameters()
				.getProperty(DepthCorrectionParameters.PROPERTY_PLATFORM_ELEVATION_FILENAME));
		loadFileFieldEditor(draughtFileFieldEditor, wizardModel.getDepthCorrectionParameters()
				.getProperty(DepthCorrectionParameters.PROPERTY_DYNAMIC_DRAUGHT_FILENAME));
	}

	/**
	 * Saves correction preferences.
	 */
	public void savePreferences() {
		wizardModel.getDepthCorrectionParameters().setProperty(DepthCorrectionParameters.PROPERTY_TIDE_FILENAME,
				tideFileFieldEditor.getStringValue());
		wizardModel.getDepthCorrectionParameters().setProperty(
				DepthCorrectionParameters.PROPERTY_PLATFORM_ELEVATION_FILENAME,
				elevationFileFieldEditor.getStringValue());
		Float verticalOffset = wizardModel.getNominalWaterLine().getValue();
		wizardModel.getDepthCorrectionParameters()
				.setProperty(DepthCorrectionParameters.PROPERTY_PLATFORM_ELEVATION_VOFFSET, verticalOffset.toString());
		wizardModel.getDepthCorrectionParameters().setProperty(
				DepthCorrectionParameters.PROPERTY_DYNAMIC_DRAUGHT_FILENAME, draughtFileFieldEditor.getStringValue());
		wizardModel.getDepthCorrectionParameters().setProperty(
				DepthCorrectionParameters.PROPERTY_TIDE_CORRECTION_SOURCE,
				wizardModel.getTideCorrectionSource().name());
		wizardModel.getDepthCorrectionParameters().setProperty(
				DepthCorrectionParameters.PROPERTY_DRAUGHT_CORRECTION_SOURCE,
				wizardModel.getDraughtCorrectionSource().name());

	}

}
