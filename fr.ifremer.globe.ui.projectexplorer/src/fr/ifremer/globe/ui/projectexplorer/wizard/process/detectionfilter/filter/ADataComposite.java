/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.filter;

import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters.Comparator;

/**
 * Swt Composite to edit an attribute.
 */
public abstract class ADataComposite extends Composite {

	/**
	 * Constructor.
	 */
	public ADataComposite(Composite parent, int style) {
		super(parent, style);
		setVisible(false);
	}

	/** Ask to update the parameters with the edition. */
	abstract public void updateParameters(SingleFilterParameters parameters);

	/** Ask to consider a new comparator. */
	public void setComparator(Comparator comparator) {
	}

	/** Allowed Comparators */
	public ViewerFilter getComparatorFilter() {
		return null;
	}

	/** Comparator by default */
	public Comparator getDefaultComparator() {
		return Comparator.between;
	}

}
