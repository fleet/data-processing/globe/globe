/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.filter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.sounder.datacontainer.DetectionType;
import fr.ifremer.globe.core.processes.detectionfilter.Attributes;
import fr.ifremer.globe.core.processes.detectionfilter.IAttributeProvider;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.beam_group1.BathymetryLayers;

/**
 *
 */
public class AllFiltersComposite extends Composite {
	protected Map<Class<?>, ADataComposite> filtersComposite = new HashMap<>();
	protected EnumFilterComposite detectionComposite;
	protected DateTimeFilterComposite timeComposite;
	protected IAttributeProvider attributeProvider;

	/**
	 * Constructor.
	 */
	public AllFiltersComposite(IAttributeProvider attributeProvider, Composite parent, int style) {
		super(parent, style);
		this.attributeProvider = attributeProvider;
		setLayout(new StackLayout());
		filtersComposite.put(Boolean.class, new BooleanFilterComposite(this, style));
		filtersComposite.put(Byte.class, new NumericFilterComposite(this, style, "-###0"));
		filtersComposite.put(Double.class, new NumericFilterComposite(this, style, "-###,###,##0.#########"));
		filtersComposite.put(Float.class, new NumericFilterComposite(this, style, "-###,###,##0.######"));
		filtersComposite.put(Integer.class, new NumericFilterComposite(this, style, "-###,###,##0"));
		filtersComposite.put(Long.class, new NumericFilterComposite(this, style, "-###,###,###,##0"));
		filtersComposite.put(Short.class, new NumericFilterComposite(this, style, "-##,##0"));
		filtersComposite.put(Date.class, new DateTimeFilterComposite(this, style));

		String[] detectionLabels = { //
				DetectionType.AMPLITUDE.toString(), //
				DetectionType.PHASE.toString(), //
				DetectionType.INVALID.toString() //
		};
		String[] detectionValues = { //
				String.valueOf(DetectionType.AMPLITUDE.get()), //
				String.valueOf(DetectionType.PHASE.get()), //
				String.valueOf(DetectionType.INVALID.get()) //
		};
		detectionComposite = new EnumFilterComposite(this, SWT.NONE, detectionLabels, detectionValues);
	}

	/**
	 * @return the current displayed AFilterComposite.
	 */
	public ADataComposite getTopFilterComposite() {
		return (ADataComposite) getLayout().topControl;
	}

	/**
	 * Consider a new Attribute selection.
	 */
	public ADataComposite show(PredefinedLayers<?> selection) {
		if (getTopFilterComposite() != null) {
			getTopFilterComposite().setVisible(false);
		}
		if (BathymetryLayers.DETECTION_TYPE == selection || Attributes.AMPLITUDE_PHASE_DETECTION == selection) {
			getLayout().topControl = detectionComposite;
		} else {
			getLayout().topControl = filtersComposite.get(attributeProvider.retrieveFilterAttributeType(selection));
		}
		layout();
		return getTopFilterComposite();
	}

	/**
	 * Follow the link.
	 *
	 * @see org.eclipse.swt.widgets.Composite#getLayout()
	 */
	@Override
	public StackLayout getLayout() {
		return (StackLayout) super.getLayout();
	}
}
