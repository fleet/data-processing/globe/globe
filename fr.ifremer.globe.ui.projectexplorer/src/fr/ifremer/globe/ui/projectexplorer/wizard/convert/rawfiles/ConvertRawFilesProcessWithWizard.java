/**
 * Globe - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.convert.rawfiles;

import static fr.ifremer.globe.ui.projectexplorer.wizard.convert.rawfiles.ConvertRawFilesWizardModel.MBG_EXTENSION;
import static fr.ifremer.globe.ui.projectexplorer.wizard.convert.rawfiles.ConvertRawFilesWizardModel.NVI_EXTENSION;
import static fr.ifremer.globe.ui.projectexplorer.wizard.convert.rawfiles.ConvertRawFilesWizardModel.XSF_EXTENSION;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.io.nvi.service.INviWriter.SamplingMode;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.SampledNavigationData;
import fr.ifremer.globe.core.runtime.gws.GwsServiceAgent;
import fr.ifremer.globe.core.runtime.gws.param.ExportToMbgParams;
import fr.ifremer.globe.core.runtime.gws.param.ExportToNviParams;
import fr.ifremer.globe.core.runtime.gws.param.ExportToXsfParams;
import fr.ifremer.globe.core.runtime.gws.param.MigrateXsfParams;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.utils.cache.WorkspaceCache;
import fr.ifremer.globe.utils.exception.GException;
import jakarta.inject.Inject;

/**
 * Convert raw files (.all/.s7k...) to sounder files (mbg,xsf...) process with wizard
 */
public class ConvertRawFilesProcessWithWizard extends DefaultProcessWithWizard<ConvertRawFilesWizardModel> {

	@Inject
	private GwsServiceAgent gwsServiceAgent;

	private static final String TITLE = "Convert raw files (.all/.s7k...) to NetCDF files (mbg,xsf...)";

	/**
	 * Constructor
	 */
	public ConvertRawFilesProcessWithWizard(Shell shell) {
		super(TITLE, ConvertRawFilesWizardModel::new, ConvertRawFilesWizard::new,
				(m, w) -> new WizardDialogWithResetButton(shell, w, m));
		outputFileNameComputer = new XsfFileNameComputer(model);

		// Restore the model in a json file
		this.model.load(gainModelFile());

	}

	/**
	 * @see fr.ifremer.globe.utils.process.GProcess#compute(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) {
		// Saving the model in a json file
		this.model.save(gainModelFile());

		IStatus result = Status.OK_STATUS;
		List<File> inputFiles = model.getInputFiles().asList();
		this.logger = logger;
		logger.info("Beginning conversion of {} files", inputFiles.size());

		SubMonitor subMonitor = SubMonitor.convert(monitor, inputFiles.size() + 1);
		var xsfOutputFiles = new ArrayList<File>();
		for (File inputFile : model.getInputFiles().asList()) {
			processConversions(inputFile, subMonitor.split(1)).ifPresent(xsfOutputFiles::add);
		}

		// Xsf post process. Launch Pyat Xsf migration
		File xsfReferenceFolder = model.getXsfReferenceFolder().doGetValue();
		if (!xsfOutputFiles.isEmpty() && xsfReferenceFolder != null && xsfReferenceFolder.exists()) {
			result = processXsfMigration(xsfOutputFiles, xsfReferenceFolder, subMonitor.split(1));
		}
		subMonitor.worked(1);

		if (Status.OK_STATUS.equals(result) && !monitor.isCanceled()) {
			for (File xsfOutputFile : xsfOutputFiles) {
				loadOutputFile(xsfOutputFile, new NullProgressMonitor());
			}
		}

		logger.info("Processed {} files", inputFiles.size());
		return result;
	}

	/**
	 * Processes the conversion for one file.
	 *
	 * @return the list of XSF output files
	 */
	protected Optional<File> processConversions(File inputFile, IProgressMonitor monitor) {
		Optional<File> result = Optional.empty();

		int totalWork = 0;
		if (model.getMbgFormat().isTrue())
			totalWork++;
		if (model.getXsfFormat().isTrue())
			totalWork++;
		if (model.getNviFormat().isTrue())
			totalWork++;
		monitor.beginTask("Processing " + inputFile.getName(), totalWork);

		// MBG
		if (!monitor.isCanceled() && model.getMbgFormat().isTrue()) {
			File outputFile = exportToMbg(inputFile, monitor.slice(1));
			loadOutputFile(outputFile, monitor);
		}

		// XSF
		if (!monitor.isCanceled() && model.getXsfFormat().isTrue()) {
			File outputFile = exportToXsf(inputFile, monitor.slice(1));
			if (outputFile.exists()) {
				result = Optional.of(outputFile);
			}
		}

		// NVI
		if (!monitor.isCanceled() && model.getNviFormat().isTrue()) {
			File outputFile = exportToNvi(inputFile, monitor);
			loadOutputFile(outputFile, monitor);
			monitor.worked(1);
		}

		return result;
	}

	/** Launch the XSF migration process for all specified XSF files */
	private IStatus processXsfMigration(List<File> xsfFilesToMigrate, File xsfReferenceFolder,
			IProgressMonitor monitor) {
		try {
			logger.info("Migrating XSF files...");
			MigrateXsfParams params = new MigrateXsfParams(xsfFilesToMigrate, xsfFilesToMigrate, List.of(),
					xsfReferenceFolder, true);
			return gwsServiceAgent.migrateXsf(params, monitor, logger);
		} catch (Exception e) {
			return new Status(IStatus.ERROR, "fr.ifremer.globe.ui.projectexplorer", "Upgrading XSF failed", e);
		}
	}

	/**
	 * Executes the conversion to XSF files.
	 */
	protected File exportToXsf(File input, IProgressMonitor monitor) {
		File result = outputFileNameComputer.compute(input, model, XSF_EXTENSION);
		if (shouldBeProcessed(result, logger)) {
			try {
				ExportToXsfParams params = new ExportToXsfParams(//
						input, //
						result, // Output file
						model.getOverwriteExistingFiles().get(), // overwrite
						model.getXsfKeywords().get(), // xsfKeywords
						model.getXsfLicense().get(), // xsfLicense
						model.getXsfRights().get(), // xsfRights
						model.getXsfSummary().get(), // xsfSummary
						model.getXsfTitle().get(), // xsfTitle
						model.getXSFIgnoreWC().get());
				gwsServiceAgent.exportToXsf(params, monitor, logger);
			} catch (Exception e) {
				logger.error("Error while XSF conversion of {}.", input.getName());
				logger.error("{}", e.getMessage(), e);
			}
		}
		monitor.done();
		return result;
	}

	/**
	 * Executes the conversion to MBG files.
	 */
	protected File exportToMbg(File inputFile, IProgressMonitor monitor) {
		File result = outputFileNameComputer.compute(inputFile, model, MBG_EXTENSION);
		if (shouldBeProcessed(result, logger)) {
			try {
				ExportToMbgParams params = new ExportToMbgParams(//
						inputFile, //
						result, // Output file
						model.getOverwriteExistingFiles().get(), // overwrite
						model.getMbgCdi().get(), model.getMbgReferencePoint().get(), model.getMbgShipName().get(),
						model.getMbgSurveyName().get());
				gwsServiceAgent.exportToMbg(params, monitor, logger);
			} catch (Exception e) {
				logger.error("Error while MBG conversion of {}.", inputFile.getName());
				logger.error("{}", e.getMessage(), e);
			}
		}
		monitor.done();
		return result;
	}

	/**
	 * Execute the conversion to nvi files
	 */
	protected File exportToNvi(File inputFile, IProgressMonitor monitor) {
		logger.info("Converting {} to NVI ", inputFile.getName());

		File result = outputFileNameComputer.compute(inputFile, model, NVI_EXTENSION);
		if (shouldBeProcessed(result, logger)) {
			try {
				// Get sampling mode
				SamplingMode samplingMode = model.getNviSamplingMode().get();
				int samplingValue = samplingMode == SamplingMode.TIME ? model.getNviTimeSamplingValue().intValue()
						: model.getNviSoundingsSamplingValue().intValue();

				IStatus status = doExportToNvi(inputFile, result, samplingMode, samplingValue, monitor, logger);
				if (status.isOK())
					logger.info("Conversion of {} to {} done", inputFile.getName(), result.getName());
				else {
					logger.error("Conversion of {} to {} failed", inputFile.getName(), result.getName());
					if (status.getException() != null)
						logger.error(status.getMessage(), status.getException());
					else
						logger.warn(status.getMessage());
				}
			} catch (Exception e) {
				logger.error("Error while NVI conversion of {} to {} ({})", inputFile.getName(), result.getName(),
						e.getMessage());
				FileUtils.deleteQuietly(result);
			}
		}
		return result;
	}

	private IStatus doExportToNvi(File inputFile, File outputFile, SamplingMode sMode, int sValue,
			IProgressMonitor monitor, Logger logger) {

		try (INavigationData navigationData = getNavigationDataProxy(inputFile).getNavigationData(true)) {
			ExportToNviParams params = new ExportToNviParams(//
					List.of(inputFile), //
					Optional.of(List.of(new SampledNavigationData(navigationData, sMode, sValue))), // navigationData
					Optional.empty(), // navPointsInFiles
					Optional.of(List.of(outputFile)), // ouputFiles
					Optional.of(model.getOverwriteExistingFiles().doGetValue()), // overwrite
					Optional.empty() // whereToLoadOutputFiles
			);
			return gwsServiceAgent.exportToNvi(params, monitor, logger);
		} catch (IOException | GException e) {
			return Status.error("Error when executing service to export navigation to NVI file", e);
		}
	}

	/** Return the json file used to store the model */
	private File gainModelFile() {
		return new File(WorkspaceCache.getPreferencePath(), model.getClass().getSimpleName() + ".json");
	}
}
