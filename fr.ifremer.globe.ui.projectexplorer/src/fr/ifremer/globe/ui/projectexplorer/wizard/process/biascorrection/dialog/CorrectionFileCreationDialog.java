package fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog;

import java.util.Iterator;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.core.processes.biascorrection.model.CommonCorrectionPoint;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList.CorrectionType;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionPoint;

/**
 * Dialog which allows to create a correction file from an empty table.
 */
public class CorrectionFileCreationDialog extends CorrectionFileDialog {

	protected TableViewer tableViewer;
	int style = SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI;

	protected CorrectionList correctionList;

	public CorrectionFileCreationDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(SWT.CLOSE | SWT.RESIZE);

		correctionList = new CorrectionList();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets .Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.timeeditor", "icons/16/movePoint.png"));
		shell.setText("Correction file creation");
		shell.addListener(SWT.Traverse, new Listener() {
			public void handleEvent(Event e) {
				if (e.detail == SWT.TRAVERSE_ESCAPE) {
					e.doit = false;
				}
			}
		});
	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected Control createContents(Composite parent) {
		Control composite = super.createContents(parent);

		getShell().pack();

		return composite;
	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		initComposites(parent);
		initOneLineWarningArea();

		this.tableViewer = initTable(true, this.style, this.tableComposite);

		createTableEditor(tableViewer, correctionList);
		updateTable(tableViewer, correctionList);

		initEditionArea();

		getShell().pack();

		CorrectionFileCreationDialog.this.correctionList.setType(CorrectionType.BIAS);
		return parent;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Export...", true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Initialize edition buttons (add and delete lines).
	 */
	public void initEditionArea() {

		this.addButton = new Button(this.editionComposite, SWT.NONE);
		this.addButton.setToolTipText("Add a new correction point.");
		this.addButton.setText("Add correction");
		this.addButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				CorrectionPoint correctionPoint = new CommonCorrectionPoint();
				if (correctionList.getSize() > 0) {
					correctionPoint.setDate(correctionList.getCorrectionPointList().get(correctionList.getSize() - 1)
							.getDate().plusMillis(1));
				}
				correctionList.addCorrectionPoint(correctionPoint);
				updateTable(tableViewer, correctionList);
			}
		});

		this.deleteButton = new Button(this.editionComposite, SWT.NONE);
		this.deleteButton.setToolTipText("Remove some correction points.");
		this.deleteButton.setText("Remove correction");
		this.deleteButton.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				int[] selectionIndices = CorrectionFileCreationDialog.this.tableViewer.getTable().getSelectionIndices();
				Iterator<CorrectionPoint> correctionPoint = CorrectionFileCreationDialog.this.correctionList
						.getCorrectionPointList().iterator();
				int i = 0;
				while (correctionPoint.hasNext()) {
					correctionPoint.next();
					for (int selectionIndex : selectionIndices) {
						if (i == selectionIndex) {
							correctionPoint.remove();
						}
					}
					i++;
				}
				updateTable(tableViewer, correctionList);
			}
		});
		this.editionComposite.layout(true);
		this.editionComposite.redraw();
	}

	/**
	 * Updates the table content.
	 */

	/**
	 * @return the correctionList
	 */
	public CorrectionList getCorrectionList() {
		return this.correctionList;
	}
}
