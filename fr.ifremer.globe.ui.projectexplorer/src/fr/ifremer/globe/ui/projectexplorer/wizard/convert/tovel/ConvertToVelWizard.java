package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tovel;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Merge wizard
 */
public class ConvertToVelWizard extends Wizard {

	/** Model */
	protected ConvertToVelWizardModel soundVelocityExtractionWizardModel;

	/** Pages */
	protected SelectInputFilesPage selectInputFilesPage;
	protected SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public ConvertToVelWizard(ConvertToVelWizardModel soundVelocityExtractionWizardModel) {
		this.soundVelocityExtractionWizardModel = soundVelocityExtractionWizardModel;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(soundVelocityExtractionWizardModel);
		addPage(selectInputFilesPage);
		selectOutputParametersPage = new SelectOutputParametersPage(soundVelocityExtractionWizardModel);
		addPage(selectOutputParametersPage);
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		return selectInputFilesPage.isPageComplete() && selectOutputParametersPage.isPageComplete();
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		return soundVelocityExtractionWizardModel.getInputFiles().isEmpty() ? selectInputFilesPage
						: selectOutputParametersPage;
	}
}
