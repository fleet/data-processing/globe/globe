package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tottb;

import java.io.File;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.io.tide.TideFileInfo;
import fr.ifremer.globe.core.io.tide.ttb.TtbFileWriter;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Tide conversion process with wizard
 */
public class ConvertToTtbProcessWithWizard extends DefaultProcessWithWizard<ConvertToTtbWizardModel> {

	private Logger logger;

	/**
	 * Constructor
	 */
	public ConvertToTtbProcessWithWizard(Shell shell) {
		super(shell, "Convert to tide files (.ttb)", ConvertToTtbWizardModel::new, ConvertToTtbWizard::new);
	}

	/**
	 * Performs processing.
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		this.logger = logger;
		List<File> inputFiles = model.getInputFiles().asList();
		logger.info("Beginning conversion of {} files", inputFiles.size());

		SubMonitor subMonitor = SubMonitor.convert(monitor, inputFiles.size());
		for (File inputFile : model.getInputFiles().asList()) {
			processConversion(inputFile, subMonitor.split(1));
		}

		logger.info("Conversion of {} files done", inputFiles.size());
		return Status.OK_STATUS;

	}

	/**
	 * Processes the conversion for one file.
	 */
	protected void processConversion(File inputFile, IProgressMonitor monitor) {
		String inputFilePath = inputFile.getPath();
		monitor.setTaskName("Processing : " + inputFilePath);
		logger.info("Converting {} to TTB ", inputFile.getName());

		File outputFile = outputFileNameComputer.compute(inputFile, model, ConvertToTtbWizardModel.TTB_EXTENSION);
		if (shouldBeProcessed(outputFile, logger)) {
			try {
				// read selected file
				monitor.subTask("Loading data ...");
				Optional<IFileInfo> infoStore = IFileService.grab().getFileInfo(inputFilePath);
				if (infoStore.isPresent() && infoStore.get() instanceof TideFileInfo) {
					TtbFileWriter.write(outputFile.getAbsolutePath(), ((TideFileInfo) infoStore.get()).getData());
				} else {
					throw new GIOException("Convert to TTB error, input file not correct: " + inputFile.getName());
				}

				logger.info("Conversion of {} to {} done", inputFile.getName(), outputFile.getName());
				loadOutputFile(outputFile, monitor);
				monitor.worked(1);
			} catch (Exception e) {
				logger.error("Error while converting from {} to {} ({})", inputFile.getName(), outputFile.getName(),
						e.getMessage());
				FileUtils.deleteQuietly(outputFile);
			}
		}
	}

}
