package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tovel;

import java.util.Set;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

/**
 * Handler class to open the process wizard (referenced in fragment.e4xmi).
 */
public class ConvertToVelOpenWizardHandler extends AbstractOpenProcessWizardHandler<ConvertToVelProcessWithWizard> {

	@Override
	protected Set<ContentType> getAcceptedContentType() {
		return ContentType.getSounderRawFileContentTypes();
	}

	@Override
	protected ConvertToVelProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new ConvertToVelProcessWithWizard(shell);
	}

}
