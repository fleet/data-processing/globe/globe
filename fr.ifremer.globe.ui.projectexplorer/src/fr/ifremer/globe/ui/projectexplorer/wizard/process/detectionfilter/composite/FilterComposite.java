package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.composite;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.processes.detectionfilter.IAttributeProvider;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.DataParameters;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters;
import fr.ifremer.globe.core.runtime.job.GProcess;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import swing2swt.layout.BorderLayout;

public class FilterComposite extends Composite implements IFilterLayer {

	private IAttributeProvider attributeProvider;

	private Composite compositeTop;
	private Button btnOr;

	private Composite compositeCenter;
	private ArrayList<SingleFilterComposite> compositeList = new ArrayList<>();

	private Composite compositeBottom;
	private DataComposite dataComposite;
	private Button btnRun;

	public FilterComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new BorderLayout(0, 0));

		compositeTop = new Composite(this, SWT.NONE);
		compositeTop.setLayoutData(BorderLayout.NORTH);
		compositeTop.setLayout(new GridLayout(1, false));

		compositeCenter = new Composite(this, SWT.NONE);
		compositeCenter.setLayoutData(BorderLayout.CENTER);
		compositeCenter.setLayout(new GridLayout(1, false));

		compositeBottom = new Composite(this, SWT.BORDER);
		compositeBottom.setLayoutData(BorderLayout.SOUTH);
		compositeBottom.setLayout(new GridLayout(2, false));

	}

	@Override
	public void remove(SingleFilterComposite layer) {

		compositeList.remove(layer);
		layer.dispose();
		updateLastControl();

		layout(true, true);
	}

	@Override
	public void add() {
		SingleFilterComposite singleFilterComposite = new SingleFilterComposite(compositeCenter, SWT.NONE, this,
				attributeProvider);
		singleFilterComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		compositeList.add(singleFilterComposite);
		updateLastControl();
		layout(true, true);
	}

	private void updateLastControl() {
		int i = 0;
		for (i = 0; i < compositeList.size(); i++) {
			boolean last = false;
			boolean first = false;
			if (i == compositeList.size() - 1) {
				last = true;
			}
			if (i == 0) {
				first = true;
			}
			compositeList.get(i).updateControlButton(last, !first);
		}
	}

	/**
	 * associate the composite with the given sounderdriverinfo
	 */
	public void configure(IAttributeProvider attributeProvider,
			BiFunction<List<SingleFilterParameters>, DataParameters, GProcess> processBuilder) {

		btnOr = new Button(compositeTop, SWT.CHECK);
		btnOr.setText("set if at least one condition is true");
		btnOr.addSelectionListener(SelectionListener
				.widgetSelectedAdapter(e -> dataComposite.getParameters().setCheckAllCriterias(!btnOr.getSelection())));

		this.attributeProvider = attributeProvider;
		dataComposite = new DataComposite(compositeBottom, SWT.NONE, attributeProvider);
		dataComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));

		btnRun = new Button(compositeBottom, SWT.NONE);
		btnRun.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnRun.setEnabled(false);
				for (SingleFilterComposite filter : compositeList) {
					filter.updateParameters();
				}
				dataComposite.updateParameters();

				ArrayList<SingleFilterParameters> filterParameters = new ArrayList<>();
				for (SingleFilterComposite filterComposite : compositeList) {
					filterParameters.add(filterComposite.getParameters());
				}

				// Launch the job
				GProcess process = processBuilder.apply(filterParameters, dataComposite.getParameters());
				process.whenIsDone(event -> btnRun.getDisplay().asyncExec(() -> {
					if (!btnRun.isDisposed())
						btnRun.setEnabled(true);
				}));
				IProcessService.grab().runInBackground(process, false);
				
			}
		});
		btnRun.setText("Go");
		if (attributeProvider != null) {
			add();
		}
	}
}
