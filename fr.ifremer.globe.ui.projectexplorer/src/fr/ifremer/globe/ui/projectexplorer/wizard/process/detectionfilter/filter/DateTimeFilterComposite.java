/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.filter;

import java.util.Date;

import org.eclipse.nebula.widgets.cdatetime.CDT;
import org.eclipse.nebula.widgets.cdatetime.CDateTime;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters.Comparator;
import fr.ifremer.globe.utils.date.DateUtils;

/**
 * Composite used to edit a Date/Time filtering criteria.
 */
public class DateTimeFilterComposite extends ADataComposite {

	private Label labelB;
	protected CDateTime valueA;
	protected CDateTime valueB;

	/**
	 * Constructor.
	 */
	public DateTimeFilterComposite(Composite parent, int style) {
		super(parent, style);

		GridLayout gridLayout = new GridLayout(3, false);
		gridLayout.marginHeight = gridLayout.marginWidth = 0;
		setLayout(gridLayout);

		valueA = new CDateTime(this, CDT.BORDER | CDT.BORDER | CDT.COMPACT | CDT.DROP_DOWN | CDT.DATE_LONG
				| CDT.TIME_SHORT | CDT.CLOCK_12_HOUR);
		valueA.setTimeZone(DateUtils.TIME_ZONE_GMT);
		valueA.setPattern(DateUtils.DATE_TIME_PATTERN);
		valueA.setSelection(new Date());
		GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gridData.widthHint = valueA.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
		valueA.setLayoutData(gridData);

		labelB = new Label(this, SWT.NONE);
		labelB.setText("and");
		labelB.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));

		valueB = new CDateTime(this, CDT.BORDER | CDT.BORDER | CDT.COMPACT | CDT.DROP_DOWN | CDT.DATE_LONG
				| CDT.TIME_SHORT | CDT.CLOCK_12_HOUR);
		valueB.setTimeZone(DateUtils.TIME_ZONE_GMT);
		valueB.setPattern(DateUtils.DATE_TIME_PATTERN);
		valueB.setSelection(new Date());
		gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gridData.widthHint = valueB.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
		valueB.setLayoutData(gridData);
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.filter.ADataComposite#updateParameters(fr.ifremer.globe.imagery.script.parameter.SingleFilterParameters)
	 */
	@Override
	public void updateParameters(SingleFilterParameters parameters) {
		Date value = valueA.getSelection();
		// returns values as nanoseconds
		parameters.setValueA(String.valueOf(DateUtils.milliSecondToNano(value.getTime())));
		if (valueB.isVisible()) {
			value = valueB.getSelection();
			parameters.setValueB(String.valueOf(DateUtils.milliSecondToNano(value.getTime())));
		}
	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.filter.ADataComposite#setComparator(fr.ifremer.globe.imagery.script.parameter.SingleFilterParameters.Comparator)
	 */
	@Override
	public void setComparator(Comparator comparator) {
		if (comparator != SingleFilterParameters.Comparator.between) {
			valueB.setVisible(false);
			labelB.setVisible(false);
		} else {
			valueB.setVisible(true);
			labelB.setVisible(true);

		}
	}
}
