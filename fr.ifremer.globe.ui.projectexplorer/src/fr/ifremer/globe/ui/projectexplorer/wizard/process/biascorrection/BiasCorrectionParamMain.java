package fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.stream.IntStream;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.processes.biascorrection.filereaders.CorrectionFileUtils;
import fr.ifremer.globe.core.processes.biascorrection.model.BiasCorrectionParameters;

public class BiasCorrectionParamMain extends WizardPage implements Listener {

	final IStatus statusOK = new Status(IStatus.OK, "not_used", 0, null, null);

	private BiasCorrectionWizardModel wizardModel;

	// Correction file filed editor
	private FileFieldEditor m_correctionFileFieldEditor;
	private ComboViewer filteringAntennaIndex;
	private Text m_correctionFileText;

	private IStatus m_fileTextStatus = statusOK;


	public BiasCorrectionParamMain(BiasCorrectionWizardModel model) {
		super("");
		setDescription("Enter parameter values for bias correction processing");
		wizardModel = model;
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		createMain(container);
		getShell().pack();
		setControl(container);
		setPageComplete(false);
		loadData();
	}

	protected void createMain(Composite parent) {
		GridLayout layout = new GridLayout();
		layout.verticalSpacing = 10;
		parent.setLayout(layout);
		layout.numColumns = 3;

		m_correctionFileFieldEditor = new FileFieldEditor(BiasCorrectionParameters.PROPERTY_CORRECTION_FILE_NAME,
				"Correction file name", parent);
		m_correctionFileFieldEditor.setFileExtensions(new String[] { CorrectionFileUtils.EXTENSION });
		m_correctionFileText = m_correctionFileFieldEditor.getTextControl(parent);
		m_correctionFileText.addListener(SWT.Modify, this);

		Label antennaLabel = new Label(parent, SWT.NONE);
		antennaLabel.setText("Antenna index");
		filteringAntennaIndex = new ComboViewer(parent);
		GridDataFactory.fillDefaults().span(2, 1).align(SWT.BEGINNING, SWT.CENTER)
				.applyTo(filteringAntennaIndex.getControl());
		filteringAntennaIndex.setContentProvider(ArrayContentProvider.getInstance());
		filteringAntennaIndex.setLabelProvider(LabelProvider
				.createTextProvider(element -> Integer.valueOf(-1).equals(element) ? "All" : element.toString()));

	}

	@Override
	public void handleEvent(Event event) {
		if (event.widget == m_correctionFileText) {
			if (m_correctionFileText.getText().isEmpty()) {
				m_fileTextStatus = new Status(IStatus.ERROR, "not_used", 0, "New file names prefix can't be empty",
						null);
			} else {
				m_fileTextStatus = statusOK;
			}
		}
		applyMostSevere();
	}

	private void applyMostSevere() {
		IStatus status = new Status(IStatus.OK, "not_used", 0, null, null);
		if (m_fileTextStatus.matches(IStatus.ERROR)) {
			status = m_fileTextStatus;
		}

		if (status.getSeverity() == IStatus.ERROR) {
			setErrorMessage(status.getMessage());
			setPageComplete(false);
		} else {
			setErrorMessage(null);
			setPageComplete(true);
		}
	}

	public void loadData() {
		// Correction File
		File path = null;
		String fileName = wizardModel.getBiasCorrectionParameters()
				.getProperty(BiasCorrectionParameters.PROPERTY_CORRECTION_FILE_NAME);
		if (fileName != null && !fileName.isEmpty()) {
			path = new File(fileName).getParentFile();
		} else {
			try {
				Platform.getInstanceLocation().getURL();
				URL locationUrl = Platform.getInstanceLocation().getURL();
				URL fileUrl = FileLocator.toFileURL(locationUrl);
				path = new File(fileUrl.getPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		m_correctionFileFieldEditor.setStringValue(fileName);
		m_correctionFileFieldEditor.setFilterPath(path);
	}

	public void saveData() {
		wizardModel.getBiasCorrectionParameters().setProperty(BiasCorrectionParameters.PROPERTY_CORRECTION_FILE_NAME,
				m_correctionFileFieldEditor.getStringValue());

		var selectedAntennaIndex = (Integer) filteringAntennaIndex.getStructuredSelection().getFirstElement();
		selectedAntennaIndex = Integer.valueOf(-1).equals(selectedAntennaIndex) ? null : selectedAntennaIndex;
		wizardModel.getBiasCorrectionParameters().setAntennaIndex(selectedAntennaIndex);
	}

	/** {@inheritDoc} */
	@Override
	public void setVisible(boolean visible) {
		if (visible) {
			var antennaModel = new LinkedList<Integer>();
			IntStream.range(-1, wizardModel.getAntennaCount()).forEach(antennaModel::add); // -1 for all antenna
			filteringAntennaIndex.setInput(antennaModel);
			if (filteringAntennaIndex.getSelection().isEmpty()) {
				filteringAntennaIndex.setSelection(new StructuredSelection(Integer.valueOf(-1)));
			}
			filteringAntennaIndex.getCombo().getParent().layout();
		}

		super.setVisible(visible);
	}

}
