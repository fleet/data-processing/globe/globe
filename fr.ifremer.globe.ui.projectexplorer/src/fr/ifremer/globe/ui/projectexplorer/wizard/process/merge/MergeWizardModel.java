package fr.ifremer.globe.ui.projectexplorer.wizard.process.merge;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang.math.LongRange;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.profile.Profile;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.core.processes.TimeIntervalParameters;
import fr.ifremer.globe.core.processes.merge.MbgMergeProcess;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableString;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;
import fr.ifremer.globe.utils.exception.FileFormatException;

/**
 * Model of the wizard page
 */
public class MergeWizardModel extends ConvertWizardDefaultModel {

	protected Logger logger = LoggerFactory.getLogger(MergeWizardModel.class);

	private EnumSet<ContentType> acceptedContents = EnumSet.of(ContentType.NVI_NETCDF_4, ContentType.MBG_NETCDF_3);

	/** File service **/
	private final IFileService fileService;

	private TimeIntervalParameters timeIntervalParameters = new TimeIntervalParameters(
			"fr.ifremer.globe.process.merge");
	private Map<File, Optional<LongRange>> outputFilesWithTimeIntervals;
	private WritableString inputFilesErrorMessage = new WritableString();

	/** ContentType of input files to merge */
	protected ContentType mergeType;

	/**
	 * Constructor
	 */
	public MergeWizardModel() {
		fileService = IFileService.grab();
		resetFilterExtensions();
		inputFiles.addChangeListener(e -> onInputFilesChanged());
	}

	private void resetFilterExtensions() {
		inputFilesFilterExtensions.clear();
		acceptedContents.forEach(c -> inputFilesFilterExtensions.addAll(fileService.getFileFilters(c)));
	}

	private void onInputFilesChanged() {
		if (!inputFiles.isEmpty()) {
			mergeType = fileService.getContentType(inputFiles.get(0).getAbsolutePath());

			// if first file is not accepted
			if (!acceptedContents.contains(mergeType)) {
				String message = String.format("File %s is invalid.", inputFiles.get(0).getAbsolutePath());
				logger.error(message);
				MessageDialog.openError(Display.getDefault().getActiveShell(), "Invalid file", message);
				inputFiles.clear();
				return;
			}

			// load infos
			List<IFileInfo> infos = new ArrayList<>();
			for (File file : inputFiles) {
				Optional<IFileInfo> optInfo = fileService.getFileInfo(file.getAbsolutePath());
				if (optInfo.isPresent() && optInfo.get().getContentType() == mergeType) {
					infos.add(optInfo.get());
				} else {
					String message = String.format("File %s is invalid.", file.getAbsolutePath());
					logger.error(message);
					MessageDialog.openError(Display.getDefault().getActiveShell(), "Invalid file", message);
					inputFiles.remove(file);
				}
			}

			updateTimeIntervalParameters(infos);
			outputFormatExtensions.clear();
			outputFormatExtensions.add(fileService.getFileFilters(mergeType).get(0).getSecond());

			// call checking method if MBG
			if (mergeType == ContentType.MBG_NETCDF_3) {
				inputFilesErrorMessage.setValue(MbgMergeProcess.checkInputFiles(infos));
			}

			// Set a filter that match files extension
			inputFilesFilterExtensions.clear();
			inputFilesFilterExtensions.addAll(fileService.getFileFilters(mergeType));
		} else {
			resetFilterExtensions();
		}
	}

	private void updateTimeIntervalParameters(List<IFileInfo> infos) {
		long minDate = Long.MAX_VALUE;
		long maxDate = -Long.MAX_VALUE;
		for (IFileInfo info : infos) {
			Pair<Instant, Instant> startEndDates = info.getStartEndDate().get();
			minDate = Math.min(minDate, startEndDates.getFirst().toEpochMilli());
			maxDate = Math.max(maxDate, startEndDates.getSecond().toEpochMilli());
		}
		if (minDate != Long.MAX_VALUE)
			timeIntervalParameters.setLongProperty(TimeIntervalParameters.PROPERTY_STARTDATETIME_VALUE, minDate);
		if (maxDate != -Long.MAX_VALUE)
			timeIntervalParameters.setLongProperty(TimeIntervalParameters.PROPERTY_ENDDATETIME_VALUE, maxDate);
	}

	// GETTERS

	public TimeIntervalParameters getTimeIntervalParameters() {
		return timeIntervalParameters;
	}

	public Map<File, Optional<LongRange>> getOutputFilesWithTimeIntervals() {
		return outputFilesWithTimeIntervals;
	}

	public WritableString getInputFilesErrorMessage() {
		return inputFilesErrorMessage;
	}

	/**
	 * @return an optional with a set of time profile, or an empty
	 * @throws IOException
	 * @throws FileFormatException
	 */
	protected Optional<Set<Profile>> getTimeProfileSet() throws IOException, FileFormatException {
		// define time interval
		Optional<Set<Profile>> optTimeProfileSet = Optional.empty();
		// cut file provided
		if (timeIntervalParameters.getBoolProperty(TimeIntervalParameters.PROPERTY_CUT_OPTION)) {
			String cutFileName = timeIntervalParameters.getProperty(TimeIntervalParameters.PROPERTY_CUT_FILE_NAME);
			optTimeProfileSet = Optional.of((new HashSet<>(ProfileUtils.loadProfilesFromFile(new File(cutFileName)))));
		}
		// time interval from manual option
		else if (timeIntervalParameters.getBoolProperty(TimeIntervalParameters.PROPERTY_MANUALLY_OPTION)) {
			long startDate = timeIntervalParameters
					.getLongProperty(TimeIntervalParameters.PROPERTY_STARTDATETIME_VALUE);
			long endDate = timeIntervalParameters.getLongProperty(TimeIntervalParameters.PROPERTY_ENDDATETIME_VALUE);
			Profile manualTimeProfile = new Profile("output_merge", startDate, endDate);
			optTimeProfileSet = Optional.of(new HashSet<>(Arrays.asList(manualTimeProfile)));
		}
		return optTimeProfileSet;
	}

	// SETTERS

	public void setOutputFilesWithTimeIntervals(Map<File, Optional<LongRange>> map) {
		outputFilesWithTimeIntervals = map;
	}

	/**
	 * @return the {@link #mergeType}
	 */
	public ContentType getMergeType() {
		return mergeType;
	}

	/**
	 * @param mergeType the {@link #mergeType} to set
	 */
	public void setMergeType(ContentType mergeType) {
		this.mergeType = mergeType;
	}

}