package fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList;

public class CorrectionFileEditionDialog extends CorrectionFileCreationDialog {

	public CorrectionFileEditionDialog(Shell parentShell, CorrectionList correctionList) {

		super(parentShell);
		this.correctionList = correctionList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Apply", true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

}
