package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter;

import java.util.List;
import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.processes.detectionfilter.FilteringProcess;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.ui.wizard.FilesToProcessEntry;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Process and wizard for validity filtering
 */
public class DetectionFilterProcessWithWizard extends DefaultProcessWithWizard<DetectionFilterWizardModel> {

	public DetectionFilterProcessWithWizard(Shell shell) {
		super(shell, "Detection filter", DetectionFilterWizardModel::new, DetectionFilterWizard::new);
	}

	/**
	 * Process method executed after a click on finish.
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {

		// Get file list (input/output/temp file tuples)
		List<FilesToProcessEntry> filesToProcess = getFilesToProcessWithTemp(logger);

		// Apply filtering
		String s = filesToProcess.size() > 1 ? "s" : "";
		logger.info("Start filtering of {} file{}...", filesToProcess.size(), s);

		FilteringProcess processFunction = new FilteringProcess(model.getAttributeProvider(),
				model.getCriterias(), model.getDataToSet());

		monitor.setTaskName("Detection filter");
		monitor.beginTask("Detection filter running ...", filesToProcess.size());

		// Loop on input files
		IDataContainerInfoService infoService = IDataContainerInfoService.grab();
		int processedFileCount = 0;
		int errorFileCount = 0;
		for (FilesToProcessEntry entry : filesToProcess) {
			logger.info("Start processing filtering for {}...", entry.getInputFile().getAbsolutePath());
			Optional<ISounderNcInfo> info = infoService.getSounderNcInfo(entry.getTempFile().getAbsolutePath());
			if (info.isPresent()) {
				try {
					// Process file
					processFunction.processFile(info.get(), monitor, logger);
					// write and load output files if asked
					writeOutputFileFromTemp(entry);
					processedFileCount++;
				} catch (Exception e) {
					errorFileCount++;
					logger.error("Error while processing file {}: {}", entry.getInputFile().getPath(), e.getMessage(),
							e);
				}
			}
			logger.info("Filtering of {} done", entry.getInputFile().getAbsolutePath());
			if (monitor.isCanceled())
				return Status.CANCEL_STATUS;
			monitor.worked(1);
		}

		// load output files if asked
		loadOutputFiles(filesToProcess, monitor);

		logger.info("End of process: Detection filter applied on {} file{}.", processedFileCount, s);
		if (errorFileCount > 0) {
			String sError = errorFileCount > 1 ? "s" : "";
			logger.error("Error while processing {} file{}",errorFileCount,sError);
		}
		return monitor.isCanceled() ? Status.CANCEL_STATUS : Status.OK_STATUS;
	}

}
