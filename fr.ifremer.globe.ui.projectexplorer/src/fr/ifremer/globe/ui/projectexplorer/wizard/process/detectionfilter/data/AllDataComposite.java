/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.data;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.processes.detectionfilter.IAttributeProvider;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;

/**
 * Set of Composite used to edit the value to set.
 */
public class AllDataComposite extends Composite {
	protected Map<Class<?>, ADataComposite> filtersComposite = new HashMap<>();
	protected IAttributeProvider attributeProvider;

	/**
	 * Constructor.
	 */
	public AllDataComposite(IAttributeProvider attributeProvider, Composite parent, int style) {
		super(parent, style);
		this.attributeProvider = attributeProvider;
		setLayout(new StackLayout());
		filtersComposite.put(Boolean.class, new BooleanDataComposite(this, style));
		filtersComposite.put(Byte.class, new NumericDataComposite(this, style, "-###0"));
		filtersComposite.put(Double.class, new NumericDataComposite(this, style, "-###,###,##0.#########"));
		filtersComposite.put(Float.class, new NumericDataComposite(this, style, "-###,###,##0.######"));
		filtersComposite.put(Integer.class, new NumericDataComposite(this, style, "-###,###,##0"));
		filtersComposite.put(Long.class, new NumericDataComposite(this, style, "-###,###,###,##0"));
		filtersComposite.put(Short.class, new NumericDataComposite(this, style, "-##,##0"));
	}

	/**
	 * @return the current displayed ADataComposite.
	 */
	public ADataComposite getTopDataComposite() {
		return (ADataComposite) getLayout().topControl;
	}

	/**
	 * Consider a new Attribute selection.
	 */
	public ADataComposite show(PredefinedLayers<?> selection) {
		getLayout().topControl = filtersComposite.get(attributeProvider.retrieveDataAttributeType(selection));
		layout();
		return getTopDataComposite();
	}

	/**
	 * Follow the link.
	 *
	 * @see org.eclipse.swt.widgets.Composite#getLayout()
	 */
	@Override
	public StackLayout getLayout() {
		return (StackLayout) super.getLayout();
	}

	/**
	 * @return the {@link #attributeProvider}
	 */
	public IAttributeProvider getAttributeProvider() {
		return attributeProvider;
	}

	/**
	 * @param attributeProvider the {@link #attributeProvider} to set
	 */
	public void setAttributeProvider(IAttributeProvider attributeProvider) {
		this.attributeProvider = attributeProvider;
	}
}
