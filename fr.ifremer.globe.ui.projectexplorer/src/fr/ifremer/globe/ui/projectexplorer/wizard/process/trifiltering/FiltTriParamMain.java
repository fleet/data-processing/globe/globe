package fr.ifremer.globe.ui.projectexplorer.wizard.process.trifiltering;

import java.io.File;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.Activator;
import fr.ifremer.globe.core.processes.filtri.model.FiltTriParameters;
import fr.ifremer.globe.core.processes.filtri.preferences.FilttriPreferences.FilteringMethod;

public class FiltTriParamMain extends WizardPage implements Listener {

	/** Default slice size */
	public static final int DEFAULT_SLICE_SIZE = 250;

	final KeyAdapter doubleKeyAdapter = new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			char c = e.character;
			if (!('0' <= c && c <= '9')
					&& !(c == '+' || c == '-' || c == '.' || c == ',' || c == 'E' || c == 8 || c == 127)) {
				e.doit = false;
				e.widget.getDisplay().beep();
			}
		}
	};

	final KeyAdapter intKeyAdapter = new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			char c = e.character;
			if (!('0' <= c && c <= '9') && !(c == '+' || c == '-' || c == 8 || c == 127)) {
				e.doit = false;
				e.widget.getDisplay().beep();
			}
		}
	};

	final IStatus statusOK = new Status(IStatus.OK, "not_used", 0, null, null);

	final int fieldWidth = 50;

	final Rectangle panelBounds = new Rectangle(0, 0, 400, 140);

	private FiltTriParameters m_parameters;
	private String m_outputDirectory;
	private Combo m_methodCombo;
	private Button m_saveOptionButton;
	private Label m_prefixLabel;
	private Text m_prefixText;

	private Group m_heightPanel;
	private Text m_heightInvalidLimit;
	private Text m_heightIterationNumber;

	private Group m_normalPanel;
	private Text m_normalHeightInvalidLimit;
	private Text m_normalSelectLimit;
	private Text m_normalAngleInvalidLimit;

	private Group m_neighbourPanel;
	private Text m_neighbourHeightInvalidLimit;
	private Text m_neighbourSelectLimit;
	private Text m_neighbourDistanceInvalidLimit;

	private DirectoryFieldEditor m_directoryFieldEditor;
	private Text m_directoryText;
	/**
	 * Output directory.
	 */
	public static final String PROPERTY_OUTPUT_DIRECTORY_NAME = "outputDirectoryName";
	private IStatus m_directoryTextStatus = this.statusOK;

	private IStatus m_prefixStatus = this.statusOK;
	private IStatus m_invalidLimitStatus = this.statusOK;
	private IStatus m_iterationNumberStatus = this.statusOK;
	private IStatus m_selectLimitStatus = this.statusOK;
	private IStatus m_angleLimitStatus = this.statusOK;
	private IStatus m_distanceLimitStatus = this.statusOK;
	private Composite container;
	private Spinner sliceSize;
	private Button btnCustomSlice;

	public FiltTriParamMain(FiltTriParameters parameters, String outputDirectory) {
		this(parameters);
		this.m_outputDirectory = outputDirectory;
	}

	public FiltTriParamMain(FiltTriParameters parameters) {
		super("FiltTri Parameters");
		setTitle("FiltTri Parameters");
		setDescription("Enter parameter values for FiltTri processing");
		this.m_parameters = parameters;
	}

	@Override
	public void createControl(Composite parent) {
		this.container = new Composite(parent, SWT.NONE);
		createMain(this.container);
		getShell().pack();
		setControl(this.container);
		setPageComplete(false);
		loadData();
	}

	public Composite createComposite(Composite parent) {
		this.container = new Composite(parent, SWT.NONE);
		createMain(this.container);
		return this.container;
	}

	protected void createMain(Composite parent) {
		GridLayout gl_container = new GridLayout();
		gl_container.verticalSpacing = 10;
		parent.setLayout(gl_container);
		gl_container.numColumns = 3;

		Label methodLabel = new Label(parent, SWT.NULL);
		methodLabel.setText("Filtering method");
		this.m_methodCombo = new Combo(parent, SWT.READ_ONLY);
		this.m_methodCombo.setItems(new String[] { "Delaunay/Heights", "Delaunay/Normals", "Delaunay/Neighbours" });
		this.m_methodCombo.addListener(SWT.Selection, this);

		Composite panelContainer = new Composite(parent, SWT.NONE);
		GridData gd = new GridData(SWT.FILL, SWT.FILL, true, false);
		gd.horizontalSpan = 3;
		panelContainer.setLayoutData(gd);

		createHeighPanel(panelContainer);
		createNormalPanel(panelContainer);
		createNeighbourPanel(panelContainer);

		// Option : Process result saved in new files or not
		if (this.m_outputDirectory != null) {
			this.m_saveOptionButton = new Button(parent, SWT.CHECK);
			this.m_saveOptionButton.setText("Save results in new file(s)");
			gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.horizontalSpan = 3;
			this.m_saveOptionButton.setLayoutData(gd);
			this.m_saveOptionButton.addListener(SWT.Selection, this);

			this.m_directoryFieldEditor = new DirectoryFieldEditor(PROPERTY_OUTPUT_DIRECTORY_NAME, "Output directory",
					parent);
			this.m_directoryText = this.m_directoryFieldEditor.getTextControl(parent);
			this.m_directoryText.addListener(SWT.Modify, this);
			// m_directoryText.setText(m_outputDirectory);

			// Prefixe for new file names
			this.m_prefixLabel = new Label(parent, SWT.NULL);
			this.m_prefixLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
			this.m_prefixLabel.setText("Prefix for new file names");

			this.m_prefixText = new Text(parent, SWT.BORDER | SWT.SINGLE);
			GridData gd_prefixText = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
			gd_prefixText.widthHint = 341;
			this.m_prefixText.setLayoutData(gd_prefixText);

			this.m_prefixText.addListener(SWT.Modify, this);
			this.m_prefixText.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.character == ' ') {
						e.doit = false;
					}
				}
			});
		}

		this.btnCustomSlice = new Button(this.container, SWT.CHECK);
		this.btnCustomSlice.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
		this.btnCustomSlice.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (FiltTriParamMain.this.btnCustomSlice.getSelection())
					FiltTriParamMain.this.sliceSize.setEnabled(true);
				else {
					FiltTriParamMain.this.sliceSize.setEnabled(false);
					FiltTriParamMain.this.sliceSize
							.setSelection(Activator.getFilttriPreferences().getSliceSize().getValue());
				}
			}
		});
		this.btnCustomSlice.setText("Customize slice size (default is "
				+ Activator.getFilttriPreferences().getSliceSize().getValue() + ")");

		this.sliceSize = new Spinner(this.container, SWT.BORDER);
		this.sliceSize.setEnabled(false);
		this.sliceSize.setIncrement(10);
		this.sliceSize.setPageIncrement(100);
		this.sliceSize.setMaximum(1000);
		this.sliceSize.setMinimum(100);
		this.sliceSize.setSelection(Activator.getFilttriPreferences().getSliceSize().getValue());

	}

	protected void createHeighPanel(Composite parent) {
		this.m_heightPanel = new Group(parent, SWT.NONE);
		this.m_heightPanel.setText("Delaunay/Heights");
		this.m_heightPanel.setBounds(this.panelBounds);

		GridLayout layout = new GridLayout();
		layout.verticalSpacing = 10;
		layout.marginLeft = 10;
		this.m_heightPanel.setLayout(layout);
		layout.numColumns = 3;

		// Height coefficient for sounding invalidation
		Label label = new Label(this.m_heightPanel, SWT.NULL);
		label.setText("Height coefficient for sounding invalidation");
		this.m_heightInvalidLimit = new Text(this.m_heightPanel, SWT.BORDER | SWT.SINGLE | SWT.RIGHT);
		GridData gd = new GridData(this.fieldWidth, SWT.DEFAULT);
		gd.horizontalSpan = 2;
		this.m_heightInvalidLimit.setLayoutData(gd);
		this.m_heightInvalidLimit.addListener(SWT.Modify, this);
		this.m_heightInvalidLimit.addKeyListener(this.doubleKeyAdapter);

		// Number of iterations
		label = new Label(this.m_heightPanel, SWT.NULL);
		label.setText("Number of treatment iterations");
		this.m_heightIterationNumber = new Text(this.m_heightPanel, SWT.BORDER | SWT.SINGLE | SWT.RIGHT);
		this.m_heightIterationNumber.setLayoutData(new GridData(this.fieldWidth, SWT.DEFAULT));
		this.m_heightIterationNumber.addListener(SWT.Modify, this);
		this.m_heightIterationNumber.addKeyListener(this.intKeyAdapter);

		this.m_heightPanel.layout();
	}

	protected void createNormalPanel(Composite parent) {
		this.m_normalPanel = new Group(parent, SWT.NONE);
		this.m_normalPanel.setText("Delaunay/Normals");
		this.m_normalPanel.setBounds(this.panelBounds);

		GridLayout layout = new GridLayout();
		layout.verticalSpacing = 10;
		layout.marginLeft = 10;
		this.m_normalPanel.setLayout(layout);
		layout.numColumns = 3;

		// Height coefficient for first sounding invalidation
		Label label = new Label(this.m_normalPanel, SWT.NULL);
		label.setText("Height coefficient for first invalidation");
		this.m_normalHeightInvalidLimit = new Text(this.m_normalPanel, SWT.BORDER | SWT.SINGLE | SWT.RIGHT);
		GridData gd = new GridData(this.fieldWidth, SWT.DEFAULT);
		gd.horizontalSpan = 2;
		this.m_normalHeightInvalidLimit.setLayoutData(gd);
		this.m_normalHeightInvalidLimit.addListener(SWT.Modify, this);
		this.m_normalHeightInvalidLimit.addKeyListener(this.doubleKeyAdapter);

		// Selection parameter
		label = new Label(this.m_normalPanel, SWT.NULL);
		label.setText("Sounding selection parameter");
		this.m_normalSelectLimit = new Text(this.m_normalPanel, SWT.BORDER | SWT.SINGLE | SWT.RIGHT);
		gd = new GridData(this.fieldWidth, SWT.DEFAULT);
		gd.horizontalSpan = 2;
		this.m_normalSelectLimit.setLayoutData(gd);
		this.m_normalSelectLimit.addListener(SWT.Modify, this);
		this.m_normalSelectLimit.addKeyListener(this.doubleKeyAdapter);

		// Max angle for invalidation
		label = new Label(this.m_normalPanel, SWT.NULL);
		label.setText("Maximum allowed angle between normals");
		this.m_normalAngleInvalidLimit = new Text(this.m_normalPanel, SWT.BORDER | SWT.SINGLE | SWT.RIGHT);
		this.m_normalAngleInvalidLimit.setLayoutData(new GridData(this.fieldWidth, SWT.DEFAULT));
		this.m_normalAngleInvalidLimit.addListener(SWT.Modify, this);
		this.m_normalAngleInvalidLimit.addKeyListener(this.doubleKeyAdapter);
		label = new Label(this.m_normalPanel, SWT.NULL);
		label.setText("Degrees");

		this.m_normalPanel.layout();
	}

	protected void createNeighbourPanel(Composite parent) {
		this.m_neighbourPanel = new Group(parent, SWT.NONE);
		this.m_neighbourPanel.setText("Delaunay/Neighbours");
		this.m_neighbourPanel.setBounds(this.panelBounds);

		GridLayout layout = new GridLayout();
		layout.verticalSpacing = 10;
		layout.marginLeft = 10;
		this.m_neighbourPanel.setLayout(layout);
		layout.numColumns = 3;

		// Height coefficient for first sounding invalidation
		Label label = new Label(this.m_neighbourPanel, SWT.NULL);
		label.setText("Height coefficient for first invalidation");
		this.m_neighbourHeightInvalidLimit = new Text(this.m_neighbourPanel, SWT.BORDER | SWT.SINGLE | SWT.RIGHT);
		GridData gd = new GridData(this.fieldWidth, SWT.DEFAULT);
		gd.horizontalSpan = 2;
		this.m_neighbourHeightInvalidLimit.setLayoutData(gd);
		this.m_neighbourHeightInvalidLimit.addListener(SWT.Modify, this);
		this.m_neighbourHeightInvalidLimit.addKeyListener(this.doubleKeyAdapter);

		// Selection parameter
		label = new Label(this.m_neighbourPanel, SWT.NULL);
		label.setText("Sounding selection parameter");
		this.m_neighbourSelectLimit = new Text(this.m_neighbourPanel, SWT.BORDER | SWT.SINGLE | SWT.RIGHT);
		gd = new GridData(this.fieldWidth, SWT.DEFAULT);
		gd.horizontalSpan = 2;
		this.m_neighbourSelectLimit.setLayoutData(gd);
		this.m_neighbourSelectLimit.addListener(SWT.Modify, this);
		this.m_neighbourSelectLimit.addKeyListener(this.doubleKeyAdapter);

		// Max distance for invalidation
		label = new Label(this.m_neighbourPanel, SWT.NULL);
		label.setText("Neighbouring limit");
		this.m_neighbourDistanceInvalidLimit = new Text(this.m_neighbourPanel, SWT.BORDER | SWT.SINGLE | SWT.RIGHT);
		this.m_neighbourDistanceInvalidLimit.setLayoutData(new GridData(this.fieldWidth, SWT.DEFAULT));
		this.m_neighbourDistanceInvalidLimit.addListener(SWT.Modify, this);
		this.m_neighbourDistanceInvalidLimit.addKeyListener(this.doubleKeyAdapter);
		label = new Label(this.m_neighbourPanel, SWT.NULL);
		label.setText("%");

		this.m_neighbourPanel.layout();
	}

	@Override
	public void handleEvent(Event event) {

		if (event.widget == this.m_saveOptionButton) {
			if (this.m_saveOptionButton.getSelection() && this.m_prefixText.getText().isEmpty()) {
				this.m_prefixStatus = new Status(IStatus.ERROR, "not_used", 0, "New file names prefix can't be empty",
						null);
			} else {
				this.m_prefixStatus = this.statusOK;
			}
			if (this.m_saveOptionButton.getSelection() && this.m_directoryText.getText().isEmpty()) {
				this.m_directoryTextStatus = new Status(IStatus.ERROR, "not_used", 0,
						"New file names prefix can't be empty", null);
			} else {
				this.m_directoryTextStatus = this.statusOK;
			}
			checkSaveOption();
		} else if (event.widget == this.m_prefixText) {
			if (this.m_prefixText.getText().isEmpty()) {
				this.m_prefixStatus = new Status(IStatus.ERROR, "not_used", 0, "New file names prefix can't be empty",
						null);
			} else {
				this.m_prefixStatus = this.statusOK;
			}
		} else if (event.widget == this.m_methodCombo) {
			checkMethodOption();
		} else if (event.widget == this.m_heightInvalidLimit) {
			this.m_invalidLimitStatus = this.statusOK;
			if (!isValidDouble(this.m_heightInvalidLimit.getText())
					|| Double.parseDouble(this.m_heightInvalidLimit.getText()) <= 0) {
				this.m_invalidLimitStatus = new Status(IStatus.ERROR, "not_used", 0,
						"Height coefficient for sounding invalidation must be greater than 0", null);
			}
		} else if (event.widget == this.m_heightIterationNumber) {
			this.m_iterationNumberStatus = this.statusOK;
			if (!isValidInteger(this.m_heightIterationNumber.getText())
					|| Integer.parseInt(this.m_heightIterationNumber.getText()) <= 0) {
				this.m_iterationNumberStatus = new Status(IStatus.ERROR, "not_used", 0,
						"Iteration number must be greater than 0", null);
			}
		} else if (event.widget == this.m_normalHeightInvalidLimit) {
			this.m_invalidLimitStatus = this.statusOK;
			if (!isValidDouble(this.m_normalHeightInvalidLimit.getText())
					|| Double.parseDouble(this.m_normalHeightInvalidLimit.getText()) <= 0) {
				this.m_invalidLimitStatus = new Status(IStatus.ERROR, "not_used", 0,
						"Height coefficient for sounding invalidation must be greater than 0", null);
			}
		} else if (event.widget == this.m_normalSelectLimit) {
			this.m_selectLimitStatus = this.statusOK;
			if (!isValidDouble(this.m_normalSelectLimit.getText())
					|| Double.parseDouble(this.m_normalSelectLimit.getText()) <= 0) {
				this.m_selectLimitStatus = new Status(IStatus.ERROR, "not_used", 0,
						"Sounding select parameter must be greater than 0", null);
			}
		} else if (event.widget == this.m_normalAngleInvalidLimit) {
			this.m_angleLimitStatus = this.statusOK;
			if (!isValidDouble(this.m_normalAngleInvalidLimit.getText())
					|| Double.parseDouble(this.m_normalAngleInvalidLimit.getText()) <= 0) {
				this.m_angleLimitStatus = new Status(IStatus.ERROR, "not_used", 0, "Angle limit must be greater than 0",
						null);
			}
		} else if (event.widget == this.m_neighbourSelectLimit) {
			this.m_selectLimitStatus = this.statusOK;
			if (!isValidDouble(this.m_neighbourSelectLimit.getText())
					|| Double.parseDouble(this.m_neighbourSelectLimit.getText()) <= 0) {
				this.m_selectLimitStatus = new Status(IStatus.ERROR, "not_used", 0,
						"Sounding select parameter must be greater than 0", null);
			}
		} else if (event.widget == this.m_neighbourDistanceInvalidLimit) {
			this.m_distanceLimitStatus = this.statusOK;
			if (!isValidDouble(this.m_neighbourDistanceInvalidLimit.getText())
					|| Double.parseDouble(this.m_neighbourDistanceInvalidLimit.getText()) <= 0) {
				this.m_distanceLimitStatus = new Status(IStatus.ERROR, "not_used", 0,
						"Neighbouring limit must be greater than 0", null);
			}
		} else if (event.widget == this.m_directoryText) {
			if (this.m_directoryText.getText().isEmpty()) {
				this.m_directoryTextStatus = new Status(IStatus.ERROR, "not_used", 0, "Output directory can't be empty",
						null);

			} else {
				this.m_directoryTextStatus = this.statusOK;
			}
		}

		applyMostSevere();
	}

	private void applyMostSevere() {
		IStatus status = new Status(IStatus.OK, "not_used", 0, null, null);
		if (this.m_saveOptionButton != null && this.m_saveOptionButton.getSelection()
				&& this.m_prefixStatus.matches(IStatus.ERROR)) {
			status = this.m_prefixStatus;
		} else if (this.m_saveOptionButton != null && this.m_saveOptionButton.getSelection()
				&& this.m_prefixStatus.matches(IStatus.ERROR)) {
			status = this.m_directoryTextStatus;
		} else if (this.m_methodCombo.getSelectionIndex() == FiltTriParameters.FILTERING_OPTION_HEIGHT) {
			if (this.m_invalidLimitStatus.matches(IStatus.ERROR)) {
				status = this.m_invalidLimitStatus;
			} else if (this.m_iterationNumberStatus.matches(IStatus.ERROR)) {
				status = this.m_iterationNumberStatus;
			}
		} else if (this.m_methodCombo.getSelectionIndex() == FiltTriParameters.FILTERING_OPTION_NORMAL) {
			if (this.m_invalidLimitStatus.matches(IStatus.ERROR)) {
				status = this.m_invalidLimitStatus;
			} else if (this.m_selectLimitStatus.matches(IStatus.ERROR)) {
				status = this.m_selectLimitStatus;
			} else if (this.m_angleLimitStatus.matches(IStatus.ERROR)) {
				status = this.m_angleLimitStatus;
			}
		} else if (this.m_methodCombo.getSelectionIndex() == FiltTriParameters.FILTERING_OPTION_NEIGHBOUR) {
			if (this.m_invalidLimitStatus.matches(IStatus.ERROR)) {
				status = this.m_invalidLimitStatus;
			} else if (this.m_selectLimitStatus.matches(IStatus.ERROR)) {
				status = this.m_selectLimitStatus;
			} else if (this.m_distanceLimitStatus.matches(IStatus.ERROR)) {
				status = this.m_distanceLimitStatus;
			}
		}

		if (status.getSeverity() == IStatus.ERROR) {
			setErrorMessage(status.getMessage());
			setPageComplete(false);
		} else {
			setErrorMessage(null);
			setPageComplete(true);
		}
	}

	private void checkSaveOption() {
		this.m_prefixLabel.setEnabled(this.m_saveOptionButton.getSelection());
		this.m_prefixText.setEnabled(this.m_saveOptionButton.getSelection());
		this.m_directoryFieldEditor.setEnabled(this.m_saveOptionButton.getSelection(), this.container);
	}

	private void checkMethodOption() {
		if (this.m_methodCombo.getSelectionIndex() == FiltTriParameters.FILTERING_OPTION_HEIGHT) {
			this.m_heightPanel.setVisible(true);
			this.m_normalPanel.setVisible(false);
			this.m_neighbourPanel.setVisible(false);
		} else if (this.m_methodCombo.getSelectionIndex() == FiltTriParameters.FILTERING_OPTION_NORMAL) {
			this.m_heightPanel.setVisible(false);
			this.m_normalPanel.setVisible(true);
			this.m_neighbourPanel.setVisible(false);
		} else if (this.m_methodCombo.getSelectionIndex() == FiltTriParameters.FILTERING_OPTION_NEIGHBOUR) {
			this.m_heightPanel.setVisible(false);
			this.m_normalPanel.setVisible(false);
			this.m_neighbourPanel.setVisible(true);
		}
	}

	public boolean isValidDouble(String number) {
		try {
			new Double(number);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean isValidInteger(String number) {
		try {
			new Integer(number);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public void loadData() {
		if (this.m_saveOptionButton != null) {
			this.m_saveOptionButton.setSelection(Activator.getFilttriPreferences().getSaveOption().getValue());
			File prefOuputDirectory = Activator.getFilttriPreferences().getOutputDirectory().getValue().toFile();
			if (prefOuputDirectory.isDirectory())
				this.m_directoryText.setText(prefOuputDirectory.getAbsolutePath());
			else
				this.m_directoryText.setText(this.m_outputDirectory);
			this.m_prefixText.setText(Activator.getFilttriPreferences().getFilePrefix().getValue());
			checkSaveOption();
		}

		FilteringMethod filteringmethodEnum = (FilteringMethod) Activator.getFilttriPreferences().getFilteringMethod()
				.getValue();
		this.m_methodCombo.select(filteringmethodEnum.getValue());

		this.m_heightInvalidLimit
				.setText(Activator.getFilttriPreferences().getHeightInvalidLimit().getValue().toString());
		this.m_heightIterationNumber
				.setText(Activator.getFilttriPreferences().getHeightIterationNumber().getValue().toString());

		this.m_normalHeightInvalidLimit
				.setText(Activator.getFilttriPreferences().getNormalHeightInvalidLimit().getValue().toString());
		this.m_normalSelectLimit
				.setText(Activator.getFilttriPreferences().getNormalSelectLimit().getValue().toString());
		this.m_normalAngleInvalidLimit
				.setText(Activator.getFilttriPreferences().getNormalAngleInvalidLimit().getValue().toString());

		this.m_neighbourHeightInvalidLimit
				.setText(Activator.getFilttriPreferences().getNeighbourHeightInvalidLimit().getValue().toString());
		this.m_neighbourSelectLimit
				.setText(Activator.getFilttriPreferences().getNeighbourSelectLimit().getValue().toString());
		this.m_neighbourDistanceInvalidLimit
				.setText(Activator.getFilttriPreferences().getNeighnourDistanceInvalidLimit().getValue().toString());
		checkMethodOption();

		int loadedSize = Activator.getFilttriPreferences().getSliceSize().getValue();
		if (loadedSize > 1)
			this.sliceSize.setSelection(loadedSize);
	}

	public void loadDataFromParameter() {

		int filteringmethodEnum = this.m_parameters.getIntProperty(FiltTriParameters.PROPERTY_FILTERING_OPTION);
		this.m_methodCombo.select(filteringmethodEnum);

		this.m_heightInvalidLimit
				.setText(this.m_parameters.getProperty(FiltTriParameters.PROPERTY_HEIGHT_INVALID_LIMIT));
		this.m_heightIterationNumber
				.setText(this.m_parameters.getProperty(FiltTriParameters.PROPERTY_HEIGHT_ITERATION_NUMBER));

		this.m_normalHeightInvalidLimit
				.setText(this.m_parameters.getProperty(FiltTriParameters.PROPERTY_NORMAL_HEIGHT_INVALID_LIMIT));
		this.m_normalSelectLimit.setText(this.m_parameters.getProperty(FiltTriParameters.PROPERTY_NORMAL_SELECT_LIMIT));
		this.m_normalAngleInvalidLimit
				.setText(this.m_parameters.getProperty(FiltTriParameters.PROPERTY_NORMAL_ANGLE_INVALID_LIMIT));

		this.m_neighbourHeightInvalidLimit
				.setText(this.m_parameters.getProperty(FiltTriParameters.PROPERTY_NEIGHBOUR_HEIGHT_INVALID_LIMIT));
		this.m_neighbourSelectLimit
				.setText(this.m_parameters.getProperty(FiltTriParameters.PROPERTY_NEIGHBOUR_SELECT_LIMIT));
		this.m_neighbourDistanceInvalidLimit
				.setText(this.m_parameters.getProperty(FiltTriParameters.PROPERTY_NEIGHBOUR_DISTANCE_INVALID_LIMIT));

		int sliceSize = this.m_parameters.getIntProperty(FiltTriParameters.PROPERTY_SLICE_SIZE);
		this.sliceSize.setSelection(sliceSize);
		this.sliceSize.setEnabled(sliceSize != DEFAULT_SLICE_SIZE);
		this.btnCustomSlice.setSelection(sliceSize != DEFAULT_SLICE_SIZE);
		checkMethodOption();
	}

	public void saveData() {
		if (this.m_saveOptionButton != null) {
			this.m_parameters.setBoolProperty(FiltTriParameters.PROPERTY_NEW_FILE_OPTION,
					this.m_saveOptionButton.getSelection());
			this.m_parameters.setProperty(FiltTriParameters.PROPERTY_NEW_FILES_DIRECTORY,
					this.m_directoryText.getText());
			this.m_parameters.setProperty(FiltTriParameters.PROPERTY_NEW_FILES_PREFIX, this.m_prefixText.getText());
		}
		this.m_parameters.setIntProperty(FiltTriParameters.PROPERTY_FILTERING_OPTION,
				this.m_methodCombo.getSelectionIndex());
		this.m_parameters.setProperty(FiltTriParameters.PROPERTY_HEIGHT_INVALID_LIMIT,
				this.m_heightInvalidLimit.getText());
		this.m_parameters.setProperty(FiltTriParameters.PROPERTY_HEIGHT_ITERATION_NUMBER,
				this.m_heightIterationNumber.getText());
		this.m_parameters.setProperty(FiltTriParameters.PROPERTY_NORMAL_HEIGHT_INVALID_LIMIT,
				this.m_normalHeightInvalidLimit.getText());
		this.m_parameters.setProperty(FiltTriParameters.PROPERTY_NORMAL_SELECT_LIMIT,
				this.m_normalSelectLimit.getText());
		this.m_parameters.setProperty(FiltTriParameters.PROPERTY_NORMAL_ANGLE_INVALID_LIMIT,
				this.m_normalAngleInvalidLimit.getText());
		this.m_parameters.setProperty(FiltTriParameters.PROPERTY_NEIGHBOUR_HEIGHT_INVALID_LIMIT,
				this.m_neighbourHeightInvalidLimit.getText());
		this.m_parameters.setProperty(FiltTriParameters.PROPERTY_NEIGHBOUR_SELECT_LIMIT,
				this.m_neighbourSelectLimit.getText());
		this.m_parameters.setProperty(FiltTriParameters.PROPERTY_NEIGHBOUR_DISTANCE_INVALID_LIMIT,
				this.m_neighbourDistanceInvalidLimit.getText());
		this.m_parameters.setIntProperty(FiltTriParameters.PROPERTY_SLICE_SIZE,
				this.btnCustomSlice.getSelection() ? this.sliceSize.getSelection()
						: Activator.getFilttriPreferences().getSliceSize().getValue());

		// Updating preferences
		if (this.m_saveOptionButton != null) {
			Activator.getFilttriPreferences().getSaveOption().setValue(this.m_saveOptionButton.getSelection(), false);
			Activator.getFilttriPreferences().getOutputDirectory().setValueAsString(this.m_directoryText.getText(),
					false);
			Activator.getFilttriPreferences().getFilePrefix().setValue(this.m_prefixText.getText(), false);
		}
		Activator.getFilttriPreferences().getFilteringMethod()
				.setValue(FilteringMethod.get(this.m_methodCombo.getText()), false);

		Activator.getFilttriPreferences().getSliceSize().setValueAsString(this.sliceSize.getText(), false);
		Activator.getFilttriPreferences().getHeightInvalidLimit().setValueAsString(this.m_heightInvalidLimit.getText(),
				false);
		Activator.getFilttriPreferences().getHeightIterationNumber()
				.setValueAsString(this.m_heightIterationNumber.getText(), false);

		Activator.getFilttriPreferences().getNormalHeightInvalidLimit()
				.setValueAsString(this.m_normalHeightInvalidLimit.getText(), false);
		;
		Activator.getFilttriPreferences().getNormalSelectLimit().setValueAsString(this.m_normalSelectLimit.getText(),
				false);
		;
		Activator.getFilttriPreferences().getNormalAngleInvalidLimit()
				.setValueAsString(this.m_normalAngleInvalidLimit.getText(), false);
		;

		Activator.getFilttriPreferences().getNeighbourHeightInvalidLimit()
				.setValueAsString(this.m_neighbourHeightInvalidLimit.getText(), false);
		;
		Activator.getFilttriPreferences().getNeighbourSelectLimit()
				.setValueAsString(this.m_neighbourSelectLimit.getText(), false);
		;
		Activator.getFilttriPreferences().getNeighnourDistanceInvalidLimit()
				.setValueAsString(this.m_neighbourDistanceInvalidLimit.getText(), false);
		;
		Activator.getFilttriPreferences().save();

	}

}
