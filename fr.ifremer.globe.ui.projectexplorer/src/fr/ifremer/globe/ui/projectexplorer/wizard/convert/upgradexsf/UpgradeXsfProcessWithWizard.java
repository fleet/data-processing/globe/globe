package fr.ifremer.globe.ui.projectexplorer.wizard.convert.upgradexsf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.api.xsf.converter.common.xsf.XsfBase;
import fr.ifremer.globe.api.xsf.converter.common.xsf.groups.RootGrp;
import fr.ifremer.globe.core.model.profile.Profile;
import fr.ifremer.globe.core.model.profile.ProfileUtils;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.runtime.gws.GwsServiceAgent;
import fr.ifremer.globe.core.runtime.gws.param.ExportToXsfParams;
import fr.ifremer.globe.core.runtime.gws.param.MergeXsfParams;
import fr.ifremer.globe.core.runtime.gws.param.MigrateXsfParams;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.ucar.NCFile;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GException;

/**
 * Xsf upgrade process
 * <ul>
 * <li>Converts raw files (.all/.s7k...) to XSF</li>
 * <li>Computes profiles and merge/cut converted XSF</li>
 * <li>Migrates cut XSF</li>
 * <li>Copy migrated XSF files to output files</li>
 * </ul>
 */
public class UpgradeXsfProcessWithWizard extends DefaultProcessWithWizard<UpgradeXsfModel> {
	@Inject
	private GwsServiceAgent gwsServiceAgent;

	/** ISounderNcInfo services */
	@Inject
	private IDataContainerInfoService dataContainerInfoService;

	/** Title of the process */
	private static final String TITLE = "Ugrading XSF files";

	/** List of files to upgrade with all their information */
	private List<UpgradeXsfFile> upgradeXsfFiles = new LinkedList<>();

	/** Temporary folder */
	private final File tempDir;

	/** Logger past on apply */
	private Logger localLogger;

	/**
	 * Constructor
	 */
	public UpgradeXsfProcessWithWizard(Shell shell) {
		super(shell, TITLE, UpgradeXsfModel::new, UpgradeXsfWizard::new);
		tempDir = new File(TemporaryCache.getRootPath(), getClass().getSimpleName());
		tempDir.mkdirs();
	}

	/** {@inheritDoc} */
	@Override
	protected IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		localLogger = logger;
		IStatus result = Status.OK_STATUS;
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		try {
			prepareData(model.getInputFiles().asList(), model.getOutputFiles().asList(), subMonitor.split(10));
			if (upgradeXsfFiles.isEmpty()) {
				return new Status(IStatus.WARNING, "fr.ifremer.globe.ui.projectexplorer",
						"No suitable file to upgrade");
			}

			List<File> newXsfFiles = List.of();
			if (!monitor.isCanceled()) {
				boolean hasWc = upgradeXsfFiles.stream().anyMatch(u -> u.wcPresent);
				// Convert raw files to XSF
				newXsfFiles = convertToXsf(model.getRawFilesModel().getInputFiles().asList(), hasWc,
						subMonitor.split(30));
			}

			// Cut/merge converted XSF files
			if (!monitor.isCanceled())
				result = cutXsfFiles(newXsfFiles, subMonitor.split(30));
			if (Status.OK_STATUS.equals(result) && !monitor.isCanceled())
				result = migrate(subMonitor.split(30));

			// Transfert metadata, copy new XSF files from temp folder to out files
			if (Status.OK_STATUS.equals(result) && !monitor.isCanceled())
				transfertResult(subMonitor.split(10));

			if (Status.OK_STATUS.equals(result) && !monitor.isCanceled()) {
				upgradeXsfFiles.forEach(f -> loadOutputFile(f.outputXsfFile, new NullProgressMonitor()));
			}

		} catch (Exception e) {
			localLogger.error("Upgrading XSF failed", e);
			return new Status(IStatus.ERROR, "fr.ifremer.globe.ui.projectexplorer", "Upgrading XSF failed", e);
		} finally {
			FileUtils.deleteDir(tempDir);
		}
		subMonitor.done();

		return result;
	}

	/** Prepare upgradeXsfFiles attribute */
	private void prepareData(List<File> inputXsfFiles, List<File> outputXsfFiles, IProgressMonitor monitor) {
		localLogger.debug("Examine XSF files");
		SubMonitor subMonitor = SubMonitor.convert(monitor, inputXsfFiles.size());
		for (int i = 0; i < Math.min(inputXsfFiles.size(), outputXsfFiles.size()); i++) {
			UpgradeXsfFile upgradeXsfFile = new UpgradeXsfFile();
			upgradeXsfFile.inputXsfFile = inputXsfFiles.get(i);
			upgradeXsfFile.outputXsfFile = outputXsfFiles.get(i);

			if (!upgradeXsfFile.outputXsfFile.exists() || model.getOverwriteExistingFiles().isTrue()) {
				try {
					upgradeXsfFile.tempCutFile = File.createTempFile(
							FileUtils.getBaseName(upgradeXsfFile.inputXsfFile) + "_", XsfBase.EXTENSION_XSF, tempDir);

					// Open XSF
					ISounderNcInfo xsfInfo = dataContainerInfoService
							.getSounderNcInfo(upgradeXsfFile.inputXsfFile.getPath())
							.orElseThrow(() -> new IOException("Unparsable file"));

					// Extract profile
					Pair<Instant, Instant> startStop = xsfInfo.getStartEndDate()
							.orElseThrow(() -> new IOException("Impossible to determine the start date of file"));
					upgradeXsfFile.profile = new Profile(FileUtils.getBaseName(xsfInfo.getPath()), //
							startStop.getFirst().toEpochMilli(), //
							startStop.getSecond().toEpochMilli());

					// Check if WC present
					upgradeXsfFile.wcPresent = xsfInfo.getWcRxBeamCount() > 0;
					upgradeXsfFiles.add(upgradeXsfFile);

				} catch (IOException e) {
					localLogger.warn("Error when processing {} : {}. File ignored",
							upgradeXsfFile.inputXsfFile.getName(), e.getMessage());
				}
			} else {
				localLogger.warn("File {} exists and overwrite not allowed. {}. File ignored",
						upgradeXsfFile.outputXsfFile.getPath(), upgradeXsfFile.inputXsfFile.getName());
			}

			subMonitor.worked(1);
		}
		subMonitor.done();
	}

	/** Convert all raw in a temporary XSF file */
	private List<File> convertToXsf(List<File> rawFiles, boolean hasWc, IProgressMonitor monitor) {
		List<File> result = new LinkedList<>();
		localLogger.debug("Converting raw files to XSF");
		SubMonitor subMonitor = SubMonitor.convert(monitor, rawFiles.size());
		for (File rawFile : rawFiles) {
			try {
				// perform conversion to XSF
				File outputXsfFile = new File(tempDir, FileUtils.getBaseName(rawFile) + XsfBase.EXTENSION_XSF);
				ExportToXsfParams params = new ExportToXsfParams(//
						rawFile, //
						outputXsfFile, //
						true, // overwrite
						"", // xsfKeywords
						"", // xsfLicense
						"", // xsfRights
						"", // xsfSummary
						"", // xsfTitle
						!hasWc);
				if (gwsServiceAgent.exportToXsf(params, monitor, localLogger).isOK())
					result.add(outputXsfFile);
				else
					localLogger.warn("Conversion failed. {} skipped", rawFile.getName());

			} catch (GException e) {
				localLogger.warn("Conversion error. {} skipped", rawFile.getName());
			}
			subMonitor.worked(1);
		}
		return result;
	}

	/**
	 * Use service to cut new converted XSF files.
	 */
	private IStatus cutXsfFiles(List<File> inXsfFiles, IProgressMonitor monitor) throws IOException, GException {
		localLogger.info("Cutting XSF files...");

		List<File> outCutXsfFiles = new LinkedList<>();
		// Can't use ProfileUtils.writeProfilesToCutFile because of the sort
		File cutFile = new File(tempDir, "profiles.cut");
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(cutFile))) {
			for (var upgradeXsfFile : upgradeXsfFiles) {
				ProfileUtils.writeProfile(writer, upgradeXsfFile.profile);
				outCutXsfFiles.add(upgradeXsfFile.tempCutFile);
			}
		}

		MergeXsfParams params = new MergeXsfParams(inXsfFiles, outCutXsfFiles, cutFile, true);
		return gwsServiceAgent.mergeXsf(params, monitor, localLogger);
	}

	/** Browse cut files and transfer them to output files */
	private void transfertResult(IProgressMonitor monitor) throws IOException {
		localLogger.debug("Finalize process");
		SubMonitor subMonitor = SubMonitor.convert(monitor, upgradeXsfFiles.size());
		for (UpgradeXsfFile upgradeXsfFile : upgradeXsfFiles) {
			if (upgradeXsfFile.tempCutFile.exists()) {
				try (NCFile inNcFile = NCFile.open(upgradeXsfFile.inputXsfFile.getPath(), NCFile.Mode.readonly);
						NCFile outNcFile = NCFile.open(upgradeXsfFile.tempCutFile.getPath(), NCFile.Mode.readwrite)) {
					duplicateAttribute(inNcFile, outNcFile, RootGrp.ATT_TITLE);
					duplicateAttribute(inNcFile, outNcFile, RootGrp.ATT_SUMMARY);
					duplicateAttribute(inNcFile, outNcFile, RootGrp.ATT_KEYWORDS);
					duplicateAttribute(inNcFile, outNcFile, RootGrp.ATT_LICENSE);
					duplicateAttribute(inNcFile, outNcFile, RootGrp.ATT_RIGHTS);
				} catch (NCException e) {
					localLogger.error("Upgrade failed for : {}", upgradeXsfFile.inputXsfFile.getName());
					continue;
				}
				Files.copy(upgradeXsfFile.tempCutFile.toPath(), upgradeXsfFile.outputXsfFile.toPath(),
						StandardCopyOption.REPLACE_EXISTING);
			}
			subMonitor.worked(1);
		}
	}

	/**
	 * Apply migrate process
	 */
	private IStatus migrate(IProgressMonitor monitor) throws GException, IOException {
		localLogger.info("Migrating XSF files...");
		List<File> refXsfFiles = upgradeXsfFiles.stream().map(u -> u.inputXsfFile).toList();
		List<File> xsfFiles = upgradeXsfFiles.stream().map(u -> u.tempCutFile).toList();

		MigrateXsfParams params = new MigrateXsfParams(xsfFiles, xsfFiles, refXsfFiles, null, true);
		return gwsServiceAgent.migrateXsf(params, monitor, localLogger);
	}

	/** Transfers an attribute from the input to the outfile */
	private void duplicateAttribute(NCFile inNcFile, NCFile outNcFile, String attributeName) {
		try {
			outNcFile.addAttribute(attributeName, inNcFile.getAttributeText(attributeName));
		} catch (NCException e) {
			localLogger.debug(e.getMessage());
		}
	}

	static class UpgradeXsfFile {
		// Original file
		public File inputXsfFile;
		// Extracted profile from the input file
		public Profile profile;
		// True when WC is expected in the output file
		public boolean wcPresent;
		// Resulting XSF file of the cut/merge process
		public File tempCutFile;
		// Final resulting XSF file
		public File outputXsfFile;

	}
}
