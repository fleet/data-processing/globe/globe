/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.widget;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.widgets.Composite;

/**
 * ComboViewer used to display actions.
 */
public class ComparatorCombo extends ComboViewer {

	/**
	 * Constructor.
	 */
	public ComparatorCombo(Composite parent, int style) {
		super(parent, style);
		setLabelProvider(new LabelProvider());
		setContentProvider(new ArrayContentProvider());
	}

}
