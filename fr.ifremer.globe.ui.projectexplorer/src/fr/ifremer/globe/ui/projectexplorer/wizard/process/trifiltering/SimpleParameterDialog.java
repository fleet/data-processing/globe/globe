/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.trifiltering;

import java.util.function.Consumer;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.processes.filtri.model.FiltTriParameters;

/**
 * Dialog to present the parameters.
 */
public class SimpleParameterDialog extends Dialog {

	/** Consumer of FiltTriParameters. */
	protected Consumer<FiltTriParameters> consumer;

	/** Parameters to edit. */
	protected FiltTriParameters parameters;

	/** Parameters panel. */
	protected FiltTriParamMain filtTriParamMain;

	/**
	 * Constructor.
	 */
	public SimpleParameterDialog(FiltTriParameters parameters, Shell parentShell) {
		super(parentShell);
		parentShell.setText("FiltTri Parameters");
		this.parameters = parameters;
	}

	/**
	 * Follow the link.
	 * 
	 * @see org.eclipse.jface.dialogs.PopupDialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public Control createDialogArea(Composite parent) {
		Composite result = (Composite) super.createDialogArea(parent);
		this.filtTriParamMain = new FiltTriParamMain(this.parameters);
		this.filtTriParamMain.createComposite(result);
		if (this.parameters.getProperty(FiltTriParameters.PROPERTY_FILTERING_OPTION) == null)
			this.filtTriParamMain.loadData();
		else
			this.filtTriParamMain.loadDataFromParameter();
		return result;
	}

	/**
	 * Follow the link.
	 * 
	 * @see org.eclipse.jface.window.Window#setShellStyle(int)
	 */
	@Override
	protected void setShellStyle(int newShellStyle) {
		super.setShellStyle(SWT.CLOSE | SWT.MODELESS | SWT.BORDER | SWT.TITLE);
	}

	/**
	 * Follow the link.
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);
		getButton(IDialogConstants.OK_ID).setText("Run");
		getButton(IDialogConstants.CANCEL_ID).setText("Close");
	}

	/**
	 * Follow the link.
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	public void okPressed() {
		setReturnCode(OK);
		this.filtTriParamMain.saveData();
		if (this.consumer != null)
			this.consumer.accept(this.parameters);
	}

	/**
	 * Follow the link.
	 * 
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Parameters of Filter by triangulation");
	}

	/**
	 * @return the {@link #consumer}
	 */
	public Consumer<FiltTriParameters> getConsumer() {
		return this.consumer;
	}

	/**
	 * @param consumer the {@link #consumer} to set
	 */
	public void setConsumer(Consumer<FiltTriParameters> consumer) {
		this.consumer = consumer;
	}

	/**
	 * Follow the link.
	 * 
	 * @see org.eclipse.jface.window.Window#open()
	 */
	@Override
	public int open() {
		setBlockOnOpen(false);
		return super.open();
	}

}
