package fr.ifremer.globe.ui.projectexplorer.wizard.process.trifiltering;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.processes.filtri.model.FiltTriParameters;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;

public class TriFilteringWizardModel extends ConvertWizardDefaultModel {

	private Projection projection = new Projection(StandardProjection.MERCATOR);
	private FiltTriParameters filtTriParameters = new FiltTriParameters();
	private static final String DEFAULT_FILTRI_SUFFIX = "filtri";

	public TriFilteringWizardModel() {
		super();

		IFileService fileService = IFileService.grab();
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.getSounderNcContentType()));

		inputFiles.addChangeListener(e -> {
			List<GeoBox> infos = inputFiles.stream()//
					.map(File::getAbsolutePath)//
					.map(fileService::getFileInfo) //
					.filter(Optional::isPresent)//
					.map(Optional::get)//
					.filter(info -> info.getContentType().isSounderNcInfo())//
					.map(IFileInfo::getGeoBox)//
					.filter(Objects::nonNull)//
					.collect(Collectors.toList());
			filtTriParameters.setGeobox(GeoBox.englobe(infos));
		});

		suffix.set(DEFAULT_FILTRI_SUFFIX);
	}

	public Projection getProjection() {
		return projection;
	}

	public void setProjection(Projection projection) {
		this.projection = projection;
	}

	public FiltTriParameters getFilTriParameters() {
		return filtTriParameters;
	}
}
