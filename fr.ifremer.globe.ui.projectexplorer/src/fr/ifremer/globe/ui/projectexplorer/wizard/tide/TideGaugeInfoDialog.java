package fr.ifremer.globe.ui.projectexplorer.wizard.tide;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.model.tide.TideGaugeInfo;
import fr.ifremer.globe.core.utils.latlon.LatLongFormater;
import org.eclipse.swt.widgets.Link;
import org.eclipse.wb.swt.SWTResourceManager;

public class TideGaugeInfoDialog extends TitleAreaDialog {

	private final TideGaugeInfo tideGaugeInfo;

	public TideGaugeInfoDialog(Shell parentShell, TideGaugeInfo tideGaugeInfo) {
		super(parentShell);
		this.tideGaugeInfo = tideGaugeInfo;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		getShell().setText("Tide gauge properties");

		Composite area = (Composite) super.createDialogArea(parent);
		setTitle(tideGaugeInfo.name() + " (" + tideGaugeInfo.shomId() + ")");
		setMessage("Latitude : " + LatLongFormater.latitudeToString(tideGaugeInfo.latitude()) + " ; Longitude : "
				+ LatLongFormater.longitudeToString(tideGaugeInfo.longitude()));

		var composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);
		composite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		var stateLabel = new Label(composite, SWT.NONE);
		stateLabel.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		stateLabel.setText("State : ");
		var stateValue = new Label(composite, SWT.NONE);
		stateValue.setText(tideGaugeInfo.state());

		var verticalRefLabel = new Label(composite, SWT.NONE);
		verticalRefLabel.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		verticalRefLabel.setText("Vertical ref :");
		var verticalRefValue = new Label(composite, SWT.NONE);
		verticalRefValue.setText(tideGaugeInfo.verticalRef().toString());

		Label infoLinkLabel = new Label(composite, SWT.NONE);
		infoLinkLabel.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		infoLinkLabel.setText("Description :");
		Link link = new Link(composite, SWT.NONE);
		link.setText("<a>" + tideGaugeInfo.detailsLink() + "</a>");
		link.addListener(SWT.Selection, e -> org.eclipse.swt.program.Program.launch(tideGaugeInfo.detailsLink()));

		Label label = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));

		Label detailsLabel = new Label(composite, SWT.NONE);
		detailsLabel.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		detailsLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		detailsLabel.setText("Details :");

		var commentsText = new Text(composite, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL);
		commentsText.setEditable(false);
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		data.widthHint = 750;
		data.horizontalSpan = 2;
		data.heightHint = 450;
		commentsText.setLayoutData(data);
		commentsText.setText(tideGaugeInfo.details());

		return area;
	}

}