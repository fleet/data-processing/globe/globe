package fr.ifremer.globe.ui.projectexplorer.wizard;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.core.processes.TimeIntervalParameters;
import fr.ifremer.globe.core.runtime.gws.GwsServiceAgent;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.databinding.validation.FileValidator;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.ui.widget.FileComposite;
import fr.ifremer.globe.ui.widget.FileComposite.SelectFileCompositeModel;
import fr.ifremer.globe.ui.widget.datetime.InstantWidget;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.osgi.OsgiUtils;

public class TimeIntervalParamPage extends WizardPage {

	private TimeIntervalParameters model;
	/**
	 * Input files specified in the Wizard. <br>
	 * List is present when cut file generation from a Shapefile is allowed
	 */
	private final Optional<List<File>> inputFiles;

	/** Inner composites **/
	private Button cutFileOptionButton;
	private Button customIntervalOptionButton;
	private Button noIntervalOptionButton;
	private Label startLabel;
	private Label endLabel;
	private InstantWidget startDateWidget;
	private InstantWidget endDateWidget;
	private FileComposite fileComposite;
	private WritableFile writableFile = new WritableFile();
	private SelectFileCompositeModel selectCutFileCompositeModel = new SelectFileCompositeModel() {

		@Override
		public List<Pair<String, String>> getSelectedFileFilterExtensions() {
			return Collections.singletonList(new Pair<>("Cut file", "*.cut"));
		}

		@Override
		public String getSelectedFileFilterExtension() {
			return "*.cut";
		}

		@Override
		public WritableFile getSelectedFile() {
			return writableFile;
		}

		@Override
		public int getFileRequirements() {
			return FileValidator.FILE;
		}
	};
	private Button fromShpFileBtn;

	/**
	 * @wbp.parser.constructor
	 */
	public TimeIntervalParamPage(TimeIntervalParameters parameters) {
		this(parameters, null);
	}

	public TimeIntervalParamPage(TimeIntervalParameters parameters, List<File> inputFiles) {
		super("");
		model = parameters;
		this.inputFiles = Optional.ofNullable(inputFiles);
		setTitle("Select time interval option");
		setDescription("Select a time interval to define which swath to process.");
	}

	@Override
	public void createControl(Composite parent) {
		var container = new Composite(parent, SWT.NULL);
		setControl(container);
		container.setLayout(new GridLayout(1, false));

		// with cut file
		cutFileOptionButton = new Button(container, SWT.RADIO);
		cutFileOptionButton.setText("Time interval(s) from Cut file");
		cutFileOptionButton.addListener(SWT.Selection, evt -> refresh());

		Composite cutFileComposite = new Composite(container, SWT.NONE);
		cutFileComposite.setLayout(new GridLayout(2, false));
		cutFileComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		fileComposite = new FileComposite(cutFileComposite, SWT.NONE, selectCutFileCompositeModel);
		writableFile.addChangeListener(e -> refresh());

		// InputFiles is present
		if (inputFiles.isPresent()) {
			fromShpFileBtn = new Button(cutFileComposite, SWT.PUSH);
			fromShpFileBtn.setText("From Shp/Kml");
			fromShpFileBtn.setImage(Icons.POLYGON.toImage());
			fromShpFileBtn.addSelectionListener(SelectionListener.widgetSelectedAdapter(e -> generateCutFile()));
		}

		// manual custom interval
		customIntervalOptionButton = new Button(container, SWT.RADIO);
		customIntervalOptionButton.setText("Custom time interval");
		customIntervalOptionButton.addListener(SWT.Selection, evt -> refresh());

		var manuallyComposite = new Composite(container, SWT.NONE);
		manuallyComposite.setLayout(new GridLayout(5, false));
		new Label(manuallyComposite, SWT.NONE);

		startLabel = new Label(manuallyComposite, SWT.NONE);
		startLabel.setText("Start :");
		startDateWidget = new InstantWidget(manuallyComposite, SWT.NONE, Instant.now());

		endLabel = new Label(manuallyComposite, SWT.NONE);
		endLabel.setText("end :");
		endDateWidget = new InstantWidget(manuallyComposite, SWT.NONE, Instant.now());

		// no time interval
		noIntervalOptionButton = new Button(container, SWT.RADIO);
		noIntervalOptionButton.setText("No time interval (correction on selected files)");
		noIntervalOptionButton.addListener(SWT.Selection, evt -> refresh());

		// restore default
		var restoreDefaultButton = new Button(container, SWT.NONE);
		restoreDefaultButton.setText("Restore Default");
		restoreDefaultButton.setLayoutData(new GridData(SWT.RIGHT, SWT.BOTTOM, true, true));
		restoreDefaultButton.addListener(SWT.Selection, evt -> {
			model.restoreDefault();
			loadData();
		});

		loadData();
	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		if (visible)
			onEnterPage();
		else
			onExitPage();
	}

	private void onEnterPage() {
		loadData();
	}

	private void onExitPage() {
		saveData();
	}

	private void refresh() {
		if (cutFileOptionButton.getSelection()) {
			if (writableFile.doGetValue() == null) {
				setErrorMessage("Cut file (.cut) is required.");
				setPageComplete(false);
			} else if (!writableFile.doGetValue().exists()) {
				setErrorMessage("Cut file doesn't exist.");
				setPageComplete(false);
			} else {
				model.setProperty(TimeIntervalParameters.PROPERTY_CUT_FILE_NAME,
						writableFile.doGetValue().getAbsolutePath());
				setErrorMessage(null);
				setPageComplete(true);
			}
		} else {
			setErrorMessage(null);
			setPageComplete(true);
		}
		fileComposite.setEnabled(cutFileOptionButton.getSelection());
		if (fromShpFileBtn != null)
			fromShpFileBtn.setEnabled(cutFileOptionButton.getSelection());

		startLabel.setEnabled(customIntervalOptionButton.getSelection());
		startDateWidget.setEnabled(customIntervalOptionButton.getSelection());
		endLabel.setEnabled(customIntervalOptionButton.getSelection());
		endDateWidget.setEnabled(customIntervalOptionButton.getSelection());
	}

	private void loadData() {
		// time Interval Option
		cutFileOptionButton.setSelection(model.getBoolProperty(TimeIntervalParameters.PROPERTY_CUT_OPTION));
		customIntervalOptionButton.setSelection(model.getBoolProperty(TimeIntervalParameters.PROPERTY_MANUALLY_OPTION));
		noIntervalOptionButton
				.setSelection(!cutFileOptionButton.getSelection() && !customIntervalOptionButton.getSelection());

		// cut file
		String fileName = model.getProperty(TimeIntervalParameters.PROPERTY_CUT_FILE_NAME);
		if (fileName != null && !fileName.isEmpty())
			writableFile.set(new File(fileName));

		// date
		startDateWidget.setInstant(
				Instant.ofEpochMilli(model.getLongProperty(TimeIntervalParameters.PROPERTY_STARTDATETIME_VALUE)));
		endDateWidget.setInstant(
				Instant.ofEpochMilli(model.getLongProperty(TimeIntervalParameters.PROPERTY_ENDDATETIME_VALUE)));

		refresh();
	}

	public void saveData() {
		// time Interval Option
		model.setBoolProperty(TimeIntervalParameters.PROPERTY_CUT_OPTION, cutFileOptionButton.getSelection());
		model.setBoolProperty(TimeIntervalParameters.PROPERTY_MANUALLY_OPTION,
				customIntervalOptionButton.getSelection());
		model.setBoolProperty(TimeIntervalParameters.PROPERTY_NOINTERVAL_OPTION, noIntervalOptionButton.getSelection());

		// cut File Name
		if (cutFileOptionButton.getSelection() && writableFile.doGetValue() != null
				&& writableFile.doGetValue().exists())
			model.setProperty(TimeIntervalParameters.PROPERTY_CUT_FILE_NAME,
					writableFile.doGetValue().getAbsolutePath());

		// custom dates
		model.setLongProperty(TimeIntervalParameters.PROPERTY_STARTDATETIME_VALUE,
				startDateWidget.getInstant().toEpochMilli());
		model.setLongProperty(TimeIntervalParameters.PROPERTY_ENDDATETIME_VALUE,
				endDateWidget.getInstant().toEpochMilli());
	}

	/** Generate a temporary cut file using a Shapefile */
	private void generateCutFile() {

		try {
			List<String> lines = List.of();
			GwsServiceAgent gwsServiceAgent = OsgiUtils.getService(GwsServiceAgent.class);
			if (gwsServiceAgent != null)
				lines = gwsServiceAgent.computeCutFileFromMbgMask(inputFiles.get());
			else
				Messages.openInfoMessage(getTitle(), "No service avalaible to produce cut file");

			var cutFile = TemporaryCache.createTemporaryFile("Lines_", ".cut");
			var lineCount = lines.size();
			if (lineCount == 0) {
				Messages.openWarningMessage(getTitle(), "No cut line found");
				return;
			} else if (lineCount == 1)
				Messages.openInfoMessage(getTitle(), "One cut line has been produced in \n" + cutFile.getName());
			else
				Messages.openInfoMessage(getTitle(),
						String.valueOf(lineCount) + " cut lines have been produced in \n" + cutFile.getName());

			Files.write(cutFile.toPath(), lines, StandardOpenOption.WRITE);
			writableFile.set(new File(cutFile.getAbsolutePath()));

		} catch (GIOException | IOException e) {
			Messages.openWarningMessage(getTitle(), "Unable to create the cut file. \n" + e.getMessage());
		}

	}

}
