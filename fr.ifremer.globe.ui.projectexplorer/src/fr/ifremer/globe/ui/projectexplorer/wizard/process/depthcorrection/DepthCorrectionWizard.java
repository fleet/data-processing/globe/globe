package fr.ifremer.globe.ui.projectexplorer.wizard.process.depthcorrection;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.projectexplorer.wizard.TimeIntervalParamPage;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Wizard to apply depth correction
 */
public class DepthCorrectionWizard extends Wizard {

	/** Model */
	protected DepthCorrectionWizardModel depthCorrectionWizardModel;

	/** Pages */
	protected SelectInputFilesPage selectInputFilesPage;
	protected DepthCorrectionSelectCorrFilePage selectTideFilePage;
	protected TimeIntervalParamPage timeIntervalParamPage;
	protected SelectOutputParametersPage selectOutputParametersPage;
	
	/**
	 * Constructor
	 */
	public DepthCorrectionWizard(DepthCorrectionWizardModel depthCorrectionWizardModel) {
		this.depthCorrectionWizardModel = depthCorrectionWizardModel;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(depthCorrectionWizardModel);
		addPage(selectInputFilesPage);
		selectTideFilePage = new DepthCorrectionSelectCorrFilePage(depthCorrectionWizardModel);
		addPage(selectTideFilePage);
		timeIntervalParamPage = new TimeIntervalParamPage(depthCorrectionWizardModel.getTimeIntervalParameters());
		addPage(timeIntervalParamPage);
		selectOutputParametersPage = new SelectOutputParametersPage(depthCorrectionWizardModel);
		addPage(selectOutputParametersPage);
	}
	
	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		selectTideFilePage.savePreferences();
		depthCorrectionWizardModel.getDepthCorrectionParameters().save();
		timeIntervalParamPage.saveData();
		depthCorrectionWizardModel.getTimeIntervalParameters().save();
		return selectInputFilesPage.isPageComplete() && selectOutputParametersPage.isPageComplete();
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		return depthCorrectionWizardModel.getInputFiles().isEmpty() ? selectInputFilesPage : selectTideFilePage;
	}

}
