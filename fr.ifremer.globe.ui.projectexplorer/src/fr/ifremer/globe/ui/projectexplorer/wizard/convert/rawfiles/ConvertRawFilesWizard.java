package fr.ifremer.globe.ui.projectexplorer.wizard.convert.rawfiles;

import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Wizard to convert sounder files
 */
public class ConvertRawFilesWizard extends Wizard {

	/** Model */
	protected ConvertRawFilesWizardModel convertRawFilesWizardModel;

	/** Pages */
	protected SelectInputFilesPage selectInputFilesPage;
	protected SelectOutputFormatPage selectOutputFormatPage;
	protected SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public ConvertRawFilesWizard(ConvertRawFilesWizardModel convertSounderFilesWizardModel) {
		convertRawFilesWizardModel = convertSounderFilesWizardModel;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(convertRawFilesWizardModel);
		addPage(selectInputFilesPage);
		selectOutputFormatPage = new SelectOutputFormatPage(convertRawFilesWizardModel);
		addPage(selectOutputFormatPage);
		selectOutputParametersPage = new SelectOutputParametersPage(convertRawFilesWizardModel);
		selectOutputParametersPage.setOutputFileNameComputer(new XsfFileNameComputer(convertRawFilesWizardModel));
		addPage(selectOutputParametersPage);

		// Recompute the output filenames when XsfReferenceFolder changed
		IChangeListener listener = e -> selectOutputParametersPage.requestOutputFilesComputation();
		convertRawFilesWizardModel.getXsfReferenceFolder().addChangeListener(listener);
		convertRawFilesWizardModel.getXsfReferenceFolder().addDisposeListener(
				e -> convertRawFilesWizardModel.getXsfReferenceFolder().removeChangeListener(listener));
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		return selectInputFilesPage.isPageComplete() && selectOutputFormatPage.isPageComplete()
				&& selectOutputParametersPage.isPageComplete();
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		if (convertRawFilesWizardModel.getInputFiles().isEmpty())
			return super.getStartingPage();
		return selectOutputFormatPage;
	}
}
