package fr.ifremer.globe.ui.projectexplorer.wizard.process.merge;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.projectexplorer.wizard.TimeIntervalParamPage;

/**
 * Merge wizard
 */
public class MergeWizard extends Wizard {

	/** Model */
	protected MergeWizardModel mergeWizardModel;

	/** Pages */
	protected MergeWizardInputPage selectInputFilesPage;
	protected TimeIntervalParamPage timeIntervalParamPage;
	protected MergeWizardOutputPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public MergeWizard(MergeWizardModel mergeWizardModel) {
		this.mergeWizardModel = mergeWizardModel;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new MergeWizardInputPage(mergeWizardModel);
		addPage(selectInputFilesPage);
		timeIntervalParamPage = new TimeIntervalParamPage(mergeWizardModel.getTimeIntervalParameters(),
				mergeWizardModel.getInputFiles());
		addPage(timeIntervalParamPage);
		selectOutputParametersPage = new MergeWizardOutputPage(mergeWizardModel);
		addPage(selectOutputParametersPage);
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		timeIntervalParamPage.saveData();
		mergeWizardModel.getTimeIntervalParameters().save();
		return selectInputFilesPage.isPageComplete() && selectOutputParametersPage.isPageComplete();
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		return mergeWizardModel.getInputFiles().isEmpty() || !mergeWizardModel.getInputFilesErrorMessage().isEmpty()
				? selectInputFilesPage
						: timeIntervalParamPage;
	}
}
