package fr.ifremer.globe.ui.projectexplorer.wizard.process.trifiltering;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

public class TriFilteringWizard extends Wizard {
	private TriFilteringWizardModel model;

	/* Pages */
	private SelectInputFilesPage selectInputFilesPage;
	private FiltTriParamMain filtTriParamPage;
	private ProjectionPage projectionPage;
	private SelectOutputParametersPage selectOutputParametersPage;

	public TriFilteringWizard(TriFilteringWizardModel model) {
		super();
		this.model = model;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(model);
		addPage(selectInputFilesPage);
		filtTriParamPage = new FiltTriParamMain(model.getFilTriParameters());
		addPage(filtTriParamPage);
		projectionPage = new ProjectionPage("Projection type");

		addPage(projectionPage);
		selectOutputParametersPage = new SelectOutputParametersPage(model);
		addPage(selectOutputParametersPage);

		// Necessary if startingPage is "selectInputFilesPage": unknown input files now
		model.getInputFiles().addChangeListener(e -> updatePageProjection());

		// Necessary if startingPage is "filtTriParamPage": known input files
		updatePageProjection();
	}

	@Override
	public boolean performFinish() {
		filtTriParamPage.saveData();
		return filtTriParamPage.isPageComplete() && projectionPage.isPageComplete();
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		return model.getInputFiles().isEmpty() ? selectInputFilesPage : filtTriParamPage;
	}

	protected void updatePageProjection() {
		Display.getDefault().asyncExec(() -> {
			projectionPage.setProjection(model.getProjection());
			projectionPage.initializeControls(model.getFilTriParameters().getGeoBox());
		});
	}

}
