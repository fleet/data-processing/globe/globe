package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.handlers;

import java.util.EnumSet;
import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.runtime.datacontainer.layers.walker.LayersWalker;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.ConvertToCsvProcessWithWizard;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.ConvertToCsvWizardModel;

public class ConvertNviToCsvWizardHandler extends AbstractOpenProcessWizardHandler<ConvertToCsvProcessWithWizard> {

	@Inject
	protected LayersWalker layersWalker;

	@Override
	protected EnumSet<ContentType> getAcceptedContentType() {
		return EnumSet.of(ContentType.NVI_NETCDF_4);
	}

	@Override
	protected ConvertToCsvProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		var wizard = new ConvertToCsvProcessWithWizard(shell, layersWalker, getAcceptedContentType(),
				ConvertToCsvWizardModel.CSV_EXTENSION);
		wizard.getModel().setPositiveElevation(Optional.of(new WritableBoolean())); // enable elevation sign option
		return wizard;
	}

}