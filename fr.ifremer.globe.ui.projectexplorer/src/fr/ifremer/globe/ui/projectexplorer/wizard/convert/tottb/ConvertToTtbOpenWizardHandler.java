package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tottb;

import java.util.EnumSet;
import java.util.Set;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

/**
 * Handler class to open the process wizard (referenced in fragment.e4xmi).
 */
public class ConvertToTtbOpenWizardHandler extends AbstractOpenProcessWizardHandler<ConvertToTtbProcessWithWizard> {

	@Override
	protected Set<ContentType> getAcceptedContentType() {
		return EnumSet.of(ContentType.TIDE_CSV);
	}

	@Override
	protected ConvertToTtbProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new ConvertToTtbProcessWithWizard(shell);
	}

}
