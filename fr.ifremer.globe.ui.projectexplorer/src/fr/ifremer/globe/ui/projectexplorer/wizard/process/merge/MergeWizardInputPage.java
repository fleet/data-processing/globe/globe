package fr.ifremer.globe.ui.projectexplorer.wizard.process.merge;

import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;

/**
 * This class extends the {@link SelectInputFilesPage} to add an error message.
 */
public class MergeWizardInputPage extends SelectInputFilesPage {

	/**
	 * Constructor
	 */
	public MergeWizardInputPage(MergeWizardModel model) {
		super(model);
		model.getInputFilesErrorMessage().addChangeListener(e -> updateInputeFilesPageErrorMessage());
		updateInputeFilesPageErrorMessage();
	}

	/**
	 * Updates error message.
	 */
	private void updateInputeFilesPageErrorMessage() {
		if (!((MergeWizardModel) selectInputFilesPageModel).getInputFilesErrorMessage().isEmpty())
			setErrorMessage(((MergeWizardModel) selectInputFilesPageModel).getInputFilesErrorMessage().get());
		else
			setErrorMessage(null);
	}

	@Override
	public boolean canFlipToNextPage() {
		return getErrorMessage() == null && super.canFlipToNextPage();
	}

	@Override
	public boolean isPageComplete() {
		return getErrorMessage() == null && super.isPageComplete();
	}
}
