package fr.ifremer.globe.ui.projectexplorer.wizard.process.merge;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang.math.LongRange;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.core.model.profile.Profile;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;
import fr.ifremer.globe.utils.exception.FileFormatException;

/**
 * This class extends {@link SelectOutputParametersPage} to override the method which compute output files.
 */
public class MergeWizardOutputPage extends SelectOutputParametersPage {

	/**
	 * Constructor
	 */
	public MergeWizardOutputPage(MergeWizardModel model) {
		super(model);
	}

	/**
	 * Forces the compute of output files.
	 */
	@Override
	public boolean isPageComplete() {
		computeOutputFiles();
		return getErrorMessage() == null && super.isPageComplete();
	}

	/**
	 * Override of output file compute method.
	 */
	@Override
	protected void computeOutputFiles() {
		MergeWizardModel model = ((MergeWizardModel) selectOutputParametersPageModel);
		List<File> inputFiles = selectOutputParametersPageModel.getInputFiles().asList();
		Map<File, Optional<LongRange>> outputFilesWithTimeInterval = new HashMap<>();

		
		// Compute output files with time intervals
		int nbOfFile = 0;
		if (!inputFiles.isEmpty()) {
			
			// Set OutputDirectory if needed
			selectOutputParametersPageModel.getOutputDirectory().setIfNull(inputFiles.get(0)::getParentFile);

			String extension = model.getInputFilesFilterExtensions().get(0).getSecond().replace("*.", "");

			Optional<Set<Profile>> optTimeProfiles;
			try {
				optTimeProfiles = model.getTimeProfileSet();
			} catch (NullPointerException | IOException | FileFormatException e) {
				setErrorMessage("Error while computing output files from \".cut\" file.");
				logger.error(getErrorMessage(), e);
				model.setOutputFilesWithTimeIntervals(outputFilesWithTimeInterval);
				Display.getDefault().asyncExec(() -> selectOutputParametersPageModel.getOutputFiles().clear());
				return;
			}
			setErrorMessage(null); // reset error message

			if (optTimeProfiles.isPresent()) {
				for (Profile p : optTimeProfiles.get()) {
					// if several profiles: no output single file name
					if (optTimeProfiles.get().size() > 1)
						selectOutputParametersPageModel.getSingleOutputFilename().set("");
					File outputFile = outputFileNameComputer.compute(new File(p.getId()),
							selectOutputParametersPageModel, extension);
					outputFilesWithTimeInterval.put(outputFile,
							Optional.of(new LongRange(p.getStartDate(), p.getEndDate())));
					if (outputFile.exists())
						nbOfFile++;
				}
			} else {
				File outputFile = outputFileNameComputer.compute(new File("output_merge"),
						selectOutputParametersPageModel, extension);
				outputFilesWithTimeInterval.put(outputFile, Optional.empty());
				if (outputFile.exists())
					nbOfFile++;
			}
		}

		model.setOutputFilesWithTimeIntervals(outputFilesWithTimeInterval);

		// Refresh model and interface
		String nbOfFileToSet = nbOfFile > 0 ? String.format(WARNING_TEXT, nbOfFile) : "";
		Display.getDefault().asyncExec(() -> {
			if (!nbOfExistingFileLabel.isDisposed()) {
				nbOfExistingFileLabel.setText(nbOfFileToSet);
				nbOfExistingFileLabel.getParent().layout();
				ouputFilesTable.setRedraw(false);
				selectOutputParametersPageModel.getOutputFiles().clear();
				selectOutputParametersPageModel.getOutputFiles().addAll(outputFilesWithTimeInterval.keySet());
				ouputFilesTable.setRedraw(true);
				updateInputFileWidgets();
			}
		});
	}

}
