package fr.ifremer.globe.ui.projectexplorer.wizard.process.soundvelocitycorrection;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.processes.soundvelocitycorrection.process.SoundVelocityCorrectionType;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

/**
 * Handler class to open the process wizard (referenced in fragment.e4xmi).
 */
public class SoundVelocityCorrectionOpenWizardHandler
		extends AbstractOpenProcessWizardHandler<SoundVelocityCorrectionProcessWithWizard> {

	@Override
	protected SoundVelocityCorrectionProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new SoundVelocityCorrectionProcessWithWizard(shell, SoundVelocityCorrectionType.CORRECTION_FROM_MBG);
	}

}