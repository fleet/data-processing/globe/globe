package fr.ifremer.globe.ui.projectexplorer.wizard.tide;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jakarta.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.exception.OutOfRangeException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.tide.TideFileInfo;
import fr.ifremer.globe.core.io.tide.csv.InputTideProcessCsvFileWriter;
import fr.ifremer.globe.core.io.tide.csv.TideCsvFileInfoSupplier;
import fr.ifremer.globe.core.io.tide.ttb.TtbFileInfoSupplier;
import fr.ifremer.globe.core.model.navigation.NavigationPoint;
import fr.ifremer.globe.core.model.tide.ITideData;
import fr.ifremer.globe.core.model.tide.TideGauge;
import fr.ifremer.globe.core.model.tide.TideGaugeSource;
import fr.ifremer.globe.core.runtime.gws.GwsServiceAgent;
import fr.ifremer.globe.core.runtime.gws.param.PredictFesHarmonicTideParams;
import fr.ifremer.globe.core.runtime.gws.param.PredictShomHarmonicTideParams;
import fr.ifremer.globe.core.utils.color.GColor;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.projectexplorer.Activator;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.widget.CheckBoxGroup;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GException;
import fr.ifremer.globe.utils.exception.GIOException;

public class TidePredictionComposite extends Composite {

	private static final Logger LOGGER = LoggerFactory.getLogger(TidePredictionComposite.class);

	private static final String FES2014_PYAT_PROCESS = "FES2014 harmonic tide predictor";
	private static final String SHOM_PYAT_PROCESS = "Shom harmonic tide predictor";

	private boolean isFesModelDirAvailable;
	private File fesModelDirFile;

	private boolean isShomHarmonicPrefAvailable;
	private File shomHarmonicPrefFile;

	@Inject
	TideCsvFileInfoSupplier tideCsvFileInfoSupplier;
	@Inject
	GwsServiceAgent gwsServiceAgent;
	@Inject
	IGeographicViewService geographicView;

	/** Properties **/
	private final ComputeTideWizardModel model;

	/** Inner widgets **/
	private Button radioButtonShom;
	private Button radioButtonFes;
	private CheckBoxGroup grpSurge;
	private TideGaugeComposite tideGaugeWidget;

	/**
	 * Constructor
	 */
	public TidePredictionComposite(Composite parent, int style, ComputeTideWizardModel wizardModel) {
		super(parent, style);
		model = wizardModel;
		ContextInitializer.inject(this);
		ComputeTidePreferences preferences = Activator.getComputeTidePreferences();
		try {
			// setup fes2014
			fesModelDirFile = preferences.getTideModelsDir().getValue().toFile();
			isFesModelDirAvailable = fesModelDirFile.exists() && fesModelDirFile.isDirectory()
					&& Stream.of(fesModelDirFile.listFiles()).anyMatch(file -> file.getName().endsWith("fes2014"));
			// setup shom
			shomHarmonicPrefFile = preferences.getShomHarmonicFile().getValue().toFile();
			isShomHarmonicPrefAvailable = shomHarmonicPrefFile.exists()
					&& shomHarmonicPrefFile.getName().endsWith(".har");
		} catch (NoSuchElementException e) {
			// error getting pyAt process : just display error message
			setLayout(new GridLayout(1, false));
			Label lblError = new Label(this, SWT.NONE);
			lblError.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
			lblError.setText("Compute tide prediction process not available (PyAT project not compatible).");
			return;
		}

		createUI();
		intialize();
	}

	/**
	 * Builds the UI.
	 */
	public void createUI() {
		setLayout(new GridLayout(1, false));

		// prediction model selection
		Group grpPredictionModel = new Group(this, SWT.NONE);
		grpPredictionModel.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpPredictionModel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpPredictionModel.setLayout(new GridLayout(6, false));
		grpPredictionModel.setText("Prediction model");

		radioButtonShom = new Button(grpPredictionModel, SWT.RADIO);
		radioButtonShom.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		radioButtonShom.setText("SHOM");

		var buttonHelpShom = new Button(grpPredictionModel, SWT.NONE);
		buttonHelpShom.setToolTipText("Show SHOM prediction help");
		buttonHelpShom
				.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.ui.projectexplorer", "icons/16/help.png"));
		buttonHelpShom.addListener(SWT.Selection, e -> gwsServiceAgent.openHelp(SHOM_PYAT_PROCESS));

		Label lblNewLabel = new Label(grpPredictionModel, SWT.NONE);
		lblNewLabel.setText("    ");

		radioButtonFes = new Button(grpPredictionModel, SWT.RADIO);
		radioButtonFes.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		radioButtonFes.setText("FES2014");

		var buttonHelpFes = new Button(grpPredictionModel, SWT.NONE);
		buttonHelpFes.setToolTipText("Show FES2014 prediction help");
		buttonHelpFes
				.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.ui.projectexplorer", "icons/16/help.png"));
		buttonHelpFes.addListener(SWT.Selection, e -> gwsServiceAgent.openHelp(FES2014_PYAT_PROCESS));

		Label lblTips = new Label(grpPredictionModel, SWT.WRAP);
		lblTips.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
		lblTips.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		lblTips.setText("Harmonic/model files must be defined in preferences to get model available.");

		grpSurge = new CheckBoxGroup(this, SWT.NONE);
		grpSurge.setText("Compute surge (difference between tide gauge observation and prediction)");
		grpSurge.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpSurge.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpSurge.setLayout(new GridLayout(1, false));

		tideGaugeWidget = new TideGaugeComposite(grpSurge.getContent(), SWT.NONE, model);
		tideGaugeWidget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		grpSurge.addListener(SWT.Selection, e -> tideGaugeWidget.setEnabled(grpSurge.getSelection()));
		grpSurge.setSelection(false);
	}

	private void intialize() {
		radioButtonShom.setEnabled(isShomHarmonicPrefAvailable);
		radioButtonShom.setSelection(isShomHarmonicPrefAvailable);
		radioButtonFes.setEnabled(isFesModelDirAvailable);
		radioButtonFes.setSelection(!isShomHarmonicPrefAvailable && isFesModelDirAvailable);
	}

	@Override
	public Point computeSize(int wHint, int hHint, boolean changed) {
		if (isVisible()) {
			return super.computeSize(wHint, hHint, changed);
		}
		return new Point(0, 0);
	}

	/**
	 * Creates a process to compute tide prediction with selected parameters.
	 */
	public Optional<ComputeTideProcess> prepareCompute() {
		if (radioButtonShom == null || !radioButtonShom.getSelection() && !radioButtonFes.getSelection())
			return Optional.empty();
		boolean withSurge = grpSurge.getSelection();

		// which process to launch ?
		Optional<String> serviceName = Optional.ofNullable(radioButtonShom.getSelection() ? SHOM_PYAT_PROCESS
				: radioButtonFes.getSelection() ? FES2014_PYAT_PROCESS : null);

		return Optional.of(new ComputeTideProcess() {
			List<ITideData> predictionTide = Collections.emptyList();
			List<ITideData> surgeData = Collections.emptyList();
			Optional<TideGauge> selectedTideGauge = tideGaugeWidget.getSelectedTideGauge();
			TideGaugeSource tideGaugeSource = tideGaugeWidget.getSelectedTideGaugeSource();

			@Override
			List<ITideData> computesTide(IProgressMonitor monitor) throws IOException, GException {
				// compute prediction input Files
				predictionTide = computeTidePrediction(model.getLocalisation(), serviceName, monitor);
				if (predictionTide.isEmpty() || !withSurge) {
					return predictionTide;
				}

				// compute surge
				try {
					if (selectedTideGauge.isPresent())
						surgeData = computeTideGaugeSurge(selectedTideGauge.get(), tideGaugeSource, serviceName,
								monitor);
				} catch (GIOException e) {
					throw GIOException.wrap("Surge can not be computed.", e);
				}
				// add surge to prediction
				var xArray = new double[surgeData.size()];
				var yArray = new double[surgeData.size()];
				for (var i = 0; i < surgeData.size(); i++) {
					xArray[i] = surgeData.get(i).getTimestamp().toEpochMilli();
					yArray[i] = surgeData.get(i).getValue();
				}
				var interpolator = new LinearInterpolator().interpolate(xArray, yArray);

				List<ITideData> predictionPlusSurge = new ArrayList<>();
				for (ITideData tideData : predictionTide) {
					try {
						double value = tideData.getValue() + interpolator.value(tideData.getTimestamp().toEpochMilli());
						predictionPlusSurge.add(ITideData.build(tideData.getTimestamp(), value));
					} catch (OutOfRangeException e) {
						// out of range data are ignored
					}
				}
				return predictionPlusSurge;
			}

			@Override
			void onFinished() {
				String predictionName = radioButtonShom.getSelection() ? "shom_prediction" : "fes2014_prediction";
				model.setTideData(result);
				model.getTideDataTitle().set(String.format("%s from %s to %s %s", predictionName, model.getStart(),
						model.getEnd(), model.getIntervalInMinutes().map(i -> "(interval : " + i + ")").orElse("")));
				if (selectedTideGauge.isPresent() && !predictionTide.isEmpty() && withSurge) {
					model.addDataToDisplay("Surge computed from " + selectedTideGauge.get().getName() + "", surgeData,
							GColor.LIGHT_ORANGE);
					model.addDataToDisplay("Prediction", predictionTide, GColor.LIGHT_GREEN);
				}

				// update output filename
				Function<Instant, String> formatDate = instant -> instant.toString().split("\\.")[0].replace(":", "-");
				model.setSingleOutputFilename(
						String.format("%s_%s_%s.%s", predictionName.toUpperCase(), formatDate.apply(model.getStart()),
								formatDate.apply(model.getEnd()), TtbFileInfoSupplier.EXTENSION_TTB));

			}
		});
	}

	private List<ITideData> computeTideGaugeSurge(TideGauge tideGauge, TideGaugeSource tideGaugeSource,
			Optional<String> serviceName, IProgressMonitor monitor) throws IOException, GException {

		// get observed data
		var tideGaugeObservationData = tideGaugeWidget.compute(tideGauge, tideGaugeSource);
		if (tideGaugeObservationData.isEmpty())
			throw new GIOException("Observation data missing at " + tideGauge.getName());

		// get predicted data
		List<NavigationPoint> tideGaugePositions = tideGaugeObservationData.stream()
				.map(tideData -> new NavigationPoint(tideGauge.getLongitude(), tideGauge.getLatitude(),
						tideData.getTimestamp()))
				.collect(Collectors.toList());
		List<ITideData> tideGaugePredictionData = computeTidePrediction(tideGaugePositions, serviceName, monitor);
		if (tideGaugeObservationData.isEmpty())
			throw new GIOException("Prediction not available at tide gauge : " + tideGauge.getName());

		// compute surge
		List<ITideData> result = new ArrayList<>();
		for (var i = 0; i < tideGaugeObservationData.size(); i++) {
			double surge = tideGaugeObservationData.get(i).getValue() - tideGaugePredictionData.get(i).getValue();
			result.add(ITideData.build(tideGaugePredictionData.get(i).getTimestamp(), surge));
		}
		return result;
	}

	private List<ITideData> computeTidePrediction(List<NavigationPoint> navPoints, Optional<String> serviceName,
			IProgressMonitor monitor) throws IOException, GException {

		File predictionInputFile = null;
		File outputFilePath = null;
		try {
			// create CSV file with input prediction data (position & time)
			predictionInputFile = TemporaryCache.createTemporaryFile("TIDE_PREDICTION_INPUT", ".csv");
			// get at least 3 points
			if (navPoints.size() < 2) {
				NavigationPoint point = navPoints.get(0);
				navPoints = List.of(
						new NavigationPoint(point.lon, point.lat, point.date.minusSeconds(model.getIntervalInSecond())),
						navPoints.get(0),
						new NavigationPoint(point.lon, point.lat, point.date.plusSeconds(model.getIntervalInSecond())));
			}
			InputTideProcessCsvFileWriter.write(predictionInputFile.getAbsolutePath(), navPoints);

			// launch prediction process
			outputFilePath = TemporaryCache.createTemporaryFile("TIDE_PREDICTION_OUTPUT", ".csv");
			if (serviceName.isPresent()) {
				if (SHOM_PYAT_PROCESS.equals(serviceName.get())) {
					computeShomTidePrediction(predictionInputFile, outputFilePath, monitor);
				} else {
					computeFesTidePrediction(predictionInputFile, outputFilePath, monitor);
				}
			}

			// get result
			TideFileInfo ttbFile = tideCsvFileInfoSupplier.getFileInfo(outputFilePath.getAbsolutePath())
					.orElseThrow(() -> new GIOException("Computed tide file not found."));
			return ttbFile.getData();
		} finally {
			// delete tmp files
			if (predictionInputFile != null)
				FileUtils.deleteQuietly(predictionInputFile);
			if (outputFilePath != null)
				FileUtils.deleteQuietly(outputFilePath);
		}
	}

	private void computeFesTidePrediction(File predictionInputFile, File outputFilePath, IProgressMonitor monitor)
			throws GException {
		var params = new PredictFesHarmonicTideParams(predictionInputFile, outputFilePath, fesModelDirFile);
		gwsServiceAgent.predictFesHarmonicTide(params, monitor, LOGGER);
	}

	private void computeShomTidePrediction(File predictionInputFile, File outputFilePath, IProgressMonitor monitor)
			throws GException {
		var params = new PredictShomHarmonicTideParams(predictionInputFile, outputFilePath, shomHarmonicPrefFile);
		gwsServiceAgent.predictShomHarmonicTide(params, monitor, LOGGER);
	}

	/**
	 * Opens help page of the current process.
	 */
	public void opendHelp() {
		if (radioButtonShom.getSelection())
			gwsServiceAgent.openHelp(SHOM_PYAT_PROCESS);
		else
			gwsServiceAgent.openHelp(FES2014_PYAT_PROCESS);
	}

}
