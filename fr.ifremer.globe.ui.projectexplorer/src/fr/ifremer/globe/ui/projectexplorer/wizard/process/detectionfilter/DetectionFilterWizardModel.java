package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.processes.detectionfilter.IAttributeProvider;
import fr.ifremer.globe.core.processes.detectionfilter.SounderDataContainerAttributeProvider;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.DataParameters;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;
import fr.ifremer.globe.utils.exception.GException;

public class DetectionFilterWizardModel extends ConvertWizardDefaultModel {

	private SounderDataContainerAttributeProvider attributeProvider;

	private List<SingleFilterParameters> criterias;
	private DataParameters dataToSet;

	private static final String DEFAULT_DETECTION_SUFFIX = "filt";

	public DetectionFilterWizardModel() {
		super();

		IFileService fileService = IFileService.grab();
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.getSounderNcContentType()));

		inputFiles.addChangeListener(e -> updateInputParams());
		suffix.set(DEFAULT_DETECTION_SUFFIX);
	}

	public IAttributeProvider getAttributeProvider() {
		return attributeProvider;
	}

	public List<SingleFilterParameters> getCriterias() {
		return criterias;
	}

	public void setCriterias(List<SingleFilterParameters> criterias) {
		this.criterias = criterias;
	}

	public DataParameters getDataToSet() {
		return dataToSet;
	}

	public void setDataToSet(DataParameters dataToSet) {
		this.dataToSet = dataToSet;
	}

	/**
	 * Books the selected file
	 * 
	 */
	protected void updateInputParams() {

		IDataContainerInfoService infoService = IDataContainerInfoService.grab();
		IDataContainerFactory dataContainerFactory = IDataContainerFactory.grab();

		BusyIndicator.showWhile(Display.getCurrent(), () -> {
			List<ISounderNcInfo> infos = inputFiles.stream().//
					map(File::getAbsolutePath).//
					map(infoService::getSounderNcInfo).//
					filter(Optional::isPresent).//
					map(Optional::get).//
					collect(Collectors.toList());

			if (!infos.isEmpty()) {
				List<ISounderDataContainerToken> inputSounderDataContainerTokens = new ArrayList<>();
				try {
					IDataContainerOwner owner = IDataContainerOwner.generate("DetectionFilterWizardModel", true);
					inputSounderDataContainerTokens = dataContainerFactory.bookSounderDataContainers(infos, owner);
					List<SounderDataContainer> inputSounderDataContainers = inputSounderDataContainerTokens.stream()//
							.map(ISounderDataContainerToken::getDataContainer)//
							.collect(Collectors.toList());

					attributeProvider = new SounderDataContainerAttributeProvider(inputSounderDataContainers,
							new NullProgressMonitor());
				} catch (GException e) {
					Messages.openErrorMessage("Error : file not available. ", e);
				} finally {
					inputSounderDataContainerTokens.forEach(ISounderDataContainerToken::close);
				}
			}
		});

	}
}
