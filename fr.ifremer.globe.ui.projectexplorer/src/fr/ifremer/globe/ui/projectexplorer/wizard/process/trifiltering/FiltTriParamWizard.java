package fr.ifremer.globe.ui.projectexplorer.wizard.process.trifiltering;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.StandardProjection;
import fr.ifremer.globe.core.processes.filtri.model.FiltTriParameters;

public class FiltTriParamWizard extends Wizard {

	protected FiltTriParameters m_parameters;
	protected String m_outputDirectory;
	protected FiltTriParamMain m_mainPage;
	protected ProjectionPage m_projectionPage;
	protected GeoBox geoBox;
	private Projection projection = new Projection(StandardProjection.MERCATOR);

	private static final Logger logger = LoggerFactory.getLogger(FiltTriParamWizard.class);

	public FiltTriParamWizard(FiltTriParameters parameters, String outputDirectory) {
		super();

		this.m_parameters = parameters;
		this.m_outputDirectory = outputDirectory;
		setNeedsProgressMonitor(true);

	}

	@Override
	public void addPages() {
		this.m_mainPage = new FiltTriParamMain(this.m_parameters, this.m_outputDirectory);
		addPage(this.m_mainPage);

		this.m_projectionPage = new ProjectionPage("Projection type");

		addPage(this.m_projectionPage);

		for (IWizardPage page : getPages()) {
			page.setTitle("FilTri generation parameters");
		}

	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		IWizardPage nextPage = super.getNextPage(page);

		if (nextPage == this.m_projectionPage) {
			// Updating geobox page
			try {

				this.m_projectionPage.setProjection(projection);
				this.m_projectionPage.initializeControls(this.m_parameters.getGeoBox());
			} catch (Exception e) {
				logger.error("error projection parameters", e);
			}
		}
		return nextPage;
	}

	@Override
	public IWizardPage getPreviousPage(IWizardPage page) {
		IWizardPage previousPage = super.getPreviousPage(page);

		return previousPage;
	}

	@Override
	public boolean performFinish() {
		this.m_parameters.setProjection(this.m_projectionPage.getProjectionSettings());
		this.m_mainPage.saveData();
		return true;
	}

}
