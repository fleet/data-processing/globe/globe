package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tottb;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Convert wizard
 */
public class ConvertToTtbWizard extends Wizard {

	/** Model */
	protected ConvertToTtbWizardModel wizardModel;

	/** Pages */
	protected SelectInputFilesPage selectInputFilesPage;
	protected SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public ConvertToTtbWizard(ConvertToTtbWizardModel wizardModel) {
		this.wizardModel = wizardModel;
		setNeedsProgressMonitor(true);
	}

	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(wizardModel);
		addPage(selectInputFilesPage);
		selectOutputParametersPage = new SelectOutputParametersPage(wizardModel);
		addPage(selectOutputParametersPage);
	}

	@Override
	public boolean performFinish() {
		return selectInputFilesPage.isPageComplete() && selectOutputParametersPage.isPageComplete();
	}

	@Override
	public IWizardPage getStartingPage() {
		return wizardModel.getInputFiles().isEmpty() ? selectInputFilesPage : selectOutputParametersPage;
	}
}
