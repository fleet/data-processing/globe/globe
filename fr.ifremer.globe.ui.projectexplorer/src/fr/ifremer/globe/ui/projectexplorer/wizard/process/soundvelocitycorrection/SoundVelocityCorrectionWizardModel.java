package fr.ifremer.globe.ui.projectexplorer.wizard.process.soundvelocitycorrection;

import java.io.File;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.processes.TimeIntervalParameters;
import fr.ifremer.globe.core.processes.soundvelocitycorrection.model.SoundVelocityCorrectionParameters;
import fr.ifremer.globe.core.processes.soundvelocitycorrection.process.SoundVelocityCorrectionType;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;

/**
 * Model of the wizard page
 */
public class SoundVelocityCorrectionWizardModel extends ConvertWizardDefaultModel {

	protected Logger logger = LoggerFactory.getLogger(SoundVelocityCorrectionWizardModel.class);

	/** Sound velocity correction parameters **/
	private final SoundVelocityCorrectionType correctionType;
	private SoundVelocityCorrectionParameters correctionParameters = new SoundVelocityCorrectionParameters();
	private TimeIntervalParameters timeIntervalParameters = new TimeIntervalParameters(
			"fr.ifremer.globe.process.soundvelocity");

	private static final String DEFAULT_VELOCITY_SUFFIX = "vel";

	/**
	 * Constructor
	 */
	public SoundVelocityCorrectionWizardModel(SoundVelocityCorrectionType correctionType) {
		this.correctionType = correctionType;

		IFileService fileService = IFileService.grab();
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.getSounderNcContentType()));

		inputFiles.addChangeListener(event -> {
			// Update
			long minDate = Long.MAX_VALUE;
			long maxDate = -Long.MAX_VALUE;
			for (File file : inputFiles) {
				Optional<IFileInfo> infoStore = fileService.getFileInfo(file.getAbsolutePath());
				if (infoStore.isPresent() && infoStore.get().getContentType().isManageableInSwathEditor()) {
					ISounderNcInfo infoFile = (ISounderNcInfo) infoStore.get();
					minDate = Math.min(minDate, infoFile.getFirstPingDate().getTime());
					maxDate = Math.max(maxDate, infoFile.getLastPingDate().getTime());
				}
			}
			if (minDate != Long.MAX_VALUE)
				timeIntervalParameters.setLongProperty(TimeIntervalParameters.PROPERTY_STARTDATETIME_VALUE, minDate);
			if (maxDate != -Long.MAX_VALUE)
				timeIntervalParameters.setLongProperty(TimeIntervalParameters.PROPERTY_ENDDATETIME_VALUE, maxDate);
		});

		suffix.set(DEFAULT_VELOCITY_SUFFIX);
	}

	// GETTERS

	public SoundVelocityCorrectionParameters getSoundVelocityCorrectionParameters() {
		return correctionParameters;
	}

	public SoundVelocityCorrectionType getCorrectionType() {
		return correctionType;
	}

	public TimeIntervalParameters getTimeIntervalParameters() {
		return timeIntervalParameters;
	}
}