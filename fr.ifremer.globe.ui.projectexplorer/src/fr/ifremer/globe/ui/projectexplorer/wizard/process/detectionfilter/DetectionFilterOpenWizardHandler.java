package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

public class DetectionFilterOpenWizardHandler
		extends AbstractOpenProcessWizardHandler<DetectionFilterProcessWithWizard> {

	@Override
	protected DetectionFilterProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new DetectionFilterProcessWithWizard(shell);
	}
}