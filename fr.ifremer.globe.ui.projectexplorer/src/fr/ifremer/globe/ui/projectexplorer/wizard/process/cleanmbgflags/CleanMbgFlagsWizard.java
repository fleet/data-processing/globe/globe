package fr.ifremer.globe.ui.projectexplorer.wizard.process.cleanmbgflags;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

public class CleanMbgFlagsWizard extends Wizard {

	/** Model */
	private final ConvertWizardDefaultModel model;

	/** Pages */
	private SelectInputFilesPage selectInputFilesPage;
	private SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public CleanMbgFlagsWizard(ConvertWizardDefaultModel model) {
		this.model = model;
		model.getInputFilesFilterExtensions().addAll(IFileService.grab().getFileFilters(ContentType.MBG_NETCDF_3));
		setNeedsProgressMonitor(true);
	}

	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(model);
		addPage(selectInputFilesPage);
		selectOutputParametersPage = new SelectOutputParametersPage(model);
		addPage(selectOutputParametersPage);
	}

	@Override
	public boolean performFinish() {
		return selectInputFilesPage.isPageComplete() && selectOutputParametersPage.isPageComplete();
	}

	@Override
	public IWizardPage getStartingPage() {
		return model.getInputFiles().isEmpty() ? selectInputFilesPage : selectOutputParametersPage;
	}
}
