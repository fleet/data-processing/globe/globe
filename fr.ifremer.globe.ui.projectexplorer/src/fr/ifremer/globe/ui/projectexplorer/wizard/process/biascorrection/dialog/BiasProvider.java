package fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog;

import static fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.CorrectionFileDialog.ATTITUDE_TIME_COLUMN_INDEX;
import static fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.CorrectionFileDialog.DATE_COLUMN_INDEX;
import static fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.CorrectionFileDialog.FILE_COLUMN_INDEX;
import static fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.CorrectionFileDialog.HEADING_COLUMN_INDEX;
import static fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.CorrectionFileDialog.MRU_HEADING_COLUMN_INDEX;
import static fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.CorrectionFileDialog.PITCH_COLUMN_INDEX;
import static fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.CorrectionFileDialog.REMOVE_LINE_INDEX;
import static fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.CorrectionFileDialog.ROLL_COLUMN_INDEX;
import static fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.CorrectionFileDialog.VELOCITY_COLUMN_INDEX;
import static fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.CorrectionFileDialog.VERTICAL_OFFSET_COLUMN_INDEX;
import static fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.CorrectionFileDialog.VERTICAL_OFFSET_TIME_COLUMN_INDEX;

import java.util.Date;

import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionPoint;
import fr.ifremer.globe.utils.date.DateUtils;

public class BiasProvider extends StyledCellLabelProvider implements ITableLabelProvider {

	public BiasProvider() {
		super();
	}

	@Override
	public void initialize(ColumnViewer viewer, ViewerColumn column) {
		super.initialize(viewer, column);
		this.setOwnerDrawEnabled(viewer, column, true);
	}

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return (ResourceManager.getPluginImage("fr.ifremer.globe.editor.swath", "icons/16/Red_X.png"));
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof CorrectionPoint) {
			CorrectionPoint correctionPoint = (CorrectionPoint) element;
			switch (columnIndex) {
			case FILE_COLUMN_INDEX:
				PointLabelProvider labelP = new PointLabelProvider();
				correctionPoint.accept(labelP);
				return labelP.getLabel();
			case DATE_COLUMN_INDEX:
				return DateUtils.formatDate(Date.from(correctionPoint.getDate()));
			case VELOCITY_COLUMN_INDEX:
				return Double.toString(correctionPoint.getVelocity());
			case ROLL_COLUMN_INDEX:
				return Double.toString(correctionPoint.getRoll());
			case PITCH_COLUMN_INDEX:
				return Double.toString(correctionPoint.getPitch());
			case VERTICAL_OFFSET_COLUMN_INDEX:
				return Double.toString(correctionPoint.getPlatformVerticalOffset());
			case HEADING_COLUMN_INDEX:
				return Double.toString(correctionPoint.getHeading());
			case MRU_HEADING_COLUMN_INDEX:
				return Double.toString(correctionPoint.getMruHeading());
			case ATTITUDE_TIME_COLUMN_INDEX:
				return Double.toString(correctionPoint.getAttitudeTimeMs());
			case VERTICAL_OFFSET_TIME_COLUMN_INDEX:
				return Double.toString(correctionPoint.getVerticalOffsetTimeMs());
			default:
				return "";
			}
		}
		return "";
	}

	@Override
	public void update(ViewerCell cell) {
		super.update(cell);
		CorrectionPoint correctionPoint = (CorrectionPoint) cell.getElement();
		int columnIndex = cell.getColumnIndex();
		if (columnIndex == REMOVE_LINE_INDEX) {
			cell.setImage(this.getColumnImage(correctionPoint, columnIndex));
		} else {
			if (columnIndex == FILE_COLUMN_INDEX) {
				cell.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GREEN));

			}
			cell.setText(this.getColumnText(correctionPoint, columnIndex));
		}
	}
}
