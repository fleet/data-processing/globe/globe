package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.composite;

import fr.ifremer.globe.utils.exception.GIOException;

public interface IFilterLayer {

	void add() throws GIOException;

	void remove(SingleFilterComposite layer);
}
