package fr.ifremer.globe.ui.projectexplorer.wizard.process.applynavigation;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.ui.widget.FileComposite;

/**
 * Page to select correction file.
 */
public class ApplyNavSelectNviPage extends WizardPage {

	/** Properties **/
	private ApplyNavWizardModel wizardModel;

	/**
	 * Constructor
	 */
	public ApplyNavSelectNviPage(ApplyNavWizardModel applyNavWizardModel) {
		super("");
		this.wizardModel = applyNavWizardModel;
		setTitle("Select navigation file");
		setDescription("Select the navigation file (.nvi or .nvi.nc) to apply.");
		wizardModel.getNavigationFile()
				.addChangeListener(e -> setPageComplete(wizardModel.getNavigationFile().exists()));
	}

	/**
	 * Creates the graphical interface.
	 */
	@Override
	public void createControl(Composite parent) {
		Composite mainComposite = new Composite(parent, SWT.NONE);
		GridLayout glMainComposite = new GridLayout(2, false);
		glMainComposite.verticalSpacing = 16;
		mainComposite.setLayout(glMainComposite);

		// Nvi file selection
		var lblNavigationFile = new Label(mainComposite, SWT.NONE);
		lblNavigationFile.setText("Navigation file (.nvi or .nvi.nc) :");
		new FileComposite(mainComposite, SWT.NONE, new SelectNviFileCompositeModel(wizardModel.getNavigationFile()));

		setControl(mainComposite);

		var btnApplyImmersion = new Button(mainComposite, SWT.CHECK);
		btnApplyImmersion.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		btnApplyImmersion.setText("Apply immersion");
		btnApplyImmersion.setSelection(wizardModel.getApplyImmersion().doGetValue());
		btnApplyImmersion.addListener(SWT.Selection,
				e -> wizardModel.getApplyImmersion().doSetValue(btnApplyImmersion.getSelection()));
		setPageComplete(false);
	}

}
