package fr.ifremer.globe.ui.projectexplorer.wizard.convert.rawfiles;

import java.util.Set;

import org.eclipse.e4.ui.model.application.ui.menu.MHandledItem;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

/**
 * Handler class to open {@link ConvertRawFilesProcessWithWizard}.
 */
public class ConvertRawFilesOpenWizardHandler
		extends AbstractOpenProcessWizardHandler<ConvertRawFilesProcessWithWizard> {

	@Override
	protected Set<ContentType> getAcceptedContentType() {
		return ContentType.getSounderRawFileContentTypes();
	}

	@Override
	protected ConvertRawFilesProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		ConvertRawFilesProcessWithWizard process = new ConvertRawFilesProcessWithWizard(shell);

		// Output format can be an option of caller item
		if (callerItem instanceof MHandledItem handledItem) {
			String format = handledItem.getParameters().get(0).getValue();
			process.getModel().getXsfFormat().set("XSF".equals(format));
			process.getModel().getMbgFormat().set("MBG".equals(format));
			process.getModel().getNviFormat().set("NVI".equals(format));
		}

		return process;
	}

}