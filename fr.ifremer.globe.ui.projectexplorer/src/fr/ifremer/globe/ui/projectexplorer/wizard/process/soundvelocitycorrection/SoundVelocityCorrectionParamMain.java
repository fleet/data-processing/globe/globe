package fr.ifremer.globe.ui.projectexplorer.wizard.process.soundvelocitycorrection;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;

import fr.ifremer.globe.core.processes.soundvelocitycorrection.model.SoundVelocityCorrectionParameters;

public class SoundVelocityCorrectionParamMain extends WizardPage implements Listener {

	final IStatus statusOK = new Status(IStatus.OK, "not_used", 0, null, null);

	private SoundVelocityCorrectionParameters m_parameters;

	private Group m_fileListGroup;
	private List fileList;
	private Composite buttonListComposite;
	private Button addFileButton;
	private Button clearListButton;


	private Group m_algoGroup;
	private Button m_timeInterpolationOptionOptionButton;
	private Button m_nearestNeightborOptionButton;
	private Button m_restoreDefault;

	private IStatus m_fileListStatus = statusOK;
	private Composite container;

	public static final String VEL_FILTER_EXTENSION = "*.vel";

	public SoundVelocityCorrectionParamMain(SoundVelocityCorrectionParameters parameters) {
		super("");
		setDescription("Enter parameter values for sound velocity correction processing");
		m_parameters = parameters;
	}

	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		createMain(container);
		getShell().pack();
		setControl(container);
		setPageComplete(false);
		loadData();

		if (fileList.getItemCount()==0) {
			m_fileListStatus = new Status(IStatus.ERROR, "not_used", 0, "List can't be empty", null);
		}
		applyMostSevere();
	}

	protected void createMain(Composite parent) {
		GridLayout gl_container = new GridLayout();
		gl_container.verticalSpacing = 10;
		parent.setLayout(gl_container);
		gl_container.numColumns = 1;

		m_fileListGroup = new Group(parent, SWT.NONE);
		m_fileListGroup.setLayout(new GridLayout(1, false));
		GridData gd_m_fileListGroup = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_m_fileListGroup.heightHint = 176;
		m_fileListGroup.setLayoutData(gd_m_fileListGroup);
		m_fileListGroup.setText("Sound Velocity File List");

		fileList = new List(m_fileListGroup, SWT.BORDER | SWT.V_SCROLL);
		GridData gd_list = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_list.widthHint = 455;
		gd_list.heightHint = 85;
		fileList.setLayoutData(gd_list);

		buttonListComposite = new Composite(m_fileListGroup, SWT.NONE);
		buttonListComposite.setLayout(new GridLayout(2, false));
		buttonListComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		addFileButton = new Button(buttonListComposite, SWT.NONE);
		addFileButton.setText("Add Sound Velocity files...");
		addFileButton.addListener(SWT.Selection, this);

		clearListButton = new Button(buttonListComposite, SWT.NONE);
		clearListButton.setText("Clear file list");
		clearListButton.addListener(SWT.Selection, this);

		m_algoGroup = new Group(parent, SWT.NONE);
		m_algoGroup.setLayout(new GridLayout(1, false));
		m_algoGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		m_algoGroup.setText("Sound Velocity Profiles Algorithm");

		m_timeInterpolationOptionOptionButton = new Button(m_algoGroup, SWT.RADIO);
		m_timeInterpolationOptionOptionButton.setText("Time interpolation");
		m_nearestNeightborOptionButton = new Button(m_algoGroup, SWT.RADIO);
		m_nearestNeightborOptionButton.setText("Nearest space neighbor");

		m_restoreDefault = new Button(parent, SWT.PUSH);
		m_restoreDefault.setText("Restore Default");
		GridData gd2 = new GridData(SWT.RIGHT, SWT.BOTTOM, true, true);
		gd2.horizontalSpan = 3;
		gd2.verticalIndent = 20;
		m_restoreDefault.setLayoutData(gd2);
		m_restoreDefault.addListener(SWT.Selection, this);

	}

	@Override
	public void handleEvent(Event event) {
		if (event.widget == m_restoreDefault) {
			m_parameters.restoreDefault();
			loadData();
		} else if (event.widget == addFileButton) {

			FileDialog dialog = new FileDialog(getShell(), SWT.SELECTED | SWT.SHEET | SWT.MULTI);
			dialog.setFilterExtensions(new String[] { VEL_FILTER_EXTENSION });

			String firstSelectedFile = dialog.open();
			if (firstSelectedFile != null) {

				String[] files = dialog.getFileNames();
				String[] filesPathArray = new String[files.length];
				for (int i = 0; i<files.length; i++) {
					filesPathArray[i] = dialog.getFilterPath() +File.separator+ files[i];

				}

				String[] previousItemsArray = fileList.getItems();

				// ajouter au tableau deja existant
				java.util.List<String> totalitemsList = new ArrayList<String>();
				for (String s : previousItemsArray) {
					totalitemsList.add(s);   
				}
				for (String s : filesPathArray) {
					totalitemsList.add(s);   
				}       

				String[] newItemsArray = new String[previousItemsArray.length+filesPathArray.length];
				totalitemsList.toArray(newItemsArray);

				fileList.setItems(newItemsArray);

				m_fileListStatus = statusOK;
			}

		} else if (event.widget == clearListButton) {
			fileList.removeAll();
			m_fileListStatus = new Status(IStatus.ERROR, "not_used", 0, "List can't be empty", null);
		}
		applyMostSevere();
	}

	private void applyMostSevere() {
		IStatus status = new Status(IStatus.OK, "not_used", 0, null, null);
		if (m_fileListStatus.matches(IStatus.ERROR)) {
			status = m_fileListStatus;
		}

		if (status.getSeverity() == IStatus.ERROR) {
			setErrorMessage(status.getMessage());
			setPageComplete(false);
		} else {
			setErrorMessage(null);
			setPageComplete(true);
		}
	}

	public void loadData() {
		// File list
		fileList.setItems((String[]) m_parameters.getArrayProperty(SoundVelocityCorrectionParameters.PROPERTY_VEL_FILE_LIST));
		//Algo Option
		m_timeInterpolationOptionOptionButton.setSelection(m_parameters.getBoolProperty(SoundVelocityCorrectionParameters.PROPERTY_TIME_INTERPOLATION_OPTION));
		m_nearestNeightborOptionButton.setSelection(m_parameters.getBoolProperty(SoundVelocityCorrectionParameters.PROPERTY_NEAREST_NEIGHTBOR_OPTION));

	}

	public void saveData() {
		// File list
		m_parameters.setArrayProperty(SoundVelocityCorrectionParameters.PROPERTY_VEL_FILE_LIST, fileList.getItems());
		//Algo Option
		m_parameters.setBoolProperty(SoundVelocityCorrectionParameters.PROPERTY_TIME_INTERPOLATION_OPTION, m_timeInterpolationOptionOptionButton.getSelection());
		m_parameters.setBoolProperty(SoundVelocityCorrectionParameters.PROPERTY_NEAREST_NEIGHTBOR_OPTION, m_nearestNeightborOptionButton.getSelection());

	}

}
