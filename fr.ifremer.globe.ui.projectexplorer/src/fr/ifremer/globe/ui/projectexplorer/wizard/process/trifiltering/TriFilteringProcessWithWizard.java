package fr.ifremer.globe.ui.projectexplorer.wizard.process.trifiltering;

import java.util.List;
import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.processes.filtri.FilTriProcess;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.ui.wizard.FilesToProcessEntry;
import fr.ifremer.globe.utils.exception.GException;

/**
 * Process and wizard for triangular filtering
 */
public class TriFilteringProcessWithWizard extends DefaultProcessWithWizard<TriFilteringWizardModel> {

	public TriFilteringProcessWithWizard(Shell shell) {
		super(shell, "Filtering by triangulation", TriFilteringWizardModel::new, TriFilteringWizard::new);
	}

	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		model.getFilTriParameters().setProjection(model.getProjection().getProjectionSettings());
		// Get file list (input/output/temp file tuples)
		List<FilesToProcessEntry> filesToProcess = getFilesToProcessWithTemp(logger);

		// Apply triangular filtering
		String s = filesToProcess.size() > 1 ? "s" : "";
		logger.info("Start processing of {} file{}...", filesToProcess.size(), s);

		// Init progression.
		monitor.setTaskName("Filtering by triangulation");
		monitor.beginTask("Filtering by triangulation running ...", filesToProcess.size());

		int processedFileCount = 0;
		int errorFileCount = 0;
		for (FilesToProcessEntry entry : filesToProcess) {
			logger.info("Processing : {}", entry.getInputFile().getAbsolutePath());
			try {
				// Call triangular filtering process
				FilTriProcess filtri = new FilTriProcess();
				filtri.process(entry.getTempFile(), model.getFilTriParameters(), Optional.of(logger), monitor);
				writeOutputFileFromTemp(entry);
				processedFileCount++;
			} catch (Exception e) {
				logger.error("Error while processing file {}: {}", entry.getInputFile().getPath(), e.getMessage(), e);
				errorFileCount++;
			}
			if (monitor.isCanceled())
				return Status.CANCEL_STATUS;
			monitor.worked(1);
		}
		loadOutputFiles(filesToProcess, monitor);

		logger.info("End of process: Filtering by triangulation applied on {} file{}.", processedFileCount, s);
		if (errorFileCount > 0) {
			String sError = errorFileCount > 1 ? "s" : "";
			logger.error("Error while processing {} file{}", errorFileCount, sError);
		}

		return monitor.isCanceled() ? Status.CANCEL_STATUS : Status.OK_STATUS;
	}

}
