package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;

import fr.ifremer.globe.core.processes.detectionfilter.IAttributeProvider;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.wizard.ValidityFilteringWizardPage;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

public class DetectionFilterWizard extends Wizard {
	private DetectionFilterWizardModel model;

	/* Pages */
	private SelectInputFilesPage selectInputFilesPage;
	private ValidityFilteringWizardPage validityPage;
	private SelectOutputParametersPage selectOutputParametersPage;

	public DetectionFilterWizard(DetectionFilterWizardModel model) {
		super();
		this.model = model;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(model);
		addPage(selectInputFilesPage);
		validityPage = new ValidityFilteringWizardPage();
		addPage(validityPage);
		selectOutputParametersPage = new SelectOutputParametersPage(model);
		addPage(selectOutputParametersPage);

		// Necessary if startingPage is "selectInputFilesPage": unknown input files now
		model.getInputFiles().addChangeListener(e -> updateValidityPage());
		// Necessary if startingPage is "validityPage": known input files
		updateValidityPage();
	}

	@Override
	public boolean performFinish() {
		model.setCriterias(validityPage.getCriterias());
		model.setDataToSet(validityPage.getDataToSet());
		return selectInputFilesPage.isPageComplete() && selectOutputParametersPage.isPageComplete()
				&& validityPage.isPageComplete();
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		return model.getInputFiles().isEmpty() ? selectInputFilesPage : validityPage;
	}

	protected void updateValidityPage() {
		Display.getDefault().asyncExec(() -> {
			IAttributeProvider provider = model.getAttributeProvider();
			if (provider != null) {
				validityPage.init(provider);
			}
		});
	}

}
