package fr.ifremer.globe.ui.projectexplorer.wizard.process.applynavigation;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.model.navigation.utils.NavigationApplier;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.ui.wizard.FilesToProcessEntry;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Apply navigation process with wizard
 */
public class ApplyNavProcessWithWizard extends DefaultProcessWithWizard<ApplyNavWizardModel> {

	private static final String TITLE = "Apply navigation to sounder files";

	/**
	 * Constructor
	 */
	public ApplyNavProcessWithWizard(Shell shell) {
		super(shell, TITLE, ApplyNavWizardModel::new, ApplyNavWizard::new);
	}

	/**
	 * Process method executed after a click on finish.
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		logger.info("Start apply navigation process...");
		NavigationApplier navigationApplier = new NavigationApplier();

		// Get navigation file
		if (!model.getNavigationFile().exists()) {
			throw new GIOException("Navigation missing.");
		}
		String nviFilePath = model.getNavigationFile().get().getAbsolutePath();
		INavigationDataSupplier navigationDataSupplier = INavigationService.grab()
				.getNavigationDataProvider(nviFilePath).orElseThrow(() -> new GIOException("Invalid navigation file."));
		logger.info("Navigation file: {}", FilenameUtils.getBaseName(nviFilePath));

		// Get file list (input/output/temp file tuples)
		List<FilesToProcessEntry> filesToProcess = getFilesToProcessWithTemp(logger);
		monitor.beginTask("Apply navigation running...", filesToProcess.size());

		IDataContainerInfoService infoService = IDataContainerInfoService.grab();
		// Apply navigation on each sounder file
		int processedFileCount = 0;
		int errorFileCount = 0;
		for (FilesToProcessEntry entry : filesToProcess) {
			Optional<ISounderNcInfo> info = infoService.getSounderNcInfo(entry.getTempFile().getAbsolutePath());
			if (info.isPresent()) {
				monitor.setTaskName("Processing: " + entry.getInputFile().getPath());
				logger.info("Processing:  {}", entry.getInputFile().getPath());
				ISounderNcInfo sounderInfo = info.get();
				try {
					if (checkSounderFiles(navigationDataSupplier, sounderInfo)) {
						processedFileCount++;
						logger.info("Apply navigation to file {}...", sounderInfo.getPath());
						boolean applyImmersion = model.getApplyImmersion().doGetValue();
						logger.info("Apply immersion : {}", applyImmersion);
						navigationApplier.apply(sounderInfo, navigationDataSupplier, applyImmersion);
					}
					// write output file
					writeOutputFileFromTemp(entry);
				} catch (Exception e) {
					logger.error("Error while processing file {}: {}", entry.getInputFile().getPath(), e.getMessage(),
							e);
					errorFileCount++;
				}
			}
			if (monitor.isCanceled())
				return Status.CANCEL_STATUS;
			monitor.worked(1);
		}

		// load output files if asked
		loadOutputFiles(filesToProcess, monitor);

		String s = processedFileCount > 1 ? "s" : "";
		logger.info("End of process: {} applied on {} file{}.", FilenameUtils.getBaseName(nviFilePath),
				processedFileCount, s);
		if (errorFileCount > 0) {
			String sError = errorFileCount > 1 ? "s" : "";
			logger.error("Error while processing {} file{}", errorFileCount, sError);
		}
		return monitor.isCanceled() ? Status.CANCEL_STATUS : Status.OK_STATUS;
	}

	/**
	 * @return sounder driver info compliant with the specified navigation file (whose the time period contains a part
	 *         of the navigation file)
	 */
	private boolean checkSounderFiles(INavigationDataSupplier navigationDataSupplier, ISounderNcInfo sounderDriverInfo)
			throws GIOException {
		boolean result = false;
		try (INavigationData navigationData = navigationDataSupplier.getNavigationData(true)) {
			long navStart = navigationData.getTime(0);
			long navEnd = navigationData.getTime(navigationData.size() - 1);
			long sounderStart = sounderDriverInfo.getFirstPingDate().getTime();
			long sounderEnd = sounderDriverInfo.getLastPingDate().getTime();

			result = ((navEnd >= sounderStart) && (navStart <= sounderEnd));
			
			if (!result) {
				logger.warn(
						"Sounder file have not common date with the navigation to apply. The file will be ignored."
								+ "\nSounder file {} : {} to {}" + "\nNavigation file {} : {} to {}",
						sounderDriverInfo.getFilename(), Instant.ofEpochMilli(sounderStart),
						Instant.ofEpochMilli(sounderEnd), //
						navigationData.getFileName(), Instant.ofEpochMilli(navStart), Instant.ofEpochMilli(navEnd));
			}

		} catch (IOException e) {
			// Error on close. Ignore
		}
		return result;
	}
}
