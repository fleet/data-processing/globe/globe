package fr.ifremer.globe.ui.projectexplorer.wizard.process.applynavigation;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

/**
 * Handler class to open the process wizard (referenced in fragment.e4xmi).
 */
public class ApplyNavOpenWizardHandler extends AbstractOpenProcessWizardHandler<ApplyNavProcessWithWizard> {

	@Override
	protected ApplyNavProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new ApplyNavProcessWithWizard(shell);
	}

}