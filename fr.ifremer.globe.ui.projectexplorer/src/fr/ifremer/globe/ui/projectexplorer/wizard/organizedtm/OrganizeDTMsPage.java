package fr.ifremer.globe.ui.projectexplorer.wizard.organizedtm;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.core.utils.preference.attributes.StringPreferenceAttribute;

/**
 * Wizard page to select parameter(s) to use to organize DTM files
 */
public class OrganizeDTMsPage extends WizardPage {

	private static final String AGE_RANGES_PATTERN = "(\\[\\d{1,3}-\\d{1,3}\\])+";
	private static final String DEFAULT_AGE_RANGES = "[0-5][6-10][11-30][31-99]";

	/** Organization parameter **/
	public enum Parameter {
		AGE("Age ranges"),
		HORIZONTAL_ACCURACY("Horizontal accuracy"),
		VERTICAL_ACCURACY("Vertical accuracy"),
		SURVEY_TYPE("Survey type"),
		PROVIDER_ID("Provider ID "),
		NA("None");

		private final String text;

		private Parameter(String s) {
			this.text = s;
		}

		@Override
		public String toString() {
			return text;
		}
	}

	/** Organization mode **/
	public enum OrganizationMode {
		HIERARCHICAL,
		FLAT
	}

	/**
	 * Parameter class: contains a parameter and, if necessary: age ranges.
	 */
	public class OrganizeDTMParameter {

		private Parameter paramType;
		private List<Pair<Integer, Integer>> ageRanges;

		public OrganizeDTMParameter(Parameter selectedParam, List<Pair<Integer, Integer>> ar) {
			this.paramType = selectedParam;
			this.setAgeRanges(ar);
		}

		public OrganizeDTMParameter(Parameter selectedParam) {
			this.paramType = selectedParam;
		}

		public Parameter getParamType() {
			return paramType;
		}

		public List<Pair<Integer, Integer>> getAgeRanges() {
			return ageRanges;
		}

		public void setAgeRanges(List<Pair<Integer, Integer>> ageRanges) {
			this.ageRanges = ageRanges;
		}

	}

	/** Contains components used to select a parameter **/
	private class ParameterSelector {
		ComboViewer comboViewer;
		Text ageRangeInput;

		public ParameterSelector(ComboViewer cv, Text t) {
			this.comboViewer = cv;
			this.ageRangeInput = t;
		}
	}

	/**
	 * Class used to save/load settings in user preference
	 */
	public static class Preference extends PreferenceComposite {
		private StringPreferenceAttribute ageRanges;

		public Preference() {
			super(PreferenceRegistry.getInstance().getViewsSettingsNode(), "Layer explorer");
			ageRanges = new StringPreferenceAttribute("ageRanges", "Organize DTMs: default age ranges",
					DEFAULT_AGE_RANGES);
			this.declareAttribute(ageRanges);
			load();
		}

		public StringPreferenceAttribute getAgeRanges() {
			return ageRanges;
		}

		public void setAgeRange(String s) {
			ageRanges.setValue(s, false);
			save();
		}
	}

	private static Preference pref;

	public static void initPreference() {
		pref = new Preference();
	}

	/** Properties **/
	private List<ParameterSelector> comboViewerList = new ArrayList<>();
	private Button btnFlatMode;
	private Button btnDeleteEmptyNodes;

	/** Constructor **/
	protected OrganizeDTMsPage() {
		super("Organize DTM files");
		setTitle("Organize DTM files");
		setDescription("Select parameters that will be used to organize files by group");
	}

	/**
	 * Build the UI
	 */
	@Override
	public void createControl(Composite parent) {

		Composite container = new Composite(parent, SWT.NULL);
		GridLayout glContainer = new GridLayout(4, false);
		glContainer.verticalSpacing = 10;
		container.setLayout(glContainer);
		setControl(container);

		// Parameters

		Label lblParameters = new Label(container, SWT.NONE);
		lblParameters.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		lblParameters.setText("Parameters :");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		for (int i = 0; i < 5; i++)
			createParamSelector(container, i);

		Composite composite = new Composite(container, SWT.NONE);
		RowLayout rlComposite = new RowLayout(SWT.HORIZONTAL);
		rlComposite.spacing = 10;
		composite.setLayout(rlComposite);
		composite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));

		// Mode selection

		Label lblOrganization = new Label(composite, SWT.NONE);
		lblOrganization.setText("Organization mode :");

		Button btnHierarchicalMode = new Button(composite, SWT.RADIO);
		btnHierarchicalMode.setText("Hierarchical");
		btnHierarchicalMode.setSelection(true);

		btnFlatMode = new Button(composite, SWT.RADIO);
		btnFlatMode.setText("Flat");

		btnDeleteEmptyNodes = new Button(container, SWT.CHECK);
		btnDeleteEmptyNodes.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 4, 1));
		btnDeleteEmptyNodes.setSelection(true);
		btnDeleteEmptyNodes.setText("Delete empty groups");
	}

	/**
	 * Build a parameter selector (= 1 combo viewer + 1 input text for age ranges)
	 */
	private void createParamSelector(Composite container, int index) {

		Label label = new Label(container, SWT.NONE);
		label.setText(index + 1 + " - ");
		label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

		ComboViewer comboViewer = new ComboViewer(container, SWT.READ_ONLY);
		comboViewer.setContentProvider(ArrayContentProvider.getInstance());
		comboViewer.setInput(Parameter.values());

		comboViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof Parameter parameter)
					return parameter.toString();

				return super.getText(element);
			}
		});
		comboViewer.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Text textAge = new Text(container, SWT.BORDER);
		textAge.setVisible(false);
		textAge.setText(pref.getAgeRanges().getValue());
		textAge.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		// If input value does not match with pattern, change font color to red
		textAge.addModifyListener(e -> textAge.setForeground(
				Pattern.matches(AGE_RANGES_PATTERN, textAge.getText()) ? SWTResourceManager.getColor(SWT.DEFAULT)
						: SWTResourceManager.getColor(SWT.COLOR_RED)));

		Label labelAge = new Label(container, SWT.NONE);
		labelAge.setVisible(false);
		labelAge.setText("Pattern to use : [0-5][6-20]...");
		labelAge.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));

		comboViewer.addSelectionChangedListener(event -> {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();
			Parameter selectedParam = (Parameter) selection.getFirstElement();
			textAge.setVisible(selectedParam == Parameter.AGE);
			labelAge.setVisible(selectedParam == Parameter.AGE);
		});

		// set combo viewer default value
		switch (index) {
		case 0:
			comboViewer.setSelection(new StructuredSelection(Parameter.AGE));
			textAge.setVisible(true);
			labelAge.setVisible(true);
			break;
		case 1:
			comboViewer.setSelection(new StructuredSelection(Parameter.VERTICAL_ACCURACY));
			break;
		case 2:
			comboViewer.setSelection(new StructuredSelection(Parameter.HORIZONTAL_ACCURACY));
			break;
		case 3:
			comboViewer.setSelection(new StructuredSelection(Parameter.PROVIDER_ID));
			break;
		case 4:
			comboViewer.setSelection(new StructuredSelection(Parameter.SURVEY_TYPE));
			break;
		default:
			// No default value
			break;
		}

		comboViewerList.add(new ParameterSelector(comboViewer, textAge));
	}

	/**
	 * Get the defined age ranges, if nothing is defined or the input does not match with the pattern: returns null
	 **/
	public List<Pair<Integer, Integer>> getAgeRanges(Text t) {

		String inputValue = t.getText();

		if (!Pattern.matches(AGE_RANGES_PATTERN, inputValue))
			return List.of();

		// Save value in user preferences
		pref.setAgeRange(inputValue);

		List<Pair<Integer, Integer>> result = new ArrayList<>();

		inputValue = inputValue.substring(1, inputValue.length() - 1);

		for (String stringRange : inputValue.split("\\]\\[")) {
			String[] array = stringRange.split("-");
			Integer start = Integer.parseInt(array[0]);
			Integer end = Integer.parseInt(array[1]);
			result.add(new Pair<>(start, end));
		}

		return result;
	}

	/**
	 * Returns the selected parameters to organize DTM files
	 */
	public List<OrganizeDTMParameter> getSelectedParameters() {
		List<OrganizeDTMParameter> result = new ArrayList<>();

		comboViewerList.forEach(selector -> {
			IStructuredSelection selection = (IStructuredSelection) selector.comboViewer.getSelection();

			if (selection.getFirstElement() instanceof Parameter selectedParam) {
				switch (selectedParam) {
				case AGE:
					result.add(new OrganizeDTMParameter(selectedParam, getAgeRanges(selector.ageRangeInput)));
					break;
				case NA:
					break;
				default:
					result.add(new OrganizeDTMParameter(selectedParam));
				}
			}

		});

		return result;
	}

	/**
	 * Returns the selected mode (flat or hierarchical)
	 */
	public OrganizationMode getSelectedMode() {
		return btnFlatMode.getSelection() ? OrganizationMode.FLAT : OrganizationMode.HIERARCHICAL;
	}

	/**
	 * Returns true if empty nodes have to be deleted
	 */
	public boolean deleteEmptyGroups() {
		return btnDeleteEmptyNodes.getSelection();
	}
}
