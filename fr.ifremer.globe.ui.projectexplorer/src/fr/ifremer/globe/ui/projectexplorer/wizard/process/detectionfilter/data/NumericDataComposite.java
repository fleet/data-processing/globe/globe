/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.data;

import org.eclipse.nebula.widgets.formattedtext.FormattedText;
import org.eclipse.nebula.widgets.formattedtext.NumberFormatter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.processes.detectionfilter.parameters.DataParameters;

/**
 * Composite used to edit a numeric value to set.
 */
public class NumericDataComposite extends ADataComposite {
	private FormattedText ValueA;

	/**
	 * Constructor.
	 */
	public NumericDataComposite(Composite parent, int style, String editPattern) {
		super(parent, style);

		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginHeight = gridLayout.marginWidth = 0;
		setLayout(gridLayout);
		ValueA = new FormattedText(this, SWT.BORDER);
		ValueA.setFormatter(new NumberFormatter(editPattern));
		GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
		gridData.widthHint = editPattern.length() * 6;
		ValueA.getControl().setLayoutData(gridData);

	}

	/**
	 * Follow the link.
	 *
	 * @see fr.ifremer.globe.editor.script.ui.data.ADataComposite#updateParameters(fr.ifremer.globe.imagery.script.parameter.DataParameters)
	 */
	@Override
	public void updateParameters(DataParameters parameters) {
		parameters.setValue(ValueA.getFormatter().getValue().toString());
	}
}
