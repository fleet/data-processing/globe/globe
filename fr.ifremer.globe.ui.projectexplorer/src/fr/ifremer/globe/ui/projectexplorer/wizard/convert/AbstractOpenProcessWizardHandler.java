package fr.ifremer.globe.ui.projectexplorer.wizard.convert;

import java.nio.file.Files;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledItem;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.runtime.datacontainer.IDataContainerInfo;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;

/**
 * Handler class to open a {@link DefaultProcessWithWizard}
 */
public abstract class AbstractOpenProcessWizardHandler<T extends DefaultProcessWithWizard<?>>
		extends AbstractNodeHandler {

	private MItem callerItem;

	@Inject
	protected IDataContainerFactory sounderDataContainerFactory;

	/**
	 * Abstract method to implement : builds the {@link DefaultProcessWithWizard}.
	 */
	protected abstract T buildProcessWithWizard(Shell shell, MItem callerItem);

	/**
	 * Defines accepted input content type.
	 */
	protected Set<ContentType> getAcceptedContentType() {
		// by default MBG & XSF are accepted
		return ContentType.getSounderNcContentType();
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		if (!openedFromProjectExplorer()) {
			return true;
		}
		Object[] selection = getSelection(selectionService);
		List<FileInfoNode> nodeList = getSuitableFileInfoNode(selection);
		return nodeList.size() == selection.length;
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService, @Active MItem item) {
		this.callerItem = item;
		return checkExecution(selectionService, modelService);
	}

	/**
	 * Handler execution from an {@link MHandledItem}
	 */
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, ESelectionService selectionService,
			@Active MHandledItem item, IEclipseContext eclipseContext) {
		T processWithWizard = buildProcessWithWizard(shell, item);
		ContextInjectionFactory.inject(processWithWizard, eclipseContext);
		execute(processWithWizard, selectionService);
	}

	/**
	 * Handler execution from an {@link MItem}
	 */
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, ESelectionService selectionService,
			@Active MItem item, IEclipseContext eclipseContext) {
		T processWithWizard = buildProcessWithWizard(shell, item);
		ContextInjectionFactory.inject(processWithWizard, eclipseContext);
		execute(processWithWizard, selectionService);
	}

	/**
	 * Handler execution : opens the process wizard.
	 */
	protected void execute(T processWithWizard, ESelectionService selectionService) {

		// Get selected files if launch with a right click on Project Explorer
		if (openedFromProjectExplorer()) {
			Object[] selection = getSelection(selectionService);
			List<FileInfoNode> nodeList = getSuitableFileInfoNode(selection);

			// Check if files
			List<IFileInfo> invalidFiles = nodeList.stream().map(FileInfoNode::getFileInfo)
					.filter(info -> !Files.isRegularFile(info.toPath()))//
					.collect(Collectors.toList());
			if (!invalidFiles.isEmpty()) {
				StringBuilder errorMsg = invalidFiles.size() > 1
						? new StringBuilder("Input files not valid (check files exist...) :\n")
						: new StringBuilder("Input file not valid (check file exists...) :\n");
				invalidFiles.forEach(file -> errorMsg.append("\n" + file.getFilename()));
				Messages.openWarningMessage("Error", errorMsg.toString());
				return;
			}

			// Check if files are not booked (only check if input files are bookable)
			List<IDataContainerInfo> dataContainerInfos = nodeList.stream().map(FileInfoNode::getFileInfo)//
					.filter(IDataContainerInfo.class::isInstance)//
					.map(IDataContainerInfo.class::cast)//
					.collect(Collectors.toList());
			if (!dataContainerInfos.isEmpty() && isBooked(dataContainerInfos)) {
				Messages.openWarningMessage("Error", "Some of the selected files are already in use.");
				return;
			}

			nodeList.stream().forEach(node -> processWithWizard.getModel().getInputFiles().add(node.getFile()));
		}

		openWizard(processWithWizard);
	}

	/**
	 * Opens the wizard and launches the process after close.
	 */
	protected void openWizard(T processWithWizard) {
		if (processWithWizard.getWizardDialog().open() == Window.OK)
			processWithWizard.runInBackground(true);
	}

	/**
	 * @return true if the handler is called from the project explorer
	 */
	protected boolean openedFromProjectExplorer() {
		return callerItem.getParent().getElementId().startsWith("fr.ifremer.globe.ui.projectexplorer.popupmenu.");
	}

	/**
	 * Seach suitable FileInfoNode for the operation. By default, a suitable FileInfoNode is a instance with the
	 * expected driver
	 *
	 * @param selection selected nodes in project explorer
	 * @return suitable FileInfoNodes
	 */
	protected List<FileInfoNode> getSuitableFileInfoNode(Object[] selection) {
		return getSelectionAsList(selection, contentType -> getAcceptedContentType().contains(contentType));
	}

	/**
	 * Check if files are booked. <br>
	 * By default, a wizard allows referencing files only if they are not booked.<br>
	 * Redefine this method to change this behavior.
	 */
	protected boolean isBooked(List<IDataContainerInfo> dataContainerInfos) {
		return sounderDataContainerFactory.isBooked(dataContainerInfos);
	}

}