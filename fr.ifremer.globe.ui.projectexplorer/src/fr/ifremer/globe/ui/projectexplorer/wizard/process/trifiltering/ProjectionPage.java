package fr.ifremer.globe.ui.projectexplorer.wizard.process.trifiltering;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.ui.projection.ProjectionDialog;

public class ProjectionPage extends WizardPage implements PropertyChangeListener {

	protected Composite container;
	private Composite projectionComposite;
	protected Projection projection;
	protected ProjectionDialog projectionDialog;

	public ProjectionPage(String description) {
		super("");
		setDescription(description);
	}

	@Override
	public void createControl(Composite parent) {

		container = new Composite(parent, SWT.NULL);
		GridLayout gl_container = new GridLayout(2, false);
		gl_container.marginHeight = 10;
		gl_container.marginWidth = 10;
		gl_container.verticalSpacing = 10;
		gl_container.horizontalSpacing = 10;
		container.setLayout(gl_container);

		projectionComposite = new Composite(container, SWT.NONE);
		projectionComposite.setLayout(new FillLayout(SWT.HORIZONTAL));
		projectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		projectionDialog = new ProjectionDialog(projectionComposite);
		
		updateLabels(true);

		setControl(container);		
	}

	
	public void initializeControls(GeoBox box) {
		projectionDialog.setGeobox(box);

		projectionDialog.setProjectionSettings(projection.getProjectionSettings());

		ModifyListener listener = new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				updateLabels(false);
			}
		};
		// initialize field
		listener.modifyText(null);
		
		projection.addPropertyChangeListener(this);
		projectionDialog.addPropertyChangeListener(this);
	}

	public void setProjection(Projection projection) {
		this.projection = projection;
		updateLabels(true);
	}

	public ProjectionSettings getProjectionSettings() {
		return projectionDialog.getProjectionSettings();
	}
	
	public ProjectionDialog getProjectionDialog() {
		return projectionDialog;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String propertyName = evt.getPropertyName();
		if (propertyName.equals(Projection.PROJECTION_CHANGED)) {
			updateLabels(true);
		}
	}

	public void updateLabels(boolean projectionChanged) {
		ProjectionSettings projectionSettings = projectionDialog.getProjectionSettings();
		if (projectionSettings != null) {
			projection.setProjectionSettings(projectionSettings);
			container.layout();
		}
	}
}


