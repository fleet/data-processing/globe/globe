package fr.ifremer.globe.ui.projectexplorer.wizard.process.depthcorrection;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ui.projectexplorer.wizard.convert.AbstractOpenProcessWizardHandler;

/**
 * Handler class to open the process wizard (referenced in fragment.e4xmi).
 */
public class DepthCorrectionOpenWizardHandler
		extends AbstractOpenProcessWizardHandler<DepthCorrectionProcessWithWizard> {

	@Override
	protected DepthCorrectionProcessWithWizard buildProcessWithWizard(Shell shell, MItem callerItem) {
		return new DepthCorrectionProcessWithWizard(shell);
	}

}