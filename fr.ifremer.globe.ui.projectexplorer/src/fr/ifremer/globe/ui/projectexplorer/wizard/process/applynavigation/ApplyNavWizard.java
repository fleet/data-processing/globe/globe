package fr.ifremer.globe.ui.projectexplorer.wizard.process.applynavigation;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Merge wizard
 */
public class ApplyNavWizard extends Wizard {

	/** Model */
	protected ApplyNavWizardModel applyNavWizardModel;

	/** Pages */
	protected SelectInputFilesPage selectInputFilesPage;
	protected ApplyNavSelectNviPage applyNavSelectNviPage;
	protected SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public ApplyNavWizard(ApplyNavWizardModel mergeWizardModel) {
		this.applyNavWizardModel = mergeWizardModel;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(applyNavWizardModel);
		addPage(selectInputFilesPage);
		applyNavSelectNviPage = new ApplyNavSelectNviPage(applyNavWizardModel);
		addPage(applyNavSelectNviPage);
		selectOutputParametersPage = new SelectOutputParametersPage(applyNavWizardModel);
		addPage(selectOutputParametersPage);
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		return selectInputFilesPage.isPageComplete() && selectOutputParametersPage.isPageComplete();
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		return applyNavWizardModel.getInputFiles().isEmpty() ? selectInputFilesPage : applyNavSelectNviPage;
	}
}
