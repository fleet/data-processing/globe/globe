package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tonvi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.io.nvi.service.INviWriter.SamplingMode;
import fr.ifremer.globe.core.model.navigation.CompositeNavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.rawfiles.ConvertRawFilesOpenWizardHandler;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;

/**
 * "Convert to NVI" process with wizard.
 *
 * This class provides methods to convert sounder files (xsf, mbg). (Note: convert to NVI from raw files (.all/s7k) is
 * {@link ConvertRawFilesOpenWizardHandler}).
 */
public class ConvertToNviProcessWithWizard extends DefaultProcessWithWizard<ConvertToNviWizardModel> {

	private static final String NAME = "Convert to navigation files (.nvi)";

	/**
	 * Constructor
	 */
	public ConvertToNviProcessWithWizard(Shell shell) {
		super(shell, NAME, ConvertToNviWizardModel::new, ConvertToNviWizard::new);
	}

	/**
	 * @see fr.ifremer.globe.utils.process.GProcess#compute(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) {
		List<File> inputFiles = model.getInputFiles().asList();
		logger.info("Beginning conversion of {} files", inputFiles.size());
		if(model.canMergeOutputFiles()) {
			exportToNviMerge(monitor, logger);
		} else {
			for (File inputFile : model.getInputFiles().asList()) {
				SubMonitor submonitor = SubMonitor.convert(monitor, inputFiles.size());
				exportToNvi(inputFile, submonitor, logger);
				submonitor.done();
			}
		}
		logger.info("Conversion of {} files done", inputFiles.size());
		return Status.OK_STATUS;
	}

	/**
	 * Execute the conversion to NVI files
	 */
	protected File exportToNvi(File inputFile, IProgressMonitor monitor, Logger logger) {
		monitor.beginTask("Processing " + inputFile.getName(), 1);
		logger.info("Converting {} to NVI ", inputFile.getName());

		File result = outputFileNameComputer.compute(inputFile, model, ConvertToNviWizardModel.NVI_EXTENSION);
		if (shouldBeProcessed(result, logger)) {
			try {
				// Get sampling mode
				SamplingMode samplingMode = model.getNviSamplingMode().get();
				int samplingValue = samplingMode == SamplingMode.TIME ? model.getNviTimeSamplingValue().intValue()
						: model.getNviSoundingsSamplingValue().intValue();

				// Export
				INavigationDataSupplier src = getNavigationDataProxy(inputFile);
				INviWriter.grab().write(src, result.getAbsolutePath(), samplingMode, samplingValue);

				logger.info("Conversion of {} to {} done", inputFile.getName(), result.getName());
				loadOutputFile(result, monitor);
				monitor.worked(1);
			} catch (Exception e) {
				logger.error("Error while NVI conversion of {} to {} ({})", inputFile.getName(), result.getName(),
						e.getMessage(), e);
				FileUtils.deleteQuietly(result);
			}
		}
		return result;
	}

	/**
	 * Execute the conversion to NVI files
	 */
	protected File exportToNviMerge(IProgressMonitor monitor, Logger logger) {
		int inputFilesLength = model.getInputFiles().asList().size();
		logger.info("Converting files to NVI ");

		File result =  model.getOutputFiles().asList().get(0);
		if (shouldBeProcessed(result, logger)) {
			try {
				// Get sampling mode
				SamplingMode samplingMode = model.getNviSamplingMode().get();
				int samplingValue = samplingMode == SamplingMode.TIME ? model.getNviTimeSamplingValue().intValue()
						: model.getNviSoundingsSamplingValue().intValue();

				// aggregate navigations
				List<INavigationDataSupplier> src = new ArrayList<>();
				for (File inputFile : model.getInputFiles().asList()) {
					src.add(getNavigationDataProxy(inputFile));
				}
				boolean readonly = true;
				INavigationData navigationData = CompositeNavigationData.build(src, readonly);
				
				// Export
				INviWriter.grab().write(accessMode -> navigationData, result.getAbsolutePath(), samplingMode, samplingValue);

				logger.info("Conversion of {} files to {} done", inputFilesLength, result.getName());
				loadOutputFile(result, monitor);
				monitor.worked(1);
			} catch (Exception e) {
				logger.error("Error while NVI conversion of {} files to {} ({})", inputFilesLength, result.getName(),
						e.getMessage(), e);
				FileUtils.deleteQuietly(result);
			}
		}
		return result;
	}

}
