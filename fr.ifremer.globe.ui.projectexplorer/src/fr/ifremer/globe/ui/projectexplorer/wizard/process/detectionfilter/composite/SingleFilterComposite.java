package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.composite;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import fr.ifremer.globe.core.processes.detectionfilter.IAttributeProvider;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters.Comparator;
import fr.ifremer.globe.core.runtime.datacontainer.PredefinedLayers;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.filter.ADataComposite;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.filter.AllFiltersComposite;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.widget.AttributeCombo;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.widget.ComparatorCombo;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.exception.GIOException;

public class SingleFilterComposite extends Composite {
	private Button btnAdd;
	private Button btnRemove;
	private AttributeCombo AvalaibleData;
	private ComparatorCombo Action;
	private Label lblIs;

	IFilterLayer layers = null;

	SingleFilterParameters parameters;
	protected AllFiltersComposite filtersComposite;

	public SingleFilterComposite(Composite parent, int style, IFilterLayer layers,
			IAttributeProvider attributeProvider) {

		super(parent, SWT.BORDER);
		this.layers = layers;
		parameters = new SingleFilterParameters(attributeProvider);

		setLayout(new GridLayout(6, false));
		AvalaibleData = new AttributeCombo(this, SWT.READ_ONLY);
		AvalaibleData.getCombo()
				.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_FILL | GridData.HORIZONTAL_ALIGN_FILL));

		lblIs = new Label(this, SWT.NONE);
		lblIs.setAlignment(SWT.CENTER);
		lblIs.setText("is");
		lblIs.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));

		Action = new ComparatorCombo(this, SWT.READ_ONLY);
		Action.getCombo().setLayoutData(new GridData(GridData.VERTICAL_ALIGN_FILL | GridData.HORIZONTAL_ALIGN_FILL));

		filtersComposite = new AllFiltersComposite(attributeProvider, this, SWT.NONE);
		filtersComposite.setLayoutData(new GridData(
				GridData.VERTICAL_ALIGN_CENTER | GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL));

		btnAdd = new Button(this, SWT.NONE);
		btnAdd.setText("+");
		GridData gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.HORIZONTAL_ALIGN_FILL);
		gridData.widthHint = 25;
		btnAdd.setLayoutData(gridData);

		btnRemove = new Button(this, SWT.NONE);
		btnRemove.setText("-");
		btnRemove.setLayoutData(gridData);
		addListener();
		fill();
	}

	public void addListener() {
		final SingleFilterComposite myRef = this;
		btnAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					layers.add();
				} catch (GIOException e1) {
					// TODO Auto-generated catch block
					Messages.openWarningMessage("Filter Editor", "Error processing : \n" + e1.getMessage());

				}
			}
		});
		btnRemove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				layers.remove(myRef);
			}
		});

		Action.addSelectionChangedListener(event -> {
			Comparator selection = (Comparator) ((IStructuredSelection) event.getSelection()).getFirstElement();
			SingleFilterComposite.this.parameters.setComparator(selection);
			if (filtersComposite.getTopFilterComposite() != null) {
				filtersComposite.getTopFilterComposite().setComparator(selection);
			}
		});

		AvalaibleData.addSelectionChangedListener(event -> {
			PredefinedLayers<?> selection = (PredefinedLayers<?>) ((IStructuredSelection) event.getSelection())
					.getFirstElement();
			ADataComposite filterComposite = filtersComposite.show(selection);

			// Which comparator are allowed ?
			ViewerFilter filter = filterComposite.getComparatorFilter();
			Action.setFilters(filter != null ? new ViewerFilter[] { filter } : new ViewerFilter[] {});
			Action.setSelection(new StructuredSelection(filterComposite.getDefaultComparator()));

			// Because of ComparatorFilter, size of actions combo may
			// change.
			((GridData) Action.getCombo().getLayoutData()).widthHint = Action.getCombo().computeSize(SWT.DEFAULT,
					SWT.DEFAULT).x;
			SingleFilterComposite.this.layout();
		});

	}

	public void fill() {
		Action.setInput(Comparator.values());
		Action.setSelection(new StructuredSelection(Comparator.between));

		AvalaibleData.setInput(parameters.getAvailableAttributes());
		AvalaibleData.setSelection(new StructuredSelection(parameters.getAvailableAttributes().get(0)));
	}

	public void updateControlButton(boolean addEnabled, boolean minusEnabled) {
		btnAdd.setEnabled(addEnabled);
		btnRemove.setEnabled(minusEnabled);
	}

	public SingleFilterParameters getParameters() {

		return parameters;
	}

	/**
	 * retrieve values and update parameters
	 */
	public void updateParameters() {
		parameters.setSelectedAttributeIndex(AvalaibleData.getCombo().getSelectionIndex());
		parameters.setComparator((Comparator) ((IStructuredSelection) Action.getSelection()).getFirstElement());
		filtersComposite.getTopFilterComposite().updateParameters(parameters);
	}

}
