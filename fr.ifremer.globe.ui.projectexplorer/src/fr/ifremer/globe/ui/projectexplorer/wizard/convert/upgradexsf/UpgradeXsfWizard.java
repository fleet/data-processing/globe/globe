package fr.ifremer.globe.ui.projectexplorer.wizard.convert.upgradexsf;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Wizard to upgrade XSF files
 */
public class UpgradeXsfWizard extends Wizard {

	/** Model */
	protected UpgradeXsfModel upgradeXsfModel;

	/** Pages */
	protected SelectInputFilesPage selectXsfFilesPage;
	protected SelectInputFilesPage selectRawFilesPage;
	protected SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public UpgradeXsfWizard(UpgradeXsfModel upgradeXsfModel) {
		this.upgradeXsfModel = upgradeXsfModel;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectXsfFilesPage = new SelectInputFilesPage(upgradeXsfModel);
		addPage(selectXsfFilesPage);

		selectRawFilesPage = new SelectInputFilesPage(upgradeXsfModel.getRawFilesModel());
		addPage(selectRawFilesPage);

		selectOutputParametersPage = new SelectOutputParametersPage(upgradeXsfModel);
		addPage(selectOutputParametersPage);
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		return selectXsfFilesPage.isPageComplete() && selectRawFilesPage.isPageComplete()
				&& selectOutputParametersPage.isPageComplete();
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		return upgradeXsfModel.getInputFiles().isEmpty() ? selectXsfFilesPage : selectRawFilesPage;
	}

}
