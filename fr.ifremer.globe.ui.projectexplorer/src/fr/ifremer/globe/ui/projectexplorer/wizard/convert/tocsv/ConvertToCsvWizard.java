package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

/**
 * Wizard to convert to CSV files
 */
public class ConvertToCsvWizard extends Wizard {

	/** Model */
	protected ConvertToCsvWizardModel wizardModel;

	/** Pages */
	protected SelectInputFilesPage selectInputFilesPage;
	protected SelectCsvExportOptionPage selectCsvExportOptionPage;
	protected SelectOutputParametersPage selectOutputParametersPage;

	/**
	 * Constructor
	 */
	public ConvertToCsvWizard(ConvertToCsvWizardModel wizardModel) {
		this.wizardModel = wizardModel;
		setNeedsProgressMonitor(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(wizardModel);
		addPage(selectInputFilesPage);
		selectCsvExportOptionPage = new SelectCsvExportOptionPage(wizardModel);
		addPage(selectCsvExportOptionPage);
		selectOutputParametersPage = new SelectOutputParametersPage(wizardModel);
		selectOutputParametersPage.enableLoadFilesAfter(false);
		addPage(selectOutputParametersPage);
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		return selectInputFilesPage.isPageComplete() && selectOutputParametersPage.isPageComplete();
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		return wizardModel.getInputFiles().isEmpty() ? selectInputFilesPage : selectCsvExportOptionPage;
	}
}
