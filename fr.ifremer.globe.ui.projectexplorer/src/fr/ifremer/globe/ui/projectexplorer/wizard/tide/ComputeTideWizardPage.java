package fr.ifremer.globe.ui.projectexplorer.wizard.tide;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.function.DoubleFunction;
import java.util.stream.Collectors;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.nebula.widgets.formattedtext.FormattedText;
import org.eclipse.nebula.widgets.formattedtext.FormattedTextObservableValue;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;
import org.eclipse.wb.swt.SWTResourceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.impl.SimpleChartSeriesBuilder;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.javafxchart.FXChart;
import fr.ifremer.globe.ui.javafxchart.utils.FXChartException;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.utils.LatLongInputFormatter;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.color.ColorUtils;
import fr.ifremer.globe.utils.function.BiDoubleFunction;

/**
 * Page to compute tide.
 */
public class ComputeTideWizardPage extends WizardPage {

	protected Logger logger = LoggerFactory.getLogger(ComputeTideWizardPage.class);

	IGeographicViewService geographicView = IGeographicViewService.grab();

	/** Properties **/
	private final ComputeTideWizardModel model;

	/** Inner widgets **/
	private TideGaugeComposite tideGaugeWidget;
	private TidePredictionComposite predictionComposite;
	private TideEstimationComposite estimationComposite;

	/** Inner widgets **/
	private Button radioButtonNavFromInputFiles;
	private Button radioButtonCustomNav;
	private Text textLatitude;
	private Text textLongitude;
	private Button btnSelectFromGeographic;
	private Label lblLatitude;
	private Label lblLongitude;

	private DateTime startDate;
	private DateTime startTime;
	private DateTime endDate;
	private DateTime endTime;
	private Button btnInterval;
	private Spinner intervalSpinner;

	private CTabItem tabTideObservation;
	private CTabItem tabTidePrediction;
	private CTabItem tabTideEstimation;
	private CTabFolder tabFolder;

	private Composite legendParentComposite;
	private Composite compositeChartParent;
	private FXChart chart;

	/** Date constant & formatters **/
	private static final TimeZone UTC = TimeZone.getTimeZone("UTC");
	private static final DateFormat DATE_FORMATTER = new SimpleDateFormat("dd/MM/yy HH:mm");
	static {
		DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone("GMT"));
	}
	private static final DoubleFunction<String> DATA_FORMATTER = d -> String.format("%.3f", d) + " m";
	public static final BiDoubleFunction<String> TOOLTIP_FORMATTER = //
			(x, y) -> DATE_FORMATTER.format(new Date((long) x)) + " : " + DATA_FORMATTER.apply(y);

	private Button btnCompute;
	/** Current running process */
	private Optional<ComputeTideProcess> processOption = Optional.empty();

	/**
	 * Constructor
	 */
	public ComputeTideWizardPage(ComputeTideWizardModel depthCorrectionWizardModel) {
		super("");
		model = depthCorrectionWizardModel;
		setTitle("Compute tide");
	}

	/**
	 * Creates the graphical interface.
	 */
	@Override
	public void createControl(Composite parent) {
		Composite mainComposite = new Composite(parent, SWT.NONE);

		// localisation
		Group grpLocalisation = new Group(mainComposite, SWT.NONE);
		grpLocalisation.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpLocalisation.setText("Localisation");
		grpLocalisation.setLayout(new GridLayout(6, false));
		grpLocalisation.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		radioButtonCustomNav = new Button(grpLocalisation, SWT.RADIO);
		radioButtonCustomNav.setText("Custom position :");

		lblLatitude = new Label(grpLocalisation, SWT.NONE);
		lblLatitude.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLatitude.setText("lat(°)");

		textLatitude = new Text(grpLocalisation, SWT.BORDER);
		textLatitude.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new FormattedText(textLatitude).setFormatter(LatLongInputFormatter.getLatitudeFormatter());

		lblLongitude = new Label(grpLocalisation, SWT.NONE);
		lblLongitude.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLongitude.setText("lon(°)");

		textLongitude = new Text(grpLocalisation, SWT.BORDER);
		textLongitude.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new FormattedText(textLongitude).setFormatter(LatLongInputFormatter.getLongitudeFormatter());

		btnSelectFromGeographic = new Button(grpLocalisation, SWT.NONE);
		btnSelectFromGeographic
				.setImage(ResourceManager.getPluginImage("fr.ifremer.globe", "icons/16/16x16-icon-earth.png"));
		btnSelectFromGeographic.setText("Select on geographic view");

		radioButtonNavFromInputFiles = new Button(grpLocalisation, SWT.RADIO);
		radioButtonNavFromInputFiles.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 6, 1));
		radioButtonNavFromInputFiles.setText("Input file navigation (position computed by time interpolation)");
		btnSelectFromGeographic.addListener(SWT.Selection, e -> selectPointOnGeographicView());

		// date
		Group grpDate = new Group(mainComposite, SWT.NONE);
		grpDate.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpDate.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1));
		grpDate.setText("Dates (UTC)");
		grpDate.setLayout(new GridLayout(7, false));

		startDate = new DateTime(grpDate, SWT.BORDER);
		startTime = new DateTime(grpDate, SWT.BORDER | SWT.TIME);

		Label label = new Label(grpDate, SWT.NONE);
		label.setText("to");

		endDate = new DateTime(grpDate, SWT.BORDER);
		endTime = new DateTime(grpDate, SWT.BORDER | SWT.TIME);

		// time interval
		btnInterval = new Button(grpDate, SWT.CHECK);
		btnInterval.setText("Interval (min)");
		intervalSpinner = new Spinner(grpDate, SWT.BORDER);
		intervalSpinner.setEnabled(false);
		intervalSpinner.setSelection(10);
		btnInterval.addListener(SWT.Selection, e -> intervalSpinner.setEnabled(btnInterval.getSelection()));

		GridLayout glMainComposite = new GridLayout(1, false);
		mainComposite.setLayout(glMainComposite);

		tabFolder = new CTabFolder(mainComposite, SWT.FLAT);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		tabFolder.addSelectionListener(new SelectionAdapter() {
			// dynamically adapt layout depending on selected tab
			@Override
			public void widgetSelected(SelectionEvent e) {
				mainComposite.layout();
			}
		});
		// observation tab
		tabTideObservation = new CTabItem(tabFolder, SWT.NONE);
		tabTideObservation.setText("Tide gauge observations");
		tabFolder.setSelection(tabTideObservation);

		var composite = new Composite(tabFolder, SWT.NONE);
		tabTideObservation.setControl(composite);
		composite.setLayout(new GridLayout(1, false));

		var lblGetTideFrom = new Label(composite, SWT.NONE);
		lblGetTideFrom.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		lblGetTideFrom.setText("Get tide from tide gauge observations.");
		lblGetTideFrom.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.ITALIC));
		tideGaugeWidget = new TideGaugeComposite(composite, SWT.NONE, model);
		tideGaugeWidget.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		// prediction tab
		tabTidePrediction = new CTabItem(tabFolder, SWT.NONE);
		tabTidePrediction.setText("Tide prediction");
		predictionComposite = new TidePredictionComposite(tabFolder, SWT.NONE, model);
		tabTidePrediction.setControl(predictionComposite);

		// plateform elevation tab
		tabTideEstimation = new CTabItem(tabFolder, SWT.NONE);
		tabTideEstimation.setText("Plateform elevation");
		estimationComposite = new TideEstimationComposite(tabFolder, SWT.NONE, model);
		tabTideEstimation.setControl(estimationComposite);

		btnCompute = new Button(mainComposite, SWT.NONE);
		btnCompute.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		btnCompute.setText("Compute");
		btnCompute.addListener(SWT.Selection, e -> onCompute());

		// chart
		compositeChartParent = new Composite(mainComposite, SWT.NONE);
		compositeChartParent.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		compositeChartParent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		GridLayout gl_compositeChartParent = new GridLayout(1, false);
		gl_compositeChartParent.verticalSpacing = 0;
		gl_compositeChartParent.marginHeight = 0;
		gl_compositeChartParent.marginWidth = 0;
		compositeChartParent.setLayout(gl_compositeChartParent);

		Composite compositeChart = new Composite(compositeChartParent, SWT.NONE);
		compositeChart.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		compositeChart.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		compositeChart.setSize(719, 86);
		compositeChart.setLayout(new FillLayout(SWT.HORIZONTAL));

		try {
			chart = new FXChart(compositeChart, "Correction data");
			chart.setXAxisLabelFormatter(d -> DATE_FORMATTER.format(new Date(d.longValue())));
			chart.setYAxisLabelFormatter(d -> DATA_FORMATTER.apply(d.doubleValue()));
			chart.setVerticalZoomEnabled(false);
		} catch (FXChartException | IOException e) {
			logger.error(e.getMessage(), e);
		}

		legendParentComposite = new Composite(compositeChartParent, SWT.NONE);
		legendParentComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		legendParentComposite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		RowLayout legendParentCompositeLayout = new RowLayout(SWT.HORIZONTAL);
		legendParentCompositeLayout.justify = true;
		legendParentComposite.setLayout(legendParentCompositeLayout);

		setControl(mainComposite);
		setPageComplete(false);
		enableComputeTide(true);
		initialize();
		mainComposite.addDisposeListener(e -> onDispose());
	}

	/**
	 * Initializes parameter with input files.
	 */
	@SuppressWarnings("unchecked")
	private void initialize() {
		model.isLocalisationFromInputFiles.addChangeListener(e -> {
			lblLongitude.setEnabled(!model.isLocalisationFromInputFiles.get());
			textLongitude.setEnabled(!model.isLocalisationFromInputFiles.get());
			lblLatitude.setEnabled(!model.isLocalisationFromInputFiles.get());
			textLatitude.setEnabled(!model.isLocalisationFromInputFiles.get());
			btnSelectFromGeographic.setEnabled(!model.isLocalisationFromInputFiles.get());
		});
		model.isLocalisationFromInputFiles.set(!model.getInputFiles().isEmpty());

		DataBindingContext bindingContext = new DataBindingContext();
		// Bind geobox local variables to widgets
		bindingContext.bindValue(WidgetProperties.widgetSelection().observe(radioButtonNavFromInputFiles),
				model.isLocalisationFromInputFiles, null, null);

		bindingContext.bindValue(
				new FormattedTextObservableValue((FormattedText) textLongitude.getData(FormattedText.TEXT_DATA_KEY)),
				model.getLongitude());
		bindingContext.bindValue(
				new FormattedTextObservableValue((FormattedText) textLatitude.getData(FormattedText.TEXT_DATA_KEY)),
				model.getLatitude());

		var center = model.getCenterPosition();
		model.getLongitude().set(center.getLongitude().degrees);
		model.getLatitude().set(center.getLatitude().degrees);

		// setup input files dependent actions
		model.getInputFiles().addChangeListener(e -> onInputFilesChanged());

		onInputFilesChanged();

	}

	/**
	 * Methode called when input files changed
	 */
	private void onInputFilesChanged() {
		// set radio buttons
		boolean inputFilePresent = !model.getInputFiles().isEmpty();
		radioButtonNavFromInputFiles.setEnabled(inputFilePresent);
		radioButtonCustomNav.setSelection(!inputFilePresent);

		model.isLocalisationFromInputFiles.set(inputFilePresent);

		// compute start / end dates from input files
		List<IFileInfo> fileInfos = IFileService.grab()
				.getFileInfos(model.getInputFiles().stream().map(File::getAbsolutePath).collect(Collectors.toList()));
		List<Pair<Instant, Instant>> startEndDates = fileInfos.stream() //
				.map(IFileInfo::getStartEndDate).filter(Optional::isPresent).map(Optional::get)//
				.collect(Collectors.toList());

		Instant startInstant = Instant.MAX;
		Instant endInstant = Instant.MIN;
		for (Pair<Instant, Instant> startEndDate : startEndDates) {
			if (startEndDate.getFirst().isBefore(startInstant))
				startInstant = startEndDate.getFirst();
			if (startEndDate.getSecond().isAfter(endInstant))
				endInstant = startEndDate.getSecond();
		}
		if (startInstant == Instant.MAX) {
			startInstant = Instant.now();
			endInstant = Instant.now();
		}

		// add/remove an hour to be sure to encapsulate files dates
		startInstant = startInstant.minus(Duration.ofHours(1));
		endInstant = endInstant.plus(Duration.ofHours(1));

		Calendar startCalendar = Calendar.getInstance(UTC);
		startCalendar.setTimeInMillis(startInstant.toEpochMilli());
		startDate.setDate(startCalendar.get(Calendar.YEAR), startCalendar.get(Calendar.MONTH),
				startCalendar.get(Calendar.DAY_OF_MONTH));
		startTime.setTime(startCalendar.get(Calendar.HOUR_OF_DAY), startCalendar.get(Calendar.MINUTE),
				startCalendar.get(Calendar.SECOND));

		Calendar endCalendar = Calendar.getInstance(UTC);
		endCalendar.setTimeInMillis(endInstant.toEpochMilli());
		endDate.setDate(endCalendar.get(Calendar.YEAR), endCalendar.get(Calendar.MONTH),
				endCalendar.get(Calendar.DAY_OF_MONTH));
		endTime.setTime(endCalendar.get(Calendar.HOUR_OF_DAY), endCalendar.get(Calendar.MINUTE),
				endCalendar.get(Calendar.SECOND));

	}

	/**
	 * Method called when the selected file changed.
	 */
	private void onCompute() {
		if (processOption.isPresent()) {
			Messages.openInfoMessage(getTitle(), "A computation is already running");
			return;
		}

		// get time interval
		Calendar startCalendar = Calendar.getInstance(UTC);
		startCalendar.set(startDate.getYear(), startDate.getMonth(), startDate.getDay(), startTime.getHours(),
				startTime.getMinutes(), startTime.getSeconds());
		model.setStart(startCalendar.toInstant());

		Calendar endCalendar = Calendar.getInstance(UTC);
		endCalendar.set(endDate.getYear(), endDate.getMonth(), endDate.getDay(), endTime.getHours(),
				endTime.getMinutes(), endTime.getSeconds());
		model.setEnd(endCalendar.toInstant());

		model.setIntervalInMinutes(
				btnInterval.getSelection() ? Optional.of(intervalSpinner.getSelection()) : Optional.empty());

		// What kind of process to launch ?
		model.getOtherDataToDisplay().clear();
		if (tabFolder.getSelection() == tabTideObservation) {
			processOption = tideGaugeWidget.prepareCompute();
		} else if (tabFolder.getSelection() == tabTidePrediction) {
			processOption = predictionComposite.prepareCompute();
		} else if (tabFolder.getSelection() == tabTideEstimation) {
			processOption = estimationComposite.prepareCompute();
		}

		processOption.ifPresent(process -> {
			// Before launching...
			setErrorMessage(null);
			setPageComplete(false);
			enableComputeTide(false);
			// After a good computation
			process.whenIsDone(e -> {
				if (!getControl().isDisposed()) {
					getControl().getDisplay().asyncExec(() -> {
						process.onFinished();
						onComputingDone();
					});
				}
			});
			// Even if something went wrong
			process.whenException(this::onComputingFailed);
			// Go !
			IProcessService.grab().runInBackground(process, false);
		});
	}

	/**
	 *
	 */
	protected void onComputingDone() {
		// update chart
		List<IChartSeries<? extends IChartData>> series = model.getOtherDataToDisplay();

		List<double[]> chartData = model.getTideData().stream()
				.map(obs -> new double[] { obs.getTimestamp().toEpochMilli(), obs.getValue() }).toList();
		series.add(new SimpleChartSeriesBuilder("Tide").withData(chartData).withTooltipFormatter(TOOLTIP_FORMATTER)
				.build());
		chart.setData(series);
		chart.setTitle(model.getTideDataTitle().get());

		// update legends
		for (Control control : legendParentComposite.getChildren()) {
			control.dispose();
		}
		series.forEach(s -> {
			var lblNewLabel = new Label(legendParentComposite, SWT.NONE);
			lblNewLabel.setText(s.getTitle());
			lblNewLabel.setForeground(ColorUtils.convertGColorToSWT(s.getColor()));
			lblNewLabel.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		});
		legendParentComposite.layout();
		compositeChartParent.layout();

		processOption = Optional.empty();
		setPageComplete(true);
		enableComputeTide(true);
	}

	/**
	 *
	 */
	protected void onComputingFailed(Throwable e) {
		getControl().getDisplay().asyncExec(() -> {
			if (!getControl().isDisposed()) {
				chart.setData(Collections.emptyList());
				chart.setTitle("Error : no data to dislay");
				model.setTideData(Collections.emptyList());
				setErrorMessage(e.getMessage());
				processOption = Optional.empty();
				setPageComplete(false);
				enableComputeTide(true);
			}
		});
	}

	/**
	 * Updates custom position after a click on geographic view.
	 */
	private void selectPointOnGeographicView() {
		geographicView.waitForClickByUser(position -> {
			if (!getControl().isDisposed()) {
				model.getLongitude().doSetValue(position.longitude.degrees);
				model.getLatitude().doSetValue(position.latitude.degrees);
			}
		});
	}

	protected void enableComputeTide(boolean enabled) {
		getControl().setEnabled(enabled);
		if (enabled) {
			setDescription("Compute tide from observations or prediction tools.");
			btnCompute.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.ui", "icons/run_exc.png"));
		} else {
			setDescription("The tide computation is in progress.");
			btnCompute.setImage(ResourceManager.getPluginImage("fr.ifremer.globe.ui", "icons/16/sand-timer.png"));
		}
	}

	@Override
	public void performHelp() {
		if (tabFolder.getSelection() == tabTideEstimation) {
			estimationComposite.opendHelp();
		} else {
			predictionComposite.opendHelp();
		}
	}

	/** {@inheritDoc} */
	@Override
	public IWizardPage getPreviousPage() {
		// When a process is running, disable the previous button
		if (processOption.isPresent()) {
			return null;
		}
		return super.getPreviousPage();
	}

	/** On dispose, kill the current process */
	protected void onDispose() {
		processOption.ifPresent(ComputeTideProcess::cancel);
	}
}
