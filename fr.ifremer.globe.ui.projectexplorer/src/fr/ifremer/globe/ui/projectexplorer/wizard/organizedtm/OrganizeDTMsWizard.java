package fr.ifremer.globe.ui.projectexplorer.wizard.organizedtm;

import java.util.List;

import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.projectexplorer.wizard.organizedtm.OrganizeDTMsPage.OrganizationMode;
import fr.ifremer.globe.ui.projectexplorer.wizard.organizedtm.OrganizeDTMsPage.OrganizeDTMParameter;


public class OrganizeDTMsWizard extends Wizard {

	private OrganizeDTMsPage page;
	private List<OrganizeDTMParameter> selectedParameters;
	private OrganizationMode selectedMode;
	private boolean deleteEmptyGroups;


	@Override
	public void addPages() {
		page = new OrganizeDTMsPage();
		addPage(page);
	}

	/**
	 * Finish button is enabled
	 */
	@Override
	public boolean canFinish() {
		return true;
	}

	/**
	 * Finish process: called when the wizard is closed, retrieve user selections
	 */
	@Override
	public boolean performFinish() {
		this.selectedParameters = page.getSelectedParameters();
		this.selectedMode = page.getSelectedMode();
		this.deleteEmptyGroups = page.deleteEmptyGroups();
		return true;
	}

	/** Get selected parameters to organize DTM **/
	public List<OrganizeDTMParameter> getSelectedParameters() {
		return this.selectedParameters;
	}

	/** Get selected mode **/
	public OrganizationMode getSelectedMode() {
		return this.selectedMode;
	}

	/** Returns true if empty groups have to be deleted **/
	public boolean deleteEmptyGroups() {
		return this.deleteEmptyGroups;
	}

}
