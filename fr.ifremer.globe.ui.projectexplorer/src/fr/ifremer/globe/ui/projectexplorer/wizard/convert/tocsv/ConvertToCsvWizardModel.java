package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv;

import java.util.Optional;

import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.SelectCsvExportOptionPage.SelectCsvExportOptionPageModel;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;

/**
 * Model of wizard page
 */
public class ConvertToCsvWizardModel extends ConvertWizardDefaultModel implements SelectCsvExportOptionPageModel {

	/** CSV file extension */
	public static final String CSV_EXTENSION = "csv";
	/** Validity flags extension */
	public static final String VALIDITY_EXTENSION = "validityflags.csv";

	/** Export option */
	protected String fileExtension;
	protected WritableObject<Character> columnSeparator = new WritableObject<>(';');
	protected WritableBoolean withHeader = new WritableBoolean(true);
	protected Optional<WritableBoolean> positiveElevation = Optional.empty();

	/**
	 * Constructor
	 */
	public ConvertToCsvWizardModel(String fileExtension) {
		this.fileExtension = fileExtension;
		outputFormatExtensions.add(fileExtension);
		columnSeparator.addChangeListener(event -> checkColumnSeparator());
	}

	/** Column separator is comma by default */
	protected void checkColumnSeparator() {
		if (columnSeparator.isNull()) {
			columnSeparator.set(';');
		}
	}

	/** Getter of {@link #columnSeparator} */
	@Override
	public WritableObject<Character> getColumnSeparator() {
		return columnSeparator;
	}

	/** Getter of {@link #withHeader} */
	@Override
	public WritableBoolean getWithHeader() {
		return withHeader;
	}

	/** Getter of {@link #positiveElevation} */
	@Override
	public Optional<WritableBoolean> getPositiveElevation() {
		return positiveElevation;
	}

	/** @return height above surface sign **/
	public double getElevationSign() {
		return positiveElevation.isPresent() && positiveElevation.get().isTrue() ? 1d : -1d;
	}

	/** Setter of {@link #positiveElevation} */
	public void setPositiveElevation(Optional<WritableBoolean> positiveElevation) {
		this.positiveElevation = positiveElevation;
	}

	/** Getter of {@link #fileExtension} */
	public String getFileExtension() {
		return fileExtension;
	}

	/** Setter of {@link #fileExtension} */
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

}