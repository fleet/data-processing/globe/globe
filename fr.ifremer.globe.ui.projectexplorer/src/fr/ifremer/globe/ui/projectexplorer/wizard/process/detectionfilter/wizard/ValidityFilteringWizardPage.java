/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.wizard;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.core.processes.detectionfilter.IAttributeProvider;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.DataParameters;
import fr.ifremer.globe.core.processes.detectionfilter.parameters.SingleFilterParameters;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.composite.DataComposite;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.composite.IFilterLayer;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.detectionfilter.composite.SingleFilterComposite;
import fr.ifremer.globe.ui.service.wizard.ISummarizableWizardPage;
import swing2swt.layout.BorderLayout;

/**
 *
 */
public class ValidityFilteringWizardPage extends WizardPage implements ISummarizableWizardPage, IFilterLayer {

	private IAttributeProvider attributeProvider;

	private Composite mainComposite;
	private Composite compositeTop;
	private Button btnOr;

	private Composite compositeCenter;
	private ArrayList<SingleFilterComposite> compositeList = new ArrayList<>();

	private Composite compositeBottom;
	private DataComposite dataComposite;

	/**
	 * Constructor
	 */
	public ValidityFilteringWizardPage() {
		super(ValidityFilteringWizardPage.class.getSimpleName(), "Select criterias", null);
		setPageComplete(false);
	}

	/** {@inheritDoc} */
	@Override
	public void createControl(Composite parent) {
		mainComposite = new Composite(parent, SWT.NONE);
		mainComposite.setLayout(new BorderLayout(0, 0));
		setControl(mainComposite);

		compositeTop = new Composite(mainComposite, SWT.NONE);
		compositeTop.setLayoutData(BorderLayout.NORTH);
		compositeTop.setLayout(new GridLayout(1, false));

		compositeCenter = new Composite(mainComposite, SWT.NONE);
		compositeCenter.setLayoutData(BorderLayout.CENTER);
		compositeCenter.setLayout(new GridLayout(1, false));

		compositeBottom = new Composite(mainComposite, SWT.BORDER);
		compositeBottom.setLayoutData(BorderLayout.SOUTH);
		compositeBottom.setLayout(new GridLayout(2, false));

		btnOr = new Button(compositeTop, SWT.CHECK);
		btnOr.setText("set if at least one condition is true");
		btnOr.addSelectionListener(SelectionListener
				.widgetSelectedAdapter(e -> dataComposite.getParameters().setCheckAllCriterias(!btnOr.getSelection())));

		dataComposite = new DataComposite(compositeBottom, SWT.NONE, null);
		dataComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
	}

	@Override
	public void remove(SingleFilterComposite layer) {
		compositeList.remove(layer);
		layer.dispose();
		updateLastControl();

		mainComposite.layout(true, true);
	}

	public void init(IAttributeProvider attributeProvider) {
		setPageComplete(attributeProvider != null);
		this.attributeProvider = attributeProvider;
		for (int i = compositeList.size() - 1; i >= 0; i--) {
			remove(compositeList.get(i));
		}
		add();
		dataComposite.setAttributeProvider(attributeProvider);
		dataComposite.layout();
	}

	@Override
	public void add() {
		SingleFilterComposite singleFilterComposite = new SingleFilterComposite(compositeCenter, SWT.NONE, this,
				attributeProvider);
		singleFilterComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		compositeList.add(singleFilterComposite);
		updateLastControl();
		mainComposite.layout(true, true);
	}

	private void updateLastControl() {
		int i = 0;
		for (i = 0; i < compositeList.size(); i++) {
			boolean last = false;
			boolean first = false;
			if (i == compositeList.size() - 1) {
				last = true;
			}
			if (i == 0) {
				first = true;
			}
			compositeList.get(i).updateControlButton(last, !first);
		}
	}

	/** {@inheritDoc} */
	@Override
	public String summarize() {
		return "";
	}

	public List<SingleFilterParameters> getCriterias() {
		return compositeList.stream()//
				.map(SingleFilterComposite::getParameters)//
				.collect(Collectors.toList());
	}

	public DataParameters getDataToSet() {
		return dataComposite.getParameters();
	}

	/** {@inheritDoc} */
	@Override
	public void setVisible(boolean visible) {

		if (!visible) {
			// Force updating parameters before switching to another page
			compositeList.stream().forEach(SingleFilterComposite::updateParameters);
			dataComposite.updateParameters();
		}

		super.setVisible(visible);
	}

}
