package fr.ifremer.globe.ui.projectexplorer.wizard.process.applynavigation;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.databinding.observable.WritableFile;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;

public class ApplyNavWizardModel extends ConvertWizardDefaultModel {

	/** Input navigation file **/
	private WritableFile navigationFile = new WritableFile();

	/** Apply immersion option */
	protected WritableBoolean applyImmersion = new WritableBoolean();

	/**
	 * Constructor
	 */
	public ApplyNavWizardModel() {
		IFileService fileService = IFileService.grab();
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.getSounderNcContentType()));
	}

	public WritableFile getNavigationFile() {
		return navigationFile;
	}

	public WritableBoolean getApplyImmersion() {
		return applyImmersion;
	}

}
