package fr.ifremer.globe.ui.projectexplorer.wizard.process.depthcorrection;

import java.io.File;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.tide.TideType;
import fr.ifremer.globe.core.processes.TimeIntervalParameters;
import fr.ifremer.globe.core.processes.depthcorrection.DepthCorrectionParameters;
import fr.ifremer.globe.core.processes.depthcorrection.DepthCorrectionSource;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.PlatformLayers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.ui.databinding.observable.WritableFloat;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;
import fr.ifremer.globe.utils.exception.GException;

/**
 * Model of the wizard page
 */
public class DepthCorrectionWizardModel extends ConvertWizardDefaultModel {

	protected Logger logger = LoggerFactory.getLogger(DepthCorrectionWizardModel.class);

	/** Depth correction parameters **/
	private DepthCorrectionParameters depthCorrectionParameters = new DepthCorrectionParameters();
	private TimeIntervalParameters timeIntervalParameters = new TimeIntervalParameters(
			"fr.ifremer.globe.process.depthcorrection");
	private TideType tideType = TideType.UNKNWON;
	private DepthCorrectionSource tideCorrectionSource = DepthCorrectionSource.NONE;
	private DepthCorrectionSource draughtCorrectionSource = DepthCorrectionSource.NONE;

	private WritableFloat nominalWaterline = new WritableFloat(0.f);

	private static final String DEFAULT_TIDE_SUFFIX = "tide";

	/**
	 * Constructor
	 */
	public DepthCorrectionWizardModel() {

		IFileService fileService = IFileService.grab();
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.getSounderNcContentType()));

		IDataContainerOwner owner = IDataContainerOwner.generate("DepthCorrectionWizardModel", true);
		IDataContainerFactory dataContainerFactory = IDataContainerFactory.grab();

		inputFiles.addChangeListener(event -> {
			// Update
			long minDate = Long.MAX_VALUE;
			long maxDate = -Long.MAX_VALUE;
			for (File file : inputFiles) {
				Optional<IFileInfo> infoStore = fileService.getFileInfo(file.getAbsolutePath());
				if (infoStore.isPresent() && infoStore.get().getContentType().isSounderNcInfo()) {
					ISounderNcInfo infoFile = (ISounderNcInfo) infoStore.get();

					// dates
					minDate = Math.min(minDate, infoFile.getFirstPingDate().getTime());
					maxDate = Math.max(maxDate, infoFile.getLastPingDate().getTime());
					// waterline (not available with MBG)
					if (infoFile.getContentType() != ContentType.MBG_NETCDF_3) {
						try (ISounderDataContainerToken token = dataContainerFactory.book(infoFile, owner)) {
							Optional<FloatLoadableLayer1D> waterlineLayer = token.getDataContainer()
									.getOptionalLayer(PlatformLayers.WATER_LEVEL);
							if (waterlineLayer.isPresent()) {
								nominalWaterline.set(waterlineLayer.get().get(0));
							}
						} catch (GException e) {
							Messages.openErrorMessage("Error : file not available. ", e);
						}
					}
				}
			}
			if (minDate != Long.MAX_VALUE) {
				timeIntervalParameters.setLongProperty(TimeIntervalParameters.PROPERTY_STARTDATETIME_VALUE, minDate);
			}
			if (maxDate != -Long.MAX_VALUE) {
				timeIntervalParameters.setLongProperty(TimeIntervalParameters.PROPERTY_ENDDATETIME_VALUE, maxDate);
			}
		});

		// Apply default options parameters
		// (from fr.ifremer.globe.process.depthcorrection/resources/defaultParameters.properties)
		timeIntervalParameters.restoreDefault();

		suffix.set(DEFAULT_TIDE_SUFFIX);
	}

	// GETTERS

	public DepthCorrectionParameters getDepthCorrectionParameters() {
		return depthCorrectionParameters;
	}

	public TimeIntervalParameters getTimeIntervalParameters() {
		return timeIntervalParameters;
	}

	public TideType getTideType() {
		return tideType;
	}

	public void setTideType(TideType tideType) {
		this.tideType = tideType;
	}

	public DepthCorrectionSource getTideCorrectionSource() {
		return tideCorrectionSource;
	}

	public void setTideCorrectionSource(DepthCorrectionSource tideCorrectionSource) {
		this.tideCorrectionSource = tideCorrectionSource;
	}

	public DepthCorrectionSource getDraughtCorrectionSource() {
		return draughtCorrectionSource;
	}

	public void setDraughtCorrectionSource(DepthCorrectionSource draughtCorrectionSource) {
		this.draughtCorrectionSource = draughtCorrectionSource;
	}

	public WritableFloat getNominalWaterLine() {
		return nominalWaterline;
	}
}