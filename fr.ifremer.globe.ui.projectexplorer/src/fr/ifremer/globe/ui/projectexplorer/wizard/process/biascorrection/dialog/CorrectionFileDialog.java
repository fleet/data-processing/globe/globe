package fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionList;
import fr.ifremer.globe.core.processes.biascorrection.model.CorrectionPoint;
import fr.ifremer.globe.utils.date.DateUtils;

/**
 * Generic class which contains commons components of correction file creation dialogs.
 */
public class CorrectionFileDialog extends Dialog {

	protected Composite modesComposite;
	protected Composite modificationModeComposite;
	protected Composite parameterModeComposite;
	protected Composite tableAndEditionComposite;
	protected Composite tableComposite;
	protected Composite editionComposite;

	protected Button parametersButton;
	protected Button timeDelayButton;
	protected Button deleteButton;
	protected Button addButton;

	protected Label parametersLabel;
	protected Label warningLabel;

	private static final String FILE_COLUMN_NAME = "File";
	private static final String DATE_COLUMN_NAME = "Date";
	private static final String VELOCITY_COLUMN_NAME = "Velocity (m/s)";
	private static final String HEADING_COLUMN_NAME = "Heading (°)";
	private static final String ROLL_COLUMN_NAME = "Roll (°)";
	private static final String PITCH_COLUMN_NAME = "Pitch (°)";
	private static final String VERTICAL_OFFSET_COLUMN_NAME = "Platform vertical offset (m)";
	private static final String MRU_HEADING_COLUMN_NAME = "MRU Heading (°)";
	private static final String ATTITUDE_TIMEDELAY_COLUMN_NAME = "Attitude Time (ms)";
	private static final String VERTICAL_OFFSET_TIMEDELAY_COLUMN_NAME = "Platform vertical offset Time (ms)";

	public static final int FILE_COLUMN_INDEX = 0;
	public static final int DATE_COLUMN_INDEX = 1;
	public static final int VELOCITY_COLUMN_INDEX = 2;
	public static final int HEADING_COLUMN_INDEX = 3;
	public static final int ROLL_COLUMN_INDEX = 4;
	public static final int PITCH_COLUMN_INDEX = 5;
	public static final int VERTICAL_OFFSET_COLUMN_INDEX = 6;
	public static final int MRU_HEADING_COLUMN_INDEX = 7;
	public static final int ATTITUDE_TIME_COLUMN_INDEX = 8;
	public static final int VERTICAL_OFFSET_TIME_COLUMN_INDEX = 9;
	public static final int REMOVE_LINE_INDEX = 10;

	public static final int EXPORT_COMMON_ID = IDialogConstants.CLIENT_ID + 1;
	public static final int EXPORT_INDIVIDUAL_ID = IDialogConstants.CLIENT_ID + 2;

	protected CorrectionFileDialog(Shell parentShell) {
		super(parentShell);
	}

	/*
	 * (non-Javadoc) Method declared on Dialog.
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		super.buttonPressed(buttonId);
	}

	/**
	 * Initialize all composites of the dialog.
	 *
	 * @param parent composite of parent dialog
	 */
	public void initComposites(Composite parent) {
		Composite modesAndTableAndButtonsComposite;

		modesAndTableAndButtonsComposite = (Composite) super.createDialogArea(parent);

		modesComposite = new Composite(modesAndTableAndButtonsComposite, SWT.NONE);
		modesComposite.setLayout(new GridLayout(1, false));

		tableComposite = new Composite(modesAndTableAndButtonsComposite, SWT.NONE);
		GridLayout tableCompositeLayout = new GridLayout(1, false);
		tableCompositeLayout.marginHeight = 0;
		tableComposite.setLayout(tableCompositeLayout);

		editionComposite = new Composite(modesAndTableAndButtonsComposite, SWT.NONE);

		editionComposite.setLayout(new GridLayout(3, false));
	}

	/**
	 * Add the components relatives the warning message.
	 */
	public void initOneLineWarningArea() {

		this.warningLabel = new Label(this.modesComposite, SWT.NONE);
		this.warningLabel.setText("Only bias mode");
		this.warningLabel.setVisible(true);

		this.warningLabel = new Label(this.modesComposite, SWT.NONE);
		this.warningLabel.setForeground(new Color(Display.getCurrent(), 255, 128, 0));
		this.warningLabel
				.setText("If a table has one line, its values will be applied to the entire target file to correct.");
		this.warningLabel.setVisible(false);
	}

	public TableViewer initTable(boolean isUnique, int style, Composite parent) {
		return initTable(isUnique, style, parent, true);
	}

	/**
	 * Initialize a {@link TableViewer} with columns and editor listener.
	 * 
	 * @param isUnique true if only one {@link TableViewer} should be into the parent composite.
	 * @param style style of the {@link Table} contained by the {@link TableViewer}.
	 * @param parent parent composite of {@link TableViewer}
	 * @param timeColumnVisible (true| false)
	 * @return a new {@link TableViewer} which is adapted for the correction files edition
	 */
	public TableViewer initTable(boolean isUnique, int style, Composite parent, boolean timeColumnVisible) {

		if (isUnique) {
			for (Control control : parent.getChildren()) {
				control.dispose();
			}
		}

		parent.setRedraw(false);
		TableViewer tableViewer = new TableViewer(parent, style);
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		if (timeColumnVisible)
			data.heightHint = 330;
		else
			data.heightHint = 145;

		tableViewer.getTable().setLayoutData(data);
		tableViewer.getTable().setLinesVisible(true);
		tableViewer.getTable().setHeaderVisible(true);

		// Windows limitation: first column alignement is always LEFT

		TableViewerColumn fileColumn = new TableViewerColumn(tableViewer, SWT.LEFT);
		fileColumn.getColumn().setText(FILE_COLUMN_NAME);
		fileColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn dateColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		dateColumn.getColumn().setText(DATE_COLUMN_NAME);
		dateColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn velocityColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		velocityColumn.getColumn().setText(VELOCITY_COLUMN_NAME);
		velocityColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn headingColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		headingColumn.getColumn().setText(HEADING_COLUMN_NAME);
		headingColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn rollColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		rollColumn.getColumn().setText(ROLL_COLUMN_NAME);
		rollColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn pitchColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		pitchColumn.getColumn().setText(PITCH_COLUMN_NAME);
		pitchColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn heaveColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		heaveColumn.getColumn().setText(VERTICAL_OFFSET_COLUMN_NAME);
		heaveColumn.setLabelProvider(new BiasProvider());

		TableViewerColumn mruHeadingColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
		mruHeadingColumn.getColumn().setText(MRU_HEADING_COLUMN_NAME);
		mruHeadingColumn.setLabelProvider(new BiasProvider());

		if (timeColumnVisible) {
			TableViewerColumn attitudeTimeDelayColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
			attitudeTimeDelayColumn.getColumn().setText(ATTITUDE_TIMEDELAY_COLUMN_NAME);
			attitudeTimeDelayColumn.setLabelProvider(new BiasProvider());
			TableViewerColumn verticalOffsetTimeDelayColumn = new TableViewerColumn(tableViewer, SWT.CENTER);
			verticalOffsetTimeDelayColumn.getColumn().setText(VERTICAL_OFFSET_TIMEDELAY_COLUMN_NAME);
			verticalOffsetTimeDelayColumn.setLabelProvider(new BiasProvider());
		}

		parent.setRedraw(true);
		parent.layout(true);

		return tableViewer;
	}

	/**
	 * Updates the table content.
	 */
	public void updateTable(TableViewer tableViewer, CorrectionList correctionList) {
		if (tableViewer != null && !tableViewer.getTable().isDisposed()) {
			tableViewer.setContentProvider(ArrayContentProvider.getInstance());
			tableViewer.setInput(correctionList.getCorrectionPointList());

			this.tableComposite.setRedraw(false);
			for (TableColumn tableColumn : tableViewer.getTable().getColumns()) {
				tableColumn.pack();
			}
			this.tableComposite.setRedraw(true);

			if (tableViewer.getTable().getItems().length == 1) {
				this.warningLabel.setVisible(true);
			} else {
				this.warningLabel.setVisible(false);
			}
		}

		getShell().pack();
	}

	/**
	 * Create the table editor.
	 */
	protected void createTableEditor(TableViewer tableViewer, CorrectionList correctionList) {
		// Table Editor
		final TableEditor editor = new TableEditor(tableViewer.getTable());
		editor.horizontalAlignment = SWT.LEFT;
		editor.grabHorizontal = true;

		tableViewer.getTable().addListener(SWT.MouseDown, new Listener() {
			@Override
			public void handleEvent(Event event) {

				Point point = new Point(event.x, event.y);
				TableItem tableItem = tableViewer.getTable().getItem(point);

				if (tableItem != null) {
					for (int columnIndex = 0; columnIndex < tableViewer.getTable().getColumnCount(); columnIndex++) {
						Rectangle rectangle = tableItem.getBounds(columnIndex);
						if (rectangle.contains(point)) {
							if (columnIndex < REMOVE_LINE_INDEX) {
								Text text = new Text(tableViewer.getTable(), SWT.NONE);
								text.addListener(SWT.FocusOut,
										new EditorListener(tableItem, text, columnIndex,
												correctionList.getCorrectionPointList()
														.get(tableViewer.getTable().getSelectionIndex())));
								text.addListener(SWT.Traverse,
										new EditorListener(tableItem, text, columnIndex,
												correctionList.getCorrectionPointList()
														.get(tableViewer.getTable().getSelectionIndex())));
								editor.setEditor(text, tableItem, columnIndex);
								text.setText(tableItem.getText(columnIndex));
								text.selectAll();
								text.setFocus();
								return;
							} else {
								Text text = new Text(tableViewer.getTable(), SWT.NONE);
								text.addListener(SWT.FocusOut,
										new EditorListener(tableItem, text, columnIndex,
												correctionList.getCorrectionPointList()
														.get(tableViewer.getTable().getSelectionIndex())));
								editor.setEditor(text, tableItem, columnIndex);

							}
						}
					}
				}
			}
		});
	}

	/**
	 * Check if value is correct (long or double).
	 */
	public void checkValidityValue(TableItem tableItem, Text text, int columnIndex, CorrectionPoint correctionPoint) {
		try {
			if (columnIndex == DATE_COLUMN_INDEX) {
				DateUtils.parseDate(text.getText());
			} else {
				Double.parseDouble(text.getText());
			}
			tableItem.setText(columnIndex, text.getText());
			setCorrectionPoint(correctionPoint, tableItem);
			text.dispose();
		} catch (NumberFormatException e) {
			text.dispose();
		}
	}

	/**
	 * Set the given correction point with TableItem data *
	 * 
	 * @param correctionPoint data to applied in item
	 * @param item item to modified
	 */
	protected void setCorrectionPoint(CorrectionPoint correctionPoint, TableItem item) {
		correctionPoint.setDate(DateUtils.parseDate(item.getText(DATE_COLUMN_INDEX)).toInstant()); // date
		correctionPoint.setVelocity(Double.parseDouble(item.getText(VELOCITY_COLUMN_INDEX))); // bias
		correctionPoint.setRoll(Double.parseDouble(item.getText(ROLL_COLUMN_INDEX))); // roll
		correctionPoint.setPitch(Double.parseDouble(item.getText(PITCH_COLUMN_INDEX))); // pitch
		correctionPoint.setPlatformVerticalOffset(Double.parseDouble(item.getText(VERTICAL_OFFSET_COLUMN_INDEX))); // heave
		correctionPoint.setHeading(Double.parseDouble(item.getText(HEADING_COLUMN_INDEX))); // heading
		correctionPoint.setMruHeading(Double.parseDouble(item.getText(MRU_HEADING_COLUMN_INDEX))); // MRU heading
		correctionPoint.setAttitudeTimeMs(Double.parseDouble(item.getText(ATTITUDE_TIME_COLUMN_INDEX))); // time
		correctionPoint.setVerticalOffsetTimeMs(Double.parseDouble(item.getText(VERTICAL_OFFSET_TIME_COLUMN_INDEX))); // time
	}

	/**
	 * Listener for the table editor feature.
	 */
	private class EditorListener implements Listener {

		private TableItem tableItem;
		private Text text;
		private int columnIndex;
		private CorrectionPoint correctionPoint;

		public EditorListener(TableItem tableItem, Text text, int columnIndex, CorrectionPoint correctionPoint) {
			super();
			this.tableItem = tableItem;
			this.text = text;
			this.columnIndex = columnIndex;
			this.correctionPoint = correctionPoint;
		}

		@Override
		public void handleEvent(final Event e) {

			switch (e.type) {
			case SWT.FocusOut:
				checkValidityValue(this.tableItem, this.text, this.columnIndex, this.correctionPoint);
				break;
			case SWT.Traverse:
				switch (e.detail) {
				case SWT.TRAVERSE_RETURN:
					checkValidityValue(this.tableItem, this.text, this.columnIndex, this.correctionPoint);
					e.doit = false;
					break;
				case SWT.TRAVERSE_ESCAPE:
					this.text.dispose();
					e.doit = false;
					break;
				default:
				}
				break;
			default:
			}
		}
	}
}
