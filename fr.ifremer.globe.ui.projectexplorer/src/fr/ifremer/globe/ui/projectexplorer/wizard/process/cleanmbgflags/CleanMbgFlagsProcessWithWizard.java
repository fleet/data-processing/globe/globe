package fr.ifremer.globe.ui.projectexplorer.wizard.process.cleanmbgflags;

import java.util.List;
import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.io.mbg.info.MbgInfo;
import fr.ifremer.globe.core.io.mbg.service.MbgFlagCleaner;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.IDataContainerInfoService;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.ui.wizard.FilesToProcessEntry;
import fr.ifremer.globe.utils.exception.GIOException;

@SuppressWarnings("restriction")
public class CleanMbgFlagsProcessWithWizard extends DefaultProcessWithWizard<ConvertWizardDefaultModel> {

	private static final String TITLE = "Clean MBG correction flags by replacing invalid values by 0";

	private final MbgFlagCleaner mbgFlagCleaner = new MbgFlagCleaner();

	/**
	 * Constructor
	 */
	public CleanMbgFlagsProcessWithWizard(Shell shell) {
		super(shell, TITLE, ConvertWizardDefaultModel::new, CleanMbgFlagsWizard::new);
	}

	/**
	 * Process method executed after a click on finish.
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		// Get file list (input/output/temp file tuples)
		List<FilesToProcessEntry> filesToProcess = getFilesToProcessWithTemp(logger);
		IDataContainerInfoService infoService = IDataContainerInfoService.grab();

		monitor.beginTask("Clean MBG flags", filesToProcess.size());

		int processedFileCount = 0;
		int errorFileCount = 0;
		for (FilesToProcessEntry entry : filesToProcess) {
			logger.info("Clean flags of {} ... ", entry.getInputFile().getPath());
			Optional<ISounderNcInfo> info = infoService.getSounderNcInfo(entry.getTempFile().getAbsolutePath());
			if (info.isPresent() && (info.get() instanceof MbgInfo)) {
				try {
					MbgInfo mbg = (MbgInfo) info.get();
					// Process file
					mbgFlagCleaner.cleanFlags(mbg);
					// write output file
					writeOutputFileFromTemp(entry);
					processedFileCount++;
				} catch (Exception e) {
					logger.error("Error while processing file {}: {}", entry.getInputFile().getPath(), e.getMessage(),
							e);
					errorFileCount++;
				}
			}

			if (monitor.isCanceled())
				return Status.CANCEL_STATUS;
			monitor.worked(1);
		}
		// load output files if asked
		loadOutputFiles(filesToProcess, monitor);

		String s = processedFileCount > 1 ? "s" : "";
		logger.info("End of process on {} file{}", processedFileCount, s);
		if (errorFileCount > 0) {
			String sError = errorFileCount > 1 ? "s" : "";
			logger.error("Error while processing {} file{}", errorFileCount, sError);
		}

		return monitor.isCanceled() ? Status.CANCEL_STATUS : Status.OK_STATUS;
	}
}
