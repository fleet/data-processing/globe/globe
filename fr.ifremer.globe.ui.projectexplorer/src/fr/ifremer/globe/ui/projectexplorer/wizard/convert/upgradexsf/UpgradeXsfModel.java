/**
 * Globe - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.wizard.convert.upgradexsf;

import java.io.File;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.databinding.observable.WritableObjectList;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;
import fr.ifremer.globe.ui.wizard.SelectInputFilesPage.SelectInputFilesPageModel;
import fr.ifremer.globe.ui.wizard.model.GenericSelectInputFilesPageModel;

/**
 * Model of the "Upgrade XSF" wizard
 */
public class UpgradeXsfModel extends ConvertWizardDefaultModel {

	/** Raw files to convert */
	private final WritableObjectList<File> rawFiles = new WritableObjectList<>();
	private final SelectInputFilesPageModel rawFilesModel = new GenericSelectInputFilesPageModel(rawFiles, true);

	/**
	 * Constructor
	 */
	public UpgradeXsfModel() {
		// input file extensions
		inputFilesFilterExtensions.clear(); // remove mbg/xsf extensions (from the mother class ConvertToNviWizardModel)
		inputFilesFilterExtensions.addAll(IFileService.grab().getFileFilters(ContentType.XSF_NETCDF_4));

		// Configure raw files filtering
		rawFilesModel.getInputFilesFilterExtensions()
				.addAll(IFileService.grab().getFileFilters(ContentType.SOUNDER_ALL));
		rawFilesModel.getInputFilesFilterExtensions().add(new Pair<>("Kongsberg (*.kmall)", "*.kmall"));
		rawFilesModel.getInputFilesFilterExtensions()
				.addAll(IFileService.grab().getFileFilters(ContentType.SOUNDER_S7K));
	}

	/**
	 * @return the {@link #rawFilesModel}
	 */
	public SelectInputFilesPageModel getRawFilesModel() {
		return rawFilesModel;
	}
}