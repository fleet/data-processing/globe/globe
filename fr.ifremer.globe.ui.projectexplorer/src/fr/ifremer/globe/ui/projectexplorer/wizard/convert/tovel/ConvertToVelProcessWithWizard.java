package fr.ifremer.globe.ui.projectexplorer.wizard.convert.tovel;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.model.velocity.ISoundVelocityData;
import fr.ifremer.globe.core.model.velocity.IVelWriter;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Sound velocity extraction process with wizard
 */
public class ConvertToVelProcessWithWizard
		extends DefaultProcessWithWizard<ConvertToVelWizardModel> {

	private Logger logger;

	/**
	 * Constructor
	 */
	public ConvertToVelProcessWithWizard(Shell shell) {
		super(shell, "Sound velocity extraction", ConvertToVelWizardModel::new,
				ConvertToVelWizard::new);
	}

	/**
	 * Performs processing.
	 */
	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GIOException {
		this.logger = logger;
		List<File> inputFiles = model.getInputFiles().asList();
		logger.info("Beginning conversion of {} files", inputFiles.size());

		SubMonitor subMonitor = SubMonitor.convert(monitor, inputFiles.size());
		for (File inputFile : model.getInputFiles().asList()) {
			processExtraction(inputFile, subMonitor.split(1));
		}

		logger.info("Processed {} files", inputFiles.size());
		return Status.OK_STATUS;

	}

	/**
	 * Processes the extraction for one file.
	 */
	protected void processExtraction(File inputFile, IProgressMonitor monitor) {
		String inputFilePath = inputFile.getPath();
		monitor.setTaskName("Processing : " + inputFilePath);
		logger.info("Processing : {}", inputFilePath);

		File outputFile = outputFileNameComputer.compute(inputFile, model,
				ConvertToVelWizardModel.VEL_EXTENSION);
		if (shouldBeProcessed(outputFile, logger)) {
			try {
				// read selected file
				monitor.subTask("Loading data ...");
				ISoundVelocityData src = getSoundVelocityDataProxy(inputFile);
				IVelWriter.grab().write(src, outputFile.getAbsolutePath());

				logger.info("Conversion of {} to {} done", inputFile.getName(), outputFile.getName());
				loadOutputFile(outputFile, monitor);
				monitor.worked(1);

				logger.info("Sound velocity extraction from {} to {} done", inputFile.getName(), outputFile.getName());
			} catch (Exception e) {
				logger.error("Error while sound velocity extraction from {} to {} ({})", inputFile.getName(),
						outputFile.getName(), e.getMessage());
				FileUtils.deleteQuietly(outputFile);
			}
		}
	}

}
