package fr.ifremer.globe.ui.projectexplorer.wizard.convert.usblreport;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.ui.wizard.ConvertWizardDefaultModel;

/**
 * Model of the wizard page
 */
public class SaveUsblReportWizardModel extends ConvertWizardDefaultModel {
	public static final String USBL_REPORT_EXTENSION = "json";

	/**
	 * Constructor
	 */
	public SaveUsblReportWizardModel() {
		IFileService fileService = IFileService.grab();
		inputFilesFilterExtensions.addAll(fileService.getFileFilters(ContentType.USBL));
		outputFormatExtensions.add(USBL_REPORT_EXTENSION);
	}
}