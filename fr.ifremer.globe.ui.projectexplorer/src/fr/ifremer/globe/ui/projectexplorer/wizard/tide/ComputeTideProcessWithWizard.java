package fr.ifremer.globe.ui.projectexplorer.wizard.tide;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;

import fr.ifremer.globe.core.io.tide.ttb.TtbFileWriter;
import fr.ifremer.globe.core.model.tide.ITideService;
import fr.ifremer.globe.core.model.tide.TideGauge;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.wizard.DefaultProcessWithWizard;
import fr.ifremer.globe.utils.exception.GException;

/**
 * Process and wizard for triangular filtering
 */
public class ComputeTideProcessWithWizard extends DefaultProcessWithWizard<ComputeTideWizardModel> {

	public ComputeTideProcessWithWizard(Shell shell) {
		super(shell, "Tide computing", ComputeTideProcessWithWizard::makeModel, ComputeTideWizard::new);
		wizardDialog.setModal(false);
	}

	@Override
	public IStatus apply(IProgressMonitor monitor, Logger logger) throws GException {
		File outputFilePath = model.getOutputFiles().asList().get(0);
		TtbFileWriter.write(outputFilePath.getAbsolutePath(), model.getTideData());
		loadOutputFile(outputFilePath, monitor);
		logger.info("Write tide file (.ttb) {} with {} data.", outputFilePath, model.getTideData().size());
		Messages.openInfoMessage("Compute tide", "Tide saved in : " + outputFilePath);
		return Status.OK_STATUS;
	}

	/**
	 * Creates the model and launchs a process to load the Tide Gauges out of the UI Thread
	 */
	private static ComputeTideWizardModel makeModel() {
		ComputeTideWizardModel result = new ComputeTideWizardModel();
		IProcessService.grab().runInBackground("Loading tide gauges",
				IProcessFunction.with(() -> loadTigeGauges(result)));
		return result;
	}

	/** Invokes the tide service to get the tide gauges . Then updates the model */
	private static void loadTigeGauges(ComputeTideWizardModel model) {
		try {
			List<TideGauge> tideGauges = ITideService.grab().getTideGauges();
			if (tideGauges != null && !tideGauges.isEmpty()) {
				model.getTideGauges().getRealm().asyncExec(() -> model.getTideGauges().addAll(tideGauges));
			} else {
				Messages.openErrorMessage("Error", "No Tide gauge retrieved.");
			}
		} catch (IOException e1) {
			Messages.openErrorMessage("Error", "Tide gauge service not available. (" + e1.getMessage() + ")", e1);
		}
	}
}
