package fr.ifremer.globe.ui.projectexplorer.wizard.tide;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import fr.ifremer.globe.ui.wizard.SelectInputFilesPage;
import fr.ifremer.globe.ui.wizard.SelectOutputParametersPage;

public class ComputeTideWizard extends Wizard {

	private ComputeTideWizardModel model;
	protected SelectInputFilesPage selectInputFilesPage;
	protected ComputeTideWizardPage computeTideWizardPage;
	protected SelectOutputParametersPage selectOutputParametersPage;

	public ComputeTideWizard(ComputeTideWizardModel model) {
		super();
		this.model = model;
		this.setHelpAvailable(true);
	}

	/** adding pages to the wizard */
	@Override
	public void addPages() {
		selectInputFilesPage = new SelectInputFilesPage(model);
		addPage(selectInputFilesPage);
		computeTideWizardPage = new ComputeTideWizardPage(model);
		addPage(computeTideWizardPage);
		selectOutputParametersPage = new SelectOutputParametersPage(model);
		addPage(selectOutputParametersPage);
	}

	@Override
	public boolean performFinish() {
		return true;
	}


	@Override
	public IWizardPage getStartingPage() {
		return computeTideWizardPage;
	}

}
