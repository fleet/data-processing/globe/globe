package fr.ifremer.globe.ui.projectexplorer.ui.dialogs;

import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.utils.mbes.Ellipsoid;

/**
 * composite to input mbg global parameters
 */
public class MbgGlobalParametersComposite extends Composite {

	protected Combo ellipsDropDown;
	protected Text shipNameText;
	protected Text surveyNameText;
	protected Text referencePointText;
	protected Text CDIText;
	protected Button NVIOption;
	protected Text xOffsetText;
	protected Text zOffsetText;
	protected Text yOffsetText;
	protected boolean exportToNVI;

	/**
	 * Constructor
	 */
	public MbgGlobalParametersComposite(boolean minimumParameters, Composite parent, int style) {
		super(parent, style);
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;

		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		// Ellipsoide
		Label ellipsLabel = new Label(this, SWT.NONE);
		ellipsLabel.setText("Ellipsoid :");
		ellipsDropDown = new Combo(this, SWT.DROP_DOWN | SWT.BORDER | SWT.READ_ONLY);
		ellipsDropDown.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		for (Ellipsoid e : Ellipsoid.values()) {
			ellipsDropDown.add(e.getEllipsName());
		}
		int index = Arrays.binarySearch(Ellipsoid.values(), Ellipsoid.WGS84);
		if (index < 0) {
			index = 0;
		}
		ellipsDropDown.select(index);

		Label shipNameLabel = new Label(this, SWT.NONE);
		shipNameLabel.setText("Ship name :");

		shipNameText = new Text(this, SWT.BORDER);
		shipNameText.setText("");
		shipNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label surveyNameLabel = new Label(this, SWT.NONE);
		surveyNameLabel.setText("Survey name :");

		surveyNameText = new Text(this, SWT.BORDER);
		surveyNameText.setText("");
		surveyNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label referencePointLabel = new Label(this, SWT.NONE);
		referencePointLabel.setText("Reference point description :");

		referencePointText = new Text(this, SWT.BORDER);
		referencePointText.setText("");
		referencePointText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label CDILabel = new Label(this, SWT.NONE);
		CDILabel.setText("CDI (Common Data Index) :");

		CDIText = new Text(this, SWT.BORDER);
		CDIText.setText("");
		CDIText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		NVIOption = new Button(this, SWT.CHECK);
		NVIOption.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		NVIOption.setText("Export navigation to a new NVI file.");
		NVIOption.setSelection(false);
		new Label(this, SWT.NONE);
		NVIOption.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				exportToNVI = NVIOption.getSelection();
			}
		});

		if (!minimumParameters) {
			// Latitude text widget
			Label sounderPosition = new Label(this, SWT.WRAP);
			sounderPosition.setText("Sounder position (relatively to navigation, in meters) :");
			GridData gd_sounderPosition = new GridData(GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL
					| GridData.HORIZONTAL_ALIGN_FILL | GridData.VERTICAL_ALIGN_CENTER);
			gd_sounderPosition.horizontalSpan = 2;
			gd_sounderPosition.widthHint = 424;
			sounderPosition.setLayoutData(gd_sounderPosition);
			sounderPosition.setFont(parent.getFont());

			Label xOffsetLabel = new Label(this, SWT.NONE);
			xOffsetLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			xOffsetLabel.setText("x :");
			xOffsetText = new Text(this, SWT.SINGLE | SWT.BORDER);
			xOffsetText.setText("0.0");
			xOffsetText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));

			Label yOffsetLabel = new Label(this, SWT.NONE);
			yOffsetLabel.setText("y :");
			yOffsetLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			yOffsetText = new Text(this, SWT.SINGLE | SWT.BORDER);
			yOffsetText.setText("0.0");
			yOffsetText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));

			Label zOffsetLabel = new Label(this, SWT.NONE);
			zOffsetLabel.setText("z :");
			zOffsetLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			zOffsetText = new Text(this, SWT.SINGLE | SWT.BORDER);
			zOffsetText.setText("0.0");
			zOffsetText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		}
	}

	/**
	 * @return the {@link #ellipsDropDown}
	 */
	public Combo getEllipsDropDown() {
		return ellipsDropDown;
	}

	/**
	 * @param ellipsDropDown
	 *            the {@link #ellipsDropDown} to set
	 */
	public void setEllipsDropDown(Combo ellipsDropDown) {
		this.ellipsDropDown = ellipsDropDown;
	}

	/**
	 * @return the {@link #shipNameText}
	 */
	public Text getShipNameText() {
		return shipNameText;
	}

	/**
	 * @param shipNameText
	 *            the {@link #shipNameText} to set
	 */
	public void setShipNameText(Text shipNameText) {
		this.shipNameText = shipNameText;
	}

	/**
	 * @return the {@link #surveyNameText}
	 */
	public Text getSurveyNameText() {
		return surveyNameText;
	}

	/**
	 * @param surveyNameText
	 *            the {@link #surveyNameText} to set
	 */
	public void setSurveyNameText(Text surveyNameText) {
		this.surveyNameText = surveyNameText;
	}

	/**
	 * @return the {@link #referencePointText}
	 */
	public Text getReferencePointText() {
		return referencePointText;
	}

	/**
	 * @param referencePointText
	 *            the {@link #referencePointText} to set
	 */
	public void setReferencePointText(Text referencePointText) {
		this.referencePointText = referencePointText;
	}

	/**
	 * @return the {@link #cDIText}
	 */
	public Text getCDIText() {
		return CDIText;
	}

	/**
	 * @param cDIText
	 *            the {@link #cDIText} to set
	 */
	public void setCDIText(Text cDIText) {
		CDIText = cDIText;
	}
	
	/**
	 * @return the {@link #NVIOption}
	 */
	public Button getNVIOption() {
		return NVIOption;
	}
	/**
	 * @param boolProperty
	 *            the {@link #NVIOption} to set
	 */	
	public void setNVIOption(boolean navOption) {
		NVIOption.setSelection(navOption);
		exportToNVI = navOption;
		
	}

	/**
	 * @return the {@link #exportToNVI}
	 */
	public boolean isExportToNVI() {
		return exportToNVI;
	}
	
	/**
	 * @return the {@link #xOffsetText}
	 */
	public Text getxOffsetText() {
		return xOffsetText;
	}

	/**
	 * @param xOffsetText
	 *            the {@link #xOffsetText} to set
	 */
	public void setxOffsetText(Text xOffsetText) {
		this.xOffsetText = xOffsetText;
	}

	/**
	 * @return the {@link #zOffsetText}
	 */
	public Text getzOffsetText() {
		return zOffsetText;
	}

	/**
	 * @param zOffsetText
	 *            the {@link #zOffsetText} to set
	 */
	public void setzOffsetText(Text zOffsetText) {
		this.zOffsetText = zOffsetText;
	}

	/**
	 * @return the {@link #yOffsetText}
	 */
	public Text getyOffsetText() {
		return yOffsetText;
	}

	/**
	 * @param yOffsetText
	 *            the {@link #yOffsetText} to set
	 */
	public void setyOffsetText(Text yOffsetText) {
		this.yOffsetText = yOffsetText;
	}


}
