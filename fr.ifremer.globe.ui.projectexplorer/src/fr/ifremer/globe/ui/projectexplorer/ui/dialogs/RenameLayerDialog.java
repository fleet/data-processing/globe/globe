package fr.ifremer.globe.ui.projectexplorer.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.GridLayout;

public class RenameLayerDialog extends Dialog {

	private String name;
	private String originalName;
	private Text renameTxt;

	public RenameLayerDialog(Shell parentShell, String originalName) {
		super(parentShell);
		this.originalName = originalName;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Rename group");
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout gridLayout = (GridLayout) container.getLayout();
		gridLayout.numColumns = 2;
		
				Label layerName = new Label(container, SWT.NONE);
				layerName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
				layerName.setText("Group name : ");

		renameTxt = new Text(container, SWT.BORDER);
		renameTxt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		renameTxt.setText(originalName);

		return container;
	}

	@Override
	protected void okPressed() {
		this.name = renameTxt.getText();
		super.okPressed();
	}

	public String getNewName() {
		return this.name;
	}
}
