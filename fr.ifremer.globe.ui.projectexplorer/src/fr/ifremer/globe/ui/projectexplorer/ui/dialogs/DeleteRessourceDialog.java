package fr.ifremer.globe.ui.projectexplorer.ui.dialogs;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.ResourceManager;

import fr.ifremer.globe.core.model.file.IFileInfo;

public class DeleteRessourceDialog extends Dialog {

	private final List<IFileInfo> fileInfos;

	private boolean deleteFromDisk;

	public DeleteRessourceDialog(Shell parentShell, List<IFileInfo> fileInfos) {
		super(parentShell);
		setShellStyle(SWT.CLOSE | SWT.RESIZE);
		this.fileInfos = fileInfos;
	}

	@Override
	protected void configureShell(Shell shell) {
		shell.setMinimumSize(new Point(530, 300));
		shell.setImage(
				ResourceManager.getPluginImage("fr.ifremer.globe.ui.projectexplorer", "icons/16/edit-delete.png"));
		super.configureShell(shell);
		shell.setText("Delete files");
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Creates dialog.
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		composite.setLayout(new GridLayout(1, false));

		Composite questionComposite = new Composite(composite, SWT.NONE);
		questionComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		questionComposite.setLayout(new GridLayout(2, false));

		Label questionImage = new Label(questionComposite, SWT.NONE);
		questionImage.setImage(Display.getDefault().getSystemImage(SWT.ICON_QUESTION));

		Label questionLabel = new Label(questionComposite, SWT.NONE);
		if (fileInfos.size() > 1) {
			questionLabel.setText(
					"Remove these " + Integer.toString(fileInfos.size()) + " files from the project explorer ?");
		} else {
			questionLabel.setText("Remove '" + fileInfos.get(0).getFilename() + "' from the project explorer ?");
		}

		Composite optionComposite = new Composite(composite, SWT.NONE);
		optionComposite.setLayout(new GridLayout(1, false));
		optionComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));

		var deleteFromDiskButton = new Button(optionComposite, SWT.CHECK);
		deleteFromDiskButton.setText("Delete from disk (cannot be undone)");
		deleteFromDiskButton.addListener(SWT.Selection, e -> deleteFromDisk = deleteFromDiskButton.getSelection());

		Composite locationComposite = new Composite(composite, SWT.NONE);
		locationComposite.setLayout(new GridLayout(1, false));
		locationComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		Label locationLabel = new Label(locationComposite, SWT.NONE);
		locationLabel.setText("File location" + (fileInfos.size() > 1 ? "s" : "") + "  :");

		Text text = new Text(locationComposite, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		GridData gridDataText = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gridDataText.heightHint = 80;
		text.setLayoutData(gridDataText);
		text.setEditable(false);
		text.setText(
				fileInfos.stream().map(IFileInfo::getAbsolutePath).collect(Collectors.joining(System.lineSeparator())));

		return parent;
	}

	public boolean deleteFromDisk() {
		return deleteFromDisk;
	}
}
