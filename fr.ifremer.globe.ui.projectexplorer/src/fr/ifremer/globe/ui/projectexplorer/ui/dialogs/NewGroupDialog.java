package fr.ifremer.globe.ui.projectexplorer.ui.dialogs;

import java.util.Optional;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.utils.image.ImageResources;

public class NewGroupDialog extends TitleAreaDialog {

	protected Logger logger = LoggerFactory.getLogger(NewGroupDialog.class);

	/** Inner widgets **/
	private String project;
	private String comments;

	public NewGroupDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		getShell().setText("New Group");
		setTitle("New Group");
		setMessage("Creates a new group");
		Optional.ofNullable(ImageResources.getImage("/icons/16/newprj_wiz.png", getClass()))
				.ifPresent(this::setTitleImage);

		var composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);
		composite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label projectLabel = new Label(composite, SWT.NONE);
		projectLabel.setText("Name :");
		var projectText = new Text(composite, SWT.BORDER);
		projectText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		projectText.addModifyListener(event -> setProjectName(projectText.getText()));

		Label commentsLabel = new Label(composite, SWT.NONE);
		commentsLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
		commentsLabel.setText("Comments :");
		var commentsText = new Text(composite, SWT.BORDER);
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		data.heightHint = 50;
		commentsText.setLayoutData(data);
		commentsText.addModifyListener(event -> comments = commentsText.getText());

		return area;
	}

	@Override
	protected Control createButtonBar(Composite parent) {
		Control c = super.createButtonBar(parent);
		getButton(OK).setEnabled(false);
		return c;
	}

	private void setProjectName(String projectName) {
		this.project = projectName;
		setErrorMessage(projectName.isEmpty() ? "Name must be specified" : null);
	}

	@Override
	public void setErrorMessage(String newErrorMessage) {
		super.setErrorMessage(newErrorMessage);
		getButton(OK).setEnabled(newErrorMessage == null || newErrorMessage.isEmpty());
	}

	public String getProjectName() {
		return project;
	}

	public String getComments() {
		return comments;
	}

}