package fr.ifremer.globe.ui.projectexplorer.converter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.primitives.IntList;

import fr.ifremer.globe.core.model.file.IxyzFile;


public class CsvFile implements IxyzFile {

	public static final String LATITUDE_HEADER = "Latitude";
	public static final String LONGITUDE_HEADER = "Longitude";
	public static final String DEPTH_HEADER = "Depth";
	public static final String VALUE_HEADER = "Value";
	private String fileName;
	private char delimiter;

	private int resolution;

	/**
	 * @return the resolution
	 */
	public int getResolution() {
		return resolution;
	}

	/**
	 * @param resolution the resolution to set
	 */
	public void setResolution(int resolution) {
		this.resolution = resolution;
	}

	/**
	 * indicates if depths are positive below surface (true) or negative below surface (false)
	 */
	private boolean isDepthPositiveBelowSurface = true;
	private Map<String, Integer> headerIndex;

	public CsvFile(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String getPath() {
		return fileName;
	}

	@Override
	public void setDelimiter(char delimiter) {
		this.delimiter = delimiter;
	}

	@Override
	public void setHeaderIndex(Map<String, Integer> headerIndex) {
		this.headerIndex = headerIndex;
	}

	@Override
	public char getDelimiter() {
		return delimiter;
	}

	@Override
	public Map<String, Integer> getHeaderIndex() {
		return headerIndex;
	}

	@Override
	public boolean isDepthPositiveBelowSurface() {
		return isDepthPositiveBelowSurface;
	}

	@Override
	public void setDepthPositiveBelowSurface(boolean value) {
		isDepthPositiveBelowSurface = value;
	}

	@Override
	public List<String> getMandatoryHeader() {
		String[] list = { LATITUDE_HEADER, LONGITUDE_HEADER, DEPTH_HEADER, VALUE_HEADER };

		return Arrays.asList(list);
	}

	@Override
	public void setDecimalPointLabel(char s) {
	}

	@Override
	public char getDecimalPoint() {
		return '.';
	}

	@Override
	public void setStartingLine(int value) {
	}

	@Override
	public int getStartingLine() {
		return 0;
	}

	@Override
	public Class<?> getType(String headerName) {
		return Double.class;
	}

	@Override
	public void setType(String headerName, Class<?> type) {
		// Never append
	}

	/** {@inheritDoc} */
	@Override
	public boolean allowsMultipleValues() {
		return false; // Not supported
	}

	/** {@inheritDoc} */
	@Override
	public void setValueIndexes(IntList indexes) {
		// Not supported
	}

}
