package fr.ifremer.globe.ui.projectexplorer.converter;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IxyzFile;
import fr.ifremer.globe.ui.wizard.CsvComposite;

/**
 * Wizard page to define parameters for converting a CSV file<br>
 * Show the pre-parsed first 10 lines in a table
 *
 *
 * @author Apside
 */
public class CsvConverterPage extends WizardPage {

	protected final Logger logger = LoggerFactory.getLogger(CsvConverterPage.class);

	private CsvComposite csvComposite;
	private Composite rootComposite;
	private Group delimiterGroup;
	private Button[] delimitersButton;
	private Text otherDelimiter;

	private SelectionListener delimiterButtonListener;
	private int resolution = 10;
	private String[] predefinedDelimiterLabels = { "Tab", "Semi-column \";\"", "Space", "Comma \",\"", "Other" };
	private char[] predefinedDelimiters = { '\t', ';', ' ', ',', ' ' };
	private ModifyListener otherDelimiterListener;

	private IxyzFile csvFile;

	private boolean askResolution;

	public void updateStatus() {
		setPageComplete(csvComposite.isDelimiterValid() && csvComposite.isHeaderValid());
	}

	/**
	 * Constructor<br>
	 * sets name, title, description, CsvFile for which the wizard page is intended<br>
	 *
	 * @param pageName page name
	 * @param title title of the page
	 * @param description description of the page
	 * @param csvFile csvFile to convert
	 */
	public CsvConverterPage(String pageName, IxyzFile csvFile, boolean askResolution) {
		super(pageName);
		setTitle("Converting CSV file: \n" + csvFile.getPath());
		setDescription("Please select delimiter and header for datas");
		// this.setPageComplete(false);

		this.askResolution = askResolution;
		this.csvFile = csvFile;

		delimitersButton = new Button[6];
		delimiterButtonListener = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				for (int i = 0; i < predefinedDelimiterLabels.length; i++) {

					if (predefinedDelimiterLabels[i].equals(((Button) e.getSource()).getText()) == false) {
						// set all others check buttons to "not selected"
						delimitersButton[i].setSelection(false);

					} else {
						delimitersButton[i].setSelection(true);

						csvComposite.setDelimiter(predefinedDelimiters[i]);
						// if "Other" is selected, set the corresponding text
						// box to enable
						if (predefinedDelimiterLabels[i].equals("Other") && delimitersButton[i].getSelection()) {
							otherDelimiter.setEnabled(true);
							var oneDelimiter = otherDelimiter.getText();
							csvComposite.setDelimiter(oneDelimiter.isEmpty() ? ' ' : oneDelimiter.charAt(0));
						} else {
							otherDelimiter.setEnabled(false);
							csvComposite.setDelimiter(predefinedDelimiters[i]);
						}
					}
				}
				update();
			}

		};

		// listener called when the user change the other listener Text
		otherDelimiterListener = e -> {
			var oneDelimiter = otherDelimiter.getText();
			csvComposite.setDelimiter(oneDelimiter.isEmpty() ? ' ' : oneDelimiter.charAt(0));
			update();
		};
		setPageComplete(false);
	}

	/**
	 * Update the table with the first parsed fields of the file
	 */
	protected void update() {
		csvComposite.updateTable();
		// Update page completion and warning message
		csvComposite.updatePageCompletion();
	}

	@Override
	public void createControl(Composite parent) {

		rootComposite = new Composite(parent, SWT.NULL);
		GridLayout gl_container = new GridLayout(1, false);
		gl_container.verticalSpacing = 10;
		rootComposite.setLayout(gl_container);

		setControl(rootComposite);

		delimiterGroup = new Group(rootComposite, SWT.NONE);
		delimiterGroup.setText("Delimiters");
		delimiterGroup.setLayout(new GridLayout(2, false));
		delimiterGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		for (int i = 0; i < predefinedDelimiterLabels.length; i++) {
			delimitersButton[i] = new Button(delimiterGroup, SWT.CHECK);
			delimitersButton[i].setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
			delimitersButton[i].addSelectionListener(delimiterButtonListener);
			delimitersButton[i].setText(predefinedDelimiterLabels[i]);

			if (predefinedDelimiterLabels[i].equals("Other")) {
				otherDelimiter = new Text(delimiterGroup, SWT.BORDER);
				otherDelimiter.setEnabled(false);
				otherDelimiter.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
				otherDelimiter.addModifyListener(otherDelimiterListener);
			} else {
				Label l1 = new Label(delimiterGroup, SWT.NONE);
				l1.setText("");
				l1.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
			}
		}

		if (askResolution) {
			Group optionGroup = new Group(rootComposite, SWT.NONE);
			optionGroup.setText("Option");
			optionGroup.setLayout(new GridLayout(2, false));
			optionGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			// Composite resolutionComposite = new Composite(rootComposite, SWT.NONE);
			// resolutionComposite.setLayout(new GridLayout(2, true));

			// Add label
			Label resolutionLabel = new Label(optionGroup, SWT.BOTTOM);
			resolutionLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
			resolutionLabel.setText("Image resolution (in meters per pixel) : ");

			final Text resolutionTextField = new Text(optionGroup, SWT.BORDER);
			resolutionTextField.setEnabled(true);
			resolutionTextField.setText(Integer.toString(Math.abs(resolution)));
			resolutionTextField.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

			// listener called when the user change the other listener Text
			ModifyListener resolutionListener = e -> {
				if (askResolution) {
					boolean needRealign = false;

					try {
						if (resolutionTextField.getText().isEmpty()) {
							resolution = 0;
						} else {
							resolution = Integer.parseInt(resolutionTextField.getText());
						}

						if (resolution < 1 || resolution > 1000) {
							needRealign = true;
							resolution = Math.max(1, resolution);
							resolution = Math.min(1000, resolution);
						}
					} catch (Throwable t) {
						needRealign = true;
						// Do nothing on error. Juste replace bad value with old one
					}

					if (needRealign) {
						askResolution = false;
						resolutionTextField.setText(Integer.toString(Math.abs(resolution)));
						askResolution = true;
					}
				}
			};

			resolutionTextField.addModifyListener(resolutionListener);

		}

		csvComposite = new CsvComposite(rootComposite, csvFile, this::updateStatus, false);

		// Parametres par défaut
		delimitersButton[1].setSelection(true);
		csvComposite.setDelimiter(predefinedDelimiters[1]);
		csvComposite.updateTable();
		// update();
	}

	public int getResolution() {
		return resolution;
	}

	/**
	 * @return the csvComposite
	 */
	public CsvComposite getCsvComposite() {
		return csvComposite;
	}
}
