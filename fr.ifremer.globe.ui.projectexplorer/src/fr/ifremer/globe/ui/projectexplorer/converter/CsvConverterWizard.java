/**
 * 
 */
package fr.ifremer.globe.ui.projectexplorer.converter;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

/**
 * Wizard to define driver parameters for converting a CSV file.
 * <br>
 * Show the {@link fr.ifremer.globe.ui.projectexplorer.converter.CsvConverterPage} page <br>
 * 
 * 
 * @author Apside
 * 
 */
public class CsvConverterWizard extends Wizard {
	private CsvConverterPage page;

	private CsvFile file;
	private boolean askResolution;


	public CsvConverterWizard(CsvFile csvFile,  boolean askResolution) {
		this.file = csvFile;
		this.askResolution = askResolution;
	}


	@Override
	public boolean performFinish() {
		file.setDelimiter(page.getCsvComposite().getDelimiter());
		file.setHeaderIndex(page.getCsvComposite().getHeaderIndex());
		file.setDepthPositiveBelowSurface(true);
		file.setResolution( page.getResolution());


		return true;
	}

	@Override
	public void addPages() {

		page = new CsvConverterPage("Csv converter", file, askResolution);
		addPage(page);
		getShell().addListener(SWT.RESIZE, new Listener() {
			@Override
			public void handleEvent(Event e) {
				//getShell().pack();
			}
		});
	}


}
