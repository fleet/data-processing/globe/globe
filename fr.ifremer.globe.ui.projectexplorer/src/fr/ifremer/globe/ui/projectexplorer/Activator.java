package fr.ifremer.globe.ui.projectexplorer;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.ui.projectexplorer.wizard.organizedtm.OrganizeDTMsPage;
import fr.ifremer.globe.ui.projectexplorer.wizard.tide.ComputeTidePreferences;

public class Activator implements BundleActivator {

	private static BundleContext context;

	private static ComputeTidePreferences computeTidePreferences;

	@Override
	public void start(BundleContext context) throws Exception {
		Activator.context = context;
		OrganizeDTMsPage.initPreference();
		computeTidePreferences = new ComputeTidePreferences(
				PreferenceRegistry.getInstance().getProcessesSettingsNode());
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		Activator.context = null;
	}

	public static BundleContext getContext() {
		return context;
	}

	public static ComputeTidePreferences getComputeTidePreferences() {
		return computeTidePreferences;
	}

}
