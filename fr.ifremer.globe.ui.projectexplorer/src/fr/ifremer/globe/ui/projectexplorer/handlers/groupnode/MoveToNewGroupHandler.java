package fr.ifremer.globe.ui.projectexplorer.handlers.groupnode;

import java.util.List;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.projectexplorer.ui.dialogs.NewGroupDialog;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.utils.SelectionUtils;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;

public class MoveToNewGroupHandler extends AbstractNodeHandler {

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return !getSelectedNodes(selectionService).isEmpty();
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, ESelectionService selectionService,
			ProjectExplorerModel projectExplorerModel, ITreeNodeFactory treeNodeFactory) {
		var selectedNodes = getSelectedNodes(selectionService);
		var parentNode = selectedNodes.get(0).getParent();
		NewGroupDialog dialog = new NewGroupDialog(shell);
		if (parentNode.isPresent() && dialog.open() == Window.OK) {
			FolderNode newGroupNode = treeNodeFactory.createFolderNode(parentNode.get(), dialog.getProjectName(),
					dialog.getComments());
			projectExplorerModel.moveNodes(selectedNodes, newGroupNode, false);
		}
	}

	private List<TreeNode> getSelectedNodes(ESelectionService selectionService) {
		return SelectionUtils.getSelection(selectionService, TreeNode.class).stream()
				.filter(node -> node instanceof FolderNode || node instanceof FileInfoNode).toList();
	}
}
