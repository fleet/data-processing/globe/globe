package fr.ifremer.globe.ui.projectexplorer.handlers;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.projectexplorer.view.ProjectExplorer;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;

/**
 * Handler to import some files.
 */
public class LoadDataFileHandler {

	@Inject
	protected IFileLoadingService fileLoadingService;
	@Inject
	protected IEventBroker eventBroker;
	@Inject
	protected FileInfoModel fileInfoModel;
	@Inject
	private EPartService partService;
	@Inject
	private ITreeNodeFactory treeNodeFactory;

	/** Last used directory */
	protected String defaultDirectory = "";

	/** All accepted file descriptions */
	protected String[] names = null;
	/** All accepted file extensions */
	protected String[] extensions = null;

	/**
	 * Constructor called by E4
	 */
	public LoadDataFileHandler() {
		initAcceptedFiles();
	}

	/**
	 * @return true if import on the selected node is possible
	 */
	@CanExecute
	public boolean canExecute(@Named(IServiceConstants.ACTIVE_SELECTION) StructuredSelection structuredSelection,
			@Active MItem callerItem) {

		// "File" menu
		if (callerItem.getParent().getElementId().equals("fr.ifremer.globe.mainmenu.file"))
			return true;

		// Import only on a folder or DataGroupNode
		return structuredSelection.size() == 1 && structuredSelection.getFirstElement() instanceof FolderNode;
	}

	/**
	 * Run import...
	 */
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell,
			@Named(IServiceConstants.ACTIVE_SELECTION) StructuredSelection structuredSelection,
			@Named("fr.ifremer.3dviewer.gui.import.parameter.load") String loadParameter, MPart part,
			@Active MItem callerItem) {

		FileDialog dialog = new FileDialog(shell, SWT.MULTI);
		dialog.setFilterNames(names);
		dialog.setFilterExtensions(extensions);
		dialog.setFilterPath(defaultDirectory);
		if (dialog.open() != null) {
			defaultDirectory = dialog.getFilterPath();
			List<String> inputFilePaths = Arrays.stream(dialog.getFileNames())
					.map(name -> Paths.get(dialog.getFilterPath(), name).toString()).toList();

			Optional<GroupNode> optParentNode = (structuredSelection != null
					&& structuredSelection.getFirstElement() instanceof GroupNode groupNode) ? Optional.of(groupNode)
							: Optional.empty();

			// reference files
			try {
				Optional.ofNullable(partService.findPart(ProjectExplorer.PART_ID)).map(MPart::getObject)
						.map(ProjectExplorer.class::cast).ifPresent(
								projectExplorer -> treeNodeFactory.createFileInfoNode(inputFilePaths, optParentNode));
			} catch (IllegalStateException ex) {
				// part not available
			}
			// load files
			if (Boolean.parseBoolean(loadParameter))
				fileLoadingService.load(inputFilePaths);
		}
	}

	/**
	 * Initialize attributes
	 */
	protected void initAcceptedFiles() {
		Map<String, String> filters = new LinkedHashMap<>();
		filters.put("All known files", "");

		IFileService fileService = IFileService.grab();
		for (ContentType contentType : ContentType.values()) {
			if (contentType != ContentType.UNDEFINED) {
				for (Pair<String, String> e : fileService.getFileFilters(contentType)) {
					filters.put(e.getFirst(), e.getSecond());
				}
			}
		}

		filters.put("All known files", StringUtils.join(filters.values(), ";"));
		filters.put("All files", "*.*");

		names = filters.keySet().toArray(new String[filters.size()]);
		extensions = filters.values().toArray(new String[filters.size()]);
	}
}
