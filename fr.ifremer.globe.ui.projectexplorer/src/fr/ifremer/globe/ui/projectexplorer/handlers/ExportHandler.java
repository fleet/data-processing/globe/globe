package fr.ifremer.globe.ui.projectexplorer.handlers;

public class ExportHandler {

//	private Logger logger = LoggerFactory.getLogger(ExportHandler.class);
//
//	/**
//	 * File filter for s3d data files.
//	 */
//	public final static String[] FORMAT_NAMES = new String[] { "SonarScope data files" };
//
//	/** File extension for import/export files. */
//	public final static String[] FORMAT_EXTENSIONS = new String[] { "*.s3d" };
//
//	@CanExecute
//	public boolean canExecute(ESelectionService selectionService) {
//		boolean value = false;
//		Object obj = selectionService.getSelection();
//		if (obj instanceof StructuredSelection) {
//			Object[] elements = ((StructuredSelection) obj).toArray();
//			if (elements != null && elements.length == 1) {
//				if (elements[0] instanceof TreeNode) {
//					if (((TreeNode<?>) elements[0]).getModel() instanceof ILazyLoading) {
//						value = true;
//					}
//				}
//			}
//		}
//		return value;
//	}
//
//	@Execute
//	public void execute(ESelectionService selectionService, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
//
//		//BathyscopeModel bm = Viewer3D.getModel();
//
//		Object selection = selectionService.getSelection();
//		if (selection instanceof StructuredSelection) {
//			Object[] elements = ((StructuredSelection) selection).toArray();
//			if (elements != null && elements.length == 1) {
//				if (elements[0] instanceof TreeNode) {
//					if (((TreeNode<?>) elements[0]).getModel() instanceof Pair<?, ?>) {
//						if (((Pair<?, ?>) ((TreeNode<?>) elements[0]).getModel()).getFirst() instanceof Layer) {
//
//							Layer layer = (Layer) ((Pair<?, ?>) ((TreeNode<?>) elements[0]).getModel()).getFirst();
//							List<File> files = new ArrayList<File>();
//							LayerConfiguration lc = new LayerConfiguration();
//							boolean empty = true;
//
//							if (layer instanceof ILazyLoading) {
//								File file = ((ILazyLoading) layer).getInputFile();
//								if (file != null) {
//									// FIXME verifier si le model ou le survey
//									// contient dela le layer?
//									// for (MacroLayer ml : bm.getMacroLayers())
//									// {
////									if (bm.getLayers().contains(layer)) {
////										lc.addLayer(new LayerDef(null, file.getName()));
////										// break;
////									}
//									// }
//								}
//							}
//							if (layer instanceof IFiles) {
//								Map<File, Boolean> m = ((IFiles) layer).getFiles();
//								if (m != null) {
//									for (File f : m.keySet()) {
//										if (f != null) {
//											files.add(f);
//											empty = false;
//										}
//									}
//								} else {
//									Logging.logger().severe("Can't export layer " + layer);
//								}
//							} else {
//								Logging.logger().fine("Can't export layer data for " + layer);
//							}
//
//							// if at least one file to export
//							if (!empty) {
//								FileDialog dialog = new FileDialog(shell, SWT.SAVE);
//
//								dialog.setFilterNames(FORMAT_NAMES);
//								dialog.setFilterExtensions(FORMAT_EXTENSIONS);
//
//								final String fileName = dialog.open();
//								if (fileName != null) {
//									File dest = new File(fileName);
//
//									// include configuration file into archive
//									File confFile = getConfigurationFile(dest.getName() + LayerConfiguration.EXTENSION);
//									lc.save(confFile);
//									files.add(confFile);
//
//									// get directories path
//									List<String> directories = new ArrayList<String>();
//									for (File f : files) {
//										if (f.isDirectory()) {
//											String path = f.getAbsolutePath();
//											directories.add(path);
//										}
//									}
//									// when a directory is added to this zip
//									// file all
//									// children files are automatically included
//									// :
//									// filter directories children
//									Iterator<File> it = files.iterator();
//									while (it.hasNext()) {
//										File f = it.next();
//										if (f.isFile()) {
//											for (String dir : directories) {
//												if (f.getParent().startsWith(dir)) {
//													it.remove();
//													break;
//												}
//											}
//										}
//									}
//									try {
//										// zip files
//										ZipUtils.zip(dest, files.toArray(new File[0]));
//									} catch (IOException e1) {
//										Logging.logger().severe("Error during export " + e1);
//									}
//
//									confFile.delete();
//
//								} else {
//									logger.info("Null file name : no export");
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//	}
//
//	/**
//	 * Returns configuration file.
//	 */
//	private File getConfigurationFile(String name) {
//		// find configuration directory in user home
//		String dirName = SSVConfiguration.configurationDirectory();
//		File dir = new File(dirName);
//		boolean exists = dir.exists();
//		// if this directory doesn't exist yet creates it
//		if (!exists) {
//			exists = dir.mkdir();
//		}
//		if (exists) {
//			String fileName = dirName + "/" + name;
//			return new File(fileName);
//		} else {
//			Logging.logger().warning("Can't find configuration directory" + dirName);
//		}
//		return null;
//	}

}
