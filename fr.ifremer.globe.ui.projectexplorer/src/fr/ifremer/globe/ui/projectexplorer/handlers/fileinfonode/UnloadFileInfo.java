/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.handlers.fileinfonode;

import java.io.File;
import java.util.List;

import jakarta.inject.Inject;

import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode.LoadedState;

public class UnloadFileInfo extends AbstractNodeLoadHandler {

	@Inject
	protected IFileLoadingService fileLoadingService;

	@Override
	public boolean canExecute(FileInfoNode node) {
		return node.getLoadedState() == LoadedState.LOADED;
	}

	@Override
	public void execute(IFileLoadingService fileLoadingService, List<File> fileList) {
		fileLoadingService.unloadFiles(fileList);
	}

}
