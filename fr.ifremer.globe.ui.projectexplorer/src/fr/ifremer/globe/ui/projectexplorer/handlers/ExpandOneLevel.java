package fr.ifremer.globe.ui.projectexplorer.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.ui.projectexplorer.view.ProjectExplorer;

public class ExpandOneLevel extends ExpandAllHandler {

	@Override
	@Execute
	public void execute(EPartService partService, ESelectionService selectionService) {
		MPart part = partService.getActivePart();
		Object explorer = part.getObject();
		if (explorer instanceof ProjectExplorer) {
			((ProjectExplorer) explorer).expandAllOneLevel(computeNodeList(partService, selectionService));
		}
	}

}
