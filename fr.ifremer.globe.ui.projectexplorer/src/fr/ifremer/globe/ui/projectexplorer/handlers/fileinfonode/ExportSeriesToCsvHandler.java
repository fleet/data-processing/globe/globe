package fr.ifremer.globe.ui.projectexplorer.handlers.fileinfonode;

import java.util.List;
import java.util.stream.Collectors;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.services.IChartService;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.wizard.saveseriesascsv.SaveWizard;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Handler to export {@link IChartSeries} to CSV.
 */
public class ExportSeriesToCsvHandler extends AbstractNodeHandler {

	@Inject
	private IChartService chartService;

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return !getSelectedFileInfoWithSeries(selectionService).isEmpty();
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService, EPartService partService,
			@Active MItem item) {
		return checkExecution(selectionService, modelService);
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, ESelectionService selectionService,
			MApplication application, EModelService modelService, EPartService partService, @Active MItem item)
			throws GIOException {
		// open save wizard
		var inputFiles = getSelectedFileInfoWithSeries(selectionService);
		if (!inputFiles.isEmpty()) {

			var series = inputFiles.stream().flatMap(f -> {
				try {
					return chartService.getChartSeries(f).stream();
				} catch (GIOException e) {
					Messages.openErrorMessage("Error while reading series from file : " + f.getFilename(), e);
					return null;
				}
			}).collect(Collectors.toList());

			((SaveWizard) ContextInitializer.make(SaveWizard.class)).open(shell, series);
		}

	}

	private List<IFileInfo> getSelectedFileInfoWithSeries(ESelectionService selectionService) {
		return getSelectionAsList(getSelection(selectionService), chartService::isAccepted).stream()
				.map(FileInfoNode::getFileInfo).collect(Collectors.toList());
	}

}