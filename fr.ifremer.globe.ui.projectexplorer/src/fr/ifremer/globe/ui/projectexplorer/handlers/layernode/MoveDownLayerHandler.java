package fr.ifremer.globe.ui.projectexplorer.handlers.layernode;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.utils.SelectionUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.ElevationNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;

public class MoveDownLayerHandler extends AbstractNodeHandler {

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return !getNodeToMove(selectionService).isEmpty();
	}

	@Execute
	public void execute(ESelectionService selectionService, IGeographicViewService geographicViewService) {
		getNodeToMove(selectionService).forEach(treeNode -> treeNode.traverseDown(node -> {
			if (node instanceof LayerNode layerNode && layerNode.getLayer().isPresent()
					&& layerNode.getLayer().get() instanceof IWWLayer layer)
				geographicViewService.moveLayerToBackground(layer);
			else if (node instanceof ElevationNode elevationNode)
				geographicViewService.moveElevationToBack(elevationNode.getElevationModel());
		}));
		geographicViewService.refresh();
	}

	static protected List<TreeNode> getNodeToMove(ESelectionService selectionService) {
		return SelectionUtils.getSelection(selectionService, TreeNode.class).stream().flatMap(node -> {
			var nodeToMove = new ArrayList<TreeNode>();
			node.traverseDown(childNode -> {
				if ((childNode instanceof LayerNode layerNode && layerNode.getLayer().isPresent()
						&& layerNode.getLayer().get() instanceof IWWLayer) || childNode instanceof ElevationNode)
					nodeToMove.add(childNode);
			});
			return nodeToMove.stream();
		}).toList();
	}
}
