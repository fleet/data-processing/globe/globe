package fr.ifremer.globe.ui.projectexplorer.handlers.groupnode;

import java.util.Optional;

import jakarta.inject.Named;

import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.projectexplorer.utils.DtmAutoOrganizer;
import fr.ifremer.globe.ui.projectexplorer.wizard.organizedtm.OrganizeDTMsWizard;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.utils.SelectionUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;

/**
 * Auto organize DTM and put the in group per QI
 */
public class AutoOrganizeDTM extends AbstractNodeHandler {

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return getSelectedGroup(selectionService).isPresent();
	}

	@Execute
	public void execute(final ESelectionService selectionService, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell,
			IProcessService jobService) {
		getSelectedGroup(selectionService).ifPresent(groupNode -> {
			OrganizeDTMsWizard w = new OrganizeDTMsWizard();
			WizardDialog dialog = new WizardDialog(Display.getCurrent().getActiveShell(), w);

			if (dialog.open() == 0) {
				DtmAutoOrganizer organizer = new DtmAutoOrganizer(groupNode, w.getSelectedParameters(),
						w.getSelectedMode(), w.deleteEmptyGroups());
				jobService.runInForeground("Organize DTM", (monitor, logger) -> {
					organizer.run(monitor);
					return Status.OK_STATUS;
				});
			}
		});
	}

	private Optional<FolderNode> getSelectedGroup(ESelectionService selectionService) {
		return SelectionUtils.getUniqueSelection(selectionService, FolderNode.class,
				folderNode -> folderNode.testDown(node -> node instanceof LayerNode layerNode
						&& layerNode.getLayer().filter(IWWTerrainLayer.class::isInstance).isPresent()));
	}
}