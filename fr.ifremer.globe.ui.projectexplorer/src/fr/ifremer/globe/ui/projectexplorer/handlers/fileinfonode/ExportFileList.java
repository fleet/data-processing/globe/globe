package fr.ifremer.globe.ui.projectexplorer.handlers.fileinfonode;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.utils.SelectionUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;

public class ExportFileList extends AbstractNodeHandler {
	private Logger logger = LoggerFactory.getLogger(ExportFileList.class);

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return !getFileInfoToExport(selectionService).isEmpty();
	}

	private List<FileInfoNode> getFileInfoToExport(ESelectionService selectionService) {
		var fileInfoNodeToExport = new ArrayList<FileInfoNode>();
		SelectionUtils
				.getSelection(selectionService, GroupNode.class,
						groupNode -> groupNode instanceof FolderNode || groupNode instanceof FileInfoNode)
				.forEach(node -> {
					if (node instanceof FileInfoNode fileInfoNode)
						fileInfoNodeToExport.add(fileInfoNode);
					else if (node instanceof FolderNode folderNode) {
						folderNode.traverseDown(childNode -> {
							if (childNode instanceof FileInfoNode fileInfoNode)
								fileInfoNodeToExport.add(fileInfoNode);
						});
					}
				});
		return fileInfoNodeToExport.stream().distinct().toList();
	}

	@Execute
	public void execute(ESelectionService selectionService, Shell shell) {
		var fileInfoNodeToExport = getFileInfoToExport(selectionService);

		if (!fileInfoNodeToExport.isEmpty()) {
			FileDialog fd = new FileDialog(shell, SWT.SAVE);
			fd.setText("Save to file ");
			String[] filterExt = { "*.txt", "*.*" };
			fd.setFilterExtensions(filterExt);
			String selected = fd.open();
			if (selected != null) {
				FileWriter fr = null;
				try {
					fr = new FileWriter(selected);
					for (FileInfoNode l : fileInfoNodeToExport) {
						String path = l.getFile().getAbsolutePath();
						fr.write(path);
						fr.write(System.lineSeparator());
					}
					Messages.openInfoMessage("Export success", String.format("File list (%d file%s) saved in : %s",
							fileInfoNodeToExport.size(), fileInfoNodeToExport.size() > 1 ? "s" : "", selected));
				} catch (IOException e) {
					Messages.openErrorMessage("Error while exporting file list to file " + selected, e.getMessage());
				} finally {
					// close resources
					try {
						fr.close();
					} catch (IOException e) {
						logger.error(e.getMessage(), e);
					}
				}

			}
		} else {
			Messages.openInfoMessage("Export aborted", "Aborted : no loaded file were found");
		}

	}

}
