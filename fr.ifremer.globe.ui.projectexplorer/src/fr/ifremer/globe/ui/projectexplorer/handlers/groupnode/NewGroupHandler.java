package fr.ifremer.globe.ui.projectexplorer.handlers.groupnode;

import java.util.Optional;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ui.projectexplorer.ui.dialogs.NewGroupDialog;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;

public class NewGroupHandler {

	/**
	 * @return the selected {@link groupNode} if exists, else an empty {@link Optional}.
	 */
	private Optional<GroupNode> getSelectedgroupNode(ESelectionService selectionService) {
		return selectionService.getSelection() instanceof StructuredSelection selection
				&& selection.getFirstElement() instanceof GroupNode groupNode ? //
						Optional.of(groupNode) : Optional.empty();
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService) {
		return getSelectedgroupNode(selectionService).isPresent();
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell, ESelectionService selectionService,
			ProjectExplorerModel projectExplorerModel, ITreeNodeFactory treeNodeFactory) {
		NewGroupDialog dialog = new NewGroupDialog(shell);
		if (dialog.open() == Window.OK) {
			var groupNode = getSelectedgroupNode(selectionService).orElse(projectExplorerModel.getDataNode());
			var newGroup = treeNodeFactory.createFolderNode(groupNode, dialog.getProjectName(), dialog.getComments());
			projectExplorerModel.setSelection(newGroup);
		}
	}

}
