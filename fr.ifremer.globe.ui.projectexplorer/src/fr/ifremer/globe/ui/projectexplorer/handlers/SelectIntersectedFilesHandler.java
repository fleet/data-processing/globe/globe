/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.handlers;

import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import jakarta.inject.Inject;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.gdal.ogr.Geometry;
import org.gdal.ogr.ogrConstants;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoEvent;
import fr.ifremer.globe.core.model.file.FileInfoModel;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.polygon.IPolygon;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.projectexplorer.view.ProjectExplorer;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.ui.utils.Messages;

/**
 * Handler to select files with a geobox intersecting the polygon files
 */
public class SelectIntersectedFilesHandler extends AbstractNodeHandler {

	/**
	 * Model of loaded IFileInfo
	 */
	@Inject
	private FileInfoModel fileInfoModel;

	@Inject
	private IEventBroker eventBroker;

	/**
	 * @return true if this handler can be executed
	 */
	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement uiElement) {
		return checkExecution(selectionService, uiElement);
	}

	/** {@inheritDoc} */
	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		List<PolygonFileInfo> polygonFiles = getSelectionAsList(selectionService, PolygonFileInfo.class);
		return !polygonFiles.isEmpty();
	}

	/**
	 * Execution of this handler
	 */
	@Execute
	public void execute(ESelectionService selectionService, @Active MPart activePart) {
		Object explorer = activePart.getObject();
		if (!(explorer instanceof ProjectExplorer)) {
			return;
		}

		// Searchs file intersecting one of the polygon
		Set<IFileInfo> filesToSelect = getIntersectingFiles(selectionService);

		if (!filesToSelect.isEmpty()) {
			eventBroker.send(FileInfoEvent.EVENT_UI_FILES_SELECTED, filesToSelect.toArray(IFileInfo[]::new));
		} else {
			Messages.openInfoMessage("Intersection selection", "No file match");
		}
	}

	/**
	 * @return the list of gdal polygons of selected polygon files
	 */
	private List<Geometry> getSelectedPolygons(ESelectionService selectionService) {
		List<PolygonFileInfo> polygonFiles = getSelectionAsList(selectionService, PolygonFileInfo.class);
		// Lists geoboxes of polygon files
		return polygonFiles.stream()//
				.flatMap(f -> f.getPolygons().stream())//
				.map(IPolygon::getGeometry)//
				.filter(Objects::nonNull)//
				.filter(geometry -> !geometry.IsEmpty())//
				.map(p -> {
					// Wrap into a OGR Polygon to allow Intersects() calls
					Geometry result = new Geometry(ogrConstants.wkbPolygon);
					result.AddGeometry(p.Clone());
					return result;
				})//
				.collect(Collectors.toList());
	}

	/**
	 * Browse the opened files. Returns those who have a Geobox intersecting one of the polygon files
	 */
	private Set<IFileInfo> getIntersectingFiles(ESelectionService selectionService) {
		List<Geometry> polygons = getSelectedPolygons(selectionService);
		return fileInfoModel.getAll().stream() //
				.filter(fileInfo -> !EnumSet.of(ContentType.POLYGON_GDAL, ContentType.GEOJSON_GDAL)
						.contains(fileInfo.getContentType())) //
				.filter(fileInfo -> fileInfo.getGeoBox() != null && !fileInfo.getGeoBox().isEmpty()) //
				.filter(fileInfo -> {
					GeoBox geobox = fileInfo.getGeoBox();
					Geometry fileGeometry = GeoBoxUtils.toPolygon(geobox);
					boolean isIntersecting = polygons.stream().anyMatch(fileGeometry::Intersect);
					fileGeometry.delete();
					return isIntersecting;
				})//
				.collect(Collectors.toSet());
	}

}
