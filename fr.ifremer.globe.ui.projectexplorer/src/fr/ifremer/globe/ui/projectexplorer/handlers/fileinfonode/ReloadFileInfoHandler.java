/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.handlers.fileinfonode;

import java.io.File;
import java.util.List;

import jakarta.inject.Inject;

import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

public class ReloadFileInfoHandler extends AbstractNodeLoadHandler {

	@Inject
	protected IFileLoadingService fileLoadingService;

	@Override
	public boolean canExecute(FileInfoNode node) {
		return true;
	}

	@Override
	public void execute(IFileLoadingService fileLoadingService, List<File> fileList) {
		fileLoadingService.reloadFiles(fileList);
	}
}
