package fr.ifremer.globe.ui.projectexplorer.handlers.fileinfonode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.utils.SelectionUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;

public abstract class AbstractNodeLoadHandler extends AbstractNodeHandler {

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return SelectionUtils.getSelection(selectionService, FileInfoNode.class).stream().anyMatch(this::canExecute)
				|| SelectionUtils.getSelection(selectionService, GroupNode.class).stream().anyMatch(this::canExecute);
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Execute
	public void execute(ESelectionService selectionService, IFileLoadingService fileLoadingService) {
		List<FileInfoNode> fileInfoNodeToExcute = new ArrayList<>();

		SelectionUtils.getSelection(selectionService, FileInfoNode.class).stream().filter(this::canExecute)
				.forEach(fileInfoNodeToExcute::add);

		SelectionUtils.getSelection(selectionService, GroupNode.class).stream().filter(this::canExecute)
				.forEach(groupNode -> groupNode.traverseDown(childNode -> {
					if (childNode instanceof FileInfoNode fileInfoNode && !fileInfoNodeToExcute.contains(fileInfoNode)
							&& canExecute(fileInfoNode))
						fileInfoNodeToExcute.add(fileInfoNode);
				}));

		execute(fileLoadingService, fileInfoNodeToExcute.stream().map(FileInfoNode::getFile).toList());
	}

	public abstract boolean canExecute(FileInfoNode node);

	public abstract void execute(IFileLoadingService fileLoadingService, List<File> fileList);

	private boolean canExecute(GroupNode groupNode) {
		return groupNode
				.testDown(childNode -> childNode instanceof FileInfoNode fileInfoNode && canExecute(fileInfoNode));
	}

}
