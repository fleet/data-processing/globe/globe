package fr.ifremer.globe.ui.projectexplorer.handlers.groupnode;

import java.util.Optional;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.projectexplorer.ui.dialogs.RenameLayerDialog;
import fr.ifremer.globe.ui.utils.SelectionUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;

public class RenameGroupHandler extends AbstractNodeHandler {

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return !getGroupToRename(selectionService).isEmpty();
	}

	@Execute
	public void execute(ESelectionService selectionService, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		getGroupToRename(selectionService).ifPresent(groupNode -> //
		Display.getDefault().syncExec(() -> {
			var dialog = new RenameLayerDialog(shell, groupNode.getName());
			if (dialog.open() == 0)
				groupNode.setName(dialog.getNewName());
		}));
	}

	private Optional<FolderNode> getGroupToRename(ESelectionService selectionService) {
		return SelectionUtils.getUniqueSelection(selectionService, FolderNode.class,
				node -> node.getParent().isPresent());
	}
}