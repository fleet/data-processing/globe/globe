/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package fr.ifremer.globe.ui.projectexplorer.handlers;

import jakarta.inject.Named;

import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MContribution;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.services.IServiceConstants;

import fr.ifremer.globe.core.model.ISaveable;
import fr.ifremer.globe.core.runtime.job.IProcessService;

public class SaveHandler {

	@CanExecute
	public boolean canExecute(@Named(IServiceConstants.ACTIVE_PART) final MContribution activePart, MWindow window) {
		if (activePart != null) {
			Object obj = activePart.getObject();
			if (obj instanceof ISaveable) {
				return ((ISaveable) obj).isDirty();
			}
		}
		return false;
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_PART) final MContribution activePart,
			IProcessService processService) {
		if (activePart != null && activePart.getObject() instanceof ISaveable saveable) {
			processService.runInForeground("Save", (monitor, logger) -> {
				return saveable.doSave(monitor) ? Status.OK_STATUS : Status.error("Save error");
			});
		}
	}
}