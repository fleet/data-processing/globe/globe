package fr.ifremer.globe.ui.projectexplorer.handlers.layernode;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.StructuredSelection;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.ITarget;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.layers.Layer;
import jakarta.inject.Inject;
import jakarta.inject.Named;

public class GoToLayerHandler extends AbstractNodeHandler {

	@Inject
	IGeographicViewService geographicViewService;

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return selectionService.getSelection() instanceof StructuredSelection selection
				&& !getSectors(selection).sectors().isEmpty();
	}

	@Execute
	public void execute(ESelectionService selectionService) {
		if (selectionService.getSelection() instanceof StructuredSelection selection) {
			ZoomSector zoomSector = getSectors(selection);
			geographicViewService.zoomToSector(zoomSector.sectors(), zoomSector.elevation);
		}
	}

	/**
	 * Synchronization of geographic view (automatic "Go to" when a layer is selected).
	 */
	@Inject
	public void onSelection(
			@org.eclipse.e4.core.di.annotations.Optional @Named(IServiceConstants.ACTIVE_SELECTION) StructuredSelection structuredSelection) {
		if (structuredSelection != null && geographicViewService.isSyncViewSelection()) {
			ZoomSector zoomSector = getSectors(structuredSelection);
			if (!zoomSector.sectors().isEmpty()) {
				geographicViewService.zoomToSector(zoomSector.sectors(), zoomSector.elevation);
			}
		}
	}

	/**
	 * @return list of sectors from selection
	 */
	private ZoomSector getSectors(StructuredSelection structuredSelection) {
		ZoomSector result = new ZoomSector(new LinkedList<>(), 0f);
		for (var selection : structuredSelection)
			result = getSectors(result, selection);
		return result;
	}

	/**
	 * @return list of sectors from selection
	 */
	private ZoomSector getSectors(ZoomSector currentZoomSector, Object selection) {
		AtomicReference<ZoomSector> result = new AtomicReference<>(currentZoomSector);

		if (selection instanceof GroupNode groupNode) {
			for (TreeNode childNode : groupNode.getChildren())
				result.set(getSectors(result.get(), childNode));
		}

		if (selection instanceof ITarget target)
			result.get().sectors().add(target.getSector());

		// get sector from file info GeoBox
		if (selection instanceof FileInfoNode fileInfoNode) {
			Optional.of(fileInfoNode.getFileInfo())//
					.map(IFileInfo::getGeoBox)//
					.filter(Objects::nonNull)//
					.map(GeoBoxUtils::convert)//
					.ifPresent(result.get().sectors::add);
		}

		// get sector from LayerNode
		if (selection instanceof LayerNode layerNode) {
			Optional<Layer> layer = layerNode.getLayer();
			if (layer.isPresent() && layer.get() instanceof Layer wwLayer) {
				if (wwLayer.hasKey(AVKey.SECTOR))
					result.get().sectors().add((Sector) wwLayer.getValue(AVKey.SECTOR));
				else if (wwLayer instanceof ITarget targetLayer)
					Optional.ofNullable(targetLayer.getSector()).ifPresent(result.get().sectors()::add);
				if (wwLayer.hasKey(IWWLayer.MAX_ELEVATION_KEY)) {
					double elevation = (Double) wwLayer.getValue(IWWLayer.MAX_ELEVATION_KEY);
					if (Double.isFinite(elevation) && elevation != Double.MAX_VALUE)
						result.getAndUpdate(zoomSector -> zoomSector.withElevation((float) elevation));
				}
			}
		}

		return result.get();
	}

	record ZoomSector(List<Sector> sectors, float elevation) {
		public ZoomSector withElevation(double newElev) {
			if (Double.isFinite(newElev) && newElev != Double.MAX_VALUE) {
				if (elevation == 0f)
					return new ZoomSector(sectors, (float) newElev);
				return new ZoomSector(sectors, Math.max(elevation, (float) newElev));
			}
			return this;
		}
	}

}
