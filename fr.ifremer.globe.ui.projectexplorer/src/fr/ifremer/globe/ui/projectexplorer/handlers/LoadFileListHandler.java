package fr.ifremer.globe.ui.projectexplorer.handlers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectToolItem;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;

/**
 * Handles file load from a text file which contains a list of path.
 */
public class LoadFileListHandler {

	@Inject
	protected IFileLoadingService fileLoadingService;

	@Inject
	protected ITreeNodeFactory nodeFactory;

	private String defaultDirectory = "";

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell,
			@Named(IServiceConstants.ACTIVE_SELECTION) StructuredSelection structuredSelection,
			@Optional @Active MDirectToolItem item, ProjectExplorerModel projectExplorerModel) {

		FileDialog dialog = new FileDialog(shell, SWT.SINGLE);
		dialog.setText("Open a file list (.txt)");
		dialog.setFilterNames(new String[] { "File path list" });
		dialog.setFilterExtensions(new String[] { "*.txt" });
		dialog.setFilterPath(defaultDirectory);

		StringBuilder errors = new StringBuilder();
		int newFile = 0;
		int existingFile = 0;

		if (dialog.open() != null) {
			defaultDirectory = dialog.getFilterPath();
			try (Stream<String> filePathStream = Files.lines(Paths.get(defaultDirectory, dialog.getFileName()))) {
				var filePaths = filePathStream.collect(Collectors.toList());
				GroupNode parentNode = (GroupNode) structuredSelection.getFirstElement();
				int line = 0;

				var filesToLoad = new ArrayList<String>(filePaths.size());
				for (String filePath : filePaths) {
					line++;
					try {
						Paths.get(filePath);// check if the paths are valid => InvalidPathException

						// creates new node if necessary
						if (projectExplorerModel.getFileInfoNode(filePath).isEmpty()) {
							nodeFactory.createFileInfoNode(new BasicFileInfo(filePath), parentNode);
							newFile++;
							filesToLoad.add(filePath);
						} else {
							existingFile++;
						}

						// load files
						fileLoadingService.load(filesToLoad, false);

					} catch (InvalidPathException ex) {
						if (errors.length() < 500) {
							errors.append("Invalid file path : " + filePath + " (line : " + line + ")\n");
						}
					}
				}
				if (errors.length() > 500) {
					errors.append("...");
				}
			} catch (IOException e) {
				errors.append(String.format("Error while reading '%s' : %s %n", dialog.getFileName(), e.toString()));
			}
			// open error message
			if (errors.length() > 0) {
				Messages.openErrorMessage("Error while loading file list", errors.toString());
			}

			// open result message
			String resultMessage = newFile + " file" + (newFile > 1 ? "s" : "") + " loaded.";
			if (existingFile > 0) {
				resultMessage += "\n" + existingFile + " file" + (existingFile > 1 ? "s" : "") + " already present.";
			}
			Messages.openInfoMessage("File list loaded", resultMessage);

		}

	}
}
