package fr.ifremer.globe.ui.projectexplorer.handlers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.DetectionStatusApplier;
import fr.ifremer.globe.core.runtime.job.IProcessFunction;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.projectexplorer.wizard.convert.tocsv.ConvertToCsvWizardModel;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

/**
 * Handler to launch the process of Validity Flags
 */
public class ApplyValidityFlagsHandler extends AbstractNodeHandler {

	/** directory path used by the FileDialog */
	protected String filterPath = null;

	/**
	 * @return true when process is possible
	 */
	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	/**
	 * @see fr.ifremer.globe.ui.handler.AbstractNodeHandler#computeCanExecute(org.eclipse.e4.ui.workbench.modeling.ESelectionService)
	 */
	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return getSelectedFiles(selectionService).size() >= 1;
	}

	/**
	 * @return selected MBG and XSF files
	 */
	protected List<ISounderNcInfo> getSelectedFiles(ESelectionService selectionService) {
		return getSelectionAsList(getSelection(selectionService),
				contentType -> contentType.isOneOf(ContentType.MBG_NETCDF_3, ContentType.XSF_NETCDF_4)).stream()
						.map(FileInfoNode::getFileInfo).//
						map(ISounderNcInfo.class::cast).//
						collect(Collectors.toList());
	}

	/**
	 * Launches the process on the selected file
	 */
	@Execute
	public void execute(final ESelectionService selectionService, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		var sounderNcFiles = getSelectedFiles(selectionService);
		if (sounderNcFiles.isEmpty())
			return;
		if (filterPath == null)
			filterPath = sounderNcFiles.get(0).getParent();

		var csvFiles = selectCsvFile(shell);
		if (csvFiles.isEmpty())
			return;

		// one to one mode
		if (sounderNcFiles.size() == 1 && csvFiles.size() == 1) {
			filterPath = csvFiles.get(0).getParent();
			IProcessService.grab().runInBackground("Apply validity flags",
					new DetectionStatusApplier(sounderNcFiles.get(0), csvFiles.get(0)), true);
			return;
		}

		// batch mode
		var processesToRun = new ArrayList<IProcessFunction>();
		var notFoundFile = new ArrayList<ISounderNcInfo>();
		for (var sounderNcFile : sounderNcFiles) {
			csvFiles.stream()
					.filter(csvFile -> csvFile.getName().contains(sounderNcFile.getBaseName().replace(".xsf", "")))
					.findAny().ifPresentOrElse(matchingCsvFile -> {
						filterPath = matchingCsvFile.getParent();
						processesToRun.add(new DetectionStatusApplier(sounderNcFile, matchingCsvFile));
					}, () -> notFoundFile.add(sounderNcFile));
		}

		IProcessService.grab().runInBackground("Apply validity flags", IProcessFunction.join(processesToRun), true);

		if (!notFoundFile.isEmpty()) {
			var warningMessage = "No validity flags file found for :\n";
			warningMessage += notFoundFile.stream().map(ISounderNcInfo::getBaseName).collect(Collectors.joining("\n"));
			Messages.openWarningMessage("Validity file missing", warningMessage);
		}
	}

	private List<File> selectCsvFile(Shell shell) {
		// Dialog box
		FileDialog dialog = new FileDialog(shell, SWT.OPEN | SWT.MULTI);
		dialog.setFilterExtensions(new String[] { "*." + ConvertToCsvWizardModel.VALIDITY_EXTENSION });
		dialog.setFilterNames(new String[] { "Validity flags (*." + ConvertToCsvWizardModel.VALIDITY_EXTENSION + ")" });
		dialog.setFilterPath(filterPath);
		dialog.setText("Choose a CSV file containing the validity flags");

		var result = new ArrayList<File>();
		if (dialog.open() != null) {
			String[] fileNames = dialog.getFileNames();
			for (String fileName : fileNames) {
				File file = new File(dialog.getFilterPath(), fileName);
				result.add(file);
			}
		}
		return result;
	}

}