package fr.ifremer.globe.ui.projectexplorer.handlers.fileinfonode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.projectexplorer.ui.dialogs.DeleteRessourceDialog;
import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;

public class DeleteFileInfoNodeHandler extends AbstractNodeHandler {

	@Inject
	protected IFileLoadingService fileLoadingService;

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return getSelection(selectionService, obj -> obj instanceof FileInfoNode || obj instanceof FolderNode)
				.size() == getSelection(selectionService).length;
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService, EPartService partService,
			@Active MItem item) {
		return checkExecution(selectionService, modelService);
	}

	/**
	 * Perform the deletion
	 */
	@Execute
	public void execute(IEclipseContext context, ESelectionService selectionService, Shell shell) {
		// get nodes to delete
		var fileNodeToDelete = new ArrayList<>(getSelection(selectionService, FileInfoNode.class));
		var folderNodeToDelete = getSelection(selectionService, FolderNode.class);
		folderNodeToDelete.forEach(folderNode -> folderNode.traverseDown(childNode -> {
			if (childNode instanceof FileInfoNode childFileInfoNode && !fileNodeToDelete.contains(childFileInfoNode))
				fileNodeToDelete.add(childFileInfoNode);
		}));

		if (fileNodeToDelete.isEmpty()) {
			folderNodeToDelete.forEach(FolderNode::delete); // directly remove empty groups
		} else {
			var fileInfoToDelete = fileNodeToDelete.stream().map(FileInfoNode::getFileInfo).toList();
			var deleteRessourceDialog = new DeleteRessourceDialog(shell, fileInfoToDelete);
			if (deleteRessourceDialog.open() == Window.OK) {
				var fileToDelete = fileInfoToDelete.stream().map(IFileInfo::getAbsolutePath).toList();
				IProcessService.grab().runInForeground(
						"Delete " + fileInfoToDelete.size() + " file" + (fileInfoToDelete.size() > 1 ? "s" : ""),
						(monitor, logger) -> {
							fileLoadingService.cancel(fileToDelete);
							fileLoadingService.delete(fileToDelete);
							folderNodeToDelete.forEach(FolderNode::delete); // useful to delete 'unloaded' file nodes
							fileNodeToDelete.forEach(FileInfoNode::delete);

							// delete from disk
							if (deleteRessourceDialog.deleteFromDisk()) {
								for (var file : fileToDelete)
									try {
										Files.deleteIfExists(Path.of(file));
									} catch (IOException | SecurityException e) {
										Messages.openErrorMessage("Delete from disk error",
												"Can't remove '" + file + "' from disk.", e);
									}
							}
							return Status.OK_STATUS;
						});
			}
		}
	}

}
