/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.ui.projectexplorer.handlers.fileinfonode;

import java.io.File;
import java.util.List;

import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode.LoadedState;

public class LoadFileInfoHandler extends AbstractNodeLoadHandler {

	@Override
	public boolean canExecute(FileInfoNode node) {
		return node.getLoadedState() != LoadedState.LOADED && node.getLoadedState() != LoadedState.LOADING;
	}

	@Override
	public void execute(IFileLoadingService fileLoadingService, List<File> fileList) {
		fileLoadingService.loadFiles(fileList);
	}

}
