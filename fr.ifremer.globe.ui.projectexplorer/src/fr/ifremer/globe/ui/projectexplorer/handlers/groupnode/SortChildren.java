package fr.ifremer.globe.ui.projectexplorer.handlers.groupnode;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.utils.SelectionUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;

/**
 * Sort Children of a group Node
 */
public class SortChildren extends AbstractNodeHandler {

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return SelectionUtils.getSelection(selectionService, FolderNode.class).size() == 1;
	}

	@Execute
	public void execute(ESelectionService selectionService) {
		SelectionUtils.getSelection(selectionService, FolderNode.class).get(0).sortChildren();
	}
}