package fr.ifremer.globe.ui.projectexplorer.handlers;

import java.io.File;
import java.io.IOException;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.widgets.Shell;

import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

public class BasicSystemOpenFileHandler extends AbstractNodeHandler {
	BasicSystemOpenFileHandler() {
		// we ensure to always show on item of the sub menu, if not it
		// disappears and is never show again
		super(false);

	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement uiElement) {
		return checkExecution(selectionService, uiElement);
	}

	@Execute
	public void execute(ESelectionService selectionService, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		try {
			Object[] selection = getSelection(selectionService);
			if (selection.length != 1) {
				return;
			}
			Object e = selection[0];
			if (e instanceof FileInfoNode) {
				File file = ((FileInfoNode) e).getFile();
				if(file != null && file.exists())
				{		
					java.awt.Desktop.getDesktop().open(file);
				}else
				{

					Messages.openErrorMessage("Cannot find file", "Cannot find file "+((FileInfoNode)e).getFile().getAbsolutePath()  );

				}
			}
		} catch (IOException e) {
			Messages.openErrorMessage("Error", e.getMessage());

		}
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		Object[] selection = getSelection(selectionService);
		if (selection.length != 1) {
			return false;
		}
		Object e = selection[0];
		if (e instanceof FileInfoNode) {
			return true;
		}
		return false;
	}

}
