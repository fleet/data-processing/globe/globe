package fr.ifremer.globe.ui.projectexplorer.handlers.layernode;

import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.utils.SelectionUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;

public class DuplicateLayerHandler extends AbstractNodeHandler {

	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return getSelectedLayerNode(selectionService).isPresent();
	}

	@Execute
	public void execute(ESelectionService selectionService) {
		getSelectedLayerNode(selectionService).ifPresent(layerNode -> {
			IFileInfo fileInfo = findFileInfo(layerNode);
			var optLayer = layerNode.getLayer();
			if (fileInfo != null && optLayer.isPresent() && optLayer.get() instanceof IWWLayer layerToDuplicate) {
				WWFileLayerStore layerStore = fileLayerStoreModel.get(fileInfo).orElseThrow();
				String newLayerName = null;
				for (int suffix = 1; suffix < Integer.MAX_VALUE; suffix++) {
					String newLayerNameFinal = newLayerName = layerNode.getName() + "_" + suffix;
					boolean indexFound = layerStore.getLayers().stream()
							.anyMatch(oneLayer -> newLayerNameFinal.equals(oneLayer.getName()));
					if (!indexFound)
						break;
				}

				IWWLayer newLayer = layerToDuplicate //
						.getDuplicationProvider().orElseThrow() // presence verified in canExecute
						.apply(newLayerName);
				newLayer.setIcon(layerNode.getImage());
				fileLayerStoreModel.addLayersToStore(fileInfo, newLayer);
			}
		});
	}

	private Optional<LayerNode> getSelectedLayerNode(ESelectionService selectionService) {
		return SelectionUtils.getUniqueSelection(selectionService, LayerNode.class)
				.filter(layerNode -> layerNode.getLayer().filter(IWWLayer.class::isInstance).map(IWWLayer.class::cast)
						.map(IWWLayer::getDuplicationProvider).isPresent());
	}

	public IFileInfo findFileInfo(TreeNode node) {
		return node instanceof FileInfoNode fileInfoNode ? fileInfoNode.getFileInfo()
				: node.getParent().map(this::findFileInfo).orElse(null);
	}

}
