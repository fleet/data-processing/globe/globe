package fr.ifremer.globe.ui.projectexplorer.handlers;

import java.io.File;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MBasicFactory;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.handler.PartManager;
import fr.ifremer.globe.ui.parts.TextEditorView;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;

public class TextEditorHandler extends AbstractNodeHandler {

	public TextEditorHandler() {
		super();
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		boolean value = false;
		Object[] selection = getSelection(selectionService);
		if (selection.length != 1) {
			return false;
		}
		Object e = selection[0];
		if (e instanceof FileInfoNode) {
			if (TextEditorView.acceptFile(new File(((FileInfoNode) e).getFileInfo().getPath()))) {
				value = true;
			} else {
				value = false;
			}
		}

		return value;
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Execute
	public void execute(ESelectionService selectionService, EPartService partService, MApplication application, EModelService modelService) {
		Object[] selection = getSelection(selectionService);

		if (selection[0] instanceof FileInfoNode) {
			FileInfoNode node = (FileInfoNode) selection[0];
			if (TextEditorView.acceptFile(new File(node.getFileInfo().getPath()))) {

				MPart textEditorPart = createPart(partService, application, modelService);
				if (textEditorPart != null) {

					// Set label
					String label = "";
					label = node.getFileInfo().getPath();

					textEditorPart.setLabel("Text Editor " + label);

					// Set selection
					if (!textEditorPart.isToBeRendered()) {
						partService.showPart("fr.ifremer.globe.ui.parts.texteditorview", PartState.ACTIVATE);
					}
					TextEditorView textEditorView = (TextEditorView) textEditorPart.getObject();
					if (textEditorView != null) {
						textEditorView.setPart(textEditorPart);
						textEditorView.openFile(new File((node).getFileInfo().getPath()));
					}

				}
			}
		}

	}

	private MPart createPart(final EPartService partService, final MApplication application, final EModelService modelService) {
		MPart part = MBasicFactory.INSTANCE.createPart();

		part.setElementId("fr.ifremer.globe.ui.parts.texteditor.partId");

		part.setCloseable(true);
		part.setContributionURI("bundleclass://fr.ifremer.globe.ui/fr.ifremer.globe.ui.parts.TextEditorView");

		PartManager.addPartToStack(part, application, modelService, PartManager.RIGHT_STACK_ID);
		PartManager.showPart(partService, part);
		return part;
	}

}
