package fr.ifremer.globe.ui.projectexplorer.handlers;

import java.util.LinkedList;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.StructuredSelection;

import fr.ifremer.globe.ui.projectexplorer.view.ProjectExplorer;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;

public class ExpandAllHandler {
	@CanExecute
	public boolean canExecute(EPartService partService, ESelectionService selectionService) {
		MPart part = partService.getActivePart();
		Object explorer = part.getObject();
		if (explorer instanceof ProjectExplorer) {
			return !computeNodeList(partService, selectionService).isEmpty();
		}
		return false;
	}

	@Execute
	public void execute(EPartService partService, ESelectionService selectionService) {
		MPart part = partService.getActivePart();
		Object explorer = part.getObject();
		if (explorer instanceof ProjectExplorer) {
			((ProjectExplorer) explorer).expandAll(computeNodeList(partService, selectionService));
		}
	}

	protected LinkedList<TreeNode> computeNodeList(EPartService partService, ESelectionService selectionService) {

		Object obj = selectionService.getSelection();
		LinkedList<TreeNode> nodelist = new LinkedList<TreeNode>();
		if (obj instanceof StructuredSelection) {
			Object[] elements = ((StructuredSelection) obj).toArray();
			for (Object target : elements) {
				if (target instanceof TreeNode) {
					nodelist.add((TreeNode) target);
				}
			}
		}
		return nodelist;

	}
}
