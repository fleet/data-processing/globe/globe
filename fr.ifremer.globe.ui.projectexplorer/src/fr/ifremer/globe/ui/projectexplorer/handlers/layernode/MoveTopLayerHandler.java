package fr.ifremer.globe.ui.projectexplorer.handlers.layernode;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.ElevationNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;

public class MoveTopLayerHandler extends AbstractNodeHandler {

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return !MoveDownLayerHandler.getNodeToMove(selectionService).isEmpty();
	}

	@Execute
	public void execute(ESelectionService selectionService, IGeographicViewService geographicViewService) {
		MoveDownLayerHandler.getNodeToMove(selectionService).forEach(treeNode -> treeNode.traverseDown(node -> {
			if (node instanceof LayerNode layerNode && layerNode.getLayer().isPresent()
					&& layerNode.getLayer().get() instanceof IWWLayer layer)
				geographicViewService.moveLayerToFront(layer);
			else if (node instanceof ElevationNode elevationNode)
				geographicViewService.moveElevationToFront(elevationNode.getElevationModel());
		}));
		geographicViewService.refresh();
	}

}
