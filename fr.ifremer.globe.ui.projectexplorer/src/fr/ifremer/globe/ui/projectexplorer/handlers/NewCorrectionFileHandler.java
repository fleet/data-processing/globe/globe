/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package fr.ifremer.globe.ui.projectexplorer.handlers;

import java.io.IOException;

import jakarta.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.processes.biascorrection.filereaders.CorrectionFileUtils;
import fr.ifremer.globe.ui.projectexplorer.wizard.process.biascorrection.dialog.CorrectionFileCreationDialog;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;

public class NewCorrectionFileHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(NewCorrectionFileHandler.class);

	@CanExecute
	public boolean canExecute(@Named(IServiceConstants.ACTIVE_SELECTION) StructuredSelection structuredSelection) {
		return structuredSelection.size() == 1 && structuredSelection.getFirstElement() instanceof FolderNode;
	}

	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell,
			@Named(IServiceConstants.ACTIVE_SELECTION) StructuredSelection structuredSelection) {

		CorrectionFileCreationDialog creationDialogdialog = new CorrectionFileCreationDialog(shell);
		if (creationDialogdialog.open() == Window.OK) {
			FileDialog dialog = new FileDialog(shell, SWT.SAVE);
			dialog.setFilterNames(new String[] { "Correction file (" + CorrectionFileUtils.EXTENSION + ")" });
			dialog.setFilterExtensions(new String[] { CorrectionFileUtils.EXTENSION });
			String filePath = dialog.open();
			if (filePath != null) {
				try {
					CorrectionFileUtils.write(filePath, creationDialogdialog.getCorrectionList());
					MessageDialog.openInformation(shell, "New correction file",
							"Correction file created : " + filePath);
				} catch (IOException e) {
					Messages.openErrorMessage("Correction file export", "Export failed : " + e.getMessage());
					LOGGER.error("Correction file export failed: " + e.getMessage(), e);
				}
			}
		}
	}
}
