package fr.ifremer.globe.ui.projectexplorer.handlers.fileinfonode;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.utils.SelectionUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;

public class ActivateXsfFlattenWCHandler extends AbstractNodeHandler {

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		return !getTextureLayerToBuild(selectionService).isEmpty();
	}

	@Execute
	public void execute(ESelectionService selectionService) {
		getTextureLayerToBuild(selectionService).forEach(IWWTextureLayer::buildTextureRenderable);
	}

	private List<IWWTextureLayer> getTextureLayerToBuild(ESelectionService selectionService) {
		var result = new ArrayList<IWWTextureLayer>();
		SelectionUtils
				.getSelection(selectionService, GroupNode.class,
						groupNode -> groupNode instanceof FileInfoNode || groupNode instanceof FolderNode)
				.forEach(selectedNode -> {
					selectedNode.traverseDown(node -> {
						if (node instanceof LayerNode layerNode && layerNode.getLayer().isPresent()
								&& layerNode.getLayer().get() instanceof IWWTextureLayer wWTextureLayer
								&& wWTextureLayer.getName().contains("_flatten"))
							result.add(wWTextureLayer);
					});
				});
		return result;
	}

}
