package fr.ifremer.globe.ui.projectexplorer.providers;

import java.util.regex.Pattern;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import fr.ifremer.globe.ui.projectexplorer.search.LayerSearchTool;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;

public class TreeNodeViewFilter extends ViewerFilter {
	String regexp = null;

	public void setFilterRegex(String value) {
		if (value == null || value == "") {
			resetFilter();
		} else {
			regexp = "*" + value + "*";
			regexp = regexp.replace("*", ".*"); // any string
			regexp = regexp.replace("?", ".?"); // any character
		}
	}

	public void resetFilter() {
		regexp = null;
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		// early rejection
		if (regexp == null) {
			return true;
		}
		if (element instanceof TreeNode node) {
			TreeNode root = node.findRootNode();
			// Filter only apply to node attached to a WMSGroupNode or
			// DataGroupNode
			if (root == null) {
				return true;
			}
			if (root instanceof GroupNode) {
				// now check hierarchy to see if node if filtered or not
				return LayerSearchTool.getInverse() ? isFiltered(node) : isExfiltered(node);
			}

			// else no filter
			return true;
		} else {
			// unknown element
			return true;
		}
	}

	private boolean isFiltered(TreeNode node) {

		Pattern p = null;

		p = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE | Pattern.COMMENTS | Pattern.MULTILINE | Pattern.DOTALL);
		return recurseDownMatch(node, p) || recurseUpMatch(node, p);

		// go down hierarchy and keep only node where one of the child match the
		// regexp
	}

	// check if one of the child match
	private boolean recurseDownMatch(TreeNode node, Pattern pattern) {
		// check if the given node match
		boolean match = pattern.matcher(node.getName()).matches();
		if (match) {
			return true;

		} else {
			// recurse to see if one of child node match
			if (node instanceof GroupNode) {
				for (TreeNode child : ((GroupNode) node).getChildren()) {
					match = recurseDownMatch(child, pattern);
					if (match) {
						return true;
					}

				}
			}

		}
		return false;

	}

	// check if one of the father match
	private boolean recurseUpMatch(TreeNode node, Pattern pattern) {
		// early rejection, we do not test on root node (if so everything is
		// displayed)
		if (node instanceof GroupNode && node.getParent().isEmpty() || node == null) {
			return false;
		}

		// check if the given node match
		boolean match = pattern.matcher(node.getName()).matches();
		return match || node.getParent().map(p -> recurseUpMatch(p, pattern)).orElse(false);
	}

	// *****************************************
	// ajout fonction pour négation du filtre
	// ****************************************

	private boolean isExfiltered(TreeNode node) {

		Pattern p = null;

		p = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE | Pattern.COMMENTS | Pattern.MULTILINE | Pattern.DOTALL);
		return recurseDownNotMatch(node, p) || recurseUpNotMatch(node, p);

		// go down hierarchy and keep only node where one of the child match the
		// regexp
	}

	// check if one of the child match
	private boolean recurseDownNotMatch(TreeNode node, Pattern pattern) {
		// check if the given node match
		boolean match = pattern.matcher(node.getName()).matches();
		if (match) {
			return false;
		} else {
			// recurse to see if one of child node match
			if (node instanceof GroupNode) {
				for (TreeNode child : ((GroupNode) node).getChildren()) {
					match = recurseDownMatch(child, pattern);
					if (match) {

						return false;
					}
				}

			}

		}
		return true;

	}

	// check if one of the father match
	private boolean recurseUpNotMatch(TreeNode node, Pattern pattern) {
		// early rejection, we do not test on root node (if so everything is
		// displayed)
		if (node instanceof GroupNode && node.getParent().isEmpty() || node == null) {
			return true;
		}

		// check if the given node match
		boolean match = pattern.matcher(node.getName()).matches();
		return !(match || node.getParent().map(p -> recurseUpMatch(p, pattern)).orElse(false));

	}

}
