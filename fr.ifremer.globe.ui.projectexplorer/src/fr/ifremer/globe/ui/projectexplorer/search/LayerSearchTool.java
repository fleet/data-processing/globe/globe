package fr.ifremer.globe.ui.projectexplorer.search;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.ui.projectexplorer.view.ProjectExplorer;
import fr.ifremer.globe.ui.utils.image.ImageResources;

public class LayerSearchTool {

	// FT5455 restore tree state
	/** The arrays of expanded elements of treeViewer. */
	private static Object[] expandedElementsOfTreeViewer;
	private static boolean saveTreeState = true;
	private static boolean inverse = true;

	/**
	 * Create the widgets of the layer search tool.
	 *
	 * @param parent
	 * @param explorer
	 */

	public static void createSearchTool(Composite parent, final ProjectExplorer explorer) {

		Composite searchComposite = new Composite(parent, SWT.NONE);
		searchComposite.setLayout(new GridLayout(3, false));
		searchComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		// Input Area
		final Text inputText = new Text(searchComposite, SWT.BORDER | SWT.SINGLE);
		inputText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		inputText.setText("");

		// Reset Button
		final Button resetButton = new Button(searchComposite, SWT.NONE);
		resetButton.setLayoutData(new GridData(SWT.END, SWT.FILL, false, false));
		resetButton.setToolTipText("Search");
		resetButton.setImage(ImageResources.getImage("/icons/16/filefind.png", new LayerSearchTool().getClass()));

		// Inverse Button
		final Button inverseButton = new Button(searchComposite, SWT.NONE);
		inverseButton.setLayoutData(new GridData(SWT.END, SWT.FILL, false, false));
		inverseButton.setToolTipText("negate search");
		inverseButton.setImage(ImageResources.getImage("/icons/16/egal.png", new LayerSearchTool().getClass()));

		// action on input area
		inputText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {

				String regex = inputText.getText();

				// FT5455 restore tree state
				if (saveTreeState && regex.length() == 1) {
					// user begin to enter a new search string (saveTreeState =
					// true).
					expandedElementsOfTreeViewer = explorer.getExpandedElements();
				}

				LayerSearchTool.search(explorer, regex);

				if (regex.length() == 0) {
					// Set the tree in her initial state (before entering a new
					// search string)
					explorer.expandElementsOfTree(expandedElementsOfTreeViewer);

					// Search string empty, enable tree state saving for the
					// next search string
					saveTreeState = true;
				}

				if (regex.length() > 1) {
					// Search string in edition, disable tree state saving
					saveTreeState = false;
				}

				if (!regex.equals("")) {
					resetButton.setToolTipText("Reset Search");
					resetButton.setImage(
							ImageResources.getImage("/icons/16/edit-delete.png", new LayerSearchTool().getClass()));
					resetButton.setEnabled(true);
					inputText.forceFocus();

				} else {
					resetButton.setToolTipText("Search");
					resetButton.setImage(
							ImageResources.getImage("/icons/16/filefind.png", new LayerSearchTool().getClass()));
				}

			}

		});

		// action on reset button
		resetButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				inputText.setText("");
				inputText.forceFocus();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// pour inverser

		// action on inverse button
		inverseButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (inverse == true) {
					inverse = false;
					inverseButton.setToolTipText("Reverse Search");
					inverseButton.setImage(
							ImageResources.getImage("/icons/16/reverse.png", new LayerSearchTool().getClass()));
					inverseButton.setEnabled(true);
					inputText.forceFocus();
					LayerSearchTool.search(explorer, inputText.getText());

				} else {

					inverse = true;
					inverseButton.setToolTipText("Reverse Search");
					inverseButton
							.setImage(ImageResources.getImage("/icons/16/egal.png", new LayerSearchTool().getClass()));
					inverseButton.setEnabled(true);
					inputText.forceFocus();
					LayerSearchTool.search(explorer, inputText.getText());

				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

	}

	/**
	 * Search a regex in a IExplorer
	 *
	 * @param explorer
	 * @param regex The regular expression to use
	 */
	private static void search(ProjectExplorer explorer, String regex) {
		explorer.updateFilter(regex);
		explorer.expandAll();
	}

	public static boolean getInverse() {
		return inverse;
	}

}
