package fr.ifremer.globe.ui.projectexplorer.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.dtm.cdi.DtmSourceId;
import fr.ifremer.globe.core.model.dtm.cdi.DtmSourceIdBuilder;
import fr.ifremer.globe.core.model.dtm.cdi.DtmSourceMetadata;
import fr.ifremer.globe.core.model.dtm.cdi.DtmSourceState;
import fr.ifremer.globe.core.model.file.ICdiService;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.projectexplorer.wizard.organizedtm.OrganizeDTMsPage.OrganizationMode;
import fr.ifremer.globe.ui.projectexplorer.wizard.organizedtm.OrganizeDTMsPage.OrganizeDTMParameter;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode.LoadedState;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;

/**
 * Auto organize DTM and put the in group per QI
 */
public class DtmAutoOrganizer {
	protected static Logger logger = LoggerFactory.getLogger(DtmAutoOrganizer.class);

	static final String MIXED_QI_NAME = "Multiple QI";
	static final String NOT_FOUND_NODE_NAME = "NotFound";

	class DtmFileInfo {
		private FileInfoNode projectExplorerNode;
		private List<String> cdis;
		private DtmSourceMetadata metaData;

		/** Constructor **/
		public DtmFileInfo(FileInfoNode node, List<String> cdis) {
			projectExplorerNode = node;
			this.cdis = cdis;
		}

		public FileInfoNode getProjectExplorerNode() {
			return projectExplorerNode;
		}

		public DtmSourceMetadata getMetaData() {
			return metaData;
		}

		public Integer getQIAge() {
			if (metaData == null || metaData.qualityIndicator() == null) {
				return null;
			} else {
				return metaData.qualityIndicator().getAge();
			}
		}

		public String getQISurveyType() {
			if (metaData == null || metaData.qualityIndicator() == null) {
				return null;
			} else {
				return Integer.toString(metaData.qualityIndicator().getSurveyType());
			}
		}

		public String getQIHorizAccuracy() {
			if (metaData == null || metaData.qualityIndicator() == null) {
				return null;
			} else {
				return Integer.toString(metaData.qualityIndicator().getHorizontalAccuracy());
			}
		}

		public String getQIVertAccuracy() {
			if (metaData == null || metaData.qualityIndicator() == null) {
				return null;
			} else {
				return Integer.toString(metaData.qualityIndicator().getVerticalAccuracy());
			}
		}

		public int getProviderId() {
			return metaData == null ? 0 : metaData.edmoCode();
		}

		/** Download meta data */
		public void retrieveMetaData() {
			DtmSourceId url = DtmSourceIdBuilder.build(cdis.get(0));
			metaData = url.retrieve();
		}

		/**
		 * Build the group name where the file should be moved (hierarchical organization)
		 */
		public String getGroupName(OrganizeDTMParameter param) {
			StringBuilder groupName = new StringBuilder();
			switch (param.getParamType()) {
			case AGE:
				List<Pair<Integer, Integer>> ageRanges = param.getAgeRanges();
				if (!ageRanges.isEmpty() && getQIAge() != null) {
					groupName.append(" Age: ");
					int age = Calendar.getInstance().get(Calendar.YEAR) - getQIAge();
					for (Pair<Integer, Integer> range : ageRanges) {
						if (age >= range.getFirst() && age <= range.getSecond()) {
							groupName.append("[" + range.getFirst() + "-" + range.getSecond() + "]");
						}
					}
				}
				break;
			case HORIZONTAL_ACCURACY:
				groupName.append(" Horiz: " + getQIHorizAccuracy());
				break;
			case VERTICAL_ACCURACY:
				groupName.append(" Vert: " + getQIVertAccuracy());
				break;
			case SURVEY_TYPE:
				groupName.append(" Type: " + getQISurveyType());
				break;
			case PROVIDER_ID:
				groupName.append(" Provider: " + getProviderId());
				break;
			default:
				break;
			}
			return groupName.toString();
		}

		/**
		 * Build the group name where the file should be moved (plan organization)
		 */
		public String getGroupName() {
			StringBuilder groupName = new StringBuilder();
			organizationParams.forEach(p -> groupName.append(getGroupName(p)));
			return groupName.toString();
		}
	}

	private GroupNode rootNode;
	private List<OrganizeDTMParameter> organizationParams;
	private OrganizationMode mode;
	private boolean deleteEmptyGroups;

	public DtmAutoOrganizer(GroupNode root, List<OrganizeDTMParameter> p, OrganizationMode m,
			boolean deleteEmptyGroups) {
		rootNode = root;
		organizationParams = p;
		mode = m;
		this.deleteEmptyGroups = deleteEmptyGroups;
	}

	/**
	 * Check all loaded files and organize them per QI
	 */
	public void run(IProgressMonitor monitor) {

		if (mode == null || organizationParams == null) {
			return;
		}

		List<DtmFileInfo> oneCDIList = new ArrayList<>();
		List<DtmFileInfo> multipleCDIList = new ArrayList<>();

		// chek all node and retains only those corresponding to a dtm
		rootNode.traverseDown(node -> {
			if (node instanceof FileInfoNode infostorenode) {
				// only loaded nodes
				if (infostorenode.getLoadedState() != LoadedState.LOADED) {
					return;
				}

				IFileInfo store = infostorenode.getFileInfo();
				if (store == null) {
					// reject
					return;
				}
				if (!store.getContentType().isDtm()) {
					// reject
					return;
				}
				// no cdi
				List<String> cdis = ICdiService.grab().getCdis(store);
				if (cdis.isEmpty()) {
					return;
				}
				if (cdis.size() > 1) {
					multipleCDIList.add(new DtmFileInfo(infostorenode, cdis));
				} else {
					oneCDIList.add(new DtmFileInfo(infostorenode, cdis));
				}
			}
		});

		monitor.beginTask("Retrieving CDI", multipleCDIList.size() + oneCDIList.size() + 1);

		// retrieve all metadata
		oneCDIList.forEach(f -> {
			f.retrieveMetaData();
			monitor.worked(1);
		});

		if (!multipleCDIList.isEmpty()) {
			// organize data
			FolderNode parent = findOrCreate(MIXED_QI_NAME, MIXED_QI_NAME, rootNode);
			multipleCDIList.forEach(f -> {
				f.getProjectExplorerNode().getParent().get().removeChild(f.getProjectExplorerNode());
				logger.info("Adding node {} to {}", f.getProjectExplorerNode().getName(), parent.getName());
				parent.addChild(f.getProjectExplorerNode());
				monitor.worked(1);
			});
		}

		if (!oneCDIList.isEmpty()) {
			if (mode == OrganizationMode.FLAT) {
				organizeNodeFlatMode(rootNode, oneCDIList);
			} else if (mode == OrganizationMode.HIERARCHICAL) {
				organizeNodeHierarchicalMode(rootNode, 0, oneCDIList);
			}
		}

		if (deleteEmptyGroups) {
			deleteEmptyNodes(rootNode);
		}

		monitor.worked(1);
	}

	/**
	 * Organize files in flat mode
	 */
	private void organizeNodeFlatMode(GroupNode rootNode, List<DtmFileInfo> files) {

		SortedMap<String, List<FileInfoNode>> foundNodes = new TreeMap<>();
		files.forEach(f -> {
			f.getProjectExplorerNode().getParent().get().removeChild(f.getProjectExplorerNode());
			String groupName = NOT_FOUND_NODE_NAME;
			if (f.getMetaData().state() == DtmSourceState.eExists) {
				groupName = f.getGroupName();
			}
			List<FileInfoNode> list = foundNodes.get(groupName);
			if (list == null) {
				list = new ArrayList<>();
				foundNodes.put(groupName, list);
			}
			list.add(f.getProjectExplorerNode());
		});
		// now add all row per alphabetic order, if already exist, we does
		// not change the order
		for (Entry<String, List<FileInfoNode>> e : foundNodes.entrySet()) {

			FolderNode parent = findOrCreate(e.getKey(), e.getKey(), rootNode);
			e.getValue().forEach(node -> {
				logger.info("Adding node {} to {}", node.getName(), parent.getName());
				parent.addChild(node);
			});
		}

	}

	/**
	 * Organize files in hierarchical mode
	 */
	private void organizeNodeHierarchicalMode(GroupNode rootNode, int paramIndex, List<DtmFileInfo> files) {
		if (paramIndex >= organizationParams.size()) {
			return;
		}

		SortedMap<String, List<DtmFileInfo>> foundNodes = new TreeMap<>();

		files.forEach(f -> {
			f.getProjectExplorerNode().getParent().get().removeChild(f.getProjectExplorerNode());
			String groupName = NOT_FOUND_NODE_NAME;
			if (f.getMetaData().state() == DtmSourceState.eExists) {
				groupName = f.getGroupName(organizationParams.get(paramIndex));
			}

			List<DtmFileInfo> list = foundNodes.get(groupName);
			if (list == null) {
				list = new ArrayList<>();
				foundNodes.put(groupName, list);
			}
			list.add(f);
		});
		// now add all row per alphabetic order, if already exist, we does
		// not change the order
		for (Entry<String, List<DtmFileInfo>> e : foundNodes.entrySet()) {

			FolderNode parent = findOrCreate(e.getKey(), e.getKey(), rootNode);
			e.getValue().forEach(node -> {
				logger.info("Adding node {} to {}", node.getProjectExplorerNode().getName(), parent.getName());
				parent.addChild(node.getProjectExplorerNode());
			});
			organizeNodeHierarchicalMode(parent, paramIndex + 1, e.getValue());
		}
	}

	/**
	 * find a node with the same name, if not exist create it under parent node
	 */
	private FolderNode findOrCreate(String name, String comments, GroupNode rootNode) {
		List<FolderNode> found = new ArrayList<>();

		/**
		 * retrieve all data group node with the same name
		 */
		rootNode.getChildren().forEach(node -> {
			if (node instanceof FolderNode && node.getName().compareTo(name) == 0) {
				found.add((FolderNode) node);
			}
		});

		if (!found.isEmpty()) {
			return found.get(0);
		} else

		{
			FolderNode node = new FolderNode(name, comments);
			rootNode.addChild(node);
			return node;
		}
	}

	/**
	 * Traverse down specified node and remove them if empty.
	 */
	private void deleteEmptyNodes(GroupNode rootNode) {

		for (int i = rootNode.getChildren().size() - 1; i >= 0; i--) {
			if (rootNode.getChildren().get(i) instanceof FolderNode) {
				deleteEmptyNodes((FolderNode) rootNode.getChildren().get(i));
			}
		}

		if (rootNode instanceof FolderNode && ((FolderNode) rootNode).getChildren().isEmpty()) {
			rootNode.getParent().get().removeChild(rootNode);
		}
	}
}
