package fr.ifremer.globe.ui.projectexplorer.dnd;

public class NodeConverter {

	public static final String STARTER = "#!!#";
	public static final String SEPARATOR = "#";

	/** Private constructor **/
	private NodeConverter() {
		// utility class
	}

	/**
	 * Converts a String to list of node UUIDs (universally unique IDs) or return empty
	 */
	public static String[] convert(String input) {
		return input.startsWith(NodeConverter.STARTER)
				? input.replace(NodeConverter.STARTER, "").split(NodeConverter.SEPARATOR)
				: new String[] {};
	}
}
