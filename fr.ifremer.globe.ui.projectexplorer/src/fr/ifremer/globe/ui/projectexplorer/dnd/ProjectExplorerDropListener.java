package fr.ifremer.globe.ui.projectexplorer.dnd;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.TransferData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.ui.service.file.IFileLoadingService;
import fr.ifremer.globe.ui.service.projectexplorer.ITreeNodeFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.GroupNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.LayerNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;

public class ProjectExplorerDropListener extends ViewerDropAdapter {

	private Logger logger = LoggerFactory.getLogger(ProjectExplorerDropListener.class);

	private final ProjectExplorerModel projectExplorerModel;

	public ProjectExplorerDropListener(Viewer viewer, ProjectExplorerModel projectExplorerModel) {
		super(viewer);
		this.projectExplorerModel = projectExplorerModel;
	}

	@Override
	public boolean performDrop(Object eventData) {
		// Nodes
		TreeNode target = getCurrentTarget() instanceof TreeNode treeNode ? treeNode
				: projectExplorerModel.getDataNode();
		ArrayList<GroupNode> retainednodes = new ArrayList<>();
		String[] uuids = null;
		if (eventData instanceof String eventDataString) {
			uuids = NodeConverter.convert(eventDataString);
		}

		// Case of internal drop into Layer Explorer view part.
		if (uuids != null) {
			for (String uuid : uuids) {
				if (!uuid.isEmpty()) {
					try {
						var optGroupNode = projectExplorerModel.getNode(UUID.fromString(uuid));
						if (optGroupNode.isPresent() && optGroupNode.get() instanceof GroupNode groupNode)
							retainednodes.add(groupNode);
					} catch (IllegalArgumentException e) {
						logger.debug("Illegal drag and drop (from Layer List to Project Explorer view part) : "
								+ e.getMessage(), e);
					}
				}
			}
		} else {
			String[] fileList = null;
			final FileTransfer fileTransfer = FileTransfer.getInstance();
			if (fileTransfer.isSupportedType(getCurrentEvent().currentDataType)) {
				fileList = (String[]) eventData;
			}
			if (fileList != null) {
				importFiles(target, fileList);
			}

			return true;
		}

		if (retainednodes.size() == 0) {
			// cannot drop between different root nodes
			return false;
		}

		String translatedLocation = "";
		switch (getCurrentLocation()) {
		case ViewerDropAdapter.LOCATION_BEFORE:
			translatedLocation = "Dropped " + eventData + " before the target " + target.getName();
			projectExplorerModel.moveBeforeAfterNode(retainednodes, target, true);
			break;
		case ViewerDropAdapter.LOCATION_AFTER:
			translatedLocation = "Dropped " + eventData + " after the target " + target.getName();
			projectExplorerModel.moveBeforeAfterNode(retainednodes, target, false);
			break;
		case ViewerDropAdapter.LOCATION_ON:
			if (target instanceof GroupNode groupNode) {
				translatedLocation = "Dropped " + eventData + " on the target " + target.getName();
				projectExplorerModel.moveNodes(retainednodes, groupNode, false);
			}
			break;
		case ViewerDropAdapter.LOCATION_NONE:
			translatedLocation = "Dropped " + eventData + " into nothing";
			target = null;
			break;
		}

		List<IWWNavigationLayer> layersToUpdate = new ArrayList<>();

		if (target instanceof FolderNode) {
			for (GroupNode groupNode : retainednodes) {
				// Get the navigation layers from different types of nodes and put them to the output list
				layersToUpdate.addAll(getChildrenNavigationLayersRecursively(groupNode));
				// TODO useful ??
			}
		}

		logger.debug(translatedLocation);
		return true;
	}

	/** Launch an event for each file to load */
	protected void importFiles(TreeNode target, String[] fileList) {
		Optional<GroupNode> optGroupNode = target instanceof GroupNode ? Optional.of((GroupNode) target)
				: Optional.empty();
		// reference files
		ITreeNodeFactory.grab().createFileInfoNode(List.of(fileList), optGroupNode);
		IFileLoadingService.grab().load(fileList);
	}

	/**
	 * Get recursively all the navigation layers from an {@link FolderNode}.
	 *
	 * @param folderNode {@link FolderNode} where to find the navigation layers
	 * @return a list of the navigation layers found recursively
	 */
	private List<IWWNavigationLayer> getChildrenNavigationLayersRecursively(TreeNode node) {
		List<IWWNavigationLayer> navigationLayers = new ArrayList<>();
		node.traverseDown(childNode -> {
			if (childNode instanceof LayerNode layerNode && layerNode.getLayer().isPresent()
					&& layerNode.getLayer().get() instanceof IWWNavigationLayer navigationLayer)
				navigationLayers.add(navigationLayer);
		});
		return navigationLayers;
	}

	@Override
	public boolean validateDrop(Object target, int operation, TransferData transferType) {
		// Anywhere in the project explorer ?
		if (target == null) {
			return true;
		}

		if (target instanceof GroupNode targetGroupNode && !(target instanceof FileInfoNode)
				&& getSelectedObject() instanceof GroupNode selectedGroupNode) {
			// cannot drop between different root nodes
			return selectedGroupNode.findRootNode() == targetGroupNode.findRootNode();
		}

		// we can DND FileInfoNode near a target (but not on)
		if (target instanceof FileInfoNode && getCurrentLocation() != LOCATION_ON) {
			return true;
		}

		// Case for external drag on a loaded infostore node
		if (target instanceof FolderNode && getCurrentLocation() == LOCATION_ON) {
			return true;
		}
		return false;
	}

	@Override
	public void dropAccept(DropTargetEvent event) {
		if (event.data == null && event.dataTypes == null) {
			event.detail = DND.DROP_NONE;
		}
	}

	/** {@inheritDoc} */
	@Override
	protected CheckboxTreeViewer getViewer() {
		return (CheckboxTreeViewer) super.getViewer();
	}

}
