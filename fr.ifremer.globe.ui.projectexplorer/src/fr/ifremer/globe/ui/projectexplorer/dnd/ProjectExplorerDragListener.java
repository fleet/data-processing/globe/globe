package fr.ifremer.globe.ui.projectexplorer.dnd;

import java.util.List;

import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.TextTransfer;

import fr.ifremer.globe.ui.utils.SelectionUtils;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FolderNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;

public class ProjectExplorerDragListener implements DragSourceListener {

	private final Viewer viewer;

	public ProjectExplorerDragListener(Viewer viewer) {
		this.viewer = viewer;
	}

	private List<TreeNode> getSelection() {
		return SelectionUtils.getSelection((StructuredSelection) viewer.getSelection(), TreeNode.class,
				node -> node instanceof FileInfoNode || (node instanceof FolderNode && node.getParent().isPresent()));
	}

	@Override
	public void dragSetData(DragSourceEvent event) {
		var selection = getSelection();
		if (selection.isEmpty()) {
			event.detail = DND.DROP_NONE;
		} else if (event.dataType != null && TextTransfer.getInstance().isSupportedType(event.dataType)) {
			var listOfData = new StringBuilder(NodeConverter.STARTER);
			selection.forEach(groupNode -> listOfData.append(groupNode.getUid().toString() + NodeConverter.SEPARATOR));
			event.data = listOfData.toString();
		}
	}

	@Override
	public void dragStart(DragSourceEvent event) {
		var selection = getSelection();
		event.doit = !selection.isEmpty();
		if (!event.doit)
			event.detail = DND.DROP_NONE;
	}

	@Override
	public void dragFinished(DragSourceEvent event) {
		/** nothing to do **/
	}

}
