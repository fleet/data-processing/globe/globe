/**
 * GLOBE - Ifremer
 */

package fr.ifremer.globe.ui.projectexplorer.service.impl;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.ui.service.wizard.IWizardBuilder;
import fr.ifremer.globe.ui.service.wizard.IWizardService;
import fr.ifremer.globe.ui.wizard.generic.WizardBuilder;

/**
 * Implementation of wizard services
 */
@Component(name = "globe_viewer3d_wizard_service", service = IWizardService.class)
public class WizardService implements IWizardService {

	@Override
	public IWizardBuilder newWizardBuilder() {
		return new WizardBuilder();
	}

}
