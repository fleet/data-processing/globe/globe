package fr.ifremer.globe.ui.projectexplorer.view;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;

import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.ITreeViewerListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.TreeExpansionEvent;
import org.eclipse.jface.viewers.TreeViewerEditor;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import fr.ifremer.globe.ui.projectexplorer.dnd.ProjectExplorerDragListener;
import fr.ifremer.globe.ui.projectexplorer.dnd.ProjectExplorerDropListener;
import fr.ifremer.globe.ui.projectexplorer.providers.TreeNodeViewFilter;
import fr.ifremer.globe.ui.projectexplorer.search.LayerSearchTool;
import fr.ifremer.globe.ui.providers.LayerCheckProvider;
import fr.ifremer.globe.ui.utils.UIUtils;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel;
import fr.ifremer.globe.ui.views.projectexplorer.ProjectExplorerModel.ProjectExplorerEvent;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNode;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNodeContentProvider;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.TreeNodeLabelProvider;

/**
 * Project explorer view part.
 */
public class ProjectExplorer {

	public static final String PART_ID = ProjectExplorerModel.PROJECT_EXPLORER_PART_ID;

	@Inject
	private ESelectionService selectionService;

	@Inject
	private ProjectExplorerModel model;

	/** The tree layer viewer explorer. */
	private CheckboxTreeViewer treeViewer;

	private TreeNodeViewFilter nodeFilter;

	private final ISelectionChangedListener selectionChangedListener = this::onNodeSelected;

	@PostConstruct
	public void createUI(Composite parent, EMenuService menuService) {
		GridLayout layout = new GridLayout(1, false);
		layout.horizontalSpacing = 0;
		layout.verticalSpacing = 0;
		layout.marginHeight = 0;
		layout.marginWidth = 0;

		Composite c = new Composite(parent, SWT.NONE);
		c.setLayout(layout);
		c.setLayoutData(new GridData(GridData.FILL_BOTH));

		nodeFilter = new TreeNodeViewFilter();
		LayerSearchTool.createSearchTool(c, this);

		// Tree Viewer
		treeViewer = new CheckboxTreeViewer(c, SWT.BOTTOM | SWT.MULTI | SWT.VIRTUAL);
		treeViewer.getControl().setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1));

		treeViewer.setCheckStateProvider(new LayerCheckProvider());
		treeViewer.setUseHashlookup(true);

		int operations = DND.DROP_COPY | DND.DROP_MOVE;
		Transfer[] transferTypes = new Transfer[] { TextTransfer.getInstance(), FileTransfer.getInstance() };
		treeViewer.addDragSupport(operations, transferTypes, new ProjectExplorerDragListener(treeViewer));
		treeViewer.addDropSupport(operations, transferTypes, new ProjectExplorerDropListener(treeViewer, model));

		treeViewer.setLabelProvider(new TreeNodeLabelProvider());
		treeViewer.setContentProvider(new TreeNodeContentProvider());
		treeViewer.setCellEditors(new CellEditor[] { new TextCellEditor(treeViewer.getTree()) });
		treeViewer.setColumnProperties(new String[] { "name", "value" });
		ColumnViewerToolTipSupport.enableFor(treeViewer);

		TreeViewerEditor.create(treeViewer, new ColumnViewerEditorActivationStrategy(treeViewer) {
			@Override
			protected boolean isEditorActivationEvent(ColumnViewerEditorActivationEvent event) {
				return event.eventType == ColumnViewerEditorActivationEvent.PROGRAMMATIC;
			}
		}, ColumnViewerEditor.DEFAULT);

		treeViewer.addSelectionChangedListener(selectionChangedListener);

		// Manage the expand status
		treeViewer.addTreeListener(new ITreeViewerListener() {
			@Override
			public void treeCollapsed(TreeExpansionEvent event) {
				if (event.getElement() instanceof TreeNode node)
					node.setExpanded(false);
			}

			@Override
			public void treeExpanded(TreeExpansionEvent event) {
				if (event.getElement() instanceof TreeNode node)
					node.setExpanded(true);
			}
		});
		ViewerFilter[] filters = { nodeFilter };
		treeViewer.setFilters(filters);
		treeViewer.addCheckStateListener(this::onNodeChecked);

		// popup menu for tree viewer
		menuService.registerContextMenu(treeViewer.getControl(), "fr.ifremer.globe.ui.projectexplorer.popupmenu");

		// initialize controller
		Consumer<ProjectExplorerEvent> modelListener = this::onProjectExplorerEvent;
		model.addListener(modelListener);
		parent.addDisposeListener(evt -> model.removeListener(modelListener));
		refresh();
	}

	// ***
	// *** EVENT HANDLERS
	// ***

	private void onNodeSelected(SelectionChangedEvent event) {
		var selection = event.getSelection();
		// if selection is empty, force updating other views by refreshing the selection
		selectionService.setSelection(selection.isEmpty() ? null : selection);
	}

	private void onNodeChecked(CheckStateChangedEvent event) {
		if (event.getElement() instanceof TreeNode checkedNode) {
			setNodeChecked(checkedNode, event.getChecked());
			// if multiple selection : check other nodes
			if (treeViewer.getStructuredSelection().size() > 1
					&& treeViewer.getStructuredSelection().toList().contains(checkedNode))
				for (var selection : treeViewer.getStructuredSelection())
					if (selection != event.getElement() && selection instanceof TreeNode node)
						setNodeChecked(node, event.getChecked());
		}
	}

	private void setNodeChecked(TreeNode node, boolean checked) {
		node.setChecked(checked, true, Optional.of(n -> nodeFilter.select(treeViewer, n.getParent(), n)));
		node.traverseUpAndDown(n -> treeViewer.update(n, null));
	}

	/**
	 * Reacts on {@link ProjectExplorerEvent}
	 */
	private void onProjectExplorerEvent(ProjectExplorerEvent event) {
		switch (event.type()) {
		case ADD -> refresh();
		case UPDATE -> {
			if (event.element().isEmpty())
				refresh();
			else
				event.element().forEach(this::refresh);
		}
		case SELECTION -> setSelection(event.element());
		case REMOVE -> refresh();
		}
	}

	public void setSelection(List<TreeNode> nodes) {
		runSafety(() -> {
			nodes.forEach(node -> {
				if (node.isExpanded())
					treeViewer.expandToLevel(node, AbstractTreeViewer.ALL_LEVELS);
			});
			treeViewer.setSelection(new StructuredSelection(nodes), true);
			treeViewer.refresh();
		});
	}

	public void reveal(TreeNode node, boolean hasToExpand, boolean hasToSelect) {
		if (node != null) {
			runSafety(() -> {
				node.getParent().ifPresent(treeViewer::refresh);
				if (hasToSelect)
					treeViewer.reveal(node);
				if (hasToExpand)
					treeViewer.expandToLevel(node, AbstractTreeViewer.ALL_LEVELS);
				if (hasToSelect) {
					treeViewer.getControl().setFocus(); // Must have focus to trigger SWT events
					treeViewer.setSelection(new StructuredSelection(node), true);
				}
			});
		}
	}

	/**
	 * Runs the {@link Runnable} only if tree viewer is not disposed, and in the display thread.
	 */
	private void runSafety(Runnable runnable) {
		UIUtils.asyncExecSafety(treeViewer, runnable);
	}

	/**
	 * Ask to refresh the layer explorer
	 */
	public void refresh() {
		runSafety(() -> {
			// reset input if necessary
			if (!Arrays.equals(((Object[]) treeViewer.getInput()), model.getRootNodes().toArray()))
				treeViewer.setInput(model.getRootNodes().toArray());

			treeViewer.getControl().setRedraw(false);
			treeViewer.setExpandedElements(model.getAll().stream().filter(TreeNode::isExpanded).toArray());
			treeViewer.refresh();
			treeViewer.getControl().setRedraw(true);
		});
	}

	/** Refresh one node */
	protected void refresh(TreeNode node) {
		runSafety(() -> {
			if (node.isExpanded())
				treeViewer.setExpandedState(node, true);
				//treeViewer.expandToLevel(node, AbstractTreeViewer.ALL_LEVELS);

			// if node was selected, select it again (to force the refreshing of other views)
			ITreeSelection actualSelection = treeViewer.getStructuredSelection();
			if (actualSelection.size() == 1 && treeViewer.getStructuredSelection().getFirstElement() == node)
				selectionService.setSelection(actualSelection);

			treeViewer.refresh(node.getParent().map(TreeNode.class::cast).orElse(node));
		});
	}

	/**
	 * Expand elements of new tree if they were expanded in the previous tree
	 */
	public void expandElementsOfTree(Object... elements) {
		runSafety(() -> treeViewer.setExpandedElements(elements));
	}

	public void updateFilter(String regex) {
		nodeFilter.setFilterRegex(regex);
		refresh();
	}

	/**
	 * Expands all ancestors of the given element or tree path so that the given element becomes visible in this
	 * viewer's tree control, and then expands the subtree rooted at the given element.
	 */
	public void collapseAll(List<TreeNode> nodelist) {
		if (nodelist != null)
			runSafety(() -> {
				for (TreeNode node : nodelist)
					treeViewer.collapseToLevel(node, AbstractTreeViewer.ALL_LEVELS);
			});
	}

	/**
	 * Expands all ancestors of the given element or tree path so that the given element becomes visible in this
	 * viewer's tree control, and then expands the subtree rooted at the given element.
	 */
	public void collapseOthers(List<TreeNode> nodelist) {
		runSafety(() -> {
			treeViewer.collapseAll();
			treeViewer.setSelection(new StructuredSelection(nodelist));
		});
	}

	/**
	 * Expands all ancestors of the given element or tree path so that the given element becomes visible in this
	 * viewer's tree control, and then expands the subtree rooted at the given element with one level depth.
	 */
	public void expandAllOneLevel(List<TreeNode> nodelist) {
		expandAll(nodelist, 1);
	}

	public void expandAll() {
		expandAll(List.of(model.getDataNode()));
	}

	/**
	 * Expands all ancestors of the given element or tree path so that the given element becomes visible in this
	 * viewer's tree control, and then expands the subtree rooted at the given element.
	 */
	public void expandAll(List<TreeNode> nodelist) {
		expandAll(nodelist, AbstractTreeViewer.ALL_LEVELS);
	}

	/**
	 * Expands all ancestors of the given element or tree path so that the given element becomes visible in this
	 * viewer's tree control, and then expands the subtree rooted at the given element.
	 */
	protected void expandAll(List<TreeNode> nodelist, int level) {
		runSafety(() -> nodelist.forEach(node -> treeViewer.expandToLevel(node, level)));
	}

	protected void setExpandedState(TreeNode node, boolean expanded) {
		runSafety(() -> treeViewer.setExpandedState(node, expanded));
	}

	public Object[] getExpandedElements() {
		return !treeViewer.getControl().isDisposed() ? treeViewer.getExpandedElements() : new Object[0];
	}

}