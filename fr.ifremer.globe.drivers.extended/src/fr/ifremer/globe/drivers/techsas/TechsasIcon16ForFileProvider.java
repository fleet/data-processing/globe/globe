/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.techsas;

import java.util.Optional;
import java.util.Set;

import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.utils.image.ImageResources;

/**
 * Offer the icon to represent a mbg file.
 */
@Component(name = "globe_drivers_techsas_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class TechsasIcon16ForFileProvider implements IIcon16ForFileProvider {

	@Override
	public Set<ContentType> getContentTypes() {
		return ContentType.getTechsasContentTypes();
	}

	@Override
	public Optional<Image> getIcon() {
		return Optional.of(ImageResources.getImage("icons/16/database.png", getClass()));
	}
}
