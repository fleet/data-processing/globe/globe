package fr.ifremer.globe.drivers.techsas;

import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.techsas.TechsasNavigationData;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.NavigationMetadataType;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer.DisplayMode;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.WWNavigationLayerParameters;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class builds {@link IWWNavigationLayer} for TECHSAS files.
 */
@Component(name = "globe_drivers_techsas_navigation_layer_provider", service = IWWLayerProvider.class)
public class TechsasNavigationLayerProvider implements IWWLayerProvider {

	/** OSGI Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	@Reference
	private INavigationService navigationService;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return ContentType.getTechsasContentTypes();
	}

	/**
	 * @return an {@link WWFileLayerStore} which contains a {@link IWWNavigationLayer} for files which can provide a
	 *         {@link INavigationDataSupplier}.
	 */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		return navigationService.getNavigationDataSupplier(fileInfo).map(navigationDataSupplier -> {
			var params = new WWNavigationLayerParameters().withDisplayMode(DisplayMode.POINT)
					.withVariableColor(true, getNavigationVariable(fileInfo));
			var layer = layerFactory.createNavigationLayer(navigationDataSupplier, params);
			return new WWFileLayerStore(fileInfo, layer);
		});
	}

	private Optional<NavigationMetadataType<?>> getNavigationVariable(IFileInfo fileInfo) {
		var fileName = fileInfo.getFilename();
		if (fileName.endsWith(".depth.nc"))
			return Optional.of(TechsasNavigationData.DEPTH);
		if (fileName.endsWith(".gravi.nc"))
			return Optional.of(TechsasNavigationData.COMPUTED_FAA);
		if (fileName.endsWith(".mag.nc"))
			return Optional.of(TechsasNavigationData.MAGNETISM);
		if (fileName.endsWith(".ths.nc"))
			return Optional.of(TechsasNavigationData.WATER_TEMPERATURE);
		return Optional.empty();
	}

}
