/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.gmt.worldwind;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.ifremer.globe.core.io.gmt.info.GmtFileInfo;
import fr.ifremer.globe.core.io.gmt.info.GmtFileInfoSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModelFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.tile.IWWTilesService;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWXML;

/**
 * Provider of WW layer for GMT files.
 */
@Component(name = "globe_drivers_gmt_netcdf4_terrain_layer_provider", service = IWWLayerProvider.class)
public class GmtTerrainLayerProvider extends BasicWWLayerProvider<GmtFileInfo> {

	/** Logger */
	private static Logger logger = LoggerFactory.getLogger(GmtTerrainLayerProvider.class);

	/** Osgi Service used to create WW layers */
	@Reference
	private IWWLayerFactory layerFactory;

	/** Osgi Service used to tiled image layers */
	@Reference
	private IWWTilesService tilesInstaller;

	/** Osgi Service used to produce WW ElevationModel */
	@Reference
	private IWWElevationModelFactory elevationModelFactory;
	/** Osgi Service used to produce WW ElevationModel */
	@Reference
	private GmtFileInfoSupplier gmtFileInfoSupplier;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return Set.of(ContentType.GMT);
	}

	@Override
	/**
	 * Creates the worldwind layers to represent the raster in the 3D viewer
	 *
	 * @param rasterInfo the raster informations
	 * @param monitor monitor of progression
	 * @throws GIOException creation failed, error occured
	 */
	protected void createLayers(GmtFileInfo fileInfo, WWFileLayerStore layerStore, IProgressMonitor monitor)
			throws GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);

		String displayName = WWIO.replaceIllegalFileNameCharacters(fileInfo.getBaseName());
		List<IWWLayer> layers = new ArrayList<>();
		ElevationModel elevationModel = null;

		// Load or create the XML docunent describing the tiles pyramid of the elevation
		List<Document> tilesPyramids = createTerrainTiles(displayName, fileInfo, subMonitor.split(90));
		if (!tilesPyramids.isEmpty()) {
			elevationModel = elevationModelFactory.createInterpolateElevationModel(displayName, fileInfo.getGeoBox(),
					tilesPyramids);

			layers.add(layerFactory.createIsobathLayer(displayName, fileInfo.getGeoBox(),
					() -> new File(gmtFileInfoSupplier.extractElevations(fileInfo, subMonitor.split(50)))));

			var terrainLayer = layerFactory.createTerrainLayer(tilesPyramids, "Elevation", displayName);
			layers.add(terrainLayer);
			layers.add(layerFactory.createColorScaleLayer(terrainLayer));

			subMonitor.done();
			layerStore.addLayers(layers);
			layerStore.setElevationModel(elevationModel);
		} else {
			logger.debug("Unable to produce the layers of GMT file {}", fileInfo.getBaseName());
		}
	}

	/**
	 * Load the XML files describing a tiled elevation pyramid
	 */
	private List<Document> createTerrainTiles(String displayName, GmtFileInfo fileInfo, IProgressMonitor monitor)
			throws GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		GeoBox geoBox = fileInfo.getGeoBox();
		List<Document> result = new LinkedList<>();
		var documentPaths = tilesInstaller.computeRasterCacheDocument(displayName, geoBox);
		if (documentPaths.stream().anyMatch(path -> !path.exists())) {
			try (GdalDataset dataset = GdalDataset
					.open(gmtFileInfoSupplier.extractElevations(fileInfo, subMonitor.split(50)))) {
				if (dataset.isPresent()) {
					dataset.deleteOnClose(true);
					result = tilesInstaller.makeTerrainProducerBuilder(dataset).//
							setDatasetName(displayName).//
							setGeoBox(fileInfo.getGeoBox()).//
							setMinMax(fileInfo.getzMinMax()).//
							run(subMonitor.split(50));
				} else {
					logger.debug("Extraction of elevations failed. Unable to produce the terrain tiles");
				}
			}
		} else {
			result = documentPaths.stream().map(WWXML::openDocument).toList();
		}
		monitor.done();
		return result;
	}

}