/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.segy.icon;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a SEG-Y file.
 */
@Component(name = "globe_drivers_segy_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class SegyIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public SegyIcon16ForFileProvider() {
		super("icons/16/seismic.png", ContentType.SEGY);
	}
}