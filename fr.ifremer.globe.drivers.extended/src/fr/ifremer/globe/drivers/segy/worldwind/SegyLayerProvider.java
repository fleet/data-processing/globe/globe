/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.segy.worldwind;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.segy.info.SegyFileInfo;
import fr.ifremer.globe.core.io.segy.info.SegyInfoSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDataUtils;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.globe.ui.utils.color.ColorMap.ColorMaps;

/**
 * IWWLayerProvider dedicated to SEGY files.
 */
@Component(name = "globe_drivers_segy_layer_provider", service = IWWLayerProvider.class)
public class SegyLayerProvider extends BasicWWLayerProvider<SegyFileInfo> {

	/** Loader of Segy file info */
	@Reference
	protected SegyInfoSupplier infoSupplier;

	/** WW layer factory */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return infoSupplier.getContentTypes();
	}

	/** {@inheritDoc} */
	@Override
	public void createLayers(SegyFileInfo fileInfo, WWFileLayerStore layerStore, IProgressMonitor monitor) {
		layerStore.addLayers(buildTextureLayer(fileInfo, monitor));
	}

	/**
	 * Creates texture layer from SEGY traces.
	 */
	protected IWWTextureLayer buildTextureLayer(SegyFileInfo fileInfo, IProgressMonitor monitor) {
		int width = fileInfo.getNTraces();
		int height = fileInfo.getNSamplesPerTrace();
		SubMonitor subMonitor = SubMonitor.convert(monitor, width);

		// SEGY trace is an oscillation between a negative minimum value and a positive maximum value.
		// Texture layer displays the absolute amplitude of the signal.
		var byteBuffer = ByteBuffer.allocateDirect(width * height * Float.BYTES).order(ByteOrder.nativeOrder());
		DoubleSummaryStatistics stats = new DoubleSummaryStatistics();
		for (int sample = 0; sample < height; sample++) {
			for (int trace = 0; trace < width; trace++) {
				// Apply log1p to improve display : TODO should be a display parameter 
				var value = (float) Math.log1p(Math.abs(fileInfo.getValue(trace, sample)));
				byteBuffer.putFloat(value);
				stats.accept(value);
			}
			subMonitor.worked(1);
		}
		byteBuffer.rewind();
		subMonitor.done();

		// Compute texture's coordinates.
		var coordinates = new ArrayList<IWWTextureCoordinate>();
		var segyPoints = fileInfo.getPoints();
		for (int i = 0; i < segyPoints.size(); i++) {
			var p = segyPoints.get(i);
			var s = (float) i / segyPoints.size();
			coordinates.add(WWTextureDataUtils.buildTopTextureCoordinate(p.lat(), p.lon(), p.topElevation(), s));
			coordinates.add(WWTextureDataUtils.buildBottomTextureCoordinate(p.lat(), p.lon(), p.bottomElevation(), s));
		}

		var textureData = WWTextureDataUtils.buildVerticalTextureData(width, height, byteBuffer, coordinates);

		// Compute texture's display parameters.
		var textureDisplayParameters = new WWTextureDisplayParameters(1, (float) stats.getMin(), (float) stats.getMax(),
				ColorMaps.GRAY.getIndex(), true);

		return layerFactory.createWWTextureLayer(fileInfo.getBaseName(), textureData, textureDisplayParameters);
	}

}
