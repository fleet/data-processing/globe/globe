/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.segy.worldwind;

import java.awt.image.BufferedImage;
import java.util.List;

import org.apache.commons.lang.math.DoubleRange;

import fr.ifremer.globe.ui.service.worldwind.layer.IWWVerticalImageLayer.Data;
import gov.nasa.worldwind.geom.Position;

/**
 * Vertical image layer data for Seg
 */
public class VerticalImageLayerData implements Data {

	protected String name;
	protected BufferedImage image;
	protected DoubleRange minMaxValues;
	protected List<Position> topPoints;
	protected List<Position> bottomPoints;

	/**
	 * @return the {@link #name}
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @param name the {@link #name} to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the {@link #image}
	 */
	@Override
	public BufferedImage getImage() {
		return image;
	}

	/**
	 * @param image the {@link #image} to set
	 */
	public void setImage(BufferedImage image) {
		this.image = image;
	}

	/**
	 * @return the {@link #minMaxValues}
	 */
	@Override
	public DoubleRange getMinMaxValues() {
		return minMaxValues;
	}

	/**
	 * @param minMaxValues the {@link #minMaxValues} to set
	 */
	public void setMinMaxValues(DoubleRange minMaxValues) {
		this.minMaxValues = minMaxValues;
	}

	/**
	 * @return the {@link #topPoints}
	 */
	@Override
	public List<Position> getTopPoints() {
		return topPoints;
	}

	/**
	 * @param topPoints the {@link #topPoints} to set
	 */
	public void setTopPoints(List<Position> topPoints) {
		this.topPoints = topPoints;
	}

	/**
	 * @return the {@link #bottomPoints}
	 */
	@Override
	public List<Position> getBottomPoints() {
		return bottomPoints;
	}

	/**
	 * @param bottomPoints the {@link #bottomPoints} to set
	 */
	public void setBottomPoints(List<Position> bottomPoints) {
		this.bottomPoints = bottomPoints;
	}

}
