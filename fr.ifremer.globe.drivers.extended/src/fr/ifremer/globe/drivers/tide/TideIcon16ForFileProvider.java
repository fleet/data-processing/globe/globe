/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.tide;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a ttb file (tide).
 */
@Component(name = "globe_drivers_tide_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class TideIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public TideIcon16ForFileProvider() {
		super("icons/16/tide.png", ContentType.TIDE_CSV);
	}
}
