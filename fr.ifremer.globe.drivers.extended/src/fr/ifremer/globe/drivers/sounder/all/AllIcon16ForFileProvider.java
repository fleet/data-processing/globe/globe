package fr.ifremer.globe.drivers.sounder.all;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a all file.
 */
@Component(name = "globe_drivers_all_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class AllIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public AllIcon16ForFileProvider() {
		super("icons/16/all.png", ContentType.SOUNDER_ALL, ContentType.SOUNDER_KMALL);
	}
}