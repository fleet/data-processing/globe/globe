package fr.ifremer.globe.drivers.sonar.service.worldwind;

import java.nio.ByteBuffer;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;

import fr.ifremer.globe.core.io.sonar.info.SonarNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SonarBeamGroup;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.NetcdfVlenLayerManager;
import fr.ifremer.globe.core.runtime.datacontainer.layers.Float2DVlenLayer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.drivers.xsf.service.worldwind.XsfTextureDataUtils;
import fr.ifremer.globe.netcdf.api.NetcdfVlenVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCDimension;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.netcdf.util.slicing.DimensionSlice;
import fr.ifremer.globe.netcdf.util.slicing.VariableSlicerFacade;
import fr.ifremer.globe.ui.application.context.ContextInitializer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.impl.WWTextureData;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Reads data from /Sonar/Beam_groupXXX/Backscatter_r and computes {@link IWWTextureData}.
 */
public class BeamGroupBackscatterTextureDataLoader {

	/** Factory of the sounder data container */
	private final IDataContainerFactory containerFactory;

	/** {@link IDataContainerOwner} to book the {@link DataContainer} **/
	private final IDataContainerOwner dataContainerOwner = IDataContainerOwner
			.generate("Generating texture for Backscatter_r variable", true);

	/** File **/
	private final SonarNcInfo sonarNcInfo;

	/**
	 * Constructor
	 */
	public BeamGroupBackscatterTextureDataLoader(SonarNcInfo sonarNcInfo) {
		this.sonarNcInfo = sonarNcInfo;
		containerFactory = ContextInitializer.getInContext(IDataContainerFactory.class);
	}

	/** Create a IWWTextureData for the specified index of frequency and compute statistics on values */
	public IWWTextureData loadTextureData(int groupIndex, IProgressMonitor monitor)
			throws GIOException, OperationCanceledException, NCException {
		SubMonitor progress = SubMonitor.convert(monitor, 100);
		if (progress.isCanceled())
			throw new OperationCanceledException();

		try (ISounderDataContainerToken sounderDataContainerToken = containerFactory.book(sonarNcInfo,
				dataContainerOwner)) {
			SounderDataContainer sounderDataContainer = sounderDataContainerToken.getDataContainer();
			SonarBeamGroup sonarBeamGroup = sounderDataContainer.getBeamGroup(groupIndex + 1);

			// Compute texture data
			ByteBuffer backscatterBuffer = loadBackscatter(sonarBeamGroup);
			if (progress.isCanceled())
				throw new OperationCanceledException();
			progress.worked(50);

			int textureWidth = sonarNcInfo.getCycleCount();
			int textureHeight = backscatterBuffer.capacity() / textureWidth / Float.BYTES;
			// Compute texture coordinates
			List<IWWTextureCoordinate> coordinates = XsfTextureDataUtils.computeCoordinates(sounderDataContainer,
					textureWidth, textureHeight, progress.split(50));

			return new WWTextureData(textureWidth, textureHeight, backscatterBuffer, coordinates, true);
		}
	}

	/**
	 * Load the VLEN backscatter_r fully
	 */
	public ByteBuffer loadBackscatter(SonarBeamGroup sonarBeamGroup) throws NCException, GIOException {
		Float2DVlenLayer backscatterLayer = sonarBeamGroup.getVlenLayer(BeamGroup1Layers.BACKSCATTER_R_VARIABLE_NAME);
		NetcdfVlenLayerManager layerManager = backscatterLayer.getLayerManager();
		NetcdfVlenVariable backscatterVlenVariable = layerManager.getNetcdfVariable();

		// Read the whole buffer
		List<NCDimension> shape = backscatterVlenVariable.getShape();
		VariableSlicerFacade slicerFacade = new VariableSlicerFacade();
		var result = slicerFacade.sliceVariable(DataType.FLOAT, backscatterVlenVariable,
				DimensionSlice.fromDimensions(shape));

		return result.orElseThrow(() -> new GIOException("Unable to read " + backscatterLayer.getName()));
	}

}
