/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.sonar.service.worldwind;

import java.nio.ByteBuffer;
import java.util.DoubleSummaryStatistics;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.IntStream;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.Activator;
import fr.ifremer.globe.core.io.sonar.griddeddata.SonarNcGriddedDataConstants;
import fr.ifremer.globe.core.io.sonar.info.SonarNcInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.model.wc.XsfWCPreferences;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.GridGroup1Layers;
import fr.ifremer.globe.drivers.xsf.service.worldwind.XsfLayerProvider;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.ui.application.event.WWLayerDescription;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureDataCache;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Driver to manage Globe 3D netCDF files.
 */
@Component(name = "globe_drivers_sonar_nc_layer_provider", service = { IWWLayerProvider.class,
		SonarNcLayerProvider.class })
public class SonarNcLayerProvider implements IWWLayerProvider {

	/** Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(SonarNcLayerProvider.class);

	public static final String BEAM_GROUP = "Beam_group";
	public static final String GRID_GROUP = "Grid_group";

	/** OSGI Service used to create WW layers */
	@Reference
	private IWWLayerFactory layerFactory;
	/** OSGI Service used to create WW layers */
	@Reference
	private XsfLayerProvider xsfLayerLoader;
	@Reference
	private ISessionService sessionService;
	@Reference
	private IWWTextureDataCache textureDataCache;

	/** Preferences */
	private final XsfWCPreferences displayPreferences = Activator.getXsfWCPreferences();

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return Set.of(ContentType.SONAR_NETCDF_4);
	}

	/**
	 * @throws GIOException
	 * @see fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider#getFileLayers(java.lang.String,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		if (fileInfo instanceof SonarNcInfo sonarNcInfo) {
			try {
				SubMonitor progress = SubMonitor.convert(monitor, 100);
				WWFileLayerStore result = new WWFileLayerStore(sonarNcInfo);

				// Loading layer as XSF files
				List<IWWLayer> beamGroupLayers = xsfLayerLoader.createLayers(sonarNcInfo, progress.split(40));
				if (!beamGroupLayers.isEmpty()) {
					beamGroupLayers.forEach(layer -> layer
							.setEnabled(layer == beamGroupLayers.get(0) || layer instanceof IWWNavigationLayer));
					result.addLayers(beamGroupLayers);
				}

				// sonar NetCDF with grid groups (= gridded data)
				if (sonarNcInfo.getGridGroupCount() > 0)
					makeGridGroupTextureLayers(sonarNcInfo, result, progress);

				// sonar NetCDF with only one beam in beam groups (EK80)
				if (sonarNcInfo.getBeamGroupCount() > 0 && sonarNcInfo.getWcRxBeamCount() == 1)
					makeBeamGroupTextureLayers(sonarNcInfo, result, progress);

				progress.done();
				return result.getLayers().isEmpty() ? Optional.empty() : Optional.of(result);
			} catch (OperationCanceledException e) {
				throw new GIOException("Operation cancelled");
			} catch (NCException e) {
				throw GIOException.wrap(e.getMessage(), e);
			}

		}
		return Optional.empty();
	}

	/** Process loading of texture for Beam_group (EK80) */
	private void makeBeamGroupTextureLayers(SonarNcInfo sonarNcInfo, WWFileLayerStore layerStore,
			IProgressMonitor monitor) throws GIOException, NCException, OperationCanceledException {
		SubMonitor progress = SubMonitor.convert(monitor, sonarNcInfo.getBeamGroupCount());

		var loadedLayers = new LinkedList<String>();
		// Restore layer contained in session.
		for (int groupIndex = 0; groupIndex < sonarNcInfo.getBeamGroupCount(); groupIndex++) {
			String layerName = computeLayerName(sonarNcInfo, BEAM_GROUP, groupIndex, groupIndex);
			if (sessionService.doesSessionContainsLayer(sonarNcInfo.getPath(), layerName)) {
				IWWTextureLayer layer = makeBeamGroupTextureLayer(sonarNcInfo, groupIndex, progress.split(1));
				layerStore.addLayers(layer);
				loadedLayers.add(layer.getName());
			}
		}

		if (!progress.isCanceled() && loadedLayers.isEmpty()) {
			// No layer restored. Load prefered group
			int defaultGroup = displayPreferences.getBeamGroupDefaultValue() - 1;
			if (defaultGroup >= 0 && defaultGroup < sonarNcInfo.getBeamGroupCount()) {
				IWWTextureLayer layer = makeBeamGroupTextureLayer(sonarNcInfo, defaultGroup, progress.split(1));
				layerStore.addLayers(layer);
				loadedLayers.add(layer.getName());
			}
		}

		if (!progress.isCanceled() && loadedLayers.isEmpty()) {
			// No layer loaded at this time, so load first group
			IWWTextureLayer layer = makeBeamGroupTextureLayer(sonarNcInfo, 0, progress.split(1));
			layerStore.addLayers(layer);
			loadedLayers.add(layer.getName());
		}

		// Declare other group layer as optional
		for (int groupIndex = 0; !progress.isCanceled() && groupIndex < sonarNcInfo.getBeamGroupCount(); groupIndex++) {
			String layerName = computeLayerName(sonarNcInfo, BEAM_GROUP, groupIndex, groupIndex);
			if (!loadedLayers.contains(layerName)) {
				WWLayerDescription optionalLayer = new WWLayerDescription(layerName, IWWTextureLayer.class,
						computeLayerSyncKey(sonarNcInfo, BEAM_GROUP, groupIndex));
				layerStore.addOptionalLayers(optionalLayer);
			}
		}

		progress.done();
	}

	/** Process loading of textures for Grid_group */
	private void makeGridGroupTextureLayers(SonarNcInfo sonarNcInfo, WWFileLayerStore layerStore,
			IProgressMonitor monitor) throws GIOException, NCException, OperationCanceledException {
		SubMonitor progress = SubMonitor.convert(monitor, sonarNcInfo.getGridGroupNames().size());

		var loadedLayers = new LinkedList<String>();
		// Restore layer contained in session.
		for (int groupIndex = 0; groupIndex < sonarNcInfo.getGridGroupNames().size(); groupIndex++) {
			for (int frequencyIndex = 0; frequencyIndex < sonarNcInfo.getFrequencies().size(); frequencyIndex++) {
				String layerName = computeLayerName(sonarNcInfo, GRID_GROUP, groupIndex, frequencyIndex);
				if (sessionService.doesSessionContainsLayer(sonarNcInfo.getPath(), layerName)) {
					IWWTextureLayer layer = makeGridGroupTextureLayer(sonarNcInfo, groupIndex, frequencyIndex,
							progress.split(1));
					layerStore.addLayers(layer);
					loadedLayers.add(layer.getName());
				}
			}
		}

		if (!progress.isCanceled() && loadedLayers.isEmpty()) {
			// No layer loaded at this time, so load first frequency for each group
			for (int groupIndex = 0; groupIndex < sonarNcInfo.getGridGroupNames().size(); groupIndex++) {
				try {
					IWWTextureLayer layer = makeGridGroupTextureLayer(sonarNcInfo, groupIndex, 0, progress.split(1));
					layerStore.addLayers(layer);
					loadedLayers.add(layer.getName());
				} catch (GIOException | NCException e) {
					LOGGER.warn("Layer {} ignored : {}", computeLayerName(sonarNcInfo, GRID_GROUP, groupIndex, 0),
							e.getMessage());
				}
			}
		}

		// Declare other layers as optional
		makeGridGroupOptionalTextureLayers(loadedLayers, sonarNcInfo, progress).forEach(layerStore::addOptionalLayers);

		progress.done();
	}

	/**
	 * Build the list of optional layers. <br>
	 * Perform some checks to ensure that the layers are loadable
	 */
	private List<WWLayerDescription> makeGridGroupOptionalTextureLayers(LinkedList<String> loadedLayers,
			SonarNcInfo sonarNcInfo, SubMonitor progress) {
		var result = new LinkedList<WWLayerDescription>();
		try {
			sonarNcInfo.open(true);

			NetcdfGroup sonarGroup = sonarNcInfo.getFile().getGroup(SonarNcGriddedDataConstants.SONAR_GRP);
			for (int groupIndex = 0; !progress.isCanceled()
					&& groupIndex < sonarNcInfo.getGridGroupNames().size(); groupIndex++) {
				NetcdfGroup ncGroup = sonarGroup.getGroup(sonarNcInfo.getGridGroupNames().get(groupIndex));
				if (ncGroup != null) {
					// Checks presence of layer integrated_backscatter
					NetcdfVariable integratedBackscatterVar = ncGroup
							.getVariable(GridGroup1Layers.INTEGRATED_BACKSCATTER_VARIABLE_NAME);
					if (integratedBackscatterVar != null) {
						// Checks Range dimension (Sometimes == 1)
						if (ncGroup.getDimension(SonarNcGriddedDataConstants.RANGE_DIM).getLength() > 1) {
							for (int frequencyIndex = 0; frequencyIndex < sonarNcInfo.getFrequencies()
									.size(); frequencyIndex++) {
								String layerName = computeLayerName(sonarNcInfo, GRID_GROUP, groupIndex,
										frequencyIndex);
								if (!loadedLayers.contains(layerName)) {
									result.add(new WWLayerDescription(layerName, IWWTextureLayer.class,
											computeLayerSyncKey(sonarNcInfo, GRID_GROUP, frequencyIndex)));
								}
							}
						} else {
							LOGGER.warn("Dimension {} insufficient. Group {} ignored.",
									SonarNcGriddedDataConstants.RANGE_DIM, ncGroup.getName());
						}
					} else {
						LOGGER.warn("Variable {} not found. Group {} ignored.",
								GridGroup1Layers.INTEGRATED_BACKSCATTER_VARIABLE_NAME, ncGroup.getName());
					}
				} else {
					LOGGER.warn("Group {} not found.", sonarNcInfo.getGridGroupNames().get(groupIndex));
				}
			}
		} catch (GIOException | NCException e) {
			LOGGER.warn("NC error while checking Grid groups", e);
		} finally {
			sonarNcInfo.close();
		}

		return result;
	}

	/**
	 * For a single-beam sonar file, creates a texture layer to display backscatter_r variable.
	 */
	public IWWTextureLayer makeBeamGroupTextureLayer(SonarNcInfo sonarNcInfo, int groupIndex, IProgressMonitor monitor)
			throws GIOException, OperationCanceledException, NCException {
		String layerName = computeLayerName(sonarNcInfo, BEAM_GROUP, groupIndex, groupIndex);
		String layerShortName = computeLayerShortName(sonarNcInfo, BEAM_GROUP, groupIndex, groupIndex);
		String syncKey = computeLayerSyncKey(sonarNcInfo, BEAM_GROUP, groupIndex);
		return restoreOrMakeTextureLayer(sonarNcInfo, layerName, layerShortName, syncKey, m -> {
			var textureDataLoader = new BeamGroupBackscatterTextureDataLoader(sonarNcInfo);
			return textureDataLoader.loadTextureData(groupIndex, m);
		}, monitor);
	}

	/**
	 * For a sonar file with Grid_group, creates a texture layer to display backscatter_r variable.
	 */
	public IWWTextureLayer makeGridGroupTextureLayer(SonarNcInfo sonarNcInfo, int groupIndex, int frequencyIndex,
			IProgressMonitor monitor) throws GIOException, NCException, OperationCanceledException {
		String layerName = computeLayerName(sonarNcInfo, GRID_GROUP, groupIndex, frequencyIndex);
		String layerShortName = computeLayerShortName(sonarNcInfo, GRID_GROUP, groupIndex, frequencyIndex);
		String syncKey = computeLayerSyncKey(sonarNcInfo, GRID_GROUP, frequencyIndex);
		return restoreOrMakeTextureLayer(sonarNcInfo, layerName, layerShortName, syncKey, m -> {
			var textureDataLoader = new GridGroupBackscatterTextureDataLoader(sonarNcInfo);
			return textureDataLoader.loadTextureData(groupIndex, frequencyIndex, m);
		}, monitor);
	}

	/** Generates the name of a TextureLayer of backscatter (Beam_group) or integrated_backscatter (Grid_group) */
	public String computeLayerName(SonarNcInfo sonarNcInfo, String groupName, int groupIndex, int frequencyIndex) {
		return computeLayerShortName(sonarNcInfo, groupName, groupIndex, frequencyIndex) + "_"
				+ sonarNcInfo.getFilename();
	}

	/** Generates the short name of a TextureLayer of backscatter (Beam_group) or integrated_backscatter (Grid_group) */
	public String computeLayerShortName(SonarNcInfo sonarNcInfo, String groupName, int groupIndex, int frequencyIndex) {
		float frequency = frequencyIndex < sonarNcInfo.getFrequencies().size()
				? sonarNcInfo.getFrequencies().get(frequencyIndex)
				: Float.NaN;
		String subGroupName = "";
		if (Float.isFinite(frequency)) {
			subGroupName += " (" + frequency;
			if (!sonarNcInfo.getFrequencyUnit().isEmpty())
				subGroupName += " " + sonarNcInfo.getFrequencyUnit();
			subGroupName += ")";
		}
		return groupName + (groupIndex + 1) + subGroupName;
	}

	/** Generate the synchronization key */
	public String computeLayerSyncKey(SonarNcInfo sonarNcInfo, String groupName, int frequencyIndex) {
		String subGroupName = "";
		if (frequencyIndex < sonarNcInfo.getFrequencies().size()) {
			subGroupName = "_" + sonarNcInfo.getFrequencies().get(frequencyIndex);
			if (!sonarNcInfo.getFrequencyUnit().isEmpty())
				subGroupName += "_" + sonarNcInfo.getFrequencyUnit();
		}
		return groupName + subGroupName;
	}

	/**
	 * Try to restore the IWWTextureData from the cache. If absent, invoke the supplier to produce one.<br>
	 * Then create and configure the IWWTextureLayer
	 */
	private IWWTextureLayer restoreOrMakeTextureLayer(SonarNcInfo sonarNcInfo, String layerName, String layerShortName,
			String syncKey, IWWTextureDataSupplier textureDataSupplier, IProgressMonitor monitor)
			throws GIOException, NCException {
		SubMonitor progress = SubMonitor.convert(monitor, 100);

		IWWTextureData textureData = textureDataCache.restore(sonarNcInfo, layerName).orElse(null);
		if (textureData == null) {
			long time = System.currentTimeMillis();
			textureData = textureDataSupplier.supply(progress.split(60));
			if (progress.isCanceled())
				throw new OperationCanceledException();

			DoubleSummaryStatistics stats = computeStats(textureData);
			progress.worked(20);
			if (progress.isCanceled())
				throw new OperationCanceledException();

			textureData.setMetadata(Map.of(//
					IWWTextureData.NAME_METADATA_KEY, layerName, //
					IWWTextureData.SYNCHRONIZATION_KEY_METADATA_KEY, syncKey, //
					IWWTextureData.SYNCHRONIZATION_LABEL_METADATA_KEY, "frequency", //
					IWWTextureData.MIN_VALUE_METADATA_KEY, String.valueOf(stats.getMin()), //
					IWWTextureData.MAX_VALUE_METADATA_KEY, String.valueOf(stats.getMax())//
			));

			LOGGER.debug("{} ms for generating the texture {} ", (System.currentTimeMillis() - time), layerName);
			textureDataCache.save(sonarNcInfo, textureData, layerName);
		} else {
			progress.worked(80);
		}

		var displayParameters = new WWTextureDisplayParameters(
				Float.parseFloat(textureData.getMetadata().getOrDefault(IWWTextureData.MIN_VALUE_METADATA_KEY, "-127")),
				Float.parseFloat(
						textureData.getMetadata().getOrDefault(IWWTextureData.MAX_VALUE_METADATA_KEY, "-127")));

		IWWTextureLayer result = layerFactory.createWWTextureLayer(layerName, textureData, displayParameters);
		result.setShortName(layerShortName);
		progress.done();
		return result;
	}

	/** Computes the minimum and maximum values of the texture buffer and set the result in the metadata */
	private DoubleSummaryStatistics computeStats(IWWTextureData textureData) {
		ByteBuffer buffer = textureData.getBuffer();
		buffer.rewind();
		DoubleSummaryStatistics stats = IntStream.range(0, buffer.capacity() / Float.BYTES)
				.mapToDouble(i -> buffer.getFloat()).filter(Double::isFinite).summaryStatistics();
		buffer.rewind();
		return stats;
	}

	@FunctionalInterface
	interface IWWTextureDataSupplier {
		IWWTextureData supply(IProgressMonitor monitor) throws GIOException, NCException;
	}
}