/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.sonar.service.icon;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a Sonar Netcdf file.
 */
@Component(name = "globe_drivers_sonar_nc_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class SonarNcIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public SonarNcIcon16ForFileProvider() {
		super("icons/16/sonar.png", ContentType.SONAR_NETCDF_4);
	}
}