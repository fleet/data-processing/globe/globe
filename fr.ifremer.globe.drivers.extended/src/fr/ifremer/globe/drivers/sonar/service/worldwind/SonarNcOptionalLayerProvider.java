/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.sonar.service.worldwind;

import java.util.Objects;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.sonar.info.SonarNcInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.ui.application.event.UILayerEventTopics;
import fr.ifremer.globe.ui.application.event.UILayerEventTopics.LayerRequestInfo;
import fr.ifremer.globe.ui.layer.WWFileLayerStoreModel;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Class responsible for the production of optional texture layers for SonarNc files
 *
 */
public class SonarNcOptionalLayerProvider {

	/** Logger */
	protected static final Logger LOGGER = LoggerFactory.getLogger(SonarNcOptionalLayerProvider.class);

	@Inject
	private SonarNcLayerProvider sonarNcLayerProvider;

	@Inject
	private WWFileLayerStoreModel fileLayerStoreModel;

	/**
	 * Event handler for TOPIC_WW_LAYERS_REQUESTED
	 */
	@Inject
	@Optional
	protected void onLayerRequested(
			@UIEventTopic(UILayerEventTopics.TOPIC_WW_LAYERS_REQUESTED) LayerRequestInfo request) {
		if (request.fileInfo().getContentType() == ContentType.SONAR_NETCDF_4) {
			SonarNcInfo sonarNcInfo = (SonarNcInfo) request.fileInfo();
			fileLayerStoreModel.get(sonarNcInfo).ifPresent(layerStore -> {
				try {
					if (layerStore.removeOptionalLayer(request.layerDesc())) {
						// Beam_group layer ?
						for (int groupIndex = 0; !request.monitor().isCanceled()
								&& groupIndex < sonarNcInfo.getBeamGroupCount(); groupIndex++) {
							String layerName = sonarNcLayerProvider.computeLayerName(sonarNcInfo,
									SonarNcLayerProvider.BEAM_GROUP, groupIndex, groupIndex);
							if (Objects.equals(layerName, request.layerDesc().name())) {
								loadBeamGroupTexture(sonarNcInfo, groupIndex, request.monitor(), request.logger());
								return;
							}
						}

						// Grid_group layer ?
						for (int groupIndex = 0; !request.monitor().isCanceled()
								&& groupIndex < sonarNcInfo.getGridGroupCount(); groupIndex++) {
							for (int frequencyIndex = 0; frequencyIndex < sonarNcInfo.getFrequencies()
									.size(); frequencyIndex++) {
								String layerName = sonarNcLayerProvider.computeLayerName(sonarNcInfo,
										SonarNcLayerProvider.GRID_GROUP, groupIndex, frequencyIndex);
								if (Objects.equals(layerName, request.layerDesc().name())) {
									loadGridGroupTexture(sonarNcInfo, groupIndex, frequencyIndex, request.monitor(),
											request.logger());
									return;
								}
							}
						}
					}
				} catch (OperationCanceledException e) {
					LOGGER.warn("Operation cancelled by user");
				} catch (GIOException | NCException e) {
					LOGGER.error("Unable to produce requested layer : {}", e.getMessage());
				}
			});
		}
	}

	private void loadBeamGroupTexture(SonarNcInfo sonarNcInfo, int groupIndex, IProgressMonitor monitor, Logger logger)
			throws GIOException, OperationCanceledException, NCException {
		String layerName = sonarNcLayerProvider.computeLayerName(sonarNcInfo, SonarNcLayerProvider.BEAM_GROUP,
				groupIndex, groupIndex);
		logger.info("Creating layer {}", layerName);
		monitor.setTaskName("Creating layer " + layerName);
		IWWTextureLayer textureLayer = sonarNcLayerProvider.makeBeamGroupTextureLayer(sonarNcInfo, groupIndex,
				monitor);
		fileLayerStoreModel.addLayersToStore(sonarNcInfo, textureLayer);
	}

	private void loadGridGroupTexture(SonarNcInfo sonarNcInfo, int groupIndex, int frequencyIndex,
			IProgressMonitor monitor, Logger logger) throws GIOException, NCException, OperationCanceledException {
		String layerName = sonarNcLayerProvider.computeLayerName(sonarNcInfo, SonarNcLayerProvider.GRID_GROUP,
				groupIndex, frequencyIndex);
		logger.info("Creating layer {}", layerName);
		monitor.setTaskName("Creating layer " + layerName);
		IWWTextureLayer textureLayer = sonarNcLayerProvider.makeGridGroupTextureLayer(sonarNcInfo, groupIndex,
				frequencyIndex, monitor);
		fileLayerStoreModel.addLayersToStore(sonarNcInfo, textureLayer);
	}
}
