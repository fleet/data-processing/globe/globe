package fr.ifremer.globe.drivers.sonar.service.worldwind;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.IntFunction;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;

import fr.ifremer.globe.core.io.sonar.griddeddata.SonarNcGriddedDataConstants;
import fr.ifremer.globe.core.io.sonar.info.SonarNcInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.GridGroup1Layers;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDataUtils;
import fr.ifremer.globe.ui.service.worldwind.texture.impl.WWTextureData;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Reads data from /Sonar/Grid_groupXXX/integrated_backscatter and computes {@link IWWTextureData}.
 */
public class GridGroupBackscatterTextureDataLoader {

	/** Byte order constant */
	private static final ByteOrder NATIVE_ORDER = ByteOrder.nativeOrder();

	/** File **/
	private final SonarNcInfo sonarNcInfo;

	/**
	 * Constructor
	 */
	public GridGroupBackscatterTextureDataLoader(SonarNcInfo sonarNcInfo) {
		this.sonarNcInfo = sonarNcInfo;
	}

	/**
	 * Create a IWWTextureData for the specified index of Grid_group and index of frequency and compute statistics on
	 * values
	 */
	public IWWTextureData loadTextureData(int groupIndex, int frequencyIndex, IProgressMonitor monitor)
			throws GIOException, NCException, OperationCanceledException {
		SubMonitor progress = SubMonitor.convert(monitor, 100);
		if (progress.isCanceled())
			throw new OperationCanceledException();

		try {
			sonarNcInfo.open(true);

			NetcdfGroup sonarGroup = sonarNcInfo.getFile().getGroup(SonarNcGriddedDataConstants.SONAR_GRP);
			NetcdfGroup ncGroup = sonarGroup.getGroup(sonarNcInfo.getGridGroupNames().get(groupIndex));
			if (ncGroup == null)
				throw new GIOException("Netcdf group not found in file {} " + sonarNcInfo.getFilename());

			// get dims
			int pingDim = (int) ncGroup.getDimension(SonarNcGriddedDataConstants.PING_DIM).getLength();
			int rangeDim = (int) ncGroup.getDimension(SonarNcGriddedDataConstants.RANGE_DIM).getLength();
			if (rangeDim < 2)
				throw new GIOException("Texture of group " + ncGroup.getPath()
						+ " will not be visible, the dimensions are insufficient");

			// Compute texture data
			ByteBuffer backscatterBuffer = loadBackscatter(ncGroup, pingDim, rangeDim, frequencyIndex);
			if (progress.isCanceled())
				throw new OperationCanceledException();
			progress.worked(50);

			// compute texture coordinates from positions
			List<IWWTextureCoordinate> coordinates = computeCoordinates(ncGroup, pingDim, rangeDim, progress.split(50));
			if (sonarNcInfo.getGeoBox() == null) {
				updateGeobox(coordinates);
			}

			return new WWTextureData(pingDim, rangeDim, backscatterBuffer, coordinates, true);
		} finally {
			sonarNcInfo.close();
		}
	}

	/** Compute geographic statistics and update geobox of sonar file */
	private void updateGeobox(List<IWWTextureCoordinate> coordinates) {
		GeoBox geobox = null;
		for (IWWTextureCoordinate coord : coordinates) {
			double lat = coord.getPosition().getLatitude().getDegrees();
			double lon = coord.getPosition().getLongitude().getDegrees();
			if (geobox == null) {
				geobox = new GeoBox(lat, lat, lon, lon);
			}
			geobox.setTop(Math.max(lat, geobox.getTop()));
			geobox.setBottom(Math.min(lat, geobox.getBottom()));
			geobox.setRight(Math.max(lon, geobox.getRight()));
			geobox.setLeft(Math.min(lon, geobox.getLeft()));
		}
		if (geobox != null)
			sonarNcInfo.setGeoBox(geobox);
	}

	/**
	 * Slice the VLEN integrated_backscatter and load in buffer
	 */
	public ByteBuffer loadBackscatter(NetcdfGroup group, int pingDim, int rangeDim, int frequencyIndex)
			throws NCException, GIOException {
		String dataLayerName = GridGroup1Layers.INTEGRATED_BACKSCATTER_VARIABLE_NAME;

		// read data for each layer
		NetcdfVariable integratedBackscatterVar = group.getVariable(dataLayerName);
		if (integratedBackscatterVar == null)
			throw new GIOException(String.format("The expecting variable %s is missing from the group : %s",
					dataLayerName, group.getName()));
		ByteBuffer tmpdataBuffer = ByteBuffer.allocateDirect(pingDim * rangeDim * Float.BYTES).order(NATIVE_ORDER);
		integratedBackscatterVar.read(new long[] { 0, 0, frequencyIndex }, new long[] { pingDim, rangeDim, 1 },
				tmpdataBuffer, DataType.FLOAT, Optional.empty());

		// Transpose
		float fillValue = integratedBackscatterVar.getFloatFillValue();
		ByteBuffer dataBuffer = ByteBuffer.allocateDirect(pingDim * rangeDim * Float.BYTES).order(NATIVE_ORDER);
		FloatBuffer floatBuffer = dataBuffer.asFloatBuffer();
		FloatBuffer tmpFloatBuffer = tmpdataBuffer.asFloatBuffer();
		for (int i = 0; i < pingDim; i++) {
			for (int j = 0; j < rangeDim; j++) {
				int from = i * rangeDim + j;
				int to = j * pingDim + i;
				float value = tmpFloatBuffer.get(from);
				if (value == fillValue || value == -1e7) {
					// Missing values as Float.NaN
					value = Float.NaN;
				}
				floatBuffer.put(to, value);
			}
		}

		dataBuffer.rewind(); // required before sending texture to renderer

		return dataBuffer;
	}

	private List<IWWTextureCoordinate> computeCoordinates(NetcdfGroup group, int pingDim, int rangeDim,
			IProgressMonitor monitor) throws NCException {
		// read position variables
		NetcdfVariable latVariable = group.getVariable(SonarNcGriddedDataConstants.CELL_LATITUDE_VAR);
		ByteBuffer latBuffer = ByteBuffer.allocateDirect(pingDim * rangeDim * Double.BYTES).order(NATIVE_ORDER);
		latVariable.read(new long[] { pingDim, rangeDim, 1 }, latBuffer, DataType.DOUBLE, Optional.empty());

		NetcdfVariable lonVariable = group.getVariable(SonarNcGriddedDataConstants.CELL_LONGITUDE_VAR);
		ByteBuffer lonBuffer = ByteBuffer.allocateDirect(pingDim * rangeDim * Double.BYTES).order(NATIVE_ORDER);
		lonVariable.read(new long[] { pingDim, rangeDim, 1 }, lonBuffer, DataType.DOUBLE, Optional.empty());

		NetcdfVariable depthVariable = group.getVariable(SonarNcGriddedDataConstants.CELL_DEPTH_VAR);
		ByteBuffer depthBuffer = ByteBuffer.allocateDirect(rangeDim * Float.BYTES).order(NATIVE_ORDER);
		depthVariable.read(new long[] { rangeDim, 1 }, depthBuffer, DataType.FLOAT, Optional.empty());

		// supplier of top and bottom positions & column index by position
		IntFunction<Position> topSupplier = positionIndex -> new Position(//
				latBuffer.getDouble(positionIndex * rangeDim * Double.BYTES), //
				lonBuffer.getDouble(positionIndex * rangeDim * Double.BYTES), //
				-depthBuffer.getFloat(0));

		IntFunction<Position> botttomSupplier = positionIndex -> new Position(
				latBuffer.getDouble(((positionIndex + 1) * rangeDim - 1) * Double.BYTES),
				lonBuffer.getDouble(((positionIndex + 1) * rangeDim - 1) * Double.BYTES),
				-depthBuffer.getFloat((rangeDim - 1) * Float.BYTES));

		// compute texture coordinates from positions
		return computeTextureCoordinates(pingDim, rangeDim, topSupplier, botttomSupplier, monitor);
	}

	/**
	 * Computes texture coordinates from coordinates of top/bottom cell's center.
	 *
	 * @param positionCount : number of provided position
	 * @param height : texture height
	 * @param topSupplier : top coordinate supplier (give the cell center position)
	 * @param bottomSupplier: bottom coordinate supplier (give the cell center position)
	 *
	 * @return a list of {@link IWWTextureData.IWWTextureCoordinate} (coordinates are ordered by couples top-bottom
	 *         points)
	 */
	private List<IWWTextureCoordinate> computeTextureCoordinates(int positionCount, int height,
			IntFunction<Position> topSupplier, IntFunction<Position> bottomSupplier, IProgressMonitor monitor) {
		SubMonitor progress = SubMonitor.convert(monitor, positionCount);

		var result = new ArrayList<IWWTextureCoordinate>();

		// s is the relative position along the texture width (between 0 & 1)
		double s = 0d;
		for (int i = 0; !progress.isCanceled() && i < positionCount; i++) {
			Position top = topSupplier.apply(i);
			Position bottom = bottomSupplier.apply(i);
			boolean isFirst = i == 0;
			boolean isLast = i == positionCount - 1;

			// Shift first coordinate of 1/2 cell vector to outside to get the left bound instead of cell centers
			if (isFirst) {
				extrapolate(top, topSupplier.apply(1));
				extrapolate(bottom, bottomSupplier.apply(1));
			}

			// Shift top & bottom along the texture height to get the top/bottom bounds instead of cell centers
			double deltaLat = (top.lat - bottom.lat) / (height - 1.) / 2.;
			double deltaLon = (top.lon - bottom.lon) / (height - 1.) / 2.;
			double deltaElevation = (top.elevation - bottom.elevation) / (height - 1.) / 2.;
			top.lat += deltaLat;
			top.lon += deltaLon;
			top.elevation += deltaElevation;
			bottom.lat -= deltaLat;
			bottom.lon -= deltaLon;
			bottom.elevation -= deltaElevation;

			// Shift last coordinate of 1/2 cell vector to outside to get the right bound instead of cell centers
			if (isLast) {
				extrapolate(top, topSupplier.apply(i - 1));
				extrapolate(bottom, bottomSupplier.apply(i - 1));
				s = 1d;
			}

			// add top & bottom coordinates for this 's'
			result.add(WWTextureDataUtils.buildTopTextureCoordinate(top.lat, top.lon, top.elevation, (float) s));
			result.add(WWTextureDataUtils.buildBottomTextureCoordinate(bottom.lat, bottom.lon, bottom.elevation,
					(float) s));

			s += 1d / (positionCount - 1d);

			progress.worked(1);
		}
		progress.done();

		return result;
	}

	/**
	 * Translate position A with a vector computed by (A - B)/2
	 */
	private void extrapolate(Position a, Position b) {
		a.lat += (a.lat - b.lat) / 2.;
		a.lon += (a.lon - b.lon) / 2.;
		a.elevation += (a.elevation - b.elevation) / 2.;
	}

	/**
	 * Private utility class to define a position.
	 */
	private class Position {
		double lat;
		double lon;
		float elevation;

		public Position(double lat, double lon, float elevation) {
			this.lat = lat;
			this.lon = lon;
			this.elevation = elevation;
		}
	}

}
