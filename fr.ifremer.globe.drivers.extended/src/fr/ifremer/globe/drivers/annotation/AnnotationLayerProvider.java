package fr.ifremer.globe.drivers.annotation;

import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.annotation.AnnotationFileInfo;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWAnnotationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Provider of layer to render an annotation file.
 */
@Component(name = "globe_drivers_annotation_layer_provider", service = { IWWLayerProvider.class,
		IIcon16ForFileProvider.class })
public class AnnotationLayerProvider extends BasicWWLayerProvider<AnnotationFileInfo>
		implements IIcon16ForFileProvider {

	/** Osgi Service used to create WW layers */
	@Reference
	private IWWLayerFactory layerFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return Set.of(ContentType.GEOJSON_GDAL);
	}

	/**
	 * Create the layers to represent the annotation file in the 3D Viewer
	 */
	@Override
	protected void createLayers(AnnotationFileInfo fileInfo, WWFileLayerStore layerStore, IProgressMonitor monitor)
			throws GIOException {
		// Annotations
		IWWAnnotationLayer annotationLayer = layerFactory.createAnnotationLayer(fileInfo);
		fileInfo.getAnnotations().forEach(annotationLayer::acceptAnnotation);
		layerStore.addLayers(annotationLayer);

		// Labels
		layerStore.addLayers(annotationLayer.getLabelLayer());

		// Markers
		annotationLayer.getMarkerLayer().ifPresent(layerStore::addLayers);

		monitor.done();
	}

	@Override
	public Set<ContentType> getContentTypes() {
		return getContentType();
	}

	@Override
	public Optional<Image> getIcon() {
		return Optional.of(Icons.ANNOTATION.toImage());
	}
}