/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.pointcloud;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.pointcloud.PointCloudFileInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.pointcloud.PointCloudFieldInfo;
import fr.ifremer.globe.ui.service.geographicview.IGeographicViewService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWColorScaleLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.annotation.IWWLabelLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.pointcloud.IWWPointCloudLayer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Provides layers for {@link PointCloudFileInfo}.
 */
@Component(name = "globe_drivers_pointcloud_layer_provider", service = IWWLayerProvider.class)
public class PointCloudLayerProvider extends BasicWWLayerProvider<PointCloudFileInfo> {

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return Set.of(ContentType.POINT_CLOUD_CSV);
	}

	/** {@inheritDoc} */
	@Override
	protected void createLayers(PointCloudFileInfo pointCloudFileInfo, WWFileLayerStore layerStore,
			IProgressMonitor monitor) throws GIOException {
		// Force the loading of the first field
		var fieldInfo = pointCloudFileInfo.getFieldInfos().get(0);
		layerStore.addLayers(getPointCloudLayers(pointCloudFileInfo, fieldInfo, monitor));
		monitor.done();
	}

	/** {@inheritDoc} */
	@Override
	public List<IWWLayer> getPointCloudLayers(IFileInfo fileInfo, PointCloudFieldInfo fieldInfo,
			IProgressMonitor monitor) throws GIOException {
		if (fileInfo instanceof PointCloudFileInfo pointCloudFileInfo) {
			var genuineFieldInfo = pointCloudFileInfo.getFieldInfos().stream()
					.filter(f -> f.name().equals(fieldInfo.name())).findFirst();
			if (genuineFieldInfo.isPresent()) {
				IWWPointCloudLayer layer = layerFactory.createPointCloudLayer(pointCloudFileInfo,
						genuineFieldInfo.get());
				layer.setName(generateLayerName(pointCloudFileInfo, genuineFieldInfo.get()));
				IWWColorScaleLayer colorScale = layerFactory.createColorScaleLayer(layer,
						fieldInfo.name() + " " + pointCloudFileInfo.getFilename());
				return List.of(layer, colorScale);
			}
		}
		return List.of();
	}

	/**
	 * Restore PointCloudLayer of the PointCloudFileInfo with all its field info
	 */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		WWFileLayerStore result = null;
		if (fileInfo instanceof PointCloudFileInfo pointCloudFileInfo) {
			if (!pointCloudFileInfo.getFieldInfos().isEmpty()) {
				IGeographicViewService geographicViewService = IGeographicViewService.grab();
				result = new WWFileLayerStore(pointCloudFileInfo);
				// First layer always loaded
				boolean first = true;
				// Iterator to avoid concurrent modification
				var iterator = pointCloudFileInfo.getFieldInfos().iterator();
				while (iterator.hasNext()) {
					PointCloudFieldInfo fieldInfo = iterator.next();
					String layerName = generateLayerName(pointCloudFileInfo, fieldInfo);
					// Other layers than first are restored if stored in the session file (and so predict in
					// IGeographicViewService)
					if (first || geographicViewService.isPredictedLayer(layerName)) {
						first = false;
						IWWPointCloudLayer layer = layerFactory.createPointCloudLayer(pointCloudFileInfo, fieldInfo);
						layer.setName(layerName);
						IWWColorScaleLayer colorScale = layerFactory.createColorScaleLayer(layer,
								fieldInfo.name() + " " + pointCloudFileInfo.getFilename());
						result.addLayers(layer, colorScale);
					}
				}

				// Need an annotation layer ?
				boolean hasLabel = pointCloudFileInfo.getPoints().stream().anyMatch(p -> p.getLabel().isPresent());
				if (hasLabel) {
					IWWLabelLayer labelLayer = layerFactory.createLabelLayer(pointCloudFileInfo);
					labelLayer.setName(pointCloudFileInfo.getBaseName() + "_annotations");
					labelLayer.setEnabled(false);
					pointCloudFileInfo.getPoints()//
							.stream()//
							.filter(p -> p.getLabel().isPresent())//
							.forEach(p -> labelLayer.acceptLabel(p.getLabel().get(), p.getLongitude(), p.getLatitude(),
									p.getElevation()));
					labelLayer.setVisibilityDistance(1e5);
					result.addLayers(labelLayer);
				}

			}
		}
		return Optional.ofNullable(result);
	}

	/**
	 * @return the name of the layer for the specified field
	 */
	private String generateLayerName(PointCloudFileInfo pointCloudFileInfo, PointCloudFieldInfo fieldInfo) {
		return pointCloudFileInfo.getBaseName() + "_" + fieldInfo.name();
	}

}
