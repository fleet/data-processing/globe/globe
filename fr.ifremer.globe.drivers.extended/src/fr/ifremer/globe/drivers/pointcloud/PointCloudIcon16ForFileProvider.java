/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.pointcloud;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.pointcloud.IPointCloudFileInfo;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a {@link IPointCloudFileInfo} file.
 */
@Component(name = "globe_drivers_pointcloud_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class PointCloudIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public PointCloudIcon16ForFileProvider() {
		super("icons/16/pointcloud.png", ContentType.POINT_CLOUD_CSV);
	}
}
