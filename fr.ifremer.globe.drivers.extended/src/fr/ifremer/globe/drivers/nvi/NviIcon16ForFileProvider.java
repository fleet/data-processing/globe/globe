/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.nvi;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a mbg file.
 */
@Component(name = "globe_drivers_nvi_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class NviIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public NviIcon16ForFileProvider() {
		super("icons/16/navigation.png", ContentType.NVI_NETCDF_4, ContentType.NVI_V2_NETCDF_4,
				ContentType.NAVIGATION_CSV);
	}
}
