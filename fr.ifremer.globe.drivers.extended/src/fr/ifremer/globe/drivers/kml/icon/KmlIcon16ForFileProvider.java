/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.kml.icon;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a kml file.
 */
@Component(name = "globe_drivers_kml_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class KmlIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public KmlIcon16ForFileProvider() {
		super("icons/16/kml.png", ContentType.KML_XML);
	}
}