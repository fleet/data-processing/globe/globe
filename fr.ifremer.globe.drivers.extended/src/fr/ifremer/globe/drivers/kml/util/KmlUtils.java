/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.kml.util;

import java.util.ArrayList;
import java.util.List;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.ogc.kml.KMLAbstractContainer;
import gov.nasa.worldwind.ogc.kml.KMLAbstractFeature;
import gov.nasa.worldwind.ogc.kml.KMLAbstractGeometry;
import gov.nasa.worldwind.ogc.kml.KMLAbstractStyleSelector;
import gov.nasa.worldwind.ogc.kml.KMLAbstractView;
import gov.nasa.worldwind.ogc.kml.KMLCamera;
import gov.nasa.worldwind.ogc.kml.KMLDocument;
import gov.nasa.worldwind.ogc.kml.KMLGroundOverlay;
import gov.nasa.worldwind.ogc.kml.KMLLineString;
import gov.nasa.worldwind.ogc.kml.KMLLinearRing;
import gov.nasa.worldwind.ogc.kml.KMLLocation;
import gov.nasa.worldwind.ogc.kml.KMLLookAt;
import gov.nasa.worldwind.ogc.kml.KMLModel;
import gov.nasa.worldwind.ogc.kml.KMLMultiGeometry;
import gov.nasa.worldwind.ogc.kml.KMLPair;
import gov.nasa.worldwind.ogc.kml.KMLPlacemark;
import gov.nasa.worldwind.ogc.kml.KMLPoint;
import gov.nasa.worldwind.ogc.kml.KMLPolyStyle;
import gov.nasa.worldwind.ogc.kml.KMLPolygon;
import gov.nasa.worldwind.ogc.kml.KMLRoot;
import gov.nasa.worldwind.ogc.kml.KMLStyle;
import gov.nasa.worldwind.ogc.kml.KMLStyleMap;

/**
 * Commons utility methods
 */
public class KmlUtils {

	/** Constructor */
	private KmlUtils() {
	}

	/** Try to determine the sector of the KML file */
	public static Sector findSector(KMLRoot root) {
		Sector result = null;
		KMLAbstractFeature feature = root.getFeature();
		if (feature != null) {
			result = findSector(feature);
			if (result == null) {
				KMLAbstractView view = feature.getView();
				if (view instanceof KMLCamera) {
					result = findSector((KMLCamera) view);
				} else if (view instanceof KMLLookAt) {
					result = findSector((KMLLookAt) view);
				}
			}
		}
		if (result != null && result.getDeltaLatDegrees() == 0 && result.getDeltaLonDegrees() == 0) {
			result = new Sector(//
					result.getMinLatitude().addDegrees(-0.01), //
					result.getMaxLatitude().addDegrees(0.01), //
					result.getMinLongitude().addDegrees(-0.01), //
					result.getMaxLongitude().addDegrees(0.01)//
			);
		}
		return result;
	}

	/** Try to determine the sector of the KML Feature */
	protected static Sector findSector(KMLAbstractFeature feature) {
		Sector result = null;

		if (feature instanceof KMLPlacemark) {
			result = findSector((KMLPlacemark) feature);
		} else if (feature instanceof KMLGroundOverlay) {
			result = findSector((KMLGroundOverlay) feature);
		} else if (feature instanceof KMLAbstractContainer) {
			for (KMLAbstractFeature innerFeature : ((KMLAbstractContainer) feature).getFeatures()) {
				Sector sector = findSector(innerFeature);
				if (sector != null) {
					if (result != null) {
						result.union(sector);
					} else {
						result = sector;
					}
				}
			}
		}

		return result;
	}

	/** Try to determine the sector of the KML Camera */
	public static Sector findSector(KMLCamera camera) {
		Sector result = null;

		Double latitude = camera.getLatitude();
		Double longitude = camera.getLongitude();
		if (latitude != null && longitude != null) {
			result = Sector.fromDegrees(latitude.doubleValue(), latitude.doubleValue(), longitude.doubleValue(),
					longitude.doubleValue());
		}

		return result;
	}

	/** Try to determine the sector of the KML LookAt */
	public static Sector findSector(KMLLookAt lookAt) {
		Sector result = null;

		Double latitude = lookAt.getLatitude();
		Double longitude = lookAt.getLongitude();
		if (latitude != null && longitude != null) {
			result = Sector.fromDegrees(latitude.doubleValue(), latitude.doubleValue(), longitude.doubleValue(),
					longitude.doubleValue());
		}

		return result;
	}

	/** Try to determine the sector of the KML Placemark */
	public static Sector findSector(KMLPlacemark placemark) {
		Sector result = null;

		// Find all the points in the placemark. We want to bring the entire placemark into view.
		KMLAbstractGeometry geometry = placemark.getGeometry();
		List<Position> positions = new ArrayList<>();
		collectPositions(geometry, positions);

		if (!positions.isEmpty()) {
			result = Sector.boundingSector(positions);
		}
		return result;
	}

	/** Try to determine the sector of the KML GroundOverlay */
	public static Sector findSector(KMLGroundOverlay groundOverlay) {
		Sector result = null;
		List<? extends Position> positions = groundOverlay.getPositions().list;
		if (!positions.isEmpty()) {
			result = Sector.boundingSector(positions);
		}
		return result;
	}

	/**
	 * Get all of the positions that make up a {@link KMLAbstractGeometry}. If the geometry contains other geometries,
	 * this method collects all the points from all of the geometries.
	 *
	 * @param geometry Geometry to collect positions from.
	 * @param positions Placemark positions will be added to this list.
	 */
	public static void collectPositions(KMLAbstractGeometry geometry, List<Position> positions) {
		if (geometry instanceof KMLPoint) {
			collectPosition((KMLPoint) geometry, positions);
		} else if (geometry instanceof KMLModel) {
			collectPosition((KMLModel) geometry, positions);
		} else if (geometry instanceof KMLLineString) {
			collectPositions((KMLLineString) geometry, positions);
		} else if (geometry instanceof KMLPolygon) {
			collectPositions((KMLPolygon) geometry, positions);
		} else if (geometry instanceof KMLMultiGeometry) {
			collectPositions((KMLMultiGeometry) geometry, positions);
		}
	}

	/**
	 * Get the position that make up a {@link KMLPoint}.
	 *
	 * @param point Geometry to KMLPoint position from.
	 * @param positions Positions will be added to this list.
	 */
	public static void collectPosition(KMLPoint point, List<Position> positions) {
		Position pos = point.getCoordinates();
		if (pos != null) {
			positions.add(pos);
		}
	}

	/**
	 * Get the position that make up a {@link KMLModel}.
	 *
	 * @param model Geometry to KMLModel position from.
	 * @param positions Positions will be added to this list.
	 */
	public static void collectPosition(KMLModel model, List<Position> positions) {
		KMLLocation location = model.getLocation();
		if (location != null) {
			Position pos = location.getPosition();
			if (pos != null) {
				positions.add(pos);
			}
		}
	}

	/**
	 * Get all the positions that make up a {@link KMLLineString}.
	 *
	 * @param lineString Geometry to KMLLineString position from.
	 * @param positions Positions will be added to this list.
	 */
	public static void collectPositions(KMLLineString lineString, List<Position> positions) {
		Position.PositionList positionList = lineString.getCoordinates();
		if (positionList != null) {
			positions.addAll(positionList.list);
		}
	}

	/**
	 * Get all the positions that make up a {@link KMLPolygon}.
	 *
	 * @param polygon Geometry to KMLPolygon position from.
	 * @param positions Positions will be added to this list.
	 */
	public static void collectPositions(KMLPolygon polygon, List<Position> positions) {
		KMLLinearRing ring = polygon.getOuterBoundary();
		collectPositions(ring, positions);
	}

	/**
	 * Get all the positions that make up a {@link KMLMultiGeometry}.
	 *
	 * @param geometry Geometry to KMLMultiGeometry position from.
	 * @param positions Positions will be added to this list.
	 */
	public static void collectPositions(KMLMultiGeometry multiGeometry, List<Position> positions) {
		for (KMLAbstractGeometry geometry : multiGeometry.getGeometries()) {
			// Recurse, adding positions for the sub-geometry
			collectPositions(geometry, positions);
		}
	}

	/** Change the fill field of all KMLPolyStyles of the KML */
	public static boolean changeFillPolyStyle(KMLRoot root, Boolean fill) {
		boolean result = false;
		KMLAbstractFeature feature = root.getFeature();
		if (feature != null) {
			result = changeFillPolyStyle(feature, fill);
		}
		return result;
	}

	/** Change the fill field of all KMLPolyStyles of the feature */
	protected static boolean changeFillPolyStyle(KMLAbstractFeature feature, Boolean extrude) {
		boolean result = false;
		if (feature instanceof KMLDocument) {
			for (KMLAbstractFeature subFeature : ((KMLDocument) feature).getFeatures()) {
				result |= changeFillPolyStyle(subFeature, extrude);
			}
		} else if (feature instanceof KMLPlacemark) {
			for (KMLAbstractStyleSelector styleSelector : ((KMLPlacemark) feature).getStyleSelectors()) {
				result |= changeFillPolyStyle(styleSelector, extrude);
			}
		}
		return result;
	}

	/** Change the fill field of all KMLPolyStyles of the KMLAbstractStyleSelector */
	protected static boolean changeFillPolyStyle(KMLAbstractStyleSelector styleSelector, Boolean fill) {
		boolean result = false;
		if (styleSelector instanceof KMLStyleMap) {
			for (KMLPair pair : ((KMLStyleMap) styleSelector).getPairs()) {
				result |= changeFillPolyStyle(pair.getStyleSelector(), fill);
			}
		} else if (styleSelector instanceof KMLStyle) {
			KMLPolyStyle polyStyle = ((KMLStyle) styleSelector).getPolyStyle();
			if (polyStyle != null) {
				polyStyle.setField("fill", fill);
				result = true;
			}
		}
		return result;
	}
}
