/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.kml.info;

import java.io.IOException;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.FilenameUtils;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.FileInfoSupplierPriority;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.drivers.kml.util.KmlUtils;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.ogc.kml.KMLRoot;

/**
 * Loader of KML/KMZ file info. Builder of BasicFileInfo
 */
@Component(name = "globe_drivers_kml_file_info_supplier", //
		service = { KmlInfoLoader.class, IFileInfoSupplier.class }, //
		property = {
				FileInfoSupplierPriority.PROPERTY_PRIORITY + ":Integer=" + FileInfoSupplierPriority.KML_XML_PRIORITY })
public class KmlInfoLoader implements IFileInfoSupplier<BasicFileInfo> {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(KmlInfoLoader.class);

	/** Supported Kml type */
	protected static final EnumSet<ContentType> CONTENT_TYPES = EnumSet.of(ContentType.KML_XML);

	/** Extensions **/
	public static final String EXTENSION1 = "kml";
	public static final String EXTENSION2 = "kmz";
	protected static final List<String> EXTENSIONS = Arrays.asList(EXTENSION1, EXTENSION2);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<BasicFileInfo> getFileInfo(String filePath) {
		BasicFileInfo result = null;
		if (isKmlFile(filePath)) {
			try {
				result = loadInfo(filePath);
			} catch (GIOException e) {
				logger.warn("Error on {} loading : {}", filePath, e.getMessage());
			}
		}
		return Optional.ofNullable(result);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EnumSet<ContentType> getContentTypes() {
		return CONTENT_TYPES;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getExtensions() {
		return EXTENSIONS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Pair<String, String>> getFileFilters() {
		return Arrays.asList(//
				new Pair<>("3D models (*." + EXTENSION1 + ")", "*." + EXTENSION1), //
				new Pair<>("Compressed 3D models (*." + EXTENSION2 + ")", "*." + EXTENSION2));
	}

	/**
	 * @return the BasicFileInfo of the specified resource. null if filePath is not a KML/KMZ
	 * @throws GIOException read failed, error occured
	 */
	protected BasicFileInfo loadInfo(String filePath) throws GIOException {
		try {
			BasicFileInfo result = new BasicFileInfo(filePath, ContentType.KML_XML);
			return readMetadata(result) ? result : null;
		} catch (IOException | XMLStreamException e) {
			throw GIOException.wrap(e.getMessage(), e);
		}
	}

	/** Read metadata from the file */
	protected boolean readMetadata(BasicFileInfo kmlInfo) throws IOException, XMLStreamException {
		KMLRoot kmlRoot = KMLRoot.createAndParse(kmlInfo.getPath());
		Sector sector = KmlUtils.findSector(kmlRoot);
		if (sector != null) {
			kmlInfo.setGeoBox(GeoBoxUtils.convert(sector));
		}
		return true;
	}

	/**
	 * @return true if filePath point to a KML/KMZ file
	 */
	public boolean isKmlFile(String filePath) {
		return EXTENSIONS.contains(FilenameUtils.getExtension(filePath).toLowerCase());
	}
}