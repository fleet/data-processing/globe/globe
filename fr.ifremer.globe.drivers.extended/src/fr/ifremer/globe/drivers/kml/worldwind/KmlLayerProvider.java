/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.kml.worldwind;

import java.io.IOException;
import java.util.Observer;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.basic.BasicFileInfo;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.core.utils.preference.attributes.BooleanPreferenceAttribute;
import fr.ifremer.globe.drivers.kml.info.KmlInfoLoader;
import fr.ifremer.globe.drivers.kml.util.KmlUtils;
import fr.ifremer.globe.ui.databinding.observable.WritableBoolean;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWRenderableLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;
import fr.ifremer.globe.ui.utils.GeoBoxUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.ogc.kml.KMLRoot;
import gov.nasa.worldwind.ogc.kml.impl.KMLController;

/**
 * Driver to manage GeoKmlf files.
 */
@Component(name = "globe_drivers_kml_layer_provider", service = IWWLayerProvider.class)
public class KmlLayerProvider extends BasicWWLayerProvider<BasicFileInfo> {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(KmlLayerProvider.class);

	/** Driver of a KML file */
	@Reference
	protected KmlInfoLoader kmlInfoLoader;

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return kmlInfoLoader.getContentTypes();
	}

	/**
	 * Create the layers to represent the kml file in the 3D Viewer
	 *
	 * @param kmlInfo the kml file
	 * @param monitor monitor of progression
	 */
	@Override
	protected void createLayers(BasicFileInfo kmlInfo, WWFileLayerStore layerStore, IProgressMonitor monitor)
			throws GIOException {
		IWWRenderableLayer layer = layerFactory.createRenderableLayer();
		layer.setPickEnabled(false);
		layer.setName("Object_" + FilenameUtils.getBaseName(kmlInfo.getPath()));

		// Have to fill polygons ?
		BooleanPreferenceAttribute fillPolygonPref = (BooleanPreferenceAttribute) PreferenceRegistry.getInstance()
				.getViewsSettingsNode().getChild("3D Viewer").getAttribute("fillPolygonShape");
		KMLRoot kmlRoot = addKmlToLayer(kmlInfo, layer);

		// Polygons have to be filled ?
		if (KmlUtils.changeFillPolyStyle(kmlRoot, fillPolygonPref.getValue())) {
			WritableBoolean fillFlag = new WritableBoolean(fillPolygonPref.getValue());
			layer.setValue("WritableBoolean.fillFlag", fillFlag);

			// React when fill polygons preference changed
			Observer obs = (o, arg) -> fillFlag.set(fillPolygonPref.getValue());
			fillPolygonPref.addObserver(obs);
			layer.addDisposeListener(() -> fillPolygonPref.deleteObserver(obs));

			// React when fill flag changed
			fillFlag.addChangeListener(event -> {
				try {
					KMLRoot newKmlRoot = addKmlToLayer(kmlInfo, layer);
					KmlUtils.changeFillPolyStyle(newKmlRoot, fillFlag.getValue());
					layer.render();
				} catch (GIOException e) {
					logger.warn("Error when parsing KML file {}", e.getMessage());
				}
			});
		}

		// Set Geobox
		if (kmlInfo.getGeoBox() != null) {
			layer.setValue(AVKey.SECTOR, GeoBoxUtils.convert(kmlInfo.getGeoBox()));
		}

		layerStore.addLayers(layer);
		monitor.done();
	}

	/**
	 * Parse the KML and add the rendering object to the layer
	 *
	 * @return The KML object
	 */
	protected KMLRoot addKmlToLayer(BasicFileInfo kmlInfo, IWWRenderableLayer layer) throws GIOException {
		try {
			KMLRoot kmlRoot = KMLRoot.createAndParse(kmlInfo.getPath());
			layer.removeAllRenderables();
			layer.addRenderable(new KMLController(kmlRoot));
			return kmlRoot;
		} catch (IOException | XMLStreamException e) {
			throw GIOException.wrap(e.getMessage(), e);
		}
	}
}