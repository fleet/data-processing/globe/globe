/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.csv.service.worldwind;

import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.csv.info.CsvFileInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWGeoBoxLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 *
 */
@Component(name = "globe_drivers_csv_layer_provider", service = IWWLayerProvider.class)
public class BathymetricCsvLayerProvider implements IWWLayerProvider {

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	@Override
	public Set<ContentType> getContentType() {
		return EnumSet.of(ContentType.EMO_CSV, ContentType.BATHIMETRIC_CSV);
	}

	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		WWFileLayerStore result = null;
		if (fileInfo instanceof CsvFileInfo) {
			CsvFileInfo csvFileInfo = (CsvFileInfo) fileInfo;
			IWWGeoBoxLayer layer = createLayers(csvFileInfo, monitor);
			if (layer != null) {
				result = new WWFileLayerStore(fileInfo, layer);
			}
		}
		return Optional.ofNullable(result);
	}

	public IWWGeoBoxLayer createLayers(CsvFileInfo emoFileInfo, IProgressMonitor monitor) {
		IWWGeoBoxLayer result = null;
		GeoBox geoBox = emoFileInfo.getGeoBox();
		if (geoBox != null) {
			result = layerFactory.createGeoBoxLayer(geoBox);
			result.setName(emoFileInfo.getBaseName());
		}
		monitor.done();
		return result;
	}

}
