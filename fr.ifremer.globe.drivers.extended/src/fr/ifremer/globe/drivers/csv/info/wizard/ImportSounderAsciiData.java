/**
 *
 */
package fr.ifremer.globe.drivers.csv.info.wizard;

import java.util.Optional;
import java.util.function.Function;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;
import fr.ifremer.globe.ui.wizard.AsciiDelimiterPage;

/**
 * Wizard to define driver parameters for reading a {@link fr.ifremer.globe.PointCloudFileInfoSettings.RawFile}<br>
 * <br>
 * Show the {@link fr.ifremer.globe.ui.wizard.AsciiDelimiterPage} page <br>
 */
public class ImportSounderAsciiData extends Wizard {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(ImportSounderAsciiData.class);

	/** Kind of CSV, model */
	protected WritableObject<ContentType> csvKind = new WritableObject<>(null);

	/** Pages */
	private KindOfCsvPage kindOfCsvPage;
	private AsciiDelimiterPage asciiDelimiterPage;

	private String filePath;

	/** Supplier of fileInfoSettingsSupplier */
	protected Function<ContentType, Optional<FileInfoSettings>> fileInfoSettingsSupplier;

	private FileInfoSettingsAdapter settingsAdapter;

	/**
	 * Constructor
	 */
	public ImportSounderAsciiData(String filePath,
			Function<ContentType, Optional<FileInfoSettings>> fileInfoSettingsSupplier) {
		this.filePath = filePath;
		this.fileInfoSettingsSupplier = fileInfoSettingsSupplier;

		csvKind.addChangeListener(e -> computeNextPage());
	}

	@Override
	public boolean performFinish() {
		if (asciiDelimiterPage != null && settingsAdapter != null) {
			settingsAdapter.setDelimiter(asciiDelimiterPage.getDelimiter());
			settingsAdapter.setDepthPositiveBelowSurface(asciiDelimiterPage.isDepthPositiveBelowSurface());
			settingsAdapter.setHeaderIndex(asciiDelimiterPage.getHeaderIndex());
			settingsAdapter.setValueIndexes(asciiDelimiterPage.getValueIndexes());
			settingsAdapter.setStartingLine(asciiDelimiterPage.getStartingLine());
			settingsAdapter.setDecimalPointLabel(asciiDelimiterPage.getDecimalPointLabel());
		}
		return true;
	}

	@Override
	public void addPages() {
		kindOfCsvPage = new KindOfCsvPage(csvKind);
		addPage(kindOfCsvPage);

		// Fake a second dummy page. Real one is created when getNextPage() is invoked
		WizardPage dummyPage = new WizardPage("Dummy page") {
			@Override
			public void createControl(Composite parent) {
				setControl(new Composite(parent, SWT.NONE));
			}
		};
		addPage(dummyPage);
	}

	protected void computeNextPage() {
		if (asciiDelimiterPage != null) {
			asciiDelimiterPage.dispose();
			asciiDelimiterPage = null;
		}
		if (csvKind.isNotNull()) {
			var fileInfoSettings = fileInfoSettingsSupplier.apply(csvKind.get()).orElse(null);
			if (fileInfoSettings != null) {
				kindOfCsvPage.setErrorMessage(null);
				if (fileInfoSettings.size() > 1) { // not only FileInfoSettings.KEY_CONTENT_TYPE
					settingsAdapter = new FileInfoSettingsAdapter(filePath, fileInfoSettings);
					asciiDelimiterPage = new AsciiDelimiterPage(settingsAdapter, false);
					asciiDelimiterPage.setWizard(this);
					asciiDelimiterPage.setTitle("Import text sounder file: \n" + settingsAdapter.getPath());
					asciiDelimiterPage.setDescription("Select ASCII parameters");
				}
			} else {
				kindOfCsvPage.setErrorMessage("The file format is not of this type");
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public IWizardPage getPreviousPage(IWizardPage page) {
		return page == asciiDelimiterPage ? kindOfCsvPage : null;
	}

	/** {@inheritDoc} */
	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		return page == kindOfCsvPage ? asciiDelimiterPage : null;
	}

	/** {@inheritDoc} */
	@Override
	public boolean canFinish() {
		return csvKind.isNotNull() // Selection has been made
				&& kindOfCsvPage.getErrorMessage() == null // No error has been detected
				&& (asciiDelimiterPage == null // No parameter is required or parameter page is completed
						|| getContainer().getCurrentPage() == asciiDelimiterPage
								&& asciiDelimiterPage.isPageComplete());
	}

}
