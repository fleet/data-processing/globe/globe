/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.csv.info.wizard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.primitives.IntList;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IxyzFile;

/**
 * Adapter of FileInfoSettings (settings of a file parsing, persisting in the session) to a IxyzFile. <br>
 * FileInfoSettings is dedicated to a CSV file
 */
public class FileInfoSettingsAdapter implements IxyzFile {

	/** Csv File path */
	protected String filePath;
	/** Adapted FileInfoSettings */
	protected FileInfoSettings settings;
	/** True when all fields are initialized and a CSV file can be parse with */
	protected boolean comprehensive = false;

	/** Bidirectional lookup between FileInfoSettings and IxyzFile columns */
	protected BidiMap<String, String> columnNameMapping = new DualHashBidiMap<>();

	/**
	 * Constructor
	 */
	public FileInfoSettingsAdapter(String filePath, FileInfoSettings fileInfoSettings) {
		this.filePath = filePath;
		settings = fileInfoSettings;

		columnNameMapping.put(FileInfoSettings.KEY_COLUMN_LONGITUDE_INDEX, IxyzFile.LONGITUDE);
		columnNameMapping.put(FileInfoSettings.KEY_COLUMN_LATITUDE_INDEX, IxyzFile.LATITUDE);
		columnNameMapping.put(FileInfoSettings.KEY_COLUMN_ELEVATION_INDEX, IxyzFile.DEPTH);
	}

	/**
	 * Called by CsvComposite when edition of columns is done. <br>
	 * First column in headerIndex has the index 0.
	 */
	protected void adaptColumnIndexes(Map<String, Integer> headerIndex) {
		// First, remove all optional columns. They may have been unset by edition
		getOptionalHeader().forEach(settings::remove);

		for (var entry : headerIndex.entrySet()) {
			String colName = entry.getKey();
			settings.put(columnNameMapping.containsValue(colName) ? columnNameMapping.getKey(colName) : colName,
					String.valueOf(entry.getValue()));
		}
	}

	/**
	 * Called by CsvComposite when initializing the edition of columns. <br>
	 * First column must have the index 1.
	 */
	protected Map<String, Integer> adaptColumnIndexes() {
		Set<String> notCsvColumns = Set.of(FileInfoSettings.KEY_CONTENT_TYPE, FileInfoSettings.KEY_LOCALE,
				FileInfoSettings.KEY_DELIMITER, FileInfoSettings.KEY_ELEVATION_SCALE_FACTOR,
				FileInfoSettings.KEY_ROW_COUNT_TO_SKIP, FileInfoSettings.KEY_NOT_NUMERIC_COLUMNS,
				FileInfoSettings.KEY_OPTIONAL_COLUMNS);

		Map<String, Integer> result = new HashMap<>();
		for (var entry : settings.entrySet()) {
			if (!notCsvColumns.contains(entry.getKey())) {
				try {
					int index = Integer.parseInt(entry.getValue());
					String colName = entry.getKey();
					result.put(columnNameMapping.getOrDefault(colName, colName), index + 1);
				} catch (NumberFormatException e) {
					// Not an Integer : entry ignored
				}
			}
		}
		return result;
	}

	/* @formatter:off
		   __    ____    __    ____  ____  ____  ____
		  /__\  (  _ \  /__\  (  _ \(_  _)( ___)(  _ \
		 /(__)\  )(_) )/(__)\  )___/  )(   )__)  )   /
		(__)(__)(____/(__)(__)(__)   (__) (____)(_)\_)

	@formatter:on */

	/** {@inheritDoc} */
	@Override
	public void setHeaderIndex(Map<String, Integer> headerIndex) {
		adaptColumnIndexes(headerIndex);
	}

	/** {@inheritDoc} */
	@Override
	public Map<String, Integer> getHeaderIndex() {
		return adaptColumnIndexes();
	}

	@Override
	public String getPath() {
		return filePath;
	}

	@Override
	public void setDelimiter(char text) {
		settings.setDelimiter(text);
	}

	@Override
	public char getDelimiter() {
		return settings.getDelimiter();
	}

	@Override
	public boolean isDepthPositiveBelowSurface() {
		return settings.getElevationScaleFactor() == -1;
	}

	@Override
	public void setDepthPositiveBelowSurface(boolean value) {
		settings.setElevationScaleFactor(value ? -1d : 1d);
	}

	@Override
	public List<String> getMandatoryHeader() {
		var result = new ArrayList<>(adaptColumnIndexes().keySet());
		result.removeAll(getOptionalHeader());
		return result;
	}

	@Override
	public void setDecimalPointLabel(char s) {
		settings.setLocale(s == ',' ? Locale.FRANCE : Locale.US);
	}

	@Override
	public char getDecimalPoint() {
		return Locale.FRANCE.equals(settings.getLocale()) ? ',' : '.';
	}

	/**
	 * @return the startingLine
	 */
	@Override
	public int getStartingLine() {
		return settings.getRowCountToSkip();
	}

	/**
	 * @param the startingLine to set
	 */
	@Override
	public void setStartingLine(int startingLine) {
		settings.setRowCountToSkip(startingLine);
	}

	/**
	 * @return the {@link #comprehensive}
	 */
	public boolean isComprehensive() {
		return comprehensive;
	}

	/**
	 * @param comprehensive the {@link #comprehensive} to set
	 */
	public void setComprehensive(boolean comprehensive) {
		this.comprehensive = comprehensive;
	}

	/**
	 * @return the {@link #settings}
	 */
	public FileInfoSettings getSettings() {
		return settings;
	}

	/** {@inheritDoc} */
	@Override
	public Class<?> getType(String headerName) {
		return List.of(settings.getNonNumericColumns()).contains(headerName) ? String.class : Double.class;
	}

	@Override
	public void setType(String headerName, Class<?> type) {
		// Never append
	}

	/** {@inheritDoc} */
	@Override
	public List<String> getOptionalHeader() {
		return List.of(settings.getOptionalColumns());
	}

	/** {@inheritDoc} */
	@Override
	public boolean allowsMultipleValues() {
		return settings.allowsMultipleValues();
	}

	/** {@inheritDoc} */
	@Override
	public void setValueIndexes(IntList indexes) {
		settings.setValueIndexes(indexes);
	}

}
