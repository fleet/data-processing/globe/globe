/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.csv.info.wizard;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.value.SelectObservableValue;
import org.eclipse.jface.databinding.swt.typed.WidgetProperties;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.databinding.observable.WritableObject;

public class KindOfCsvPage extends WizardPage {

	/** Kind of CSV, model */
	protected WritableObject<ContentType> csvKind;

	/** Widgets, view */
	private Button btnNavigation;
	private Button btnPointCloud;
	private Button btnBathy;
	private Button btnTide;

	/** Binding model/view */
	protected DataBindingContext bindingContext;

	/**
	 * Constructor
	 */
	public KindOfCsvPage(WritableObject<ContentType> csvKind) {
		super("KindOfCsvPage");
		setTitle("Kind of CSV");
		setDescription("What kind of data contains this files ?");
		this.csvKind = csvKind;
		setPageComplete(false);
	}

	/** {@inheritDoc} */
	@Override
	public void createControl(Composite parent) {
		Group container = new Group(parent, SWT.NONE);

		setControl(container);
		container.setLayout(new GridLayout(1, false));

		Button btnFake = new Button(container, SWT.RADIO);
		GridDataFactory.fillDefaults().hint(0, 1).applyTo(btnFake);

		btnNavigation = new Button(container, SWT.RADIO);
		btnNavigation.setText("Navigation");

		btnBathy = new Button(container, SWT.RADIO);
		btnBathy.setText("Bathymetry");

		btnPointCloud = new Button(container, SWT.RADIO);
		btnPointCloud.setText("Point cloud");

		btnTide = new Button(container, SWT.RADIO);
		btnTide.setText("Tide");

		initDataBindings();

		csvKind.addChangeListener(e -> setPageComplete(csvKind.isNotNull()));
	}

	/** Initialize BoundOption bindings */
	protected void initDataBindings() {
		bindingContext = new DataBindingContext();

		var navigationObservable = WidgetProperties.buttonSelection().observe(btnNavigation);
		var bathyObservable = WidgetProperties.buttonSelection().observe(btnBathy);
		var pointCloudObservable = WidgetProperties.buttonSelection().observe(btnPointCloud);
		var tideObservable = WidgetProperties.buttonSelection().observe(btnTide);

		var csvKindObservable = new SelectObservableValue<>(ContentType.class);
		csvKindObservable.addOption(ContentType.NAVIGATION_CSV, navigationObservable);
		csvKindObservable.addOption(ContentType.BATHIMETRIC_CSV, bathyObservable);
		csvKindObservable.addOption(ContentType.POINT_CLOUD_CSV, pointCloudObservable);
		csvKindObservable.addOption(ContentType.TIDE_CSV, tideObservable);

		bindingContext.bindValue(csvKindObservable, csvKind);
	}
}
