/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.csv.info;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.FileInfoSupplierPriority;
import fr.ifremer.globe.core.io.csv.ICsvFileInfo;
import fr.ifremer.globe.core.io.csv.ICsvFileInfoSupplier;
import fr.ifremer.globe.core.io.csv.info.CsvInfoSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.drivers.csv.info.wizard.ImportSounderAsciiData;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.utils.FileUtils;

/**
 * IFileInfoSupplier managing the paramater edition of a CSV file.
 */
@Component(name = "globe_drivers_extended_csv_file_info_supplier", service = IFileInfoSupplier.class, //
		property = { FileInfoSupplierPriority.PROPERTY_PRIORITY + ":Integer="
				+ FileInfoSupplierPriority.EXTENDED_CSV_PRIORITY }//
)
public class CsvInfoSupplierExtended implements IFileInfoSupplier<ICsvFileInfo> {

	@Reference
	protected CsvInfoSupplier csvInfoSupplier;

	/** {@inheritDoc} */
	@Override
	public Optional<ICsvFileInfo> getFileInfo(String filePath) {
		var extension = FileUtils.getExtension(filePath);
		if (!csvInfoSupplier.getExtensions().contains(extension))
			// Not a CSV file
			return Optional.empty();

		List<String> firstLines = FileUtils.readLines(filePath, 10);
		if (firstLines.isEmpty())
			return Optional.empty();

		var possibleCsvSuppliers = csvInfoSupplier.getCsvFileInfoSuppliers();
		// Keep in mind the result of the wizard
		AtomicInteger returnCode = new AtomicInteger(Window.CANCEL);
		AtomicReference<FileInfoSettings> fileInfoSettings = new AtomicReference<>();
		AtomicReference<ICsvFileInfoSupplier> csvFileInfoSupplier = new AtomicReference<>();
		Display.getDefault().syncExec(() -> {
			var wizard = new ImportSounderAsciiData(filePath, csvKind -> {
				fileInfoSettings.set(null);
				csvFileInfoSupplier.set(null);
				for (var possibleCsvSupplier : possibleCsvSuppliers)
					if (possibleCsvSupplier.getContentType() == csvKind) {
						var optionSettings = possibleCsvSupplier.evaluatesSettings(filePath, firstLines);
						if (optionSettings.isPresent()) {
							csvFileInfoSupplier.set(possibleCsvSupplier);
							fileInfoSettings.set(optionSettings.get());
							return optionSettings;
						}
					}
				return Optional.empty();
			});
			var dialog = new WizardDialog(Display.getCurrent().getActiveShell(), wizard);
			dialog.setShellStyle(dialog.getShellStyle() | SWT.PRIMARY_MODAL | SWT.APPLICATION_MODAL);
			returnCode.set(dialog.open());
		});

		if (returnCode.get() == Window.OK) {
			var settings = fileInfoSettings.get();
			var supplier = csvFileInfoSupplier.get();

			if (settings != null && supplier != null) {
				// The file is suitable for a supplier
				ISessionService.grab().getPropertiesContainer().add(filePath,
						FileInfoSettings.REALM_PROPERTIES_FILE_SETTINGS, settings);
				var info = supplier.getFileInfo(filePath, settings);
				// Open error message if there is some
				info.filter(i -> !i.getParsingErrors().isEmpty()) //
						.map(ICsvFileInfo::getParsingErrors) //
						.ifPresent(errors -> {
							var msg = errors.size() + " parsing error" + (errors.size() > 1 ? "s" : "");
							for (int i = 0; i < Math.min(errors.size() - 1, 5); i++)
								msg += "\n" + errors.get(i).getLineNumber() + " : " + errors.get(i).getMessage();
							if (errors.size() > 5)
								msg += "\n...";
							Messages.openErrorMessage("CSV parsing error", msg);
						});
				return info;
			}
		} else
			// Throws an exception to stop the search for a suitable driver for this file.
			throw new CancellationException();

		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<ICsvFileInfo> getFileInfo(String filePath, FileInfoSettings settings) {
		// Not this Supplier's responsibility
		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<ICsvFileInfo> getFileInfoSilently(String filePath) {
		// Impossible to be silent !
		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentTypes() {
		return csvInfoSupplier.getContentTypes();
	}

	/** {@inheritDoc} */
	@Override
	public List<String> getExtensions() {
		return Collections.emptyList();
	}

	/** {@inheritDoc} */
	@Override
	public List<Pair<String, String>> getFileFilters() {
		return Collections.emptyList();
	}

}