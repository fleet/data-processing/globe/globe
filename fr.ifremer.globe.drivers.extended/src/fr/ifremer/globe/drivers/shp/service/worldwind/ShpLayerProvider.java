/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.shp.service.worldwind;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.shp.info.ShpInfo;
import fr.ifremer.globe.core.io.shp.info.ShpInfoSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.projection.ProjectionSettings;
import fr.ifremer.globe.gdal.GdalOgrUtils;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWShapefileLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWShapefileLayer.Data;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Driver to manage SHP files.
 */
@Component(name = "globe_drivers_shp_layer_provider", service = IWWLayerProvider.class)
public class ShpLayerProvider extends BasicWWLayerProvider<ShpInfo> {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(ShpLayerProvider.class);

	/** InfoLoader of a shp file */
	@Reference
	protected ShpInfoSupplier shpInfoSupplier;

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return shpInfoSupplier.getContentTypes();
	}

	/**
	 * Create the layers to represent the tif file in the 3D Viewer
	 *
	 * @param tifInfo the tif file
	 * @param monitor monitor of progression
	 */
	@Override
	protected void createLayers(ShpInfo shpInfo, WWFileLayerStore layerStore, IProgressMonitor monitor)
			throws GIOException {
		File shapeFile = new File(shpInfo.getPath());
		if (shpInfo.getProjection() == null || !ProjectionSettings.isLongLatProjection(shpInfo.getProjection())) {
			shapeFile = transformToWGS84(shpInfo, monitor);
		}

		final File finalShapeFile = shapeFile;
		IWWShapefileLayer layer = layerFactory.createShapefileLayer(new Data() {

			/** {@inheritDoc} */
			@Override
			public String getName() {
				return shpInfo.getGeometryType() != null ? shpInfo.getGeometryType() : Data.super.getName();
			}

			/** {@inheritDoc} */
			@Override
			public Map<String, Set<String>> getFields() {
				return shpInfo.getFields() != null ? shpInfo.getFields() : Data.super.getFields();
			}

			@Override
			public File getShapeFile() {
				return finalShapeFile;
			}
		});

		if (layer != null) {
			layer.setName(shpInfo.getGeometryType() + "_" + FilenameUtils.getBaseName(shpInfo.getPath()));
			layerStore.addLayers(layer);
		}

		monitor.done();
	}

	/**
	 * Transform SHP file to WGS84 spatial reference
	 *
	 * @param shpInfo source ShpInfo
	 * @param monitor progress monitor
	 * @throws GIOException transformation failed
	 */
	protected File transformToWGS84(ShpInfo shpInfo, IProgressMonitor monitor) throws GIOException {
		monitor.subTask("Projecting to world geodetic system");
		String filename = FilenameUtils.getBaseName(shpInfo.getPath());
		try {
			File tmpDir = TemporaryCache.createTemporaryDirectory(filename, "");
			GdalOgrUtils.transformToSrs(shpInfo.getPath(), GdalOsrUtils.SRS_WGS84, GdalUtils.SHP_DRIVER_NAME,
					tmpDir.getAbsolutePath(), monitor);
			return new File(tmpDir, filename + ".shp");
		} catch (IOException e) {
			throw GIOException.wrap(e.getMessage(), e);
		}
	}

}