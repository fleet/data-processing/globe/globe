package fr.ifremer.globe.drivers.mgd;

import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.mgd.MgdNavigation;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer.DisplayMode;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.WWNavigationLayerParameters;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class builds {@link IWWNavigationLayer} for MGD77 files.
 */
@Component(name = "globe_drivers_mgd77_navigation_layer_provider", service = IWWLayerProvider.class)
public class MgdNavigationLayerProvider implements IWWLayerProvider {

	/** OSGI Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	@Reference
	private INavigationService navigationService;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return Set.of(ContentType.MGD77);
	}

	/**
	 * @return an {@link WWFileLayerStore} which contains a {@link IWWNavigationLayer} for files which can provide a
	 *         {@link INavigationDataSupplier}.
	 */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		return navigationService.getNavigationDataSupplier(fileInfo).map(navigationDataSupplier -> {
			var params = new WWNavigationLayerParameters().withDisplayMode(DisplayMode.POINT)
					.withVariableColor(true, Optional.of(MgdNavigation.GRAVITY));
			var layer = layerFactory.createNavigationLayer(navigationDataSupplier, params);
			return new WWFileLayerStore(fileInfo, layer);
		});
	}

}
