package fr.ifremer.globe.drivers.g3d.service.worldwind;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.dtm.netcdf4.constant.DtmConstants;
import fr.ifremer.globe.core.io.g3d.G3dConstants;
import fr.ifremer.globe.core.io.g3d.info.G3dInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDataUtils;
import fr.ifremer.globe.ui.service.worldwind.texture.impl.WWTextureData;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Texture data loader : reads data and computes {@link IWWTextureData}.
 */
public class G3dElevationMappedTextureDataLoader {

	class TextureCache<T> {
		/***
		 * Texture cache is a hashmap indexed by group name (key). Each value is a hashmap of data (texture data)
		 * indexed by layer name Cache cleaning is made only on group name, we try to keep maxCacheLen group loaded. By
		 * default maxCacheLen = 1
		 */
		private int maxCacheLen;

		TextureCache() {
			maxCacheLen = 1;
		}

		// cache containing
		private LinkedHashMap<String, HashMap<String, T>> sliceCache = new LinkedHashMap<String, HashMap<String, T>>();

		/**
		 * Returns the value to which the specified keys are mapped, or {@code null} if this map contains no mapping for
		 * the keys.
		 *
		 */
		public T get(String slice, String dataLayerName) {
			HashMap<String, T> value = sliceCache.get(slice);
			if (value == null)
				return null;
			return value.get(dataLayerName);
		}

		/**
		 * Associates the specified value with the specified keys in this map. If the map previously contained a mapping
		 * for the keys, the old value are replaced.
		 */
		public void put(String slicePath, String dataLayerName, T value) {
			HashMap<String, T> store = sliceCache.computeIfAbsent(slicePath, k -> new HashMap<String, T>());
			store.put(dataLayerName, value);
			pack();
		}

		/**
		 * remove useless data from cache
		 */
		public void pack() {
			int elementToRemove = sliceCache.size() - maxCacheLen;
			// linked hashmap store data as added, we could rely on order to remove older datasets
			ArrayList<String> keys = new ArrayList<>(sliceCache.keySet());
			for (int i = 0; i < elementToRemove; i++) {
				String k = keys.get(i);
				if (sliceCache.get(k) != null)
					sliceCache.get(k).clear();
				sliceCache.remove(k);
			}
		}
	}

	TextureCache<IWWTextureData> cache = new TextureCache<>();

	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(G3dElevationMappedTextureDataLoader.class);

	/** Byte order constant **/
	private static final ByteOrder NATIVE_ORDER = ByteOrder.nativeOrder();

	/** File **/
	private final G3dInfo gdfInfo;

	private final List<String> slices;
	private final List<String> layerNames;

	/** Statistics **/
	private final Map<String, Pair<Float, Float>> minMaxByDataLayer = new HashMap<>();
	private double minLat = Double.NaN;
	private double maxLat = Double.NaN;
	private double minLon = Double.NaN;
	private double maxLon = Double.NaN;

	/**
	 * Constructor
	 */
	public G3dElevationMappedTextureDataLoader(G3dInfo gdfInfo, IProgressMonitor monitor) throws NCException {
		this.gdfInfo = gdfInfo;

		// Load few groups to get statistics...

		List<String> dataLayerNames = gdfInfo.getDataLayers().stream().map(layer -> layer.name).distinct()
				.collect(Collectors.toList());
		layerNames = gdfInfo.getNetcdfFile().getVariables().stream().map(NetcdfVariable::getName)
				.collect(Collectors.toList());

		slices = layerNames.stream() // get variable name
				.filter(name -> name.indexOf(dataLayerNames.get(0)) >= 0) // check if datalayer contained in name
				.map(name -> name.substring(0, name.indexOf(dataLayerNames.get(0)))) // retrieve slice prefix
				.collect(Collectors.toList());

		try {
			loadSlice(slices.get(0), dataLayerNames);
			if (slices.size() > 2) {
				loadSlice(slices.get(slices.size() / 2), dataLayerNames);
				loadSlice(slices.get(slices.size() - 1), dataLayerNames);
			}
		} catch (NCException | GIOException e) {
			e.printStackTrace();
		}
		// Update GDF file info
		gdfInfo.setGeoBox(new GeoBox(maxLat, minLat, minLon, maxLon));

	}

	/**
	 * Builds a {@link IWWTextureData} for the first slice and first data layer.
	 */
	public IWWTextureData getTextureData() {
		return getTextureData(Optional.empty(), Optional.empty());
	}

	/**
	 * Builds a {@link IWWTextureData} for the specified slice and first data layer.
	 */
	public IWWTextureData getTextureData(Integer index) {
		return getTextureData(Optional.empty(), Optional.of(index));
	}

	/**
	 * Builds a {@link IWWTextureData} for the first slice and the specified data layer.
	 */
	public IWWTextureData getTextureData(String datalayer) {
		return getTextureData(Optional.of(datalayer), Optional.empty());
	}

	/**
	 * Builds a {@link IWWTextureData} for the specified data layer and index
	 */
	public IWWTextureData getTextureData(String datalayer, Integer index) {
		return getTextureData(Optional.of(datalayer), Optional.of(index));
	}

	/**
	 * Builds a {@link IWWTextureData} for the specified data layer and group
	 *
	 * Parameters are optional.
	 */
	public IWWTextureData getTextureData(Optional<String> datalayer, Optional<Integer> sliceIndex) {
		try {
			// get slice name, if not specified: get first group
			String slice = sliceIndex.isPresent() ? slices.get(sliceIndex.get()) : slices.get(0);

			// get data layer name, if not specified: get first data
			// layer
			String dataLayerName = datalayer.isPresent() ? datalayer.get() : gdfInfo.getDataLayers().get(0).name;

			// read data & build IWWTextureData
			return loadSlice(slice, List.of(dataLayerName)).get(dataLayerName);
		} catch (NCException | GIOException e) {
			LOGGER.error("Error with texture file: {}, datalayer : {}, slice : '{}'", gdfInfo.getFilename(),
					datalayer.orElse("not defined"),
					sliceIndex.isPresent() ? sliceIndex.get().toString() : "not defined", e);
		}
		return null;
	}

	public float getMinValue(String datalayerName) {
		return minMaxByDataLayer.containsKey(datalayerName) ? minMaxByDataLayer.get(datalayerName).getFirst()
				: Float.NaN;

	}

	public float getMaxValue(String datalayerName) {
		return minMaxByDataLayer.containsKey(datalayerName) ? minMaxByDataLayer.get(datalayerName).getSecond()
				: Float.NaN;
	}

	/**
	 * Loads a {@link NetcdfGroup}.
	 */
	private Map<String, IWWTextureData> loadSlice(String slice, List<String> dataLayerNames)
			throws NCException, GIOException {
		// Build IWWTextureData from the group for each data type

		Map<String, IWWTextureData> newTextureDataMap = getTextureData(slice, dataLayerNames);

		// Compute statistics
		for (Entry<String, IWWTextureData> entry : newTextureDataMap.entrySet()) {
			float minValue = getMinValue(entry.getKey());
			float maxValue = getMaxValue(entry.getKey());

			IWWTextureData newTextureData = entry.getValue();
			for (int i = 0; i < newTextureData.getHeight(); i++) {
				for (int j = 0; j < newTextureData.getWidth(); j++) {
					float value = ((ByteBuffer) newTextureData.getBuffer()).getFloat();
					if (!Float.isNaN(value)) {
						minValue = Float.isNaN(minValue) ? value : Math.min(minValue, value);
						maxValue = Float.isNaN(maxValue) ? value : Math.max(maxValue, value);
					}
				}
			}
			newTextureData.getBuffer().rewind();
			minMaxByDataLayer.put(entry.getKey(), new Pair<>(minValue, maxValue));
		}

		// Compute geographic statistics
		for (IWWTextureCoordinate coord : newTextureDataMap.values().iterator().next().getCoordinates()) {
			double lat = coord.getPosition().getLatitude().getDegrees();
			double lon = coord.getPosition().getLongitude().getDegrees();
			minLat = Double.isNaN(minLat) ? lat : Math.min(minLat, lat);
			maxLat = Double.isNaN(maxLat) ? lat : Math.max(maxLat, lat);
			minLon = Double.isNaN(minLon) ? lon : Math.min(minLon, lon);
			maxLon = Double.isNaN(maxLon) ? lon : Math.max(maxLon, lon);
		}

		return newTextureDataMap;
	}

	/**
	 * Builds the {@link IWWTextureData} for a specified {@link NetcdfGroup}.
	 */
	private Map<String, IWWTextureData> getTextureData(String slice, List<String> dataLayerNames)
			throws NCException, GIOException {
		// build a cache key

		Map<String, IWWTextureData> textureDataByDataLayer = new HashMap<>();

		ArrayList<String> notCachedDataLayerNames = new ArrayList<String>();

		Map<String, String> fullDataLayerNames = new HashMap<>();
		for (String dataLayer : dataLayerNames) {
			layerNames.stream().filter(name -> name.startsWith(slice + dataLayer)).findFirst()
					.ifPresent(fullName -> fullDataLayerNames.put(dataLayer, fullName));
		}

		for (String datalayer : dataLayerNames) {
			IWWTextureData data = cache.get(slice, fullDataLayerNames.get(datalayer));
			if (data != null) {
				textureDataByDataLayer.put(datalayer, data);
			} else {
				notCachedDataLayerNames.add(datalayer);
			}
		}
		if (textureDataByDataLayer.size() == dataLayerNames.size()) {
			// we got all needed data
			return textureDataByDataLayer;
		}

		// get dims
		NetcdfGroup group = this.gdfInfo.getNetcdfFile();
		int width = (int) group.getDimension(DtmConstants.LAYER_LON_NAME).getLength();
		int height = (int) group.getDimension(DtmConstants.LAYER_LAT_NAME).getLength();

		// read data for each layer
		Map<String, ByteBuffer> dataBuffersByDataLayer = new HashMap<>();
		Map<String, String> unitsByDataLayer = new HashMap<>();
		Map<String, Float> verticalOffsetByDataLayer = new HashMap<>();
		for (String dataLayerName : notCachedDataLayerNames) {
			String fullLayerName = fullDataLayerNames.get(dataLayerName);
			NetcdfVariable var = group.getVariable(fullLayerName);
			if (var == null)
				throw new GIOException(String.format("The expecting variable %s is missing from the slice : %s",
						dataLayerName, slice));
			ByteBuffer dataBuffer = ByteBuffer.allocateDirect(width * height * Float.BYTES).order(NATIVE_ORDER);
			var.read(new long[] { height, width }, dataBuffer, DataType.FLOAT, Optional.empty());
			dataBuffer.rewind(); // required before sending texture to renderer
			dataBuffersByDataLayer.put(dataLayerName, dataBuffer);
			// parse units attribute
			if (var.hasAttribute(G3dConstants.UNITS_ATT))
				unitsByDataLayer.put(dataLayerName, var.getAttributeAsString(G3dConstants.UNITS_ATT));
			if (var.hasAttribute(G3dConstants.VERTICAL_OFFSET_ATT))
				verticalOffsetByDataLayer.put(dataLayerName, var.getAttributeFloat(G3dConstants.VERTICAL_OFFSET_ATT));
		}

		// read metadata
		Map<String, String> metadata = new HashMap<>();
		group.getAttributes().forEach((key, value) -> {
			switch (key) {
			case G3dConstants.NAME_ATT:
				metadata.put(IWWTextureData.NAME_METADATA_KEY, value);
				break;
			case G3dConstants.DATE_ATT:
				metadata.put(IWWTextureData.DATE_METADATA_KEY, value);
				break;
			default:
				metadata.put(key, value);
			}
		});

		// ... metadata by width
		Map<String, BiFunction<Integer, Integer, String>> metadataProviders = new HashMap<>();
		for (NetcdfVariable var : group.getVariables()) {
			long[] dims = var.getDimensions();
			if (dims.length == 1 && dims[0] == width) {
				String title = var.getName();
				ByteBuffer dataBuffer = ByteBuffer.allocateDirect(width * Float.BYTES).order(NATIVE_ORDER);
				var.read(new long[] { width }, dataBuffer, DataType.FLOAT, Optional.empty());
				metadataProviders.put(title, (row, col) -> Float.toString(dataBuffer.getFloat(col * Float.BYTES)));
			}
		}

		// ... metadata by height
		for (NetcdfVariable var : group.getVariables()) {
			long[] dims = var.getDimensions();
			if (dims.length == 1 && dims[0] == height) {
				String title = var.getName();
				ByteBuffer dataBuffer = ByteBuffer.allocateDirect(height * Float.BYTES).order(NATIVE_ORDER);
				var.read(new long[] { height }, dataBuffer, DataType.FLOAT, Optional.empty());
				metadataProviders.put(title, (row, col) -> Float.toString(dataBuffer.getFloat(row * Float.BYTES)));
			}
		}

		// read position variables
		ByteBuffer latBuffer = ByteBuffer.allocateDirect(height * Double.BYTES).order(NATIVE_ORDER);
		NetcdfVariable latVariable = group.getVariable(DtmConstants.LAYER_LAT_NAME);
		latVariable.read(new long[] { height }, latBuffer, DataType.DOUBLE, Optional.empty());

		ByteBuffer lonBuffer = ByteBuffer.allocateDirect(width * Double.BYTES).order(NATIVE_ORDER);
		NetcdfVariable lonVariable = group.getVariable(DtmConstants.LAYER_LON_NAME);
		lonVariable.read(new long[] { width }, lonBuffer, DataType.DOUBLE, Optional.empty());

		NetcdfVariable elevationVariable = group.getVariable(DtmConstants.LAYER_ELEVATION);
		ByteBuffer eBuffer = ByteBuffer.allocateDirect(width * height * Float.BYTES).order(NATIVE_ORDER);
		int gridHeight = Math.max(2, height / 10);
		int gridWidth = Math.max(2, width / 10);
		if (elevationVariable != null) {
			// load elevation references
			elevationVariable.read(new long[] { height, width }, eBuffer, DataType.FLOAT, Optional.empty());
		} else {
			// use constant elevation
			gridHeight = 2;
			gridWidth = 2;
		}
		int gridRowMax = gridHeight - 1;
		int gridColMax = gridWidth - 1;
		BiFunction<Integer, Integer, IWWTextureCoordinate> coordSupplier = (gridRowIndex, gridColIndex) -> {
			int rowIndex = (int) (1.f * gridRowIndex * (height - 1) / gridRowMax);
			int colIndex = (int) (1.f * gridColIndex * (width - 1) / gridColMax);
			float s = (colIndex + 0.5f) / width;
			float t = (rowIndex + 0.5f) / height;
			return WWTextureDataUtils.buildTextureCoordinate(//
					latBuffer.getDouble(rowIndex * Double.BYTES), // latitude
					lonBuffer.getDouble(colIndex * Double.BYTES), // longitude
					eBuffer.getFloat((rowIndex * width + colIndex) * Float.BYTES), // elevation
					s, t);
		};
		double deltaLat = latBuffer.getDouble(Double.BYTES) - latBuffer.getDouble(0);
		double deltaLon = lonBuffer.getDouble(Double.BYTES) - lonBuffer.getDouble(0);

		// compute texture coordinates from positions
		List<IWWTextureCoordinate> coordinates = computeTextureCoordinates(gridWidth, gridHeight, deltaLat, deltaLon,
				coordSupplier);

		// create texture data
		textureDataByDataLayer = new HashMap<>();
		for (Entry<String, ByteBuffer> entry : dataBuffersByDataLayer.entrySet()) {
			String dataLayerName = entry.getKey();

			// create WWTextureData
			WWTextureData textureData = new WWTextureData(width, height, gridHeight, entry.getValue(),
					coordinates, false);
			// add metadata
			if (verticalOffsetByDataLayer.containsKey(entry.getKey())) {
				Float verticalOffset = verticalOffsetByDataLayer.get(dataLayerName);
				metadata.put(IWWTextureData.VERTICAL_OFFSET_METADATA_KEY,
						verticalOffset.toString());
				
				//Display label in indexed player
				metadata.put(IWWTextureData.NAME_METADATA_KEY, String.format("z=%.2f", verticalOffset));
			}
			if (unitsByDataLayer.containsKey(entry.getKey()))
				metadata.put(IWWTextureData.UNITS_METADATA_KEY, unitsByDataLayer.get(dataLayerName));
			metadata.put(IWWTextureData.DATA_TYPE_METADATA_KEY, dataLayerName);
			textureData.setMetadata(metadata);
			textureData.setMetadataProviders(metadataProviders);

			textureDataByDataLayer.put(entry.getKey(), textureData);
			cache.put(group.getPath(), entry.getKey(), textureData);
		}
		return textureDataByDataLayer;
	}

	/**
	 * Computes texture coordinates from coordinates of cell's center.
	 *
	 * @param gridWidth : grid width
	 * @param gridHeight : grid height
	 * @param deltaLat : latitude resolution of texture
	 * @param deltaLon : longitude resolution of texture
	 * @param posSupplier : texture coordinate supplier (give the cell center position)
	 * 
	 *
	 * @return a list of {@link IWWTextureData.IWWTextureCoordinate} (coordinates are ordered by column bottom->top
	 *         points)
	 */
	private List<IWWTextureCoordinate> computeTextureCoordinates(int gridWidth, int gridHeight, double deltaLat,
			double deltaLon, BiFunction<Integer, Integer, IWWTextureCoordinate> coordSupplier) {
		List<IWWTextureCoordinate> coordinates = new ArrayList<>();
		for (int i = 0; i < gridWidth; i++) {
			boolean isLeft = i == 0;
			boolean isRight = i == gridWidth - 1;

			for (int j = 0; j < gridHeight; j++) {
				boolean isBottom = j == 0;
				boolean isTop = j == gridHeight - 1;

				IWWTextureCoordinate coord = coordSupplier.apply(j, i);
				double longitude = coord.getPosition().longitude.degrees;
				double latitude = coord.getPosition().latitude.degrees;
				double elevation = coord.getPosition().elevation;

				// Shift top & bottom of 1/2 cell to get the top/bottom bounds
				// instead of cell centers
				if (isBottom) {
					latitude -= deltaLat / 2;
				} else if (isTop) {
					latitude += deltaLat / 2;
				}
				// Shift first/last coordinate of 1/2 cell to outside to get the left/right bound
				// instead of cell centers
				if (isLeft) {
					longitude -= deltaLon / 2;
				} else if (isRight) {
					longitude += deltaLon / 2;
				}

				// compute 's': the relative position along the texture width (between 0 & 1)
				float s = isLeft ? 0f : (isRight ? 1f : coord.getS());
				// compute 't': the relative position along the texture height (between 0 & 1)
				float t = isBottom ? 0f : (isTop ? 1f : coord.getT());

				coordinates.add(WWTextureDataUtils.buildTextureCoordinate(latitude, longitude, elevation, s, t));
			}
		}
		return coordinates;
	}

}
