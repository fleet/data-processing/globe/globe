/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.g3d.service.worldwind;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.g3d.G3dConstants;
import fr.ifremer.globe.core.io.g3d.G3dDataLayer;
import fr.ifremer.globe.core.io.g3d.info.G3dInfo;
import fr.ifremer.globe.core.io.g3d.info.G3dInfoSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.playable.WWPlayableTextureInitParameters;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.globe.ui.utils.color.ColorMap;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Driver to manage Globe 3D netCDF files.
 */
@Component(name = "globe_drivers_g3d_layer_provider", service = IWWLayerProvider.class)
public class G3dLayerProvider implements IWWLayerProvider {

	/** Logger */
	protected static final Logger LOGGER = LoggerFactory.getLogger(G3dLayerProvider.class);

	protected static final String SESSION_DISPLAY_PARAMETERS = "Display_Parameters";
	protected static final String SESSION_PLAYER_INDEX = "Player_Index";

	/** InfoSupplier of a GDF file */
	@Reference
	protected G3dInfoSupplier gdfInfoSupplier;

	/** OSGI Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return gdfInfoSupplier.getContentTypes();
	}

	/**
	 * @see fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider#getFileLayers(java.lang.String,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		if (fileInfo instanceof G3dInfo g3dInfo) {
			try {
				// Build layer in function of GDF type
				switch (g3dInfo.getDatasetType()) {
				case G3dConstants.DATASET_TYPE_FLY_TEXTURE: {
					List<IWWTextureLayer> textureLayers = buildWWVerticalTextureLayers(g3dInfo, monitor);
					WWFileLayerStore layerStore = new WWFileLayerStore(g3dInfo);
					// display only first layer
					textureLayers.forEach(layer -> layer.setEnabled(layer == textureLayers.get(0)));
					layerStore.addLayers(textureLayers);
					return Optional.of(layerStore);
				}
				case G3dConstants.DATASET_TYPE_ELEVATION_MAPPED_TEXTURE: {
					var textureLayers = buildWWTextureLayers(g3dInfo, monitor);
					WWFileLayerStore layerStore = new WWFileLayerStore(g3dInfo);
					// display only first layer
					textureLayers.forEach(layer -> layer.setEnabled(layer == textureLayers.get(0)));
					layerStore.addLayers(textureLayers);
					return Optional.of(layerStore);
				}
				}
			} catch (NCException e) {
				LOGGER.error("Error while creating texture from " + g3dInfo.getPath(), e);
			}
		}
		return Optional.empty();
	}

	/**
	 * Build a vertical texture layer.
	 */
	private List<IWWTextureLayer> buildWWVerticalTextureLayers(G3dInfo g3dInfo, IProgressMonitor monitor)
			throws NCException {
		// compute data
		G3dFlyTextureDataLoader loader = new G3dFlyTextureDataLoader(g3dInfo, monitor);

		// build WW layers from G3D data layers
		var result = new LinkedList<IWWTextureLayer>();
		for (var g3dLayer : g3dInfo.getDataLayers()) {
			String layerName = FileUtils.getFileShortName(g3dInfo.getPath()) + "_" + g3dLayer.name;

			IWWTextureLayer newTextureLayer = null;
			// 1 slice : simple layer
			if (g3dInfo.getSliceNumber() == 1) {
				newTextureLayer = layerFactory.createWWTextureLayer(layerName, loader.getTextureData(g3dLayer.name),
						getDisplayParameter(g3dLayer, loader));
			} else {// mutli slice : playable layer
				// G3D with WC can not be synchronized
				var syncKey = Optional.ofNullable(g3dInfo.containsPolarEchograms() ? null : "G3D_FlyTexture");
				var newPlayableTextureLayer = layerFactory.createWWPlayableTextureLayer(
						new WWPlayableTextureInitParameters(layerName, g3dInfo.getSliceNumber(),
								g3dInfo.getNetcdfGroupDates(), syncKey),
						i -> loader.getTextureData(g3dLayer.name, i), getDisplayParameter(g3dLayer, loader));
				// The best texture is usually in the middle. Start the display with it
				if (!g3dInfo.containsPolarEchograms())
					newPlayableTextureLayer.getPlayer().setCurrent(g3dInfo.getSliceNumber() / 2);

				newTextureLayer = newPlayableTextureLayer;
			}
			newTextureLayer.setShortName(g3dLayer.displayName);
			newTextureLayer.setIcon((g3dInfo.containsPolarEchograms() ? Icons.WC : Icons.WC_SLICE).toImage());
			result.add(newTextureLayer);
		}
		return result;
	}

	/**
	 * Build a elevation mapped texture layer.
	 */
	private List<IWWTextureLayer> buildWWTextureLayers(G3dInfo g3dInfo, IProgressMonitor monitor) throws NCException {
		// compute data
		G3dElevationMappedTextureDataLoader loader = new G3dElevationMappedTextureDataLoader(g3dInfo, monitor);

		// build WW layers from G3D data layers
		var result = new LinkedList<IWWTextureLayer>();
		for (var g3dLayer : g3dInfo.getDataLayers()) {
			String layerName = FileUtils.getFileShortName(g3dInfo.getPath()) + "_" + g3dLayer.name;

			IWWTextureLayer newTextureLayer = null;
			// 1 slice : simple layer
			if (g3dInfo.getSliceNumber() == 1) {
				newTextureLayer = layerFactory.createWWTextureLayer(layerName, loader.getTextureData(g3dLayer.name),
						getDisplayParameter(g3dLayer, loader));
			} else {// mutli slice : playable layer
				var syncKey = Optional.of("G3D_ElevationMappedTexture");
				newTextureLayer = layerFactory.createWWPlayableTextureLayer(
						new WWPlayableTextureInitParameters(layerName, g3dInfo.getSliceNumber(),
								g3dInfo.getNetcdfGroupDates(), syncKey),
						i -> loader.getTextureData(g3dLayer.name, i), getDisplayParameter(g3dLayer, loader));
			}
			newTextureLayer.setShortName(g3dLayer.displayName);
			newTextureLayer.setIcon(Icons.RASTER.toImage());
			result.add(newTextureLayer);
		}
		return result;
	}

	/**
	 * @return {@link WWTextureDisplayParameters} for a {@link G3dDataLayer}.
	 */
	private WWTextureDisplayParameters getDisplayParameter(G3dDataLayer g3dLayer, G3dFlyTextureDataLoader loader) {
		float minValue = g3dLayer.contrastMin.filter(v -> !Float.isNaN(v))
				.orElseGet(() -> loader.getMinValue(g3dLayer.name));
		float maxValue = g3dLayer.contrastMax.filter(v -> !Float.isNaN(v))
				.orElseGet(() -> loader.getMaxValue(g3dLayer.name));
		if (g3dLayer.colorName.isPresent()) {
			int colorMapIndex = ColorMap.getColorMapIntIndex(g3dLayer.colorName.get());
			return new WWTextureDisplayParameters(minValue, maxValue, colorMapIndex);
		} else {
			return new WWTextureDisplayParameters(minValue, maxValue);
		}
	}

	/**
	 * @return {@link WWTextureDisplayParameters} for a {@link G3dDataLayer}.
	 */
	private WWTextureDisplayParameters getDisplayParameter(G3dDataLayer g3dLayer,
			G3dElevationMappedTextureDataLoader loader) {
		float minValue = g3dLayer.contrastMin.filter(v -> !Float.isNaN(v))
				.orElseGet(() -> loader.getMinValue(g3dLayer.name));
		float maxValue = g3dLayer.contrastMax.filter(v -> !Float.isNaN(v))
				.orElseGet(() -> loader.getMaxValue(g3dLayer.name));
		if (g3dLayer.colorName.isPresent()) {
			int colorMapIndex = ColorMap.getColorMapIntIndex(g3dLayer.colorName.get());
			return new WWTextureDisplayParameters(minValue, maxValue, colorMapIndex);
		} else {
			return new WWTextureDisplayParameters(minValue, maxValue);
		}
	}

}