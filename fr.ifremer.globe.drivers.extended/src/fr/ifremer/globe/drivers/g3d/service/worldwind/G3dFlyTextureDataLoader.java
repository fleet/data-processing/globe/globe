package fr.ifremer.globe.drivers.g3d.service.worldwind;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.g3d.G3dConstants;
import fr.ifremer.globe.core.io.g3d.info.G3dInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.netcdf.api.NetcdfGroup;
import fr.ifremer.globe.netcdf.api.NetcdfVariable;
import fr.ifremer.globe.netcdf.ucar.DataType;
import fr.ifremer.globe.netcdf.ucar.NCException;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDataUtils;
import fr.ifremer.globe.ui.service.worldwind.texture.impl.WWTextureData;
import fr.ifremer.globe.utils.date.DateUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Texture data loader : reads data and computes {@link IWWTextureData}.
 */
public class G3dFlyTextureDataLoader {

	class TextureCache<T> {
		/***
		 * Texture cache is a hashmap indexed by group name (key). Each value is a hashmap of data (texture data)
		 * indexed by layer name Cache cleaning is made only on group name, we try to keep maxCacheLen group loaded. By
		 * default maxCacheLen = 1
		 */
		private int maxCacheLen;

		TextureCache() {
			maxCacheLen = 1;
		}

		// cache containing
		private LinkedHashMap<String, HashMap<String, T>> groupCache = new LinkedHashMap<String, HashMap<String, T>>();

		/**
		 * Returns the value to which the specified keys are mapped, or {@code null} if this map contains no mapping for
		 * the keys.
		 *
		 */
		public T get(String groupPath, String dataLayerName) {
			HashMap<String, T> value = groupCache.get(groupPath);
			if (value == null)
				return null;
			return value.get(dataLayerName);
		}

		/**
		 * Associates the specified value with the specified keys in this map. If the map previously contained a mapping
		 * for the keys, the old value are replaced.
		 */
		public void put(String groupPath, String dataLayerName, T value) {
			HashMap<String, T> store = groupCache.get(groupPath);
			if (store == null) {
				store = new HashMap<String, T>();
				groupCache.put(groupPath, store);
			}
			store.put(dataLayerName, value);
			pack();
		}

		/**
		 * remove useless data from cache
		 */
		public void pack() {
			int elementToRemove = groupCache.size() - maxCacheLen;
			// linked hashmap store data as added, we could rely on order to remove older datasets
			ArrayList<String> keys = new ArrayList<String>(groupCache.keySet());
			for (int i = 0; i < elementToRemove; i++) {
				String k = keys.get(i);
				if (groupCache.get(k) != null)
					groupCache.get(k).clear();
				groupCache.remove(k);
			}
		}
	}

	TextureCache<IWWTextureData> cache = new TextureCache<IWWTextureData>();

	/** Logger **/
	private static final Logger LOGGER = LoggerFactory.getLogger(G3dFlyTextureDataLoader.class);

	/** Byte order constant **/
	private static final ByteOrder NATIVE_ORDER = ByteOrder.nativeOrder();

	/**
	 * Private utility class to define a position.
	 */
	private class Position {
		double lat;
		double lon;
		float elevation;

		public Position(double lat, double lon, float elevation) {
			this.lat = lat;
			this.lon = lon;
			this.elevation = elevation;
		}
	}

	/** File **/
	private final G3dInfo gdfInfo;

	private final List<NetcdfGroup> groups;

	/** Statistics **/
	private final Map<String, Pair<Float, Float>> minMaxByDataLayer = new HashMap<>();
	private double minLat = Double.NaN;
	private double maxLat = Double.NaN;
	private double minLon = Double.NaN;
	private double maxLon = Double.NaN;

	/**
	 * Constructor
	 */
	public G3dFlyTextureDataLoader(G3dInfo gdfInfo, IProgressMonitor monitor) throws NCException {
		this.gdfInfo = gdfInfo;

		// Load few groups to get statistics...
		groups = gdfInfo.getNetcdfFile().getGroups();

		Set<String> dataLayerNames = gdfInfo.getDataLayers().stream().map(layer -> layer.name)
				.collect(Collectors.toSet());
		try {
			loadGroup(groups.get(0), dataLayerNames);
			if (groups.size() > 2) {
				loadGroup(groups.get(groups.size() / 2), dataLayerNames);
				loadGroup(groups.get(groups.size() - 1), dataLayerNames);
			}
		} catch (NCException | GIOException e) {
			e.printStackTrace();
		}
		// Update GDF file info
		gdfInfo.setGeoBox(new GeoBox(maxLat, minLat, minLon, maxLon));
		gdfInfo.setNetcdfGroupDates(loadGroupDates(groups));

		monitor.done();
	}

	/**
	 * Browses all groups to extract the dates.
	 * 
	 * @return List.empty() on error or when a date is missing
	 */
	private List<Instant> loadGroupDates(List<NetcdfGroup> groups) {
		List<Instant> result = new ArrayList<>(groups.size());
		try {
			for (NetcdfGroup group : groups) {
				// All group must have a date
				if (!group.hasAttribute(G3dConstants.DATE_ATT))
					return List.of();
				String date = group.getAttributeText(G3dConstants.DATE_ATT);
				Optional<Instant> instant = DateUtils.parseInstant(date);
				if (instant.isEmpty())
					return List.of();
				result.add(instant.get());
			}
		} catch (NumberFormatException | NCException e) {
			return List.of();
		}
		return result;
	}

	/**
	 * Builds a {@link IWWTextureData} for the first slice and first data layer.
	 */
	public IWWTextureData getTextureData() {
		return getTextureData(Optional.empty(), Optional.empty());
	}

	/**
	 * Builds a {@link IWWTextureData} for the specified slice and first data layer.
	 */
	public IWWTextureData getTextureData(Integer index) {
		return getTextureData(Optional.empty(), Optional.of(index));
	}

	/**
	 * Builds a {@link IWWTextureData} for the first slice and the specified data layer.
	 */
	public IWWTextureData getTextureData(String datalayer) {
		return getTextureData(Optional.of(datalayer), Optional.empty());
	}

	/**
	 * Builds a {@link IWWTextureData} for the specified data layer and index
	 */
	public IWWTextureData getTextureData(String datalayer, Integer index) {
		return getTextureData(Optional.of(datalayer), Optional.of(index));
	}

	/**
	 * Builds a {@link IWWTextureData} for the specified data layer and group
	 *
	 * Parameters are optional.
	 */
	public IWWTextureData getTextureData(Optional<String> datalayer, Optional<Integer> groupIndex) {
		try {
			// get netcdf group ( = slice), if not specified: get first group
			NetcdfGroup ncGroup = groupIndex.isPresent() ? groups.get(groupIndex.get()) : groups.get(0);
			if (ncGroup == null) {
				LOGGER.warn("Netcdf group not found in file {} ", gdfInfo.getFilename());
				return null;
			}

			// get netcdf variable (= data layer name), if not specified: get first data
			// layer
			String dataLayerName = datalayer.isPresent() ? datalayer.get() : gdfInfo.getDataLayers().get(0).name;

			// read data & build IWWTextureData
			return loadGroup(ncGroup, Set.of(dataLayerName)).get(dataLayerName);
		} catch (NCException | GIOException e) {
			LOGGER.error("Error with texture file: {}, datalayer : {}, slice : '{}'", gdfInfo.getFilename(),
					datalayer.orElse("not defined"),
					groupIndex.isPresent() ? groupIndex.get().toString() : "not defined", e);
		}
		return null;
	}

	public float getMinValue(String datalayerName) {
		return minMaxByDataLayer.containsKey(datalayerName) ? minMaxByDataLayer.get(datalayerName).getFirst()
				: Float.NaN;

	}

	public float getMaxValue(String datalayerName) {
		return minMaxByDataLayer.containsKey(datalayerName) ? minMaxByDataLayer.get(datalayerName).getSecond()
				: Float.NaN;
	}

	/**
	 * Loads a {@link NetcdfGroup}.
	 */
	private Map<String, IWWTextureData> loadGroup(NetcdfGroup group, Set<String> dataLayerNames)
			throws NCException, GIOException {
		// Build IWWTextureData from the group for each data type

		Map<String, IWWTextureData> newTextureDataMap = getTextureData(group, dataLayerNames);

		// Compute statistics
		for (Entry<String, IWWTextureData> entry : newTextureDataMap.entrySet()) {
			float minValue = getMinValue(entry.getKey());
			float maxValue = getMaxValue(entry.getKey());

			IWWTextureData newTextureData = entry.getValue();
			for (int i = 0; i < newTextureData.getHeight(); i++) {
				for (int j = 0; j < newTextureData.getWidth(); j++) {
					float value = newTextureData.getBuffer().getFloat();
					if (!Float.isNaN(value)) {
						minValue = Float.isNaN(minValue) ? value : Math.min(minValue, value);
						maxValue = Float.isNaN(maxValue) ? value : Math.max(maxValue, value);
					}
				}
			}
			newTextureData.getBuffer().rewind();
			minMaxByDataLayer.put(entry.getKey(), new Pair<>(minValue, maxValue));
		}

		// Compute geographic statistics
		for (IWWTextureCoordinate coord : newTextureDataMap.values().iterator().next().getCoordinates()) {
			double lat = coord.getPosition().getLatitude().getDegrees();
			double lon = coord.getPosition().getLongitude().getDegrees();
			minLat = Double.isNaN(minLat) ? lat : Math.min(minLat, lat);
			maxLat = Double.isNaN(maxLat) ? lat : Math.max(maxLat, lat);
			minLon = Double.isNaN(minLon) ? lon : Math.min(minLon, lon);
			maxLon = Double.isNaN(maxLon) ? lon : Math.max(maxLon, lon);
		}
		return newTextureDataMap;
	}

	/**
	 * Builds the {@link IWWTextureData} for a specified {@link NetcdfGroup}.
	 */
	private Map<String, IWWTextureData> getTextureData(NetcdfGroup group, Set<String> dataLayerNames)
			throws NCException, GIOException {
		// build a cache key

		Map<String, IWWTextureData> textureDataByDataLayer = new HashMap<>();

		ArrayList<String> notCachedDataLayerNames = new ArrayList<String>();
		for (String datalayer : dataLayerNames) {
			IWWTextureData data = cache.get(group.getPath(), datalayer);
			if (data != null) {
				textureDataByDataLayer.put(datalayer, data);
			} else {
				notCachedDataLayerNames.add(datalayer);
			}
		}
		if (textureDataByDataLayer.size() == dataLayerNames.size()) {
			// we got all needed data
			return textureDataByDataLayer;
		}

		// get dims
		int width = (int) group.getDimension(G3dConstants.LENGTH_DIM).getLength();
		int height = (int) group.getDimension(G3dConstants.HEIGHT_DIM).getLength();
		int positionNumber = (int) group.getDimension(G3dConstants.POSITION_DIM).getLength();
		int vectorDim = (int) group.getDimension(G3dConstants.VECTOR_DIM).getLength();

		// read data for each layer
		Map<String, ByteBuffer> dataBuffersByDataLayer = new HashMap<>();
		Map<String, String> unitsByDataLayer = new HashMap<>();
		for (String dataLayerName : notCachedDataLayerNames) {
			NetcdfVariable var = group.getVariable(dataLayerName);
			if (var == null)
				throw new GIOException(String.format("The expecting variable %s is missing from the group : %s",
						dataLayerName, group.getName()));
			ByteBuffer dataBuffer = ByteBuffer.allocateDirect(width * height * Float.BYTES).order(NATIVE_ORDER);
			var.read(new long[] { height, width }, dataBuffer, DataType.FLOAT, Optional.empty());
			dataBuffer.rewind(); // required before sending texture to renderer
			dataBuffersByDataLayer.put(dataLayerName, dataBuffer);
			// parse units attribut
			if (var.hasAttribute(G3dConstants.UNITS_ATT))
				unitsByDataLayer.put(dataLayerName, var.getAttributeAsString(G3dConstants.UNITS_ATT));
		}

		// read metadata
		Map<String, String> metadata = new HashMap<>();
		group.getAttributes().forEach((key, value) -> {
			switch (key) {
			case G3dConstants.NAME_ATT:
				metadata.put(IWWTextureData.NAME_METADATA_KEY, value);
				break;
			case G3dConstants.DATE_ATT:
				metadata.put(IWWTextureData.DATE_METADATA_KEY, value);
				break;
			case "across_dist_L": // polar echograms from sonarscope
				metadata.put(IWWTextureData.LONGITUDINAL_OFFSET_METADATA_KEY, value);
			default:
				metadata.put(key, value);
			}
		});

		// ... metadata by width
		Map<String, BiFunction<Integer, Integer, String>> metadataProviders = new HashMap<>();
		for (NetcdfVariable var : group.getVariables()) {
			long[] dims = var.getDimensions();
			if (dims.length == 1 && dims[0] == width) {
				String title = var.getName();
				if (title.equals(G3dConstants.TIME_VAR)) {
					ByteBuffer dataBuffer = ByteBuffer.allocateDirect(width * Long.BYTES).order(NATIVE_ORDER);
					var.read(new long[] { width }, dataBuffer, DataType.LONG, Optional.empty());
					metadataProviders.put(IWWTextureData.DATE_METADATA_KEY,
							(row, col) -> Instant.ofEpochMilli(dataBuffer.getLong(col * Long.BYTES)).toString());
				} else {
					ByteBuffer dataBuffer = ByteBuffer.allocateDirect(width * Float.BYTES).order(NATIVE_ORDER);
					var.read(new long[] { width }, dataBuffer, DataType.FLOAT, Optional.empty());
					metadataProviders.put(title, (row, col) -> Float.toString(dataBuffer.getFloat(col * Float.BYTES)));
				}
			}
		}

		// ... metadata by height
		for (NetcdfVariable var : group.getVariables()) {
			long[] dims = var.getDimensions();
			if (dims.length == 1 && dims[0] == height) {
				String title = var.getName();
				ByteBuffer dataBuffer = ByteBuffer.allocateDirect(height * Float.BYTES).order(NATIVE_ORDER);
				var.read(new long[] { height }, dataBuffer, DataType.FLOAT, Optional.empty());
				metadataProviders.put(title, (row, col) -> Float.toString(dataBuffer.getFloat(row * Float.BYTES)));
			}
		}

		// read position variables
		ByteBuffer latBuffer = ByteBuffer.allocateDirect(positionNumber * vectorDim * Double.BYTES).order(NATIVE_ORDER);
		NetcdfVariable latVariable = group.getVariable(G3dConstants.LATITUDE_VAR);
		latVariable.read(new long[] { vectorDim, positionNumber }, latBuffer, DataType.DOUBLE, Optional.empty());

		ByteBuffer lonBuffer = ByteBuffer.allocateDirect(positionNumber * vectorDim * Double.BYTES).order(NATIVE_ORDER);
		NetcdfVariable lonVariable = group.getVariable(G3dConstants.LONGITUDE_VAR);
		lonVariable.read(new long[] { vectorDim, positionNumber }, lonBuffer, DataType.DOUBLE, Optional.empty());

		NetcdfVariable elevationVariable = group.getVariable(G3dConstants.ELEVATION_VAR);
		ByteBuffer eBuffer = ByteBuffer.allocateDirect(positionNumber * vectorDim * Float.BYTES).order(NATIVE_ORDER);
		elevationVariable.read(new long[] { vectorDim, positionNumber }, eBuffer, DataType.FLOAT, Optional.empty());

		Optional<NetcdfVariable> optPosColIndexVariable = Optional
				.ofNullable(group.getVariable(G3dConstants.POSITION_COL_INDEX_VAR));
		Optional<ByteBuffer> positionColIndexBuffer = Optional.empty();
		if (optPosColIndexVariable.isPresent()) {
			positionColIndexBuffer = Optional
					.of(ByteBuffer.allocateDirect(positionNumber * Integer.BYTES).order(NATIVE_ORDER));
			optPosColIndexVariable.get().read(new long[] { positionNumber }, eBuffer, DataType.INT, Optional.empty());
		}

		// supplier of top and bottom positions & column index by position
		IntFunction<Position> topSupplier = positionIndex -> new Position(//
				latBuffer.getDouble(positionIndex * Double.BYTES), //
				lonBuffer.getDouble(positionIndex * Double.BYTES), //
				eBuffer.getFloat(positionIndex * Float.BYTES));

		IntFunction<Position> botttomSupplier = positionIndex -> new Position(
				latBuffer.getDouble((positionNumber + positionIndex) * Double.BYTES),
				lonBuffer.getDouble((positionNumber + positionIndex) * Double.BYTES),
				eBuffer.getFloat((positionNumber + positionIndex) * Float.BYTES));

		IntFunction<Integer> colIndexSupplier = null;
		if (positionNumber == width) { // case 1 : one position per column
			colIndexSupplier = positionIndex -> positionIndex;
		} else if (positionNumber == 2) { // case 2: only position at start and end
			colIndexSupplier = positionIndex -> positionIndex == 0 ? 0 : width - 1;
		} else if (positionColIndexBuffer.isPresent()) {
			// case 3: several positions, and column index given by a variable
			final ByteBuffer fPositionColIndexBuffer = positionColIndexBuffer.get();
			colIndexSupplier = fPositionColIndexBuffer::getInt;
		} else {// case 4: several position, uniformly distributed
			colIndexSupplier = positionIndex -> positionIndex / positionNumber;
		}

		// compute texture coordinates from positions
		List<IWWTextureCoordinate> coordinates = computeTextureCoordinates(positionNumber, height, topSupplier,
				botttomSupplier, colIndexSupplier);

		// create texture data
		textureDataByDataLayer = new HashMap<>();
		for (Entry<String, ByteBuffer> entry : dataBuffersByDataLayer.entrySet()) {
			String dataLayerName = entry.getKey();

			// create WWTextureData
			WWTextureData textureData = new WWTextureData(width, height, entry.getValue(), coordinates, true);

			// add metadata
			if (unitsByDataLayer.containsKey(entry.getKey()))
				metadata.put(IWWTextureData.UNITS_METADATA_KEY, unitsByDataLayer.get(dataLayerName));
			metadata.put(IWWTextureData.DATA_TYPE_METADATA_KEY, dataLayerName);
			textureData.setMetadata(metadata);
			textureData.setMetadataProviders(metadataProviders);

			textureDataByDataLayer.put(entry.getKey(), textureData);
			cache.put(group.getPath(), entry.getKey(), textureData);
		}
		return textureDataByDataLayer;
	}

	/**
	 * Computes texture coordinates from coordinates of top/bottom cell's center.
	 *
	 * @param positionCount : number of provided position
	 * @param height : texture height
	 * @param topSupplier : top coordinate supplier (give the cell center position)
	 * @param bottomSupplier: bottom coordinate supplier (give the cell center position)
	 *
	 * @return a list of {@link IWWTextureData.IWWTextureCoordinate} (coordinates are ordered by couples top-bottom
	 *         points)
	 */
	private List<IWWTextureCoordinate> computeTextureCoordinates(int positionCount, int height,
			IntFunction<Position> topSupplier, IntFunction<Position> bottomSupplier, IntFunction<Integer> col) {
		List<IWWTextureCoordinate> coordinates = new ArrayList<>();
		for (int i = 0; i < positionCount; i++) {
			Position top = topSupplier.apply(i);
			Position bottom = bottomSupplier.apply(i);
			boolean isFirst = i == 0;
			boolean isLast = i == positionCount - 1;

			// Shift first coordinate of 1/2 cell vector to outside to get the left bound
			// instead of cell centers
			if (isFirst) {
				double extrapolationFactor = 1 / (2. * col.apply(1));
				extrapolate(top, topSupplier.apply(1), extrapolationFactor);
				extrapolate(bottom, bottomSupplier.apply(1), extrapolationFactor);
			}

			// Shift last coordinate of 1/2 cell vector to outside to get the right bound
			// instead of cell centers
			if (isLast) {
				double extrapolationFactor = 1 / (2. * (col.apply(i) - col.apply(i - 1)));
				extrapolate(top, topSupplier.apply(i - 1), extrapolationFactor);
				extrapolate(bottom, bottomSupplier.apply(i - 1), extrapolationFactor);
			}

			// Shift top & bottom along the texture height to get the top/bottom bounds
			// instead of cell centers
			if (height > 1) {
				int heightFactor = 2 * (height - 1);
				double deltaLat = (top.lat - bottom.lat) / heightFactor;
				double deltaLon = (top.lon - bottom.lon) / heightFactor;
				double deltaElevation = (top.elevation - bottom.elevation) / heightFactor;
				top.lat += deltaLat;
				top.lon += deltaLon;
				top.elevation += deltaElevation;
				bottom.lat -= deltaLat;
				bottom.lon -= deltaLon;
				bottom.elevation -= deltaElevation;
			}

			// compute 's': the relative position along the texture width (between 0 & 1)
			float s = isFirst ? 0f : isLast ? 1f : (i + 0.5f) / positionCount;

			// add coordinates (vertical flip to display correctly the texture)
			coordinates.add(WWTextureDataUtils.buildTopTextureCoordinate(bottom.lat, bottom.lon, bottom.elevation, s));
			coordinates.add(WWTextureDataUtils.buildBottomTextureCoordinate(top.lat, top.lon, top.elevation, s));
		}
		return coordinates;
	}

	/**
	 * Translate position A with a vector computed by (A - B)/2
	 */
	private void extrapolate(Position a, Position b, double extrapolationFactor) {
		a.lat += extrapolationFactor * (a.lat - b.lat);
		a.lon += extrapolationFactor * (a.lon - b.lon);
		a.elevation += extrapolationFactor * (a.elevation - b.elevation);
	}

	///////////////////////////////////////////////////////////
	/////////////// DEBUG & TEST METHODS //////////////////////
	///////////////////////////////////////////////////////////
	@SuppressWarnings("unused")
	private IWWTextureData getFAKETextureData() {
		// get dims
		int width = 4;
		int height = 4;

		// read data
		ByteBuffer dataBuffer = ByteBuffer.allocateDirect(width * height * Float.BYTES).order(NATIVE_ORDER);
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				float value = (float) (-64 + Math.random() * 70);
				dataBuffer.putFloat(value);
			}
		}
		dataBuffer.rewind(); // required to generate texture from
		/*
		 * Position[] tops = new Position[] { new Position(-12.8, 45.68, 0), // new Position(-12.79, 45.68, 0), // new
		 * Position(-12.78, 45.68, 0),// }; Position[] bottoms = new Position[] { new Position(-12.8, 45.68, -1000), //
		 * new Position(-12.79, 45.68, -1000), // new Position(-12.78, 45.68, -1000),// };
		 */

		Position[] tops = new Position[] { new Position(-12.8, 45.66, -500), //
				new Position(-12.8, 45.665, -500), //
				new Position(-12.8, 45.67, -500), //
				new Position(-12.8, 45.675, -500), //
				new Position(-12.8, 45.68, -500),//
		};
		Position[] bottoms = new Position[] { new Position(-12.81, 45.66, -500), //
				new Position(-12.81, 45.665, -500), //
				new Position(-12.81, 45.67, -500), //
				new Position(-12.81, 45.675, -500), //
				new Position(-12.81, 45.68, -500),//
		};

		// Compute texture coordinates from positions
		List<IWWTextureCoordinate> coordinates = computeTextureCoordinates(tops.length, height, i -> tops[i],
				i -> bottoms[i], i -> i);

		// create model
		return WWTextureDataUtils.buildVerticalTextureData(width, height, dataBuffer, coordinates);
	}

}
