/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.g3d.service.icon;

import java.util.Optional;
import java.util.Set;

import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.io.g3d.info.G3dInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.utils.image.ImageResources;

/**
 * Offer the icon to represent a G3D file.
 */
@Component(name = "globe_drivers_g3d_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class G3dIcon16ForFileProvider implements IIcon16ForFileProvider {

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentTypes() {
		return Set.of(ContentType.G3D_NETCDF_4);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Image> getIcon() {
		// Can’t guess the icon without the IFileInfo
		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Image> getIcon(IFileInfo fileInfo) {
		Image result = null;
		if (fileInfo instanceof G3dInfo) {
			G3dInfo gdfInfo = (G3dInfo) fileInfo;
			// Choose the icon according to the G3D content
			result = ImageResources.getImage(
					gdfInfo.containsPolarEchograms() ? "icons/16/polarEcho.png" : "icons/16/echogramLong.png",
					getClass());
		}
		return Optional.ofNullable(result);
	}

}