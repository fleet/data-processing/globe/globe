package fr.ifremer.globe.drivers.bsar;

import java.util.Optional;
import java.util.Set;

import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Offer the icon to represent a bsar.nc file.
 */
@Component(name = "globe_drivers_bsar_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class BsarIconProvider implements IIcon16ForFileProvider {

	@Override
	public Set<ContentType> getContentTypes() {
		return Set.of(ContentType.BSAR_NETCDF_4);
	}

	@Override
	public Optional<Image> getIcon() {
		return Optional.of(Icons.CHART_LINE.toImage());
	}
}
