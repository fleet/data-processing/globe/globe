/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.marker.service.worldwind;

import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.marker.info.MarkerFileInfo;
import fr.ifremer.globe.core.io.marker.info.WCMarkerFileInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.marker.IMarker;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.marker.IWWMarkerLayer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 *
 */
@Component(name = "globe_drivers_marker_layer_provider", service = IWWLayerProvider.class)
public class MarkerLayerProvider extends BasicWWLayerProvider<MarkerFileInfo<? extends IMarker>> {

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return Set.of(ContentType.MARKER_CSV);
	}

	@Override
	protected void createLayers(MarkerFileInfo<? extends IMarker> markerInfo, WWFileLayerStore layerStore,
			IProgressMonitor monitor) throws GIOException {
		IWWMarkerLayer layer = markerInfo instanceof WCMarkerFileInfo
				? layerFactory.createWaterColumnMarkerLayer((WCMarkerFileInfo) markerInfo)
				: layerFactory.createMarkerLayer(markerInfo);
		layer.setName(markerInfo.getBaseName());
		layerStore.addLayers(layer);
		monitor.done();
	}
}
