/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.xsf.service.icon;

import java.util.Optional;
import java.util.Set;

import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.io.xsf.info.DeprecatedXsfInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.utils.image.ImageResources;

/**
 * Offer the icon to represent a xsf file.
 */
@Component(name = "globe_drivers_xsf_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class XsfIcon16ForFileProvider implements IIcon16ForFileProvider {

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentTypes() {
		return Set.of(ContentType.POLE_INF, ContentType.XSF_NETCDF_4, ContentType.OLD_XSF_NETCDF_4);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Image> getIcon() {
		// Can’t guess the icon without the IFileInfo
		return Optional.empty();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Image> getIcon(IFileInfo fileInfo) {
		Image result = null;
		if (fileInfo instanceof DeprecatedXsfInfo) {
			result = ImageResources.getImage(
					"icons/16/loadingError.png", getClass());
		} else {
			result = ImageResources.getImage("icons/16/xsf.png", getClass());
		}
		return Optional.ofNullable(result);
	}

}