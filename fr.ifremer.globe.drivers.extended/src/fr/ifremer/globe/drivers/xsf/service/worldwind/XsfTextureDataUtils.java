package fr.ifremer.globe.drivers.xsf.service.worldwind;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;

import fr.ifremer.globe.core.model.sounder.AntennaInstallationParameters;
import fr.ifremer.globe.core.model.sounder.datacontainer.SonarBeamGroup;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.DoubleLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDataUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.mbes.MbesUtils;

/**
 * Utility class for factoring some useful methods for IWWTextureData producers.
 */
public class XsfTextureDataUtils {

	/**
	 * Computes texture coordinates.
	 */
	public static List<IWWTextureCoordinate> computeCoordinates(SounderDataContainer sounderDataContainer, int width,
			int height, IProgressMonitor monitor) throws GIOException {
		SubMonitor progress = SubMonitor.convert(monitor, width);
		List<IWWTextureCoordinate> coordinates = new ArrayList<>();

		// Retrieve current beam group
		SonarBeamGroup sonarBeamGroup = sounderDataContainer.getBeamGroup();
		DoubleLayer1D latitudeLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.PLATFORM_LATITUDE);
		DoubleLayer1D longitudeLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.PLATFORM_LONGITUDE);
		FloatLayer1D headingLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.PLATFORM_HEADING);
		FloatLayer1D soundspeedLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.SOUND_SPEED_AT_TRANSDUCER);
		FloatLayer1D wcSampleIntervalLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.SAMPLE_INTERVAL);
		Optional<FloatLoadableLayer1D> waterlineLayer = sonarBeamGroup
				.getOptionalLayer(BeamGroup1Layers.WATERLINE_TO_CHART_DATUM);
		Optional<FloatLoadableLayer1D> txDepthLayer = sonarBeamGroup
				.getOptionalLayer(BeamGroup1Layers.TX_TRANSDUCER_DEPTH);

		AntennaInstallationParameters rxParameters = sounderDataContainer.getInfo().getRxParameters().get(0);

		float previousValidHeading = 0;
		float previousSampleInterval = Float.NaN;
		float previousHeightOfOneCellInMeter = Float.NaN;
		int previousSwathIndex = -4;

		for (int swathIndex = 0; !progress.isCanceled() && swathIndex < width; swathIndex++) {
			boolean isLast = swathIndex == width - 1;

			// check if this swath is at the beginning of a texture block : first
			// element after a change of sampleInterval. In that case an additional texture
			// coordinate should be compute at the left of the pixel.
			// Other coordinates are computed at the center of the pixel or skipped
			float sampleInterval = wcSampleIntervalLayer.get(swathIndex);
			boolean intervalChanged = (sampleInterval != previousSampleInterval) && !coordinates.isEmpty();

			// loop on swath 4 by 4 (to avoid overlapping issues with multiping) & keep the
			// last and first swath of each texture block
			if ((swathIndex > previousSwathIndex + 3) || intervalChanged || isLast) {
				// Sounds speed approximation : retrieved from surface...
				float heightOfOneCellInMeter = soundspeedLayer.get(swathIndex) * sampleInterval / 2f;

				// get coordinates only if cell height is available
				if (Float.isNaN(heightOfOneCellInMeter)) {
					continue;
				}

				double shipLatitude = latitudeLayer.get(swathIndex);
				double shipLongitude = longitudeLayer.get(swathIndex);
				float heading = headingLayer.get(swathIndex);
				if (Float.isNaN(heading)) {
					heading = previousValidHeading;
				} else {
					previousValidHeading = heading;
				}
				double[] latlon = MbesUtils.acrossAlongToWGS84LatLong(shipLatitude, shipLongitude, heading,
						rxParameters.yoffset, rxParameters.xoffset);

				// center the texture coordinate on pixel 
				float s = (swathIndex + 0.5f) / width;

				float waterline = 0.f;
				if (waterlineLayer.isPresent()) {
					waterline = waterlineLayer.get().get(swathIndex);
					if (waterlineLayer.get().getFillValue() == waterline || Float.isNaN(waterline))
						waterline = 0.f;
				}
				float txElevation = waterline;
				if (txDepthLayer.isPresent()) {
					float txDepth = txDepthLayer.get().get(swathIndex);
					if (!Float.isNaN(txDepth))
						txElevation -= txDepth;
				}
				float top = txElevation;
				float bottom = txElevation - heightOfOneCellInMeter * height;

				if (intervalChanged) {
					// add top & bottom coordinates to close a texture block
					float closingS = ((float) swathIndex) / width;
					float closingBottom = txElevation - previousHeightOfOneCellInMeter * height;
					coordinates.add(WWTextureDataUtils.buildTopTextureCoordinate(latlon[0], latlon[1], top, closingS));
					coordinates.add(
							WWTextureDataUtils.buildBottomTextureCoordinate(latlon[0], latlon[1], closingBottom, closingS));
				}

				// add top & bottom coordinates for this 's'
				coordinates.add(WWTextureDataUtils.buildTopTextureCoordinate(latlon[0], latlon[1], top, s));
				coordinates.add(WWTextureDataUtils.buildBottomTextureCoordinate(latlon[0], latlon[1], bottom, s));

				previousSwathIndex = swathIndex;
				previousSampleInterval = sampleInterval;
				previousHeightOfOneCellInMeter = heightOfOneCellInMeter;
			}
			progress.worked(1);
		}

		return coordinates;

	}
}