/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.xsf.service.worldwind;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.widgets.Display;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.Activator;
import fr.ifremer.globe.core.io.sonar.info.SonarNcInfo;
import fr.ifremer.globe.core.io.xsf.info.XsfInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.WWLayerPreferences;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWTextureLayer;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureDataCache;
import fr.ifremer.globe.ui.utils.image.Icons;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Driver to manage XSF files.
 */
@Component(name = "globe_drivers_xsf_layer_provider", service = { IWWLayerProvider.class, //
		XsfLayerProvider.class // Useful for SonarNcLayerLoader
})
public class XsfLayerProvider implements IWWLayerProvider {

	/** Logger */
	protected static final Logger LOGGER = LoggerFactory.getLogger(XsfLayerProvider.class);

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;
	@Reference
	WWLayerPreferences layerPreferences;
	@Reference
	IWWTextureDataCache textureDataCache;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return Set.of(ContentType.XSF_NETCDF_4);
	}

	/**
	 * @see fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider#getFileLayers(java.lang.String,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		return getContentType().contains(fileInfo.getContentType()) //
				&& fileInfo instanceof ISounderNcInfo sounderNcInfo //
				&& sounderNcInfo.getBeamGroupCount() > 0
						? Optional.of(createLayers(sounderNcInfo, monitor)).filter(layers -> !layers.isEmpty())
								.map(layers -> new WWFileLayerStore(sounderNcInfo, layers))
						: Optional.empty();
	}

	/**
	 * Creates layers to render the {@link XsfInfo}.
	 */
	public List<IWWLayer> createLayers(ISounderNcInfo sounderNcInfo, IProgressMonitor monitor) {

		// Water column
		List<IWWLayer> result = new ArrayList<>();
		if (sounderNcInfo.getWcRxBeamCount() != 0) {
			// Native Ping water column slices
			layerFactory.createNativeWCLayer(sounderNcInfo).ifPresent(result::add);

			// Ping water column slices
			if (result.isEmpty() || layerPreferences.isUsingWcLegacyLayers())
				result.add(layerFactory.createWCLayer(sounderNcInfo));

			if (sounderNcInfo.getWcRxBeamCount() > 1) {
				IWWTextureLayer layer = createFlattenTextureLayer(sounderNcInfo);
				result.add(layer);
			}
		}

		monitor.done();
		return result;
	}

	/** For a multi-beam sonar file, creates a texture layer to display flatten water column longitudinal slice */
	private IWWTextureLayer createFlattenTextureLayer(ISounderNcInfo sounderNcInfo) {
		String name = "wc_flatten_" + sounderNcInfo.getFilename();
		XsfFlattenWCTextureData dataSupplier = new XsfFlattenWCTextureData(sounderNcInfo, name);
		XsfFlattenWCTextureDisplayParams params = new XsfFlattenWCTextureDisplayParams();
		IWWTextureLayer layer = layerFactory.createWWComputableTextureLayer(name, dataSupplier, params,
				XsfFlattenWCTextureData.DEFAULT_COMPUTE_PARAMETERS);
		layer.setIcon(Icons.WC_SLICE.toImage());
		layer.setShortName("Flatten water column");

		layer.addPropertyChangeListener(IWWTextureLayer.DISPLAY_PARAMETERS_PROPERTY, e -> params.saveInPreferences());
		// optionnaly Autoload flatten WC for SonarNc files or when texture present in cache
		if (textureDataCache.isInCache(sounderNcInfo, name)
				|| (Activator.getXsfWCPreferences().getComputeFlattenTextureOnLoading()
						&& sounderNcInfo instanceof SonarNcInfo)) {
			Display.getDefault().asyncExec(() -> {
				layer.buildTextureRenderable();
				layer.setEnabled(true);
			});
		}

		return layer;
	}

}