package fr.ifremer.globe.drivers.xsf.service.worldwind;

import fr.ifremer.globe.core.utils.preference.PreferenceComposite;
import fr.ifremer.globe.core.utils.preference.PreferenceRegistry;
import fr.ifremer.globe.core.utils.preference.attributes.IntegerPreferenceAttribute;
import fr.ifremer.globe.core.utils.preference.attributes.NumberPreferenceAttribute;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDisplayParameters;
import fr.ifremer.globe.ui.utils.color.ColorMap.ColorMaps;

/**
 * {@link WWTextureDisplayParameters} linked with preference values.
 */
public class XsfFlattenWCTextureDisplayParams extends WWTextureDisplayParameters {

	private static PreferenceComposite preferenceWCXSf;
	private static NumberPreferenceAttribute minContrastPrefAtt;
	private static NumberPreferenceAttribute maxContrastPrefAtt;
	private static NumberPreferenceAttribute minThresholdPrefAtt;
	private static NumberPreferenceAttribute maxThresholdPrefAtt;
	private static IntegerPreferenceAttribute colorMapIndexPrefAtt;

	/**
	 * Preference declaration
	 */
	static {
		PreferenceComposite rootNode = PreferenceRegistry.getInstance().getViewsSettingsNode().getChild("3D Viewer");
		preferenceWCXSf = new PreferenceComposite(rootNode, "XSF Flatten Water Column");

		minContrastPrefAtt = new NumberPreferenceAttribute("MIN_CONTRAST", "Min contrast", -64f);
		preferenceWCXSf.declareAttribute(minContrastPrefAtt);
		maxContrastPrefAtt = new NumberPreferenceAttribute("MAX_CONTRAST", "Max contrast", 64f);
		preferenceWCXSf.declareAttribute(maxContrastPrefAtt);

		minThresholdPrefAtt = new NumberPreferenceAttribute("MIN_Threshold", "Min Threshold", -64f);
		preferenceWCXSf.declareAttribute(minThresholdPrefAtt);
		maxThresholdPrefAtt = new NumberPreferenceAttribute("MAX_Threshold", "Max Threshold", 64f);
		preferenceWCXSf.declareAttribute(maxThresholdPrefAtt);
		int colorIndex = ColorMaps.UDIV_CMOCEAN_DELTA.getIndex();
		colorMapIndexPrefAtt = new IntegerPreferenceAttribute("COLOR_MAP_INDEX", "Color map index", colorIndex);
		preferenceWCXSf.declareAttribute(colorMapIndexPrefAtt);

		// load preference values
		preferenceWCXSf.load();
	}

	/**
	 * Constructor
	 */
	public XsfFlattenWCTextureDisplayParams() {
		super(1f, minContrastPrefAtt.getValue().floatValue(), maxContrastPrefAtt.getValue().floatValue(),
				minContrastPrefAtt.getValue().floatValue(), maxContrastPrefAtt.getValue().floatValue(),
				minThresholdPrefAtt.getValue().floatValue(), maxThresholdPrefAtt.getValue().floatValue(),
				colorMapIndexPrefAtt.getValue(), false, false, false, false, 0f);
	}

	/** Save values in preferences */
	public void saveInPreferences() {
		minContrastPrefAtt.setValue(getMinContrast(), false);
		maxContrastPrefAtt.setValue(getMaxContrast(), false);
		minThresholdPrefAtt.setValue(getMinThreshold(), false);
		maxThresholdPrefAtt.setValue(getMaxThreshold(), false);
		colorMapIndexPrefAtt.setValue(getColorMapIndex(), false);
		preferenceWCXSf.save();
	}

}
