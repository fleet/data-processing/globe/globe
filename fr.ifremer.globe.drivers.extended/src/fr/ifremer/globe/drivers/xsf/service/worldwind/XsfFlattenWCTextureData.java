package fr.ifremer.globe.drivers.xsf.service.worldwind;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;

import fr.ifremer.globe.core.Activator;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.model.sounder.datacontainer.ISounderDataContainerToken;
import fr.ifremer.globe.core.model.sounder.datacontainer.SonarBeamGroup;
import fr.ifremer.globe.core.model.sounder.datacontainer.SounderDataContainer;
import fr.ifremer.globe.core.model.wc.XsfWCPreferences;
import fr.ifremer.globe.core.runtime.datacontainer.DataContainer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.Float2DVlenLayer;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLayer1D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.FloatLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.IntLoadableLayer2D;
import fr.ifremer.globe.core.runtime.datacontainer.layers.buffers.vlen.Float3DVlenBuffer;
import fr.ifremer.globe.core.runtime.datacontainer.predefinedlayers.sonar.BeamGroup1Layers;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerOwner;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.drivers.application.context.ContextInitializer;
import fr.ifremer.globe.ui.service.worldwind.layer.texture.IWWComputableTextureLayer.ComputableTextureParameters;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureCoordinate;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureData;
import fr.ifremer.globe.ui.service.worldwind.texture.IWWTextureDataCache;
import fr.ifremer.globe.ui.service.worldwind.texture.WWTextureDataUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.signal.SignalUtils;

/**
 * This class computes a {@link IWWTextureData} by flattening water column echo
 * for each range.
 */
public class XsfFlattenWCTextureData implements Function<ComputableTextureParameters, IWWTextureData> {

	/** Factory of the sounder data container */
	private final IDataContainerFactory containerFactory = IDataContainerFactory.grab();

	/** {@link IDataContainerOwner} to book the {@link DataContainer} **/
	private final IDataContainerOwner dataContainerOwner = IDataContainerOwner.generate("Water column flatten", true);

	/** Input {@link ISounderNcInfo} **/
	private final ISounderNcInfo sounderNcInfo;
	/** Name of the WW layer in creation **/
	private final String layerName;

	/** Texture width (= swath count) **/
	private final int width;

	/** Texture height (= sample count; depends of swath: keep maximum) **/
	private int height;

	private XsfWCPreferences displayPreferences;

	private boolean isReson = false;

	public static final ComputableTextureParameters DEFAULT_COMPUTE_PARAMETERS = new ComputableTextureParameters(false,
			0, 800);

	/**
	 * Constructor
	 */
	public XsfFlattenWCTextureData(ISounderNcInfo sounderNcInfo, String layerName) {
		this.sounderNcInfo = sounderNcInfo;
		this.layerName = layerName;
		width = sounderNcInfo.getCycleCount();
		displayPreferences = Activator.getXsfWCPreferences();
		isReson = sounderNcInfo.getConstructor().toLowerCase().contains("Reson".toLowerCase());

	}

	/**
	 * If not already done: computes {@link IWWTextureData} from
	 * {@link ISounderNcInfo}.
	 *
	 * @return {@link IWWTextureData}
	 */

	@Override
	public IWWTextureData apply(ComputableTextureParameters parameters) {
		var textureDataCache = ContextInitializer.getInContext(IWWTextureDataCache.class);
		// List of texture data generated from the provided XSF file (length = 1 here)
		IWWTextureData textureData = textureDataCache.restore(sounderNcInfo, layerName).orElse(null);
		if (textureData == null) {
			var createdTextureData = new AtomicReference<IWWTextureData>();
			var processService = ContextInitializer.getInContext(IProcessService.class);
			processService.runInForeground("Water column flattening...", (monitor, logger) -> {
				SubMonitor progress = SubMonitor.convert(monitor, 100);
				// Open a data container (throws an exception if fails)
				try (ISounderDataContainerToken sounderDataContainerToken = containerFactory.book(sounderNcInfo,
						dataContainerOwner)) {
					SounderDataContainer sounderDataContainer = sounderDataContainerToken.getDataContainer();

					if (displayPreferences.getBeamGroupDefaultValue() > 0 && displayPreferences
							.getBeamGroupDefaultValue() <= sounderDataContainer.getInfo().getBeamGroupCount()) {
						sounderDataContainer.getBeamGroup(displayPreferences.getBeamGroupDefaultValue());
					}

					int beamStart = parameters != null && parameters.isEnabled() ? parameters.min() : 0;
					int beamStop = parameters != null && parameters.isEnabled() ? parameters.max() : Integer.MAX_VALUE;

					// Compute texture data
					ByteBuffer dataBuffer = computeData(sounderDataContainer, progress.split(50), beamStart, beamStop);
					if (progress.isCanceled()) {
						return Status.CANCEL_STATUS;
					}

					// Compute texture coordinates
					List<IWWTextureCoordinate> coordinates = XsfTextureDataUtils
							.computeCoordinates(sounderDataContainer, width, height, progress.split(50));

					createdTextureData
							.set(WWTextureDataUtils.buildVerticalTextureData(width, height, dataBuffer, coordinates));
				}
				return Status.OK_STATUS;
			});
			textureData = createdTextureData.get();
			if (textureData != null)
				textureDataCache.save(sounderNcInfo, textureData, layerName);

		}
		return textureData;
	}

	/**
	 * If WC is in Amplitude transform values to db
	 */
	public void todB(Float3DVlenBuffer waterColumnData) {
		if (displayPreferences.isDisplayAmplitudeAsdB() && isReson) {
			double offset = displayPreferences.getResonGlobalOffset();
			waterColumnData.applyTransform(v -> SignalUtils.amplitudeTodB(v - offset + 1));
		}
	}

	/**
	 * Computes data buffer.
	 *
	 * @param monitor
	 */
	private ByteBuffer computeData(SounderDataContainer sounderDataContainer, IProgressMonitor monitor,
			int beamIndexStart, int beamIndexStop) throws GIOException {

		// Retrieve current beam group
		SonarBeamGroup sonarBeamGroup = sounderDataContainer.getBeamGroup();
		// get layers
		Float2DVlenLayer waterColumnLayer = sonarBeamGroup.getVlenLayer(BeamGroup1Layers.BACKSCATTER_R_VARIABLE_NAME);
		FloatLayer1D soundspeedLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.SOUND_SPEED_AT_TRANSDUCER);
		IntLoadableLayer2D txd = sonarBeamGroup.getLayer(BeamGroup1Layers.TRANSMIT_BEAM_INDEX);
		FloatLayer1D wcSampleIntervalLayer = sonarBeamGroup.getLayer(BeamGroup1Layers.SAMPLE_INTERVAL);

		Optional<FloatLoadableLayer2D> bottomDetectionRangeLayer = Optional.empty();
		FloatLoadableLayer2D sampleTimeOffset = null;
		FloatLoadableLayer2D blankingInterval = null;
		if (sounderDataContainer.hasLayer(BeamGroup1Layers.DETECTED_BOTTOM_RANGE)) {
			bottomDetectionRangeLayer = Optional.of(sonarBeamGroup.getLayer(BeamGroup1Layers.DETECTED_BOTTOM_RANGE));
			sampleTimeOffset = sonarBeamGroup.getLayer(BeamGroup1Layers.SAMPLE_TIME_OFFSET); // MANDATORY
			blankingInterval = sonarBeamGroup.getLayer(BeamGroup1Layers.BLANKING_INTERVAL); // MANDATORY
			// range is
			// sound_speed_at_transducer*(blanking_interval+bottom_index*sample_interval -
			// sample_time_offset)/2.
		}

		height = 0;
		float[][] computedValues = new float[width][];
		float[] heightOfOneCellInMeter = new float[width];

		String taskTitle = "Water column flattening..." + (beamIndexStop != Integer.MAX_VALUE
				? String.format(" (beam index [%d:%d])", beamIndexStart, beamIndexStop)
				: " (no filter)");

		// for each swath
		monitor.beginTask(taskTitle, width);
		for (int swathIndex = 0; swathIndex < width; swathIndex++) {
			waterColumnLayer.load(swathIndex);
			float sampleInterval = wcSampleIntervalLayer.get(swathIndex);
			Float3DVlenBuffer waterColumnData = waterColumnLayer.getData();

			todB(waterColumnData);

			// compute range count
			int rangeCount = waterColumnData.getRowCount();
			if (bottomDetectionRangeLayer.isPresent()) {
				int beam = waterColumnData.getColCount() / 2;
				int sector = Math.max(txd.get(swathIndex, beam), 0);
				double bottomRangeInMeter = bottomDetectionRangeLayer.get().get(swathIndex, beam);
				double speed = soundspeedLayer.get(swathIndex);
				double blanking = blankingInterval.get(swathIndex, beam);
				double timeOffset = sampleTimeOffset.get(swathIndex, sector);

				int bottomRangeIndex = (int) Math
						.round((2 * bottomRangeInMeter / speed + timeOffset - blanking) / sampleInterval);
				// range is
				// sound_speed_at_transducer*(blanking_interval+bottom_index*sample_interval -
				// sample_time_offset)/2.
				rangeCount = bottomRangeIndex > 1 ? Math.min(waterColumnData.getRowCount(), bottomRangeIndex + 20) // margin
						: waterColumnData.getRowCount(); // if no bottom, display all
			}

			//compute beam count 
			int beamCount = Math.min(beamIndexStop, waterColumnData.getColCount() - 1) - beamIndexStart + 1;

			// compute means for each range
			computedValues[swathIndex] = new float[rangeCount];
			for (int rangeIndex = 0; rangeIndex < rangeCount; rangeIndex++) {
				double rangeSum = 0;
				for (int beamIndex = beamIndexStart; beamIndex < beamIndexStart + beamCount; beamIndex++) {
					rangeSum += SignalUtils.dBToAmplitude(waterColumnData.get(rangeIndex, beamIndex, 0));
				}
				computedValues[swathIndex][rangeIndex] = (float)SignalUtils.amplitudeTodB(rangeSum / beamCount);
			}

			// if necessary : redefine texture height
			height = Math.max(height, rangeCount);

			// Sounds speed approximation : retrieved from surface...
			heightOfOneCellInMeter[swathIndex] = soundspeedLayer.get(swathIndex) * sampleInterval / 2f;

			// update monitor (or cancel if asked)
			if (monitor.isCanceled()) {
				return null;
			}
			monitor.worked(1);
		}

		if (height == 0)
			throw new GIOException("No data to compute water column flatten texture.");

		// fill texture data buffer with computed values
		ByteBuffer dataBuffer = ByteBuffer.allocateDirect(width * height * Float.BYTES).order(ByteOrder.nativeOrder());
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				dataBuffer.putFloat(row < computedValues[col].length ? computedValues[col][row] : Float.NaN);
			}
		}
		dataBuffer.rewind();
		return dataBuffer;
	}

}
