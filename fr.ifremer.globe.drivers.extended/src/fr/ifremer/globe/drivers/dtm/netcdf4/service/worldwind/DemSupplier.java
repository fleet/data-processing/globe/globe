/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.dtm.netcdf4.service.worldwind;

import java.io.File;
import java.io.IOException;
import java.util.function.Supplier;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.gdal.osr.SpatialReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.gdal.programs.GdalTranslate;
import fr.ifremer.globe.gdal.programs.GdalWarp;
import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * Supplies a DTM layer as a TiFF file.
 */
public class DemSupplier implements Supplier<File> {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(DemSupplier.class);

	/** Progression monitor */
	protected IProgressMonitor monitor;

	/** Input DTM raster **/
	protected RasterInfo rasterInfo;

	/** Resulting file */
	protected File suppliedFile;

	/** Target projection **/
	private SpatialReference targetSpatialReference = GdalOsrUtils.SRS_WGS84;

	/** Output temporary file prefix **/
	private String outputTmpFilePrefix = getClass().getSimpleName();

	/**
	 * Constructor
	 */
	public DemSupplier(RasterInfo rasterInfo, IProgressMonitor monitor) {
		this.rasterInfo = rasterInfo;
		this.monitor = monitor;
	}

	/**
	 * Constructor with specific {@link SpatialReference} and output file prefix.
	 */
	public DemSupplier(RasterInfo rasterInfo, SpatialReference targetSpatialReference, String outputTmpFilePrefix,
			IProgressMonitor monitor) {
		super();
		this.monitor = monitor;
		this.rasterInfo = rasterInfo;
		this.targetSpatialReference = targetSpatialReference;
		this.outputTmpFilePrefix = outputTmpFilePrefix;
	}

	/**
	 * @see java.util.function.Supplier#get()
	 */
	@Override
	public File get() {
		if (suppliedFile == null) {
			translateToTif();
		}
		return suppliedFile;
	}

	/**
	 * Extract the layer and translate it in a Tif file. <br>
	 * Reproject if necessary
	 */
	protected void translateToTif() {
		try {
			suppliedFile = TemporaryCache.createTemporaryFile(outputTmpFilePrefix, ".tif");
			suppliedFile.deleteOnExit();

			var originalDataset = GdalDataset.open(rasterInfo.getGdalLoadingKey());
			if (originalDataset.isPresent()) {
				monitor.subTask("Extracting raster");
				// Specific case for old DTM. Must translate to apply offset and scale factor to values
				if (rasterInfo.getContentType() == ContentType.DTM_NETCDF_3) {
					GdalTranslate translate = new GdalTranslate(originalDataset.get());
					translate.setUnscale();
					translate.setOutputType(Float.class);
					translate.setProjection(originalDataset.getSpatialReference());
					var result = translate.run(new NullProgressMonitor());
					result.deleteOnClose(true);
					originalDataset.close();
					originalDataset = result;
				}
			}

			if (originalDataset.isPresent()) {
				var dataset = originalDataset.get();
				GdalWarp gdalWarp = new GdalWarp(dataset, suppliedFile.getPath()).addProgressMonitor(monitor);
				gdalWarp.addMetadata(false);
				gdalWarp.addTargetNoData(-32768f);
				if (!rasterInfo.getProjection().getSpatialReference().equals(targetSpatialReference)) {
					gdalWarp.addTargetProjection(targetSpatialReference);
					if (targetSpatialReference.equals(GdalOsrUtils.SRS_WGS84)) {
						try {
							var nsew = GdalUtils.getBoundingBox(dataset);
							var wn = rasterInfo.getProjection().unproject(nsew[3], nsew[0]);
							var es = rasterInfo.getProjection().unproject(nsew[2], nsew[1]);
							// Spanning 180th meridian ?
							if (wn[0] > es[0]) {
								es[0] = 360.0 + es[0];
							}
							gdalWarp.addTargetExtent(new double[] { wn[0], es[1], es[0], wn[1] });
							gdalWarp.addTargetSize(dataset.getRasterXSize(), dataset.getRasterYSize());
						} catch (ProjectionException e) {
							logger.warn("Unable to evalute the resolution of the raster {}", e.getMessage());
						}
					}
				}
				gdalWarp.runAndClose();
			}
		} catch (IOException e) {
			logger.warn("Can't create the temporary file to reproject the dtm file {}", rasterInfo.getGdalLoadingKey());
		}
	}

	/**
	 * Compute min and max values for the specified file
	 *
	 * @return the Dataset containing all the values
	 */
	private GdalDataset loadFully(RasterInfo rasterInfo) {
		var originalDataset = GdalDataset.open(rasterInfo.getGdalLoadingKey());
		GdalDataset result = originalDataset;
		if (originalDataset.isPresent()) {
			if (!rasterInfo.isFullyLoaded()) {
				// Specific case for old DTM. Must translate to apply offset and scale factor to values
				if (rasterInfo.getContentType() == ContentType.DTM_NETCDF_3) {
					GdalTranslate translate = new GdalTranslate(originalDataset.get());
					translate.setUnscale();
					translate.setOutputType(Float.class);
					translate.setProjection(originalDataset.getSpatialReference());
					result = translate.run(new NullProgressMonitor());
					originalDataset.close();
				}
				rasterInfo.setNoDataValue(result.getNoDataValue());
				result.computeMinMax().ifPresent(rasterInfo::setMinMaxValues);
				// An old DTM is never considered as fully loaded to always force the translate
				rasterInfo.setFullyLoaded(rasterInfo.getContentType() != ContentType.DTM_NETCDF_3);
			}
		}
		return result;
	}

}
