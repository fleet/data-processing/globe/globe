/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.dtm.netcdf4.service.icon;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a dtm file.
 */
@Component(name = "globe_drivers_dtm_netcdf4_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class DtmIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public DtmIcon16ForFileProvider() {
		super("icons/16/dtm.png", ContentType.DTM_NETCDF_4, ContentType.DTM_NETCDF_3);
	}
}