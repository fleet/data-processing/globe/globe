package fr.ifremer.globe.drivers.dtm.netcdf4.handlers;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconstConstants;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridCoverageFactory;
import org.geotools.coverage.grid.io.AbstractGridFormat;
import org.geotools.gce.geotiff.GeoTiffFormat;
import org.geotools.gce.geotiff.GeoTiffWriteParams;
import org.geotools.gce.geotiff.GeoTiffWriter;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.metadata.math.Statistics;
import org.geotools.referencing.CRS;
import org.opengis.parameter.GeneralParameterValue;
import org.opengis.parameter.ParameterValueGroup;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import fr.ifremer.globe.core.Activator;
import fr.ifremer.globe.core.GeotiffExportPreference.MercatorConvert;
import fr.ifremer.globe.core.io.dtm.netcdf4.info.DtmInfoSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.gdal.GdalUtils;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import it.geosolutions.imageio.plugins.tiff.BaselineTIFFTagSet;

/**
 * Handler which manage export of Globe files to the GeoTIFF format.
 */
public class ExportToTiffWithGeotoolsHandler extends AbstractNodeHandler {

	private final String TIFF_EXTENSION = ".tif";

	/** InfoLoader of a dtm file */
	@Inject
	protected DtmInfoSupplier dtmInfoSupplier;

	@Inject
	protected IRasterService rasterService;

	private Optional<File> inputFile;

	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		List<FileInfoNode> selectedFiles = getSelectionAsList(getSelection(selectionService),
				ContentType.DTM_NETCDF_4::equals);
		inputFile = selectedFiles.size() == 1 //
				? Optional.of(selectedFiles.get(0).getFileInfo().toFile())//
				: Optional.empty();
		return inputFile.isPresent();
	}

	@CanExecute
	public boolean canExecute(ESelectionService selectionService, MUIElement modelService) {
		return checkExecution(selectionService, modelService);
	}

	@Execute
	public void execute(ESelectionService selectionService, @Named(IServiceConstants.ACTIVE_SHELL) Shell shell)
			throws GIOException {
		var driver = gdal.GetDriverByName(GdalUtils.TIF_DRIVER_NAME);
		var extensions = driver.GetMetadataItem(gdalconstConstants.GDAL_DMD_EXTENSIONS).split(" ");
		var filters = new String[extensions.length];
		for (int i = 0; i < extensions.length; i++) {
			extensions[i] = "." + extensions[i];
			filters[i] = driver.getShortName() + " files (" + extensions[i] + ")";
		}
		String extension = extensions.length > 0 ? extensions[0] : "";
		FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		dialog.setFileName(FileUtils.getBaseName(inputFile.get()) + extension);
		dialog.setFilterExtensions(extensions);
		dialog.setFilterNames(filters);
		dialog.setText("Output file Name");
		String outputFilePath = dialog.open();
		if (outputFilePath == null || inputFile.isEmpty())
			return;

		exportToTIFF(inputFile.get().getAbsolutePath(), outputFilePath, true);
	}

	/**
	 * Export to GeoTIFF with Geotools framework.
	 * 
	 * @param inputFilePath : DTM input file path
	 * @param outputFilePath : TIFF output file path
	 * @param arcgis9Compliance : if true, get formated projection to be accepted by ARCGIS 9 (=Mimosa)
	 * @throws GIOException
	 */
	public void exportToTIFF(String inputFilePath, String outputFilePath, boolean arcgis9Compliance)
			throws GIOException {

		var dtmInfo = dtmInfoSupplier.loadInfo(inputFilePath);

		var resultMessage = new StringBuilder(
				inputFile.get().getName() + " has been successfully exported.\n\nOutput files: ");

		IProcessService.grab().runInForeground("Export to Mimosa GeoTIFF (with Geotool)", (monitor, logger) -> {
			monitor.beginTask("Export " + inputFile.get().getName() + " to GeoTIFF (with Geotool)",
					dtmInfo.getRasterInfos().size() * 3);

			for (RasterInfo rasterInfo : dtmInfo.getRasterInfos()) {
				// one output file per raster
				String currentOutputPath = FileUtils.changeExtension(outputFilePath,
						"_" + rasterInfo.getDataType().replace(" ", "_") + TIFF_EXTENSION);

				// do not export if file already exists
				if (new File(currentOutputPath).exists()) {
					logger.info("Error, file already exists : {}", currentOutputPath);
					resultMessage.append("\nError, file already exists : " + currentOutputPath);
					monitor.worked(3);
					continue;
				}
				monitor.worked(1);
				monitor.subTask("Exporting : " + currentOutputPath);
				logger.info("Exporting {} to GeoTIFF (with Geotool)", currentOutputPath);

				try (var rasterReader = rasterService.getRasterReader(rasterInfo)
						.orElseThrow(() -> new GIOException("Raster not readable"))) {
					// read raster
					rasterReader.open(rasterInfo.getGdalLoadingKey());
					Statistics stats = new Statistics();
					int lineCount = rasterReader.getRasterYSize();
					int columnCount = rasterReader.getRasterXSize();
					float[][] imagePixelData = new float[lineCount][columnCount];
					for (int row = 0; row < lineCount; row++) {
						for (int col = 0; col < columnCount; col++) {
							var line = lineCount - 1 - row;
							var value = rasterReader.getValue(line, col);
							if (value != rasterReader.getNoDataValue()) {
								stats.add(value);
								imagePixelData[row][col] = (float) value;
							} else {
								imagePixelData[row][col] = arcgis9Compliance ? (float) value : Float.NaN;
							}
						}
					}
					monitor.worked(1);

					var projGeoBox = dtmInfo.getGeoBox().project(dtmInfo.getProjection());

					// write TIFF with Geotool
					writeGeoTIFFFile(dtmInfo.getProjection(), //
							projGeoBox.getLeft(), projGeoBox.getRight(), projGeoBox.getBottom(), projGeoBox.getTop(),
							stats, //
							imagePixelData, //
							currentOutputPath, //
							rasterReader.getNoDataValue(), //
							arcgis9Compliance);
					resultMessage.append("\n" + new File(currentOutputPath).getName());
				} catch (IOException | FactoryException e) {
					logger.error("Error : {}", e.getMessage(), e);
					resultMessage.append("\nError : " + e.getMessage());
					return Status.CANCEL_STATUS;
				}
				monitor.worked(1);
			}
			monitor.done();
			return Status.OK_STATUS;
		});

		Messages.openInfoMessage("Export to GeoTIFF", resultMessage.toString());
	}

	/**
	 * Set the GeoTIFF writer configuration and write a GeoTIFF file
	 *
	 * @param projection projection of the input layer
	 * @param statistics {@link Statistics} about the input layer
	 * @param imagePixelData data of the input layer ordered in a 2D table
	 * @param outputFilePath path of the output GeoTIFF file
	 * @param missingValue value which defines a missing data in the input layer
	 * @throws IOException
	 * @throws FactoryException
	 */
	private static void writeGeoTIFFFile(Projection projection, double xMin, double xMax, double yMin, double yMax,
			Statistics statistics, float[][] imagePixelData, String outputFilePath, double missingValue,
			boolean arcgis9Compliance) throws IOException, FactoryException {

		boolean convertMercator = false;
		if (!arcgis9Compliance) {
			convertMercator = Activator.getGeotiffExportPref().getConvertMercator().getValue() == MercatorConvert.Yes;
		}

		// that's a pity but there is a bug in geotools (and gdal) in case of Mercator_2SP projection, the
		// standard_parallel_1 is not taken into account in geotiff.
		// we need to transform proj4 from Mercator_2SP to Mercator_1SP
		CoordinateReferenceSystem crs = CRS.parseWKT(projection.getProj4FormattedWkt(convertMercator));

		ReferencedEnvelope referencedEnvelope = new ReferencedEnvelope(xMin, xMax, yMin, yMax, crs);
		GridCoverage2D coverage = new GridCoverageFactory().create("GridCoverage", imagePixelData, referencedEnvelope);
		GeoTiffWriteParams wp = new GeoTiffWriteParams();
		wp.setCompressionMode(GeoTiffWriteParams.MODE_EXPLICIT);
		wp.setCompressionType("PackBits");
		ParameterValueGroup params = new GeoTiffFormat().getWriteParameters();
		params.parameter(AbstractGridFormat.GEOTOOLS_WRITE_PARAMS.getName().toString()).setValue(wp);

		String xmlStats = "<GDALMetadata><Item name=\"STATISTICS_MAXIMUM\">" + statistics.maximum()
				+ "</Item><Item name=\"STATISTICS_MEAN\">" + statistics.mean()
				+ "</Item><Item name=\"STATISTICS_MINIMUM\">" + statistics.minimum()
				+ "</Item><Item name=\"STATISTICS_STDDEV\">" + statistics.standardDeviation(true)
				+ "</Item></GDALMetadata>";

		GeoTiffWriter writer = new GeoTiffWriter(new File(outputFilePath));
		if (!arcgis9Compliance) {
			// we do not use packbit compression, since esri have pb with it and it prevent from interpeting no data
			// values
			// see https://community.esri.com/thread/101981
			// writer.setMetadataValue(Integer.toString(BaselineTIFFTagSet.TAG_COMPRESSION),
			// Integer.toString(BaselineTIFFTagSet.COMPRESSION_PACKBITS));
			writer.setMetadataValue(Integer.toString(BaselineTIFFTagSet.TAG_SOFTWARE), "Globe");
			// Statistics not used by QGIS or ARCGIS
			writer.setMetadataValue("GDAL:42112", xmlStats);
			writer.setMetadataValue("GDAL:42113", "nan");
		} else {
			writer.setMetadataValue(Integer.toString(BaselineTIFFTagSet.TAG_COMPRESSION),
					Integer.toString(BaselineTIFFTagSet.COMPRESSION_PACKBITS));
			writer.setMetadataValue(Integer.toString(BaselineTIFFTagSet.TAG_SOFTWARE), "Globe");
			writer.setMetadataValue("GDAL:42112", xmlStats);
			writer.setMetadataValue("GDAL:42113", Double.toString(missingValue));
		}

		writer.write(coverage, params.values().toArray(new GeneralParameterValue[1]));
	}
}