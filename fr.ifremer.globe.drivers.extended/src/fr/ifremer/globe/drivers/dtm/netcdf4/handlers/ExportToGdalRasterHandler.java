/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.dtm.netcdf4.handlers;

import java.io.File;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.gdal.gdalconst.gdalconstConstants;

import fr.ifremer.globe.core.Activator;
import fr.ifremer.globe.core.GeotiffExportPreference.MercatorConvert;
import fr.ifremer.globe.core.io.dtm.common.info.DtmInfo;
import fr.ifremer.globe.core.io.dtm.netcdf4.info.DtmInfoSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.runtime.job.IProcessService;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.gdal.programs.Compression;
import fr.ifremer.globe.gdal.programs.GdalWarp;
import fr.ifremer.globe.ui.handler.AbstractNodeHandler;
import fr.ifremer.globe.ui.utils.Messages;
import fr.ifremer.globe.ui.views.projectexplorer.nodes.FileInfoNode;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Handler which manage export of Globe files to the Gdal Raster format.
 */
public class ExportToGdalRasterHandler extends AbstractNodeHandler {

	/**
	 * Parameter of the command defined in the e4xmi. Used to get the expected Gdal
	 * output format
	 */
	static final String PARAM_GDAL_DRIVER = "fr.ifremer.globe.drivers.extended.commandparameter.gdalDriver";

	static final String PARAM_NODATA_VALUE = "fr.ifremer.globe.drivers.extended.commandparameter.gdalDriver";

	/** InfoLoader of a dtm file */
	@Inject
	protected DtmInfoSupplier dtmInfoSupplier;

	private Optional<File> inputFile;

	@CanExecute
	public boolean canExecute(@Named(PARAM_GDAL_DRIVER) String gdalDriverName, ESelectionService selectionService,
			MUIElement uiElement) {
		return checkExecution(selectionService, uiElement) && gdal.GetDriverByName(gdalDriverName) != null;
	}

	/**
	 * Checks if there is one DTM file selected.
	 */
	@Override
	protected boolean computeCanExecute(ESelectionService selectionService) {
		List<FileInfoNode> selectedFiles = getSelectionAsList(getSelection(selectionService),
				ContentType.DTM_NETCDF_4::equals);
		inputFile = selectedFiles.size() == 1 //
				? Optional.of(selectedFiles.get(0).getFileInfo().toFile())//
				: Optional.empty();
		return inputFile.isPresent();
	}

	/**
	 * Executes the export of the selected file.
	 */
	@Execute
	public void execute(@Named(PARAM_GDAL_DRIVER) String gdalDriverName, ESelectionService selectionService,
			@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) throws GIOException {
		var convertToMerc1SP = Activator.getGeotiffExportPref().getConvertMercator().getValue() == MercatorConvert.Yes;
		var dstNoDataValue = Activator.getGeotiffExportPref().getNoDataValue().getValue().floatValue();
		var driver = gdal.GetDriverByName(gdalDriverName);
		var extensions = driver.GetMetadataItem(gdalconstConstants.GDAL_DMD_EXTENSIONS).split(" ");
		var filters = new String[extensions.length];
		for (int i = 0; i < extensions.length; i++) {
			extensions[i] = "." + extensions[i];
			filters[i] = driver.getShortName() + " files (" + extensions[i] + ")";
		}
		String extension = extensions.length > 0 ? extensions[0] : "";
		FileDialog dialog = new FileDialog(shell, SWT.SAVE);
		dialog.setFileName(FileUtils.getBaseName(inputFile.get()) + extension);
		dialog.setFilterExtensions(extensions);
		dialog.setFilterNames(filters);
		dialog.setText("Output file Name");
		String outputFilePath = dialog.open();
		if (outputFilePath == null) {
			return;
		}
		DtmInfo dtmInfo = dtmInfoSupplier.loadInfo(inputFile.get().getAbsolutePath());
		var resultMessage = new StringBuilder(inputFile.get().getName() + " has been successfully exported." + //
				(convertToMerc1SP ? "\n(Convert Mercator_2SP to Mercator_1SP.)" : "") + "\n\nOutput files: ");

		IProcessService.grab().runInForeground("Export " + driver.getLongName(), (monitor, logger) -> {
			monitor.beginTask("Export " + inputFile.get().getName() + " to " + driver.getLongName(),
					dtmInfo.getRasterInfos().size() * 3);
			for (RasterInfo rasterInfo : dtmInfo.getRasterInfos()) {
				String currentOutputPath = outputFilePath.replace(extension,
						"_" + rasterInfo.getDataType().replace(" ", "_") + extension);

				// Do not export if file already exists
				if (new File(currentOutputPath).exists()) {
					logger.info("Error, file already exists : {}", currentOutputPath);
					resultMessage.append("\nError, file already exists : " + currentOutputPath);
					monitor.worked(3);
					continue;
				}

				monitor.subTask("Exporting : " + currentOutputPath);
				logger.info("Exporting {} to {}", currentOutputPath, gdalDriverName);

				// open current dataset
				try (GdalDataset dataset = GdalDataset.open(rasterInfo.getGdalLoadingKey())) {
					monitor.worked(1);

					GdalWarp gdalWarp = new GdalWarp(dataset.get(), currentOutputPath);
					// Arcgis does not support tiff with non finite NoDataValue
					if (!Double.isFinite(rasterInfo.getNoDataValue()))
						gdalWarp.addTargetNoData(dstNoDataValue);
					gdalWarp.addCompression(Compression.DEFLATE);
					Dataset result = gdalWarp.run();

					// create Raster
					monitor.worked(1);
					// get correct projection string (handle MERCATOR 1SP / SP2)
					result.SetProjection(dtmInfo.getProjection().getProj4FormattedWkt(convertToMerc1SP));

					// flush
					result.FlushCache();
					result.delete();
					monitor.worked(1);
				}

				resultMessage.append("\n" + new File(currentOutputPath).getName());
			}
			monitor.done();
			return Status.OK_STATUS;
		});

		Messages.openInfoMessage("Export " + driver.getLongName(), resultMessage.toString());
	}

}