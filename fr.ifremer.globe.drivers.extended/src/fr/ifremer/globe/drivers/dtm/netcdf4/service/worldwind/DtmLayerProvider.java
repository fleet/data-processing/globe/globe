/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.dtm.netcdf4.service.worldwind;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fr.ifremer.globe.core.io.dtm.common.info.DtmInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.gdal.programs.GdalColorRelief;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModelFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.WWLayerProviderStatus;
import fr.ifremer.globe.ui.service.worldwind.layer.WWRasterLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.tile.IWWTilesService;
import fr.ifremer.globe.ui.utils.cache.NasaCache;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.terrain.CompoundElevationModel;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWXML;

/**
 * Driver to manage GeoDtmf files.
 */
@Component(name = "globe_drivers_dtm_netcdf4_layer_provider", service = IWWLayerProvider.class, //
		property = { WWLayerProviderStatus.SERVICE_PROPERTY + "=" + WWLayerProviderStatus.DEPRECATED })
public class DtmLayerProvider implements IWWLayerProvider {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(DtmLayerProvider.class);

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** Osgi Service used to tiled image layers */
	@Reference
	protected IWWTilesService tilesInstaller;

	/** Osgi Service used to produce WW ElevationModel */
	@Reference
	protected IWWElevationModelFactory elevationModelFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return Set.of(ContentType.DTM_NETCDF_3, ContentType.DTM_NETCDF_4);
	}

	/**
	 * Compute WW layers to render the Elevation Dtm layer
	 *
	 * @see fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider#getFileLayers(java.lang.String,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		WWFileLayerStore result = null;
		if (fileInfo instanceof DtmInfo) {
			DtmInfo dtmInfo = (DtmInfo) fileInfo;
			if (!dtmInfo.getRasterInfos().isEmpty()) {
				result = new WWFileLayerStore(dtmInfo);
				// At least, the elevation layer
				RasterInfo elevationRasterInfo = dtmInfo.getRasterInfo(RasterInfo.RASTER_ELEVATION).orElse(null);
				if (elevationRasterInfo != null) {
					result.addLayers(createLayersForRaster(fileInfo.getAbsolutePath(), elevationRasterInfo, monitor));
				}

				// Reload other layers if their tiles exists
				for (RasterInfo rasterInfo : dtmInfo.getRasterInfos()) {
					if (rasterInfo != elevationRasterInfo && hasTilesFor(fileInfo.getAbsolutePath(), rasterInfo)) {
						result.addLayers(createLayersForRaster(fileInfo.getAbsolutePath(), rasterInfo, monitor));
					}
				}
			}
		}
		return Optional.ofNullable(result);
	}

	/**
	 * @see fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider#getRasterLayers(fr.ifremer.globe.model.services.raster.RasterInfo,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public Optional<WWRasterLayerStore> getRasterLayers(RasterInfo rasterInfo, IProgressMonitor monitor)
			throws GIOException {
		WWRasterLayerStore result = rasterInfo.getContentType().isOneOf(ContentType.DTM_NETCDF_3,
				ContentType.DTM_NETCDF_4) //
						? createLayersForRaster(computeFileName(rasterInfo.getGdalLoadingKey()), rasterInfo, monitor) //
						: null;

		return Optional.ofNullable(result);
	}

	/**
	 * Creates the worldwind layers to represent the raster in the 3D viewer
	 *
	 * @param dtmPath the dtm file path
	 * @param rasterInfo the raster informations
	 * @param monitor monitor of progression
	 * @throws GIOException creation failed, error occured
	 */
	protected WWRasterLayerStore createLayersForRaster(String dtmPath, RasterInfo rasterInfo, IProgressMonitor monitor)
			throws GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);

		boolean isElevation = RasterInfo.RASTER_ELEVATION.equals(rasterInfo.getDataType());
		String displayName = computeDisplayName(dtmPath, rasterInfo);
		File xmlImageFile = tilesInstaller.computeImageCacheDocument(displayName);

		var geoBox = rasterInfo.getGeoBox();
		File xmlElevationFile1 = null;
		File xmlElevationFile2 = null;
		if (geoBox.isSpanning180Th()) {
			// In case the Geobox is spanning the 180th meridian, split the elevation tiles production
			xmlElevationFile1 = tilesInstaller.computeElevationCacheDocument(displayName + "_West");
			xmlElevationFile2 = tilesInstaller.computeElevationCacheDocument(displayName + "_East");
		} else {
			xmlElevationFile1 = tilesInstaller.computeElevationCacheDocument(displayName);
		}

		DemSupplier demSupplier = new DemSupplier(rasterInfo, subMonitor.split(10));
		if (!xmlImageFile.exists() || isElevation && !xmlElevationFile1.exists()) {
			// Force the extraction of the raster to allow a correct managing of the ProgressMonitor
			demSupplier.get();
		}
		// Ensure we have the minMax values
		monitor.subTask("Computing minimum and maximum values");
		loadFully(rasterInfo, demSupplier);

		// Load or create the XML docunent describing the tiles pyramid of the texture
		Document imagePyramidTiles = createTextureTiles(displayName, rasterInfo, xmlImageFile, demSupplier,
				subMonitor.split(isElevation ? 50 : 90));

		List<IWWLayer> layers = new ArrayList<>();
		ElevationModel elevationModel = null;

		// Load or create the XML docunent describing the tiles pyramid of the elevation
		IWWTerrainLayer rgbLayer = null;
		if (isElevation) {
			rgbLayer = layerFactory.createElevationLayer(new File(dtmPath), imagePyramidTiles,
					rasterInfo.getDataType());
			layers.add(rgbLayer);

			// One or two elevation pyramid ?
			if (xmlElevationFile2 == null) {
				Document elevationPyramidTiles1 = createElevationTiles(displayName, xmlElevationFile1, demSupplier,
						rasterInfo.getGeoBox(), subMonitor.split(40));
				elevationModel = elevationModelFactory
						.createInterpolateElevationModel(elevationPyramidTiles1.getDocumentElement());
			} else {
				// West
				var west = new GeoBox(rasterInfo.getGeoBox().getTop(), rasterInfo.getGeoBox().getBottom(), 180d,
						rasterInfo.getGeoBox().getLeft());
				var elevationPyramidTilesWest = createElevationTiles(displayName + "_West", xmlElevationFile1,
						demSupplier, west, subMonitor.split(40));
				var elevationModelWest = elevationModelFactory
						.createInterpolateElevationModel(elevationPyramidTilesWest.getDocumentElement());
				// East
				var east = new GeoBox(rasterInfo.getGeoBox().getTop(), rasterInfo.getGeoBox().getBottom(),
						rasterInfo.getGeoBox().getRight(), -180d);
				var elevationPyramidTilesEast = createElevationTiles(displayName + "_East", xmlElevationFile2,
						demSupplier, east, subMonitor.split(40));
				var elevationModelEast = elevationModelFactory
						.createInterpolateElevationModel(elevationPyramidTilesEast.getDocumentElement());

				var compoundElevationModel = new CompoundElevationModel();
				compoundElevationModel.addElevationModel(elevationModelWest);
				compoundElevationModel.addElevationModel(elevationModelEast);
				elevationModel = compoundElevationModel;
			}
		} else {
			rgbLayer = layerFactory.createDtmLayer(new File(dtmPath), imagePyramidTiles, rasterInfo.getDataType());
			layers.add(rgbLayer);
		}

		// Isobath
		if (isElevation) {
			layers.add(layerFactory.createIsobathLayer(displayName, rasterInfo.getGeoBox(), demSupplier));
		}

		// Color Scale layer
		layers.add(layerFactory.createColorScaleLayer(rgbLayer));

		subMonitor.done();

		return new WWRasterLayerStore(rasterInfo, layers, elevationModel);
	}

	/**
	 * Compute min and max values for the specified file
	 */
	private void loadFully(RasterInfo rasterInfo, DemSupplier demSupplier) throws GIOException {
		if (!rasterInfo.isFullyLoaded()) {
			try (var gdalDataset = GdalDataset.open(demSupplier.get().getPath())) {
				rasterInfo.setNoDataValue(gdalDataset.getNoDataValue());
				var minMax = gdalDataset.computeMinMax();
				if (minMax.isPresent()) {
					rasterInfo.setMinMaxValues(minMax.get());
				} else {
					throw new GIOException("No valid value found in layer '" + rasterInfo.getDataType() + "'");
				}
			}
		}
	}

	/**
	 * @return true when tiles exist for the specified RasterInfo
	 * @throws GIOException
	 */
	protected boolean hasTilesFor(String dtmPath, RasterInfo rasterInfo) {
		boolean isElevation = RasterInfo.RASTER_ELEVATION.equals(rasterInfo.getDataType());
		String displayName = computeDisplayName(dtmPath, rasterInfo);
		File xmlImageFile = tilesInstaller.computeImageCacheDocument(displayName);
		File xmlElevationFile = tilesInstaller.computeElevationCacheDocument(displayName);
		return xmlImageFile.exists() && (!isElevation || xmlElevationFile.exists());
	}

	/** @return how to present the file name in Nasa */
	protected String computeDisplayName(String filePath, RasterInfo rasterInfo) {
		return WWIO.replaceIllegalFileNameCharacters(FilenameUtils.getBaseName(filePath) + '_'
				+ StringUtils.capitalize(rasterInfo.getDataType()).replace(' ', '_'));
	}

	/**
	 * Load or create the XML file describing a tiled texture pyramid
	 */
	protected Document createTextureTiles(String displayName, RasterInfo rasterInfo, File xmlImageFile,
			DemSupplier demSupplier, IProgressMonitor monitor) throws GIOException {
		// Load or create the XML docunent describing the tiles pyramid of the texture
		Document result = null;
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		try {
			if (!xmlImageFile.exists()) {
				File rgbFile = TemporaryCache.createTemporaryFile(displayName, ".tif");
				colorRelief(displayName, rasterInfo, demSupplier.get(), rgbFile, subMonitor.split(30));
				result = tilesInstaller.makeImageProducerBuilder(rgbFile).//
						setDatasetName(displayName).//
						setMinMax(rasterInfo.getMinMaxValues()).//
						setUnit(rasterInfo.getUnit()).//
						setGeoBox(rasterInfo.getGeoBox()).//
						run(subMonitor.split(70)).get(0);
				NasaCache.delete(rgbFile);
			} else {
				result = WWXML.openDocument(xmlImageFile);
				retrieveMinMaxValues(rasterInfo, result);
			}
		} catch (IOException e) {
			logger.warn("Can't create a temporary file {} ", e.getMessage());
		}
		subMonitor.done();

		return result;
	}

	/**
	 * Create a RGB dtm file with an elevation one.
	 *
	 * @param rasterFile dtm layer in tif file
	 * @param monitor monitor of progression.
	 * @return the created RGB dtm file. This file is stored in the TemporaryCache and must be deleted
	 * @throws GIOException creation failed, error occured
	 */
	protected void colorRelief(String displayName, RasterInfo rasterInfo, File rasterFile, File rgbFile,
			IProgressMonitor monitor) throws GIOException {
		monitor.subTask("Computing the texture");
		try {
			Dataset dataset = gdal.Open(rasterFile.getAbsolutePath());
			if (dataset != null) {
				GdalColorRelief colorRelief = new GdalColorRelief(dataset, rgbFile);
				colorRelief.setBand(rasterInfo.getGdalBand());
				colorRelief.setNearestColorEntry();
				colorRelief.setAlpha();
				colorRelief.fitColorsFor(rasterInfo.getMinMaxValues());
				colorRelief.runAndClose(monitor);
				dataset.delete();
			} else {
				throw new GIOException("Can't open file " + displayName);
			}
		} catch (IOException e) {
			throw GIOException.wrap("Can't generate an image with " + displayName, e);
		}
	}

	/**
	 * Load the XML file describing a tiled elevation pyramid
	 */
	protected Document createElevationTiles(String displayName, File xmlElevationFile, DemSupplier demSupplier,
			GeoBox geobox, IProgressMonitor monitor) throws GIOException {
		Document result = null;
		if (!xmlElevationFile.exists()) {
			result = tilesInstaller.makeElevationProducerBuilder(demSupplier.get()).//
					setDatasetName(displayName).//
					setGeoBox(geobox).//
					run(monitor).get(0);
		} else {
			result = WWXML.openDocument(xmlElevationFile);
		}
		monitor.done();

		return result;
	}

	/**
	 * When Document is loaded, try to find the Min and Max value inside.
	 */
	protected void retrieveMinMaxValues(RasterInfo rasterInfo, Document document) {
		if (rasterInfo.getMinMaxValues() == null) {
			Double min = null;
			NodeList nodeList = document.getElementsByTagName("ValMin");
			if (nodeList.getLength() > 0) {
				min = Double.valueOf(((Element) nodeList.item(0)).getChildNodes().item(0).getNodeValue());
			}

			Double max = null;
			nodeList = document.getElementsByTagName("ValMax");
			if (nodeList.getLength() > 0) {
				max = Double.valueOf(((Element) nodeList.item(0)).getChildNodes().item(0).getNodeValue());
			}

			if (min != null && max != null) {
				rasterInfo.setMinMaxValues(new DoubleRange(min, max));
			}
		}
	}

	/** Extract the file name from a subset name */
	public String computeFileName(String subsetName) {
		int start = subsetName.indexOf('"');
		int end = subsetName.lastIndexOf('"');
		return start > 0 && end > start ? subsetName.substring(start + 1, end) : subsetName;
	}

}