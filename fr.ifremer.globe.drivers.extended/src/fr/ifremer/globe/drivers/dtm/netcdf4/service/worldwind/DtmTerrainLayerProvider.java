/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.dtm.netcdf4.service.worldwind;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.DoubleRange;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fr.ifremer.globe.core.io.dtm.common.info.DtmInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.gdal.programs.GdalTranslate;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModelFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.WWLayerProviderStatus;
import fr.ifremer.globe.ui.service.worldwind.layer.WWRasterLayerStore;
import fr.ifremer.globe.ui.service.worldwind.tile.IWWTilesService;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWXML;

/**
 * Driver to manage DTM files.
 */
@Component(name = "globe_drivers_dtm_netcdf4_terrain_layer_provider", service = IWWLayerProvider.class, //
		property = { WWLayerProviderStatus.SERVICE_PROPERTY + "=" + WWLayerProviderStatus.EXPERIMENTAL })
public class DtmTerrainLayerProvider implements IWWLayerProvider {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(DtmTerrainLayerProvider.class);

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** Osgi Service used to tiled image layers */
	@Reference
	protected IWWTilesService tilesInstaller;

	/** Osgi Service used to produce WW ElevationModel */
	@Reference
	protected IWWElevationModelFactory elevationModelFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return Set.of(ContentType.DTM_NETCDF_3, ContentType.DTM_NETCDF_4);
	}

	/**
	 * Compute WW layers to render the Elevation Dtm layer
	 *
	 * @see fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider#getFileLayers(java.lang.String,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		WWFileLayerStore result = null;
		if (fileInfo instanceof DtmInfo dtmInfo && !dtmInfo.getRasterInfos().isEmpty()) {
			result = new WWFileLayerStore(dtmInfo);
			// At least, the elevation layer
			RasterInfo elevationRasterInfo = dtmInfo.getRasterInfo(RasterInfo.RASTER_ELEVATION).orElse(null);
			if (elevationRasterInfo != null)
				result.addLayers(createLayersForTerrain(fileInfo.getAbsolutePath(), elevationRasterInfo, monitor));

			// Reload other layers if their tiles exists
			for (RasterInfo rasterInfo : dtmInfo.getRasterInfos()) {
				if (rasterInfo != elevationRasterInfo && hasTilesFor(fileInfo.getAbsolutePath(), rasterInfo))
					result.addLayers(
							createLayersForTerrain(fileInfo.getAbsolutePath(), rasterInfo, new NullProgressMonitor()));
			}
		}
		return Optional.ofNullable(result);
	}

	/**
	 * @see fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider#getRasterLayers(fr.ifremer.globe.model.services.raster.RasterInfo,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public Optional<WWRasterLayerStore> getRasterLayers(RasterInfo rasterInfo, IProgressMonitor monitor)
			throws GIOException {
		WWRasterLayerStore result = rasterInfo.getContentType().isOneOf(ContentType.DTM_NETCDF_3,
				ContentType.DTM_NETCDF_4) //
						? createLayersForTerrain(computeFileName(rasterInfo.getGdalLoadingKey()), rasterInfo, monitor) //
						: null;

		return Optional.ofNullable(result);
	}

	/**
	 * Creates the worldwind layers to represent the terrain in the 3D viewer
	 *
	 * @param dtmPath the dtm file path
	 * @param rasterInfo the raster informations
	 * @param monitor monitor of progression
	 * @throws GIOException creation failed, error occured
	 */
	protected WWRasterLayerStore createLayersForTerrain(String dtmPath, RasterInfo rasterInfo, IProgressMonitor monitor)
			throws GIOException {
		boolean isElevation = RasterInfo.RASTER_ELEVATION.equals(rasterInfo.getDataType());
		String displayName = computeDisplayName(dtmPath, rasterInfo);

		// Ensure we have the minMax values
		monitor.subTask("Computing minimum and maximum values");
		List<IWWLayer> layers = new ArrayList<>();
		ElevationModel elevationModel = null;

		try (GdalDataset gdalDataset = loadFully(rasterInfo);
				// Find elevation dataset
				GdalDataset elevationDataset = isElevation ? gdalDataset
						: IFileService.grab().getFileInfo(dtmPath).map(DtmInfo.class::cast)
								.flatMap(dtmInfo -> dtmInfo.getRasterInfo(RasterInfo.RASTER_ELEVATION))
								.map(this::loadFully).orElse(gdalDataset)) {

			// Load or create the XML document describing the tiles pyramid of the elevation
			List<Document> tilesPyramids = createTerrainTiles(displayName, rasterInfo, gdalDataset, elevationDataset,
					monitor);
			if (isElevation) {
				elevationModel = elevationModelFactory.createInterpolateElevationModel(displayName,
						rasterInfo.getGeoBox(), tilesPyramids);
				layers.add(layerFactory.createIsobathLayer(displayName, rasterInfo.getGeoBox(),
						new DemSupplier(rasterInfo, new NullProgressMonitor())));
			}
			var dtmLayer = layerFactory.createTerrainLayer(tilesPyramids, rasterInfo.getDataType(), displayName);
			layers.add(dtmLayer);
			layers.add(layerFactory.createColorScaleLayer(dtmLayer));
		}
		monitor.done();

		return new WWRasterLayerStore(rasterInfo, layers, elevationModel);
	}

	/**
	 * Compute min and max values for the specified file
	 *
	 * @return the Dataset containing all the values
	 */
	private GdalDataset loadFully(RasterInfo rasterInfo) {
		var originalDataset = GdalDataset.open(rasterInfo.getGdalLoadingKey());
		GdalDataset result = originalDataset;
		if (originalDataset.isPresent()) {
			if (!rasterInfo.isFullyLoaded()) {
				// Specific case for old DTM. Must translate to apply offset and scale factor to values
				if (rasterInfo.getContentType() == ContentType.DTM_NETCDF_3) {
					GdalTranslate translate = new GdalTranslate(originalDataset.get());
					translate.setUnscale();
					translate.setOutputType(Float.class);
					translate.setProjection(originalDataset.getSpatialReference());
					result = translate.run(new NullProgressMonitor());
					result.deleteOnClose(true);
					originalDataset.close();
				}
				rasterInfo.setNoDataValue(result.getNoDataValue());
				result.computeStatistics(RasterInfo.HISTOGRAM_SIZE).ifPresent(statistics -> {
					rasterInfo.setMinMaxValues(new DoubleRange(statistics.min(), statistics.max()));
					rasterInfo.setMean(statistics.mean());
					rasterInfo.setStdev(statistics.stddev());
					rasterInfo.setHistogram(statistics.histogram());
				});
			}
			result.computeMinMax().ifPresent(rasterInfo::setMinMaxValues);
			// An old DTM is never considered as fully loaded to always force the translate
			rasterInfo.setFullyLoaded(rasterInfo.getContentType() != ContentType.DTM_NETCDF_3);
		}
		return result;
	}

	/**
	 * @return true when tiles exist for the specified RasterInfo
	 */
	protected boolean hasTilesFor(String dtmPath, RasterInfo rasterInfo) {
		String displayName = computeDisplayName(dtmPath, rasterInfo);
		List<File> xmlTerrainFiles = tilesInstaller.computeRasterCacheDocument(displayName, rasterInfo.getGeoBox());
		return xmlTerrainFiles.stream().allMatch(File::exists);
	}

	/** @return how to present the file name in Nasa */
	protected String computeDisplayName(String filePath, RasterInfo rasterInfo) {
		return WWIO.replaceIllegalFileNameCharacters(FilenameUtils.getBaseName(filePath) + '_'
				+ StringUtils.capitalize(rasterInfo.getDataType()).replace(' ', '_'));
	}

	/**
	 * Produces the tile pyramids for the DEM supplier. (2 pyramids if spanning 180 meridian)
	 */
	protected List<Document> createTerrainTiles(String displayName, RasterInfo rasterInfo, GdalDataset gdalDataset,
			GdalDataset elevationDataset, IProgressMonitor monitor) throws GIOException {

		var geoBox = rasterInfo.getGeoBox();
		List<Document> result = new LinkedList<>();
		var documentPaths = tilesInstaller.computeRasterCacheDocument(displayName, geoBox);
		if (documentPaths.stream().anyMatch(path -> !path.exists())) {
			if (gdalDataset.isPresent()) {
				result = tilesInstaller.makeTerrainProducerBuilder(gdalDataset, elevationDataset).//
						setDatasetName(displayName).//
						setGeoBox(rasterInfo.getGeoBox()).//
						setMinMax(rasterInfo.getMinMaxValues()).//
						run(monitor);
			}
		} else {
			result = documentPaths.stream().map(WWXML::openDocument).toList();
		}
		monitor.done();

		return result;
	}

	/**
	 * When Document is loaded, try to find the Min and Max value inside.
	 */
	protected void retrieveMinMaxValues(RasterInfo rasterInfo, Document document) {
		if (rasterInfo.getMinMaxValues() == null) {
			Double min = null;
			NodeList nodeList = document.getElementsByTagName("ValMin");
			if (nodeList.getLength() > 0) {
				min = Double.valueOf(((Element) nodeList.item(0)).getChildNodes().item(0).getNodeValue());
			}

			Double max = null;
			nodeList = document.getElementsByTagName("ValMax");
			if (nodeList.getLength() > 0) {
				max = Double.valueOf(((Element) nodeList.item(0)).getChildNodes().item(0).getNodeValue());
			}

			if (min != null && max != null) {
				rasterInfo.setMinMaxValues(new DoubleRange(min, max));
			}
		}
	}

	/** Extract the file name from a subset name */
	public String computeFileName(String subsetName) {
		int start = subsetName.indexOf('"');
		int end = subsetName.lastIndexOf('"');
		return start > 0 && end > start ? subsetName.substring(start + 1, end) : subsetName;
	}
}