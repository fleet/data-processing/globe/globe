/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.usbl.icon;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a mbg file.
 */
@Component(name = "globe_drivers_usbl_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class UsblIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public UsblIcon16ForFileProvider() {
		super("icons/16/usbl.png", ContentType.USBL);
	}
}
