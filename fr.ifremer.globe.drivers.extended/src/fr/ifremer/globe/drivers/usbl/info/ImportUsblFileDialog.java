package fr.ifremer.globe.drivers.usbl.info;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.ifremer.globe.core.io.usbl.UsblFileInfo;
import fr.ifremer.globe.core.io.usbl.UsblSettings;
import fr.ifremer.globe.ui.widget.usbl.UsblLayerParameterComposite;

/**
 * Dialog window to get {@link UsblSettings}.
 */
public class ImportUsblFileDialog extends Dialog {

	private final UsblFileInfo usblFileInfo;
	private UsblSettings usblLayerParameters;
	private static String survey = ""; // static to keep in memory
	private static String description = ""; // static to keep in memory

	public ImportUsblFileDialog(Shell parentShell, UsblFileInfo fileInfo) {
		super(parentShell);
		this.usblFileInfo = fileInfo;
		this.usblLayerParameters = usblFileInfo.getSettings();
	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("USBL target");
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// create OK and Cancel buttons
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(1, false));

		Group grpMetadata = new Group(composite, SWT.NONE);
		grpMetadata.setText("Metadata");
		grpMetadata.setLayout(new GridLayout(2, false));
		grpMetadata.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblSurvey = new Label(grpMetadata, SWT.NONE);
		lblSurvey.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblSurvey.setText("Survey :");

		var surveyText = new Text(grpMetadata, SWT.BORDER);
		surveyText.setText(survey);
		surveyText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		surveyText.addModifyListener((e) -> {
			survey = surveyText.getText();
			usblLayerParameters = usblLayerParameters.copyWith(survey, description);
		});

		Label lblDescription = new Label(grpMetadata, SWT.NONE);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDescription.setText("Description :");

		var descriptionText = new Text(grpMetadata, SWT.BORDER);
		descriptionText.setText(description);
		descriptionText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		descriptionText.addModifyListener((e) -> {
			description = descriptionText.getText();
			usblLayerParameters = usblLayerParameters.copyWith(survey, description);
		});

		new UsblLayerParameterComposite(parent, usblLayerParameters,
				newParameters -> this.usblLayerParameters = newParameters);

		return composite;
	}

	public UsblSettings getSettings() {
		return usblLayerParameters;
	}

}
