/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.usbl.info;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.FileInfoSupplierPriority;
import fr.ifremer.globe.core.io.usbl.UsblFileInfo;
import fr.ifremer.globe.core.io.usbl.UsblFileInfoSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.utils.Pair;

@Component(name = "globe_drivers_extended_usbl_file_info_supplier", service = IFileInfoSupplier.class, //
		property = { FileInfoSupplierPriority.PROPERTY_PRIORITY + ":Integer="
				+ FileInfoSupplierPriority.USBL_EXTENDED_PRIORITY }//
)
public class UsblInfoSupplierExtended implements IFileInfoSupplier<UsblFileInfo> {

	@Reference
	protected UsblFileInfoSupplier usblFileInfoSupplier;

	/** {@inheritDoc} */
	@Override
	public Optional<UsblFileInfo> getFileInfo(String filePath) {
		var optFileInfo = usblFileInfoSupplier.getFileInfo(filePath);

		if (optFileInfo.isPresent()) {
			var fileInfo = optFileInfo.get();

			// Display a dialog to get some parameters from user.
			AtomicInteger returnCode = new AtomicInteger(Window.CANCEL);
			Display.getDefault().syncExec(() -> {
				ImportUsblFileDialog dialog = new ImportUsblFileDialog(Display.getCurrent().getActiveShell(), fileInfo);
				returnCode.set(dialog.open());
				if (returnCode.get() == Window.OK) {
					fileInfo.setSettings(dialog.getSettings());
				}
			});

			// Throws an exception to stop the search for a suitable driver for this file. throw new
			if (returnCode.get() != Window.OK)
				throw new CancellationException();
		}
		return optFileInfo;
	}

	@Override
	public Set<ContentType> getContentTypes() {
		return usblFileInfoSupplier.getContentTypes();
	}

	@Override
	public List<String> getExtensions() {
		return Collections.emptyList();
	}

	@Override
	public List<Pair<String, String>> getFileFilters() {
		return Collections.emptyList();
	}

	@Override
	public Optional<UsblFileInfo> getFileInfoSilently(String filePath) {
		// Impossible to be silent, load from session is done by UsblFileInfoSupplier.
		return Optional.empty();
	}

}