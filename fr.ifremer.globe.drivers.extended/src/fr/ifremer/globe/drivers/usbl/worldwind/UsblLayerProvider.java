package fr.ifremer.globe.drivers.usbl.worldwind;

import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.usbl.UsblFileInfo;
import fr.ifremer.globe.core.io.usbl.UsblNavigation;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.WWNavigationLayerParameters;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class builds {@link IWWNavigationLayer} for USBL files.
 */
@Component(name = "globe_drivers_usbl_layer_provider", service = IWWLayerProvider.class)
public class UsblLayerProvider extends BasicWWLayerProvider<UsblFileInfo> {

	@Reference
	protected IWWLayerFactory layerFactory;

	@Reference
	private INavigationService navigationService;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return Set.of(ContentType.USBL);
	}

	@Override
	protected void createLayers(UsblFileInfo fileInfo, WWFileLayerStore layerStore, IProgressMonitor monitor)
			throws GIOException {
		// Navigation layer
		navigationService.getNavigationDataSupplier(fileInfo).ifPresent(navigationDataSupplier -> {
			var params = new WWNavigationLayerParameters() //
					.withLineWidth(1) //
					.withDisplayPoints(true) //
					.withDecimation(false, 100) //
					.withVariableColor(true, Optional.of(UsblNavigation.IS_BEFORE_LANDING));
			layerStore.addLayers(layerFactory.createNavigationLayer(navigationDataSupplier, params));
		});

		// Layer to display target and landing areas
		layerStore.addLayers(layerFactory.createUsblTargetLayer(fileInfo));

		monitor.done();
	}
}
