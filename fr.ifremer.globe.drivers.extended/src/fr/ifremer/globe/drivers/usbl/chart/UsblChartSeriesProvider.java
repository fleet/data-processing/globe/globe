package fr.ifremer.globe.drivers.usbl.chart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.ToDoubleFunction;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.io.usbl.UsblData;
import fr.ifremer.globe.core.io.usbl.UsblFileInfo;
import fr.ifremer.globe.core.model.chart.IChartData;
import fr.ifremer.globe.core.model.chart.IChartSeries;
import fr.ifremer.globe.core.model.chart.IChartSeriesStatusHandler;
import fr.ifremer.globe.core.model.chart.impl.BasicChartSeries;
import fr.ifremer.globe.core.model.chart.impl.ChartFileInfoDataSource;
import fr.ifremer.globe.core.model.chart.impl.SimpleChartData;
import fr.ifremer.globe.core.model.chart.services.IChartSeriesSupplier;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerService;
import fr.ifremer.globe.ui.service.worldwind.layer.usbl.IWWUsblLayer;

/**
 * This class provides method to get {@link IChartSeries} from {@link UsblFileInfo}.
 */
@Component(name = "globe_drivers_usbl_chart_series_supplier", service = { IChartSeriesSupplier.class })
public class UsblChartSeriesProvider implements IChartSeriesSupplier {

	@Override
	public Set<ContentType> getContentTypes() {
		return Set.of(ContentType.USBL);
	}

	/**
	 * @return a builder of {@link IChartSeries} for {@link UsblFileInfo}.
	 */
	@Override
	public List<UsblChartSeries> getChartSeries(IFileInfo fileInfo) {
		if (fileInfo instanceof UsblFileInfo usblFileInfo) {
			var source = new ChartFileInfoDataSource<UsblFileInfo>(usblFileInfo);
			return List.of(//
					new UsblChartSeries(source, "Depth", UsblData::getElevation),
					new UsblChartSeries(source, "Latitude", UsblData::getLatitude),
					new UsblChartSeries(source, "Longitude", UsblData::getLongitude));
		}
		return Collections.emptyList();
	}

	/**
	 * Implementation of {@link IChartSeries} for a USBL variable.
	 */
	public static class UsblChartSeries extends BasicChartSeries<UsblChartSeriesData> {

		private final List<UsblChartSeriesData> chartDataList = new ArrayList<>();

		/**
		 * Constructor
		 */
		public UsblChartSeries(ChartFileInfoDataSource<UsblFileInfo> source, String title,
				ToDoubleFunction<UsblData> valueProvider) {
			super(title, source);
			var beaconData = source.getFileInfo().getBeaconData();
			for (int i = 0; i < beaconData.size(); i++) {
				var data = beaconData.get(i);
				double value = valueProvider.applyAsDouble(data);
				if (!Double.isNaN(value)) {
					chartDataList.add(new UsblChartSeriesData(this, data, i, valueProvider));
				}
			}
			setStatusHandler(new UsblChartSeriesStatusHandler(source.getFileInfo()));
		}

		@Override
		public int size() {
			return chartDataList.size();
		}

		@Override
		public UsblChartSeriesData getData(int index) {
			return chartDataList.get(index);
		}

	}

	/**
	 * {@link IChartData} build from an {@link UsblData}.
	 */
	private static class UsblChartSeriesData extends SimpleChartData {

		private final UsblData usblData;

		public UsblChartSeriesData(UsblChartSeries series, UsblData usblData, int index,
				ToDoubleFunction<UsblData> valueProvider) {
			super(series, index, usblData.getDatetime().toEpochMilli(), valueProvider.applyAsDouble(usblData));
			this.usblData = usblData;
		}

		public UsblData getUsblData() {
			return usblData;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + Objects.hash(usblData);
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			UsblChartSeriesData other = (UsblChartSeriesData) obj;
			return Objects.equals(usblData, other.usblData);
		}

	}

	/**
	 * Implementation of {@link IChartSeriesStatusHandler} to handle validity edition.
	 */
	public static class UsblChartSeriesStatusHandler implements IChartSeriesStatusHandler<UsblChartSeriesData> {

		private final UsblFileInfo usblFileInfo;
		private boolean isDirty = false;

		protected UsblChartSeriesStatusHandler(UsblFileInfo usblFileInfo) {
			this.usblFileInfo = usblFileInfo;
		}

		@Override
		public int getStatus(UsblChartSeriesData data) {
			return data.getUsblData().isValid() ? 0 : 1;
		}

		@Override
		public void setStatus(UsblChartSeriesData data, int status) {
			if (getStatus(data) != status) {
				data.getUsblData().setValid(status == 0);
				isDirty = true;
			}
		}

		@Override
		public boolean isDirty() {
			return isDirty;
		}

		@Override
		public void flush() {
			isDirty = !usblFileInfo.flush();
			
			/// Reload USBL layer
			IWWLayerService.grab().getFileLayerStoreModel().get(usblFileInfo)
			.map(layerStore -> layerStore.getLayers(IWWUsblLayer.class))
			.ifPresent(layers -> layers.forEach(IWWUsblLayer::refresh));
		}

	}

}
