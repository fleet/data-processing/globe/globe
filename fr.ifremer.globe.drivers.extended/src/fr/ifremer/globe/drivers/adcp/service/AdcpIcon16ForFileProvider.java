/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.adcp.service;

import java.util.Optional;

import org.eclipse.swt.graphics.Image;
import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.io.adcp.AdcpInfo;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;
import fr.ifremer.globe.ui.utils.image.Icons;

/**
 * Offer the icon to represent an ADCP file.
 */
@Component(name = "globe_drivers_adcp_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class AdcpIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public AdcpIcon16ForFileProvider() {
		super("icons/16/current.png", ContentType.ADCP_STA);
	}

	/** {@inheritDoc} */
	@Override
	public Optional<Image> getIcon(IFileInfo fileInfo) {
		// Specific case : waits for the python driver to run
		if (fileInfo instanceof AdcpInfo adcpInfo && adcpInfo.getCurrentData().isEmpty())
			return Optional.of(Icons.SAND_TIMER.toImage());
		return getIcon();
	}
}
