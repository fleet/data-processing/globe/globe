package fr.ifremer.globe.drivers.adcp.service;

import java.util.Optional;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.adcp.AdcpInfo;
import fr.ifremer.globe.core.io.adcp.AdcpOceansiteData;
import fr.ifremer.globe.core.io.adcp.AdcpOceansiteInfo;
import fr.ifremer.globe.core.model.current.ICurrentData;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.IWWNavigationLayer.DisplayMode;
import fr.ifremer.globe.ui.service.worldwind.layer.navigation.WWNavigationLayerParameters;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * This class builds WW layers for ADCP files in Oceansite format.
 */
@Component(name = "globe_drivers_adcp_oceansite_layer_provider", service = IWWLayerProvider.class)
public class AdcpOceansiteLayerProvider implements IWWLayerProvider {

	/** OSGI Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	@Reference
	private INavigationService navigationService;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return Set.of(ContentType.ADCP_OCEANSITE, ContentType.ADCP_STA);
	}

	/**
	 * @return an {@link WWFileLayerStore} which contains a {@link IWWNavigationLayer} for files which can provide a
	 *         {@link INavigationDataSupplier}.
	 */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		WWFileLayerStore result = null;
		if (fileInfo instanceof AdcpOceansiteInfo adcpInfo) {
			AdcpOceansiteData data = adcpInfo.getAdcpOceansiteData(true);

			// Current layer
			var currentLayer = layerFactory.createCurrentLayer(data);

			// Navigation layer
			var params = new WWNavigationLayerParameters().withDisplayMode(DisplayMode.LINE);
			var navigationLayer = layerFactory.createNavigationLayer(b -> data, params);

			result = new WWFileLayerStore(adcpInfo, currentLayer, navigationLayer);
		} else if (fileInfo instanceof AdcpInfo adcpInfo) {
			result = new WWFileLayerStore(adcpInfo);
			// Current layer
			Optional<ICurrentData> currentData = adcpInfo.getCurrentData();
			if (currentData.isPresent()) {
				// Adding layer if current data are present
				var currentLayer = layerFactory.createCurrentLayer(currentData.get());
				result.addLayers(currentLayer);
			}
		}
		return Optional.ofNullable(result);
	}

}
