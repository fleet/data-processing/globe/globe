/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.undefined.icon;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent an undefinid file type.
 */
@Component(name = "globe_drivers_undefined_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class UndefinedIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public UndefinedIcon16ForFileProvider() {
		super("icons/16/loadingError.png", ContentType.UNDEFINED);
	}
}