/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.vel;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a mbg file.
 */
@Component(name = "globe_drivers_vel_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class VelIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public VelIcon16ForFileProvider() {
		super("icons/16/defaultFile.png", ContentType.VEL_NETCDF_4);
	}
}
