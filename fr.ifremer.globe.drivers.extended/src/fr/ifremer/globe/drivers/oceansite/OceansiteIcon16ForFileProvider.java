/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.oceansite;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent an Oceansite file.
 */
@Component(name = "globe_drivers_oceansite_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class OceansiteIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public OceansiteIcon16ForFileProvider() {
		super("icons/16/oceansite.png", ContentType.ADCP_OCEANSITE);
	}
}
