/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.gdal.service.worldwind;

import java.io.File;
import java.io.IOException;
import java.util.function.Supplier;

import org.apache.commons.io.FilenameUtils;
import org.gdal.gdal.Dataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.gdal.GdalOsrUtils;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.gdal.programs.GdalWarp;
import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * Supply a tif file for any raster file
 */
public class GdalRasterDemSupplier implements Supplier<File> {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(GdalRasterDemSupplier.class);

	/** Raster informations */
	protected RasterInfo rasterInfo;

	/**
	 * Constructor
	 */
	public GdalRasterDemSupplier(RasterInfo rasterInfo) {
		this.rasterInfo = rasterInfo;
	}

	/**
	 * @see java.util.function.Supplier#get()
	 */
	@Override
	public File get() {
		File result = null;
		if (rasterInfo.getProjection().getProjectionSettings().isLongLatProjection()) {
			result = new File(rasterInfo.getGdalLoadingKey());
		} else {
			try {
				result = TemporaryCache.createTemporaryFile(
						"DEM_PROJ_" + FilenameUtils.getBaseName(rasterInfo.getGdalLoadingKey()), ".tif");
				try (GdalDataset dataset = GdalDataset.open(rasterInfo.getGdalLoadingKey())) {
					if (dataset.isPresent()) {
						// Reprojection to lat / lon
						Dataset reprojectedDataset = new GdalWarp(dataset.get(), result.getPath())
								.addTargetProjection(GdalOsrUtils.SRS_WGS84).run();
						if (reprojectedDataset != null) {
							reprojectedDataset.delete();
						}
					}
				}
			} catch (IOException e) {
				logger.warn("Can't create the temporary file to reproject the tif file {}",
						rasterInfo.getGdalLoadingKey());
			}
		}
		return result;
	}

}
