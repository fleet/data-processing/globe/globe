/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.gdal.service.icon;

import org.osgi.service.component.annotations.Component;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.ui.service.icon.IIcon16ForFileProvider;
import fr.ifremer.globe.ui.service.icon.basic.BasicIcon16ForFileProvider;

/**
 * Offer the icon to represent a Gdal file.
 */
@Component(name = "globe_drivers_gdal_raster_icon_16_for_file_provider", service = IIcon16ForFileProvider.class)
public class GdalRasterIcon16ForFileProvider extends BasicIcon16ForFileProvider {

	/**
	 * Constructor
	 */
	public GdalRasterIcon16ForFileProvider() {
		super("icons/16/raster.png", ContentType.IMAGE_GDAL, ContentType.RASTER_GDAL, ContentType.GMT);
	}
}