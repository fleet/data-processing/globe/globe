/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.gdal.service.worldwind;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.gdal.gdal.Dataset;
import org.gdal.gdal.gdal;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.ifremer.globe.core.io.gdal.info.GdalFileInfo;
import fr.ifremer.globe.core.io.gdal.info.GdalInfoLoader;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.gdal.programs.GdalColorRelief;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModelFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.WWLayerProviderStatus;
import fr.ifremer.globe.ui.service.worldwind.layer.WWRasterLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.tile.IWWTilesService;
import fr.ifremer.globe.utils.FileUtils;
import fr.ifremer.globe.utils.cache.TemporaryCache;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWXML;

/**
 * Service providing Nasa layer for any Gdal Raster file
 */
@Component(name = "globe_drivers_gdal_raster_layer_provider", service = IWWLayerProvider.class, //
		property = { WWLayerProviderStatus.SERVICE_PROPERTY + "=" + WWLayerProviderStatus.DEPRECATED })
public class GdalRasterLayerProvider implements IWWLayerProvider {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(GdalRasterLayerProvider.class);

	/** Loader of Gdal file info */
	@Reference
	protected GdalInfoLoader gdalInfoLoader;

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** Osgi Service used to tiled image layers */
	@Reference
	protected IWWTilesService tilesInstaller;

	/** Osgi Service used to produce WW ElevationModel */
	@Reference
	protected IWWElevationModelFactory elevationModelFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return gdalInfoLoader.getContentTypes();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		WWFileLayerStore result = null;
		if (fileInfo instanceof GdalFileInfo) {
			result = createLayers((GdalFileInfo) fileInfo, silently, monitor);
		}
		return Optional.ofNullable(result);
	}

	/**
	 * @see fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider#getRasterLayers(fr.ifremer.globe.model.services.raster.RasterInfo,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public Optional<WWRasterLayerStore> getRasterLayers(RasterInfo rasterInfo, IProgressMonitor monitor)
			throws GIOException {
		WWRasterLayerStore result = rasterInfo.getSupplier() == gdalInfoLoader ? //
				createLayersForRaster(rasterInfo, monitor) : //
				null;
		return Optional.ofNullable(result);
	}

	/**
	 * Create the layers to represent the gdal raster file in the 3D Viewer
	 *
	 * @param gdalFileInfo the raster file
	 * @param monitor monitor of progression
	 */
	protected WWFileLayerStore createLayers(GdalFileInfo gdalFileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		WWFileLayerStore result = new WWFileLayerStore(gdalFileInfo);

		// RGB file ?
		if (!gdalFileInfo.hasRasters()) {
			result.addLayers(createLayersForImage(gdalFileInfo, monitor));
		} else {
			RasterInfo rasterInfo = gdalFileInfo.getRasterInfos().get(0);
			result.addLayers(createLayersForRaster(rasterInfo, monitor));
		}
		monitor.done();

		return result;
	}

	/**
	 * Complete the layers parameter with some layers to represent the rgb file in the 3D viewer
	 *
	 * @param gdalFileInfo the raster file
	 * @param monitor monitor of progression
	 * @throws GIOException creation failed, error occured
	 */
	protected IWWLayer createLayersForImage(GdalFileInfo gdalFileInfo, IProgressMonitor monitor) throws GIOException {
		Document pyramidTiles = null;
		String displayName = computeDisplayName(gdalFileInfo.getPath(), "");
		File xmlConfigurationFile = tilesInstaller.computeImageCacheDocument(displayName);
		if (!xmlConfigurationFile.exists()) {
			pyramidTiles = tilesInstaller.makeImageProducerBuilder(new File(gdalFileInfo.getPath())).//
					setDatasetName(displayName).//
					run(monitor).get(0);
		} else {
			pyramidTiles = WWXML.openDocument(xmlConfigurationFile);
		}

		IWWLayer layer = layerFactory.createTiledImageLayer(pyramidTiles);
		layer.setEnabled(true);

		return layer;
	}

	/**
	 * Creates the worldwind layers to represent the raster in the 3D viewer
	 *
	 * @param rasterInfo the raster informations
	 * @param monitor monitor of progression
	 * @throws GIOException creation failed, error occured
	 */
	protected WWRasterLayerStore createLayersForRaster(RasterInfo rasterInfo, IProgressMonitor monitor)
			throws GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);

		List<IWWLayer> layers = new ArrayList<>(3);
		boolean isElevation = RasterInfo.RASTER_ELEVATION.equals(rasterInfo.getDataType());
		String displayName = computeDisplayName(rasterInfo.getGdalLoadingKey(), rasterInfo.getDataType());
		File xmlImageFile = tilesInstaller.computeImageCacheDocument(displayName);
		File xmlElevationFile = tilesInstaller.computeElevationCacheDocument(displayName);

		// Load or create the XML docunent describing the tiles pyramid of the texture
		Document imagePyramidTiles = createTextureTiles(displayName, xmlImageFile, rasterInfo,
				subMonitor.split(isElevation ? 50 : 90));

		// Load or create the XML docunent describing the tiles pyramid of the elevation
		IWWTerrainLayer rgbLayer = null;
		ElevationModel elevationModel = null;
		File demFile = new File(rasterInfo.getGdalLoadingKey());
		if (isElevation) {
			Document elevationPyramidTiles = createElevationTiles(displayName, xmlElevationFile, demFile,
					subMonitor.split(40));
			rgbLayer = layerFactory.createElevationLayer(demFile, imagePyramidTiles, rasterInfo.getDataType());
			layers.add(rgbLayer);
			elevationModel = elevationModelFactory
					.createInterpolateElevationModel(elevationPyramidTiles.getDocumentElement());
		} else {
			rgbLayer = layerFactory.createDtmLayer(demFile, imagePyramidTiles, rasterInfo.getDataType());
			layers.add(rgbLayer);
		}

		// Isobath
		if (isElevation) {
			layers.add(layerFactory.createIsobathLayer(displayName, rasterInfo.getGeoBox(),
					new GdalRasterDemSupplier(rasterInfo)));
		}

		// Color Scale layer
		layers.add(layerFactory.createColorScaleLayer(rgbLayer));

		subMonitor.done();
		return new WWRasterLayerStore(rasterInfo, layers, elevationModel);
	}

	/**
	 * Load or create the XML file describing a tiled texture pyramid
	 */
	protected Document createTextureTiles(String displayName, File xmlImageFile, RasterInfo rasterInfo,
			IProgressMonitor monitor) throws GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		Document result = null;
		if (!xmlImageFile.exists()) {
			File rgbFile = colorRelief(rasterInfo, subMonitor.split(40));
			result = tilesInstaller.makeImageProducerBuilder(rgbFile).//
					setDatasetName(displayName).//
					setMinMax(rasterInfo.getMinMaxValues()).//
					setUnit(rasterInfo.getUnit()).//
					run(subMonitor.split(60)).get(0);
			FileUtils.delete(rgbFile);
		} else {
			result = WWXML.openDocument(xmlImageFile);
			subMonitor.done();
		}
		return result;
	}

	/**
	 * Create a RGB tif file from a raster.
	 *
	 * @param rasterInfo considered raster
	 * @param monitor monitor of progression.
	 * @return the created RGB tif file. This file is stored in the TemporaryCache and must be deleted
	 * @throws GIOException creation failed, error occured
	 */
	protected File colorRelief(RasterInfo rasterInfo, IProgressMonitor monitor) throws GIOException {
		File result = null;
		monitor.subTask("Computing the texture");
		String displayName = computeDisplayName(rasterInfo.getGdalLoadingKey(), rasterInfo.getDataType());
		try {
			Dataset dataset = gdal.Open(rasterInfo.getGdalLoadingKey());
			if (dataset != null) {
				result = TemporaryCache.createTemporaryFile(displayName, ".tif");
				GdalColorRelief colorRelief = new GdalColorRelief(dataset, result);
				colorRelief.setBand(rasterInfo.getGdalBand());
				colorRelief.setNearestColorEntry();
				colorRelief.setAlpha();
				colorRelief.fitColorsFor(rasterInfo.getMinMaxValues());
				colorRelief.runAndClose(monitor);
				dataset.delete();
			} else {
				throw new GIOException("Can't open file " + displayName);
			}
			return result;
		} catch (IOException e) {
			throw GIOException.wrap("Can't generate an image with " + displayName, e);
		}
	}

	/**
	 * Load the XML file describing a tiled elevation pyramid
	 */
	protected Document createElevationTiles(String displayName, File xmlElevationFile, File demFile,
			IProgressMonitor monitor) throws GIOException {
		Document result = null;
		if (!xmlElevationFile.exists()) {
			result = tilesInstaller.makeElevationProducerBuilder(demFile).//
					setDatasetName(displayName).//
					run(monitor).get(0);
		} else {
			result = WWXML.openDocument(xmlElevationFile);
		}
		monitor.done();

		return result;
	}

	/** @return how to present the file name in Nasa */
	protected String computeDisplayName(String filePath, String description) {
		String result = FilenameUtils.getBaseName(filePath) + (description.isEmpty() ? "" : "_" + description);
		return WWIO.replaceIllegalFileNameCharacters(result);
	}

}