/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.gdal.service.worldwind;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import fr.ifremer.globe.core.io.gdal.info.GdalFileInfo;
import fr.ifremer.globe.core.io.gdal.info.GdalInfoLoader;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.IFileInfo;
import fr.ifremer.globe.core.model.raster.IRasterService;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.utils.color.AColorPalette;
import fr.ifremer.globe.gdal.dataset.GdalDataset;
import fr.ifremer.globe.ui.service.worldwind.elevation.IWWElevationModelFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.WWLayerProviderStatus;
import fr.ifremer.globe.ui.service.worldwind.layer.WWRasterLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainLayer;
import fr.ifremer.globe.ui.service.worldwind.layer.terrain.IWWTerrainWithPaletteLayer;
import fr.ifremer.globe.ui.service.worldwind.tile.IWWTilesService;
import fr.ifremer.globe.utils.exception.GIOException;
import gov.nasa.worldwind.globes.ElevationModel;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWXML;

/**
 * Service providing Nasa layer for any Gdal Raster file
 */
@Component(name = "globe_drivers_gdal_terrain_layer_provider", service = IWWLayerProvider.class, //
		property = { WWLayerProviderStatus.SERVICE_PROPERTY + "=" + WWLayerProviderStatus.EXPERIMENTAL })
public class GdalTerrainLayerProvider implements IWWLayerProvider {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(GdalRasterLayerProvider.class);

	/** Loader of Gdal file info */
	@Reference
	protected GdalInfoLoader gdalInfoLoader;

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** Osgi Service used to tiled image layers */
	@Reference
	protected IWWTilesService tilesInstaller;

	/** Osgi Service used to produce WW ElevationModel */
	@Reference
	protected IWWElevationModelFactory elevationModelFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return gdalInfoLoader.getContentTypes();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<WWFileLayerStore> getFileLayers(IFileInfo fileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		WWFileLayerStore result = null;
		if (fileInfo instanceof GdalFileInfo) {
			result = createLayers((GdalFileInfo) fileInfo, silently, monitor);
		}
		return Optional.ofNullable(result);
	}

	/**
	 * @see fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider#getRasterLayers(fr.ifremer.globe.model.services.raster.RasterInfo,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public Optional<WWRasterLayerStore> getRasterLayers(RasterInfo rasterInfo, IProgressMonitor monitor)
			throws GIOException {
		WWRasterLayerStore result = rasterInfo.getSupplier() == gdalInfoLoader ? //
				createLayersForRaster(rasterInfo, monitor) : //
				null;
		return Optional.ofNullable(result);
	}

	/**
	 * Create the layers to represent the gdal raster file in the 3D Viewer
	 *
	 * @param gdalFileInfo the raster file
	 * @param monitor monitor of progression
	 */
	protected WWFileLayerStore createLayers(GdalFileInfo gdalFileInfo, boolean silently, IProgressMonitor monitor)
			throws GIOException {
		WWFileLayerStore result = new WWFileLayerStore(gdalFileInfo);

		// RGB file ?
		if (!gdalFileInfo.hasRasters()) {
			result.addLayers(createLayersForImage(gdalFileInfo, monitor));
		} else {
			RasterInfo rasterInfo = gdalFileInfo.getRasterInfos().get(0);
			result.addLayers(createLayersForRaster(rasterInfo, monitor));
		}
		monitor.done();

		return result;
	}

	/**
	 * Complete the layers parameter with some layers to represent the rgb file in the 3D viewer
	 *
	 * @param gdalFileInfo the raster file
	 * @param monitor monitor of progression
	 * @throws GIOException creation failed, error occured
	 */
	protected IWWLayer createLayersForImage(GdalFileInfo gdalFileInfo, IProgressMonitor monitor) throws GIOException {
		Document pyramidTiles = null;
		String displayName = computeDisplayName(gdalFileInfo.getPath(), "");
		File xmlConfigurationFile = tilesInstaller.computeImageCacheDocument(displayName);
		if (!xmlConfigurationFile.exists()) {
			pyramidTiles = tilesInstaller.makeImageProducerBuilder(new File(gdalFileInfo.getPath())).//
					setDatasetName(displayName).//
					run(monitor).get(0);
		} else {
			pyramidTiles = WWXML.openDocument(xmlConfigurationFile);
		}

		IWWLayer layer = layerFactory.createTiledImageLayer(pyramidTiles);
		layer.setEnabled(true);

		return layer;
	}

	/**
	 * Creates the worldwind layers to represent the raster in the 3D viewer
	 *
	 * @param rasterInfo the raster informations
	 * @param monitor monitor of progression
	 * @throws GIOException creation failed, error occured
	 */
	protected WWRasterLayerStore createLayersForRaster(RasterInfo rasterInfo, IProgressMonitor monitor)
			throws GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);

		boolean isElevation = RasterInfo.RASTER_ELEVATION.equals(rasterInfo.getDataType());
		String displayName = computeDisplayName(rasterInfo.getGdalLoadingKey(), rasterInfo.getDataType());

		List<IWWLayer> layers = new ArrayList<>();
		ElevationModel elevationModel = null;
		RasterInfo elevationRasterInfo = IRasterService.grab().getElevationRaster(rasterInfo).orElse(rasterInfo);

		// Load or create the XML document describing the tiles pyramid of the elevation
		List<Document> tilesPyramids = createTerrainTiles(displayName, rasterInfo, elevationRasterInfo,
				subMonitor.split(90));

		// With palette ?
		AColorPalette palette = rasterInfo.getPalette().orElse(null);
		if (palette != null) {
			IWWTerrainWithPaletteLayer terrainLayer = layerFactory.createTerrainWithPaletteLayer(tilesPyramids,
					displayName, palette);
			layers.add(terrainLayer);
		} else {
			if (isElevation) {
				elevationModel = elevationModelFactory.createInterpolateElevationModel(displayName,
						rasterInfo.getGeoBox(), tilesPyramids);
				layers.add(layerFactory.createIsobathLayer(displayName, rasterInfo.getGeoBox(),
						new GdalRasterDemSupplier(rasterInfo)));
			}
			IWWTerrainLayer terrainLayer = layerFactory.createTerrainLayer(tilesPyramids, rasterInfo.getDataType(),
					displayName);
			layers.add(terrainLayer);
			layers.add(layerFactory.createColorScaleLayer(terrainLayer));
		}
		subMonitor.done();

		return new WWRasterLayerStore(rasterInfo, layers, elevationModel);
	}

	/**
	 * Load the XML files describing a tiled elevation pyramid
	 */
	protected List<Document> createTerrainTiles(String displayName, RasterInfo rasterInfo, IProgressMonitor monitor)
			throws GIOException {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		var geoBox = rasterInfo.getGeoBox();
		List<Document> result = new LinkedList<>();
		var documentPaths = tilesInstaller.computeRasterCacheDocument(displayName, geoBox);
		if (documentPaths.stream().anyMatch(path -> !path.exists())) {
			try (var gdalDataset = GdalDataset.open(rasterInfo.getGdalLoadingKey())) {
				result = tilesInstaller.makeTerrainProducerBuilder(gdalDataset).//
						setDatasetName(displayName).//
						setGeoBox(rasterInfo.getGeoBox()).//
						setMinMax(rasterInfo.getMinMaxValues()).//
						run(subMonitor.split(100));
			}
		} else {
			result = documentPaths.stream().map(WWXML::openDocument).collect(Collectors.toList());
		}
		subMonitor.done();
		return result;
	}

	/**
	 * Load the XML files describing a tiled elevation pyramid
	 */
	protected List<Document> createTerrainTiles(String displayName, RasterInfo rasterInfo, RasterInfo elevationInfo,
			IProgressMonitor monitor) throws GIOException {
		if (rasterInfo == elevationInfo) {
			return createTerrainTiles(displayName, rasterInfo, monitor);
		} else {
			SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
			var geoBox = rasterInfo.getGeoBox();
			List<Document> result = new LinkedList<>();
			var documentPaths = tilesInstaller.computeRasterCacheDocument(displayName, geoBox);
			if (documentPaths.stream().anyMatch(path -> !path.exists())) {
				try (var gdalDataset = GdalDataset.open(rasterInfo.getGdalLoadingKey());
						var elevationDataset = GdalDataset.open(elevationInfo.getGdalLoadingKey())) {
					result = tilesInstaller.makeTerrainProducerBuilder(gdalDataset, elevationDataset).//
							setDatasetName(displayName).//
							setGeoBox(rasterInfo.getGeoBox()).//
							setMinMax(rasterInfo.getMinMaxValues()).//
							run(subMonitor.split(100));
				}
			} else {
				result = documentPaths.stream().map(WWXML::openDocument).collect(Collectors.toList());
			}
			subMonitor.done();
			return result;
		}
	}

	/** @return how to present the file name in Nasa */
	protected String computeDisplayName(String filePath, String description) {
		String result = FilenameUtils.getBaseName(filePath) + (description.isEmpty() ? "" : "_" + description);
		return WWIO.replaceIllegalFileNameCharacters(result);
	}

}