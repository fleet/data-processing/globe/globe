/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.gdal.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import fr.ifremer.globe.core.io.FileInfoSupplierPriority;
import fr.ifremer.globe.core.io.gdal.info.GdalFileInfo;
import fr.ifremer.globe.core.io.gdal.info.GdalInfoLoader;
import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.FileInfoSettings;
import fr.ifremer.globe.core.model.file.IFileInfoSupplier;
import fr.ifremer.globe.core.model.raster.RasterInfo;
import fr.ifremer.globe.core.model.session.ISessionService;
import fr.ifremer.globe.core.utils.Pair;
import fr.ifremer.globe.ui.utils.Messages;

/**
 * Loader of Gdal file info. For raster files, ask the user if they contain elevations
 */
@Component(//
		name = "globe_drivers_gdal_file_info_supplier", //
		service = { GdalInfoSupplierExtended.class, IFileInfoSupplier.class }, //
		property = { FileInfoSupplierPriority.PROPERTY_PRIORITY + ":Integer="
				+ FileInfoSupplierPriority.EXTENDED_GDAL_RASTER_PRIORITY }//
)
public class GdalInfoSupplierExtended implements IFileInfoSupplier<GdalFileInfo> {

	/** Loader of Gdal file info */
	@Reference
	protected GdalInfoLoader gdalInfoLoader;
	/** Session service */
	@Reference
	ISessionService sessionService;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentTypes() {
		return gdalInfoLoader.getContentTypes();
	}

	/** {@inheritDoc} */
	@Override
	public List<String> getExtensions() {
		return gdalInfoLoader.getExtensions();
	}

	/** {@inheritDoc} */
	@Override
	public List<Pair<String, String>> getFileFilters() {
		return gdalInfoLoader.getFileFilters();
	}

	/** {@inheritDoc} */
	@Override
	public Optional<GdalFileInfo> getFileInfo(String filePath) {
		Optional<GdalFileInfo> result = gdalInfoLoader.makeFileInfo(filePath);
		if (result.isPresent()) {
			GdalFileInfo fileInfo = result.get();
			// Raster file, not rgb
			if (fileInfo.hasRasters()) {
				var firstRaster = fileInfo.getRasterInfos().get(0);
				if (!RasterInfo.RASTER_BITFIELD.equals(firstRaster.getDataType())) {
					boolean hasElevation = //
							filePath.endsWith("DEPTH.tif") // Specific for tif named ...DEPTH.tif : must have elevations
									|| // or ask to the user
									Messages.openSyncQuestionMessage("Elevation information",
											"Does " + FilenameUtils.getName(filePath) + " contain elevation ?");

					String dataType = hasElevation ? RasterInfo.RASTER_ELEVATION : RasterInfo.RASTER_RAW;
					fileInfo.getRasterInfos().forEach(rasterInfo -> rasterInfo.setDataType(dataType));
					// Stores information that the file has elevation (or not) in the session
					var fileInfoSettings = new FileInfoSettings();
					fileInfoSettings.put("DataType", dataType);
					sessionService.getPropertiesContainer().add(filePath,
							FileInfoSettings.REALM_PROPERTIES_FILE_SETTINGS, fileInfoSettings);
				}
			}

		}

		return result;
	}

	/** {@inheritDoc} */
	@Override
	public Optional<GdalFileInfo> getFileInfoSilently(String filePath) {
		return gdalInfoLoader.makeFileInfo(filePath);
	}

}
