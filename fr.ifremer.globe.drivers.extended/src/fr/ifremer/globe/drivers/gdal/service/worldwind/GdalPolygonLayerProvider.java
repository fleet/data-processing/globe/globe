/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.drivers.gdal.service.worldwind;

import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.ContentType;
import fr.ifremer.globe.core.model.file.polygon.PolygonFileInfo;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerFactory;
import fr.ifremer.globe.ui.service.worldwind.layer.IWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.WWFileLayerStore;
import fr.ifremer.globe.ui.service.worldwind.layer.basic.BasicWWLayerProvider;
import fr.ifremer.globe.ui.service.worldwind.layer.polygon.IWWPolygonLayer;
import fr.ifremer.globe.utils.exception.GIOException;

/**
 * Driver to manage SHP files.
 */
@Component(name = "globe_drivers_gdal_polygon_layer_provider", service = IWWLayerProvider.class)
public class GdalPolygonLayerProvider extends BasicWWLayerProvider<PolygonFileInfo> {

	/** Logger */
	protected static Logger logger = LoggerFactory.getLogger(GdalPolygonLayerProvider.class);

	/** Osgi Service used to create WW layers */
	@Reference
	protected IWWLayerFactory layerFactory;

	/** {@inheritDoc} */
	@Override
	public Set<ContentType> getContentType() {
		return Set.of(ContentType.POLYGON_GDAL);
	}

	/**
	 * Create the layers to represent the shp file in the 3D Viewer
	 */
	@Override
	protected void createLayers(PolygonFileInfo polygonInfo, WWFileLayerStore layerStore, IProgressMonitor monitor)
			throws GIOException {
		IWWPolygonLayer layer = layerFactory.createPolygonLayer(polygonInfo);
		layerStore.addLayers(layer);
		monitor.done();
	}
}