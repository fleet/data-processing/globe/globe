package fr.ifremer.globe.editor.navshift.controller;

import static org.junit.Assert.assertNotNull;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.junit.Test;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.tests.NavShifterBaseTest;

public class SessionControllerTest extends NavShifterBaseTest {

	@Inject
	@Named(ContextNames.LONGLAT_PROJECTION)
	Projection longlatProjection;

	@Test
	public void createControllerTest() {
		assertNotNull(sessionController);
	}

	@Test
	public void testApplicationStarted() {
		assertNotNull("Injection trouble. ContextInitializer failed ?", sessionController);
		assertNotNull("Injection trouble. ContextInitializer failed ?", longlatProjection);
	}
}
