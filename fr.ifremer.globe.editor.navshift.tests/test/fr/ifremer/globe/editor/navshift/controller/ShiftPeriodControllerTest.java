package fr.ifremer.globe.editor.navshift.controller;

import static org.junit.Assert.assertNotNull;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.junit.Test;

import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.tests.NavShifterBaseTest;

public class ShiftPeriodControllerTest extends NavShifterBaseTest {

	@Inject
	@Named(ContextNames.SHIFTPERIOD_CONTROLLER)
	protected ShiftPeriodController controller;

	@Test
	public void createControllerTest() {
		assertNotNull(sessionController);
		assertNotNull(controller);
	}

}
