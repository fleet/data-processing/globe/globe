/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collections;

import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.navigation.INavigationData;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.impl.NavigationModel;
import fr.ifremer.globe.editor.navshift.model.impl.SessionContextImpl;
import fr.ifremer.globe.editor.navshift.tests.NavShifterBaseTest;
import fr.ifremer.globe.test.utils.BenchUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Tests of NavigationModelController class
 */
public class NavigationModelControllerTests extends NavShifterBaseTest {

	/** Fichiers à tester. */
	public static final String MBG_FILE1 = GlobeTestUtil.getTestDataPath()
			+ "/file/FusMBG/In/FiltTri_cel_tide_0033_20160325_172722_Thalia.mbg";

	/**
	 * Test method for {@link
	 * fr.ifremer.globe.editor.navshift.controller.NavigationModelController#loadNavigationModelForProfile(NavigationModel,
	 * fr.ifremer.globe.editor.navshift.model.IProfile, INavigationData, org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Test
	public void testMakeNavigationModel() {
		try {
			BenchUtils.timedPerformanceTest(NavigationModelControllerTests.class
					.getMethod("makeNavigationModelListOfISounderNcInfoIProgressMonitor"), this);
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			Assert.fail("Test failed with exception: " + e.getMessage());
		}
	}

	public void makeNavigationModelListOfISounderNcInfoIProgressMonitor() {
		logger.warn(getClass().getSimpleName() + ".makeNavigationModelListOfISounderNcInfoIProgressMonitor");

		ISounderNcInfo sounderNcInfo = (ISounderNcInfo) IFileService.grab().getFileInfo(MBG_FILE1).orElse(null);
		assertNotNull(sounderNcInfo);

		SessionContextImpl sessionContext = new SessionContextImpl();
		sessionContext.setSounderNcInfos(Collections.singletonList(sounderNcInfo));
		sessionContext.setGeoBox(new GeoBox());
		testContext.set(ContextNames.SESSION_CONTEXT, sessionContext);

		INavigationData navigationDataProxy = null;
		try {
			navigationDataProxy = sounderNcInfo.getNavigationData(true);
		} catch (GIOException e) {
			e.printStackTrace();
			fail("Unable to create navigation Data Proxy");
		} finally {
			IOUtils.closeQuietly(navigationDataProxy);
		}
		int navSize = navigationDataProxy.size();

		INavigationModel navigationModel;
		try {
			sessionContext.setProfiles(profileController.load(new NullProgressMonitor()));
			sessionContext.setNavigationDataSupplier(navigationModelController.load(new NullProgressMonitor()));
			navigationModel = sessionContext.getNavigationDataSupplier();
			assertEquals("Wong number of elements", navSize, navigationModel.size());
			long previousTime = 0l;
			for (int index = 0; index < navSize; index++) {
				assertTrue("Not in chronological order", previousTime < navigationModel.getTime(index));
				previousTime = navigationModel.getTime(index);
				assertTrue("Latitude seems not to be set", navigationModel.getLatitude(index) != 0d);
				assertTrue("Longitude seems not to be set", navigationModel.getLongitude(index) != 0d);
				assertTrue("Projection seems not to be perfomed", navigationModel.getX(index) != 0d);
				assertTrue("Projection seems not to be perfomed", navigationModel.getY(index) != 0d);
			}
		} catch (GIOException e) {
			e.printStackTrace();
			fail("Unable to create navigation model");
		} finally {
			navigationModelController.onContextReset();
		}

	}

}
