/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.application;

import static org.junit.Assert.assertNull;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.junit.Test;

import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;

/**
 * Tests of ContextInitializer class
 */
public class ContextInitializerTests {

	@Inject
	@Named(ContextNames.LONGLAT_PROJECTION)
	Projection longlatProjection;

	@Inject
	@Named(ContextNames.MERCATOR_PROJECTION)
	Projection mercatorProjection;

	/**
	 * Test method for
	 * {@link fr.ifremer.globe.editor.navshift.application.context.ContextInitializer#applicationStarted(org.osgi.service.event.Event)}.
	 */
	@Test
	public void testApplicationStarted() {
		assertNull("Injection trouble. ContextInitializer failed ?", longlatProjection);
		assertNull("Injection trouble. ContextInitializer failed ?", mercatorProjection);
	}

}
