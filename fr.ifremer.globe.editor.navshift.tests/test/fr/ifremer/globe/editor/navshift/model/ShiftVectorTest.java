package fr.ifremer.globe.editor.navshift.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Before;
import org.junit.Test;

import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.controller.ShiftPeriodController;
import fr.ifremer.globe.editor.navshift.tests.NavShifterBaseTest;
import fr.ifremer.globe.utils.exception.GIOException;

public class ShiftVectorTest extends NavShifterBaseTest {

	private static final String TEST_FILE_PATH = TEST_DIR + File.separator + "0002_20090513_143735_ShipName_CIB.mbg";

	private static final String SAVE_FILE_PATH = TEST_DIR + File.separator + "navShifter" + File.separator
			+ "serializationTest.xml";

	@Inject
	@Named(ContextNames.SHIFTPERIOD_CONTROLLER)
	protected ShiftPeriodController controller;

	@Override
	@Before
	public void setUp() {
		super.setUp();
		List<File> files = new ArrayList<>();
		files.add(new File(TEST_FILE_PATH));

		ISessionContext sessionContext = activateSession(files);

		assertNotNull(sessionController.getSessionContext().getGeoBox());

		try {
			sessionContext.setNavigationDataSupplier(navigationModelController.load(new NullProgressMonitor()));
		} catch (GIOException e) {
			e.printStackTrace();
			fail("unable to load data files content");
		}
		INavigationModel navDataProvider = sessionController.getSessionContext().getNavigationDataSupplier();
		assertNotNull(navDataProvider);

	}

	@Test
	public void driftVectorTest() {
		logger.warn(getClass().getSimpleName() + ".driftVectorTest");

		try {
			IShiftPeriod period = controller.createShiftPeriod();
			assertNotNull(period);
			IShiftVector vect = period.createAndAddShiftVector(18, 0d, 0d);
			assertNotNull(vect);
			assertTrue(vect.getOriginX() < -3); // Longitude
		} catch (Exception e) {
			e.printStackTrace();
			fail("Error when manipulating a vector: " + e.getMessage());
		}
	}

	@Test
	public void writeStateWithVectorsTest() {
		logger.warn(getClass().getSimpleName() + ".writeStateWithVectorsTest");

		List<String> filePaths = new ArrayList<>();
		filePaths.add(TEST_FILE_PATH);
		filePaths.add(MBG_FILE1);
		filePaths.add(MBG_FILE2);
		filePaths.add(MBG_FILE3);
		INavigationModel navDataProvider = sessionController.getSessionContext().getNavigationDataSupplier();

		assertNotNull(navDataProvider);

		IShiftPeriod period = controller.createShiftPeriod();
		assertNotNull(period);
		period.setStartIndex(0);
		period.setEndIndex(navDataProvider.size() - 1);
		assertNotNull(sessionController.getSessionContext());
		sessionController.getSessionContext().setShiftPeriod(period);

		createSomeTestVectors(period, 8);

		File fic = new File(SAVE_FILE_PATH);
		if (fic.exists()) {
			fic.delete();
		}
		assertFalse(fic.exists());

		sessionController.saveContextFile(SAVE_FILE_PATH);

		fic = new File(SAVE_FILE_PATH);
		assertTrue(fic.exists());
	}

	void createSomeTestVectors(IShiftPeriod period, int nb) {
		logger.warn(getClass().getSimpleName() + ".createSomeTestVectors");

		INavigationModel data = period.getNavigationDataSupplier();
		int sz = data.size();
		Random rd = new Random();
		IntStream.range(1, nb + 1).forEach(nbr -> {
			int idx = rd.nextInt(sz);
			double x = data.getX(idx) + rd.nextDouble();
			double y = data.getY(idx) + rd.nextDouble();
			IShiftVector vect = period.createAndAddShiftVector(idx, x, y);
			assertNotNull(vect);
		});
	}

}
