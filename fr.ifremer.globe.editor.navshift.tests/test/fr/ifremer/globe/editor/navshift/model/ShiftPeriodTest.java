package fr.ifremer.globe.editor.navshift.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Before;
import org.junit.Test;

import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.controller.ShiftPeriodController;
import fr.ifremer.globe.editor.navshift.tests.NavShifterBaseTest;
import fr.ifremer.globe.utils.exception.GIOException;

public class ShiftPeriodTest extends NavShifterBaseTest {

	private static final String TEST_FILE_PATH = TEST_DIR + File.separator + "0002_20090513_143735_ShipName_CIB.mbg";

	private static final String SAVE_FILE_PATH = TEST_DIR + File.separator + "navShifter" + File.separator
			+ "serializationTest.xml";

	@Inject
	@Named(ContextNames.SHIFTPERIOD_CONTROLLER)
	protected ShiftPeriodController controller;

	@Override
	@Before
	public void setUp() {
		super.setUp();
		List<File> files = new ArrayList<>();
		files.add(new File(TEST_FILE_PATH));

		ISessionContext sessionContext = activateSession(files);

		try {
			sessionContext.setNavigationDataSupplier(navigationModelController.load(new NullProgressMonitor()));
		} catch (GIOException e) {
			e.printStackTrace();
			fail("unable to load data files content");
		}
		INavigationModel navDataProvider = sessionController.getSessionContext().getNavigationDataSupplier();
		assertNotNull(navDataProvider);

	}

	@Test
	public void injectPeriodTest() {
		assertNotNull(sessionController);
		try {
			IShiftPeriod period = controller.createShiftPeriod();
			assertNotNull(period);

			sessionController.getSessionContext().setShiftPeriod(period);

		} catch (Exception e) {
			fail("Error when creationg a Shifting Period: " + e.getMessage());
		}
	}

	@Test
	public void writeStateWithShiftPeriodTest() {

		assertNotNull(sessionController);
		IShiftPeriod period = controller.createShiftPeriod();
		assertNotNull(period);

		File fic = new File(SAVE_FILE_PATH);
		if (fic.exists()) {
			fic.delete();
		}
		assertFalse(fic.exists());

		assertNotNull(period);

		sessionController.getSessionContext().setShiftPeriod(period);

		sessionController.saveContextFile(SAVE_FILE_PATH);

		fic = new File(SAVE_FILE_PATH);
		assertTrue(fic.exists());
	}

}
