package fr.ifremer.globe.editor.navshift.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.core.runtime.AssertionFailedException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Test;

import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.commons.SounderNcInfoUtility;
import fr.ifremer.globe.editor.navshift.tests.NavShifterBaseTest;
import fr.ifremer.globe.utils.exception.GIOException;

public class SessionContextTest extends NavShifterBaseTest {

	private static final String TEST_FILE_PATH = TEST_DIR + File.separator + "0002_20090513_143735_ShipName_CIB.mbg";

	private static final String SAVE_FILE_PATH = TEST_DIR + File.separator + "navShifter" + File.separator
			+ "serializationTest.xml";

	private static final String LOAD_FILE_PATH = TEST_DIR + File.separator + "navShifter" + File.separator
			+ "navShifterContext.xml";

	@Test
	public void loadDataFilesTest() {
		logger.warn(getClass().getSimpleName() + ".loadDataFilesTest");

		assertNotNull(sessionController);
		List<String> filePaths = new ArrayList<>();
		List<File> files = new ArrayList<>();
		filePaths.add(MBG_FILE1);
		filePaths.add(MBG_FILE2);
		filePaths.add(MBG_FILE3);

		// add files to session Context
		for (String f : filePaths) {
			files.add(new File(f));
		}

		activateSession(files);
		ISessionContext sessContext = sessionController.getSessionContext();
		assertNotNull(sessContext);

		assertEquals(sessContext.getCurrentFileset().size(), 3);

		assertNotNull(sessContext.getSounderNcInfos());
		assertTrue(sessContext.getSounderNcInfos().size() == filePaths.size());
		assertNotNull(sessContext.getGeoBox());

		try {
			sessContext.setNavigationDataSupplier(navigationModelController.load(new NullProgressMonitor()));
		} catch (GIOException e) {
			e.printStackTrace();
			fail("unable to load data files content");
		}
		INavigationModel navDataProvider = sessionController.getSessionContext().getNavigationDataSupplier();
		assertNotNull(navDataProvider);

		sessionController.disposeSession();

	}

	@Test
	public void failWriteDeactivatedSessionTest() {
		logger.warn(getClass().getSimpleName() + ".failWriteDeactivatedSessionTest");

		assertNotNull(sessionController);

		File fic = new File(SAVE_FILE_PATH);
		if (fic.exists()) {
			fic.delete();
		}
		assertFalse(fic.exists());

		try {
			sessionController.saveContextFile(SAVE_FILE_PATH);
		} catch (AssertionFailedException e) {
			assertNull(sessionController.getSessionContext());
		}

		fic = new File(SAVE_FILE_PATH);
		assertFalse(fic.exists());
	}

	@Test
	public void writeSimpleContextTest() {
		logger.warn(getClass().getSimpleName() + ".writeSimpleContextTest");

		assertNotNull(sessionController);
		List<File> files = new ArrayList<>();
		files.add(new File(TEST_FILE_PATH));
		activateSession(files);
		ISessionContext sessContext = sessionController.getSessionContext();
		assertNotNull(sessContext);

		File fic = new File(SAVE_FILE_PATH);
		if (fic.exists()) {
			fic.delete();
		}
		assertFalse(fic.exists());

		sessionController.saveContextFile(SAVE_FILE_PATH);

		fic = new File(SAVE_FILE_PATH);
		assertTrue(fic.exists());
		sessionController.disposeSession();
	}

	@Test
	public void writeContextWithInputFilesTest() {
		logger.warn(getClass().getSimpleName() + ".writeContextWithInputFilesTest");

		assertNotNull(sessionController);
		List<String> filePaths = new ArrayList<>();
		List<File> files = new ArrayList<>();
		filePaths.add(MBG_FILE1);
		filePaths.add(MBG_FILE2);
		filePaths.add(MBG_FILE3);

		// add files to session Context
		for (String f : filePaths) {
			files.add(new File(f));
		}

		activateSession(files);
		ISessionContext sessContext = sessionController.getSessionContext();
		assertNotNull(sessContext);

		// file that will be used to save
		File fic = new File(SAVE_FILE_PATH);
		if (fic.exists()) {
			fic.delete();
		}
		assertFalse(fic.exists());

		// Save
		sessionController.saveContextFile(SAVE_FILE_PATH);

		fic = new File(SAVE_FILE_PATH);
		assertTrue(fic.exists());
	}

	@Test
	public void readFileDoesNotExistTest() {
		logger.warn(getClass().getSimpleName() + ".readFileDoesNotExistTest");

		assertNotNull(sessionController);
		try {
			sessionController.loadContextFile("this file does not exist.xml");
		} catch (GIOException e) {
			return;
		}
		fail("Should not be able to read not existing file");
	}

	@Test
	public void readContextFileTest() {
		logger.warn(getClass().getSimpleName() + ".readContextFileTest");

		assertNotNull(sessionController);
		File aFile = new File(LOAD_FILE_PATH);

		assertTrue(aFile.exists());

		sessionController.activateSession((sessionCtl, sessionContext) -> {
			/**
			 * Load the given context file
			 */

			try {
				sessionController.loadContextFile(aFile);
			} catch (GIOException e) {
				// Messages.openErrorMessage("File Error",
				// String.format("Unable to load selected Context file: %s", e.getMessage()));
			}

			try {
				List<ISounderNcInfo> sounderNcInfos = new ArrayList<ISounderNcInfo>();
				SounderNcInfoUtility.load(locateFile(sessionContext.getCurrentFileset()),
						list -> sounderNcInfos.addAll(list), sessionController.getSessionContext()::setGeoBox,
						Optional.empty());
				sessionContext.setSounderNcInfos(sounderNcInfos);
			} catch (GIOException e) {
				e.printStackTrace();
				fail("Unable to load selected Data files: " + e.getMessage());
				return false;
			}

			return true;

		});

		assertNotNull(sessionController.getSessionContext());
		assertTrue(sessionController.getSessionContext().getShiftPeriod().getEndTime() > 0);
		assertTrue(sessionController.getSessionContext().getShiftPeriod().getEndIndex() > 0);
		assertTrue(sessionController.getSessionContext().getShiftPeriod().getStartIndex() > 0);
		sessionController.disposeSession();

	}
}
