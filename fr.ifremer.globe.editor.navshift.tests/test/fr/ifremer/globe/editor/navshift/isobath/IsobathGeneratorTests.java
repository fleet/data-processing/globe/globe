/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.isobath;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.projection.Projection;
import fr.ifremer.globe.core.model.projection.ProjectionException;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.commons.Constants;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.impl.SessionContextImpl;
import fr.ifremer.globe.editor.navshift.tests.NavShifterBaseTest;
import fr.ifremer.globe.test.utils.BenchUtils;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;
import fr.ifremer.viewer3d.layers.lineisobaths.tools.IsobathGenerator;

/**
 * Tests of NavigationModelController class
 */
public class IsobathGeneratorTests extends NavShifterBaseTest {

	@Inject
	@Named(ContextNames.TMP_FOLDER)
	protected File tmpDir;

	@Inject
	@Named(ContextNames.MERCATOR_PROJECTION)
	protected Projection projection;

	private SessionContextImpl sessionContext;

	/** Fichiers à tester. */
	public static final String MBG_FILE1 = GlobeTestUtil.getTestDataPath()
			+ "/file/FusMBG/In/FiltTri_cel_tide_0034_20160325_173402_Thalia.mbg";

	@Override
	@Before
	public void setUp() {
		super.setUp();
		sessionContext = new SessionContextImpl();
		sessionContext.setGeoBox(new GeoBox());
		testContext.set(ContextNames.SESSION_CONTEXT, sessionContext);
		sessionContext.setCurrentFileset(Collections.singletonList(new File(MBG_FILE1)));
		tmpDir.mkdirs();
	}

	@Override
	@After
	public void tearDown() {
		FileUtils.deleteQuietly(tmpDir);
		testContext.remove(ContextNames.SESSION_CONTEXT);
		sessionContext = null;
		super.tearDown();
	}

	/**
	 * Test method
	 */
	@Test
	public void testMakeIsobath() {
		try {
			BenchUtils.timedPerformanceTest(IsobathGeneratorTests.class.getMethod("makeIsobath"), this);
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			Assert.fail("Test failed with exception: " + e.getMessage());
		}
	}

	/**
	 *
	 */
	public void makeIsobath() {
		logger.warn(getClass().getSimpleName() + ".makeIsobath");
		try {
			NullProgressMonitor monitor = new NullProgressMonitor();
			File isobathsFile = new File(tmpDir, "Isobaths");
			List<IProfile> profiles = sessionContext.getProfiles();

			for (IProfile profile : profiles) {
				IsobathGenerator isobathsGenerator = new IsobathGenerator();
				String layerName = String.valueOf(System.currentTimeMillis());
				File demFileInMercator = File.createTempFile(
						FilenameUtils.getBaseName(profile.getSounderNcInfo().getPath()) + "_InMercator_",
						Constants.TIFF, tmpDir);
				profileController.generateElevationTiFFFile(profile.getSounderNcInfo(),
						demFileInMercator.getAbsolutePath(), monitor);
				isobathsGenerator.generate(demFileInMercator, projection.getSpatialReference(), 0,
						sessionContext.getIsobathStep(), isobathsFile, layerName, monitor);
			}
		} catch (GIOException | IOException | ProjectionException | OperationCanceledException e) {
			e.printStackTrace();
			fail("Unable to create Isobaths");
		}
	}

}
