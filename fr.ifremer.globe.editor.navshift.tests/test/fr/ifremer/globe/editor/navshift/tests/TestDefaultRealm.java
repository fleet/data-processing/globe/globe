/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.tests;

import org.eclipse.core.databinding.observable.Realm;

/**
 * 
 * This Realm is used so that there is a default Realm set for unit testing
 *
 */
public class TestDefaultRealm extends Realm {

	private Realm previousRealm;

	public TestDefaultRealm() {
		previousRealm = super.setDefault(this);
	}

	/**
	 * @return always returns true
	 */
	@Override
	public boolean isCurrent() {
		return true;
	}

	@Override
	protected void syncExec(Runnable runnable) {
		runnable.run();
	}

	/**
	 * @throws UnsupportedOperationException
	 */
	@Override
	public void asyncExec(Runnable runnable) {
		throw new UnsupportedOperationException("asyncExec is unsupported");
	}

	/**
	 * Removes the realm from being the current and sets the previous realm to the default.
	 */
	public void dispose() {
		if (getDefault() == this) {
			setDefault(previousRealm);
		}
	}
}
