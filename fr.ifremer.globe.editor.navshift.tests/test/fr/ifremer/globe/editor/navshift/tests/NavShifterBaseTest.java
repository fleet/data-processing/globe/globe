/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.internal.workbench.E4Workbench;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.commons.SounderNcInfoUtility;
import fr.ifremer.globe.editor.navshift.controller.NavigationModelController;
import fr.ifremer.globe.editor.navshift.controller.ProfileController;
import fr.ifremer.globe.editor.navshift.controller.SessionController;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class NavShifterBaseTest {
	/*
	 * Test files
	 */

	public static final String MBG_FILE1 = GlobeTestUtil.getTestDataPath()
			+ "/file/FusMBG/In/FiltTri_cel_tide_0033_20160325_172722_Thalia.mbg";
	public static final String MBG_FILE2 = GlobeTestUtil.getTestDataPath()
			+ "/file/FusMBG/In/FiltTri_cel_tide_0034_20160325_173402_Thalia.mbg";
	public static final String MBG_FILE3 = GlobeTestUtil.getTestDataPath()
			+ "/file/FusMBG/In/FiltTri_cel_tide_0035_20160325_174855_Thalia.mbg";

	/*
	 * Pre-injection static values
	 */
	protected static Logger logger = LoggerFactory.getLogger(NavShifterBaseTest.class);

	protected static final String TEST_DIR = GlobeTestUtil.getTestDataPath() + File.separator + "file";

	protected IEclipseContext testContext;

	protected TestDefaultRealm myrealm;

	/*
	 * Injected Values
	 */

	@Inject
	@Named(ContextNames.SESSION_CONTROLLER)
	protected SessionController sessionController;
	@Inject
	@Named(ContextNames.NAVIGATION_MODEL_CONTROLLER)
	protected NavigationModelController navigationModelController;
	@Inject
	@Named(ContextNames.PROFILE_CONTROLLER)
	protected ProfileController profileController;
	@Inject
	protected IFileService fileService;

	@Before
	public void setUp() {
		logger.warn("setUp {}", getClass().getSimpleName());
		myrealm = new TestDefaultRealm();

		testContext = E4Workbench.getServiceContext();
		assertNotNull(testContext);
		ContextInitializer.activate(testContext);
		ContextInitializer.inject(this);
		assertNotNull(sessionController);
		assertNotNull(navigationModelController);
	}

	@After
	public void tearDown() {
		logger.warn("tearDown {}", getClass().getSimpleName());
		if (myrealm != null) {
			myrealm.dispose();
		}
		if (sessionController != null) {
			if (sessionController.getSessionContext() != null) {
				sessionController.disposeSession();
			}
		}
	}

	/**
	 * Activate a Session
	 */

	public ISessionContext activateSession(List<File> filePaths) {
		logger.warn("activateSession {}. Nb files = {}", getClass().getSimpleName(), filePaths.size());
		sessionController.activateSession((sessionCtl, sessionContext) ->

		{
			sessionContext.setCurrentFileset(filePaths);

			try {
				List<ISounderNcInfo> sounderNcInfos = new ArrayList<ISounderNcInfo>();
				SounderNcInfoUtility.load(sessionContext.getCurrentFileset(), list -> sounderNcInfos.addAll(list),
						sessionContext::setGeoBox, Optional.empty());

				sessionContext.setSounderNcInfos(sounderNcInfos);

			} catch (GIOException e) {
				// Messages.openErrorMessage("File Error",
				// String.format("Unable to load selected Data files: %s", e.getMessage()));
				return false;
			}

			return true;
		});
		assertNotNull(sessionController.getSessionContext());
		return sessionController.getSessionContext();
	}

	protected List<File> locateFile(List<File> files) {
		List<File> result = new ArrayList<>();
		for (File file : files) {
			if (!file.exists()) {
				file = new File(new File(GlobeTestUtil.getTestDataPath()), file.getPath().replace("C:\\", ""));
				logger.warn("FILE DOES NOT EXIST: " + file.getAbsolutePath());
			}
			if (!file.exists()) {
				logger.warn("FILE DOES NOT EXIST II: " + file.getAbsolutePath());
			}
			assertTrue(file.exists());
			logger.warn("locateFile {}", file.getAbsolutePath());
			result.add(file);
		}
		return result;
	}

}
