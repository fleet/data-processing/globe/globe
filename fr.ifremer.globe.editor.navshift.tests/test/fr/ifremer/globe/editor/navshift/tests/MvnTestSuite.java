package fr.ifremer.globe.editor.navshift.tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
/**
 * GLOBE - Ifremer
 */
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.ifremer.globe.editor.navshift.application.ContextInitializerTests;
import fr.ifremer.globe.editor.navshift.commons.SounderNcInfoSorterTests;
import fr.ifremer.globe.editor.navshift.controller.NavigationModelControllerTests;
import fr.ifremer.globe.editor.navshift.controller.SessionControllerTest;
import fr.ifremer.globe.editor.navshift.controller.ShiftPeriodControllerTest;
import fr.ifremer.globe.editor.navshift.isobath.IsobathGeneratorTests;
import fr.ifremer.globe.editor.navshift.model.SessionContextTest;
import fr.ifremer.globe.editor.navshift.model.ShiftPeriodTest;
import fr.ifremer.globe.editor.navshift.model.ShiftVectorTest;
import fr.ifremer.globe.editor.navshift.shifter.ApplyShiftedNavTest;
import fr.ifremer.globe.editor.navshift.shifter.NavShiftAlgorithmTest;
import fr.ifremer.globe.editor.navshift.shifter.NavShifterPerformanceTest;
import fr.ifremer.globe.utils.cache.TemporaryCache;

/**
 * Test suite for maven , declared test will be launched while compiling with command mvn integration-test
 */
@RunWith(Suite.class)

@SuiteClasses({ ContextInitializerTests.class, //
		SessionControllerTest.class, SessionContextTest.class, //
		NavigationModelControllerTests.class, //
		ShiftPeriodControllerTest.class, ShiftPeriodTest.class, ShiftVectorTest.class, //
		NavShiftAlgorithmTest.class, SounderNcInfoSorterTests.class, //
		NavShifterPerformanceTest.class, IsobathGeneratorTests.class, //
		ApplyShiftedNavTest.class })
public class MvnTestSuite {
	@AfterClass
	public static void cleanUp() {
		TemporaryCache.cleanAllFiles();
	}
}
