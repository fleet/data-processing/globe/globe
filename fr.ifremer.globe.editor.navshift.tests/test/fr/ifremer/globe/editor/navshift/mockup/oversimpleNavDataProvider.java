package fr.ifremer.globe.editor.navshift.mockup;

import org.eclipse.core.runtime.IProgressMonitor;

import fr.ifremer.globe.editor.navshift.model.INavigationDataUpdater;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.array.IDoubleArray;
import fr.ifremer.globe.utils.array.ILongArray;

/**
 * INavigationDataUpdater that only prints result to stdout, and provides data from a static array
 * 
 * @author cguychard
 *
 */
public class oversimpleNavDataProvider implements INavigationDataUpdater {

	private static double[][] navData = { { 5, 1, 2, 3, 4, 5, 6, 7, 8 }, //
			{ 6, 1, 2, 3, 4, 5, 6, 7, 8 }, //
			{ 7, 1, 2, 3, 4, 5, 6, 7, 8 }, //
			{ 8, 1, 2, 3, 4, 5, 6, 7, 8 }, //
			{ 9, 1, 2, 3, 4, 5, 6, 7, 8 }, //
			{ 10, 1, 2, 3, 4, 5, 6, 7, 8 }, //
	};

	@Override
	public int size() {
		return 6;
	}

	@Override
	public long getTime(int index) {
		return (long) navData[index][0];
	}

	@Override
	public double getLatitude(int index) {
		return navData[index][2];
	}

	@Override
	public double getLongitude(int index) {
		return navData[index][1];
	}

	@Override
	public void close() {
		return;
	}

	@Override
	public void updateNavPoint(int idx, long time, double longitude, double latitude) {
		System.out.println(
				"New Nav Point, at index " + idx + "at time " + time + "\t  Lat:" + latitude + "\t Long: " + longitude);

	}

	@Override
	public double getX(int index) {
		return -1;
	}

	@Override
	public double getY(int index) {
		return -1;
	}

	@Override
	public int getNearestPointIndex(double[] point) {
		return -1;
	}

	@Override
	public int getNearestPointIndexForDate(long dateval, MATCH_POLICY matchPolicy) {
		return 0;
	}

	@Override
	public boolean isEqual(INavigationModel other) {
		return true;
	}

	@Override
	public NavPointDifferences compare(INavigationModel other, double precision) {
		return null;
	}

	@Override
	public void setX(int index, double value) {
	}

	@Override
	public void setY(int index, double value) {
	}

	@Override
	public <T extends INavigationModel> T duplicate(IArrayFactory arrayFactory, IProgressMonitor progress) {
		return null;
	}

	@Override
	public void copyFrom(INavigationModel input, IArrayFactory arrayFactory, IProgressMonitor progress) {
		return;
	}

	@Override
	public double getZ(int index) {
		return 0;
	}

	@Override
	public ILongArray getTimes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IDoubleArray getLatitudes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IDoubleArray getLongitudes() {
		// TODO Auto-generated method stub
		return null;
	}

}
