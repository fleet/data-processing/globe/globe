/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.editor.navshift.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fr.ifremer.globe.core.model.file.IFileService;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.test.utils.BenchUtils;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

/**
 * Tests of SounderNcInfoSorter class
 */
public class SounderNcInfoSorterTests {

	/** Fichiers à tester. */
	public static final String MBG_FILE1 = GlobeTestUtil.getTestDataPath()
			+ "/file/FusMBG/In/FiltTri_cel_tide_0033_20160325_172722_Thalia.mbg";
	public static final String MBG_FILE2 = GlobeTestUtil.getTestDataPath()
			+ "/file/FusMBG/In/FiltTri_cel_tide_0034_20160325_173402_Thalia.mbg";
	public static final String MBG_FILE3 = GlobeTestUtil.getTestDataPath()
			+ "/file/FusMBG/In/FiltTri_cel_tide_0035_20160325_174855_Thalia.mbg";

	/**
	 * Test method for
	 * {@link fr.ifremer.globe.editor.navshift.commons.SounderNcInfoSorter#chronologicalSort(java.util.List)}.
	 */
	@Test
	public void testChronologicalSort() {
		List<ISounderNcInfo> sounderNcInfos = new ArrayList<>();
		// Fri Mar 25 18:27:22 CET 2016
		ISounderNcInfo sounderNcInfo1 = (ISounderNcInfo) IFileService.grab().getFileInfo(MBG_FILE1).orElse(null);
		// Fri Mar 25 18:34:02 CET 2016
		ISounderNcInfo sounderNcInfo2 = (ISounderNcInfo) IFileService.grab().getFileInfo(MBG_FILE2).orElse(null);
		// Fri Mar 25 18:48:55 CET 2016
		ISounderNcInfo sounderNcInfo3 = (ISounderNcInfo) IFileService.grab().getFileInfo(MBG_FILE3).orElse(null);

		// test in chronological order
		sounderNcInfos.add(sounderNcInfo1);
		sounderNcInfos.add(sounderNcInfo2);
		sounderNcInfos.add(sounderNcInfo3);
		List<ISounderNcInfo> result = SounderNcInfoUtility.chronologicalSort(sounderNcInfos);
		assertEquals("Wong number of elements", 3, result.size());
		assertSame("Wong order detected", sounderNcInfo1, result.get(0));
		assertSame("Wong order detected", sounderNcInfo2, result.get(1));
		assertSame("Wong order detected", sounderNcInfo3, result.get(2));

		// test in reverse order
		sounderNcInfos.set(0, sounderNcInfo3);
		sounderNcInfos.set(2, sounderNcInfo1);
		result = SounderNcInfoUtility.chronologicalSort(sounderNcInfos);
		assertEquals("Wong number of elements", 3, result.size());
		assertSame("Wong order detected", sounderNcInfo1, result.get(0));
		assertSame("Wong order detected", sounderNcInfo2, result.get(1));
		assertSame("Wong order detected", sounderNcInfo3, result.get(2));
	}

	@Test
	public void timedChronologicalSort() {
		try {
			BenchUtils.timedPerformanceTest(SounderNcInfoSorterTests.class.getMethod("testChronologicalSort"), this);
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			Assert.fail("Test failed with exception: " + e.getMessage());
		}
	}

}
