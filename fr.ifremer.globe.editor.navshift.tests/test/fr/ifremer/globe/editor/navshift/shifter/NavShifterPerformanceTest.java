package fr.ifremer.globe.editor.navshift.shifter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import jakarta.inject.Inject;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.commons.SounderNcInfoUtility;
import fr.ifremer.globe.editor.navshift.controller.NavigationModelController;
import fr.ifremer.globe.editor.navshift.model.INavigationDataUpdater;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.INavigationModel.NavPointDifferences;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.impl.NavigationModel;
import fr.ifremer.globe.editor.navshift.process.NavShifter;
import fr.ifremer.globe.editor.navshift.tests.NavShifterBaseTest;
import fr.ifremer.globe.test.utils.BenchUtils;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class NavShifterPerformanceTest extends NavShifterBaseTest {
	/*
	 * Test files, context restore
	 */

	public static final String SESSION_CTXT_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/navShifter/Test_CaraibesRecalage/sessionContext.xml";

	/*
	 *
	 */
	public static final String REF_RESULTING_MBG_DATA_FILE1 = GlobeTestUtil.getTestDataPath()
			+ "/file/navShifter/Test_CaraibesRecalage/Shift_extract_10.mbg";

	/*
	 * Pre-injection static values
	 */
	protected static Logger logger = LoggerFactory.getLogger(NavShifterPerformanceTest.class);

	protected static final String TEST_DIR = GlobeTestUtil.getTestDataPath() + File.separator + "file";

	/** Factory of {@link IArray}. */
	@Inject
	protected IArrayFactory arrayFactory;

	/*
	 * Other properties
	 */
	GeoBox geoBox;

	private INavigationDataUpdater destData;

	/**
	 * Performance Test from MBgs
	 */
	@Test
	public void testPerformWithMbg() {
		try {
			BenchUtils.timedPerformanceTest(NavShifterPerformanceTest.class.getMethod("performWithMbg"), this);
			compareWithRef();
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			Assert.fail("Test failed with exception: " + e.getMessage());
		}
	}

	public void performWithMbg() {
		logger.warn(getClass().getSimpleName() + ".performWithMbg");

		assertNotNull(sessionController);
		File aFile = new File(SESSION_CTXT_FILE);

		assertTrue(aFile.exists());

		sessionController.activateSession((sessionCtl, sessionContext) -> {
			/**
			 * Load the given context file
			 */

			try {
				sessionController.loadContextFile(aFile);
			} catch (GIOException e) {
				// Messages.openErrorMessage("File Error",
				// String.format("Unable to load selected Context file: %s", e.getMessage()));
			}

			try {
				List<ISounderNcInfo> sounderNcInfos = new ArrayList<ISounderNcInfo>();
				SounderNcInfoUtility.load(locateFile(sessionContext.getCurrentFileset()),
						list -> sounderNcInfos.addAll(list), sessionController.getSessionContext()::setGeoBox,
						Optional.empty());
				sessionController.getSessionContext().setSounderNcInfos(sounderNcInfos);
			} catch (GIOException e) {
				// Messages.openErrorMessage("File Error",
				// String.format("Unable to load selected Data files: %s", e.getMessage()));
				assertNull(sessionController.getSessionContext());
				return false;
			}

			return true;

		});

		INavigationModel navDataProvider = sessionController.getSessionContext().getNavigationDataSupplier();
		assertNotNull(navDataProvider);

		NavShifter shifter = new NavShifter();
		assertNotNull(shifter);

		NullProgressMonitor monitor = new NullProgressMonitor();
		destData = (INavigationDataUpdater) navDataProvider.duplicate(arrayFactory, monitor);
		assertNotNull(destData);
		assertTrue(navDataProvider.isEqual(destData));

		IShiftPeriod period = sessionController.getSessionContext().getShiftPeriod();
		assertNotNull(period);

		// Prepare and execute shift
		try {
			shifter.prepare(navDataProvider, period, monitor);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Error when preparing data");
		}

		try {
			shifter.shift(destData, monitor);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Error when shifting nav");
		}

	}

	public void compareWithRef() {
		logger.warn(getClass().getSimpleName() + ".compareWithRef");

		NullProgressMonitor monitor = new NullProgressMonitor();

		// Load reference result files and compare

		List<File> referenceFileset = new ArrayList<File>();
		referenceFileset.add(new File(REF_RESULTING_MBG_DATA_FILE1));

		List<ISounderNcInfo> refNavInfos = new ArrayList<>();
		try {
			SounderNcInfoUtility.load(referenceFileset, list -> refNavInfos.addAll(list), this::setGeoBox,
					Optional.empty());
		} catch (GIOException e) {
			// Messages.openErrorMessage("File Error",
			// String.format("Unable to load selected Data files: %s", e.getMessage()));
		}

		assertNotNull(refNavInfos.size() == refNavInfos.size());

		NavigationModelController refNavModelController = new NavigationModelController();
		ContextInitializer.inject(refNavModelController);
		try {
			refNavModelController.load(refNavInfos, monitor);
		} catch (GIOException e) {
			e.printStackTrace();
			fail("Unable to build Navigation Model");
		}
		assertNotNull(refNavModelController.getNavigationModel());

		NavigationModel refModel = refNavModelController.getNavigationModel();
		if (!refModel.isEqual(destData)) {
			NavPointDifferences diffRefResult = refModel.compare(destData, 6.4e-8);
			displayDiffs(diffRefResult, destData, refModel);
			if (diffRefResult.size() > 0) {
				fail(" Result not equal to reference data, there are " + diffRefResult.size() + " differences!");
			}
		}
	}

	void setGeoBox(GeoBox val) {
		geoBox = val;
	}

	void displayDiffs(NavPointDifferences diffs, INavigationModel target, INavigationModel source) {
		logger.warn(getClass().getSimpleName() + ".displayDiffs");

		System.out.println(" Result not equal to reference data, there are " + diffs.size() + " differences!");

		if (diffs.size() > 0) {
			IntStream iterate = IntStream.range(0, diffs.size());
			iterate.forEach(idx -> {
				int navIndex = diffs.getIndex(idx);
				int navDiff = diffs.getDiff(idx);
				System.out.println("Difference @ " + navIndex + " : ");
				if ((NavPointDifferences.DIFF_LAT & navDiff) != 0)
					System.out.println("                     LATITUDE " + source.getLatitude(navIndex) + " vs "
							+ target.getLatitude(navIndex) + " --> "
							+ (source.getLatitude(navIndex) - target.getLatitude(navIndex)));
				if ((NavPointDifferences.DIFF_LONG & navDiff) != 0)
					System.out.println("                     LONGITUDE " + source.getLongitude(navIndex) + " vs "
							+ target.getLongitude(navIndex) + " --> "
							+ (source.getLongitude(navIndex) - target.getLongitude(navIndex)));
				if ((NavPointDifferences.DIFF_TIME & navDiff) != 0)
					System.out.println("                     TIME " + source.getTime(navIndex) + " vs "
							+ target.getTime(navIndex));
			});
		}
	}

}
