package fr.ifremer.globe.editor.navshift.shifter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.junit.Before;
import org.junit.Test;

import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.controller.ShiftPeriodController;
import fr.ifremer.globe.editor.navshift.mockup.oversimpleNavDataProvider;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.INavigationDataUpdater;
import fr.ifremer.globe.editor.navshift.model.ISessionContext;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.IShiftVector;
import fr.ifremer.globe.editor.navshift.model.impl.SessionContextImpl;
import fr.ifremer.globe.editor.navshift.process.NavShifter;
import fr.ifremer.globe.editor.navshift.tests.NavShifterBaseTest;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.exception.GIOException;

public class NavShiftAlgorithmTest extends NavShifterBaseTest {

	private static final String TEST_FILE_PATH = TEST_DIR + File.separator + "0002_20090513_143735_ShipName_CIB.mbg";

	@Inject
	@Named(ContextNames.SHIFTPERIOD_CONTROLLER)
	protected ShiftPeriodController controller;

	/** Factory of {@link IArray}. */
	@Inject
	protected IArrayFactory arrayFactory;

	@Override
	@Before
	public void setUp() {
		super.setUp();
		List<File> files = new ArrayList<>();
		files.add(new File(TEST_FILE_PATH));

		ISessionContext sessionContext = activateSession(files);

		try {
			sessionContext.setNavigationDataSupplier(navigationModelController.load(new NullProgressMonitor()));
		} catch (GIOException e) {
			e.printStackTrace();
			fail("unable to load data files content");
		}
		INavigationModel navDataProvider = sessionController.getSessionContext().getNavigationDataSupplier();
		assertNotNull(navDataProvider);

	}

	@Test
	public void createNavShifterTest() {
		logger.warn(getClass().getSimpleName() + ".createNavShifterTest");
		try {
			assertNotNull(controller);
			NavShifter shifter = new NavShifter();
			assertNotNull(shifter);
		} catch (Exception e) {
			fail("Unable to create shifter: " + e.getMessage());
		}
	}

	@Test
	public void failPrepareTest() {
		logger.warn(getClass().getSimpleName() + ".failPrepareTest");

		NavShifter shifter = new NavShifter();
		assertNotNull(shifter);

		INavigationModel sourceData = sessionController.getSessionContext().getNavigationDataSupplier();
		assertNotNull(sourceData);

		IShiftPeriod period = controller.createShiftPeriod();
		assertNotNull(period);
		sessionController.getSessionContext().setShiftPeriod(period);
		try {
			shifter.prepare(sourceData, period, new NullProgressMonitor());
		} catch (IllegalArgumentException e) {
			assertEquals(period.getStartTime(), period.getEndTime(), 0.0);
		}
		shifter.reset();
	}

	@Test
	public void basicPrepareTest() {
		logger.warn(getClass().getSimpleName() + ".basicPrepareTest");

		NavShifter shifter = new NavShifter();
		assertNotNull(shifter);
		INavigationModel sourceData = new oversimpleNavDataProvider();
		((SessionContextImpl) sessionController.getSessionContext()).setNavigationDataSupplier(sourceData);

		IShiftPeriod period = controller.createShiftPeriod();
		assertNotNull(period);

		period.setStartIndex(1);
		period.setEndIndex(4);

		sessionController.getSessionContext().setShiftPeriod(period);

		createOverSimpleVectors(period, 2, 4);

		try {
			shifter.prepare(sourceData, period, new NullProgressMonitor());

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Error when preparing data");
		}
		shifter.reset();
	}

	@Test
	public void randomPrepareTest() {
		logger.warn(getClass().getSimpleName() + ".randomPrepareTest");

		NavShifter shifter = new NavShifter();
		assertNotNull(shifter);

		assertNotNull(sessionController);
		INavigationModel sourceData = null;
		List<String> filePaths = new ArrayList<String>();
		filePaths.add(TEST_FILE_PATH);
		try {
			sourceData = sessionController.getSessionContext().getNavigationDataSupplier();
			assertNotNull(sourceData);
		} catch (Exception e) {
			fail("Error when loading file: " + e.getMessage());
		}
		((SessionContextImpl) sessionController.getSessionContext()).setNavigationDataSupplier(sourceData);

		int max = sourceData.size();

		IShiftPeriod period = controller.createShiftPeriod();
		assertNotNull(period);

		period.setStartIndex((int) (max * 0.1));
		period.setEndIndex((int) (max * 0.9));

		createSomeTestVectors(period, 10);

		sessionController.getSessionContext().setShiftPeriod(period);

		try {
			shifter.prepare(sourceData, period, new NullProgressMonitor());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Error when preparing data");
		}
		shifter.reset();
	}

	@Test
	public void simpleShiftingTest() {
		logger.warn(getClass().getSimpleName() + ".simpleShiftingTest");

		NavShifter shifter = new NavShifter();
		assertNotNull(shifter);

		INavigationDataUpdater data = new oversimpleNavDataProvider();
		assertNotNull(data);
		((SessionContextImpl) sessionController.getSessionContext()).setNavigationDataSupplier(data);

		IShiftPeriod period = controller.createShiftPeriod();
		assertNotNull(period);

		period.setStartIndex(1);
		assertEquals(6, period.getStartTime(), 0);
		period.setEndIndex(4);
		assertEquals(9, period.getEndTime(), 0);

		createOverSimpleVectors(period, 2, 3);

		sessionController.getSessionContext().setShiftPeriod(period);

		NullProgressMonitor monitor = new NullProgressMonitor();

		try {
			shifter.prepare(data, period, monitor);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Error when preparing data");
		}

		try {
			shifter.shift(data, monitor);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Error when shifting nav");
		}
	}

	@Test
	public void startToEndShiftingTest() {
		logger.warn(getClass().getSimpleName() + ".startToEndShiftingTest");

		NavShifter shifter = new NavShifter();
		assertNotNull(shifter);

		INavigationDataUpdater data = new oversimpleNavDataProvider();
		assertNotNull(data);
		((SessionContextImpl) sessionController.getSessionContext()).setNavigationDataSupplier(data);

		IShiftPeriod period = controller.createShiftPeriod();
		assertNotNull(period);

		period.setStartIndex(0);
		assertEquals(5, period.getStartTime(), 0);
		period.setEndIndex(5);
		assertEquals(10, period.getEndTime(), 0);

		createOverSimpleVectors(period, 0, 5);

		sessionController.getSessionContext().setShiftPeriod(period);

		NullProgressMonitor monitor = new NullProgressMonitor();

		try {
			shifter.prepare(data, period, monitor);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Error when preparing data");
		}

		assertNotNull(shifter);
		try {
			shifter.shift(data, monitor);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Error when shifting nav");
		}
	}

	@Test
	public void randomShiftingTest() {
		logger.warn(getClass().getSimpleName() + ".randomShiftingTest");

		NavShifter shifter = new NavShifter();
		assertNotNull(shifter);

		INavigationModel sourceData = sessionController.getSessionContext().getNavigationDataSupplier();

		assertNotNull(sourceData);

		NullProgressMonitor monitor = new NullProgressMonitor();
		INavigationDataUpdater destData = sourceData.duplicate(arrayFactory, monitor);

		assertNotNull(destData);
		assertTrue(sourceData.isEqual(destData));
		int max = sourceData.size();

		IShiftPeriod period = controller.createShiftPeriod();
		assertNotNull(period);

		period.setStartIndex((int) (max * 0.1));
		period.setEndIndex((int) (max * 0.9));

		createSomeTestVectors(period, 10);

		sessionController.getSessionContext().setShiftPeriod(period);
		try {
			shifter.prepare(sourceData, period, monitor);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Error when preparing data");
		}

		assertNotNull(shifter);
		try {
			shifter.shift(destData, monitor);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Error when shifting nav");
		}
	}

	void createOverSimpleVectors(IShiftPeriod period, int startidx, int endidx) {
		logger.warn(getClass().getSimpleName() + ".createOverSimpleVectors");

		INavigationModel data = period.getNavigationDataSupplier();

		double x = 0.0, y = 0.0;
		x = data.getX(startidx) + 8;
		y = data.getY(startidx) + 0.5;
		IShiftVector vect = period.createAndAddShiftVector(startidx, x, y);
		assertNotNull(vect);

		x = data.getLongitude(endidx) - 5;
		y = data.getLatitude(endidx) - 7.2;
		vect = period.createAndAddShiftVector(endidx, x, y);
		assertNotNull(vect);
	}

	void createSomeTestVectors(IShiftPeriod period, int nb) {
		logger.warn(getClass().getSimpleName() + ".createSomeTestVectors");

		INavigationModel data = period.getNavigationDataSupplier();
		int start = period.getStartIndex();
		int end = period.getEndIndex();
		Random rd = new Random();
		IntStream.range(1, nb + 1).forEach(nbr -> {
			int idx = rd.nextInt(end) + start;
			double x = data.getLongitude(idx) + rd.nextDouble();
			double y = data.getLatitude(idx) + rd.nextDouble();
			IShiftVector vect = period.createAndAddShiftVector(idx, x, y);
			assertNotNull(vect);
		});
	}

}
