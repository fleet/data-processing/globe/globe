package fr.ifremer.globe.editor.navshift.shifter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import jakarta.inject.Inject;
import jakarta.inject.Named;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.ifremer.globe.core.io.nvi.service.INviWriter;
import fr.ifremer.globe.core.model.geo.GeoBox;
import fr.ifremer.globe.core.model.navigation.INavigationDataSupplier;
import fr.ifremer.globe.core.model.navigation.services.INavigationService;
import fr.ifremer.globe.core.model.navigation.utils.NavigationApplier;
import fr.ifremer.globe.core.model.sounder.ISounderNcInfo;
import fr.ifremer.globe.core.runtime.datacontainer.service.IDataContainerFactory;
import fr.ifremer.globe.editor.navshift.application.context.ContextInitializer;
import fr.ifremer.globe.editor.navshift.application.context.ContextNames;
import fr.ifremer.globe.editor.navshift.commons.Constants;
import fr.ifremer.globe.editor.navshift.commons.SounderNcInfoUtility;
import fr.ifremer.globe.editor.navshift.controller.NavigationModelController;
import fr.ifremer.globe.editor.navshift.model.INavigationDataUpdater;
import fr.ifremer.globe.editor.navshift.model.INavigationModel;
import fr.ifremer.globe.editor.navshift.model.INavigationModel.NavPointDifferences;
import fr.ifremer.globe.editor.navshift.model.IProfile;
import fr.ifremer.globe.editor.navshift.model.IShiftPeriod;
import fr.ifremer.globe.editor.navshift.model.impl.NavigationModel;
import fr.ifremer.globe.editor.navshift.model.impl.NavigationSupplierWrapper;
import fr.ifremer.globe.editor.navshift.process.NavShifter;
import fr.ifremer.globe.editor.navshift.tests.NavShifterBaseTest;
import fr.ifremer.globe.test.utils.BenchUtils;
import fr.ifremer.globe.ui.utils.dico.DicoBundle;
import fr.ifremer.globe.utils.array.IArray;
import fr.ifremer.globe.utils.array.IArrayFactory;
import fr.ifremer.globe.utils.exception.GIOException;
import fr.ifremer.globe.utils.test.GlobeTestUtil;

public class ApplyShiftedNavTest extends NavShifterBaseTest {

	@Inject
	@Named(ContextNames.TMP_FOLDER)
	protected File tmpDir;

	/** CycleContainer services. */
	@Inject
	protected IDataContainerFactory sounderDataContainerFactory;

	/** Writer of NVI files. */
	@Inject
	protected INviWriter nviWriter;

	/** Navigation services. */
	@Inject
	protected INavigationService navigationService;

	/*
	 * Test files, context restore
	 */

	public static final String SESSION_CTXT_FILE = GlobeTestUtil.getTestDataPath()
			+ "/file/navShifter/Test_CaraibesRecalage/sessionContext.xml";

	/*
	 *
	 */
	public static final String REF_RESULTING_MBG_DATA_FILE1 = GlobeTestUtil.getTestDataPath()
			+ "/file/navShifter/Test_CaraibesRecalage/Shift_extract_10.mbg";

	/*
	 * Pre-injection static values
	 */
	protected static Logger logger = LoggerFactory.getLogger(ApplyShiftedNavTest.class);

	protected static final String TEST_DIR = GlobeTestUtil.getTestDataPath() + File.separator + "file";

	/** Factory of {@link IArray}. */
	@Inject
	protected IArrayFactory arrayFactory;

	/*
	 * Other properties
	 */
	GeoBox geoBox;

	private INavigationDataUpdater destData;

	private ArrayList<ISounderNcInfo> sounderNcInfos;

	private File navTmpDir;

	private File nviFile;

	@Override
	@After
	public void setUp() {
		super.setUp();
		navTmpDir = new File(tmpDir, "applyNav");
		navTmpDir.mkdirs();
	}

	@Override
	@After
	public void tearDown() {
		super.tearDown();
		if (navTmpDir != null) {
			FileUtils.deleteQuietly(navTmpDir);
		}
	}

	/**
	 * Performance Test from MBgs
	 */
	@Test
	public void testPerformWithMbg() {
		try {
			BenchUtils.timedPerformanceTest(ApplyShiftedNavTest.class.getMethod("performWithMbg"), this);
			compareWithRef();
			BenchUtils.timedPerformanceTest(ApplyShiftedNavTest.class.getMethod("exportNavSingleFile"), this);
			BenchUtils.timedPerformanceTest(ApplyShiftedNavTest.class.getMethod("exportNavMultipleFiles"), this);
			BenchUtils.timedPerformanceTest(ApplyShiftedNavTest.class.getMethod("applyShiftedNav"), this);
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			Assert.fail("Test failed with exception: " + e.getMessage());
		}
	}

	/**
	 * Nav shifting with Mbg
	 */
	public void performWithMbg() {
		logger.warn(getClass().getSimpleName() + ".performWithMbg");

		assertNotNull(sessionController);
		File aFile = new File(SESSION_CTXT_FILE);

		assertTrue(aFile.exists());

		sessionController.activateSession((sessionCtl, sessionContext) -> {
			/**
			 * Load the given context file
			 */

			try {
				sessionController.loadContextFile(aFile);
			} catch (GIOException e) {
				logger.error(String.format("Unable to load selected Context file: %s", e.getMessage()), e);
				fail(String.format("Unable to load selected Context file: %s", e.getMessage()));
			}

			try {
				sounderNcInfos = new ArrayList<ISounderNcInfo>();
				SounderNcInfoUtility.load(locateFile(sessionContext.getCurrentFileset()),
						list -> sounderNcInfos.addAll(list), sessionController.getSessionContext()::setGeoBox,
						Optional.empty());
				sessionController.getSessionContext().setSounderNcInfos(sounderNcInfos);
			} catch (GIOException e) {
				// Messages.openErrorMessage("File Error",
				// String.format("Unable to load selected Data files: %s", e.getMessage()));
				assertNull(sessionController.getSessionContext());
				return false;
			}

			return true;

		});

		INavigationModel navDataProvider = sessionController.getSessionContext().getNavigationDataSupplier();
		assertNotNull(navDataProvider);

		NavShifter shifter = new NavShifter();
		assertNotNull(shifter);

		NullProgressMonitor monitor = new NullProgressMonitor();
		destData = (INavigationDataUpdater) navDataProvider.duplicate(arrayFactory, monitor);
		assertNotNull(destData);
		assertTrue(navDataProvider.isEqual(destData));

		IShiftPeriod period = sessionController.getSessionContext().getShiftPeriod();
		assertNotNull(period);

		// Prepare and execute shift
		try {
			shifter.prepare(navDataProvider, period, monitor);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Error when preparing data");
		}

		try {
			shifter.shift(destData, monitor);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			fail("Error when shifting nav");
		}

	}

	/**
	 * Export Nav in a single file
	 */
	public void exportNavSingleFile() {
		logger.warn(getClass().getSimpleName() + ".exportNavSingleFile");

		NavigationSupplierWrapper proxy = new NavigationSupplierWrapper(destData);

		try {
			// single File
			nviFile = File.createTempFile("Shift_Test_Result_SingleFile", "." + Constants.NVI, navTmpDir);

			nviWriter.write(proxy, nviFile.getAbsolutePath());
		} catch (GIOException | IOException e) {
			logger.error("Unable to save NAvigation : " + e.getMessage(), e);
		}
	}

	/**
	 *
	 */
	public void exportNavMultipleFiles() {
		logger.warn(getClass().getSimpleName() + ".exportNavMultipleFiles");

		NavigationSupplierWrapper proxy = new NavigationSupplierWrapper(destData);
		// Multiple files

		for (IProfile p : sessionController.getSessionContext().getProfiles()) {

			try {
				File outputFile = File.createTempFile(
						FilenameUtils.getBaseName(p.getSounderNcInfo().getPath()) + "_MulitpleFiles",
						"." + Constants.NVI, navTmpDir);

				List<Integer> indexesToExport = IntStream.range(p.getFirstIndex(), p.getLastIndex() + 1).boxed()
						.collect(Collectors.toList());

				nviWriter.write(proxy, outputFile.getAbsolutePath(), indexesToExport);
			} catch (GIOException | IOException e) {
				logger.error("Unable to save NAvigation :" + e.getMessage(), e);
			}
		}
	}

	/**
	 * Apply Nav to existing MBG
	 */
	public void applyShiftedNav() {
		logger.warn(getClass().getSimpleName() + ".applyShiftedNav");
		List<ISounderNcInfo> duplicatedNCInfos = new ArrayList<ISounderNcInfo>();

		// Duplicate input files
		try {
			for (ISounderNcInfo f : sounderNcInfos) {
				SounderNcInfoUtility.copy(f, navTmpDir);
				File duplicatedFile = new File(
						navTmpDir.getAbsolutePath() + File.separator + FilenameUtils.getBaseName(f.getPath()) + "."
								+ FilenameUtils.getExtension(f.getPath()));
				fileService.getSounderNcInfo(duplicatedFile).ifPresent(duplicatedNCInfos::add);
			}
		} catch (IOException e) {
			e.printStackTrace();
			fail("Unable to duplicate input files");
		}

		// Check if files are not booked
		if (nviFile != null) {
			String nviFilePath = nviFile.getAbsolutePath();
			try {
				// Get navigation file
				INavigationDataSupplier nviInfo = navigationService.getNavigationDataProvider(nviFilePath)
						.orElseThrow(() -> new GIOException("Invalid navigation file."));

				// Apply navigation on each sounder file
				NavigationApplier navApplier = new NavigationApplier();
				for (ISounderNcInfo sounderInfo : duplicatedNCInfos)
					navApplier.apply(sounderInfo, nviInfo);

			} catch (Exception e) {
				logger.error("Error during navigation application.", e);
				Display.getDefault()
						.asyncExec(() -> MessageDialog.openError(Display.getDefault().getActiveShell(),
								DicoBundle.getString("WORD_ERROR"),
								"Error during navigation application (" + e.getMessage() + ")"));
			}
		}

	}

	/**
	 *
	 */
	public void compareWithRef() {
		logger.warn(getClass().getSimpleName() + ".compareWithRef");

		NullProgressMonitor monitor = new NullProgressMonitor();

		// Load reference result files and compare

		List<File> referenceFileset = new ArrayList<File>();
		referenceFileset.add(new File(REF_RESULTING_MBG_DATA_FILE1));

		List<ISounderNcInfo> refNavInfos = new ArrayList<>();
		try {
			SounderNcInfoUtility.load(referenceFileset, list -> refNavInfos.addAll(list), this::setGeoBox,
					Optional.empty());
		} catch (GIOException e) {
			logger.error(String.format("Unable to load selected Data files: %s", e.getMessage()), e);
		}

		assertNotNull(refNavInfos.size() == refNavInfos.size());

		NavigationModelController refNavModelController = new NavigationModelController();
		ContextInitializer.inject(refNavModelController);
		try {
			refNavModelController.load(refNavInfos, monitor);
		} catch (GIOException e) {
			e.printStackTrace();
			fail("Unable to build Navigation Model");
		}
		assertNotNull(refNavModelController.getNavigationModel());

		NavigationModel refModel = refNavModelController.getNavigationModel();
		if (!refModel.isEqual(destData)) {
			NavPointDifferences diffRefResult = refModel.compare(destData, 6.4e-8);
			displayDiffs(diffRefResult, destData, refModel);
			if (diffRefResult.size() > 0) {
				fail(" Result not equal to reference data, there are " + diffRefResult.size() + " differences!");
			}
		}
	}

	void setGeoBox(GeoBox val) {
		geoBox = val;
	}

	void displayDiffs(NavPointDifferences diffs, INavigationModel target, INavigationModel source) {
		logger.warn(getClass().getSimpleName() + ".displayDiffs");

		System.out.println(" Result not equal to reference data, there are " + diffs.size() + " differences!");

		if (diffs.size() > 0) {
			IntStream iterate = IntStream.range(0, diffs.size());
			iterate.forEach(idx -> {
				int navIndex = diffs.getIndex(idx);
				int navDiff = diffs.getDiff(idx);
				System.out.println("Difference @ " + navIndex + " : ");
				if ((NavPointDifferences.DIFF_LAT & navDiff) != 0)
					System.out.println("                     LATITUDE " + source.getLatitude(navIndex) + " vs "
							+ target.getLatitude(navIndex) + " --> "
							+ (source.getLatitude(navIndex) - target.getLatitude(navIndex)));
				if ((NavPointDifferences.DIFF_LONG & navDiff) != 0)
					System.out.println("                     LONGITUDE " + source.getLongitude(navIndex) + " vs "
							+ target.getLongitude(navIndex) + " --> "
							+ (source.getLongitude(navIndex) - target.getLongitude(navIndex)));
				if ((NavPointDifferences.DIFF_TIME & navDiff) != 0)
					System.out.println("                     TIME " + source.getTime(navIndex) + " vs "
							+ target.getTime(navIndex));
			});
		}
	}

}
