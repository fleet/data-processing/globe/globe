package fr.ifremer.globe.utils.opengl;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;

public class OpenGLUtils {

	/**
	 * @param pDouble
	 *            double
	 * @return the string
	 */
	public static Boolean isShaderSupported(GL gl) {
		Boolean ret = (gl.isFunctionAvailable("glCreateShader") && gl.isFunctionAvailable("glShaderSource") && gl.isFunctionAvailable("glCompileShader") && gl.isFunctionAvailable("glCreateProgram")
				&& gl.isFunctionAvailable("glAttachShader") && gl.isFunctionAvailable("glLinkProgram") && gl.isFunctionAvailable("glValidateProgram") && gl.isFunctionAvailable("glUseProgram") && gl
				.isFunctionAvailable("glFramebufferTextureEXT"));
		return ret;
	}

	/**
	 * set current color in provided GL context
	 */
	public static void setAsGLColor(GL2 gl, int r, int g, int b) {
		gl.glColor3d(r / 255.0, g / 255.0, b / 255.0);
	}

	/**
	 * set current color in provided GL context
	 */
	public static int getMaxTextureSize(GL gl) {
		int[] maxSize = { Integer.MAX_VALUE };
		gl.glGetIntegerv(GL.GL_MAX_TEXTURE_SIZE, maxSize, 0);
		return maxSize[0];
	}

}
