/**
 * GLOBE - Ifremer
 */
package fr.ifremer.globe.slf4j;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * @author ldevos
 *
 */
public class Activator implements BundleActivator {

	/**
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		// Actuce pour éviter les warnings de SLF : c'est radical.
		String noErr = System.getProperty("NoErr");
		if (noErr != null) {
			System.err.close();
		}
	}

	/**
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
	}

}
